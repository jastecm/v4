$(function() {
	$( document ).ajaxStart(function() {
		$("body").css("cursor","wait");
	});
	
	$( document ).ajaxComplete(function() {
		$("body").css("cursor","auto");
	});
	
	$(document).on("keyup",".numberOnly",function(){
		$(this).val($(this).val().replace(/[^0-9]/gi,""));
	});
	
	$(document).on("keyup",".hourOnly",function(){
		if(Number($(this).val()) < 0) $(this).val(0);
		if(Number($(this).val()) > 23) $(this).val(23);
	});
	$(document).on("keyup",".minuteOnly",function(){
		if(Number($(this).val()) < 0) $(this).val(0);
		if(Number($(this).val()) > 59) $(this).val(59);
	});
	
	$(".btn_main").on("click",function(){
		$V4.move("/main");
	});
	
	$(".btn_login").on("click",function(){
		$V4.move("/login");
	});
	
	$(".datePicker").datepicker({});
	
	$(".datePicker").on("change",function(){
    	if($(this).val()){
    		var v = $(this).datepicker( "getDate" ).getTime();
    		var t = $(this).data("target");
    		$(t).val(v);
    		$(t).trigger("change");
    	}else{
    		$(t).val("");
    		$(t).trigger("change");
    	}
    });
});

jQuery.fn.serializeObject = function(f) {

	var obj = null;

	try {

		if (this[0].tagName && this[0].tagName.toUpperCase() == "FORM") {

			var arr = this.serializeArray();

			if (arr) {

				obj = {};

				jQuery.each(arr, function() {
					if( !f ) obj[this.name] = this.value;
					else if ( f && this.value.length != 0) {
						console.log(this.value,this.name);
						obj[this.name] = this.value;
					}
				});

			}

		}

	} catch (e) {

		alert(e.message);

	} finally {
		
	}

	return obj;

};
