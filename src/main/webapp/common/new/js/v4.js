
var $V4 = {};

(function(a){
	a.unitDate = _unitDate;
	a.unitLength = _unitLength;
	a.unitVolume = _unitVolume;
	a.unitTemperature = _unitTemperature;
	a.timezoneOffset = _timezoneOffset;
	a.path = _defaultPath;
	
	a.move = function(cmd,p){
		if(p){
			if(isJsonObject(p)){
				p.cmd = cmd;
			}else if(p[0].tagName && p[0].tagName.toUpperCase() == "FORM"){
				p = p.serializeObject();
				p.cmd = cmd;
			}else if(isJson(p)) {
				p = JSON.parse(p);
				p.cmd = cmd;
			} 
			
			a.instance_post("/index", p);
		}else{
			p = p?p:{};
			p.cmd = cmd;
			
			a.instance_post("/index", p);
		}
	}
	
	a.required = function(v,msg,focus){
		return (!v && (alert(msg)||1) && focus.focus() );
	}
	a.requiredMsg = function(v,msg){
		return (!v && (alert(msg)||1) );
	}
	a.fileUpload = function(d,o){
		var sync = true;
		if(o.sync) sync = false;
		$.ajax({
			url:a.path+"/api/1/fileUpload"
			,async: sync
			,contentType : false
			,processData : false
			,type:"POST"
			,data:d
			,cache:!1
			,headers : {"Cache-Control":"no-cache"}
			,success:function(t){
				try{
					!isJsonObject(t)&&(t = JSON.parse(t));
				}catch(e){
					
				}
				null!=o.success&&o.success(t);
			}		
			,error:function(t){
				try{
					!isJsonObject(t)&&(t = JSON.parse(t));
					t.responseText&&!t.responseJSON&&(t.responseJSON = JSON.parse(t.responseText));
				}catch(e){
					console.log(e);
				}
				null!=o.error&&o.error(t);
				null==o.error&&t.responseJSON?alert(t.status+"\n"+t.responseJSON.result):null==o.error&&console.log(t.status+"\n"+t.statusText);
				if(o.loading) {
					o.loading.show();
					$(".loading").remove();
				}
			}
			,beforeSend: function(xhr) {
				if(o.loading) {					
					o.loading.hide();
					o.loading.each(function(){
						var w = $(this).css("width");
						var h = $(this).css("height");
						var strHtml = "<img class='loading' src='"+_defaultPath+"/common/images/loading2.gif?"+new Date().getTime()+"' style='display:inline-block;width:"+w+";height:"+h+"'/>";						
						$(this).after(strHtml);
					})
				}
				null!=o.beforeSend&&o.beforeSend(xhr);
				null!=o.header&&$.each(o.header,function(i,j){xhr.setRequestHeader(i, j);});
				
			}
		}).done(function(){
			if(o.loading) {				
				o.loading.show();
				$(".loading").remove();				
			}
			null!=o.done&&o.done()
		});
		
	}
	
	a.http_post = function(u,d,o){
		
		var sync = true;
		if(o.sync) sync = false;
		var requestMethod = o.requestMethod?o.requestMethod:"POST";
		
		var deferred = $.ajax({
			url:a.path+u
			,async: sync
			,dataType : 'text'
			,contentType: "application/json; charset=UTF-8"
			,type:requestMethod
			,data:(requestMethod=="GET" || requestMethod=="DELETE")?d:JSON.stringify(d)
			,cache:!1
			,headers : {"Cache-Control":"no-cache"}
			,success:function(t){
				try{
					!isJsonObject(t)&&(t = JSON.parse(t));
				}catch(e){
					
				}
				null!=o.success&&o.success(t);
			}		
			,error:function(t){
				try{
					!isJsonObject(t)&&(t = JSON.parse(t));
					t.responseText&&!t.responseJSON&&(t.responseJSON = JSON.parse(t.responseText));
				}catch(e){
					console.log(e);
				}
				
				
				null!=o.error&&o.error(t);
				null==o.error&&t.responseJSON?alert(t.status+"\n"+isJsonObject(t.responseJSON.result)?JSON.stringify(t.responseJSON.result):t.responseJSON.result):null==o.error&&console.log(t.status+"\n"+t.statusText);
				if(o.loading) {
					o.loading.show();
					$(".loading").remove();
				}
			}
			,beforeSend: function(xhr) {
				if(o.loading) {					
					o.loading.hide();
					o.loading.each(function(){
						var w = $(this).css("width");
						var h = $(this).css("height");
						var strHtml = "<img class='loading' src='"+_defaultPath+"/common/images/loading2.gif?"+new Date().getTime()+"' style='display:inline-block;width:"+w+";height:"+h+"'/>";						
						$(this).after(strHtml);
					})
				}
				null!=o.beforeSend&&o.beforeSend(xhr);
				null!=o.header&&$.each(o.header,function(i,j){xhr.setRequestHeader(i, j);});
				null!=o.beforeCommonListLoad&&null!=o.commonListTarget&&o.beforeCommonListLoad(o.commonListTarget);
				
			}
		});
		
		
		deferred.done(function(){
			if(o.loading) {				
				o.loading.show();
				$(".loading").remove();				
			}
			null!=o.done&&o.done()
			null!=o.doneCommonListLoad&&o.doneCommonListLoad();
		});
		
		return deferred;
	}
	
	a.http_get = function(u,o){
		
		var sync = true;
		if(o.sync) sync = false;
		
		$.ajax({
			url:a.path+u
			,async: sync
			,dataType : 'json'
			,contentType: "application/x-www-form-urlencoded; charset=UTF-8"
			,type:"GET"
			,cache:!1
			,success:function(t){
				null!=o.success&&o.success(t)
			}		
			,error:function(t){
				null!=o.error&&o.error(t);
				null==o.error&&t.responseJSON?alert(t.status+"\n"+t.responseJSON.result):null==o.error&&console.log(t.status+"\n"+t.statusText);
				if(o.loading) {
					o.loading.show();
					$(".loading").remove();
				}
			}
			,beforeSend: function() {
				if(o.loading) {					
					o.loading.hide();
					o.loading.each(function(){
						var w = $(this).css("width");
						var h = $(this).css("height");
						var strHtml = "<img class='loading' src='"+_defaultPath+"/common/images/loading2.gif' style='display:inline-block;width:"+w+";height:"+h+"'/>";
						$(this).after(strHtml);
					})
				}
				null!=o.beforeSend&&o.beforeSend();
				null!=o.header&&o.header(xhr);
			}
		});
	}
	
	a.submit_post = function(u,f){
		f.attr("action",a.path+u);
		f.attr("method","POST");
		f.attr("accept-charset","UTF-8");
		f.submit();	
	}
	
	
	
	a.instance_post = function(u,p){
		$("body").append("<form id='instanceFrm'></form>");
		for (key in p) {
			var strHtml = '<input type="text" name="'+key+'" value="'+p[key]+'" >';
			$("#instanceFrm").append(strHtml);
		}
		$("#instanceFrm").attr("action",a.path+u);
		$("#instanceFrm").attr("method","POST");
		$("#instanceFrm").attr("accept-charset","UTF-8");
		$("#instanceFrm").submit();
		$("#instanceFrm").remove();
	}
	
	a.ajaxCreateApiSelect = function(url,param,target,listType,defaultVal,initVal,trigger,afterFunk,errorFunk){
		if(typeof afterFunk == "undefined"){
			afterFunk = function(){};
		}
		$.ajax({
			type : 'POST',
			dataType : 'json',
			url : a.path+url,
			contentType: "application/x-www-form-urlencoded; charset=UTF-8",
			data : param,
			success: function(result){
				if(listType) a.createSingleSelect(target,result.result,defaultVal,initVal,trigger,afterFunk,errorFunk);
				else a.createCodeValueSelect(target,result.result,defaultVal,initVal,trigger,afterFunk,errorFunk);
			},
			error:function(result){
				errorFunk&&errorFunk(result);
			}
		});
	}
	
	a.ajaxCreateSelect = function(url,param,target,listType,defaultVal,initVal,trigger,afterFunk){
		if(typeof afterFunk == "undefined"){
			afterFunk = function(){};
		}
		$.ajax({
			type : 'POST',
			dataType : 'json',
			url : a.path+url,
			contentType: "application/x-www-form-urlencoded; charset=UTF-8",
			data : param,
			success: function(result){
				if(listType) a.createSingleSelect(target,result.rtvList,defaultVal,initVal,trigger,afterFunk);
				else a.createCodeValueSelect(target,result.rtvList,defaultVal,initVal,trigger,afterFunk);
			},
			error:function(result){
			}
		});
	}
	
	a.createSingleSelect = function(target,valList,defaultVal,initVal,trigger,afterFunk){
		target.html("");
		
		var strHtml = "";
		
		if(defaultVal){
			strHtml += "<option value=''>"+defaultVal+"</option>";
		}
		
		for(var i = 0 ; i < valList.length ; i++){
			if(initVal == valList[i]) strHtml += "<option value='"+valList[i]+"' selected>"+valList[i]+"</option>";
			else strHtml += "<option value='"+valList[i]+"'>"+valList[i]+"</option>";
		}
		
		target.html(strHtml);
		
		null!=afterFunk&&afterFunk();
		null!=trigger&&target.trigger("change");
		
	}

	a.createCodeValueSelect = function(target,valList,defaultVal,initVal,trigger,afterFunk){
		target.html("");
		
		var strHtml = "";
		if(defaultVal){
			strHtml += "<option value=''>"+defaultVal+"</option>";
		}
		for(var i = 0 ; i < valList.length ; i++){
			if(initVal == valList[i].code) strHtml += "<option value='"+valList[i].code+"' selected>"+valList[i].codeName+"</option>";
			else strHtml += "<option value='"+valList[i].code+"'>"+valList[i].codeName+"</option>";
		}
		
		target.html(strHtml);
		
		null!=afterFunk&&afterFunk();
		null!=trigger&&target.trigger("change");
		
	}
	
	function PageNavigator(currentPage,rowPerPage,pagePerGroup){
		
		this.currentPage = currentPage;
		this.rowsPerPage = rowPerPage;
		this.pagePerGroup = pagePerGroup;
		this.startRow = 0;
		this.endRow = 0;
		this.totalSize = 0;
		this.pageTotal = 0;
		this.pageGroupStart = 0;
		this.pageGroupEnd = 0;
		
		this.settings = {
		
		};
		
		this._init();
		
	}

	$.extend(PageNavigator.prototype , {
		
		_init : function(){
			this.setCurrentPage(this.currentPage);
			this.setRowPerPage(this.rowsPerPage);
			this.setPagePerGroup(this.pagePerGroup);
			return this;
		},
		
		getCurrentPage : function() {
		    if ((this.pageTotal > 0) && (this.currentPage > this.pageTotal)) {
		    	this.currentPage = this.pageTotal;
		    }
		    return this.currentPage;
		},
		
		setCurrentPage : function(currentPage){
			if (currentPage < 1) currentPage = 1;
		    this.currentPage = currentPage;
		    this.calcStartEndRow(currentPage);
		    
		    
		    if(currentPage < this.pageGroupStart || currentPage > this.pageGroupEnd){
		    	this.setPageGroupStart();
		    	this.setPageGroupEnd();
		    }
		    
		},
		
		getRowPerPage : function(){
			return this.rowsPerPage;
		},
		
		setRowPerPage : function(rowsPerPage){
			this.rowsPerPage = rowsPerPage;
			this.setCurrentPage(1);
			this.calcStartEndRow(this.currentPage);
		    this.setPageGroupStart();
	    	this.setPageGroupEnd();
	    	
		},
		
		getPagePerGroup : function(){
			return this.pagePerGroup;
		},
		
		setPagePerGroup : function(pagePerGroup){
			this.pagePerGroup = pagePerGroup;
			this.calcStartEndRow(this.currentPage);
		},
		
		getStartRow : function(){
			 return this.startRow+1;
		},
		
		getEndRow : function(){
			return this.endRow+1;
		},
		
		getTotalSize : function(){
			return this.totalSize;
		},
		
		setTotalSize : function(totalSize){
			this.totalSize = totalSize;
		    this.setPageTotal();
		    this.setPageGroupStart();
		    this.setPageGroupEnd();
		},
		
		getPageGroupStart : function(){
			return this.pageGroupStart;
		},
		
		getPageGroupEnd : function() {
		    return Math.ceil(this.pageGroupEnd); 
		},
		
		getPageTotal : function() {
		    return this.pageTotal; 
		},
		
		setPageTotal : function(){
			var tempPageTotal = this.totalSize / this.rowsPerPage;
		    if (this.totalSize > tempPageTotal * this.rowsPerPage) ++tempPageTotal;
		    this.pageTotal = Math.ceil(tempPageTotal);
		},
		
		setPageGroupStart : function() {
		    this.pageGroupStart = ((this.currentPage - 1) / this.pagePerGroup * this.pagePerGroup + 1);
		    
		},
		
		setPageGroupEnd : function(){
			var tempPageGroupEnd = this.getPageGroupStart() + this.pagePerGroup;
			
		    if (tempPageGroupEnd > this.getPageTotal()) tempPageGroupEnd = this.getPageTotal() + 1;
		    
		    if (tempPageGroupEnd > 1) --tempPageGroupEnd;
		    
		    this.pageGroupEnd = tempPageGroupEnd;
		},
		/*
		setPageGroupEnd : function(totalPage) {
		    this.pageGroupEnd = totalPage;
		},
		 */
		
		calcStartEndRow : function(currentPage) {
		    this.startRow = ((currentPage - 1) * this.rowsPerPage + 1) -1;
		    this.endRow = (this.startRow + this.rowsPerPage - 1);
		}
	});
	
	
	
	function CommonList(idx){
		this.settings = {
			url : "" //request url
			,param : {} //request param
			,http_post_option : {}
			,thead : []
			,bodyHtml : function(data){return "<tr></tr>";}
			
			/*이하 option change 해도 아무기능없음*/
			,limit : 5
			,currentPage : 1
			,pagePerGroup : 10
			,initSearch : true
			,loadingBodyViewer : true
		};
		this.targetObj;
		this.pageObj;
		this.noContents = "";
		this.totalCnt = 0;
		this.pager = {
			currentPage : this.settings.currentPage
			,limit : this.settings.limit
			,pagePerGroup : this.settings.pagePerGroup
		};
		this.pageNavi;
		this.initNoContentsViewer = true;
		this.commonListIdx = idx;
		this.data;
	}
	//$("#vehiclePop").commonList("setParam",param).search(); //search  type1
	//$("#vehiclePop").commonList("search",param); //search type2
	//$("#vehiclePop").commonList("setLimit",5).search(); //limit change 
	
	//option 의 경우 기존 option그대로 인채로 다시 initionalize함
	//$("#vehiclePop").commonList("option",{}); //initSearch : false인경우 조회를 안하므로 주의
	//$("#vehiclePop").commonList("option",{bodyHtml : bodyHtml}); //settings.xxx changeType , 
	//$("#vehiclePop").commonList("option",{bodyHtml : bodyHtml , initSearch : true , thead : thead}); //settings.xxx changeType
	//$("#vehiclePop").commonList("option",{limit : 5 , pagePerGroup : 2 ,bodyHtml : bodyHtml ,initSearch : true , loadingBodyViewer : false}); //settings.xxx changeType
	
	
	
	$.extend(CommonList.prototype,{
		
		_initCommonList : function(){
			//console.log(this.options);			
			//a.http_post(a.path+this.settings.url,this.settings.data,this.option);
			$(this.targetObj).data("commonList","commonList"+this.commonListIdx);
			this._clearCommonList();
			this.createHeaderCommonList();
			this.pageNavi = new PageNavigator(1,this.pager.limit,this.pager.pagePerGroup);
			if(this.settings.initSearch) this.loadCommonList();
			else this.pageNavi.setTotalSize(0);
		},
		
		_clearCommonList : function(){
			$(this.targetObj).html("");
		},
		
		createHeaderCommonList : function(){
			var head = this.settings.thead;
			
			var htmlColgroup = $("<colgroup></colgroup>");
			var htmlHead = $("<thead><tr></tr></thead>");
			
			$(this.targetObj).append(htmlColgroup).append(htmlHead);
			
			var htmlCol = $("<col>");
			var htmlTh = $("<th scope='col'></th>");
			
			var headColGroup = htmlColgroup;
			
			var strHtmlCol = "",strHtmlTh = "";
			for(key in head){
				
				var inner = head[key];
				
				for(innerKey in inner){
					var col = htmlCol;
					col.css("width",inner[innerKey]);
					strHtmlCol += col.prop("outerHTML");
					
					var th = htmlTh;
					th.html(innerKey);
					strHtmlTh += th.prop("outerHTML");
					
				}
				
			}
			$(this.targetObj).find("colgroup").append(strHtmlCol);
			$(this.targetObj).find("thead").append(strHtmlTh);
			$(this.targetObj).append("<tbody></tbody>");
			this.noContents = "<tr><td colspan="+head.length+" style='width:881px'>데이터가 없습니다.</td></tr>";
			this.lodingContents = "<tr><td colspan="+head.length+" style='width:881px'>데이터로딩중!!!</td></tr>";
			if(this.initNoContentsViewer) $(this.targetObj).find("tbody").append(this.noContents);
		},
		
		loadCommonList : function(){
			var _this = this;
			var p = $.extend({} , this.settings.param);
			
			p.limit = this.pageNavi.getRowPerPage();
			p.offset = this.pageNavi.getStartRow()-1;
			if(p.offset == 0) p.counting = true;
			//console.log(p);
			
			$.extend(this.settings.http_post_option,{success : function(rtv){
				_this.settings.debugMode&&console.log(rtv);
				_this.data = rtv;				
				_this.drawBody();
				if(rtv.totalCnt) {
					_this.pageNavi.setTotalSize(rtv.totalCnt);
				}
				_this.createPaging();
			}});
			
			
			if(this.settings.loadingBodyViewer){
				this.settings.http_post_option.commonListTarget = this;
				this.settings.http_post_option.beforeCommonListLoad = function(t){
					$(t.targetObj).find("tbody").html(t.lodingContents);
				};				
			}
			
			a.http_post(this.settings.url,p,this.settings.http_post_option);
			
			
			
		},
		
		drawBody : function(){
			if(!this.data || this.data.length == 0 || this.data.result.length == 0) $(this.targetObj).find("tbody").html("").append(this.noContents);
			else{				
				var strHtml = this.settings.bodyHtml(this.data);
				
				$(this.targetObj).find('tbody').remove();
				$(this.targetObj).append(strHtml);
				
				//$(this.targetObj).find("tbody").html("").append(strHtml);
				this.checkboxAllEvent();
				this.settings.afterFunk&&this.settings.afterFunk();
			}
		},
		
		move : function(pageNum){
			this.pageNavi.setCurrentPage(pageNum);
			this.loadCommonList();
		},
		
		_searchCommonList : function(targetObj,param){
			
			this.settings.param = param;
			this.pageNavi.setTotalSize(0);
			this.move(1);
			
		},
		
		search : function(){
			this.pageNavi.setTotalSize(0);
			this.move(1);
		},
		
		_setParamCommonList : function(targetObj,param){
			this.settings.param = $.extend({},param);
			
			return this;
		},
	
		attachCommonList : function(target ,settings){
			$.extend( this.settings, settings);
			$.extend( this.pager, {limit:settings.limit,pagePerGroup:settings.pagePerGroup});
			
			
			this.targetObj = target;
			this._initCommonList();
		},
		
		createPaging : function(){
			var targetId = $(this.targetObj).attr("id");
			$("."+targetId).remove();
			var strHtml = '<div id="paging" class="'+$(this.targetObj).attr("id")+'"></div>';
			
			$(this.targetObj).after($(strHtml));
			this.pageObj = $(this.targetObj).next();
			
			/*
			console.log(this.pageNavi.getCurrentPage(),"current");
			console.log(this.pageNavi.getRowPerPage(),"rowPerPage");
			console.log(this.pageNavi.getPagePerGroup(),"pagePerGroup");
			console.log(this.pageNavi.getStartRow()),"startRow";
			console.log(this.pageNavi.getEndRow(),"endRow");
			console.log(this.pageNavi.getTotalSize(),"totalSize");
			console.log(this.pageNavi.getPageGroupStart(),"pageGroupStart");
			console.log(this.pageNavi.getPageGroupEnd(),"pageGroupEnd");
			*/
			
			var strHtml = ""; 
			var p = this.pageNavi;
			if(p.getPageTotal() != 1){
				if(p.getTotalSize() > 0){
					if(p.getCurrentPage()-p.getPagePerGroup() >=1){
						strHtml += '<a href="#none" class="first icon-spr" onClick="goPage(\''+$(this.targetObj).attr("id")+'\','+1+')">제일 처음으로 page(1)</a>';
						strHtml += '<a href="#none" class="prev icon-spr" onClick="goPage(\''+$(this.targetObj).attr("id")+'\','+(p.getCurrentPage()-p.getPagePerGroup())+')">이전으로 page('+(p.getCurrentPage()-p.getPagePerGroup())+')</a>';
					}else{
						if(p.getCurrentPage() == 1){
							strHtml += '<a href="#none" class="first icon-spr">제일 처음으로 page(none)</a>';
							strHtml += '<a href="#none" class="prev icon-spr">제일 처음으로 page(none)</a>';
						}else{
							strHtml += '<a href="#none" class="first icon-spr" onClick="goPage(\''+$(this.targetObj).attr("id")+'\','+1+')">제일 처음으로 page(1)</a>';
							strHtml += '<a href="#none" class="prev icon-spr" onClick="goPage(\''+$(this.targetObj).attr("id")+'\','+1+')">이전으로 page(1)</a>';
						}
						
					}
					
					
					for(var i = p.getPageGroupStart() ; i <= p.getPageGroupEnd() ; i++){
						if(i == p.getCurrentPage()){
							strHtml += '<span class="current">'+i+'</span>';
						}else{
							strHtml += '<a href="#none" class="num" onClick="goPage(\''+$(this.targetObj).attr("id")+'\','+i+')">'+i+'</a>';
						}
					}
					
					if(p.getPageGroupStart()+p.getPagePerGroup() <= p.getPageTotal()){
						strHtml += '<a href="#none" class="next icon-spr" onClick="goPage(\''+$(this.targetObj).attr("id")+'\','+(p.getPageGroupStart()+p.getPagePerGroup())+')" >다음으로 page('+(p.getPageGroupStart()+p.getPagePerGroup())+')</a>';
						strHtml += '<a href="#none" class="last icon-spr" onClick="goPage(\''+$(this.targetObj).attr("id")+'\','+(p.getPageTotal())+')">제일 마지막으로 page('+p.getPageTotal()+')</a>';
					}else{
						if(p.getPageTotal() == p.getCurrentPage()){
							strHtml += '<a href="#none" class="next icon-spr">다음으로 page(none)</a>';
							strHtml += '<a href="#none" class="last icon-spr">제일 마지막으로 page(none)</a>';
						}else{
							strHtml += '<a href="#none" class="next icon-spr" onClick="goPage(\''+$(this.targetObj).attr("id")+'\','+(p.getPageTotal())+')">다음으로 page('+p.getPageTotal()+')</a>';
							strHtml += '<a href="#none" class="last icon-spr" onClick="goPage(\''+$(this.targetObj).attr("id")+'\','+(p.getPageTotal())+')">제일 마지막으로 page('+p.getPageTotal()+')</a>';
						}
					}
				}
				
			}
			
			$(this.pageObj).html(strHtml);
		},
		
		_goPageCommonList : function(targetObj,pageNum){
			this.move(pageNum);
			
		},
		
		_setLimitCommonList : function(targetObj,limit){
			this.pageNavi.setRowPerPage(limit);
			
			return this;
		},
		
		_optionCommonList : function(targetObj,args){
			this.initNoContentsViewer = false;
			this.attachCommonList(targetObj,args);
		},
		
		_getDataCommonList : function(targetObj){
			return this.data;
		}
		
		,checkboxAllEvent : function(){
			var _this = this;
			var targetCheck = $(this.targetObj).find("thead input:checkbox");
			
			if(targetCheck.length > 0){
				
				var eventHander = true;
				var e = $._data( $(targetCheck).get(0), 'events' );
				$.each( e , function(i,o){
					if(i == "click") eventHander = false;
				});
				if(eventHander) {
					$(targetCheck).on("click",function(e){
						var targetObjBody = $(_this.targetObj).find("tbody input:checkbox");
						if($(this).is(":checked")){
							targetObjBody.prop("checked",true);
						}else{
							targetObjBody.prop("checked",false);
						}
					});
					
				}
			}
		},
		_refreshCommonList : function(){
			
			var pageNum = this.pageNavi.getCurrentPage();
			this.move(pageNum);
		},
		
		_getParamCommonList : function(){
			return this.settings.param;
		}
	});
	
	$.fn.commonList = function(options){
		var otherArgs = Array.prototype.slice.call( arguments, 1 );
		
		if ( typeof options === "string" && ( options === "goPage" || options === "setParam" || options === "search" || options === "setLimit") || options === "getData" || options === "refresh" || options === "getParam") {
			return $.commonList[ "_" + options+"CommonList"].apply( $.commonList, [ this[ 0 ] ].concat( otherArgs ) );
		}
		
		if ( options === "option" && arguments.length === 2) {
			return $.commonList[ "_" + options+"CommonList"].apply( $.commonList, [ this[ 0 ] ].concat( otherArgs ) );
		}
		return this.each( function() {
			typeof options === "string" ?
				$.commonList[ "_" + options+"CommonList"].apply( $.commonList, [ this[ 0 ] ].concat( otherArgs ) ) :
				$.commonList.attachCommonList( this, options );
		} );
	}
	
	$.fn.commonList2 = function(options){
		var otherArgs = Array.prototype.slice.call( arguments, 1 );
		
		if ( typeof options === "string" && ( options === "goPage" || options === "setParam" || options === "search" || options === "setLimit")  || options === "getData" || options === "refresh" || options === "getParam") {
			return $.commonList2[ "_" + options+"CommonList"].apply( $.commonList2, [ this[ 0 ] ].concat( otherArgs ) );
		}
		
		if ( options === "option" && arguments.length === 2) {
			return $.commonList2[ "_" + options+"CommonList"].apply( $.commonList2, [ this[ 0 ] ].concat( otherArgs ) );
		}
		return this.each( function() {
			typeof options === "string" ?
				$.commonList2[ "_" + options+"CommonList"].apply( $.commonList2, [ this[ 0 ] ].concat( otherArgs ) ) :
				$.commonList2.attachCommonList( this, options );
		} );
	}
	
	$.fn.commonList3 = function(options){
		var otherArgs = Array.prototype.slice.call( arguments, 1 );
		
		
		if ( typeof options === "string" && ( options === "goPage" || options === "setParam" || options === "search" || options === "setLimit")  || options === "getData" || options === "refresh" || options === "getParam") {
			return $.commonList3[ "_" + options+"CommonList"].apply( $.commonList3, [ this[ 0 ] ].concat( otherArgs ) );
		}
		
		if ( options === "option" && arguments.length === 2) {
			return $.commonList3[ "_" + options+"CommonList"].apply( $.commonList3, [ this[ 0 ] ].concat( otherArgs ) );
		}
		return this.each( function() {
			typeof options === "string" ?
				$.commonList3[ "_" + options+"CommonList"].apply( $.commonList3, [ this[ 0 ] ].concat( otherArgs ) ) :
				$.commonList3.attachCommonList( this, options );
		} );
	}
	
	$.fn.commonList4 = function(options){
		var otherArgs = Array.prototype.slice.call( arguments, 1 );
		
		
		if ( typeof options === "string" && ( options === "goPage" || options === "setParam" || options === "search" || options === "setLimit")  || options === "getData" || options === "refresh" || options === "getParam") {
			return $.commonList4[ "_" + options+"CommonList"].apply( $.commonList4, [ this[ 0 ] ].concat( otherArgs ) );
		}
		
		if ( options === "option" && arguments.length === 2) {
			return $.commonList4[ "_" + options+"CommonList"].apply( $.commonList4, [ this[ 0 ] ].concat( otherArgs ) );
		}
		return this.each( function() {
			typeof options === "string" ?
				$.commonList4[ "_" + options+"CommonList"].apply( $.commonList4, [ this[ 0 ] ].concat( otherArgs ) ) :
				$.commonList4.attachCommonList( this, options );
		} );
	}
	
	$.fn.commonList5 = function(options){
		var otherArgs = Array.prototype.slice.call( arguments, 1 );
		
		
		if ( typeof options === "string" && ( options === "goPage" || options === "setParam" || options === "search" || options === "setLimit")  || options === "getData" || options === "refresh" || options === "getParam") {
			return $.commonList5[ "_" + options+"CommonList"].apply( $.commonList5, [ this[ 0 ] ].concat( otherArgs ) );
		}
		
		if ( options === "option" && arguments.length === 2) {
			return $.commonList5[ "_" + options+"CommonList"].apply( $.commonList5, [ this[ 0 ] ].concat( otherArgs ) );
		}
		return this.each( function() {
			typeof options === "string" ?
				$.commonList5[ "_" + options+"CommonList"].apply( $.commonList5, [ this[ 0 ] ].concat( otherArgs ) ) :
				$.commonList5.attachCommonList( this, options );
		} );
	}
	
	$.fn.commonList6 = function(options){
		var otherArgs = Array.prototype.slice.call( arguments, 1 );
		
		
		if ( typeof options === "string" && ( options === "goPage" || options === "setParam" || options === "search" || options === "setLimit")  || options === "getData" || options === "refresh" || options === "getParam") {
			return $.commonList6[ "_" + options+"CommonList"].apply( $.commonList6, [ this[ 0 ] ].concat( otherArgs ) );
		}
		
		if ( options === "option" && arguments.length === 2) {
			return $.commonList6[ "_" + options+"CommonList"].apply( $.commonList6, [ this[ 0 ] ].concat( otherArgs ) );
		}
		return this.each( function() {
			typeof options === "string" ?
				$.commonList6[ "_" + options+"CommonList"].apply( $.commonList6, [ this[ 0 ] ].concat( otherArgs ) ) :
				$.commonList6.attachCommonList( this, options );
		} );
	}
	
	$.commonList = new CommonList(1);
	$.commonList2 = new CommonList(2);
	$.commonList3 = new CommonList(3);
	$.commonList4 = new CommonList(4);
	$.commonList5 = new CommonList(5);
	$.commonList6 = new CommonList(6);
	
	function CommonPop(){
		
		this.settings = {
			
			type : ""
				
			//request
			,url : ""
			,param : {}
			,http_post_option : {}
			,selType : "checkbox"
			,action : {}
				
			//paging	
			,limit : 10
			,currentPage : 1
			,pagePerGroup : 10
			
			//targetObj
			,targetTable : null
			,targetCounter : null
			,targetSearchType : null
			,targetSearchText : null
			,targetSearchBtn : null
			,targetCancleBtn : null
			,targetOkBtn : null			
			
		};
		this.targetObj;				
		this.pager = {
			currentPage : this.settings.currentPage
			,limit : this.settings.limit
			,pagePerGroup : this.settings.pagePerGroup
		};
	}
	
	
	
	
	
	$.extend(CommonPop.prototype,{
		attachCommonPop : function(target ,settings){
			this.targetObj = target;
			this._initCommonPop(settings);
		},
		
		_initCommonPop : function(s){
			var _this = this;
			
			$(this.targetObj).on("click",function(e){
				
				var settings = $(this).data();
				_this.targetObj = $(this);
				
				if(settings.type == "user"){
					settings.url = "/api/v1/account";
					settings.http_post_option = $.extend(settings.http_post_option,{requestMethod : "GET",counting:true}); 
					settings.popObj = "#commonUserDialog";						
				}else if(settings.type == "vehicle"){
					settings.url = "/api/v1/vehicle";
					settings.http_post_option = $.extend(settings.http_post_option,{requestMethod : "GET",counting:true});
					settings.popObj = "#commonVehicleDialog";
				}else if(settings.type == "group"){
					settings.url = "/api/v1/groupTree";
					settings.http_post_option = $.extend(settings.http_post_option,{requestMethod : "GET",counting:true});
					settings.popObj = "#commonGroupDialog";
				}
				
				settings.limit = 10;
				settings.targetTable = $(settings.popObj).find("table");
				settings.targetCounter = $(settings.popObj).find(".commonCounter");
				settings.targetSearchType = $(settings.popObj).find(".commonSearchType");
				settings.targetSearchText = $(settings.popObj).find(".commonSearchText");
				settings.targetSearchBtn = $(settings.popObj).find(".btn_commonSearch");
				settings.targetCancleBtn = $(settings.popObj).find(".btn_commonPopCancle");
				settings.targetOkBtn = $(settings.popObj).find(".btn_commonPopOk");
				 
				!settings.extendParam && (settings.extendParam = {});
				$.extend( _this.settings, settings);
				$.extend( _this.pager, {limit:settings.limit});
				
				
				$(_this.settings.popObj).dialog({
			        autoOpen: true,
			        show: {
			            duration: 500
			        },
			        width: '960',
			        modal: true
			    });
				
				
				//카운터 reset
				$(_this.settings.targetCounter).html(0);
				
				//searchType reset
				$(_this.settings.targetSearchType).val("");
								
				//searchText reset
				var initSearchText = $(_this.settings.targetInput)?$(_this.settings.targetInput).val():"";
				$(_this.settings.targetSearchText).val(initSearchText);
				
				//okBtn unbind
				$(_this.settings.targetOkBtn).off("click");
				//cancle unbind
				$(_this.settings.targetCancleBtn).off("click");
				
				
				//okBtn rebind
				$(_this.settings.targetOkBtn).on("click",function(){				
					if(_this.settings.action.ok) {
						_this.settings.action.ok($(_this.settings.targetTable).find("tbody input:checked"),_this.settings.targetInput,_this.settings.targetInputValue,_this.targetObj);
						$(_this.settings.popObj).dialog("close");
					}
				});
				//cancle rebind
				$(_this.settings.targetCancleBtn).on("click",function(){				
					$(_this.settings.popObj).dialog("close");
				});
				
				
				
				_this.loadLazy(settings.debugMode);
			});
			
			
			var settings = $(this.targetObj).data();
			
			settings.targetObj = $(this.targetObj);
			if(settings.type == "user"){				
				settings.popObj = "#commonUserDialog";
					
			}
			else if(settings.type == "vehicle"){				
				settings.popObj = "#commonVehicleDialog";
			}
			else if(settings.type == "group"){
				settings.popObj = "#commonGroupDialog";
			}
			
			
			settings.targetSearchBtn = $(settings.popObj).find(".btn_commonSearch");
			settings.targetSearchText = $(settings.popObj).find(".commonSearchText");
			
			//target input enter search
			if($(settings.targetInput)){
				settings.targetInput.on("keyup",function(e){
					if(e.keyCode == 13) {
						$(settings.targetObj).trigger("click");
					}
				});
			}
			
			//pop search btn event - just one event 
			if($(settings.targetSearchBtn)){
				var eventHander = true;
				var e = $._data( $(settings.targetSearchBtn).get(0), 'events' );
				$.each( e , function(i,o){
					if(i == "click") eventHander = false;
				});
				if(eventHander) $(settings.targetSearchBtn).on("click",function(e){
					_this.loadLazy();
				});
			}
			
			//pop search text enter search event - just one event
			if($(settings.targetSearchText)){
				var eventHander = true;
				var e = $._data( $(settings.targetSearchText).get(0), 'events' );
				$.each( e , function(i,o){
					if(i == "keyup") eventHander = false;
				});
				if(eventHander) $(settings.targetSearchText).on("keyup",function(e){
					if(e.keyCode == 13) _this.loadLazy();
				});
			}
			
		},
		
		loadLazy : function(debugMode){
			
			var p = {searchType : $(this.settings.targetSearchType).val()
					,searchText : $(this.settings.targetSearchText).val()}
			
			this.settings.extendParam&&$.extend(p,this.settings.extendParam);
			
			$(this.settings.targetTable).commonLazy({
				url : this.settings.url
				,http_post_option : this.settings.http_post_option
				,thead : this.createThead()
				,param : p
				,limit : 10
				,bodyHtml : this.createBody()
				,resultCnt : this.resultCnt()
				,debugMode : debugMode
			});
		},
		createThead : function(){
			if(this.settings.type == "user"){
				if(this.settings.selType == "checkbox") return ['<input type="checkbox" />','부서명','성명','아이디','연락처','등록일'];
				else if(this.settings.selType == "radio") return ['','부서명','성명','아이디','연락처','등록일'];
			}else if(this.settings.type == "vehicle"){
				if(this.settings.selType == "checkbox") return ['<input type="checkbox" />','소속부서','차종','차량번호','OBO종류','OBO 단말번호','등록일'];
				else if(this.settings.selType == "radio") return ['','소속부서','차종','차량번호','OBO종류','OBO 단말번호','등록일'];
			}else if(this.settings.type == "group"){
				if(this.settings.selType == "checkbox") return ['<input type="checkbox" />','상위부서명','부서명','부서장명','구성원','구성차량','등록일'];
				else if(this.settings.selType == "radio") return ['','상위부서명','부서명','부서장명','구성원','구성차량','등록일'];
			}
			
		},
		createBody : function(){
			if(this.settings.type == "user"){
				if(this.settings.selType == "checkbox"){
					return function(data){
						var strHtml = "";
						console.log(data);
						for(i in data.result){
							var obj = data.result[i];
							strHtml += "<tr>";									
							strHtml	+= "<td><input type='checkbox' class='commonUserPopChk' data-info='"+jsonObjToBase64(obj)+"'/></td>";
							strHtml += "<td>" ;
							strHtml += (obj.group&&obj.group.parentGroupNm)?(convertNullString(obj.group.parentGroupNm)):"";
							strHtml += (obj.group&&obj.group.groupNm)?("/"+convertNullString(obj.group.groupNm)):"";
							strHtml += "</td>";
							strHtml += "<td>" + convertNullString(obj.name) + "</td>";
							strHtml += "<td>" + convertNullString(obj.accountId) + "</td>";
							strHtml += "<td>" + convertNullString(obj.mobilePhoneCtyCode) +" "+convertNullString(obj.mobilePhone)+"<br />"+ convertNullString(obj.phoneCtyCode) +" "+convertNullString(obj.phone)+"</td>";									
							strHtml += "<td>" + convertDateUint(new Date(obj.joinDate),a.unitDate,a.timezoneOffset,0) + "</td>";
							strHtml += "</tr>";
						}
						return strHtml;
					}
				}else if(this.settings.selType == "radio"){
					return function(data){
						var strHtml = "";
						for(i in data.result){
							var obj = data.result[i];
							strHtml += "<tr>";									
							strHtml	+= "<td><input type='radio' name='commonUserPopRadio' data-info='"+jsonObjToBase64(obj)+"'/></td>";
							strHtml += "<td>" ;
							strHtml += (obj.group&&obj.group.parentGroupNm)?(convertNullString(obj.group.parentGroupNm)):"";
							strHtml += (obj.group&&obj.group.groupNm)?("/"+convertNullString(obj.group.groupNm)):"";
							strHtml += "</td>";
							strHtml += "<td>" + convertNullString(obj.name) + "</td>";
							strHtml += "<td>" + convertNullString(obj.accountId) + "</td>";
							strHtml += "<td>" + convertNullString(obj.mobilePhoneCtyCode) +" "+convertNullString(obj.mobilePhone)+"<br />"+ convertNullString(obj.phoneCtyCode) +" "+convertNullString(obj.phone)+"</td>";									
							strHtml += "<td>" + convertDateUint(new Date(obj.joinDate),a.unitDate,a.timezoneOffset,0) + "</td>";
							strHtml += "</tr>";
									
						}
						return strHtml;
					}
				}
			}else if(this.settings.type == "vehicle"){
				if(this.settings.selType == "checkbox"){
					return function(data){
						var strHtml = "";
						for(i in data.result){
							var obj = data.result[i];
							
							strHtml += "<tr>";									
							strHtml += "<td><input type='checkbox' class='commonVehiclePopChk' data-info='"+jsonObjToBase64(obj)+"'/></td>";
							strHtml +="<td>";
							if(obj.groups && obj.groups.length != 0){
								for(var i = 0 ; i < obj.groups.length ; i++){
									group = obj.groups[i];
									if(i == 0){
										strHtml += (group.parentGroupNm)?(convertNullString(group.parentGroupNm)):"";
										strHtml += (group.groupNm)?("/"+convertNullString(group.groupNm)):"";
									}else{
										strHtml += "</br>";
										strHtml += (group.parentGroupNm)?(convertNullString(group.parentGroupNm)):"";
										strHtml += (group.groupNm)?("/"+convertNullString(group.groupNm)):"";
									}
										 
								}
							}
							strHtml +="</td>";
							strHtml += "<td>" + convertNullString(obj.vehicleModel.modelMaster) + "</td>";
							strHtml += "<td>" + obj.plateNum + "</td>";
							strHtml +="<td>";
							if(obj.device) strHtml += obj.device.deviceSeries;
							strHtml +="</td>";
																
							strHtml +="<td>";
							if(obj.device) strHtml += obj.device.deviceSn;
							strHtml +="</td>";
							
							strHtml += "<td>" + convertDateUint(new Date(obj.regDate),a.unitDate,a.timezoneOffset,0) + "</td>";
							strHtml += "</tr>";
						}
						return strHtml;
					}
				}else if(this.settings.selType == "radio"){
					return function(data){
						var strHtml = "";
						for(i in data.result){
							var obj = data.result[i];
							
							strHtml += "<tr>";									
							strHtml +="<td><input type='radio' name='commonVehiclePopRadio' data-info='"+jsonObjToBase64(obj)+"'/></td>";
							strHtml +="<td>";
							if(obj.groups && obj.groups.length != 0){
								for(var i = 0 ; i < obj.groups.length ; i++){
									group = obj.groups[i];
									if(i == 0){
										strHtml += (group.parentGroupNm)?(convertNullString(group.parentGroupNm)):"";
										strHtml += (group.groupNm)?("/"+convertNullString(group.groupNm)):"";
									}else{
										strHtml += "</br>";
										strHtml += (group.parentGroupNm)?(convertNullString(group.parentGroupNm)):"";
										strHtml += (group.groupNm)?("/"+convertNullString(group.groupNm)):"";
									}
										 
								}
							}
							strHtml +="</td>";
							strHtml += "<td>" + convertNullString(obj.vehicleModel.modelMaster) + "</td>";
							strHtml += "<td>" + obj.plateNum + "</td>";
							strHtml +="<td>";
							if(obj.device) strHtml += obj.device.deviceSeries;
							strHtml +="</td>";
																
							strHtml +="<td>";
							if(obj.device) strHtml += obj.device.deviceSn;
							strHtml +="</td>";
							
							strHtml += "<td>" + convertDateUint(new Date(obj.regDate),a.unitDate,a.timezoneOffset,0) + "</td>";
							strHtml += "</tr>";
									
						}
						return strHtml;
					}
				}
			}else if(this.settings.type == "group"){
				if(this.settings.selType == "checkbox"){
					return function(data){
						var strHtml = "";
						for(i in data.result){
							var obj = data.result[i];
							strHtml +="<tr>"		;							
							strHtml +="<td><input type='checkbox' class='commonGroupPopChk' data-info='"+jsonObjToBase64(obj)+"'/></td>";
							strHtml +="<td>" + convertNullString(obj.parentGroupNm) + "</td>";
							strHtml +="<td>" + convertNullString(obj.groupNm) + "</td>";
							if(obj.manager) strHtml +="<td>" + convertNullString(obj.manager.name) + "</td>";
							else strHtml +="<td></td>";
							strHtml +="<td>" + convertNullString(obj.userCount)+"</td>"		;							
							strHtml +="<td>" + convertNullString(obj.vehicleCount)+"</td>";									
							strHtml +="<td>" + convertDateUint(new Date(obj.regDate),a.unitDate,a.timezoneOffset,0) + "</td>";
							strHtml +="</tr>";
						}
						return strHtml;
					}
				}else if(this.settings.selType == "radio"){
					return function(data){
						var strHtml = "";
						for(i in data.result){
							var obj = data.result[i];
							
							strHtml += "<tr>"									
							strHtml	+= "<td><input type='radio' name='commonGroupPopRadio' data-info='"+jsonObjToBase64(obj)+"'/></td>";
							strHtml	+= "<td>" + convertNullString(obj.parentGroupNm) + "</td>";
							strHtml	+= "<td>" + convertNullString(obj.groupNm) + "</td>";
									if(obj.manager) strHtml +="<td>" + convertNullString(obj.manager.name) + "</td>";
									else strHtml +="<td></td>";
							strHtml	+= "<td>" + convertNullString(obj.userCount)+"</td>";									
							strHtml	+= "<td>" + convertNullString(obj.vehicleCount)+"</td>";									
							strHtml	+= "<td>" + convertDateUint(new Date(obj.regDate),a.unitDate,a.timezoneOffset,0) + "</td>";
							strHtml	+= "</tr>";
									
						}
						return strHtml;
					}
				}
			}
			
			
		},
		resultCnt : function(){
			var targetCounter = this.settings.targetCounter;  
			return function(cnt){
				$(targetCounter).html(cnt);
			};
		}
		
		
		//,_openCommonPop
		
	});
	
	
	
	$.fn.commonPop = function(options){
		
		var otherArgs = Array.prototype.slice.call( arguments, 1 );
		
		/*
		 * 필요할까?
		if ( typeof options === "string" && ( options === "goPage" || options === "setParam" || options === "search" || options === "setLimit") ) {
			return $.commonPop[ "_" + options+"CommonPop"].apply( $.CommonPop, [ this[ 0 ] ].concat( otherArgs ) );
		}
		
		if ( options === "option" && arguments.length === 2) {
			return $.commonPop[ "_" + options+"CommonPop"].apply( $.CommonPop, [ this[ 0 ] ].concat( otherArgs ) );
		}
		*/
		return this.each( function(i) {
			typeof options === "string" ?
				$.commonPop[ "_" + options+"CommonPop"].apply( $.CommonPop, [ this[ 0 ] ].concat( otherArgs ) ) :
				$.commonPop.attachCommonPop( this, options );	
				
		} );
	}
	
	$.commonPop = new CommonPop();
	
	
	function CommonLazy(idx){
		this.settings = {
			url : "" //request url
			,param : {} //request param
			,http_post_option : {}
			,thead : []
			,bodyHtml : function(data){return "<tr></tr>";}
				
			/*이하 option change 해도 아무기능없음*/
			,limit : 5
			,currentPage : 1
			,pagePerGroup : 10
			,initSearch : true
			,loadingBodyViewer : true
			,debugMode : false
		};
		this.targetObj;
		this.pageObj;
		this.noContents = "";
		this.totalCnt = 0;
		this.pager = {
			currentPage : this.settings.currentPage
			,limit : this.settings.limit
			,pagePerGroup : this.settings.pagePerGroup
		};
		this.pageNavi;
		this.initNoContentsViewer = true;
		this.commonLazyIdx = idx;
		this.eventSwitch = false;
		this.loading = false;
		this.isFirst = true;
	}
	
	$.extend(CommonLazy.prototype,{
		attachCommonLazy : function(target ,settings){
			$.extend( this.settings, settings);
			$.extend( this.pager, {limit:settings.limit,pagePerGroup:settings.pagePerGroup});
			
			
			this.targetObj = target;
			this._initCommonLazy();
		},
		_initCommonLazy : function(){
			
			$(this.targetObj).data("commonLazy","commonLazy"+this.commonLazyIdx);
			this._clearCommonLazy();
			this.createHeaderCommonLazy();
			this.pageNavi = new PageNavigator(1,this.pager.limit,this.pager.pagePerGroup);
			if(this.settings.initSearch) this.loadCommonLazy();
			else this.pageNavi.setTotalSize(0);
			
			this.scrollEvent();
			
		},
		_clearCommonLazy : function(){
			$(this.targetObj).html("");
		},
		createHeaderCommonLazy : function(){
			var head = this.settings.thead;
			
			var htmlHead = $("<thead><tr></tr></thead>");
			
			$(this.targetObj).append(htmlHead);
			
			var htmlTh = $("<th scope='col'></th>");
			
			var strHtmlTh = "";
			for(key in head){
				
				var inner = head[key];
				var th = htmlTh;
					th.html(inner);
				strHtmlTh += th.prop("outerHTML");
					
			}
				
			$(this.targetObj).find("thead tr").append(strHtmlTh);
			$(this.targetObj).append("<tbody></tbody>");
			this.noContents = "<tr class='trNoContents' ><td colspan="+head.length+" style='width:100%'>데이터가 없습니다.</td></tr>";
			this.lodingContents = "<tr class='trLoading'><td colspan="+head.length+" style='width:100%'>데이터로딩중!!!</td></tr>";
			$(this.targetObj).find("tbody").html(this.lodingContents);
		},
		loadCommonLazy : function(){
			var _this = this;
			var p = $.extend({} , this.settings.param);
			
			p.limit = this.pageNavi.getRowPerPage();
			p.offset = this.pageNavi.getStartRow()-1;
			if(p.offset == 0) p.counting = true;
			if(p.offset != 0) p.counting = false;
			
			$.extend(this.settings.http_post_option,{success : function(rtv){
				_this.settings.debugMode&&console.log(rtv);
				_this.data = rtv;
				if(rtv.totalCnt) {
					_this.pageNavi.setTotalSize(rtv.totalCnt);
					_this.settings.resultCnt&&_this.settings.resultCnt(rtv.totalCnt);
				}
				_this.drawBody();
			}});
			
			
			if(this.settings.loadingBodyViewer){
				this.settings.http_post_option.commonListTarget = this;
				this.settings.http_post_option.beforeCommonListLoad = function(t){
					t.loading=true;
					$(t.targetObj).find("tbody .trLoading").show();
				};				
			}
			
			a.http_post(this.settings.url,p,this.settings.http_post_option);
			
			
			
		},
		drawBody : function(){
			
			$(this.targetObj).find(".trLoading").hide();
					
			if(this.pageNavi.getTotalSize() == 0) $(this.targetObj).find(".trLoading").before(this.noContents);
			else{				
				var strHtml = this.settings.bodyHtml(this.data);
				$(this.targetObj).find(".trLoading").before(strHtml);
			}
			
			if(this.pageNavi.getTotalSize() <= $(this.targetObj).find("tbody tr").length-1)
				 this.eventSwitch = false;
			else this.eventSwitch = true;
						
			this.loading = false;
			this.checkboxAllEvent();
			
		},
		scrollEvent : function(){
			var _this = this;
			$(this.targetObj).find("tbody").on("scroll",function() {
				var elem = $(_this.targetObj).find("tbody");
				if ( elem[0].scrollHeight - elem.scrollTop() - elem.outerHeight() < 100)
			    {
					if(!_this.loading) _this.moveNext();
			    }
			});
		},
		moveNext : function(){
			var p = this.pageNavi.getCurrentPage();
			this.move(p+1);
		},
		move : function(p){
			this.pageNavi.setCurrentPage(p);
			if(this.eventSwitch) this.loadCommonLazy();
			else{
				if(!$(this.targetObj).find("tbody tr.trNoContents").length) $(this.targetObj).find(".trLoading").before(this.noContents);
			}
		},
		checkboxAllEvent : function(){
			var targetCheck = $(this.targetObj).find("thead input:checkbox");
			var targetObjBody = $(this.targetObj).find("tbody input:checkbox");
			if(targetCheck.length > 0){
				
				var eventHander = true;
				var e = $._data( $(targetCheck).get(0), 'events' );
				$.each( e , function(i,o){
					if(i == "click") eventHander = false;
				});
				if(eventHander) {
					$(targetCheck).on("click",function(e){
						if($(this).is(":checked")){
							targetObjBody.prop("checked",true);
						}else{
							targetObjBody.prop("checked",false);
						}
					});
					
				}
			}
		}
	});
	
	$.fn.commonLazy = function(options){
		var otherArgs = Array.prototype.slice.call( arguments, 1 );
		
		
		if ( typeof options === "string" && ( options === "goPage" || options === "setParam" || options === "search" || options === "setLimit") ) {
			return $.commonLazy[ "_" + options+"CommonLazy"].apply( $.CommonLazy, [ this[ 0 ] ].concat( otherArgs ) );
		}
		
		if ( options === "option" && arguments.length === 2) {
			return $.commonLazy[ "_" + options+"CommonLazy"].apply( $.CommonLazy, [ this[ 0 ] ].concat( otherArgs ) );
		}
		return this.each( function() {
			typeof options === "string" ?
				$.commonLazy[ "_" + options+"CommonLazy"].apply( $.CommonLazy, [ this[ 0 ] ].concat( otherArgs ) ) :
				$.commonLazy.attachCommonLazy( this, options );
		} );
	}
	
	$.commonLazy = new CommonLazy(1);
	
	
})($V4);

function goPage(targetObjId,pageNum){
	var listId = $("#"+targetObjId).data("commonList");
	
	if(!listId || listId == 'commonList1') {
		$("#"+targetObjId).commonList("goPage",pageNum);
	}else {
		var strFunc = "$('#"+targetObjId+"')."+listId+"('goPage',"+pageNum+");";
		eval(strFunc);
	}
	
	
}





