// #gnb (상단메뉴 슬라이딩)
function chk() {
    if (cc == 1) {
        $("#gnb ul li ul").slideDown();
        $(".bg-gnb").slideDown();
    } else {
        $("#gnb ul li ul").slideUp(200);
        $(".bg-gnb").slideUp(200);
    }
}

$(function() {
    $('#gnb>ul>li').mouseover(function() {
        setTimeout(chk, 100);
        cc = 1;
        $(this).addClass('active');
    });

    $('#gnb').mouseout(function() {
        setTimeout(chk, 100);
        cc = 0;
        $('#gnb ul li').removeClass('active');
    });

    $('#gnb ul li a').focus(function() {
        setTimeout(chk, 100);
        cc = 1;
        $(this).parents('li.depth1').addClass('active');
    });

    $('#gnb ul li a').blur(function() {
        setTimeout(chk, 100);
        cc = 0;
        $('#gnb ul li').removeClass('active');
    });
});

// #quick-menu (우측 사이드 바)
$(function() {
    var quickulH = $('#quick-menu>.inner>ul').height();

    // 기본 퀵바 높이 설정
    $('#quick-menu .inner').css("min-height", $('.cont').height() + 153);

    // 퀵바 세로 가운데 정렬
    if (quickulH < $(window).height()) {
        $("#quick-menu>.inner>ul>li:first-child").css("margin-top", $(window).height() / 2 - (quickulH / 2) - 30);
    }

    $('#quick-menu>.inner>ul>li>a').each(function() {
        $(this).mouseover(function() {
            $('#quick-menu').addClass('over');
        });
        $(this).mouseleave(function() {
            $('#quick-menu').removeClass('over');
        });
	});
	
    // 메뉴 버튼 엑션
    $('#quick-menu>.inner>ul>li.depth1>a').each(function() {
        $(this).click(function(e) {
            $('#quick-menu li.depth1').removeClass('active');
            $(this).parent().addClass('active');
            $('body').addClass('open');
            $('#quick-menu').css({
                'width': '340px',
                'z-index': '50'
            });
            // 퀵메뉴 높이 조절
            if ($(this).siblings('.cont').height() + 153 > quickulH) {
                $('#quick-menu .inner').css("min-height", $(this).siblings('.cont').height() + 153);
            } else {
                $('#quick-menu .inner').css("min-height", quickulH);
            }
        });
    });

    // 닫기 버튼
    $('.btn-closed').click(function() {
        $('#quick-menu li.depth1').removeClass('active');
        $('body').removeClass('open');
        setTimeout(function() {
            $('#quick-menu').css({
                'width': '61px',
                'z-index': '20'
            });
        }, 300);
    });
    
   $('.bg-modal').click(function(){
    	$('#quick-menu li.depth1').removeClass('active');
        $('body').removeClass('open');
        setTimeout(function() {
            $('#quick-menu').css({
                'width': '61px',
                'z-index': '20'
            });
        }, 300);
    });
});

// 상단으로 이동하기 버튼
$(function() {
    $('#page-up').click(function() {
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
});

// table th 정렬
$(function() {
    $('.table th.order').click(function() {
        $(this).toggleClass('active');
    });
});

// 로그인 팝업
$(function() {
    $("#login-dialog").dialog({
        autoOpen: false,
        show: {
            duration: 500
        },
        width: '530',
        height: '580',
        modal: true
    });

    $("#login-open").on("click", function() {
        $("#login-dialog").dialog("open");
    });

    $(".login-tab>li>a").click(function() {
        $(this).parent().addClass("on").siblings().removeClass("on");
        return false;
    });
});