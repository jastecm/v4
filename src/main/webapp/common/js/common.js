
$(function(){
	$(".menu_btn strong").mouseover(function() {
		$(".menu_btn strong").removeClass("on");
		$(this).addClass("on");
		$(".menu_detail").slideDown();
		$(".menu_detail li").removeClass("on");
		$(this).parent().parent().find(".menu_detail li").eq($(this).index()).addClass("on");
	});

	$(".menu_detail li").mouseover(function() {
		$(".menu_detail li").removeClass("on");
		$(this).addClass("on");
		$(".menu_btn strong").removeClass("on");
		$(this).parent().parent().find(".menu_btn strong").eq($(this).index()).addClass("on");
	});

	$(".lnb .layout").mouseleave(function() {
		$(".menu_detail").slideUp();
	});
	
	$(".statu_2 span").mouseover(function() {
		$(this).parent().find("strong").show();
	});
	$(".statu_2 span").mouseleave(function() {
		$(this).parent().find("strong").hide();
	});

	$(".btn_cal1").click(function() {
		$("#div3_example").fadeIn();
		$("#div4_example").fadeOut();
	});

	$(".btn_cal5").click(function() {
		$("#div5_example").fadeIn();
	});

	$(".btn_cal3").click(function() {
		$("#div1_example").fadeIn();
	});

	$(".btn_cal4").click(function() {
		$("#div2_example").fadeIn();
	});


	$(".btn_cal2").click(function() {
		$("#div4_example").fadeIn();
		$("#div3_example").fadeOut();
	});

	$(".btn_close").click(function() {
		$(".pop1").fadeOut();
		$(".pop2").fadeOut();
		$(".pop3").fadeOut();
		$(".pop4").fadeOut();
		$(".pop6").fadeOut();
		$(".pop7").fadeOut();
		$(".pop8").fadeOut();
		$(".pop9").fadeOut();
		$(".pop10").fadeOut();
		$(".user_up_ppp").fadeOut();
		$(".user_up_pop").fadeOut();
		$(".black").fadeOut();

		
	});


	$(".btn_view_map").click(function() {
		$(".pop9").fadeIn();
		$(".black").fadeIn();
	});
	
	$(".btn_user_up").click(function() {
		$(".user_up_pop").fadeIn();
		$(".black").fadeIn();
	});
	
	$(".btn_drive_list").click(function() {
		$(".pop6").fadeIn();
		$(".black").fadeIn();
	});


	$(".btn_help").click(function() {
		$(".pop10").fadeIn();
		$(".black").fadeIn();
	});
	
	$(".btn_other").click(function() {
		$(".pop1").fadeIn();
		$(".black").fadeIn();
	});

	$(".btn_cost").click(function() {
		$(".pop4").fadeIn();
		$(".black").fadeIn();
	});
	
	$(".btn_cost_save").click(function() {
		$(".pop4").fadeOut();
		$(".pop5").fadeIn();
	});

	$(".btn_payment_car").click(function() {
		$(".car_select_pop2").fadeIn();
		$(".black").fadeIn();
	});

	$(".btn_cancle").click(function() {
		$(".pop5").fadeOut();
		$(".pop6").fadeOut();
		$(".pop3").fadeOut();
		$(".black").fadeOut();
		$(".user_up_pop").fadeOut();
		$(".car_select_pop").fadeOut();
		$(".car_select_pop2").fadeOut();
	});

	$(".btn_pic_up").click(function() {
		$(".user_up_ppp").fadeIn();
		$(".black").fadeIn();
	});

	

	$(".btn_cancel").click(function() {
		$(".pop7").fadeOut();
		$(".pop8").fadeOut();
		$(".user_up_ppp").fadeOut();
		$(".black").fadeOut();
	});


	$(".btn_ok").click(function() {
		$(".pop5").fadeOut();
		$(".pop10").fadeOut();
		$(".black").fadeOut();
	});
	
	

	$(".btn_return").click(function() {
		$(".pop3").fadeIn();
		$(".black").fadeIn();
	});

	

	$(".btn_close3").click(function() {
		$("#div1_example").fadeOut();
	});

	$(".btn_close4").click(function() {
		$("#div2_example").fadeOut();
	});

	$(".btn_close5").click(function() {
		$("#div5_example").fadeOut();
	});

	$(".btn_close1").click(function() {
		$("#div3_example").fadeOut();
	});
	$(".btn_close2").click(function() {
		$("#div4_example").fadeOut();
	});

	$(".btn_cal").click(function() {
		$(this).parent().find(".cal_layout").fadeIn();
	});

	$(".btn_enter").click(function() {
		//$(this).parent().parent().fadeOut();
		$(this).parent().fadeOut();
	});


	$(".btn_car1").click(function() {
		$(".car_txt1").toggleClass("on");
	});
	
	$(".btn_detail").click(function() {
		$(".pop2").fadeIn();
		$(".black").fadeIn();
	});

	$(".checkboxes").click(function() {
		if ($(this).attr('checked')) {
			$(this).parent().addClass("on");
		} else {
			$(this).parent().removeClass("on");
		}
	});

	$(".chk_all").click(function() {
		if($(this).hasClass("on")){
			$(this).removeClass("on");
			$(".check_box_1").removeClass("on");
		} else {
			$(this).addClass("on");
			$(".check_box_1").addClass("on");
		}
	});

	$(".check_box_1").click(function() {
		if($(this).hasClass("on")){
			$(this).addClass("on");
		} else {
			$(this).removeClass("on");
		}
	});

	$(".btn_add").click(function() {
		$(".repair_box2_2").append('<div class="repair_box2_2_3"><div><span class="tit">정비항목</span><input type="text" class="input1" /><a href="#none" class="btn_delete"><img src="images/btn_delete.jpg" alt="" /></a></div><div class="r3"><span class="r3_1">부품등급</span><div class="select_type5"><div><select><option value="1">A</option><option value="2">B</option></select></div></div><span class="r3_2">수량</span><div class="select_type5"><div><select><option value="1">1</option><option value="2">2</option></select></div></div><span class="r3_3">부품금액</span><input type="text" class="input2" /><span class="r3_4">원</span><span class="r3_5">공임</span><input type="text" class="input2" /><span class="r3_4">원</span></div></div><script>$(".btn_delete").click(function() {$(this).parent().parent().remove();});</script>');
	});

	$(".btn_delete").click(function() {
		$(this).parent().parent().remove();
	});

	$(".graph_viewcar").mouseover(function() {
		$(this).append('<div class="risk_graph_over"><div class="risk_graph_over1"><h1>급출발</h1><h2>46%</h2><p>위험횟수 : 418회<br>총운행횟수 : 4,268회</p></div></div>');
	});

	$(".graph_viewcar").mouseleave(function() {
		$(".risk_graph_over").hide();
	});

	$(".graph_user").mouseover(function() {
		$(this).append('<div class="risk_graph_over"><div class="risk_graph_over2"><h1>급출발</h1><h2>46%</h2><p>위험횟수 : 418회<br>총운행횟수 : 4,268회</p></div></div>');
	});

	$(".graph_user").mouseleave(function() {
		$(".risk_graph_over").hide();
	});
	
	$(".tab_menu3 a").click(function() {
		$(this).parent().find("a").removeClass("on");
		$(this).addClass("on");
		$(this).parent().parent().find(".car_choice_area .car_choice_area_1").hide();
		$(this).parent().parent().find(".car_choice_area .car_choice_area_1").eq($(this).index()).show();
	});

	$(".btn_car_search").click(function() {
		$(".black").fadeIn();
		$(".car_select_pop").fadeIn();		
	});

	$(".btn_drive_list").click(function() {
		
	});

	$(".status_area1 div strong").click(function() {
		$(".status_area1 div strong").removeClass("on");
		$(this).addClass("on");
	});

	$(".a_btn6").click(function() {
		$(".black").fadeIn();
		$(".pop8").fadeIn();
	});
/*
	$(".btn_x").on("click",function(){
		window.history.back();
	});
	
	$(".btn_x1").on("click",function(){
		window.history.back();
		
	});
*/
});


$(window).load(function(e) {
	
});

$(window).resize(function(e) {
	
});


$(window).scroll(function(e) {
	
});


jQuery(function($){  
    $.datepicker.regional['co-KR'] = {  
        closeText: '닫기',  
        prevText: '<지난달',  
        nextText: '다음달>',  
        currentText: '오늘',  
        monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],  
        monthNamesShort: ['1','2','3','4','5','6','7','8','9','10','11','12'],  
        dayNames: ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'],
        dayNamesShort: ['일요일','월요일','화요일','수요일','목요일','금요일','토요일'],  
        dayNamesMin: ['일','월','화','수','목','금','토'],  
        weekHeader: 'Week',
        dateFormat: 'yy-mm-dd',  
        firstDay: 1,  
        isRTL: false,  
        showMonthAfterYear: true,  
        yearSuffix: '년'};  
    $.datepicker.setDefaults($.datepicker.regional['co-KR']);  
});





