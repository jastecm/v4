function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
function isJsonObject( obj )
{
    var has_keys = 0 ;
    for( var pr in obj )
    {
        if ( obj.hasOwnProperty( pr ) && !( /^\d+$/.test( pr ) ) )
        {
           has_keys = 1 ;
           break ;
        }
    }

    return ( has_keys && obj.constructor == Object && obj.constructor != Array ) ? 1 : 0 ;
}

function LPAD(s, c, n) {    
    if (! s || ! c || s.length >= n) {
        return s;
    }
 
    var max = (n - s.length)/c.length;
    for (var i = 0; i < max; i++) {
        s = c + s;
    }
 
    return s;
}
 
function RPAD(s, c, n) {  
    if (! s || ! c || s.length >= n) {
        return s;
    }
 
    var max = (n - s.length)/c.length;
    for (var i = 0; i < max; i++) {
        s += c;
    }
 
    return s;
}

function replaceAll(str, searchStr, replaceStr) {
    return str.split(searchStr).join(replaceStr);
}
function replaceNumber(str) {
    return replaceAll(str,/[^0-9]/g,'');
}
/*
 * d date 객체 or 숫자8자리 or 문자열10자리(yyyy/mm/dd , yyyy-mm-dd)
 * ex) d = new Date(d); 
 * ex) d = new Number(i) 
 * ex) d = new String('string')
 * 
 * calcType - d:일 , m:월
 * v - 증감일
 * 
 * returnType - instance Date,Number,String(구분자)[String("/") = yyyy/mm/dd]
 */
function calcDate(d , calcType , v , returnType){
	var c;
	
	if(d instanceof Date){
		c = d;
	}else if(d instanceof Number){
		d = new String(d);
		c = new Date(d.substring(0,4),Number(d.substring(4,6))-1,Number(d.substring(6,8)));
	}else if(d instanceof String){
		var d = replaceNumber(d);
		c = new Date(d.substring(0,4),Number(d.substring(4,6))-1,Number(d.substring(6,8)));
	}
	
	if(calcType == "d") c.setDate(c.getDate()+v);
	if(calcType == "m") c.setMonth(c.getMonth()+v);
	if(calcType == "mi") c.setMinutes(c.getMinutes()+v);
	
	if(returnType instanceof Date){
		return c;
	}else if(returnType instanceof Number){
		var y = c.getFullYear();
		var m = LPAD(Number(c.getMonth()+1)+"",'0',2);
		var d = LPAD(Number(c.getDate())+"",'0',2);
		return new Number(y+""+m+""+d);
	}else if(returnType instanceof String){
		var sep = returnType;
		var y = c.getFullYear();
		var m = LPAD(Number(c.getMonth()+1)+"",'0',2);
		var d = LPAD(Number(c.getDate())+"",'0',2);
		return new String(y+sep+m+sep+d);
	}
	
}

function convertDateUint(date,unit,timezone,appendTime){
	if(!date) return "";
	
	var localTimezone = date.getTimezoneOffset();
	var convertTimezone = parseInt(timezone);
	var intervalTimeOffset = localTimezone+convertTimezone; 
		
	date = calcDate(date,"mi",intervalTimeOffset,new Date());
	
	var y = date.getFullYear();
	var m = LPAD(Number(date.getMonth()+1)+"",'0',2);
	var d = LPAD(Number(date.getDate())+"",'0',2);
	var h = LPAD(Number(date.getHours())+"",'0',2);
	var mi = LPAD(Number(date.getMinutes())+"",'0',2);
	var se = LPAD(Number(date.getSeconds())+"",'0',2);
	if(unit == '%Y/%m/%d'){
		var rtv = y+"/"+m+"/"+d;
	}
	if(unit == '%m/%d/%Y'){
		var rtv = m+"/"+d+"/"+y;
	}
	if(unit == '%d/%m/%Y'){
		var rtv = d+"/"+m+"/"+y;
	}
	
	return appendTime?rtv+" "+h+":"+mi+":"+se:rtv;
}



function jsonObjToBase64(jsonObj){
	return Base64.encode(JSON.stringify(jsonObj));
}
function base64ToJsonObj(base64){
	return JSON.parse(Base64.decode(base64));
}
 
//3자리마다 콤마를 찍어줌
function number_format(data) {
 data = new String(data);
 minusFlag = data.indexOf("-")>-1;
 if(minusFlag) data = data.replace("-","");
 exp_data = data.split('.');
 tmp = '';
 number = '';
 cutlen = 3;
 comma = ',';
 len = exp_data[0].length;
 mod = (len % cutlen);
 k = cutlen - mod;
 for (NF_i=0; NF_i<len; NF_i++) {
  number = number + data.charAt(NF_i);
  if (NF_i < len - 1) {
   k++;
   if ((k % cutlen) == 0) {
    number = number + comma;
    k = 0;
   }
  }
 }
 if (exp_data[1] != undefined) number += '.' + exp_data[1];
 
 if(minusFlag) return "-"+number;
 else return number;
}

/*function convertNullString(a){
	console.log(a);
	if(a=='null' || !a) return '';
}
*/
function secondToTime(t,lang){
	if(t&&!isNaN(t)){		
		if(lang == "ko") return parseInt(t/3600)+'시간 '+LPAD(''+parseInt((t%3600)/60),'0',2)+'분 '+LPAD(''+parseInt((t%3600)%60),'0',2)+'초';
		else return parseInt(t/3600)+'H '+LPAD(''+parseInt((t%3600)/60),'0',2)+'M '+LPAD(''+parseInt((t%3600)%60),'0',2)+'S';
	}
}

function calScore(a,lang){
	
	if(lang == "en"){
		if(a==null) return 'E(0PTS)';
		else if(a>100) return 'A(100PTS)';
		else if(a>=80) return 'A('+a+'PTS)';
		else if(a>=60) return 'B('+a+'PTS)';
		else if(a>=40) return 'C('+a+'PTS)';
		else if(a>=20) return 'D('+a+'PTS)';
		else if(a>=0) return 'E('+a+'PTS)';
		else return 'E(0PTS)';
	}else{
		if(a==null) return 'E(0점)';
		else if(a>100) return 'A(100점)';
		else if(a>=80) return 'A('+a+'점)';
		else if(a>=60) return 'B('+a+'점)';
		else if(a>=40) return 'C('+a+'점)';
		else if(a>=20) return 'D('+a+'점)';
		else if(a>=0) return 'E('+a+'점)';
		else return 'E(0점)';
	}
	
}

function calRank(a){
	if(a==null) return 'E';
	else if(a>100) return 'A';
	else if(a>=80) return 'A';
	else if(a>=60) return 'B';
	else if(a>=40) return 'C';
	else if(a>=20) return 'D';
	else if(a>=0) return 'E';
	else return 'E';
}

function minusError(a){
	return replaceAll(a,"-,","-");
}

function convertNullString(a,b){
	if(a==null || a=='null') 
		return b==undefined ? '' : b;
	else 
		return a;
}

function getBitsSystemArchitecture()
{
    var _to_check = [] ;
    if ( window.navigator.cpuClass ) _to_check.push( ( window.navigator.cpuClass + "" ).toLowerCase() ) ;
    if ( window.navigator.platform ) _to_check.push( ( window.navigator.platform + "" ).toLowerCase() ) ;
    if ( navigator.userAgent ) _to_check.push( ( navigator.userAgent + "" ).toLowerCase() ) ;

    var _64bits_signatures = [ "x86_64", "x86-64", "Win64", "x64;", "amd64", "AMD64", "WOW64", "x64_64", "ia64", "sparc64", "ppc64", "IRIX64" ] ;
    var _bits = 32, _i, _c ;
    outer_loop:
    for( var _c = 0 ; _c < _to_check.length ; _c++ )
    {
        for( _i = 0 ; _i < _64bits_signatures.length ; _i++ )
        {
            if ( _to_check[_c].indexOf( _64bits_signatures[_i].toLowerCase() ) != -1 )
            {
               _bits = 64 ;
               break outer_loop;
            }
        }
    }
    return _bits ; 
}


function is_32bits_architecture() { return getBitsSystemArchitecture() == 32 ? 1 : 0 ; }
function is_64bits_architecture() { return getBitsSystemArchitecture() == 64 ? 1 : 0 ; }

function kmToUnit(v,unit){
	if(v){
		if(unit=='mi'){
			v = v*0.621371;
			return number_format(v.toFixed(2));
		}else return number_format(v.toFixed(2));
	}else{
		return 0;
	}
	
}

function UnitToKm(v,unit){
	if(v){
		if(unit=='mi'){
			v = v*1.60934;
			return v.toFixed(2);
		}else return v;
	}else{
		return 0;
	}
	
}

function nullToString(d){
	if(d){
		return d;
	}else{
		return "";
	}
	
}

function concatString(a,b){
	return nullToString(a)+' '+nullToString(b);
}

function objNullToString(obj){
	if(obj.constructor == Object){
		for (var prop in obj) {
			obj[prop] = nullToString(obj[prop]);
		}
	}
	return obj;
}

function simpleUserNameViewer(parentGroupNm ,delimeter1 , groupNm ,delimeter2, position ,delimeter3, name){
	var strHtml = "";
	if(parentGroupNm) strHtml += parentGroupNm;
	if(parentGroupNm&&delimeter1) strHtml += delimeter1;
	if(groupNm) strHtml += groupNm; 
	if(strHtml&&delimeter2) strHtml += delimeter2;
	if(position) strHtml += position;
	if(strHtml&&delimeter3) strHtml += delimeter3;
	if(name) strHtml += name;
	return strHtml;
}

function simpleUserObjectNameViewer(obj,delimeter1,delimeter2,delimeter3){
	
	var strHtml = "";
	/*
	if(obj.group&&obj.group.parentGroupNm)
		strHtml += convertNullString(obj.group.parentGroupNm);*/
	if(strHtml&&delimeter1) 
		strHtml += delimeter1;
	if(obj.group&&obj.group.groupNm)
		strHtml += convertNullString(obj.group.groupNm);
	if(strHtml&&delimeter2) 
		strHtml += delimeter2;
	if(obj.corpPosition)
		strHtml += convertNullString(obj.corpPosition);
	if(strHtml&&delimeter3) strHtml += delimeter3;
	strHtml += obj.name;
	
	return strHtml;
}

function getProperty(obj,chain,nullString){
	var chains = chain.split(".");
	for(i in chains){
		if(obj[chains[i]]) obj = obj[chains[i]];
		else return nullString?nullString:null;
	}
	return obj;
}


Date.prototype.yyyymmdd = function() {
	  var mm = this.getMonth() + 1; // getMonth() is zero-based
	  var dd = this.getDate();
	  
	  return [this.getFullYear(),
	          (mm>9 ? '' : '0') + mm,
	          (dd>9 ? '' : '0') + dd
	         ].join('/'); 
	};
	

Date.prototype.yyyymmddTime = function() {
	  var mm = this.getMonth() + 1; // getMonth() is zero-based
	  var dd = this.getDate();
	  var yyyymmdd = [this.getFullYear(),(mm>9 ? '' : '0') + mm,(dd>9 ? '' : '0') + dd].join('/');
	  var time = [(this.getHours()>9 ? '' : '0') + this.getHours(),(this.getMinutes()>9 ? '' : '0') + this.getMinutes(),(this.getSeconds()>9 ? '' : '0') + this.getSeconds()].join(':');
	  
	  return  yyyymmdd+" "+time;
	};
	
Date.prototype.yyyymm = function() {
	  var mm = this.getMonth() + 1; // getMonth() is zero-based
	  var dd = this.getDate();
	  
	  return [this.getFullYear(),
	          (mm>9 ? '' : '0') + mm
	         ].join('-'); 
};		

Date.prototype.yyyymm2 = function() {
	  var mm = this.getMonth() + 1; // getMonth() is zero-based
	  var dd = this.getDate();
	  
	  return [this.getFullYear(),
	          (mm>9 ? '' : '0') + mm
	         ].join(''); 
};
//monthpicker plugin을 위한 함수 매번 현재 년도를 갱신해주기 귀찮타..
function getMonthPickerYear(prevYearCount){
	var arr = [];
	
	var nowYear = new Date().getFullYear();
	
	for(var i = 0 ; i < prevYearCount; i++){
		arr.push(nowYear - i)
	}
	
	return arr;
}		
	
	function generatorExcel(fileEle,customHeader,nullToZero, cb ) {

		var to_json = function(workbook,ch) {
			var parseData = [];
			if(workbook.SheetNames.length > 0){
				var sheet = workbook.Sheets[workbook.SheetNames[0]];
				if(ch.length > 0){
					parseData = XLSX.utils.sheet_to_json(sheet, {
						header : 1
						,range : 1
					});
				}else{
					parseData = XLSX.utils.sheet_to_json(sheet);
				}
			}
			var resultData = JSON.parse(JSON.stringify(parseData,2,2));
			for(var i = 0 ; i < resultData.length ; i++){
				resultData[i] = resultData[i].reduce(function(result, item, index, array) {
					if(nullToZero){
						if(item == null) result[ch[index]] = 0;
						else result[ch[index]] = item
					}else{
						result[ch[index]] = item;
					}
					  
					  return result;
				}, {})
			}
			return resultData;
		};

		var fileExtension = function(filename, opts) {
			if (!opts)
				opts = {};
			if (!filename)
				return "";
			var ext = (/[^./\\]*$/.exec(filename) || [ "" ])[0];
			return opts.preserveCase ? ext : ext.toLowerCase();
		};

		if (fileEle.length == 0) {
			cb([]);
			return;
		}
		var fileObj = fileEle[0];
		var f = fileObj.files[0];
		if(fileObj.files.length == 0){
			alert('파일을 선택해주세요');
			cb([]);
			return;
		}
		var ext = fileExtension(f.name);

		if (ext == 'xls' || ext == 'xlsx') {

		} else {
			alert('업로드 할수 없는 파일 형태 입니다.')
			cb([]);
			return;
		}
		
		var reader = new FileReader();
		reader.onload = function(e) {
			var data = e.target.result;
			
			var workbook = XLSX.read(data, {
				type: 'binary',
				dateNF : 'yyyy-mm-dd',
				cellFormula : false,
				cellHTML : false
			});

			cb(to_json(workbook,customHeader));
		};
		
		if (!FileReader.prototype.readAsBinaryString) {
	        FileReader.prototype.readAsBinaryString = function (fileData) {
	          var binary = "";
	          var reader = new FileReader();
	          reader.onload = function (e) {
	            var bytes = new Uint8Array(reader.result);
	            var length = bytes.byteLength;
	            for (var i = 0; i < length; i++) {
	              binary += String.fromCharCode(bytes[i]);
	            }
	            var workbook = XLSX.read(binary, {
					type: 'binary',
					dateNF : 'yyyy-mm-dd',
					cellFormula : false,
					cellHTML : false
				});
	            
	            cb(to_json(workbook,customHeader));

	          }
	          reader.readAsArrayBuffer(fileData);
	        }
	      }
	    reader.readAsBinaryString(f);

	}
	
	
/**
 * 
 * @param arrData array데이타 []
 * @param fileName 다운로드될 파일명
 * @param sheetName 시트명
 * @param jsonHeader 첫번째 로우 해더 {}
 */
function writeExcelFile(arrData, fileName, sheetName, jsonHeader){
	var wb = XLSX.utils.book_new();
    var sheet = undefined;
    if( jsonHeader) {
      var head = Object.keys(jsonHeader);
      for(var i = 0 ; i < arrData.length; i++){
    	  var obj = arrData[i];
    	  Object.keys(obj).forEach(function(key) {
    		    if(!head.includes(key)){
    		    	delete obj[key];
    		    }
    	  });
      }
      // header를 index 0 번에 주어 excel 로 떨굴때 나오게 함.
      arrData.splice(0, 0, jsonHeader);
      //array.splice(index, 0, jsonToPutIn);
      // skipHeader: true 를 한 이유는 위의 jsonHeader의 값을 넣을 것이므로.
      // skipHeader : false 로 놓으면, arrData내 json object 의 key 값이 header 가 됨
      sheet = XLSX.utils.json_to_sheet(arrData, {header:Object.keys(jsonHeader), skipHeader:true}  );
    }else{
      // arrHeaderOrder가 없음
      sheet = XLSX.utils.json_to_sheet(arrData);
    }
    XLSX.utils.book_append_sheet(wb, sheet, sheetName);
    XLSX.writeFile(wb, fileName );
}

if (![].includes) {
	  Array.prototype.includes = function(searchElement /*, fromIndex*/ ) {
	    'use strict';
	    var O = Object(this);
	    var len = parseInt(O.length) || 0;
	    if (len === 0) {
	      return false;
	    }
	    var n = parseInt(arguments[1]) || 0;
	    var k;
	    if (n >= 0) {
	      k = n;
	    } else {
	      k = len + n;
	      if (k < 0) {k = 0;}
	    }
	    var currentElement;
	    while (k < len) {
	      currentElement = O[k];
	      if (searchElement === currentElement ||
	         (searchElement !== searchElement && currentElement !== currentElement)) {
	        return true;
	      }
	      k++;
	    }
	    return false;
	  };
	}

function getWeekDay(date){
	var week = ['일', '월', '화', '수', '목', '금', '토'];
	
	return week[new Date(date).getDay()];
}

function getMonthPickerYear(prevYearCount){
	var arr = [];
	
	var nowYear = new Date().getFullYear();
	
	for(var i = 0 ; i < prevYearCount; i++){
		arr.push(nowYear - i)
	}
	
	return arr;
}
var checkNumber = function(_data){
	
	regNumber = /^[0-9]*$/;
	  
	var check;
	
	if(!regNumber.test(_data)) {
		check =  false;
	}else{
		check = true;
	}
	
	return check;
};

//소유,리스, 렌터, 개인
function vehicleBizTypeName(type,lang){
	
	if(type == "1" || type=="2" || type=="3" || type=="4"){
		var nmko = ["소유","리스","렌터","개인"];
		var nmen = ["aa","bb","cc","dd"];
		
		if(lang == "ko"){
			return nmko[parseInt(type) - 1];
		}else if(lang == "en"){
			return nmen[parseInt(type) - 1];
		}else{
			
		}
	}else{
		return "";
	}
	
	
}

function arraySum(arr,key){
	var sum = 0;
	for(var i = 0 ; i < arr.length ; i++){
		if(arr[i][key]){
			if(_.isNumber(arr[i][key])){
				sum = sum + arr[i][key]
			}
		}
	}
	
	return sum;
}

function avgGrade(arr){
	var sum;
	for(var i = 0 ; i < arr.length;i++){
		sum = parseInt(arr[i]["partsQuality"]);
	}
	var avg = Math.round(sum / arr.length)
	var result;
	if(avg == 0){
		result = "A";
	}else if( avg == 1){
		result = "B";
	}else if( avg == 2){
		result = "C";
	}else{
		result = "";
	}
	return result;
}
