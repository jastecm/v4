
$(function(){
	$(document).ready(function(){
		//parking location popup
		$(document).on("click",".btn_pop3View",function() {
			var lon = "";
			var lat = "";
			if($(this).data("loc").split(",").length == 2){
				lon = $(this).data("loc").split(",")[0];
				lat = $(this).data("loc").split(",")[1];
			}
			
			
			$("iframe").attr("src",_defaultPath+"/map/parking.do?lon="+lon+"&lat="+lat+"&readonly=1");
			
			$(".pop3View #returnLon").val(lon);
			$(".pop3View #returnLat").val(lat);
			$(".pop3View #returnLocation").val($(this).data("addr"));
			
			$(".pop3View").fadeIn();
			$(".black").fadeIn();
		});
		
		//parking location popup close
		$(".pop3View .btn_cancle,.pop3View .btn_close ,#btn_ok,#btn_ok2").click(function() {
			$(".pop3View").fadeOut();
			$(".pop8").fadeOut();
			$(".car_throble_pop").fadeOut();
			$(".black").fadeOut();
			
		});
		
		$(".btn_gps,.btn_gps2").click(function(){
			$(".pop2").fadeIn();
			$(".black").fadeIn();
		});
		
		$("#btn_pop7Close").on("click",function(){
			$(".pop7").fadeOut();
			$(".black").fadeOut();
		});
		
		$("#btn_pop7_2Close").on("click",function(){
			$(".pop7_2").fadeOut();
			$(".black").fadeOut();
		});
		
		$('.btn_x').on("click",function(){
			history.back(); 
		});
		
		$(".btn_return3").click(function() {
			
			$(".pop3View #returnLat").val($(this).data("loc").split(",")[0]);
			$(".pop3View #returnLon").val($(this).data("loc").split(",")[1]);
			$(".pop3View #returnLocation").val($(this).data("addr"));
			
			$(".pop3View").fadeIn();
			$(".black").fadeIn();
		});
		
		$(".btn_car_search").click(function(){			
			$(".car_select_pop").fadeIn();
			$(".black").fadeIn();
		});
		
		$(document).on("click",".btn_car_search",function(){
			$(".car_select_pop").fadeIn();
			$(".black").fadeIn();
		})
		
		$(".btn_close2").on("click",function(){
			$(".car_select_pop2").fadeOut();
			$(".black").fadeOut();
		});
	});
	
	
});


$(window).load(function(e) {
	
});

$(window).resize(function(e) {
	
});


$(window).scroll(function(e) {
	
});


