
var $VDAS = {};

(function(a){
	a.path = _defaultPath;
	a.http_post = function(u,d,o){
		
		var sync = true;
		if(o.sync) sync = false;
		var requestMethod = o.requestMethod?o.requestMethod:"POST";
		
		$.ajax({
			url:a.path+u
			,async: sync
			,dataType : 'json'
			,contentType: "application/x-www-form-urlencoded; charset=UTF-8"
			,type:requestMethod
			,data:d
			,cache:!1
			,headers : {"Cache-Control":"no-cache"}
			,success:function(t){
				null!=o.success&&o.success(t)
			}		
			,error:function(t){
				null!=o.error&&o.error(t);
				null==o.error&&t.responseJSON?alert(t.status+"\n"+t.responseJSON.result):null==o.error&&console.log(t.status+"\n"+t.statusText);
				if(o.loading) {
					o.loading.show();
					$(".loading").remove();
				}
			}
			,beforeSend: function(xhr) {
				if(o.loading) {					
					o.loading.hide();
					o.loading.each(function(){
						var w = $(this).css("width");
						var h = $(this).css("height");
						var strHtml = "<img class='loading' src='"+_defaultPath+"/common/images/loading2.gif?"+new Date().getTime()+"' style='display:inline-block;width:"+w+";height:"+h+"'/>";						
						$(this).after(strHtml);
					})
				}
				null!=o.beforeSend&&o.beforeSend(xhr)
			}
		}).done(function(){
			if(o.loading) {				
				o.loading.show();
				$(".loading").remove();				
			}
			null!=o.done&&o.done()
		});
	}
	
	a.http_get = function(u,o){
		
		var sync = true;
		if(o.sync) sync = false;
		
		$.ajax({
			url:a.path+u
			,async: sync
			,dataType : 'json'
			,contentType: "application/x-www-form-urlencoded; charset=UTF-8"
			,type:"GET"
			,cache:!1
			,success:function(t){
				null!=o.success&&o.success(t)
			}		
			,error:function(t){
				null!=o.error&&o.error(t);
				null==o.error&&t.responseJSON?alert(t.status+"\n"+t.responseJSON.result):null==o.error&&console.log(t.status+"\n"+t.statusText);
				if(o.loading) {
					o.loading.show();
					$(".loading").remove();
				}
			}
			,beforeSend: function() {
				if(o.loading) {					
					o.loading.hide();
					o.loading.each(function(){
						var w = $(this).css("width");
						var h = $(this).css("height");
						var strHtml = "<img class='loading' src='"+_defaultPath+"/common/images/loading2.gif' style='display:inline-block;width:"+w+";height:"+h+"'/>";
						$(this).after(strHtml);
					})
				}
				null!=o.beforeSend&&o.beforeSend()
			}
		});
	}
	
	a.submit_post = function(u,f){
		f.attr("action",a.path+u);
		f.attr("method","POST");
		f.attr("accept-charset","UTF-8");
		f.submit();	
	}
	
	
	
	a.instance_post = function(u,p){
		$("body").append("<form id='instanceFrm'></form>");
		for (key in p) {
			var strHtml = '<input type="text" name="'+key+'" value="'+p[key]+'" >';
			$("#instanceFrm").append(strHtml);
		}
		$("#instanceFrm").attr("action",a.path+u);
		$("#instanceFrm").attr("method","POST");
		$("#instanceFrm").attr("accept-charset","UTF-8");
		$("#instanceFrm").submit();
		$("#instanceFrm").remove();
	}
	
	a.ajaxCreateApiSelect = function(url,param,target,listType,defaultVal,initVal,lang,trigger,afterFunk,errorFunk){
		if(typeof afterFunk == "undefined"){
			afterFunk = function(){};
		}
		$.ajax({
			type : 'POST',
			dataType : 'json',
			url : a.path+url,
			contentType: "application/x-www-form-urlencoded; charset=UTF-8",
			data : param,
			success: function(result){
				console.log(result.rtvList);
				
				if(listType) a.createSingleSelect(target,result.result,defaultVal,initVal,lang,trigger,afterFunk,errorFunk);
				else a.createCodeValueSelect(target,result.result,defaultVal,initVal,lang,trigger,afterFunk,errorFunk);
			},
			error:function(result){
				errorFunk&&errorFunk(result);
			}
		});
	}
	
	a.ajaxCreateSelect = function(url,param,target,listType,defaultVal,initVal,lang,trigger,afterFunk){
		if(typeof afterFunk == "undefined"){
			afterFunk = function(){};
		}
		$.ajax({
			type : 'POST',
			dataType : 'json',
			url : a.path+url,
			contentType: "application/x-www-form-urlencoded; charset=UTF-8",
			data : param,
			success: function(result){
				console.log(result.rtvList);
				
				if(listType) a.createSingleSelect(target,result.rtvList,defaultVal,initVal,lang,trigger,afterFunk);
				else a.createCodeValueSelect(target,result.rtvList,defaultVal,initVal,lang,trigger,afterFunk);
			},
			error:function(result){
			}
		});
	}
	
	a.createSingleSelect = function(target,valList,defaultVal,initVal,lang,trigger,afterFunk){
		target.html("");
		
		var strHtml = "";
		
		if(defaultVal){
			if(lang == 'ko'){
				if(defaultVal == "A"){
					strHtml += "<option value=''>전체</option>";				
				}else if(defaultVal == "S"){
					strHtml += "<option value=''>선택</option>";				
				}else
					strHtml += "<option value=''>"+defaultVal+"</option>";
			}else{
				if(defaultVal == "A"){
					strHtml += "<option value=''>All</option>";				
				}else if(defaultVal == "S"){
					strHtml += "<option value=''>Select</option>";				
				}else
					strHtml += "<option value=''>"+defaultVal+"</option>";
			}
		}
		
		for(var i = 0 ; i < valList.length ; i++){
			if(initVal == valList[i]) strHtml += "<option value='"+valList[i]+"' selected>"+valList[i]+"</option>";
			else strHtml += "<option value='"+valList[i]+"'>"+valList[i]+"</option>";
		}
		
		target.html(strHtml);
		
		null!=afterFunk&&afterFunk();
		null!=trigger&&target.trigger("change");
		
	}

	a.createCodeValueSelect = function(target,valList,defaultVal,initVal,lang,trigger,afterFunk){
		target.html("");
		
		var strHtml = "";
		if(defaultVal){
			if(lang == 'ko'){
				if(defaultVal == "A"){
					strHtml += "<option value=''>전체</option>";				
				}else if(defaultVal == "S"){
					strHtml += "<option value=''>선택</option>";				
				}else
					strHtml += "<option value=''>"+defaultVal+"</option>";
			}else{
				if(defaultVal == "A"){
					strHtml += "<option value=''>All</option>";				
				}else if(defaultVal == "S"){
					strHtml += "<option value=''>Select</option>";				
				}else
					strHtml += "<option value=''>"+defaultVal+"</option>";
			}
		}
		for(var i = 0 ; i < valList.length ; i++){
			if(initVal == valList[i].code) strHtml += "<option value='"+valList[i].code+"' selected>"+valList[i].codeName+"</option>";
			else strHtml += "<option value='"+valList[i].code+"'>"+valList[i].codeName+"</option>";
		}
		
		target.html(strHtml);
		
		null!=afterFunk&&afterFunk();
		null!=trigger&&target.trigger("change");
		
	}
})($VDAS);


(function($){
	
	$.lazyLoader = {
		clear : function(lazyLoaderObj){
			var action = lazyLoaderObj.data('lazyLoaderAction');
			action.clear();
		},setSearch : function(lazyLoaderObj,setsearchFunc){
			var action = lazyLoaderObj.data('lazyLoaderAction');
			action.setSearch(setsearchFunc);
		},search : function(lazyLoaderObj){
			var action = lazyLoaderObj.data('lazyLoaderAction');
			action.search();
		},resize : function(lazyLoaderObj){
			var action = lazyLoaderObj.data('lazyLoaderAction');
			action.resize();
		}
		
	}
	
	$.fn.lazyLoader = function(options){
		
		return this.each(function(){
			var settings = $.extend({
				lazyLoader : false
				,searchFrmObj : null // $formObj
				,searchFrmVal : {} //{key : $inputObj,selectObj,radio,checkBox...}
				,headLength : $(this).find('thead tr:first').children().length
				,path : $VDAS.path
				,table : $(this)
				,tbody : $(this).find('tbody')
				,thead : $(this).find('thead')
				,offset : 0
				,limit : 5
				,setSearchFunc : null
				,lastRow : null
				,scrollRow : 4
				,rowHeight : 80
				,createDataHtml : function(r){}
				,loadUrl : null
				,$initOffset    : $("<input type='hidden' name='offset' value='0' />")
				,$initLimit    : $("<input type='hidden' name='limit' value='5' />")
				,$resultNone :  ""
				,initSearch : false
		    }, options );
			
			$(this).data('lazyLoaderOpts',settings);
			
			var createLoadingRow = function(){
				var strHtml ='<tr class="lazyLoader_loading" style="display:none"><td colspan="'+settings.headLength+'"><img src="'+settings.path+'/common/images/loading.gif" style="height:50px;"></td></tr>'
				settings.tbody.append(strHtml);
				settings.lastRow = settings.table.find('.lazyLoader_loading');
				
				if(settings.searchFrmObj.find("input[name=offset]").length == 0){
					settings.searchFrmObj.append(settings.$initOffset);
				}
				settings.searchFrmVal.offset = settings.$initOffset;
				if(settings.searchFrmObj.find("input[name=limit]").length == 0){
					settings.searchFrmObj.append(settings.$initLimit);
				}
				settings.searchFrmVal.limit = settings.searchFrmObj.find("[name=limit]");
				settings.searchFrmVal.offset = settings.searchFrmObj.find("[name=offset]");
			}
			
			var createInitRow = function(){
				var strHtml = '';
				if(_lang == 'ko'){
					strHtml = '<tr><td colspan="'+settings.headLength+'">검색결과가 없습니다.</td></tr>';
				}else{
					strHtml = '<tr><td colspan="'+settings.headLength+'">No result</td></tr>';
				}
				
				settings.$resultNone = strHtml;
				settings.lastRow.before(strHtml);	
				
			}
			
			var resize = function(){
				var rowLength = settings.tbody.find('tr').length-1;
				if(rowLength < settings.scrollRow){
					settings.tbody.css('display','');
					settings.tbody.css('height',(rowLength*settings.rowHeight)+"px");
					settings.tbody.css('overflow-y','none');
					settings.tbody.css('overflow-x','none');
					settings.tbody.css('width','100%');
				}else{
					settings.tbody.css('display','block');
					settings.tbody.css('height',settings.scrollRow*settings.rowHeight+"px");
					settings.tbody.css('overflow-y','auto');
					settings.tbody.css('overflow-x','hidden');
					settings.tbody.css('width',settings.thead.css("width"));
					settings.tbody.find("tr:first").find("td,th").each(function(i){
						
						var headWidth = parseInt(settings.table.find("thead tr:first th").eq(i).css("width"));
						if(settings.tbody.find("tr:first").find("td,th").length-1 == i) headWidth = headWidth-17;
						$(this).css('width',headWidth+"px");
					});
					
					settings.lastRow.find("td").css("width",settings.thead.find("tr:first").css("width"));						
				} 
				return this;
			}
			
			createLoadingRow();
			if(settings.tbody.find('tr').length == 1)
				createInitRow();
			
			//resize();
			
			var eventSwitch = function(b){
				if(b){
					settings.tbody.on("scroll",function() {
						var elem = settings.tbody;
						if ( elem[0].scrollHeight - elem.scrollTop() - elem.outerHeight() < 100)
					    {
							if(!lazyLoader) {
								
								var attr = settings.lastRow.prev().attr('last');

								if (typeof attr === typeof undefined || attr === false) {
									settings.searchFrmVal.offset.val(settings.tbody.find("tr").length-1);
									lazyLoaderAction.load();
								}
								console.log(settings.searchFrmVal);
							}
					    }
					});	
				}else{
					settings.tbody.off('scroll');
				}
					
			}
			eventSwitch(1);
			
			var lazyLoaderAction = {
				clear : function(){
					settings.tbody.find('tr:not(:last-child)').remove();
					settings.searchFrmVal.offset.val(0);
					settings.searchFrmVal.limit.val(settings.limit);
					eventSwitch(0);
					eventSwitch(1);
				},
				setSearch : function(setSearchFunc){
					for (var key in settings.searchFrmVal){
				        var attrName = key;
				        var attrValue = settings.searchFrmVal[key].val();
				        
				        settings.searchFrmObj.find('input[name='+attrName+']').val(attrValue);
				        
				        null!=setSearchFunc&&setSearchFunc();
				    }
				}
				,search : function(){
					lazyLoaderAction.clear();
					lazyLoaderAction.setSearch(settings.setSearchFunc);
					lazyLoaderAction.load();
					eventSwitch(0);
					eventSwitch(1);
					
				},resize : function(){
					resize();
				},load : function(loadFunc){
					if(true) {
						$VDAS.http_post(settings.loadUrl,settings.searchFrmObj.serialize(),{
							beforeSend : function() {
								lazyLoader = true;
								settings.lastRow.show();
							}
							,success: function(r){
								var strHtml = settings.createDataHtml(r);
								settings.lastRow.before(strHtml);
								if(strHtml.length == 0) settings.lastRow.before(settings.$resultNone);
								resize();
								if(r.rtvList)
									if(r.rtvList.length < settings.searchFrmVal.limit.val()) {
										eventSwitch(0);
									}
								if(r.result)
									if(r.result.length < settings.searchFrmVal.limit.val()) {
										eventSwitch(0);
									}
								
							},done : function(r){
								lazyLoader = false;
								settings.lastRow.hide();
							}
						});												
					}
					
				}
			}
			
			$(this).data('lazyLoaderAction',lazyLoaderAction);
			
			if(settings.initSearch) lazyLoaderAction.search();
		});
	};
	
})(jQuery);



(function($){
	
	$.lazyAddrLoader = {
		clear : function(lazyLoaderObj){
			var action = lazyLoaderObj.data('lazyLoaderAction');
			action.clear();
		},setSearch : function(lazyLoaderObj,setsearchFunc){
			var action = lazyLoaderObj.data('lazyLoaderAction');
			action.setSearch(setsearchFunc);
		},search : function(lazyLoaderObj){
			var action = lazyLoaderObj.data('lazyLoaderAction');
			action.search();
		},resize : function(lazyLoaderObj){
			var action = lazyLoaderObj.data('lazyLoaderAction');
			action.resize();
		}
		
	}
	
	$.fn.lazyAddrLoader = function(options){
		
		return this.each(function(){
			var settings = $.extend({
				lazyLoader : false
				,searchFrmObj : null // $formObj
				,searchFrmVal : {} //{key : $inputObj,selectObj,radio,checkBox...}
				,headLength : $(this).find('thead tr:first').children().length
				,path : $VDAS.path
				,table : $(this)
				,tbody : $(this).find('tbody')
				,thead : $(this).find('thead')
				,offset : 0
				,limit : 10
				,setSearchFunc : null
				,lastRow : null
				,scrollRow : 4
				,rowHeight : 80
				,createDataHtml : function(r){}
				,loadUrl : null
				,$initOffset    : $("<input type='hidden' name='offset' value='0' />")
				,$initLimit    : $("<input type='hidden' name='limit' value='10' />")
				,$resultNone :  ""
				,initSearch : false
		    }, options );
			
			$(this).data('lazyLoaderOpts',settings);
			
			var createLoadingRow = function(){
				var strHtml ='<tr class="lazyLoader_loading" style="display:none"><td colspan="'+settings.headLength+'"><img src="'+settings.path+'/common/images/loading.gif" style="height:50px;"></td></tr>'
				settings.tbody.append(strHtml);
				settings.lastRow = settings.table.find('.lazyLoader_loading');
				
				if(settings.searchFrmObj.find("input[name=offset]").length == 0){
					settings.searchFrmObj.append(settings.$initOffset);
				}
				settings.searchFrmVal.offset = settings.$initOffset;
				if(settings.searchFrmObj.find("input[name=limit]").length == 0){
					settings.searchFrmObj.append(settings.$initLimit);
				}
				settings.searchFrmVal.limit = settings.searchFrmObj.find("[name=limit]");
				settings.searchFrmVal.offset = settings.searchFrmObj.find("[name=offset]");
			}
			
			var createInitRow = function(){
				var strHtml = '';
				if(_lang == 'ko'){
					strHtml = '<tr><td colspan="'+settings.headLength+'">검색결과가 없습니다.</td></tr>';
				}else{
					strHtml = '<tr><td colspan="'+settings.headLength+'">No result</td></tr>';
				}
				settings.$resultNone = strHtml;
				settings.lastRow.before(strHtml);	
				
			}
			
			var resize = function(){
				var rowLength = settings.tbody.find('tr').length;
				if(rowLength < settings.scrollRow){
					
					settings.tbody.css('display','');
					settings.tbody.css('height',(rowLength*settings.rowHeight)+"px");
					settings.tbody.css('overflow-y','none');
					settings.tbody.css('overflow-x','none');
					settings.tbody.css('width','100%');
					                 
				}else{               
					settings.tbody.css('display','block');
					settings.tbody.css('height',settings.scrollRow*settings.rowHeight+"px");
					settings.tbody.css('overflow-y','auto');
					settings.tbody.css('overflow-x','hidden');
					settings.tbody.css('width',settings.thead.css("width"));
					
					settings.tbody.find("tr:first").find("td,th").each(function(i){
						
						var headWidth = parseInt(settings.table.find("thead tr:first th").eq(i).css("width"));
						if(settings.tbody.find("tr:first").find("td,th").length-1 == i) headWidth = headWidth-17;
						$(this).css('width',headWidth+"px");
					});
					
					settings.lastRow.find("td").css("width",settings.thead.find("tr:first").css("width"));						
				} 
				return this;
			}
			
			createLoadingRow();
			if(settings.tbody.find('tr').length == 1)
				createInitRow();
			resize();
			
			var eventSwitch = function(b){
				if(b){
					settings.tbody.on("scroll",function() {
						var elem = settings.tbody;
						if ( elem[0].scrollHeight - elem.scrollTop() - elem.outerHeight() < 100)
					    {
							if(!lazyLoader) {
								
								var attr = settings.lastRow.prev().attr('last');

								if (typeof attr === typeof undefined || attr === false) {
									settings.searchFrmVal.offset.val(settings.tbody.find("tr").length-1);
									lazyLoaderAction.load();
								}
								console.log(settings.searchFrmVal);
							}
					    }
					});	
				}else{
					settings.tbody.off('scroll');
				}
					
			}
			eventSwitch(1);
			
			var lazyLoaderAction = {
				clear : function(){
					settings.tbody.find('tr:not(:last-child)').remove();
					settings.searchFrmVal.offset.val(0);
					settings.searchFrmVal.limit.val(settings.limit);
					eventSwitch(0);
					eventSwitch(1);
				},
				setSearch : function(setSearchFunc){
					for (var key in settings.searchFrmVal){
				        var attrName = key;
				        var attrValue = settings.searchFrmVal[key].val();
				        
				        settings.searchFrmObj.find('input[name='+attrName+']').val(attrValue);
				        
				        null!=setSearchFunc&&setSearchFunc();
				    }
				}
				,search : function(){
					lazyLoaderAction.clear();
					lazyLoaderAction.setSearch(settings.setSearchFunc);
					lazyLoaderAction.load();
					eventSwitch(0);
					eventSwitch(1);
					
				},resize : function(){
					resize();
				},load : function(loadFunc){
					if(true) {
						settings.searchFrmObj.find("input[name='countPerPage']").val(settings.searchFrmObj.find("input[name='limit']").val());
						settings.searchFrmObj.find("input[name='currentPage']").val((settings.searchFrmObj.find("input[name='offset']").val()/settings.searchFrmObj.find("input[name='limit']").val())+1);
						$.ajax({
							 url : "https://www.juso.go.kr/addrlink/addrLinkApiJsonp.do" 
							,type: "post"
							,data: settings.searchFrmObj.serialize()
							,dataType:"jsonp"								
							,crossDomain:true
							,beforeSend : function() {
								lazyLoader = true;
								settings.lastRow.show();
							}
							,success:function(r){
								
								var errCode = r.results.common.errorCode;
								var errDesc = r.results.common.errorMessage;
								if(errCode != "0"){
									alert(errCode+"="+errDesc);
								}else{
									var strHtml = settings.createDataHtml(r.results);
									settings.lastRow.before(strHtml);
									if(strHtml.length == 0) settings.lastRow.before(settings.$resultNone);
									resize();
									if(r.results.juso)
										if(r.results.juso.length < settings.searchFrmVal.limit.val()) {
											eventSwitch(0);
										}
									if(r.results.juso)
										if(r.results.juso.length < settings.searchFrmVal.limit.val()) {
											eventSwitch(0);
										}
								}
								
								lazyLoader = false;
								settings.lastRow.hide();
							}
						    ,done : function(r){
								lazyLoader = false;
								settings.lastRow.hide();
							}
						});
					}
					
				}
			}
			
			$(this).data('lazyLoaderAction',lazyLoaderAction);
			
			if(settings.initSearch) lazyLoaderAction.search();
		});
	};
	
})(jQuery);

$(function(){
	$(document).ready(function(){
		$( document ).ajaxStart(function() {
			$("body").css("cursor","wait");
		});
		
		$( document ).ajaxComplete(function() {
			$("body").css("cursor","auto");
		});
		
		$(document).on("keyup",".numberOnly",function(){
			$(this).val($(this).val().replace(/[^0-9]/gi,""));
		});
		
		$(document).on("keyup",".hourOnly",function(){
			if(Number($(this).val()) < 0) $(this).val(0);
			if(Number($(this).val()) > 23) $(this).val(23);
		});
		$(document).on("keyup",".minuteOnly",function(){
			if(Number($(this).val()) < 0) $(this).val(0);
			if(Number($(this).val()) > 59) $(this).val(59);
		});
		
		//searchbox select (none event)
		/*$('.sel').sSelect({ddMaxHeight: '300px'});*/
		
		var date = new Date();
		/*$("#nowDate")&&$("#nowDate").html(date.getFullYear()+"/"+LPAD(Number(date.getMonth()+1)+"",'0',2)+"/"+LPAD(Number(date.getDate())+"",'0',2)+"&nbsp;"+date.getHours()+":"+LPAD(Number(date.getMinutes())+"",'0',2)+" "+$message["label.now"]+" <a href='#none' id='btn_reload'>"+$message["label.refresh"]+"</a>");*/
		$("#btn_reload").on("click",function(){			
			document.location.reload();
		});
		
	});
		
});


$(window).load(function(e) {
	
});

$(window).resize(function(e) {
	
});


$(window).scroll(function(e) {
	
});


jQuery.fn.serializeObject = function() {

	var obj = null;

	try {

		// this[0].tagName이 form tag일 경우

		if (this[0].tagName && this[0].tagName.toUpperCase() == "FORM") {

			var arr = this.serializeArray();

			if (arr) {

				obj = {};

				jQuery.each(arr, function() {

					// obj의 key값은 arr의 name, obj의 value는 value값

					obj[this.name] = this.value;

				});

			}

		}

	} catch (e) {

		alert(e.message);

	} finally {
	}

	return obj;

};




