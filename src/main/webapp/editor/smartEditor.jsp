<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String contents = request.getParameter("contents");

	//출력 오류 처리 필요
	contents = contents.replaceAll("\"", "'");
	contents = contents.replaceAll("\r", "");
	contents = contents.replaceAll("\n", "");
	contents = contents.replaceAll("\r\n", "");
	
/*
	// nl2br 함수 - 게시판 글 개행-엔터 처리
	public String nl2br(String str) {
		str = str.replaceAll("\r\n", "<br>");
		str = str.replaceAll("\r", "<br>");
		str = str.replaceAll("\n", "<br>");
		
		return str;
	}

	public static String htmlToText(String pStr) {
		if (pStr == null) {
			pStr = "";
		} else {
			//pStr = pStr.trim();
			pStr = replace(pStr, "&", "&amp;");
			pStr = replace(pStr, "<", "&lt;");
			pStr = replace(pStr, ">", "&gt;");
			pStr = replace(pStr, "\"", "&quot;");
			pStr = replace(pStr, "'", "&#39;");
			pStr = replace(pStr, "\"", "&#34;");
		}
	
		return pStr;
	}
*/
%>

<script type="text/javascript" src="${pageContext.request.contextPath}/editor/js/HuskyEZCreator.js" charset="utf-8"></script>
<textarea id="editor1" name="contents" rows="10" cols="100" style="width:100%; height:250px; display:none;"></textarea>
<script type="text/javascript">

var oEditors = [];
nhn.husky.EZCreator.createInIFrame({
	oAppRef: oEditors,
	elPlaceHolder: "editor1",
	sSkinURI: "${pageContext.request.contextPath}/editor/SmartEditor2Skin.html",	
	htParams : {bUseToolbar : true,
		fOnBeforeUnload : function(){
			//alert("아싸!");	
		}
	}, //boolean
	fOnAppLoad : function(){
		//데이타 삽입
		oEditors.getById["editor1"].exec("PASTE_HTML", ["<%=contents%>"]);
		
	},
	fCreator: "createSEditor2"
});

function submitContents(elClickedObj) {
    oEditors.getById["editor1"].exec("UPDATE_CONTENTS_FIELD", []); // 에디터의 내용이 textarea에 적용됩니다.
	
    sHTML = oEditors.getById["editor1"].getIR();
    return sHTML;
    
	// 에디터의 내용에 대한 값 검증은 이곳에서 document.getElementById("ir1").value를 이용해서 처리하면 됩니다.
	
	/* try {
		elClickedObj.form.submit();
	} catch(e) {} */
}
function clearContents(){
	oEditors.getById["editor1"].exec("SET_CONTENTS", [""]);  // 내용초기화
}
function setContents(html) {
	oEditors.getById["editor1"].exec("PASTE_HTML", [html]);
}

</script>