<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<%

response.setStatus(HttpServletResponse.SC_OK);
response.setHeader("server", "");
%>
<style>
.error_box {border:0px solid #cecece; padding:45px 0; text-align:center;}
.error_box .img_box { margin-bottom:40px;}
.error_box p { margin:0 0 40px 0; font-size:15px; line-height:28px; font-family:'Noto Sans KR'; font-weight:300;}
.error_box p em {color:#272727; font-weight:800; font-size:20px;}
.error_box + .join_error_box { margin-top:15px;}
</style>
<div class="join_layout" style="padding-top : 0px;">
	<div class="pageTab" style="    text-align: center;">
		<%-- <img src="${pathCommon}/images/400.png" alt=""> --%>
		
		<div class="error_box">
			<div class="img_box">
				<img src="${pathCommon}/images/ico_notice3.png" alt="">
			</div>

			<p>
 				<em>알 수 없는 오류가 발생하였습니다.</em>
 			</p>
     		<h3>
	      		<p>
					요청하신 페이지에서 일시적인 에러가 발생하였습니다.<br>
					서비스 이용에 불편을 드려 죄송합니다.<br>
					신속히 조치하도록 하겠습니다.<br>
				</p>
				<p>
					동일한 문제가 지속적으로 발생하는 경우는<br> 자스텍엠 고객센터(1599-8439)로 문의해주세요.
					<br>감사합니다.
	  			</p>
 			</h3>
     	</div>
    </div>
</div>