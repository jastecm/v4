<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 


<%@include file="/WEB-INF/jsp/common/common.jsp"%>


<script type="text/javascript">
$(document).ready(function(){
	
	
/*	
	$("#btn_noti").on("click",function(){
		$("#li_noti").show();
		$("#li_festival").hide();
		$("#li_faq").hide();
	});
	$("#btn_festival").on("click",function(){
		$("#li_festival").show();
		$("#li_faq").hide();
		$("#li_noti").hide();
	});
	$("#btn_faq").on("click",function(){
		$("#li_faq").show();
		$("#li_noti").hide();
		$("#li_festival").hide();
	});
	
	$(".btn_close").on("click",function(){
		$( "#pop" ).dialog("destroy");
	});
	
	$("body").on("click",".btn_view",function(){
		var id = $(this).data("id");
		var board = $(this).data("board");
		if(id.length == 0) return;
		else{
			
			$VDAS.http_post("/helpdesk/board/"+board+"/"+id,{},{
				success : function(r){
					var vo = r.result;
					console.log(r.result);
					$( "#pop" ).attr("title",vo.title);
					$("#pop .contents").html(vo.contents);
					if(vo.fileId){
						$("#pop .info").html('<fmt:message key='main.file'/> : <a href="${defaultPath}/com/getFile.do?uuid='+vo.fileId+'">'+vo.orgFileNm+'</a>').show();
					}else{
						$("#pop .info").html("").hide();
					}
					
					
					$( "#pop" ).dialog({
				        width:1000,
				        dialogClass:'alert',
				        draggable: false,
				        modal:true,
				        height:"auto",
				        resizable:false,
				        closeOnEscape:true,
				        buttons:{
				        	'<fmt:message key='main.confirm'/>':function(){
				     			$(this).dialog("destroy");
				     		}
				        }
				    });
					
					
				}
			});
		}		
	});
	*/
});



</script>
<form id="detailFrm" style="display:none" method="post">
	<input type="hidden" name="searchBoardId" value="" />
</form>
<div class="main_box1">
	<div class="main_box1_1">
		<img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_img1_1.png" alt="" />
 		<a class="btn_mainbtn01" href="https://play.google.com/store/apps/details?id=com.jastecm.viewcar.android" target="_blank"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_btn1.jpg" /></a>
        <a class="btn_mainbtn02" href="https://itunes.apple.com/kr/app/%EB%B7%B0%EC%B9%B4-%ED%94%84%EB%A1%9C/id1221803378?mt=8" target="_blank"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_btn2.jpg" /></a>
	</div>
	<div class="main_box1_2">
		<div class="main_box1_3">
			<span><a href="${defaultPath }/drivingMng/searchDriving.do"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_btn2_1.jpg" alt="" /></a></span><span><a id="btn_interval" href="${defaultPath }/vehicleMng/vehicleReport.do"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_btn2_2.jpg" alt="" /></a><a href="${defaultPath }/report/drivingReport.do"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_btn2_3.jpg" alt="" /></a></span>
		</div>
		<ul class="main_box1_4">
			<li>
				<a href="${defaultPath }/vehicleMng/vehicleFee.do"><fmt:message key="main.vehicleFee"/></a>
				<a href="${defaultPath }/clinic/vehicleMaintenance.do"><fmt:message key="main.vehicleMaintenance"/></a>
				<a href="${defaultPath }/drivingMng/searchLocation.do" ><fmt:message key="main.searchLocation"/></a>
				<a href="${defaultPath }/report/integrationReport.do" ><fmt:message key="main.integrationReport"/></a>
			</li>
			<li id="li_noti">
				<h1>
					<fmt:message key="main.noti"/>
					<a id="btn_festival"><img src="${pageContext.request.contextPath}/common/images/btn_notice.jpg" alt="" /></a>
				</h1>
				<div>
					<c:forEach var="vo" items="${noti}" varStatus="i">
						<a href="#none" class="btn_view" data-id="${vo.boardKey }" data-board="${vo.boardType }">${vo.titleShort }</a>
					</c:forEach>
				</div>
			</li>
			<li id="li_festival" style="display:none">
				<h1>
					<fmt:message key="main.event"/>
					<a id="btn_faq"><img src="${pageContext.request.contextPath}/common/images/btn_notice.jpg" alt="" /></a>
				</h1>
				<div>	
					<c:forEach var="vo" items="${festival}" varStatus="i">
						<a href="#none" class="btn_view" data-id="${vo.boardKey }" data-board="${vo.boardType }">${vo.titleShort }</a>
					</c:forEach>
				</div>
			</li>
			<li id="li_faq" style="display:none">
				<h1>
					<fmt:message key="main.faq"/>
					<a id="btn_noti"><img src="${pageContext.request.contextPath}/common/images/btn_notice.jpg" alt="" /></a>
				</h1>
				<div>	
					<c:forEach var="vo" items="${faq}" varStatus="i">
						<a href="#none" class="btn_view" data-id="${vo.boardKey }" data-board="${vo.boardType }">${vo.titleShort }</a>
					</c:forEach>
				</div>
			</li>
		</ul>
	</div>
</div>
	
	
	
	
	
<div id="pop" title="" class="myPopup" style="display: none">
	<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt=""></a>
	<div class="info"></div>		
	<div class="contents"></div>
</div>
<style>
#pop .info{width: 100%;height: 30px;border-bottom: 1px solid #747474;margin-bottom: 20px;display:none}
#pop .contents div,p{line-height: 18px; font-size: 13px;} 
</style>