<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>  
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/index.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#btn_interval").on("click",function(){
		if("${VDAS.allocationUsed}" == "1") document.location.href = "${defaultPath}//vehicle/vehicleInterval.do";
		else alert("${_C.label1}");
	});
	
	$("#moveUserInfo").on("click",function(){
		var userId = "${VDAS.id}";
		if(userId.length == 0) return;
		else{
		var url = _defaultPath+"/driving/searchUserDetail.do";		
		$("#detailFrm input[name=searchUserId]").val(userId);
		$("#detailFrm").attr("action",url);
		$("#detailFrm").submit();
		}
	});
	
	$("#btn_faq").on("click",function(){
		$("#li_noti").hide();
		$("#li_faq").show();
	});
	
	$("#btn_noti").on("click",function(){
		$("#li_noti").show();
		$("#li_faq").hide();
	});
	
	$(".btn_view").on("click",function(){
		var id = $(this).data("id");
		if(id.length == 0) return;
		else{
		var url = _defaultPath+"/helpdesk/boardView.do";
		$("#detailFrm input[name=searchBoardId]").val(id);
		$("#detailFrm").attr("action",url);
		$("#detailFrm").submit();
		}		
	});
	
	$(".scoreChart").on("click",function(){
		var d = $(this).data("val");
		$(".scoreChart").removeClass("on");
		$(this).addClass("on");
		
		$VDAS.http_post("/main/userScore.do",{searchDay : d},function(rtv){
			
			var safe = rtv.avgScore[0].safe;
			var eco = rtv.avgScore[0].eco;
			
			drawScore([
			           { title: "${_C.label2}", value:  safe,   color: "#97dc6f" },
			           { title: "", value : 100-safe,  color: "#e7e7e7" },
			           { title: "dummy", value:  safe+(100-safe),   color: "#e7e7e7" }
			         ],[
			            { title: "${_C.label3}", value:  eco,   color: "#ff9d3d" },
			            { title: "", value : 100-eco,  color: "#e7e7e7" },
			            { title: "dummy", value:  eco+(100-eco),   color: "#e7e7e7" }
			            
			         ]);
			$("#avgSafeScore").text(safe);
			$("#avgEcoScore").text(eco);
			$("#avgUserSafeScore").text("${_C.label4} "+rtv.userScore[0].safe+"${_C.label6}");
			$("#avgUserEcoScore").text("${_C.label5} "+rtv.userScore[0].eco+"${_C.label6}");
		});
		
			
	});
	$(".scoreChart").eq(0).trigger("click");
	
	$(".scoreTime").on("click",function(){
		var d = $(this).data("val");
		$(".scoreTime").removeClass("on");
		$(this).addClass("on");
		
		$VDAS.http_post("/main/userTime.do",{searchDay : d},function(rtv){
			drawTime([
                       { title: "${_C.label7}", value : rtv.time[0].intervalTime-rtv.time[0].drivingTime,  color: "#e7e7e7" },
                       { title: "${_C.label8}", value:  rtv.time[0].drivingTime,   color: "#e81626" },
                       { title: "${_C.label9}", value:  rtv.time[0].outDrivingTime,   color: "#2370ee" }
                     ]);
			
			
			$("#drivingTime").text(rtv.time[0].drivingTime+" h");
			$("#outDrivingTime").text(rtv.time[0].outDrivingTime+" h");
			$("#noneDrivingTime").text(rtv.time[0].intervalTime-rtv.time[0].drivingTime+" h");
		});
		
			
	});
	$(".scoreTime").eq(0).trigger("click");
	
	$(".scoreFee").on("click",function(){
		var d = $(this).data("val");
		$(".scoreFee").removeClass("on");
		$(this).addClass("on");
		
		$VDAS.http_post("/main/userFee.do",{searchDay : d},function(rtv){
			drawFee([
			   	  { title: "${_C.label10}", value : rtv.fee[0].feeFuel,  color: "#3abf9b" },
				  { title: "${_C.label11}", value: rtv.fee[0].feePass,   color: "#ffaa24" },
				  { title: "${_C.label12}", value: rtv.fee[0].feeParking,   color: "#4884ff" },
				  { title: "${_C.label13}", value: rtv.fee[0].feeMente,   color: "#ff3b69" }
				]);
			
			$("#feeFuel").text(number_format(rtv.fee[0].feeFuel)+" ${_C.label14}");
			$("#feePass").text(number_format(rtv.fee[0].feePass)+" ${_C.label14}");
			$("#feeParking").text(number_format(rtv.fee[0].feeParking)+" ${_C.label14}");
			$("#feeMente").text(number_format(rtv.fee[0].feeMente)+" ${_C.label14}");
		});
		
			
	});
	$(".scoreFee").eq(0).trigger("click");
	
	drawFeeHstr();
});

var drawScore = function(safe,eco){
	$("#doughnutChart3 svg,#doughnutChart3 .doughnutSummary").remove();
	$("#doughnutChart3").drawDoughnutChart(safe,{startLeft:true});
	
	$("#doughnutChart4 svg,#doughnutChart4 .doughnutSummary").remove();
    $("#doughnutChart4").drawDoughnutChart(eco,{startLeft:true});
}
var drawTime = function(data){
	$("#doughnutChart5 svg,#doughnutChart5 .doughnutSummary").remove();
	$("#doughnutChart5").drawDoughnutChart(data);
}
var drawFee = function(data){
	$("#doughnutChart6 svg,#doughnutChart6 .doughnutSummary").remove();
	$("#doughnutChart6").drawDoughnutChart(data);
}
var drawFeeHstr = function(){
	
	$VDAS.http_post("/main/userFeeHstr.do",{},function(rtv){
		var dataset = [];
		var labels = [];
		var max = 0;
		for(var i = 0 ; i < rtv.feeHstrCost.length ; i++){
			var ymd = rtv.feeHstrCost[i].ymd;
			var cost = rtv.feeHstrCost[i].fee+rtv.feeHstrMente[i].fee;
			if(cost > max) max = cost;
			labels.push(ymd);
			dataset.push(cost);
		}
		
		var chartData = {
				node: "graph",
				dataset: dataset,
				labels: labels,
				pathcolor: "#898989",
				fillcolor: "#e81626",
				xPadding: 0,
				yPadding: 20,
				ybreakperiod: max/5
			};
		if(max) drawlineChart(chartData);
		
	});
	
	
}
</script>
<form id="detailFrm" style="display:none" method="post">
	<input type="hidden" name="searchUserId" value="" />	
	<input type="hidden" name="searchVehicleId" value="" />
	<input type="hidden" name="searchBoardId" value="" />
</form>
<div class="main_box1">
	<div class="main_box1_1">
		<img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_img1.jpg" alt="" />
		<a href="https://play.google.com/store/apps/details?id=kr.co.infinityplus.viewcar" target="_blank"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_btn1.jpg" alt="${_C.label15}" /></a>
	</div>
	<div class="main_box1_2">
		<div class="main_box1_3">
			<span><a href="${defaultPath }/driving/searchVehicle.do"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_btn2_1.jpg" alt="" /></a></span><span><a id="btn_interval"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_btn2_2.jpg" alt="" /></a><a href="${defaultPath }/vehicle/vehicleReport.do"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_btn2_3.jpg" alt="" /></a></span>
		</div>
		<ul class="main_box1_4">
			<li>
				<a href="${defaultPath }/vehicle/vehicleCost.do">${_C.label16}</a>
				<a href="${defaultPath }/clinic/vehicleMaintenance.do">${_C.label17}</a>
				<a href="#none" onClick="alert('서비스 준비중입니다.');return;">${_C.label18}</a>
				<a id="moveUserInfo" >${_C.label19}</a>
			</li>
			<li id="li_noti">
				<h1>
					${_C.label20}
					<a id="btn_faq"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/btn_notice.jpg" alt="" /></a>
				</h1>
				<div>
					<c:forEach var="vo" items="${notiList}" varStatus="i">
						<a href="#none" class="btn_view" data-id="${vo.id }">${vo.titleShort }</a>
					</c:forEach>
				</div>
			</li>
			<li id="li_faq" style="display:none">
				<h1>
					${_C.label21}
					<a id="btn_noti"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/btn_notice.jpg" alt="" /></a>
				</h1>
				<div>
					<c:forEach var="vo" items="${faqList}" varStatus="i">
						<a href="#none" class="btn_view" data-id="${vo.id }">${vo.titleShort }</a>
					</c:forEach>
				</div>
			</li>
		</ul>
	</div>
</div>
<div class="main_box2">
	<div class="main_box2_1">
		<h1>
			<img src="${pageContext.request.contextPath}/common/images/${_LANG}/h1_2.jpg" alt="${_C.label22}" />
			<span>
				<a class="scoreChart" data-val="D">${_C.label23}</a><a class="scoreChart" data-val="W">${_C.label24}</a><a class="scoreChart" data-val="M">${_C.label25}</a>
			</span>
		</h1>
		<div class="chart_layout">
			<div id="doughnutChart3" class="chart">
				<div class="main_chart_box1">
					<div class="con">
						<img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_icon1.jpg" alt="" />
						<p id="avgSafeScore">0</p>
						<span>${_C.label2}</span>
						<strong class="c1" id="avgUserSafeScore">${_C.label4} 0${_C.label6}</strong>
					</div>
				</div>
			</div>
			<div id="doughnutChart4" class="chart">
				<div class="main_chart_box1">
					<div class="con">
						<img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_icon2.jpg" alt="" />
						<p id="avgEcoScore">0</p>
						<span>${_C.label3}</span>
						<strong class="c2" id="avgUserEcoScore">${_C.label5} 0${_C.label6}</strong>
					</div>
				</div> 
			</div>
		</div>
	</div>
	<div class="main_box2_2">
		<h1>
			<img src="${pageContext.request.contextPath}/common/images/${_LANG}/h1_3.jpg" alt="${_C.label26}" />
			<span>
				<a class="scoreTime" data-val="D">${_C.label23}</a><a class="scoreTime" data-val="W">${_C.label24}</a><a class="scoreTime" data-val="M">${_C.label25}</a>
			</span>
		</h1>
		<div class="main_box2_3">
			<div class="chart_layout">
				<div id="doughnutChart5" class="chart">
				</div>
			</div>
			<div class="main_box2_4">
				<p>${_C.label26}</p>
				<ul>
					<li>
						<img src="${pageContext.request.contextPath}/common/images/${_LANG}/dot5.jpg" alt="" />${_C.label8}<span id="drivingTime">0 h</span>
					</li>
					<li>
						<img src="${pageContext.request.contextPath}/common/images/${_LANG}/dot6.jpg" alt="" />${_C.label9}<span id="outDrivingTime">0 h</span>
					</li>
					<li>
						<img src="${pageContext.request.contextPath}/common/images/${_LANG}/dot7.jpg" alt="" />${_C.label7}<span id="noneDrivingTime">0 h</span>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="main_box2 main_box3">
	<div class="main_box2_1">
		<h1>
			<img src="${pageContext.request.contextPath}/common/images/${_LANG}/h1_4.jpg" alt="차량 운영비" />
			<span>
				<a class="scoreFee" data-val="D">${_C.label23}</a><a class="scoreFee" data-val="W">${_C.label24}</a><a class="scoreFee" data-val="M">${_C.label25}</a>
			</span>
		</h1>
		<div class="main_box2_3">
			<div class="chart_layout">
				<div id="doughnutChart6" class="chart">
				</div>
			</div>
			<div class="main_box2_4">
				<p>${_C.label28}</p>
				<ul>
					<li>
						<img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_icon4.jpg" alt="" />${_C.label10}<span id="feeFuel">0 ${_C.label14}</span>
					</li>
					<li>
						<img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_icon6.jpg" alt="" />${_C.label11}<span id="feePass">0 ${_C.label14}</span>
					</li>
					<li>
						<img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_icon5.jpg" alt="" />${_C.label12}<span id="feeParking">0 ${_C.label14}</span>
					</li>
					<li>
						<img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_icon7.jpg" alt="" />${_C.label13}<span id="feeMente">0 ${_C.label14}</span>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="main_box2_2">
		<h1>
			<img src="${pageContext.request.contextPath}/common/images/${_LANG}/h1_5.jpg" alt="${_C.label29}" />
			<span>
			</span>
		</h1>
		<div class="main_box4">
			<div style="margin:20px auto; text-align:center;"><canvas id="graph" width="449" height="187" align="center"></canvas></div>
		</div>
	</div>
</div>