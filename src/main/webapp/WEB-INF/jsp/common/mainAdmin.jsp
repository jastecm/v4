<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>  
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/index.js"></script>
<style>.svg-clipped{-webkit-clip-path:url(#svgPath);clip-path:url(#svgPath);}</style>
<svg height="0" width="0">
	<defs>
		<clipPath id="svgPath" clipPathUnits="objectBoundingBox">
  			<circle cx=".5" cy=".5" r=".5" />
   		</clipPath>
	</defs>
</svg>
<script type="text/javascript">
$(document).ready(function(){
	
	
	init();
	
	$(".sel_vehicleRank").sSelect({ddMaxHeight: '300px'}).on("change",function(){
		var p = $(this).val();
		$(".vehicleRank").each(function(){
			if($(this).is(".on")){
				$(this).trigger("click");
				return false;
			}
		});
	});
	
	$(".sel_costRank").sSelect({ddMaxHeight: '300px'}).on("change",function(){
		var p = $(this).val();
		$(".scoreCost").each(function(){
			if($(this).is(".on")){
				$(this).trigger("click");
				return false;
			}
		});
	});
	
	$(".sel_userRank").sSelect({ddMaxHeight: '300px'}).on("change",function(){
		var p = $(this).val();
		$(".userRank").each(function(){
			if($(this).is(".on")){
				$(this).trigger("click");
				return false;
			}
		});
	});
	
	$(".vehicleRank").on("click",function(){
		var d = $(this).data("val");
		var o = $(".sel_vehicleRank").val();
		
		$(".vehicleRank").removeClass("on");
		$(this).addClass("on");
		
		
		$VDAS.http_post("/main/adminVehicleRank.do",{searchDay : d,searchOrder:o},function(rtv){
			
			for(var i = 0 ; i < rtv.rtvList.length ; i++){
				var data = rtv.rtvList[i];
				var rank = i+1;
				var strHtml = '<span><img src="${pageContext.request.contextPath}/common/images/vehicle/'+data.imgS+'.png" alt="" /></span><span><a href="#none" class="a_vehicleDetail" data-id="'+data.vehicleId+'">'+data.modelMaster+'</a><a href="#none" class="a_vehicleDetail" data-id="'+data.vehicleId+'">'+data.plateNumber+'</a></span>';
				$("#vehicleRank"+rank).html(strHtml);
				if(o == "T") strHtml = '<p>${_C.label1} '+data.drivingTime+'${_C.label3} / ${_C.label2} '+data.intervalTime+'${_C.label3}</p>';
				else if(o == "D") strHtml = '<p>${_C.label1} '+data.distance+'km</p>';
				$("#vehicleRank"+rank).next().remove();
				$("#vehicleRank"+rank).after(strHtml);
			}
			
			
		});
		
			
	});
	$(".vehicleRank").eq(0).trigger("click");
	
	$(".scoreChart").on("click",function(){
		var d = $(this).data("val");
		$(".scoreChart").removeClass("on");
		$(this).addClass("on");
		
		$VDAS.http_post("/main/adminScore.do",{searchDay : d},function(rtv){
			
			var safe = rtv.avgScore[0].safe;
			var eco = rtv.avgScore[0].eco;
			
			drawScore([
			           { title: "${_C.label4}", value:  safe,   color: "#97dc6f" },
			           { title: "", value : 100-safe,  color: "#e7e7e7" },
			           { title: "dummy", value:  safe+(100-safe),   color: "#e7e7e7" }
			         ],[
			            { title: "${_C.label5}", value:  eco,   color: "#ff9d3d" },
			            { title: "", value : 100-eco,  color: "#e7e7e7" },
			            { title: "dummy", value:  eco+(100-eco),   color: "#e7e7e7" }
			            
			         ]);
			$("#avgSafeScore").text(safe);
			$("#avgEcoScore").text(eco);
		});
		
			
	});
	$(".scoreChart").eq(0).trigger("click");
	
	$(".scoreTime").on("click",function(){
		var d = $(this).data("val");
		$(".scoreTime").removeClass("on");
		$(this).addClass("on");
		
		$VDAS.http_post("/main/adminTime.do",{searchDay : d},function(rtv){
			var outTime = (rtv.time[0].intervalTime-rtv.time[0].drivingTime)<0?0:(rtv.time[0].intervalTime-rtv.time[0].drivingTime);
			drawTime([
                       { title: "${_C.label9}", value : outTime,  color: "#e7e7e7" },
                       { title: "${_C.label10}", value:  rtv.time[0].drivingTime,   color: "#e81626" },
                       { title: "${_C.label11}", value:  rtv.time[0].outDrivingTime,   color: "#2370ee" }
                     ]);
			
			
			$("#drivingTime").text(rtv.time[0].drivingTime+" h");
			$("#outDrivingTime").text(rtv.time[0].outDrivingTime+" h");
			$("#noneDrivingTime").text(outTime+" h");
		});
		
			
	});
	$(".scoreTime").eq(0).trigger("click");
	
	$(".scoreFee").on("click",function(){
		var d = $(this).data("val");
		$(".scoreFee").removeClass("on");
		$(this).addClass("on");
		
		$VDAS.http_post("/main/adminFee.do",{searchDay : d},function(rtv){
			drawFee([
			   	  { title: "${_C.label12}", value : rtv.fee[0].feeFuel,  color: "#3abf9b" },
				  { title: "${_C.label13}", value: rtv.fee[0].feePass,   color: "#ffaa24" },
				  { title: "${_C.label14}", value: rtv.fee[0].feeParking,   color: "#4884ff" },
				  { title: "${_C.label15}", value: rtv.fee[0].feeMente,   color: "#ff3b69" }
				]);
			
			$("#feeFuel").text(number_format(rtv.fee[0].feeFuel)+" ${_C.label16}");
			$("#feePass").text(number_format(rtv.fee[0].feePass)+" ${_C.label16}");
			$("#feeParking").text(number_format(rtv.fee[0].feeParking)+" ${_C.label16}");
			$("#feeMente").text(number_format(rtv.fee[0].feeMente)+" ${_C.label16}");
		});
		
			
	});
	$(".scoreFee").eq(0).trigger("click");
	
	drawFeeHstr();
	
	
	$(".scoreCost").on("click",function(){
		var d = $(this).data("val");
		var o = $(".sel_costRank").val();
		$(".scoreCost").removeClass("on");
		$(this).addClass("on");
		
		$VDAS.http_post("/main/adminCostRank.do",{searchDay : d,searchOrder:o},function(rtv){
			
			for(var i = 0 ; i < 3 ; i++){
				var data = rtv.costRank[i];
				if(data){
				var rank = i+1;
				
				var strHtml = '';
				if(d == "U"){
					if(data.typeImg){
						strHtml = '<span><img class="svg-clipped" style="height:49px;width:49px;" src="${defaultPath}/com/getUserImg.do?uuid='+data.typeImg+'" alt="" /></span><span><a href="#none" class="a_userDetail" data-id="'+data.typeId+'">'+(data.typeGroup==''?'${_C.label17}':data.typeGroup)+'</a><a href="#none" class="a_userDetail" data-id="'+data.typeId+'">'+data.typeNm+'</a></span>'	
					}else{
						strHtml = '<span><img src="${pageContext.request.contextPath}/common/images/${_LANG}/user_none.png" alt="" /></span><span><a href="#none" class="a_userDetail" data-id="'+data.typeId+'">'+(data.typeGroup==''?'${_C.label17}':data.typeGroup)+'</a><a href="#none" class="a_userDetail" data-id="'+data.typeId+'">'+data.typeNm+'</a></span>'
					}
				}else if(d=="V"){
					strHtml = '<span><img src="${pageContext.request.contextPath}/common/images/vehicle/'+data.typeImg+'.png" alt="" /></span><span><a href="#none" class="a_vehicleDetail" data-id="'+data.typeId+'">'+data.typeGroup+'</a><a href="#none" class="a_vehicleDetail" data-id="'+data.typeId+'">'+data.typeNm+'</a></span>'
				}else if(d=="G"){
					strHtml = '<span><img src="${pageContext.request.contextPath}/common/images/${_LANG}/user_none.png" alt="" /></span><span><a href="#none" class="" ></a><a href="#none" class="" >'+(data.typeNm==''?'${_C.label17}':data.typeNm)+'</a></span>'
				}
				
				$("#costRank"+rank).html(strHtml);
				var fee = 0;
				if(o == "ALL"){
					fee = data.feeFuel+data.feePass+data.feeParking+data.feeMente;
				}else if(o == "FUEL"){
					fee = data.feeFuel;
				}else if(o == "PASS"){
					fee = data.feePass;
				}else if(o == "PARK"){
					fee = data.feeParking;
				}else if(o == "MAIN"){
					fee = data.feeMente;
				}
				strHtml = '<p style="padding-left:47px;">'+fee+'${_C.label16}'+'</p>';
				$("#costRank"+rank).next().remove();
				$("#costRank"+rank).after(strHtml);
				}else{
					var rank = i+1;
					$("#costRank"+rank).html("");
					$("#costRank"+rank).next().remove();
					$("#costRank"+rank).after("");
				}
			}
			
			
		});
		
			
	});
	
	
	
	$(".scoreCost").eq(0).trigger("click");
	
	$(".userRank").on("click",function(){
		var d = $(this).data("val");
		var o = $(".sel_userRank").val();
		$(".userRank").removeClass("on");
		$(this).addClass("on");
		$VDAS.http_post("/main/adminUserRank.do",{searchDay : d,searchOrder:o},function(rtv){
			
			for(var i = 0 ; i < 3 ; i++){
				var data = rtv.rtvList[i];
				if(data){
					var rank = i+1;
					
					var strHtml = '';
					if(data.imgId){
						strHtml = '<span><img class="svg-clipped" style="height:49px;width:49px;" src="${defaultPath}/com/getUserImg.do?uuid='+data.imgId+'" alt="" /></span><span><a href="#none" class="a_userDetail" data-id="'+data.userId+'">'+(data.corpGroupNm==''?'${_C.label17}':data.corpGroupNm)+'</a><a href="#none" class="a_userDetail" data-id="'+data.userId+'">'+data.userNm+'</a></span>'	
					}else{
						strHtml = '<span><img src="${pageContext.request.contextPath}/common/images/${_LANG}/user_none.png" alt="" /></span><span><a href="#none" class="a_userDetail" data-id="'+data.userId+'">'+(data.corpGroupNm==''?'${_C.label17}':data.corpGroupNm)+'</a><a href="#none" class="a_userDetail" data-id="'+data.userId+'">'+data.userNm+'</a></span>'
					}
					
					$("#userRank"+rank).html(strHtml);
					if(o == "T") strHtml = '<p style="padding-left:0px;">${_C.label1} '+data.drivingTime+'${_C.label3} / ${_C.label2} '+data.intervalTime+'${_C.label3}</p>';
					else if(o == "D") strHtml = '<p style="padding-left:47px;">${_C.label1} '+data.distance+'km</p>';
					$("#userRank"+rank).next().remove();
					$("#userRank"+rank).after(strHtml);
				
				}else{
					var rank = i+1;
					$("#userRank"+rank).html("");
					$("#userRank"+rank).next().remove();
					$("#userRank"+rank).after("");
				}
			}
		});
	});
	$(".userRank").eq(0).trigger("click");
	
	$(".groupCnt").on("click",function(){
		var d = $(this).data("val");
		$(".groupCnt").removeClass("on");
		$(this).addClass("on");
		$VDAS.http_post("/main/getAdminGroupUserRank.do",{searchDay : d},function(rtv){
			var arrData = [];
			var strHtml = "";
			arrData.push({title: "", value : 0});
			for(var i = 0 ; i < rtv.rtvList.length ; i++){
				var data = rtv.rtvList[i];
				var corpGroupNm = data.corpGroupNm==''?'${_C.label17}':data.corpGroupNm;
				var value = data.cnt;
				var gorupValue = data.corpGroupCnt;
				
				var color = "";
				if(i==0) color = "#ca83f3"; 
				if(i==1) color = "#ffc71b"; 
				if(i==2) color = "#3cb3eb"; 
				if(i==3) color = "#ff6a91"; 
				var param = { title: corpGroupNm, value : value,  color: color };
				
				arrData.push(param);
				
				strHtml +="<li style='padding-right:0px;'>";
				strHtml +="<img src='${pageContext.request.contextPath}/common/images/${_LANG}/main_icon"+(8+i)+".jpg' alt='' />"+corpGroupNm+"<span>"+value+"${_C.label18}/"+gorupValue+"${_C.label18}</span>";
				strHtml +="</li>";
			}
			drawGroup(arrData);
			$("#groupCntUl").html(strHtml);
		});
	});
	$(".groupCnt").eq(0).trigger("click");
	
	$(".oilCnt").on("click",function(){
		var d = $(this).data("val");
		$(".oilCnt").removeClass("on");
		$(this).addClass("on");
		$VDAS.http_post("/main/getAdminOilPrice.do",{searchDay : d},function(rtv){
			var arrData = [];
			var strHtml = "";
			arrData.push({title: "", value : 0});
			
			var distanceSum = 0;
			var fuelSum = 0;
			var priceSum = 0;
			
			var cnt1 = 0;
			var cnt2 = 0;
			var cnt3 = 0;
			for(var i = 0 ; i < rtv.rtvList.length ; i++){
				var data = rtv.rtvList[i];
				var contentNm = data.content;
				var cnt = data.cnt;
				var distance = data.distance;
				var fuelUse = data.fuelUse;
				var price = data.price;
				
				distanceSum += parseInt(distance);
				fuelSum += parseInt(fuelUse);
				priceSum += parseInt(price);
				
				var color = "";
				if(i==0) {
					$("#oilCnt2").html(cnt+"${_C.label74}");
					cnt2 = cnt;
					color = "#ca83f3";
				}
				if(i==1) {
					$("#oilCnt1").html(cnt+"${_C.label74}");
					cnt1 = cnt;
					color = "#ffc71b"; 
				}
				if(i==2) {
					$("#oilCnt3").html(cnt+"${_C.label74}");
					cnt3 = cnt;
					color = "#3cb3eb"; 
				}
				var param = { title: contentNm, value : cnt,  color: color };
				
				arrData.push(param);
				
				strHtml += "<li style='padding-right:0px;'><table><colgroup><col width='180px'><col width='90px'><col width='70px'><col width='*'></colgroup><tr><td>";
				strHtml += "<img src='${pageContext.request.contextPath}/common/images/${_LANG}/main_icon"+(8+i)+".jpg' alt='' />"+contentNm;
				strHtml += "</td><td>"+number_format(parseInt(distance))+" km</td><td>"+number_format(parseInt(fuelUse))+" ℓ</td><td>￦ "+number_format(parseInt(price))+"</td></tr></table></li>";
									
				
			}
			drawOil(arrData);
			$("#oilUl").html(strHtml);
			
			strHtml ='<td style="padding-left: 18px;">';
			strHtml +="${_C.label75}";
			strHtml +='</td><td style="padding-left: 20px;">';
			strHtml += (number_format(distanceSum) +" km");
			strHtml += '</td><td style="padding-left: 18px;">';
			strHtml += (number_format(fuelSum)+' ℓ');
			strHtml += '</td><td style="padding-left: 17px;">';
			strHtml += ('￦ '+number_format(priceSum)+'</td>');
			$("#oilSum").html(strHtml);
			
			var tCnt = cnt1+cnt2+cnt3;
			var cntRatio = Math.round(cnt1/tCnt*100,2);
			
			$("#oilCnt4").html(cntRatio+"%");
		});
	});
	$(".oilCnt").eq(0).trigger("click");
	
	drawSafeScore();
	
});

var init = function(){
	$("#allChange").sSelect({ddMaxHeight: '300px'}).on("change",function(){
		var v = $(this).val();
		
		$(".oilCnt").eq(v).trigger("click");
		$(".vehicleRank").eq(v).trigger("click");
		$(".scoreChart").eq(v).trigger("click");
		$(".scoreTime").eq(v).trigger("click");
		$(".scoreFee").eq(v).trigger("click");
		$(".userRank").eq(v).trigger("click");
		$(".groupCnt").eq(v).trigger("click");
	});
	
	$("#btn_interval").on("click",function(){
		if("${VDAS.allocationUsed}" == 1) document.location.href = "${defaultPath}//vehicle/vehicleInterval.do";
		else alert("${_C.label19}");
	});
	
	$("#btn_faq").on("click",function(){
		$("#li_noti").hide();
		$("#li_faq").show();
	});
	
	$("#btn_noti").on("click",function(){
		$("#li_noti").show();
		$("#li_faq").hide();
	});
	
	$(".btn_view").on("click",function(){
		var id = $(this).data("id");
		if(id.length == 0) return;
		else{
		var url = _defaultPath+"/helpdesk/boardView.do";
		$("#detailFrm input[name=searchBoardId]").val(id);
		$("#detailFrm").attr("action",url);
		$("#detailFrm").submit();
		}		
	});
	
	$VDAS.http_post("/main/feeSumary.do",{},function(rtv){		
		
		var data = rtv.feeSummary;
		var prev = data[0].fee+${costSummary[0].fee};
		var now = data[1].fee+${costSummary[1].fee};	
		$("#prevFee").html(prev+"${_C.label16}");
		$("#nowFee").html(now+"${_C.label16}");
		
	});
	
}
var drawScore = function(safe,eco){
	$("#doughnutChart3 svg,#doughnutChart3 .doughnutSummary").remove();
	$("#doughnutChart3").drawDoughnutChart(safe,{startLeft:true});
	
	$("#doughnutChart4 svg,#doughnutChart4 .doughnutSummary").remove();
    $("#doughnutChart4").drawDoughnutChart(eco,{startLeft:true});
}
var drawTime = function(data){
	$("#doughnutChart5 svg,#doughnutChart5 .doughnutSummary").remove();
	$("#doughnutChart5").drawDoughnutChart(data);
}
var drawFee = function(data){
	$("#doughnutChart6 svg,#doughnutChart6 .doughnutSummary").remove();
	$("#doughnutChart6").drawDoughnutChart(data,{summaryTitleClass: "doughnutSummaryTitle_${_LANG}"});
}
var drawGroup = function(data){
	$("#doughnutChart7 svg,#doughnutChart7 .doughnutSummary").remove();
	$("#doughnutChart7").drawDoughnutChart(data);
}
var drawOil = function(data){
	$("#doughnutChart8 svg,#doughnutChart8 .doughnutSummary").remove();
	$("#doughnutChart8").drawDoughnutChart(data);
}
var drawFeeHstr = function(){
	
	$VDAS.http_post("/main/adminFeeHstr.do",{},function(rtv){
		var dataset = [];
		var labels = [];
		var max = 5;
		for(var i = 0 ; i < rtv.feeHstrCost.length ; i++){
			var ymd = rtv.feeHstrCost[i].ymd;
			var cost = rtv.feeHstrCost[i].fee+rtv.feeHstrMente[i].fee;
			if(cost > max) max = cost;
			labels.push(parseInt(ymd)+"${_C.label20}");
			dataset.push(cost);
		}
		
		var chartData = {
				node: "graph",
				dataset: dataset,
				labels: labels,
				pathcolor: "#898989",
				fillcolor: "#e81626",
				xPadding: 0,
				yPadding: 20,
				ybreakperiod: parseInt(max/5)
			};
			drawlineChart(chartData);
	});
}

var drawSafeScore = function(){
	
	$VDAS.http_post("/main/drawSafeScoreAll.do",{},function(rtv){
		$("#avgUserSafeScore").text("${_C.label6} "+rtv.rtvList[0].safe+"${_C.label8}");
		$("#avgUserEcoScore").text("${_C.label7} "+rtv.rtvList[0].eco+"${_C.label8}");
	});
}
</script>
<form id="detailFrm" style="display:none" method="post">
	<input type="hidden" name="searchUserId" value="" />	
	<input type="hidden" name="searchVehicleId" value="" />
	<input type="hidden" name="searchBoardId" value="" />
</form>
<div class="main_box1">
	<div class="main_box1_1">
		<img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_img1.jpg" alt="" />
		<a href="https://play.google.com/store/apps/details?id=kr.co.infinityplus.viewcar" target="_blank"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_btn1.jpg" alt="${_C.label21}" /></a>
	</div>
	<div class="main_box1_2">
		<div class="main_box1_3">
			<span><a href="${defaultPath }/driving/searchVehicle.do"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_btn2_1.jpg" alt="" /></a></span><span><a id="btn_interval"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_btn2_2.jpg" alt="" /></a><a href="${defaultPath }/driving/drivingReport.do"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_btn2_3.jpg" alt="" /></a></span>
		</div>
		<ul class="main_box1_4">
			<li>
				<a href="${defaultPath }/vehicle/vehicleCost.do">${_C.label22}</a>
				<a href="${defaultPath }/clinic/vehicleMaintenance.do">${_C.label23}</a>
				<a href="${defaultPath }/analysis/drivingAnalysis.do">${_C.label24}</a>
				<a href="${defaultPath }/driving/searchLocation.do">${_C.label25}</a>
			</li>
			<li id="li_noti">
				<h1>
					${_C.label26}
					<a id="btn_faq"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/btn_notice.jpg" alt="" /></a>
				</h1>
				<div>
					<c:forEach var="vo" items="${notiList}" varStatus="i">
						<a href="#none" class="btn_view" data-id="${vo.id }">${vo.titleShort }</a>
					</c:forEach>
				</div>
			</li>
			<li id="li_faq" style="display:none">
				<h1>
					${_C.label27}
					<a id="btn_noti"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/btn_notice.jpg" alt="" /></a>
				</h1>
				<div>
					<c:forEach var="vo" items="${faqList}" varStatus="i">
						<a href="#none" class="btn_view" data-id="${vo.id }">${vo.titleShort }</a>
					</c:forEach>
				</div>
			</li>
		</ul>
	</div>
</div>














<div style="line-height: 27px;">
	<!-- <span>전체변경 :</span>  -->
	<div class="tab_select">
		<div class="select_type2 select_type2_1">
			<div class="select_type1_1">
				<select id="allChange">
					<option value="0">${_C.label36}</option>
					<option value="1">${_C.label37}</option>
					<option value="2">${_C.label38}</option>
				</select>
			</div>
		</div>
	</div>
</div>

<div class="main_box5">
	<span>${_C.label76 }</span><b id="oilCnt1">0${_C.label74}</b><span>${_C.label77 }</span><b id="oilCnt2">0${_C.label74}</b><span>${_C.label78 }</span><b id="oilCnt3">0${_C.label74}</b><span>${_C.label79}</span><b id="oilCnt4">0%</b>	
</div>
<div class="main_box2 main_box2_5">
	<div class="main_box2_1">
		<h1>
			<img src="${pageContext.request.contextPath}/common/images/${_LANG}/h1_17.jpg" alt="${_C.label72}" />
			<span>
				<a class="oilCnt" data-val="D">${_C.label36}</a><a class="oilCnt" data-val="W">${_C.label37}</a><a class="oilCnt" data-val="M">${_C.label38}</a>
			</span>
		</h1>
		<div class="main_box2_3">
			<div class="chart_layout">
				<div id="doughnutChart8" class="chart">
				</div>
			</div>
			<div class="main_box2_4">
				<table style="width:100%;font-size: 12px;color: #505050;font-weight: 600;border-bottom: 2px solid #c0c0c0;">
					<colgroup>
						<col width="180px">
						<col width="90px">
						<col width="70px">
						<col width="*">
					</colgroup>
						<tr id="oilSum">
							<td style="padding-left: 15px;">
							${_C.label75}
							</td>
							<td style="padding-left: 15px;">
							0 km
							</td>
							<td style="padding-left: 10px;">
							0 ℓ
							</td>
							<td style="padding-left: 7px;">
							￦ 0
							</td>
						</tr>
				</table>
				<ul id="oilUl">
				</ul>
			</div>
		</div>
	</div>
</div>



<div class="main_box5">
	<span>${_C.label28}</span><b>${vehicleTot}${_C.label31}</b>
	<span>${_C.label29}</span><b>${intervalTot}${_C.label31}</b>
	<span>${_C.label30}</span><b>${vehicleTot-intervalTot}${_C.label31}</b>
	<a href="${defaultPath}/config/addVehicleDetail.do"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_btn3.jpg" alt="${_C.label32}" /></a>
</div>
<div class="main_box6">
	<h1>
		<img src="${pageContext.request.contextPath}/common/images/${_LANG}/h1_6.jpg" alt="${_C.label33}" />
		<div class="tab_select">
			<div class="select_type2 select_type2_1">
				<div class="select_type1_1">
					<select class="sel_vehicleRank">
						<option value="T">${_C.label34}</option>
						<option value="D">${_C.label35}</option>
					</select>
				</div>
			</div>
		</div>
		<span>
			<a class="vehicleRank on" data-val="D">${_C.label36}</a><a class="vehicleRank" data-val="W">${_C.label37}</a><a class="vehicleRank" data-val="M">${_C.label38}</a>
		</span>
	</h1>
	<ul class="main_box6_1">
		<li>
			<span class="no">1</span>
			<div id="vehicleRank1">
			</div>
		</li>
		<li>
			<span class="no">2</span>
			<div id="vehicleRank2">
			</div>
		</li>
		<li>
			<span class="no">3</span>
			<div id="vehicleRank3">
			</div>
		</li>
	</ul>
</div>
<div class="main_box2">
	<div class="main_box2_1">
		<h1>
			<img src="${pageContext.request.contextPath}/common/images/${_LANG}/h1_2.jpg" alt="운전지수" />
			<span>
				<a class="scoreChart" data-val="D">${_C.label36}</a><a class="scoreChart" data-val="W">${_C.label37}</a><a class="scoreChart" data-val="M">${_C.label38}</a>
			</span>
		</h1>
		<div class="chart_layout">
			<div id="doughnutChart3" class="chart">
				<div class="main_chart_box1">
					<div class="con">
						<img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_icon1.jpg" alt="" />
						<p id="avgSafeScore">0</p>
						<span>${_C.label39}</span>
						<strong class="c1" id="avgUserSafeScore">${_C.label40} 0${_C.label41}</strong>
					</div>
				</div>
			</div>
			<div id="doughnutChart4" class="chart">
				<div class="main_chart_box1">
					<div class="con">
						<img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_icon2.jpg" alt="" />
						<p id="avgEcoScore">0</p>
						<span>${_C.label42}</span>
						<strong class="c2" id="avgUserEcoScore">${_C.label43} 0${_C.label41}</strong>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="main_box2_2">
		<h1>
			<img src="${pageContext.request.contextPath}/common/images/${_LANG}/h1_3.jpg" alt="${_C.label44}" />
			<span>
				<a class="scoreTime" data-val="D">${_C.label36}</a><a class="scoreTime" data-val="W">${_C.label37}</a><a class="scoreTime" data-val="M">${_C.label38}</a>
			</span>
		</h1>
		<div class="main_box2_3">
			<div class="chart_layout">
				<div id="doughnutChart5" class="chart">
				</div>
			</div>
			<div class="main_box2_4">
				<p>${_C.label44}</p>
				<ul>
					<li>
						<img src="${pageContext.request.contextPath}/common/images/${_LANG}/dot5.jpg" alt="" />${_C.label46}<span id="drivingTime">0 h</span>
					</li>
					<li>
						<img src="${pageContext.request.contextPath}/common/images/${_LANG}/dot6.jpg" alt="" />${_C.label47}<span id="outDrivingTime">0 h</span>
					</li>
					<li>
						<img src="${pageContext.request.contextPath}/common/images/${_LANG}/dot7.jpg" alt="" />${_C.label48}<span id="noneDrivingTime">0 h</span>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="main_box5">
	<span>${_C.label49}</span><b id="prevFee">0${_C.label16}</b><span>${_C.label50}</span><b id="nowFee">0${_C.label16}</b>
	<a href="${defaultPath }/vehicle/vehicleCost.do"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_btn4.jpg" alt="${_C.label51}" /></a>
</div>
<div class="main_box6">
	<h1>
		<img src="${pageContext.request.contextPath}/common/images/${_LANG}/h1_7.jpg" alt="${_C.label52}" />
		<div class="tab_select">
			<div class="select_type2 select_type2_1">
				<div class="select_type1_1">
					<select class="sel_costRank">
						<option value="ALL">${_C.label53}</option>
						<option value="FUEL">${_C.label54}</option>
						<option value="PASS">${_C.label55}</option>
						<option value="PARK">${_C.label56}</option>
						<option value="MAIN">${_C.label57}</option>
					</select>
				</div>
			</div>
		</div>
		<span>
			<a class="scoreCost" data-val="U">${_C.label58}</a><a class="scoreCost" data-val="G">${_C.label59}</a><a class="scoreCost" data-val="V">${_C.label60}</a>
		</span>
	</h1>
	<ul class="main_box6_1 main_box6_2">
		<li>
			<span class="no">1</span>
			<div id="costRank1">
			</div>
		</li>
		<li>
			<span class="no">2</span>
			<div id="costRank2">
			</div>
		</li>
		<li>
			<span class="no">3</span>
			<div id="costRank3">
			</div>
		</li>
	</ul>
</div>
<div class="main_box2 main_box3 mab0">
	<div class="main_box2_1">
		<h1>
			<img src="${pageContext.request.contextPath}/common/images/${_LANG}/h1_4.jpg" alt="${_C.label61}" />
			<span>
				<a class="scoreFee" data-val="D">${_C.label36}</a><a class="scoreFee" data-val="W">${_C.label37}</a><a class="scoreFee" data-val="M">${_C.label38}</a>
			</span>
		</h1>
		<div class="main_box2_3">
			<div class="chart_layout">
				<div id="doughnutChart6" class="chart">
				</div>
			</div>
			<div class="main_box2_4">
				<p>${_C.label62}</p>
				<ul>
					<li>
						<img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_icon4.jpg" alt="" />${_C.label12}<span id="feeFuel">0 ${_C.label16}</span>
					</li>
					<li>
						<img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_icon6.jpg" alt="" />${_C.label13}<span id="feePass">0 ${_C.label16}</span>
					</li>
					<li>
						<img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_icon5.jpg" alt="" />${_C.label14}<span id="feeParking">0 ${_C.label16}</span>
					</li>
					<li>
						<img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_icon7.jpg" alt="" />${_C.label15}<span id="feeMente">0 ${_C.label16}</span>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="main_box2_2">
		<h1>
			<img src="${pageContext.request.contextPath}/common/images/${_LANG}/h1_5.jpg" alt="${_C.label63}" />
			<!-- 
			<span>
				<a href="#" class="on">최근 5개월</a><a href="#">월별</a><a href="#">연별</a>
			</span>
			 -->
		</h1>
		<div class="main_box4">
			<div style="margin:20px auto; text-align:center;"><canvas id="graph" width="449" height="187" align="center"></canvas></div>
		</div>
	</div>
</div>
<div class="main_box5">

	<span>${_C.label64}</span><b>${intervalTot1 }${_C.label65}</b><span>${_C.label69}</span><b>${intervalTot2 } ${_C.label65}</b><span>${_C.label66}</span><b>${intervalTot3 }${_C.label65}</b><span>${_C.label67}</span><b>${intervalTot4 }${_C.label65}</b>
	<a href="${defaultPath }/config/addUserDetail.do"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/images_icon102.jpg" alt="운행경비 조회" /></a>
</div>
<div class="main_box6">
	<h1>
		<img src="${pageContext.request.contextPath}/common/images/${_LANG}/h1_8.jpg" alt="${_C.label69}" />
		<div class="tab_select">
			<div class="select_type2 select_type2_1">
				<div class="select_type1_1">
					<select class="sel_userRank">
						<option value="T">${_C.label70}</option>
						<option value="D">${_C.label71}</option>
					</select>
				</div>
			</div>
		</div>
		<span>
			<a class="userRank on" data-val="D">${_C.label36}</a><a class="userRank" data-val="W">${_C.label37}</a><a class="userRank" data-val="M">${_C.label38}</a>
		</span>
	</h1>
	<ul class="main_box6_1 main_box6_2">
		<li>
			<span class="no">1</span>
			<div id="userRank1">
			</div>
		</li>
		<li>
			<span class="no">2</span>
			<div id="userRank2">
			</div>
		</li>
		<li>
			<span class="no">3</span>
			<div id="userRank3">
			</div>
		</li>
	</ul>
</div>
<div class="main_box2 main_box2_5">
	<div class="main_box2_1">
		<h1>
			<img src="${pageContext.request.contextPath}/common/images/${_LANG}/h1_9.jpg" alt="${_C.label72}" />
			<span>
				<a class="groupCnt" data-val="D">${_C.label36}</a><a class="groupCnt" data-val="W">${_C.label37}</a><a class="groupCnt" data-val="M">${_C.label38}</a>
			</span>
		</h1>
		<div class="main_box2_3">
			<div class="chart_layout">
				<div id="doughnutChart7" class="chart">
				</div>
			</div>
			<div class="main_box2_4">
				<p>${_C.label73}<a href="${defaultPath }/config/addGroup.do"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_btn5.jpg" alt="" /></a></p>
				<ul id="groupCntUl">
				</ul>
			</div>
		</div>
	</div>
</div>






