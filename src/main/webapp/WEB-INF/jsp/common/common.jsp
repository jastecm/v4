<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    

<c:set var="defaultPath" value="${pageContext.request.contextPath}" />
<c:set var="path" value="${pageContext.request.contextPath}" />
<c:set var="pathCommon" value="${pageContext.request.contextPath}/common" />
<c:set var="_LANG" value="ko" />

<script type="text/javascript">
var _defaultPath = "${defaultPath}";
var _contextPath = "${pageContext.request.contextPath}";
var _lang = "${_LANG}";

var _timezoneOffset = "${V4.timezoneOffset}";
var _unitDate = "${V4.unitDate}";
var _unitLength = "${V4.unitLength}";
var _unitVolume = "${V4.unitVolume}";
var _unitTemperature = "${V4.unitTemperature}";
var _unitWeight = "${V4.unitWeight}";
</script>

