<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>


<script type="text/javascript">
<c:if test="${VDAS.rule ne '0' }">
window.location.href = "${defaultPath}/main/index.do";
</c:if>
var lazy;
$(document).ready(function(){
	initMenuSel("M9001");
	
	$('#frm').ajaxForm({
		beforeSubmit: function (data,form,option) {
	    	return true;
		}
		,success: function(res,status){
			if(res.rtv && res.rtv == "S"){
				alert("success");
			}else{
				if(res.MSG) alert(res.MSG);
				else alert("fail");
			}	
		},
		error: function(){
	    	alert("ajax error");
		}                               
	});
	$("#frm").submit(function(){return false});
	
	$("#send").on("click",function(){
		$('#frm').submit();
	});
});

</script>
<div class="right_layout">
	<form id="frm" action="${defaultPath}/admin/saveFirmUpload.do" method="post" enctype="multipart/form-data">
		<select id="deviceSeries" name="deviceSeries">
			<option value="von-S31">von-S31</option>
		</select>
		<input type="text" id="version" name="version" value="" style="border:1px solid;width:50px;height: 25px;" />
		<input type="file" name="file" />
	</form>
	<button id="send" style="width:55px;height:25px;">보내기</button>
</div>