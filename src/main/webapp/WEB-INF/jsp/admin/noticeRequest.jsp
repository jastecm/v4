<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script src='${pageContext.request.contextPath}/common/js/jquery.MultiFile.js' type="text/javascript" language="javascript"></script>
<script type="text/javascript">
var lazy;

$(document).ready(function(){
	
	$("#searchType").on("change",function(){
		$("#searchFrm input[name=contentsType]").val($(this).val());
		$.lazyLoader.search(lazy);
	});
	
	$("body").on("click",".btn_view",function(){
		var boardkey = $(this).data("boardkey");
		if(boardkey.length == 0) return;
		else{
			
			$VDAS.http_post("/helpdesk/board/1/"+boardkey,{},{
				success : function(r){
					var vo = r.result;
					clearContents();
					clearFileFeild();
					
					console.log(r.result);
					var boardKey = vo.boardKey;
					var title = vo.title;
					var serviceType = vo.serviceType;
					var contentType = vo.contentType;
					var contents = vo.contents;
					var fileId = vo.fileId;
					var orgFileNm = vo.orgFileNm
					
					$("#frm input[name=boardKey]").val(boardKey);
					$("#frm input[name=title]").val(title);
					$("#targetCorp").val(serviceType);
					$("#contentType").val(contentType);
					clearContents();
					setContents(contents);
					
					if(fileId) {
						$("fieldset").remove();
						var strHtml = "<a id='file' href='${defaultPath}/com/getFile.do?uuid="+fileId+"' style='line-height:27px;'>&nbsp;"+orgFileNm+"&nbsp;&nbsp;</a><a id='delFile'><img src='${pageContext.request.contextPath}/common/images/file_delete.jpg' alt='' /></a><input id='fileId' type='hidden' name='fileId' >";
						$("#before_file").after(strHtml);
					}else{
						clearFileFeild();
					}
					
					
				}
			});
		}		
	});
	
	if(!lazy)
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
				
			}
			,scrollRow : 5
			,rowHeight : 70
			,limit : 10			
			,loadUrl : "/helpdesk/boardAll/1"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.result;				
				var strHtml = '';
				
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += '<tr>';
					strHtml += '<td><a class="btn8 btn_del" data-boardkey="'+vo.boardKey+'"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/btn_delete.jpg" alt="" /></a></td>';					
					strHtml += '<td>'+vo.contentTypeNm+'</td>';					
					strHtml += '<th><a href="#none" class="btn_view" data-boardkey="'+vo.boardKey+'">'+vo.title+'</a></th>';					
					strHtml += '<td>'+vo.regDate.substr(0,10)+'</td>';					
					var str = "";
					if(vo.serviceType == '1') str = "Web";
					else if(vo.serviceType == '2') str = "App";
					else str = "전체";
					strHtml += '<td>'+str+'</td>';			
					
					strHtml += '</tr>';
				}
				
				return strHtml;

			}
		});
	
	$('#frm').ajaxForm({
		beforeSubmit: function (data,form,option) {
			var returnVal = submitContents();
			$("#editor1").val(returnVal);
			for(var i = 0 ; i < data.length ; i++){
				if(data[i].name=="contents") data[i].value=returnVal;
			}
	    	return true;
		}
		,success: function(res,status){
	    	
	    	if(res.rtvCd == "1"){
	    		alert("저장되었습니다.");
	    		$("#btn_cancle").trigger("click");
	    		$.lazyLoader.search(lazy);
	    	}else{
	    		alert("error");
	    	}
		},
		error: function(){
	    	//에러발생을 위한 code페이지
		}                               
	});
	
	$("#frm").submit(function(){return false});
	
	$("#btn_save").on("click",function(){
		$('#frm').submit();
	});
	
	
	$("#frm").on("click","#delFile",function(){
		clearFileFeild();
	});
	
	$("body").on("click",".btn_del",function(){
		var $this_ = $(this);
		if(confirm("정말 삭제하시겠습니까?")){
			var boardKey = $(this).data("boardkey");
			if(boardKey.length == 0) return;
			else{
				$VDAS.http_post("/admin/boardDel.do",{boardKey:boardKey},{
					success :function(r){
						if(r.rtvCd == "1") {
							alert("삭제되었습니다.");
							$.lazyLoader.search(lazy);
							$("#btn_cancle").trigger("click");						
						}else{
							alert("error");
						}
					}
				});
			}	
		}
			
	});
	
	$("#btn_cancle").on("click",function(){
		$("#frm input[name=boardKey]").val("");
		$("#frm input[name=title]").val("");
		$("#targetCorp").val("");
		$("#contentType").val("1");
		clearContents();
		clearFileFeild();
	});
});


function clearFileFeild(){
	$("#file").remove();
	$("#delFile").remove();
	$("#fileId").remove();
	$("fieldset").remove();
	
	var strHtml = "<fieldset style='border:0'><div class='btn_file'><input type='file' name='fileId' class='multi' maxlength='1'/></div></fieldset>";
	
	$("#before_file").after(strHtml);
	$("input[type=file].multi").MultiFile();
}





</script>
<div class="right_layout">
	<div class="board_layout pat0">
		<div class="detail_view">
			<a href="#none" class="btn_close btn_x"><img src="${pageContext.request.contextPath}/common/images/close.jpg" alt=""></a>
			<h2 class="h2tit">공지사항 등록</h2>
			<form id="frm" method="POST" action="${defaultPath }/admin/noticeSave.do" enctype="multipart/form-data">
				<input type="hidden" name="boardKey"/>
				<div class="sub_top2">
					공지사항을 등록합니다.
				</div>
				<div class="repair_box2_1 pal25 borB1">
	            	<span class="tit">제목</span>
	               	<input type="text" name="title" style="width:600px;"/><br><br>
	            </div>
	            <div class="repair_box2_1 pal25 borB1">
	            	<span class="tit">공개설정</span>
					<div class="select_type6" style="width:400px;padding-bottom: 8px;">
						<div style="display:inline-block;" class="select_type6_a">
							<select class="select form" name="targetCorp" id="targetCorp" style="   vertical-align: baseline;">
								<option value="">전체</option>
								<c:forEach var="vo" items="${targetCorp}" varStatus="i">
									<option value="${vo.code }"  >${vo.codeName }</option>
								</c:forEach>
							</select>
						</div>						
						<span class="" style="display:inline-block;vertical-align:top;line-height:27px;font-size:12px;font-weight:600;width:35px;padding-left: 15px;">분류</span>
						<div style="display:inline-block;" class="select_type6_a">
							<select class="select form" name="contentType" id="contentType" style="   vertical-align: baseline;">
								<c:forEach var="vo" items="${contentsType}" varStatus="i">
									<option value="${vo.code }"  >${vo.codeName }</option>
								</c:forEach>
							</select>
						</div>
						<input type="radio" name="lang" value="ko" <c:if test="${_LANG eq 'ko'}">checked</c:if>/>한글					
						<input type="radio" name="lang" value="en" <c:if test="${_LANG ne 'ko'}">checked</c:if>/>영문
					</div>
	            </div>              
				<div class="bo_textarea">
					<c:import url="/editor/smartEditor.jsp">
						<c:param name="contents" value=""></c:param>
					</c:import>
				</div>
				<div class="car_info4 car_info5_3">
					<span class="tit">&nbsp;&nbsp;첨부파일</span><div class="car_info4_1">
						<div class="car_info4_2">
							<span class="tit3" id="before_file">파일추가</span><fieldset style="border:0">
								<div class="btn_file"><input type="file" name="fileId" class="multi" maxlength="1"/></div>
							</fieldset>
						</div>
					</div>
				</div>
			</form>
			<div class="btn_area2"><a href="#none" id="btn_cancle" class="btn_cancle">취소</a><a href="#none" id="btn_save">확인</a></div>
		</div>
	</div>
	<div class="board_layout">
		<form id="searchFrm">
			<input type="hidden" name="boardType" value="1" />
			<input type="hidden" name="contentsType" value="" />
		</form>
		<h3 class="h3tit">공지사항 목록</h3>
		<table class="table2" id="contentsTable">
			<colgroup>
				<col width="65">
				<col width="143">
				<col width="*">
				<col width="112">
				<col width="80">
			</colgroup>
			<thead>
				<tr>
					<th>No.</th>
					<th>
						<select class="select form" id="searchType">
							<option value="">분류</option>
							<c:forEach var="vo" items="${contentsType }">
								<option value="${vo.code}">${vo.codeName }</option>	
							</c:forEach>																	
						</select>
					</th>
					<th>제목</th>
					<th>작성일</th>
					<th>타입</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>