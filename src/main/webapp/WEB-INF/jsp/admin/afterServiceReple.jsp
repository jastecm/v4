<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script src='${pageContext.request.contextPath}/common/js/jquery.MultiFile.js' type="text/javascript" language="javascript"></script>
<script type="text/javascript">
var lazy;

$(document).ready(function(){
	
	
	$("body").on("click",".btn_view",function(){
		var boardkey = $(this).data("boardkey");
		if(boardkey.length == 0) return;
		else{
			
			$VDAS.http_post("/helpdesk/board/4/"+boardkey,{},{
				success : function(r){
					var vo = r.result;
					clearContents();
					
					console.log(r.result);
					var boardKey = vo.boardKey;
					var title = vo.title;
					var contents = vo.contents;
					
					$("#frm input[name=boardKey]").val(boardKey);
					$("#frm input[name=title]").val(title);
					clearContents();
					setContents(contents);
					
					
					
				}
			});
		}		
	});
	
	$("body").on("click",".btn_reple",function(){
		var id = $(this).data("boardkey");
		if(id.length == 0) return;
		else{
			
			$VDAS.http_post("/helpdesk/board/4/"+id,{},{
				success : function(r){
					var vo = r.result;
					console.log(r.result);
					$( "#pop" ).attr("title",vo.asDeviceSeries+" / "+vo.asDeviceSn+" / "+vo.asVehiclePlateNumber);
					$("#pop .contents").html(vo.contents);
					
					if(vo.contentsReple&&vo.contentsReple.length != 0)
						$("#pop .contentsReple").html(vo.contentsReple).show();
					else $("#pop .contentsReple").html("").hide();
					
					if(vo.fileId){
						$("#pop .info").html('첨부파일 : <a href="${defaultPath}/com/getFile.do?uuid='+vo.fileId+'">'+vo.orgFileNm+'</a>').show();
					}else{
						$("#pop .info").html("").hide();
					}
					
					
					$( "#pop" ).dialog({
				        width:1000,
				        dialogClass:'alert',
				        draggable: false,
				        modal:true,
				        height:"auto",
				        resizable:false,
				        closeOnEscape:true,
				        buttons:{
				        	'확인':function(){
				     			$(this).dialog("destroy");
				     		}
				        }
				    });
				}
			});
		}			
	});
	
	if(!lazy)
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
			}
			,scrollRow : 5
			,rowHeight : 70
			,limit : 10			
			,loadUrl : "/helpdesk/board/4"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.result;				
				var strHtml = '';
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += '<tr>';
					strHtml += '<td><a href="#none" class="btn_view" data-boardkey="'+vo.boardKey+'">'+vo.asDeviceSeries+'</a></td>';
					strHtml += '<td><a href="#none" class="btn_view" data-boardkey="'+vo.boardKey+'">'+vo.asDeviceSn+'</a></td>';
					strHtml += '<td><a href="#none" class="btn_view" data-boardkey="'+vo.boardKey+'">'+vo.asVehiclePlateNumber+'</a></td>';
					strHtml += '<td>';
					if(vo.fileId)
						strHtml += '<a id="file" href="${defaultPath}/com/getFile.do?uuid='+vo.fileId+'" style="line-height:27px;">'+"디스크이미지"+'</a>';
					strHtml += '</td>';
					strHtml += '<td>'+vo.regDate.substr(0,10)+'</td>';
					var strState = "";
					if(vo.contentType == '4') strState = "re_ok";
					else strState = "re_ready";
					strHtml += '<td><a href="#none" class="btn_reple" data-boardkey="'+vo.boardKey+'"><span class="'+strState+'">'+vo.contentTypeNm+'</span></a></td>';
					strHtml += '</tr>';
				}
				
				return strHtml;
				
				
				

			}
		});
	
	$('#frm').ajaxForm({
		beforeSubmit: function (data,form,option) {
			var returnVal = submitContents();
			$("#editor1").val(returnVal);
			for(var i = 0 ; i < data.length ; i++){
				if(data[i].name=="contents") data[i].value=returnVal;
			}
	    	return true;
		}
		,success: function(res,status){
	    	
	    	if(res.rtvCd == "1"){
	    		alert("저장되었습니다.");
	    		$("#btn_cancle").trigger("click");
	    		$.lazyLoader.search(lazy);
	    	}else{
	    		alert("error");
	    	}
		},
		error: function(){
	    	//에러발생을 위한 code페이지
		}                               
	});
	
	$("#frm").submit(function(){return false});
	
	$("#btn_save").on("click",function(){
		$('#frm').submit();
	});
	
	
	
	$("#btn_cancle").on("click",function(){
		$("#frm input[name=boardKey]").val("");
		$("#frm input[name=title]").val("");
		$("#targetCorp").val("");
		$("#contentType").val("3");
		clearContents();
	});
});






</script>
<div class="right_layout">
	<div class="board_layout pat0">
		<div class="detail_view">
			<a href="#none" class="btn_close btn_x"><img src="${pageContext.request.contextPath}/common/images/close.jpg" alt=""></a>
			<h2 class="h2tit">A/S 답변</h2>
			<form id="frm" method="POST" action="${defaultPath }/admin/afterServiceRepleSave.do" enctype="multipart/form-data">
				<input type="hidden" name="boardKey"/>
				<div class="sub_top2">
					A/S 답변
				</div>
				<div class="repair_box2_1 pal25 borB1">
	            	<span class="tit">제목</span>
	               	<input type="text" name="title" style="width:600px;"/><br><br>
	            </div>
				<div class="bo_textarea">
					<c:import url="/editor/smartEditor.jsp">
						<c:param name="contents" value=""></c:param>
					</c:import>
				</div>
			</form>
			<div class="btn_area2"><a href="#none" id="btn_cancle" class="btn_cancle">취소</a><a href="#none" id="btn_save">확인</a></div>
		</div>
	</div>
	<div class="board_layout">
		<form id="searchFrm">
			<input type="hidden" name="boardType" value="4" />
			<input type="hidden" name="contentsType" value="" />
			<input type="hidden" name="serviceType" value="" />
		</form>
		<h3 class="h3tit">A/S 목록</h3>
		<table class="table2" id="contentsTable">
			<colgroup>
				<col width="150">
				<col width="150">
				<col width="*">
				<col width="80">
				<col width="112">
				<col width="80">
			</colgroup>
			<thead>
				<tr>
					<th>단말타입</th>
					<th>시리얼</th>
					<th>차량번호</th>
					<th>파일</th>
					<th>작성일</th>
					<th>답변</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<div id="pop" title="" class="myPopup" style="display: none">
	<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt=""></a>
	<div class="info"></div>		
	<div class="contents"></div>
	<div class="contentsReple"></div>
</div>
<style>
#pop .info{width: 100%;height: 30px;border-bottom: 1px solid #747474;margin-bottom: 20px;display:none}
#pop .contents p{line-height: 18px; font-size: 13px;} 
#pop .contents div{line-height: 18px; font-size: 13px;} 
#pop .contentsReple{border-top: 1px solid #747474;display:none;padding-top:20px;} 
#pop .contentsReple div{line-height: 18px; font-size: 13px} 
#pop .contentsReple p{line-height: 18px; font-size: 13px} 
</style>