<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	$(".tabItem").on("click",function(){
		$(".tabItem").removeClass("on");
		$(this).addClass("on");
		
		$(".table1").hide();
		var tab = $(this).data("tab");
		searchKey = tab;
		$("."+tab).show();
		
	});
});
</script>
<form id="searchFrm" style="display:none">
	<input type="hidden" name="searchType" value="" />
	<input type="hidden" name="searchText" value="" />
	<input type="hidden" name="searchOrder" value="" />
	<input type="hidden" name="searchdrivingState" value="" />
	<input type="hidden" name="searchState" value="" />
	<input type="hidden" name="searchUsed" value="" />
</form>
<form id="searchDtcFrm" style="display:none;">
	<input type="hidden" name="dtcKey" value="" />
</form>
<div class="right_layout">
	<h2 class="tit_sub">
		법인정보
	</h2>
	<div class="sub_top1">
	</div>
	
	<div class="tab_layout">
		<div class="pageTab">
			<ul class="tab_nav">
				<li class="tabItem on" data-tab='corp'><a href="#none" >법인</a></li>
				<li class='tabItem' data-tab='master'><a href="#none" >관리자</a></li>
			</ul>
		</div>
		
		<table class="table1 corp" id="contentsTable" >
			<colgroup>
				<col style="width:200px;" />
				<col style="width:120px;" />
				<col />
			</colgroup>
			<thead>
				<tr>
					<th>법인명</th>
					<th>도메인</th>
					<th>apiKey</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="vo" items="${corpList}" varStatus="i">
					<tr>
						<td>
							${vo.corpName }
						</td>
						<td>
							${vo.corpDomain }
						</td>
						<td>
							${vo.apiKey }
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<table class="table1 master" id="contentsTable2" style="display:none">
			<colgroup>
				<col style="width:200px;" />
				<col style="width:270px;" />
				<col style="width:120px;" />
				<col />
			</colgroup>
			<thead>
				<tr>
					<th>법인명</th>
					<th>도메인</th>
					<th>아이디</th>
					<th>비번</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="vo" items="${masterList}" varStatus="i">
					<tr>
						<td>
							${vo.corpName }
						</td>
						<td>
							${vo.corpDomain }
						</td>
						<td>
							${vo.userId }
						</td>
						<td>
							${vo.userPw }
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>
