<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>


<script type="text/javascript">
<c:if test="${VDAS.rule ne '0' }">
window.location.href = "${defaultPath}/main/index.do";
</c:if>
var lazy;
$(document).ready(function(){
	initMenuSel("M9001");
	
	$("#send").on("click",function(){
		$VDAS.http_post("/admin/sendNotiPush.do",{title:$("#title").val(),msg:$("#msg").val()},{
			success : function(r){
				alert("성공");
			},error : function(r){
				alert("실패");
			}
		});
	});
	
	
});

</script>
<div class="right_layout">
	<input type="text" id="title" value="" style="border:1px solid;width:200px;height: 25px;" />
	<input type="text" id="msg" value="" style="border:1px solid;width:500px;height: 25px;" />
	<form id="frm" action="${defaultPath}/admin/sendingPushMessage.do" method="post" enctype="multipart/form-data">
		<!-- <div class="btn_file"><input type="file" name="img" accept="jpg|gif|png" maxlength="1"/></div> -->
	</form>
	<button id="send" style="width:55px;height:25px;">보내기</button>
</div>