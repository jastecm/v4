<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<c:if test="${VDAS.rule eq '0' or VDAS.rule eq '1' }">
<script type="text/javascript">
var lazy;
$(document).ready(function(){
	//left menu on
	initMenuSel("M0005");

	$(".btn_search").on("click",function(){
		if($("#searchType").val() == null || $("#searchType").val() == '')
			alert("검색할 항목을 선택해 주세요.")
		else $.lazyLoader.search(lazy);
	});

	$("#searchText").on("keyup",function(e){
		if(e.keyCode == 13) $(".btn_search").trigger("click");
	});
	
	$("#allChker").on("click",function(){
		if($(this).is(":checked"))
			$(".selObj","#contentsTable tbody").prop("checked",true);
		else $(".selObj","#contentsTable tbody").prop("checked",false);
	});
	
	totCntRefresh();
	
	if(!lazy)
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
				searchType : $("#searchType")
				,searchText : $("#searchText")
			}
			,scrollRow : 5
			,rowHeight : 40
			,limit : 10
			,loadUrl : "/config/getWorkingTime.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.result;
				console.log(data);
				var strHtml = "";
				for(var i = 0 ; i < data.length ; i++){
					
					var vo = data[i];
					strHtml += "<tr>";
					strHtml += '<td><input type="checkbox" class="selObj" data-key="'+vo.workTimeKey+'"/></td>';
					strHtml += '<th style="padding-left:0px;"><b>'+(vo.corpGroupNm?vo.corpGroupNm:"")+'</b></th>';
					strHtml += '<td>'+(vo.accountNm?vo.accountNm:"")+(vo.corpPosition?"/"+vo.corpPosition:"")+'</td>';
					strHtml += '<td>'+(vo.dayMon?"&#10003;":"")+'</td>';
					strHtml += '<td>'+(vo.dayTue?"&#10003;":"")+'</td>';
					strHtml += '<td>'+(vo.dayWed?"&#10003;":"")+'</td>';
					strHtml += '<td>'+(vo.dayThu?"&#10003;":"")+'</td>';
					strHtml += '<td>'+(vo.dayFri?"&#10003;":"")+'</td>';
					strHtml += '<td>'+(vo.daySat?"&#10003;":"")+'</td>';
					strHtml += '<td>'+(vo.daySun?"&#10003;":"")+'</td>';
					strHtml += '<td>'+LPAD(vo.startH,'0',2)+':'+LPAD(vo.startM,'0',2)+'~'+LPAD(vo.endH,'0',2)+':'+LPAD(vo.endM,'0',2)+'</td>';
					strHtml += '<td><a href="#none" class="btn_moveDetail" data-key="'+vo.workTimeKey+'"><img src="${pathCommon}/images/btn3.jpg" alt=""></a></td>';
					strHtml += '</tr>';	
				}
				
				return strHtml;
			}
		});

	
	$("body").on("click",".btn_moveDetail",function(){
		var key = $(this).data("key");
		
		$VDAS.instance_post("/config/registWorkingTimeDetail.do",{workTimeKey:key});
		
	});
	
	$(".btn_del").on("click",function(){
		var selObj = $(".selObj:checked");

		if(selObj.length == 0){
			alert("삭제할 업무시간을 선택해주세요.");
			return;
		}
		
		$( "#dialog" ).dialog({
	        width:800,
	        dialogClass:'alert',
	        draggable: false,
	        modal:true,
	        height:"auto",
	        resizable:false,
	        closeOnEscape:true,
	        buttons:{
	            '확인':function(){
	            	
	            	var strKey = "";
	            	$(".selObj:checked").each(function(){
	            		strKey += strKey==""?$(this).data("key"):","+$(this).data("key");
	            	});
	            	
	            	$VDAS.http_post("/config/deleteWorkingTime.do",{workTimeKey:strKey},{
	            		success : function(r){
	            			alert("처리되었습니다.");
	            			$("#dialog").dialog("close");
	            			totCntRefresh();
	            			$.lazyLoader.search(lazy);
	            		}
	            	});
	            	
	               
	            },'취소':function(){
	            	$(this).dialog("close");
	            }
	        }
	    });
	});
	/*
	$(".btn_moveDetail").on("click",function(){
		var id = $(this).data("id");
		
		$("#detailFrm input[name=searchWorkingId]").val(id);
		$("#detailFrm").attr("action","${defaultPath}/config/workingTimeDetail.do");
		$("#detailFrm").submit();
	});
	
	$(".btn_del").on("click",function(){
		var t = $("#contents input[type=checkbox]:checked");
		if(t.length == 0){
			alert("${_C.label1}");					
		}else{			
			var p = "";
			t.each(function(){
				var v = $(this).data("id");
				if(p.length == 0) p = v;
				else p += ","+v;
			});
			
			var url = _defaultPath+"/config/workingTimeDel.do";
			
			$.ajax({
				type : 'POST',
				dataType : 'json',
				url : url,
				contentType: "application/x-www-form-urlencoded; charset=UTF-8",
				data : {id : p},
				success: function(result){
					if(result.rtvCd == "0"){
						if(result.MSG!="") alert(result.MSG);
						else alert("error");
					}else{
						alert("${_C.label2}");
						document.location.href = _defaultPath+"/config/workingTime.do";
					}
				},
				error:function(result){
				}
			});
			
		}
	});
	*/
	
	<%-- <tr>
	<td><span class="check_box check_box_1"><input type="checkbox" class="checkboxes" data-id="${vo.id }"/></td>
	<th><b>${vo.corpGroupNm }</b></th>
	<td><c:if test="${!empty vo.userNm}">${vo.userNm }</c:if></td>
	<td><c:if test="${vo.dayMon eq '1'}">&#10003;</c:if></td>
	<td><c:if test="${vo.dayTue eq '1'}">&#10003;</c:if></td>
	<td><c:if test="${vo.dayWed eq '1'}">&#10003;</c:if></td>
	<td><c:if test="${vo.dayThu eq '1'}">&#10003;</c:if></td>
	<td><c:if test="${vo.dayFri eq '1'}">&#10003;</c:if></td>
	<td><c:if test="${vo.daySat eq '1'}">&#10003;</c:if></td>
	<td><c:if test="${vo.daySun eq '1'}">&#10003;</c:if></td>
	<td>${vo.startH}:${vo.startM} ~ ${vo.endH}:${vo.endM}</td>
	<td><a href="#none" data-id="${vo.id }" class="btn_moveDetail"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/btn3.jpg" alt=""></a></td>
</tr> --%>
});
function totCntRefresh(){
	$VDAS.http_post("/config/getWorkingTimeTotCnt.do",{},{
		success : function(r){
			$("#totCnt").text(r.result + " 건");
		}
	});
}

</script>
<form id="searchFrm" style="display:none" method="post">
	<input type="hidden" name="searchType" value="" />
	<input type="hidden" name="searchText" value="" />
</form>
<div class="right_layout">
	<h2 class="tit_sub">
		업무시간 설정
		<p>소속직원 전체 또는 부서별, 직원별 업무시간을 설정하실 수 있습니다.</p>
	</h2>
	<div class="sub_top1">
		<span>업무시간 설정건<b id="totCnt"></b></span>
		<strong id="nowDate"></strong>
	</div>
	<h3 class="h3tit">업무시간 등록</h3>
	<div class="box1">
		기본업무시간은 09:00~18:00이며, 운행기록부의 기준데이터로 설정되니 정확히 입력해주세요.
		<a href="${defaultPath}/config/registWorkingTimeDetail.do">업무시간 등록</a>
	</div>
	<h3 class="h3tit">업무시간 조회</h3>
	<div class="search_box1">
		<form id="frmSearch" method="post" onSubmit="return false;">
			<div class="search_box1_1">
				<div class="select_type1">
					<div class="select_type1_1">
						<select id="searchType" class="sel">
							<option value="">선택</option>
							<option value="corpGroupNm">부서명</option>
							<option value="accountNm">사용자명</option>
						</select>
					</div>
				</div><input type="text" id="searchText" class="search_input" value="" placeholder="검색할 항목을 먼저 선택해주세요."/>
			</div><a href="#none" class="btn_search">조회</a>
		</form>
	</div>
	<div class="tab_layout">
		<div class="align_right mgt30">
			<a href="#none" class="btn_del"><img src="${pathCommon}/images/${_LANG}/btn_delete.jpg" alt=""></a>
		</div>
		<table class="table2 mgt10" id="contentsTable">
			<colgroup>
				<col width="65">
				<col width="100">
				<col width="*">
				<col width="35">
				<col width="35">
				<col width="35">
				<col width="35">
				<col width="35">
				<col width="35">
				<col width="35">
				<col width="155">
				<col width="75">
			</colgroup>
			<thead>
				<tr>
					<th>
						<input type="checkbox" class="selObj" id="allChker" />
					</th>
					<th>부서</th>
					<th>사용자명</th>
					<th>월</th>
					<th>화</th>
					<th>수</th>
					<th>목</th>
					<th>금</th>
					<th>토</th>
					<th>일</th>
					<th>업무시간</th>
					<th>등록확인</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<div id="dialog" title="업무시간 삭제" class="myPopup" style="display:none">
	<div class="pop_close"></div>
	<p class="pop_txt">
		선택된 업무시간을 삭제 하시겠습니까?		
	</p>
</div>
</c:if>
<c:if test="${VDAS.rule ne '0' and VDAS.rule ne '1' }">
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("M0005");
});
</script>
<div class="right_layout">
	<img src="${pathCommon}/images/${_LANG}/authError.png" alt="" style="width:100%">
</div>
</c:if>