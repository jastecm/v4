<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<c:if test="${VDAS.rule eq '0' or VDAS.rule eq '1' }">
<style>.svg-clipped{-webkit-clip-path:url(#svgPath);clip-path:url(#svgPath);}</style>
<svg height="0" width="0">
	<defs>
		<clipPath id="svgPath" clipPathUnits="objectBoundingBox">
  			<circle cx=".5" cy=".5" r=".5" />
   		</clipPath>
	</defs>
</svg>
<script type="text/javascript">
var lazy;
var lazy_userPlus;
var lazy_userMinus;
var lazy_vehiclePlus;
var lazy_vehicleMinus;
$(document).ready(function(){
	
	//left menu on
	initMenuSel("M0003");
	
	if(!lazy)
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {}
			,scrollRow : 5
			,rowHeight : 87
			
			,limit : 10
			,loadUrl : "/com/getGroupList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.rtvList;
				var strHtml = "";
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					
					
					
					strHtml += "<tr>";
					
					strHtml += '<td style="padding:10px 0;"><input type="checkbox" name="selObj" class="selObj" data-key="'+vo.groupKey+'" data-groupnm="'+vo.corpGroupNm+'" /></td>';
					strHtml += '<td style="padding:10px 0;">'+vo.rNum+'</td>';
					strHtml += '<td style="padding:10px 0;" id="group'+vo.groupKey+'">'+vo.memberCnt+'</td>';
					strHtml += '<td style="padding:10px 0;"><a href="#none" class="btn_plus_user" data-key="'+vo.groupKey+'"><img src="${pathCommon}/images/btn_puls.jpg" ></a>&nbsp;<a href="#none" class="btn_minus_user" data-key="'+vo.groupKey+'"><img src="${pathCommon}/images/btn_minus.jpg" ></a></td>';
					strHtml += '<td style="padding:10px 0;" id="group'+vo.groupKey+'">'+vo.vehicleCnt+'</td>';
					strHtml += '<td style="padding:10px 0;"><a href="#none" class="btn_plus_vehicle" data-key="'+vo.groupKey+'"><img src="${pathCommon}/images/btn_puls.jpg" ></a>&nbsp;<a href="#none" class="btn_minus_vehicle" data-key="'+vo.groupKey+'"><img src="${pathCommon}/images/btn_minus.jpg" ></a></td>';
					strHtml += '<th style="padding:10px 0;" class="user_tit"><span class="txt_gorupNm">'+vo.corpGroupNm+'</span><input type="text" class="input_gorupNm input151w" name="corpGroupNm" style="display:none" value=""><a href="#none" class="btn8 btn_edit" style="display:none" data-key="'+vo.groupKey+'"><img src="${pathCommon}/images/${_LANG}/btn8.jpg" alt="" /></a></th>';
					strHtml += '<td style="padding:10px 0;">'+vo.regDate+'</td>';
					strHtml += "</tr>";
				}
				
				
				return strHtml;
			}
		});
	
	$("#btn_mod").on("click",function(){
		var chk = $("#contentsTable tbody").find("input[type=checkbox]:checked");
		
		$(chk).each(function(){
			var nm = $(this).data("groupnm");
			$(this).closest("tr").find(".input_gorupNm").val(nm);
			$(this).closest("tr").find(".txt_gorupNm").hide();
			$(this).closest("tr").find(".input_gorupNm,.btn_edit").show();			
			
		});
		
		$("#contentsTable").find("input[type=checkbox]").attr("checked",false);
		
	});
	
	$("#btn_add").on("click",function(){
		var nm = $("#groupNm").val();
		$VDAS.http_post("/group/addGroup.do",{corpGroupNm:nm},{
			success:function(r){
				alert("저장되었습니다.");
				document.location.reload();
			}
		});		
	});
	
	$("#all_checked").on("click",function(){
		if($(this).is(":checked")){
			$("#contentsTable tbody input[type=checkbox]").prop("checked",true);	
		}else
			$("#contentsTable tbody input[type=checkbox]").prop("checked",false);
	});
	
	$("#contentsTable").on("click",".btn_edit",function(){		
		var _$this = $(this);
		var key = _$this.data("key");
		var nm = _$this.closest("tr").find(".input_gorupNm").val();
		
		$VDAS.http_post("/group/updateGroupNm.do",{groupKey:key,corpGroupNm:nm},{
			success:function(r){
				alert("저장되었습니다.");
				var tr = _$this.closest("tr");
				tr.find("input[type=checkbox]").data("groupnm",nm);
				tr.find(".input_gorupNm,.btn_edit").hide();				
				tr.find(".txt_gorupNm").text(nm).show();
			}
		});		
		
	});
	
	$("#btn_del").on("click",function(){
		
		var objs = $(".selObj:checked");
		if(objs.length == 0){
			alert("삭제할 부서를 선택해주세요.");
			return false;
		}else{
			var strKeys = "";
			objs.each(function(){
				var key = $(this).data("key");
				if(strKeys.length == 0 ) strKeys = key;
				else strKeys += ","+key;
				
			});
			
			$VDAS.http_post("/group/deleteGroupNm.do",{groupKey:strKeys},{
				success:function(r){
					alert("저장되었습니다.");
					document.location.reload();
				}
			});	
		}
		
		
		
			
	});
	
	$(".btn_searchUserPop").on("click",function(e){
		var e = $(this).data("e");
		
		var target = "";
		if(e == 'userPlus') target = lazy_userPlus;
		else if(e == 'userMinus') target = lazy_userMinus;
		else if(e == 'vehiclePlus') target = lazy_vehiclePlus;
		else if(e == 'vehicleMinus') target = lazy_vehicleMinus;
		
		$.lazyLoader.search(target);
		
	});
	
	$(".btn_searchUserPopText").on("keyup",function(e){
		if(e.keyCode == 13) {
			$(this).closest("form").find(".btn_searchUserPop").trigger("click");
		}
	})
	
	
	$("#contentsTable").on("click",".btn_plus_user",function(){
		$("#user_plus_groupKey").val($(this).data("key"));
		$("#dialog input[name=searchRemoveGroup]").val($(this).data("key"));
		
		if(lazy_userPlus)
			$.lazyLoader.search(lazy_userPlus);
		
		$( "#dialog" ).dialog({
	        width:800,
	        dialogClass:'alert',
	        draggable: false,
	        modal:true,
	        height:"auto",
	        resizable:false,
	        closeOnEscape:true,
	        buttons:{
	        	'취소':function(){
         			$(this).dialog("close");
         		}
	            ,'확인':function(){
	            	var selObj = $('.selPopObj:checked',$(this));
	            	if(selObj.length == 0)
	            		alert("사용자가 선택되지 않았습니다.");
	            	else{
	            		var strKeys = "";
	            		selObj.each(function(){
	        				var key = $(this).data("accountkey");
	        				if(strKeys.length == 0 ) strKeys = key;
	        				else strKeys += ","+key;
	        				
	        			});
	            		
	        			$VDAS.http_post("/group/activeUserGroup.do",{groupKey:$("#user_plus_groupKey").val(),accountKey:strKeys},{
	        				success:function(r){
	        					alert("저장되었습니다.");
	        					$.lazyLoader.search(lazy);
	        					$( "#dialog" ).dialog("close");
	        				}
	        			});	
	            		
	        				
	            	}
	            	
	            }
	        }
	    });
		
		if(!lazy_userPlus)
			lazy_userPlus = $("#dialog table").lazyLoader({
				searchFrmObj : $("#popupSearchForm")
				,searchFrmVal : {searchOrder:$("#popupSearchForm input[name=searchOrder]")
								,searchUserNm:$("#popupSearchForm input[name=searchUserNm]")
								,searchRemoveGroup:$("#popupSearchForm input[name=searchRemoveGroup]")
				}
				,scrollRow : 4
				,rowHeight : 80
				,limit : 5
				,loadUrl : "/com/getUserList.do"
				,initSearch : true
				,createDataHtml : function(r){
					var data = r.rtvList;
					var strHtml = "";
					for(var i = 0 ; i < data.length ; i++){
						var vo = data[i];
						strHtml += "<tr>";
						strHtml += '<td><input type="checkbox" name="selPopObj" class="selPopObj" data-accountnm="'+vo.accountNm
						+'" data-accountkey="'+vo.accountKey
						+'" data-corpgroupnm="'+vo.corpGroupNm
						+'" data-name="'+vo.name
						+'"/></td>';
						strHtml += '<th>';
						if(vo.imgId&&vo.imgId.length > 0)
							strHtml += '<strong class="car_img"><img class="svg-clipped" style="width:49px;height:49px;" src="${defaultPath}/com/getUserImg.do?uuid='+vo.imgId+'" alt="" /></strong>';
						else strHtml += '<strong class="car_img"><img class="svg-clipped" src="${pathCommon}/images/user_img7.jpg" alt="" style="width:49px;height:49px;"/></strong>';
						strHtml += '<a href="#none">';
						strHtml += '<span>'+vo.accountNm+'</span>';
						strHtml += vo.name;
						strHtml += '</a>';
						strHtml += '</th>';
						strHtml += '<td>';
						strHtml += '<p>'+vo.corpGroupNm+'/'+vo.corpPosition+'</p>';
						strHtml += '</td>';
						strHtml += '<td>';
						strHtml += '<p>'+vo.phone+'</p>';
						strHtml += '<p>'+vo.mobilePhone+'</p>';
						strHtml += '</td>';
						strHtml += '<td>'+vo.descript+'</td>';
						strHtml += '</tr>';	
					}
					
					return strHtml;
				}
			});
	});	
	
	
	$("#contentsTable").on("click",".btn_minus_user",function(){
		$("#user_minus_groupKey").val($(this).data("key"));
		$("#dialogUserRemove input[name=searchGroup]").val($(this).data("key"));
				
		if(lazy_userMinus)
			$.lazyLoader.search(lazy_userMinus);
		
		$( "#dialogUserRemove" ).dialog({
	        width:800,
	        dialogClass:'alert',
	        draggable: false,
	        modal:true,
	        height:"auto",
	        resizable:false,
	        closeOnEscape:true,
	        buttons:{
	        	'취소':function(){
         			$(this).dialog("close");
         		}
	            ,'확인':function(){
	            	var selObj = $('.selPopObj:checked',$(this));
	            	if(selObj.length == 0)
	            		alert("사용자가 선택되지 않았습니다.");
	            	else{
	            		var strKeys = "";
	            		selObj.each(function(){
	        				var key = $(this).data("accountkey");
	        				if(strKeys.length == 0 ) strKeys = key;
	        				else strKeys += ","+key;
	        				
	        			});
	            		
	        			$VDAS.http_post("/group/inactiveUserGroup.do",{accountKey:strKeys},{
	        				success:function(r){
	        					alert("저장되었습니다.");
	        					$.lazyLoader.search(lazy);
	        					$( "#dialogUserRemove" ).dialog("close");
	        				}
	        			});	
	            		
	        				
	            	}
	            	
	            }
	        }
	    });
				
		if(!lazy_userMinus)
			lazy_userMinus = $("#dialogUserRemove table").lazyLoader({
				searchFrmObj : $("#popupSearchFormUserRemove")
				,searchFrmVal : {searchOrder:$("#popupSearchFormUserRemove input[name=searchOrder]")
								,searchUserNm:$("#popupSearchFormUserRemove input[name=searchUserNm]")
								,searchGroup:$("#popupSearchFormUserRemove input[name=searchGroup]")
				}
				,scrollRow : 4
				,rowHeight : 80
				,limit : 5
				,loadUrl : "/com/getUserList.do"
				,initSearch : true
				,createDataHtml : function(r){
					var data = r.rtvList;
					var strHtml = "";
					for(var i = 0 ; i < data.length ; i++){
						var vo = data[i];
						strHtml += "<tr>";
						strHtml += '<td><input type="checkbox" name="selPopObj" class="selPopObj" data-accountnm="'+vo.accountNm
						+'" data-accountkey="'+vo.accountKey
						+'" data-corpgroupnm="'+vo.corpGroupNm
						+'" data-name="'+vo.name
						+'"/></td>';
						strHtml += '<th>';
						if(vo.imgId&&vo.imgId.length > 0)
							strHtml += '<strong class="car_img"><img class="svg-clipped" style="width:49px;height:49px;" src="${defaultPath}/com/getUserImg.do?uuid='+vo.imgId+'" alt="" /></strong>';
						else strHtml += '<strong class="car_img"><img class="svg-clipped" src="${pathCommon}/images/user_img7.jpg" alt="" style="width:49px;height:49px;"/></strong>';
						strHtml += '<a href="#none">';
						strHtml += '<span>'+vo.accountNm+'</span>';
						strHtml += vo.name;
						strHtml += '</a>';
						strHtml += '</th>';
						strHtml += '<td>';
						strHtml += '<p>'+vo.corpGroupNm+'/'+vo.corpPosition+'</p>';
						strHtml += '</td>';
						strHtml += '<td>';
						strHtml += '<p>'+vo.phone+'</p>';
						strHtml += '<p>'+vo.mobilePhone+'</p>';
						strHtml += '</td>';
						strHtml += '<td>'+vo.descript+'</td>';
						strHtml += '</tr>';	
					}
					
					return strHtml;
				}
			});
	});	
	
	
	$("#contentsTable").on("click",".btn_plus_vehicle",function(){
		$("#vehicle_plus_groupKey").val($(this).data("key"));
		$("#popupSearchFormVehiclePlus input[name=searchRemoveGroup]").val($(this).data("key"));
		
		if(lazy_vehiclePlus)
			$.lazyLoader.search(lazy_vehiclePlus);
		
		$( "#dialogVehiclePlus" ).dialog({
	        width:800,
	        dialogClass:'alert',
	        draggable: false,
	        modal:true,
	        height:"auto",
	        resizable:false,
	        closeOnEscape:true,
	        buttons:{
	        	'취소':function(){
         			$(this).dialog("close");
         		}
	            ,'확인':function(){
	            	var selObj = $('.selPopObj:checked',$(this));
	            	if(selObj.length == 0)
	            		alert("차량이 선택되지 않았습니다.");
	            	else{
	            		var strKeys = "";
	            		selObj.each(function(){
	        				var key = $(this).data("vehiclekey");
	        				if(strKeys.length == 0 ) strKeys = key;
	        				else strKeys += ","+key;
	        				
	        			});
	            		
	        			$VDAS.http_post("/group/activeVehicleGroup.do",{groupKey:$("#vehicle_plus_groupKey").val(),vehicleKey:strKeys},{
	        				success:function(r){
	        					alert("저장되었습니다.");
	        					$.lazyLoader.search(lazy);
	        					$( "#dialogVehiclePlus" ).dialog("close");
	        				}
	        			});	
	            		
	        				
	            	}
	            	
	            }
	        }
	    });
		
		if(!lazy_vehiclePlus)
			lazy_vehiclePlus = $("#dialogVehiclePlus table").lazyLoader({
				searchFrmObj : $("#popupSearchFormVehiclePlus")
				,searchFrmVal : {searchOrder:$("#popupSearchFormVehiclePlus input[name=searchOrder]")
								,searchType:$("#popupSearchFormVehiclePlus input[name=searchType]")
								,searchText:$("#popupSearchFormVehiclePlus input[name=searchText]")
								,searchRemoveGroup:$("#popupSearchFormVehiclePlus input[name=searchRemoveGroup]")
				}
				,scrollRow : 4
				,rowHeight : 80
				,limit : 5
				,loadUrl : "/com/getVehicleList.do"
				,initSearch : true
				,createDataHtml : function(r){
					var data = r.rtvList;
					var strHtml = "";
					
					for(var i = 0 ; i < data.length ; i++){
						var vo = data[i];
						
						var used = "";
						if(vo.deviceId.length>0)
							used = '1';
						else
							used = '0';
						
						strHtml += "<tr>";
						strHtml += '<td><input type="checkbox" class="selPopObj" data-vehiclekey="'+vo.vehicleKey
						+'" data-modelmaster="'+vo.modelMaster
						+'" data-platenum="'+vo.plateNum
						+'" data-used="'+used
						+'"/></td>';
						strHtml += '<th>';
						strHtml += '<strong class="car_img"><img src="${path}/common/images/vehicle/'+vo.imgS+'.png" alt="" /></strong>';
						strHtml += '<a href="#none">';
						strHtml += '<span>'+vo.modelMaster+'</span>';
						strHtml += vo.plateNum;
						strHtml += '</a>';
						strHtml += '</th>';
						strHtml += '<td><div class="start_end txt_left">';
						strHtml += '<p>'+vo.year+'</p>';
						strHtml += '<p>'+number_format((vo.totDistance/1000).toFixed(1))+'</p>';
						strHtml += '</div></td>';
						strHtml += '<td><div class="start_end txt_left">';
						strHtml += '<p>'+vo.deviceSeries+'</p>';
						strHtml += '<p>'+vo.deviceSn+'</p>';
						strHtml += '</div></td>';
						if(vo.deviceId.length>0)
							strHtml += '<td>사용중</td>';
						else
							strHtml += '<td>사용중지</td>';
						strHtml += '</tr>';	
					}
					
					return strHtml;
				}
			});
	});	
	
	
	$("#contentsTable").on("click",".btn_minus_vehicle",function(){
		$("#vehicle_minus_groupKey").val($(this).data("key"));
		$("#popupSearchFormVehicleMinus input[name=searchGroup]").val($(this).data("key"));
		
		if(lazy_vehicleMinus)
			$.lazyLoader.search(lazy_vehicleMinus);
		
		$( "#dialogVehicleMinus" ).dialog({
	        width:800,
	        dialogClass:'alert',
	        draggable: false,
	        modal:true,
	        height:"auto",
	        resizable:false,
	        closeOnEscape:true,
	        buttons:{
	        	'취소':function(){
         			$(this).dialog("close");
         		}
	            ,'확인':function(){
	            	var selObj = $('.selPopObj:checked',$(this));
	            	if(selObj.length == 0)
	            		alert("차량이 선택되지 않았습니다.");
	            	else{
	            		var strKeys = "";
	            		selObj.each(function(){
	        				var key = $(this).data("vehiclekey");
	        				if(strKeys.length == 0 ) strKeys = key;
	        				else strKeys += ","+key;
	        				
	        			});
	            		
	        			$VDAS.http_post("/group/inactiveVehicleGroup.do",{vehicleKey:strKeys},{
	        				success:function(r){
	        					alert("저장되었습니다.");
	        					$.lazyLoader.search(lazy);
	        					$( "#dialogVehicleMinus" ).dialog("close");
	        				}
	        			});	
	            		
	        				
	            	}
	            	
	            }
	        }
	    });
		
		if(!lazy_vehicleMinus)
			lazy_vehicleMinus = $("#dialogVehicleMinus table").lazyLoader({
				searchFrmObj : $("#popupSearchFormVehicleMinus")
				,searchFrmVal : {searchOrder:$("#popupSearchFormVehicleMinus input[name=searchOrder]")
								,searchType:$("#popupSearchFormVehicleMinus input[name=searchType]")
								,searchText:$("#popupSearchFormVehicleMinus input[name=searchText]")
								,searchGroup:$("#popupSearchFormVehicleMinus input[name=searchGroup]")
				}
				,scrollRow : 4
				,rowHeight : 80
				,limit : 5
				,loadUrl : "/com/getVehicleList.do"
				,initSearch : true
				,createDataHtml : function(r){
					var data = r.rtvList;
					var strHtml = "";
					
					for(var i = 0 ; i < data.length ; i++){
						var vo = data[i];
						
						var used = "";
						if(vo.deviceId.length>0)
							used = '1';
						else
							used = '0';
						
						strHtml += "<tr>";
						strHtml += '<td><input type="checkbox" class="selPopObj" data-vehiclekey="'+vo.vehicleKey
						+'" data-modelmaster="'+vo.modelMaster
						+'" data-platenum="'+vo.plateNum
						+'" data-used="'+used
						+'"/></td>';
						strHtml += '<th>';
						strHtml += '<strong class="car_img"><img src="${path}/common/images/vehicle/'+vo.imgS+'.png" alt="" /></strong>';
						strHtml += '<a href="#none">';
						strHtml += '<span>'+vo.modelMaster+'</span>';
						strHtml += vo.plateNum;
						strHtml += '</a>';
						strHtml += '</th>';
						strHtml += '<td><div class="start_end txt_left">';
						strHtml += '<p>'+vo.year+'</p>';
						strHtml += '<p>'+number_format((vo.totDistance/1000).toFixed(1))+'</p>';
						strHtml += '</div></td>';
						strHtml += '<td><div class="start_end txt_left">';
						strHtml += '<p>'+vo.deviceSeries+'</p>';
						strHtml += '<p>'+vo.deviceSn+'</p>';
						strHtml += '</div></td>';
						if(vo.deviceId.length>0)
							strHtml += '<td>사용중</td>';
						else
							strHtml += '<td>사용중지</td>';
						strHtml += '</tr>';	
					}
					
					return strHtml;
				}
			});
	});	
	
	
	
	/*
	$(".btn_search").on("click",function(){
		alert("todo! - param");
		$.lazyLoader.search(lazy);
	});
	
	$("#searchText").on("keyup",function(e){
		if(e.keyCode == 13) $(".btn_search").trigger("click");
	});
	
	
	
	$("#btn_upload").on("click",function(){
		
		$( "#dialog2" ).dialog({
	        width:800,
	        dialogClass:'alert',
	        draggable: false,
	        modal:true,
	        height:"auto",
	        resizable:false,
	        closeOnEscape:true,
	        buttons:{
	        	'확인':function(){
	            	
	        		$("#fileFrm").submit();
	        		
	            }
	        }
	    });
		
		$("#dialog2.ui-dialog-content").css({padding:"0px"});
		
	});
	
	$('#fileFrm').ajaxForm({
		beforeSubmit: function (data,form,option) {
	    	return true;
		}
		,success: function(r){
	    	
			alert("처리되었습니다.");
			$.lazyLoader.search(lazy);
	    	$( "#dialog2" ).dialog("close");
	    	
		},
		error: function(r){
			var obj = JSON.parse(r.responseText).result;
			var index = JSON.parse(r.responseText).index;
			alert($message[obj]+"\n"+(index)+"줄");
		}                               
	});
	
    $('.pop_close').click(function(){
        $(this).closest("div.myPopup").dialog("close");
    });
    
  //move page car detail
	$("body").on("click",".a_detail",function(){
		var accountKey = $(this).data("key")?$(this).data("key"):"";
		$VDAS.instance_post("/config/registAccountDetail.do",{accountKey:accountKey});
	});
  
	
	
	
	
	$("body").on("click",".btn_edit",function(){
		var key = $(this).data("key")?$(this).data("key"):"";
		$VDAS.instance_post("/config/registAccountDetail.do",{accountKey:key});				
	});
	
	$VDAS.http_post("/account/getAccountCnt.do",{},{
		success : function(r){
			var data = r.result;
			
			$("#titleCnt1").text(data.total+"명");
		}
	});
	*/
	
	
	$(".btn_close").on("click",function(){
		$("#"+$(this).closest("div").attr("id")).dialog("close");
	});
});







</script>
<form id="searchFrm" style="display:none">
	<!-- <input type="hidden" name="searchGroupKey" value="" /> -->
</form>
<div class="right_layout">
	<h2 class="tit_sub">
		부서 등록
		<p>부서를 등록하거나 수정할 수 있습니다.</p>
	</h2>
	<div class="sub_top1">
		<span>전체 등록 부서<b id="titleCnt1"></b></span>
		<strong id="nowDate"></strong>
	</div>
	
	<h3 class="h3tit">부서등록</h3>
	<div class="box1">
		<div class="search_box1">		
			<div class="search_box1_2" style="left:-50px">			
				<input type="text" id="groupNm" class="search_input" placeholder="등록할 부서명 입력"/>
				<a href="#none" id="btn_add" style="right:-128px;" >등록</a>				
			</div>
		</div>
	</div>
	<h3 class="h3tit">등록 부서 조회</h3>
	<div class="tab_layout">
		<div style="margin-bottom: 10px; overflow: hidden;">
			<div class="select_type2_1">
				<a href="#none" id="btn_mod" class="btn8"><img src="${pathCommon}/images/${_LANG}/btn8.jpg" alt="" /></a>
				<a href="#none" id="btn_del" class="" ><img src="${pathCommon}/images/${_LANG}/btn_delete.jpg" alt=""></a>
			</div>
		</div>
		<table class="table1 repair_box2_1" id="contentsTable">
			<colgroup>
				<col width="75">
				<col width="60">
				<col width="55">
				<col width="80">
				<col width="65">
				<col width="80">
				<col width="*">
				<col width="112">
			</colgroup>
			<thead>
				<tr>
					<th><input type="checkbox" id="all_checked"></th>
					<th>No.</th>
					<th>구성원수</th>
					<th>구성원</th>
					<th>구성차량수</th>
					<th>구성차량</th>
					<th class="user_tit"><b>부서명</b></th>
					<th>등록일</th>
				</tr>
			</thead>
			<tbody>
			<%-- <tr>
							<td><span class="check_box check_box_1"><input type="checkbox" class="checkboxes" data-id="${vo.id }" data-groupnm="${vo.corpGroupNm }"/></td>
							<td>${vo.rNum }</td>
							<td><a href="#none" class="btn_plus" data-id="${vo.id }"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/btn_puls.jpg" ></a>&nbsp;<a href="#none" class="btn_minus" data-id="${vo.id }"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/btn_minus.jpg" ></a></td>
							<td id="group${vo.id}">${vo.memberCnt}</td>
							<th class="user_tit"><span class="txt_gorupNm">${vo.corpGroupNm}</span><input type="text" class="input_gorupNm input151w" name="corpGroupNm" style="display:none" value=""><a href="#none" class="btn_searc_c btn_groupNm" data-id="${vo.id }" style="display:none;text-decoration:initial;line-height:25px;vertical-align:top;text-align:center;">${_C.label29}</a></th>
							<fmt:parseDate value="${vo.regDate }" pattern="yyyy-MM-dd HH:mm:ss.S" var="regDate" />
							<td><fmt:formatDate value="${regDate}" type="both" pattern="yyyy/MM/dd" /></td>
						</tr> --%>
			</tbody>
		</table>
	</div>
</div>
<div id="dialog" title="구성원 추가" class="myPopup" style="display: none">
	<input type="hidden" id="user_plus_groupKey" value="" />
	<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt=""></a>		
	<p class="pop_txt">
		구성원 추가
	</p>
	<div class="p_relative">
		<form id='popupSearchForm' onSubmit="return false;">
			<input type="hidden" name="searchRemoveGroup" />
			<div class="tab_select">
				<select class="select form" name="searchOrder" style="height:22px;">
					<option value="regDate">최근 등록순</option>
				</select>
			</div>
			<strong class="search_area">
				<input type="text" placeholder="직접검색" id="input_searchUserPop" name="searchUserNm" class='btn_searchUserPopText' /><a href="#none" class="btn_searchUserPop" data-e="userPlus"><img src="${pathCommon}/images/btn_search2.jpg" alt=""></a>
			</strong>
		</form>
	</div>
	<table class="table1 table1_1">
		<colgroup>
			<col width="60">
			<col width="170">
			<col width="150">
			<col width="140">
			<col width="150">
		</colgroup>
		<thead>
			<tr>
				<th>선택</th>
				<th>사용자 정보</th>
				<th>부서/직급</th>
				<th>연락처</th>
				<th>비고</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<div id="dialogUserRemove" title="구성원 삭제" class="myPopup" style="display: none">
	<input type="hidden" id="user_minus_groupKey" value="" />
	<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt=""></a>		
	<p class="pop_txt">
		구성원 삭제
	</p>
	<div class="p_relative">
		<form id='popupSearchFormUserRemove' onSubmit="return false;">
			<input type="hidden" name="searchGroup" />
			<div class="tab_select">
				<select class="select form" name="searchOrder" style="height:22px;">
					<option value="regDate">최근 등록순</option>
				</select>
			</div>
			<strong class="search_area">
				<input type="text" placeholder="직접검색" id="input_searchUserPop" name="searchUserNm" class='btn_searchUserPopText'/><a href="#none" class="btn_searchUserPop" data-e="userMinus"><img src="${pathCommon}/images/btn_search2.jpg" alt=""></a>
			</strong>
		</form>
	</div>
	<table class="table1 table1_1">
		<colgroup>
			<col width="60">
			<col width="170">
			<col width="150">
			<col width="140">
			<col width="150">
		</colgroup>
		<thead>
			<tr>
				<th>선택</th>
				<th>사용자 정보</th>
				<th>부서/직급</th>
				<th>연락처</th>
				<th>비고</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<div id="dialogVehiclePlus" title="구성차량 추가" class="myPopup" style="display: none">
	<input type="hidden" id="vehicle_plus_groupKey" value="" />
	<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt=""></a>		
	<p class="pop_txt">
		구성차량 추가
	</p>
	<div class="p_relative">
		<form id='popupSearchFormVehiclePlus' onSubmit="return false;">
			<input type="hidden" name="searchRemoveGroup" />
			<input type="hidden" name="searchType" value="plateNum"/>
			<div class="tab_select">
				<select class="select form" name="searchOrder" style="height:22px;">
					<option value="regDate">최근 등록순</option>
				</select>
			</div>
			<strong class="search_area">
				<input type="text" placeholder="직접검색" id="input_searchUserPop" name="searchText" class='btn_searchUserPopText'/><a href="#none" class="btn_searchUserPop" data-e="vehiclePlus"><img src="${pathCommon}/images/btn_search2.jpg" alt=""></a>
			</strong>
		</form>
	</div>
	<table class="table1 table1_1">
		<colgroup>
			<col width="60">
			<col width="170">
			<col width="150">
			<col width="*">
			<col width="80">
		</colgroup>
		<thead>
			<tr>
				<th>선택</th>
				<th>차량정보</th>
				<th>년식/주행거리</th>
				<th>단말</th>
				<th>사용여부</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<div id="dialogVehicleMinus" title="구성차량 삭제" class="myPopup" style="display: none">
	<input type="hidden" id="vehicle_minus_groupKey" value="" />
	<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt=""></a>		
	<p class="pop_txt">
		구성차량 삭제
	</p>
	<div class="p_relative">	
		<form id='popupSearchFormVehicleMinus' onSubmit="return false;">
			<input type="hidden" name="searchGroup" />
			<input type="hidden" name="searchType" value="plateNum"/>
			<div class="tab_select">
				<select class="select form" name="searchOrder" style="height:22px;">
					<option value="regDate">최근 등록순</option>
				</select>
			</div>
			<strong class="search_area">
				<input type="text" placeholder="직접검색" id="input_searchUserPop" name="searchText" class='btn_searchUserPopText'/><a href="#none" class="btn_searchUserPop" data-e="vehicleMinus"><img src="${pathCommon}/images/btn_search2.jpg" alt=""></a>
			</strong>
		</form>
	</div>
	<table class="table1 table1_1">
		<colgroup>
			<col width="60">
			<col width="170">
			<col width="150">
			<col width="*">
			<col width="80">
		</colgroup>
		<thead>
			<tr>
				<th>선택</th>
				<th>차량정보</th>
				<th>년식/주행거리</th>
				<th>단말</th>
				<th>사용여부</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
</c:if>
<c:if test="${VDAS.rule ne '0' and VDAS.rule ne '1' }">
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("M0003");
});
</script>
<div class="right_layout">
	<img src="${pathCommon}/images/${_LANG}/authError.png" alt="" style="width:100%">
</div>
</c:if>