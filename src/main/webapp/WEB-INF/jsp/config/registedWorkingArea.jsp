<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<c:if test="${VDAS.rule eq '0' or VDAS.rule eq '1' }">
<script type="text/javascript">
var lazy;
$(document).ready(function(){
	//left menu on
	initMenuSel("M0004");
	
	$VDAS.ajaxCreateSelect("/com/getCityLev1.do",{},$("#cityLev1"),false,"전체 시/도","","${_LANG}",false,null);
	
	$("#cityLev1").on("change",function(){
		$VDAS.ajaxCreateSelect("/com/getCityLev2.do",{city:$(this).val()},$("#cityLev2"),false,"전체 시/군/구","","${_LANG}",false,null);	
		
	});
	
	if(!lazy)
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
				cityLev1 : $("#cityLev1")
				,cityLev2 : $("#cityLev2")
				,searchName : $("#searchName")
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 10
			,loadUrl : "/config/getWorkingArea.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.result;
				var strHtml = "";
				for(var i = 0 ; i < data.length ; i++){
					
					var vo = data[i];
					strHtml += "<tr>";
					strHtml += '<td><input type="checkbox" class="selObj" data-key="'+vo.workAreaKey+'"/></td>';
					strHtml += '<th><b>'+(vo.corpGroupNm?vo.corpGroupNm:"")+'</b></th>';
					strHtml += '<td>'+(vo.accountNm?vo.accountNm:"")+(vo.corpPosition?"/"+vo.corpPosition:"")+'</td>';
					strHtml += '<td>'+vo.lev1Nm+(vo.lev2Nm?" "+vo.lev2Nm:" 전체")+'</td>';
					strHtml += '<td><a href="#none" data-key="'+vo.workAreaKey+'" class="btn_moveDetail"><img src="${pathCommon}/images/btn3.jpg" alt=""></a></td>';
					strHtml += '</tr>';	
				}
				
				return strHtml;
			}
		});
	
	$(".btn_search").on("click",function(){
		$.lazyLoader.search(lazy);
	});
	
	$("#searchName").on("keyup",function(e){
		if(e.keyCode == 13) $(".btn_search").trigger("click");
	});
	
	$("#allChker").on("click",function(){
		if($(this).is(":checked"))
			$(".selObj","#contentsTable tbody").prop("checked",true);
		else $(".selObj","#contentsTable tbody").prop("checked",false);
	});
	
	$(".btn_del").on("click",function(){
		var b = true;
		var selObj = $(".selObj:checked");

		if(b){
			if(selObj.length == 0){
				alert("삭제할 이수지역을 선택해주세요.");
				return;
			}
			
			$( "#dialog" ).dialog({
		        width:800,
		        dialogClass:'alert',
		        draggable: false,
		        modal:true,
		        height:"auto",
		        resizable:false,
		        closeOnEscape:true,
		        buttons:{
		            '확인':function(){
		            	
		            	var strKey = "";
		            	$(".selObj:checked").each(function(){
		            		strKey += strKey==""?$(this).data("key"):","+$(this).data("key");
		            	});
		            	
		            	$VDAS.http_post("/config/deleteWorkingArea.do",{workAreaKey:strKey},{
		            		success : function(r){
		            			alert("처리되었습니다.");
		            			$("#dialog").dialog("close");
		            			$.lazyLoader.search(lazy);
		            		}
		            	});
		            	
		               
		            },'취소':function(){
		            	$(this).dialog("close");
		            }
		        }
		    });
		}
	});
	
	$VDAS.http_post("/config/getWorkingAreaTotCnt.do",{},{
		success : function(r){
			$("#totCnt").text(r.result + " 건");
		}
	});
	
	$("body").on("click",".btn_moveDetail",function(){
		var key = $(this).data("key");
		
		$VDAS.instance_post("/config/registWorkingAreaDetail.do",{workAreaKey:key});
		
	});
});







</script>
<form id="searchFrm" style="display:none">
	<input type="hidden" name="cityLev1" value="" />
	<input type="hidden" name="cityLev2" value="" />
	<input type="hidden" name="searchName" value="" />
</form>
<div class="right_layout">
	<h2 class="tit_sub">
		이수지역 설정
		<p>소속직원 전체 또는 부서별, 직원별 이수지역을 설정 하실 수 있습니다.</p>
	</h2>
	<div class="sub_top1">
		<span>이수지역 설정건 <b id="totCnt"></b> </span>
		<strong id="nowDate"></strong>
	</div>
	<h3 class="h3tit">이수지역 등록</h3>
	<div class="box1">
		이수지역 등록시, 운행기록부의 기준데이터로 설정되니 정확히 입력해주세요.
		<a href="${defaultPath}/config/registWorkingAreaDetail.do">이수지역 등록</a>
	</div>
	<h3 class="h3tit">이수지역 조회</h3>
	<div class="search_box1 mgb0">
		<select class="select form" style="width:160px;" id="cityLev1">
			<option value="">전체 시/도</option>
		</select>
		<select class="select form" style="width:160px;" id="cityLev2">
			<option value="">전체 시/군/구</option>
		</select>
		<div class="search_box1_2" style="width:200px;">
			<input type="text" class="search_input" id="searchName" placeholder="사용자 이름을 입력해주세요."/>
		</div><a href="#none" class="btn_search">조회</a>
	</div>
	<div class="tab_layout">
		<div class="align_right mgt30">
			<a href="#none" class="btn_del"><img src="${pathCommon}/images/${_LANG}/btn_delete.jpg" alt=""></a>
		</div>
		<table class="table1 mgt10" id="contentsTable">
			<colgroup>
				<col width="65">
				<col width="200">
				<col width="*">
				<col width="150">
				<col width="80">
			</colgroup>
			<thead>			
				<tr>
					<th><input type="checkbox" id="allChker" /></th>
					<th>부서</th>
					<th>성명/직급</th>
					<th>이수지역</th>
					<th>수정</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<div id="dialog" title="이수지역 삭제" class="myPopup" style="display:none">
	<div class="pop_close"></div>
	<p class="pop_txt">
		선택된 이수지역을 삭제 하시겠습니까?		
	</p>
</div>
</c:if>
<c:if test="${VDAS.rule ne '0' and VDAS.rule ne '1' }">
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("M0004");
});
</script>
<div class="right_layout">
	<img src="${pathCommon}/images/${_LANG}/authError.png" alt="" style="width:100%">
</div>
</c:if>