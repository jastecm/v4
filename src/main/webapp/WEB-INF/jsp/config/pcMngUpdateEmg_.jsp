<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>  
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	
	$("body").on("keyup","#searchDeviceSn",function(e){
		if(e.keyCode == '13'){
			var sn = $("#searchDeviceSn").val();
			if(sn.length != 4) {
				alert("시리얼번호 뒤 4자리를 입력해주세요.");
				return;
			}
			
			$(".sub_layout").html('<span style="font-size: 17px;">시리얼번호 : A000</span><input type="text" class="w172" id=\'searchDeviceSn\'  style="display: inline-block;vertical-align: top;width: 123px;height: 25px;border: 1px solid #c3c3c3;padding-left: 10px;font-size: 17px;"/>&nbsp;&nbsp;&nbsp;<span style="font-size: 17px;">Gper번호 : </span><input type="text" class="w172" id=\'searchText\'  style="display: inline-block;vertical-align: top;width: 123px;height: 25px;border: 1px solid #c3c3c3;padding-left: 10px;font-size: 17px;"/><br/><br/><span style="color: red;font-weight: bold;font-size: 15px;">A000를 제외한 일련번호 끝에 4자리를 입력해주세요. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;제품 박스 및 단말기에 기재된 Gper번호를 입력해주세요.</span><br/><br/><span style="color: red;font-weight: bold;font-size: 15px;">일련번호 또는 Gper번호 입력후, \'엔터키\'를 눌러주세요.</span>');
			
				
			$VDAS.http_post("/com/getVehicleList.do",{searchDeviceSn:"A000"+sn},{
				success:function(r){
					var data = r.rtvList;				
					var strHtml = '';
					
					if(data.length == 0){
						strHtml += '<br/>';
						strHtml += '<br/>';
						strHtml += '<br/>';
						strHtml += '<br/>';
						strHtml += '결과없음';
					}
					
					for(var i = 0 ; i < data.length ; i++){
						var vo = data[i];
						strHtml += '<br/>';
						strHtml += '<br/>';
						strHtml += '<br/>';
						strHtml += '<br/>';
						strHtml += 'Gper번호 : <span id="plateNum" style="padding-right:15px;">'+vo.plateNum+'</span>   ';
						strHtml += '시리얼번호 : <span id="deviceSn"style="padding-right:15px;">'+vo.deviceSn+'</span>   ';
						
						var dbNm = "";
						var firmNm = "";
						$VDAS.http_post("/vehicle/getVerInfoVehicle.do",{vehicleKey:vo.vehicleKey},{
							sync : true
							,success : function(r){
								var d = r.result;
								dbNm = d.vehicleVer;
								firmNm = d.firmware;
							}
						});
						
						strHtml += 'DB버전 : <span id="dbNm" style="padding-right:15px;">'+dbNm+'</span>   ';
						strHtml += '펌웨어버전 : <span id="firmNm" style="padding-right:15px;">'+firmNm+'</span>   ';
						strHtml += '<a class="btn_update" data-key="'+vo.connectionKey+'" data-convcode="'+vo.convCode+'"><img src="${pathCommon}/images/${_LANG}/btn_update_red.png" /></a>';
					}
					
					$(".sub_layout").append(strHtml);
				}
			});
		}
	});
	
	$("body").on("keyup","#searchText",function(e){
		if(e.keyCode == '13'){
			var sn = $("#searchText").val();
			if(sn.length == 0) {
				alert("값을 입력해주세요.");
				return;
			}
			
			
			$(".sub_layout").html('<span style="font-size: 17px;">시리얼번호 : A000</span><input type="text" class="w172" id=\'searchDeviceSn\'  style="display: inline-block;vertical-align: top;width: 123px;height: 25px;border: 1px solid #c3c3c3;padding-left: 10px;font-size: 17px;"/>&nbsp;&nbsp;&nbsp;<span style="font-size: 17px;">Gper번호 : </span><input type="text" class="w172" id=\'searchText\'  style="display: inline-block;vertical-align: top;width: 123px;height: 25px;border: 1px solid #c3c3c3;padding-left: 10px;font-size: 17px;"/><br/><br/><span style="color: red;font-weight: bold;font-size: 15px;">A000를 제외한 일련번호 끝에 4자리를 입력해주세요. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;제품 박스 및 단말기에 기재된 Gper번호를 입력해주세요.</span><br/><br/><span style="color: red;font-weight: bold;font-size: 15px;">일련번호 또는 Gper번호 입력후, \'엔터키\'를 눌러주세요.</span>');
			
			
			$VDAS.http_post("/com/getVehicleList.do",{searchType:'plateNum',searchText:sn},{
				success:function(r){
					var data = r.rtvList;				
					var strHtml = '';
					
					if(data.length == 0){
						strHtml += '<br/>';
						strHtml += '<br/>';
						strHtml += '<br/>';
						strHtml += '<br/>';
						strHtml += '결과없음';
					}
					
					for(var i = 0 ; i < data.length ; i++){
						var vo = data[i];
						strHtml += '<br/>';
						strHtml += '<br/>';
						strHtml += '<br/>';
						strHtml += '<br/>';
						strHtml += 'Gper번호 : <span id="plateNum" style="padding-right:15px;">'+vo.plateNum+'</span>   ';
						strHtml += '시리얼번호 : <span id="deviceSn"style="padding-right:15px;">'+vo.deviceSn+'</span>   ';
						
						var dbNm = "";
						var firmNm = "";
						$VDAS.http_post("/vehicle/getVerInfoVehicle.do",{vehicleKey:vo.vehicleKey},{
							sync : true
							,success : function(r){
								var d = r.result;
								dbNm = d.vehicleVer;
								firmNm = d.firmware;
							}
						});
						
						strHtml += 'DB버전 : <span id="dbNm" style="padding-right:15px;">'+dbNm+'</span>   ';
						strHtml += '펌웨어버전 : <span id="firmNm" style="padding-right:15px;">'+firmNm+'</span>   ';
						strHtml += '<a class="btn_update" data-key="'+vo.connectionKey+'" data-convcode="'+vo.convCode+'"><img src="${pathCommon}/images/${_LANG}/btn_update_red.png" /></a>';
					}
					
					$(".sub_layout").append(strHtml);
				}
			});
		}
	});
	
	$("body").on("click",".btn_update",function(){
		var $_this = $(this);
		var totDistance = 0;
		
		var deviceSound = 0;
		
		
		var connectionKey = $(this).data("key");
		var convCode = $(this).data("convcode");
		//var key = "1b07959d-9d08-11e7-b5ba-0002b3da4116";
		var key = "6d6ea5a5-8867-11e7-b9e0-0002b3da4116";
		
		$_this.attr("href","pcmgrProto://updateVehicle/?connectionKey="+connectionKey+"&convCode="+convCode+"&totDistance="+totDistance+"&deviceSound="+deviceSound+"&key="+key);
		
	});
	
	if(is_32bits_architecture())
		$(".pcMngDown").attr("href","https://viewcar.co.kr/pcmgrvon41installer32.exe");
	else
		$(".pcMngDown").attr("href","https://viewcar.co.kr/pcmgrvon41installer32.exe");
	
	if(is_32bits_architecture())
		$(".pcDriverDown").attr("href","https://viewcar.co.kr/JASTECVirtualCOMPort_Driver.zip");
	else
		$(".pcDriverDown").attr("href","https://viewcar.co.kr/JASTECVirtualCOMPort_Driver.zip");
	
});
</script>
<div class="top_wrap">
	<form id="searchFrm">
		<input type="hidden" name="searchDeviceSn" />
	</form>
	
	<img src="${pageContext.request.contextPath}/common/images/${_LANG}/emgLogo.png" />
	<span style="margin-left:22px;"><a class="pcMngDown" href="#none"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/download_pcMng.png" /></a></span>
	<span style="margin-left:22px;"><a class="pcDriverDown" href="#none"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/download_usbDriver.png" /></a></span>
	
	<div class="sub_layout" style="padding-left:0px;width:1024px;margin-left:22px;">
		<span style="font-size: 17px;">시리얼번호 : A000</span><input type="text" class="w172" id='searchDeviceSn'  style="display: inline-block;vertical-align: top;width: 123px;height: 25px;border: 1px solid #c3c3c3;padding-left: 10px;font-size: 17px;"/>
		&nbsp;&nbsp;&nbsp;<span style="font-size: 17px;">Gper번호 : </span><input type="text" class="w172" id='searchText'  style="display: inline-block;vertical-align: top;width: 123px;height: 25px;border: 1px solid #c3c3c3;padding-left: 10px;font-size: 17px;"/>
		<br/>
		<br/>
		<span style="color: red;font-weight: bold;font-size: 15px;">A000를 제외한 일련번호 끝에 4자리를 입력해주세요. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;제품 박스 및 단말기에 기재된 Gper번호를 입력해주세요.</span>
		<br/>
		<br/>
		<span style="color: red;font-weight: bold;font-size: 15px;">일련번호 또는 Gper번호 입력후, '엔터키'를 눌러주세요.</span> 
		
	</div>
</div>




