<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>  
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	
	$("body").on("click",".btn_update",function(){
		$("#fileFrm").submit();		
	});
	
	$('#fileFrm').ajaxForm({
		beforeSubmit: function (data,form,option) {
	    	return true;
		}
		,success: function(r){
	    	
			alert("처리되었습니다.");
	    	
		},
		error: function(r){
			var obj = JSON.parse(r.responseText).result;
			alert(obj);
		}                               
	});
	
});
</script>
<div class="top_wrap">
	<form id="searchFrm">
		<input type="hidden" name="searchDeviceSn" />
	</form>
	<form id="fileFrm" class="P10" action="${path}/com/fileUpload.do" method="post" enctype="multipart/form-data" onSubmit="return false;">
		<input type="hidden" name="uploadType" value="excelTempVehicle" />
		<div class="btn_file">
			<input type="file" name="uploadFile" class="multi" accept="xls|xlsx" maxlength="1"/>
			&nbsp;<a class="btn_update"><img src="${pathCommon}/images/${_LANG}/btn_update_red.png" /></a>
		</div>
		</br>
		</br>
		</br>
		</br>
		<a href="pcmgrProto://newVehicle/?key=${VDAS.accountKey}" class="btn_cancel" style="display: inline-block; position: ; top: 50%; right: 16px; width: 123px; height: 38px; line-height: 38px; text-align: center; background: #4e4e4e; color: #fff; font-weight: 600; font-size: 13px; margin-top: -19px;">단말기 등록 실행</a>
	</form>
</div>




