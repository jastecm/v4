<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<c:if test="${VDAS.rule eq '0' or VDAS.rule eq '1' }">
<style>.svg-clipped{-webkit-clip-path:url(#svgPath);clip-path:url(#svgPath);}</style>
<svg height="0" width="0">
	<defs>
		<clipPath id="svgPath" clipPathUnits="objectBoundingBox">
  			<circle cx=".5" cy=".5" r=".5" />
   		</clipPath>
	</defs>
</svg>
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("M0002");
	
	$("input[type=file].multi").MultiFile();
	
	$('#fileFrm').ajaxForm({
		beforeSubmit: function (data,form,option) {
	    	return true;
		}
		,success: function(res,status){
	    	
	    	if(res.rtv == "S"){
	    		var img = res.rtvMap.uploadFile;
				if(!img || img.length == 0){
					$("#user").attr("src","${pageContext.request.contextPath}/common/images/user_img6.jpg");
		    		$("#userS").attr("src","${pageContext.request.contextPath}/common/images/user_img7.jpg");
		    		$("#uploadFile").val("");	
				}else{
					$("#user").attr("src","${defaultPath}/com/getUserImg.do?uuid="+res.rtvMap.uploadFile);
		    		$("#userS").attr("src","${defaultPath}/com/getUserImg.do?uuid="+res.rtvMap.uploadFile);
		    		$("#uploadFile").val(res.rtvMap.uploadFile);	
				}    		
	    		
	    	}else{
	    		
	    	}
	    	
		},
		error: function(){
	    	alert("error");
		}                               
	});
	$("#fileFrm").submit(function(){return false});
	
	$("#btn_upload").on("click",function(){
		$('#fileFrm').submit();
	});
	
	$("#btn_fileUploadComplate").on("click",function(){
		var v = $("#uploadFile").val();
		if(v.length == 0){
			$("#frmImg").attr("src", "${pageContext.request.contextPath}/common/images/user_img6.jpg");
			$("#frm input[name=imgId]").val("");
		}else{
			$("#frmImg").attr("src", "${defaultPath}/com/getUserImg.do?uuid="+v);
			$("#frm input[name=imgId]").val(v);
		}
		$(".black").fadeOut();
		$(".user_up_ppp").fadeOut();
	});
	
	gorupLoad("${reqVo.corpGroup}");

    $( "#birthDate" ).datepicker({
    	changeMonth: true,
        changeYear: true,
        yearRange: 'c-80:c+80',
        showButtonPanel: true,
        dateFormat: 'yymmdd',
        showMonthAfterYear: false,
        onClose: function(dateText, inst) { 
            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay));
        }
    });
    
	$("#btn_next").on("click",function(){
		console.log($("#frm").serialize());
		$VDAS.http_post("/config/registUser.do",$("#frm").serialize(),{
			success:function(r){
				alert("저장되었습니다.");
				$VDAS.instance_post("/config/registedAccount.do",{});
			},
			error:function(r){
				var obj = JSON.parse(r.responseText).result;
				alert($message[obj]);
				var key = obj.split(".")[0];
				if($("#frm [name="+key+"]")) $("#frm [name="+key+"]").focus();
				
			}
		});
	});
	
	$('.ui_radio_box input[type=radio]').checkboxradio({
        icon: false
    });
	
	$("#chkSync").on("click",function(){
		if($(this).is(":checked")){
			$("#frm input[name=authMailNm]").val($("#frm input[name=accountNm]").val());
			$("#frm input[name=authMailDomain]").val("${VDAS.corpDomain}");
		}
	});
	
	$("#authMailSample").on("change",function(){
		var _this = $(this).val();
		var _target = $("#frm input[name=authMailDomain]");
		if(_this.length == 0){
			_target.val("");
		}else{
			_target.val(_this);
		}
	})
	
});

function gorupLoad(sel){

	$VDAS.http_post("/com/getGroupList.do",{},{
		success : function(r){
			
			var target = $("#corpGroup")
			
			target.html("");
			
			var strHtml = "";
			
			if("${_LANG}" == 'ko'){
				strHtml += "<option value=''>선택</option>";
			}else{
				strHtml += "<option value=''>Select</option>";	
			}
			var valList = r.rtvList;
			for(var i = 0 ; i < valList.length ; i++){
				if(sel == valList[i].groupKey) strHtml += "<option value='"+valList[i].groupKey+"' selected>"+valList[i].corpGroupNm+"</option>";
				else strHtml += "<option value='"+valList[i].groupKey+"'>"+valList[i].corpGroupNm+"</option>";
			}
			
			target.html(strHtml);
			
		}
	});
}
</script>
<div class="right_layout">
	<form id="frm" method="post">
		<input type="hidden" name="accountKey" value="${reqVo.accountKey}" >
		<div class="detail_view">
			<a href="javascript:window.history.back();" class="btn_close"><img src="${pathCommon}/images/close.jpg" alt="" /></a>
			<h2 class="h2tit">사용자 등록</h2>
			<div class="sub_top1 sub_top2">
				<span>소속직원의 정보를 정확히 입력해주세요.</span>
			</div>
			<div class="repair_layout2 pat0">
				<div class="repair_box1">
					<div class="user_img">
						<div>
							<c:if test="${empty reqVo.imgId }"><img class="" style="width:115px;height:115px;" id="frmImg" src="${pageContext.request.contextPath}/common/images/user_img6.jpg" alt="" /></c:if>
							<c:if test="${!empty reqVo.imgId }"><img class="" style="width:115px;height:115px;" id="frmImg" src="${defaultPath}/com/getUserImg.do?uuid=${reqVo.imgId}" alt="" /></c:if>							
						</div>							
						<a href="#none" class="btn_pic_up"><img src="${pathCommon}/images/${_LANG}/btn_img_up.jpg" alt="" /></a>
					</div>
					<div class="repair_box2">
						<div class=" repair_box2_1 pal25">
							<div class="form_item">
								<div class="row">
									<div class="col col-2">
										<span class="tit">성명</span>
										<input type="text" class="w172" name="name" value="${reqVo.name }" maxlength="20"/>
									</div>
									<div class="col col-2">
										<span class="tit">소속</span>
										<div class="user_txt1">${VDAS.corpNm}</div>
									</div>
								</div>
							</div>
							<div class="form_item">
								<div class="row">
									<div class="col col-2">
										<span class="tit">부서명</span>
										<select class="select form" style="width:184px" id="corpGroup" name="corpGroup"></select>
									</div>
									<div class="col col-2">
										<span class="tit">직급</span>
										<input type="text" class="w172" name="corpPosition" value="${reqVo.corpPosition}" maxlength="20"/>
									</div>
								</div>
							</div>
							<div class="form_item">
								<div class="row">
									<div class="col col-2">
										<span class="tit">전화번호</span>
										<c:set var="phoneArr" value="${fn:split(reqVo.phone,'-')}" />
										<input type="text" class="numberOnly" maxlength="3" style="width:45px" id="phone1" name="phone1" value="${phoneArr[0]}" />
										-
										<input type="text" class="numberOnly" maxlength="4" style="width:45px" id="phone2" name="phone2" value="${phoneArr[1]}"/>
										-
										<input type="text" class="numberOnly" maxlength="4" style="width:45px" id="phone3" name="phone3" value="${phoneArr[2]}"/>
									</div>

									<div class="col col-2">
										<span class="tit">휴대폰 번호</span>
										<c:set var="mobilePhoneArr" value="${fn:split(reqVo.mobilePhone,'-')}" />
										<select class="select form" style="width:50px" id="mobilePhone1" name="mobilePhone1" >
											<c:forEach var="vo" items="${mPhoneTitle}" varStatus="i">
												<option value="${vo.code }" <c:if test="${vo.code eq mobilePhoneArr[0]}">selected</c:if> >${vo.code }</option>
											</c:forEach>   
										</select>
										-
										<input type="text" class="numberOnly" maxlength="4" style="width:45px" id="mobilePhone2" name="mobilePhone2" value="${mobilePhoneArr[1]}"/>
										-
										<input type="text" class="numberOnly" maxlength="4" style="width:45px" id="mobilePhone3" name="mobilePhone3" value="${mobilePhoneArr[2]}"/>
									</div>
								</div>
							</div>

							<div class="form_item">
								<div class="row">
									<div class="col col-2">
										<span class="tit">성별</span>
										<div class="ui_radio_box">
											<label for="genderM">남자</label>
											<input type="radio" name="gender" id="genderM" value="M" <c:if test="${reqVo.gender eq 'M'}">checked="checked"</c:if> >
											<label for="genderF">여자</label>
											<input type="radio" name="gender" id="genderF" value="F" <c:if test="${reqVo.gender eq 'F'}">checked="checked"</c:if> >
										</div>
									</div>

									<div class="col col-2">
										<span class="tit">혈액형</span>
										<select class="select form" style="width:50px" name="bloodRh">
											<option value="rh+" <c:if test="${reqVo.bloodRh eq 'rh+'}">selected</c:if> >Rh+</option>
											<option value="rh-" <c:if test="${reqVo.bloodRh eq 'rh-'}">selected</c:if> >Rh-</option>
										</select>
										<div class="ui_radio_box">
											<label for="radio2-1">A</label>
											<input type="radio" name="blood" id="radio2-1" value="A" <c:if test="${reqVo.blood eq 'A'}">checked="checked"</c:if>>
											<label for="radio2-2">B</label>
											<input type="radio" name="blood" id="radio2-2" value="B" <c:if test="${reqVo.blood eq 'B'}">checked="checked"</c:if>>
											<label for="radio2-3">O</label>
											<input type="radio" name="blood" id="radio2-3" value="O" <c:if test="${reqVo.blood eq 'O'}">checked="checked"</c:if>>
											<label for="radio2-4">AB</label>
											<input type="radio" name="blood" id="radio2-4" value="AB" <c:if test="${reqVo.blood eq 'AB'}">checked="checked"</c:if>>
										</div>
									</div>
								</div>

							<div class="form_item borB1">
								<div class="row">
									<div class="col col-2">
										<span class="tit">생년월일</span>
										<input type="text" id="birthDate" name="birth" class="date datePicker" value="${reqVo.birth }"/>
									</div>
								</div>
								<br/>
							</div>

							<div class="form_item">
								<br/>
								<div class="row">
									<span class="tit">아이디</span>
									<input type="text" style="width:127px;" name="accountNm" value="${reqVo.accountNm}" maxlength="20" <c:if test="${not empty reqVo.accountKey }">readonly</c:if>>
									@
									${VDAS.corpDomain }
									<br/>
									<span class="tit">&nbsp;</span>
									<div class="form error">
										* @ 뒤 도메인 주소는 회사를 구분하는 용도로 쓰여지므로, 회사 이메일을 사용해주세요.
										<br/>
										(회사 이메일이 없는 경우, 임의로 회사 명을 작성해주세요.)
									</div>
								</div>
							</div>

							<div class="form_item">
								<div class="row">
									<div class="col col-2">
										<span class="tit">비밀번호</span>
										<input type="password" style="width: 160px;" name="accountPw" maxlength="20"/>
									</div>
									<div class="col col-2">
										<span class="tit">비밀번호 확인</span>
										<input type="password" style="width: 160px;" name="accountPwConfirm" maxlength="20"/>
									</div>
								</div>
								<div class="row">
									<span class="tit">&nbsp;</span>
									<div class="form tip">
										* 8~20자리 대소문자, 숫자, 특수문자 조합
									</div>
								</div>
							</div>

							<div class="form_item borB1">
								<div class="row">
									<span class="tit">보조 이메일</span>
									<c:set var="authMailArr" value="${fn:split(reqVo.authMail,'@')}" />
									<input type="text" style="width:127px;" name="authMailNm" value="${authMailArr[0]}" maxlength="20">
									@
									<input type="text" style="width:127px;" name="authMailDomain" value="${authMailArr[1]}" maxlength="20">
									<select name="" id="authMailSample" class="select form" style="width:127px;">
										<option value="">이메일 선택</option>
										<option value="naver.com">네이버</option>
                            			<option value="gmail.com">G메일</option>
									</select>
									<br/>
									<span class="tit">&nbsp;</span>
									<div class="form tip">
										* 비밀번호를 잃어버렸을 경우 사용되는 이메일입니다.
									</div>
								</div>
								<div class="row">
									<div style="margin-left: 80px;">
										<input id="chkSync" type="checkbox" >
										<label for="chkSync">아이디와 동일하게 사용</label>
									</div>
								</div>
								<br/>
							</div>
							<div class="form_item">
								<br/>
								<span class="tit">비고</span>
								<input type="text" style="width:470px;" name="descript" value="${reqVo.descript }" maxlength="255">
								<br/>
							</div>

						</div>
					</div>
				</div>
			</div>
			<input type="hidden" name="imgId" value="${reqVo.imgId}" /> 
			<div class="btn_area2">
				<a href="javascript:window.history.back();" class="btn_cancel">취소</a>
				<a href="#none" id="btn_next">등록</a>
			</div>
		</div>
	</form>
</div>
<div class="black"></div>
<!-- 사용자 등록하기 -->
<div class="user_up_ppp">
	<div class="pop_layout">
		<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt=""></a>
		<h1>사진 업로드</h1>
		<div class="user_pop_txt1"><img src="${pathCommon}/images/${_LANG}/user_up_pop_img1.jpg" alt="" /></div>
		<div class="user_pop_txt2">
			<div class="txt"><a href="#none" style="height:17px;line-height:19px;margin-top:-5px;" class="btn_searc_c" id="btn_upload">업로드</a>ㆍ가로세로가 동일한(정사각형)사진을 등록하셔야 리스트에서 사진이 깨지지않습니다.</div>
			<span class="tit">파일업로드</span>
			<fieldset style="border:0">
				<form id="fileFrm" action="${defaultPath}/com/fileUpload.do" method="post" enctype="multipart/form-data" class="P10">
					<input type="hidden" name="uploadType" value="userImg" />
					<div class="btn_file"><input type="file" class="multi" name="uploadFile" accept="jpg|gif" maxlength="1"/></div>
				</form>
			</fieldset>
		</div>
		<div class="user_pop_img">
			<input type="hidden" id="uploadFile" value="${rtv.imgId }" >
			<span class="tit">미리보기</span><div class="img1">
				<div>
					<img class=""  id="user" src="${pageContext.request.contextPath}/common/images/user_img6.jpg" alt="" style="width:97px;height:97px;"/>				
				</div>
				<p>상세화면</p>
			</div>
				<div class="img2">
					<div>
					<img  class="svg-clipped" id="userS" src="${pageContext.request.contextPath}/common/images/user_img7.jpg" alt="" style="width:97px;height:97px;"/>
					</div>
				<p>상세화면</p>
			</div>
		</div>
		<div class="btn_area2"><a href="#none" class="btn_cancel">취소</a><a href="#none" id="btn_fileUploadComplate">등록완료</a></div>
	</div>
</div>
<!--// 사용자 등록하기 -->
</c:if>
<c:if test="${VDAS.rule ne '0' and VDAS.rule ne '1' }">
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("M0002");
});
</script>
<div class="right_layout">
	<img src="${pathCommon}/images/${_LANG}/authError.png" alt="" style="width:100%">
</div>
</c:if>