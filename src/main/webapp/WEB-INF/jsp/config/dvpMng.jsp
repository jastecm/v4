<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<fmt:setLocale value="${_LANG}" scope="request" />
<fmt:setBundle basename="properties.messages.rest.message" />

<script type="text/javascript">




$(document).ready(function(){
	initMenuSel("M0001");
	
	$VDAS.http_post("/config/breakPcmng.do",{},{
		
	});
	
	if(is_32bits_architecture())
		$(".pcMngDown").attr("href","https://viewcar.co.kr/pcmgrvon41installer32.exe");
	else
		$(".pcMngDown").attr("href","https://viewcar.co.kr/pcmgrvon41installer32.exe");
	
	if(is_32bits_architecture())
		$(".pcDriverDown").attr("href","https://viewcar.co.kr/JASTECVirtualCOMPort_Driver.zip");
	else
		$(".pcDriverDown").attr("href","https://viewcar.co.kr/JASTECVirtualCOMPort_Driver.zip");
	
	
	$(".btn_down").on("click",function(){
		
		$VDAS.http_post("/config/startPcmng.do",{},{
			success:function(r){
				$(".black").fadeIn();
				$VDAS.http_post("/config/monitorPcmng.do",{},{
					success:function(r){
						pcMonitor();
					}
				});
			}
		});
		
	});
	
	$(".btn_mngDown2").on("click",function(){
		$(".black").fadeOut();
	});
	
	
	
	$("#btn_complate").on("click",function(){
		document.location.href = _contextPath+"/config/registedVehicle.do";
	});
});

function pcMonitor(){
	$VDAS.http_post("/config/monitorPcmng.do",{},{
		success:function(r){
			if(r.result == "1"){
				$("#btn_complate").show();
				$(".black").fadeOut();
			}else if(r.repeat=="1") pcMonitor();
		}
	});
}

</script>
<div class="right_layout">
	<form id="addVehicleFrm" method="post">
		<div class="detail_view">
			<a href="javascript:window.history.back();" class="btn_close"><img src="${pathCommon}/images/close.jpg" alt="" /></a>
			<h2 class="h2tit"><fmt:message key='dvpMng.txt1'/></h2>
			<div class="step_box">
				<img src="${pathCommon}/images/${_LANG}/img_caradd_step2.png" alt="" />
			</div>
			<div class="repair_layout2" >
				<div class="pcimg_box">
					<div style="height:180px;width:100%">
						<div style="width: 100%;text-align: center;padding-top:20px;font-size:12px;font-weight: 600;color: #424242">
							<a class="pcMngDown btn_mngDown" href="#none" style="text-decoration: underline;color:#2a20eb">
								<fmt:message key='dvpMng.txt2'/>
							</a> 
							<fmt:message key='dvpMng.txt2_1'/>
						</div>
						<div style="width: 100%;text-align: center;padding-top:20px;font-size:12px;font-weight: 600;color: #828282">
							<fmt:message key='dvpMng.txt3'/>
								<a class="pcDriverDown" href="#none" style="text-decoration: underline;color:#2a20eb">
									<fmt:message key='dvpMng.txt3_1'/>
								</a>
							<fmt:message key='dvpMng.txt3_2'/>
						</div>
						<div style="width: 100%;text-align: center;padding-top:20px;font-size:12px;font-weight: 600;color: #828282">
							<fmt:message key='dvpMng.txt3_3'/>
						</div>
						</br>
						<div style="width: 100%;text-align: center;padding-top:20px;font-size:12px;font-weight: 600;color: #828282">
							<fmt:message key='dvpMng.txt4'/>
						</div>
						<div style="width: 100%;text-align: center;padding-top:20px;font-size:12px;font-weight: 600;color: #828282">
							<fmt:message key='dvpMng.txt4_1'/>
						</div>
					</div>
				</div>
				<div style="width: 100%;text-align: center;padding-top:20px;">
					<div style="width: 100%;text-align: center;padding-top:20px;font-size:12px;font-weight: 600;color: #424242">
						<fmt:message key='dvpMng.txt5'/>
					</div>
				</div>
				<div style="width: 100%;text-align: center;padding-top:50px;">
					<img src = "${pathCommon}/images/${_LANG}/pcmng.png" alt="" />
				</div>					
											
				<div class="btn_area5">
					<a href="pcmgrProto://newVehicle/?key=${VDAS.accountKey}" class="btn_down"><fmt:message key='dvpMng.deviceRegOper'/></a>
				</div>
				<div class="btn_area2"><a href="javascript:window.history.back();" class="btn_cancle"><fmt:message key='dvpMng.prev'/></a><a href="#none" id="btn_complate" style="display: none"><fmt:message key='dvpMng.regComplete'/></a></div>
			</div>
		</div>
	</form>
</div>
<div class="black" style="text-align: center">
	<div style="width: 100%;text-align: center;padding-top:300px;"></div>
	<div style="width: 100%;text-align: center;padding-top:20px;font-size:12px;font-weight: 600;color: #424242;background: white"><fmt:message key='dvpMng.txt6'/></div>
	<div style="width: 100%;text-align: center;padding-top:20px;font-size:12px;font-weight: 600;color: #424242;background: white"><fmt:message key='dvpMng.txt6_1'/><a class="pcMngDown btn_mngDown2" href="#none" style="text-decoration: underline;color:#2a20eb"><fmt:message key='dvpMng.txt6_2'/></a><fmt:message key='dvpMng.txt6_3'/></div>
	<div style="width: 100%;text-align: center;padding-top:20px;padding-bottom:20px;font-size:12px;font-weight: 600;color: #828282;background: white"><fmt:message key='dvpMng.txt6_4'/></div>	
</div>
