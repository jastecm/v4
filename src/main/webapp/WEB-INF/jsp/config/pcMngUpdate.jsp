<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 

<fmt:setLocale value="${_LANG}" scope="request" />
<fmt:setBundle basename="properties.messages.rest.message" />

<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script type="text/javascript">

var lazy;


$(document).ready(function(){
	initMenuSel("M0006");
	
	$VDAS.http_post("/config/breakPcmng.do",{},{
		
	});
	
	if(is_32bits_architecture())
		$(".pcMngDown").attr("href","https://viewcar.co.kr/pcmgrvon41installer32.exe");
	else
		$(".pcMngDown").attr("href","https://viewcar.co.kr/pcmgrvon41installer32.exe");
	
	if(is_32bits_architecture())
		$(".pcDriverDown").attr("href","https://viewcar.co.kr/JASTECVirtualCOMPort_Driver.zip");
	else
		$(".pcDriverDown").attr("href","https://viewcar.co.kr/JASTECVirtualCOMPort_Driver.zip");
	
	$("body").on("click",".deviceSound",function(){
		var objs = $(this).closest("div").find(".deviceSound");
		objs.removeClass("on");
		$(this).addClass("on");
	});
	
	$("body").on("click",".btn_update",function(){
		var $_this = $(this);
		var totDistance = $(this).closest("tr").find(".numberOnly").val()*1000;
		
		var deviceSound = $(this).closest("tr").find(".deviceSound.on").data("val");
		
		
		var connectionKey = $(this).data("key");
		var convCode = $(this).data("convcode");
		var key = "${VDAS.accountKey}";
		
		
		
		$_this.attr("href","pcmgrProto://updateVehicle/?connectionKey="+connectionKey+"&convCode="+convCode+"&totDistance="+totDistance+"&deviceSound="+deviceSound+"&key="+key);
		
		//console.log("pcmgrProto://updateVehicle/?connectionKey="+connectionKey+"&convCode="+convCode+"&totDistance="+totDistance+"&deviceSound="+deviceSound+"&key="+key);
		
		$VDAS.http_post("/config/startPcmng.do",{},{
			success:function(r){
			
				$(".black").fadeIn();
			
				pcMonitor();
				
			}
		});
		
		$(".btn_mngDown2").on("click",function(){
			$(".black").fadeOut();
		});
		
		
	});
	
	$VDAS.http_post("/vehicle/getVehicleCnt.do",{},{
		success : function(r){
			var data = r.result;
			
			$("#titleCnt1").text(data.used+"<fmt:message key='searchinfo.ea'/>");
		}
	});
	
	
	if(!lazy)
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
				searchOrder : $("#searchOrder")
				,searchDeviceSn : $("#searchDeviceSn")
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/com/getVehicleList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.rtvList;				
				var strHtml = '';
				
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += '<tr>';
					strHtml += '<th class="">';					
					strHtml += '<a href="#none" class="btn_vehicleDetail" data-key="'+vo.vehicleKey+'">';
					strHtml += '<span>'+vo.modelMaster+'</span>';
					strHtml += vo.plateNum;
					strHtml += '</a>';
					strHtml += '</th>';
					strHtml += '<td class="">';					
					strHtml += '<span>'+vo.deviceSeries+'</span>';
					strHtml += '</br>'+vo.deviceSn;
					strHtml += '</td>';
					strHtml += '<td>';
					strHtml += '<input type="text" style="border:1px solid black;width:80px;margin-left:0px;margin-right:20px;" class="numberOnly" value="'+(vo.totDistance/1000).toFixed(0)+'" />';
					strHtml += '</td>';
					strHtml += '<td>';
					strHtml += '<div class="btn_group2">';
					strHtml += '<a href="#none" class="deviceSound '+((vo.deviceSound.length==0||vo.deviceSound=='1')?'on':'')+'" data-val="1" style="line-height: 25px;text-align:center">ON</a>';
					strHtml += '<a href="#none" class="deviceSound '+((vo.deviceSound=='0')?'on':'')+'" data-val="0" style="line-height: 25px;text-align:center;">OFF</a>';
					strHtml += '</div>';
					strHtml += '</td>';
					strHtml += '<td>';
					var dbNm = "";
					var firmNm = "";
					$VDAS.http_post("/vehicle/getVerInfoVehicle.do",{vehicleKey:vo.vehicleKey},{
						sync : true
						,success : function(r){
							var d = r.result;
							dbNm = d.vehicleVer;
							firmNm = d.firmware;
						}
					});
					
					strHtml += vo.dbNm;
					
					if(dbNm == vo.dbNm)
						strHtml += "</br>"+"<span style='color:black'><fmt:message key='pcMngUpdate.latest'/></span>";	
					if(dbNm != vo.dbNm)
						strHtml += "</br>"+"<span style='color:red'><fmt:message key='pcMngUpdate.needUpdate'/></span>";
					
					strHtml += '</td>';
					strHtml += '<td>';
					strHtml += vo.firmNm;
					if(firmNm == vo.firmNm)
						strHtml += "</br>"+"<span style='color:black'><fmt:message key='pcMngUpdate.latest'/></span>";	
					if(firmNm != vo.firmNm)
						strHtml += "</br>"+"<span style='color:red'><fmt:message key='pcMngUpdate.needUpdate'/></span>";
					
					
					strHtml += '</td>';
					strHtml += '<td>';
					strHtml += '<a class="btn_update" data-key="'+vo.connectionKey+'" data-convcode="'+vo.convCode+'"><img src="${pathCommon}/images/${_LANG}/btn_update_red.png" /></a>';
					strHtml += '</td>';
					strHtml += '</tr>';
					
					
				}
				
				return strHtml;

			}
		});
	
	$("#searchDeviceSn").on("keyup",function(e){
		if(e.keyCode == 13) $.lazyLoader.search(lazy);
	});
	
});

function pcMonitor(){
	$VDAS.http_post("/config/monitorPcmng.do",{},{
		success:function(r){
			if(r.result == "1"){
				$.lazyLoader.search(lazy);
				$(".black").fadeOut();
			}else if(r.repeat=="1") pcMonitor();
		}
	});
}

</script>
<div class="right_layout">
	<form id="searchFrm">
		<input type="hidden" name="searchOrder" />
		<input type="hidden" name="searchUsed" value="1" />
		<input type="hidden" name="searchDeviceSn" />
	</form>
	<h2 class="tit_sub">
		<fmt:message key='pcMngUpdate.update'/>
		<p><fmt:message key='pcMngUpdate.updateTxt'/></p>
	</h2>
	<div class="sub_top1">
		<span><fmt:message key='pcMngUpdate.deviceCnt'/> <b id="titleCnt1">0<fmt:message key='pcMngUpdate.ea'/></b> </span>
		<strong id="nowDate"></strong>
	</div>
	<h3 class="h3tit"><fmt:message key='pcMngUpdate.updateProc'/></h3>
	<div class="box3">
		<span style="width: 100%;text-align: center;padding-top:20px;font-size:12px;font-weight: 600;color: #424242">① <fmt:message key='pcMngUpdate.txt1'/></span>
		<div style="width: 100%;text-align: center;padding-top:0px;font-size:12px;font-weight: 600;color: #828282;text-align:left"><fmt:message key='pcMngUpdate.txt2'/> <a class="pcDriverDown" href="#none" style="text-decoration: underline;color:#2a20eb"><fmt:message key='pcMngUpdate.txt3'/></a><fmt:message key='pcMngUpdate.txt4'/>
		</br><fmt:message key='pcMngUpdate.txt5'/>
		</br>※ <fmt:message key='pcMngUpdate.txt6'/></div>
		</br><span style="width: 100%;text-align: center;padding-top:20px;font-size:12px;font-weight: 600;color: #424242">② <fmt:message key='pcMngUpdate.txt7'/></span>
		</br><span style="width: 100%;text-align: center;padding-top:20px;font-size:12px;font-weight: 600;color: #424242">③ <fmt:message key='pcMngUpdate.txt8'/></span>
		<span><a class="pcMngDown right" href="#none" ><fmt:message key='pcMngUpdate.download'/></a></span>
	</div>
	<h3 class="h3tit"><fmt:message key='pcMngUpdate.deviceSearch'/></h3>
	<div class="tab_layout">
		<div style="margin-bottom: 10px;">
			<select id="searchOrder" class="select form" style="width:105px;">
				<option value="regDate"><fmt:message key='pcMngUpdate.orderReg'/></option>
			</select>
			<input type="text" class="w172" id='searchDeviceSn'  style="display: inline-block;vertical-align: top;width: 123px;height: 25px;border: 1px solid #c3c3c3;padding-left: 10px;"/>
		</div>	
		<table class="table1 mgt10" id="contentsTable">
			<colgroup>
				<col width="*">
				<col width="110">
				<col width="90">
				<col width="120">
				<col width="120">
				<col width="120">
				<col width="115">
			</colgroup>
			<thead>			
				<tr>
					<th><fmt:message key='pcMngUpdate.header1'/></th>
					<th><fmt:message key='pcMngUpdate.header2'/></th>
					<th><fmt:message key='pcMngUpdate.header3'/></th>
					<th><fmt:message key='pcMngUpdate.header4'/></th>
					<th><fmt:message key='pcMngUpdate.header5'/></th>
					<th><fmt:message key='pcMngUpdate.header6'/></th>
					<th><fmt:message key='pcMngUpdate.header7'/></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<div class="black" style="text-align: center">
	<div style="width: 100%;text-align: center;padding-top:300px;"></div>
	<div style="width: 100%;text-align: center;padding-top:20px;font-size:12px;font-weight: 600;color: #424242;background: white"><fmt:message key='pcMngUpdate.txt9'/></div>
	<div style="width: 100%;text-align: center;padding-top:20px;font-size:12px;font-weight: 600;color: #424242;background: white"><fmt:message key='pcMngUpdate.txt10'/> '<a class="pcMngDown btn_mngDown2" href="#none"  style="text-decoration: underline;color:#2a20eb"><fmt:message key='pcMngUpdate.txt11'/></a>' <fmt:message key='pcMngUpdate.txt12'/></div>
	<div style="width: 100%;text-align: center;padding-top:20px;padding-bottom:20px;font-size:12px;font-weight: 600;color: #828282;background: white">※ <fmt:message key='pcMngUpdate.txt13'/></div>	
</div>
