<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<c:if test="${VDAS.rule eq '0' or VDAS.rule eq '1' }">
<script type="text/javascript">
var lazy;
var lazyManage;
$(document).ready(function(){

	initMenuSel("M0004");
    
	//workAreaKey groupKey corpGroupNm accountKey accountNm corpPosition lev1Cd lev1Nm lev2Nm lev2Cd
	//0ac0e925-678d-11e7-ac63-0002b3da41165f1a4f22-5ca2-11e7-8f4a-0002b3da4117KCC TEST8165426f-2626-11e7-a63a-0002b3da4116 정종웅 NULL 26 부산광역시 강서구 26440

	
	$VDAS.ajaxCreateSelect("/com/getGroupCode.do",{},$("#corpGroup"),false,"전체 부서","${rtv.groupKey}","${_LANG}",null,function(){
		$VDAS.ajaxCreateSelect("/com/getGroupUserList.do",{corpGroup:"${rtv.groupKey}"},$("#accountKey"),false,"전체 사용자","${rtv.accountKey}","${_LANG}",null,null);
	});
	
	$("#corpGroup").on("change",function(){
		$VDAS.ajaxCreateSelect("/com/getGroupUserList.do",{corpGroup:$(this).val()},$("#accountKey"),false,"전체 사용자","","${_LANG}",null,null);	
	});
	
	
	$VDAS.ajaxCreateSelect("/com/getCityLev1.do",{},$(".cityLev1"),false,"전체 시/도","${rtv.lev1Cd}","${_LANG}",null,function(){
		$VDAS.ajaxCreateSelect("/com/getCityLev2.do",{city:"${rtv.lev1Cd}"},$(".cityLev1").next(),false,"전체 시/군/구","${rtv.lev2Cd}","${_LANG}",null,null);	
	});
	$("body").on("change",'.cityLev1',function(){
		$VDAS.ajaxCreateSelect("/com/getCityLev2.do",{city:$(this).val()},$(this).next(),false,"전체 시/군/구","","${_LANG}",null,null);	
		
	});
	
	
	$("body").on("click",".ys_close",function(){
		$(this).closest("div.addr_sel").remove();
	});
	
	<c:if test="${empty rtv.workAreaKey }">
	$(".btn_append").on("click",function(){
		var strHtml = '';

		strHtml += '<div class="addr_sel">';
		strHtml += '<select class="select form cityLev1" style="width: 160px;"></select>';
		strHtml += '<select class="select form cityLev2" style="width: 160px;"></select>';
		strHtml += '<a href="#none" class="ys_close"><img src="${pathCommon}/images/close4.jpg" alt="" /></a>';
		strHtml += '</div>';
		
		$(this).before(strHtml);
		
		var divObj = $(this).prev();
		
		$VDAS.ajaxCreateSelect("/com/getCityLev1.do",{},divObj.find(".cityLev1"),false,"전체 시/도","","${_LANG}",false,null);
	});
	
	$("body").on("click",".btn_regist",function() {
		var valid = true;
		$(".cityLev1").each(function(){
			var val = $(this).val();
			if(val.length == 0) {
				valid&&alert("시/도는 필수 항목입니다.");
				valid = false;
				
			}
		});
		
		if(valid){
			
			var corpGroup = $("#corpGroup").val();
			var account = $("#accountKey").val();

			if(account.length == 0){
				
				if(corpGroup.length == 0){
					$("#dialogTextGroup").text("");
					$("#dialogTextUser").text("모든 사용자");
				}else{
					$("#dialogTextGroup").text("부서");
					$("#dialogTextUser").text($('#corpGroup option:selected').text());	
				}
				
			}else{
				$("#dialogTextGroup").text("직원");
				$("#dialogTextUser").text($('#accountKey option:selected').text());
			}
			
			if($(".cityLev1").length == 1){
				
				var strText = $(".cityLev1 option:selected").text()+" "+(($(".cityLev2").val().length == 0)?"전체":$(".cityLev2 option:selected").text());
				$("#dialogTextArea").text(strText);
				
			}else{
				$("#dialogTextArea").text("선택된 지역으");
			}
			
		    $( "#dialog" ).dialog({		    	
		        width:800,
		        dialogClass:'alert',
		        draggable: false,
		        modal:true,
		        height:"auto",
		        resizable:false,
		        closeOnEscape:true,
		        buttons:{
		            '확인':function(){
		            	
		            	var param = {};
		            	
		            	param.corpGroup = $("#corpGroup").val();		            	
		            	param.accountKey = $("#accountKey").val();
		            	
		            	param.cityLev1 = "";
		            	param.cityLev2 = "";
		            	
		            	$(".cityLev1").each(function(){
		            		var val = $(this).val();
		            		
		            		if(param.cityLev1) param.cityLev1 = param.cityLev1+","+val;
		            		else param.cityLev1 = val;
		            		
		            	});
		            	
		            	$(".cityLev2").each(function(i){
		            		var val = $(this).val();
		            		
		            		if(i == 0) param.cityLev2 = val;
		            		else param.cityLev2 = param.cityLev2+","+val;
		            		
		            	});
		            	
		            	
		            	$VDAS.http_post("/config/addWorkingArea.do",param,{
		            		success : function(r){
		            			alert("저장되었습니다.");
		            			$( "#dialog" ).dialog("close");
		            			document.location.href = "${path}/config/registedWorkingArea.do";
		            		},error : function(r){
		            			var obj = JSON.parse(r.responseText).result;
		            			alert($message[obj]);
		            		}
		            	});
		            	
		            	
		            	
		                
		            },
		            '취소':function(){
		            	$(this).dialog("close");
		            },
	
		        }
		    });
		   
		}
	});
	</c:if>
	
	<c:if test="${not empty rtv.workAreaKey }">
		$(".btn_edit").on("click",function(){
			var corpGroup = $("#corpGroup").val();
			var account = $("#accountKey").val();

			if(account.length == 0){
				
				if(corpGroup.length == 0){
					$("#dialogTextGroup").text("");
					$("#dialogTextUser").text("모든 사용자");
				}else{
					$("#dialogTextGroup").text("부서");
					$("#dialogTextUser").text($('#corpGroup option:selected').text());	
				}
				
			}else{
				$("#dialogTextGroup").text("직원");
				$("#dialogTextUser").text($('#accountKey option:selected').text());
			}
			
			if($(".cityLev1").length == 1){
				
				var strText = $(".cityLev1 option:selected").text()+" "+(($(".cityLev2").val().length == 0)?"전체":$(".cityLev2 option:selected").text());
				$("#dialogTextArea").text(strText);
				
			}else{
				$("#dialogTextArea").text("선택된 지역으");
			}
			
		    $( "#dialog" ).dialog({		    	
		        width:800,
		        dialogClass:'alert',
		        draggable: false,
		        modal:true,
		        height:"auto",
		        resizable:false,
		        closeOnEscape:true,
		        buttons:{
		            '확인':function(){
		            	
		            	var param = {};
		            	
		            	param.corpGroup = $("#corpGroup").val();		            	
		            	param.accountKey = $("#accountKey").val();
		            	
		            	param.cityLev1 = "";
		            	param.cityLev2 = "";
		            	
		            	$(".cityLev1").each(function(){
		            		var val = $(this).val();
		            		
		            		if(param.cityLev1) param.cityLev1 = param.cityLev1+","+val;
		            		else param.cityLev1 = val;
		            		
		            	});
		            	
		            	$(".cityLev2").each(function(i){
		            		var val = $(this).val();
		            		
		            		if(i == 0) param.cityLev2 = val;
		            		else param.cityLev2 = param.cityLev2+","+val;
		            		
		            	});
		            	
		            	param.workAreaKey = "${rtv.workAreaKey}"; 
		            	
		            	$VDAS.http_post("/config/updateWorkingArea.do",param,{
		            		success : function(r){
		            			alert("저장되었습니다.");
		            			$( "#dialog" ).dialog("close");
		            			document.location.href = "${path}/config/registedWorkingArea.do";
		            		},error : function(r){
		            			var obj = JSON.parse(r.responseText).result;
		            			alert($message[obj]);
		            		}
		            	});
		            	
		            	
		            	
		                
		            },
		            'cancel':function(){
		            	$(this).dialog("close");
		            },
	
		        }
		    });
		});
	
	
	</c:if>
	
	 $('.pop_close').click(function(){
        $(this).parent().dialog("close");
    });
});

</script>

<div class="right_layout">
	<div class="detail_view">
		<h2 class="h2tit">이수지역 등록</h2>
		<div class="gray_box">
			<p>이수지역 등록시, 운행기록부의 기준데이터로 설정되니 정확히 입력해주세요.</p>
		</div>
		<div class="ysup">
			<dl>
				<dt>등록 대상</dt>
				<dd>
					<select class="select form" id='corpGroup' style="width: 160px;">
						<option value="">전체 부서</option>
					</select>
					<select class="select form" id="accountKey" style="width: 160px;">
						<option value="">전체 사용자</option>
					</select>
				</dd>
			</dl>
			<dl>
				<dt>상세 지역</dt>
				<dd>
					<div class="addr_sel">
						<select class="select form cityLev1" style="width: 160px;">
						</select>
						<select class="select form cityLev2" style="width: 160px;">
							<option vlaue="">전체 시/군/구</option>
						</select>
					</div>
					<c:if test="${empty rtv.workAreaKey }">
						<div class="btn_append"></div>
					</c:if>
				</dd>
			</dl>
		</div>
		<!-- 버튼 -->
		<div class="btn_area2"><a href="javascript:window.history.back();" class="btn_cancle">취소</a>
			<c:if test="${empty rtv.workAreaKey }">
				<a href="#none" class="btn_regist">등록</a>
			</c:if>
			<c:if test="${not empty rtv.workAreaKey }">
				<a href="#none" class="btn_edit">수정</a>
			</c:if>
		</div>
	</div>
</div>
<div id="dialog" title="이수지역 등록" class="myPopup" style="display:none">
	<div class="pop_close"></div>
	<p class="pop_txt">
		<span id="dialogTextUser">정준하jana0712@jastecm.com(아이피앤/영업부/대리)</span> <span id="dialogTextGroup">직원</span>을/를 <em id="dialogTextArea">서울특별시 강동구</em>로
		<br />이수지역 <c:if test="${empty rtv.workAreaKey }">등록</c:if><c:if test="${not empty rtv.workAreaKey }">수정</c:if>하시겠습니까?
	</p>
</div>
</c:if>
<c:if test="${VDAS.rule ne '0' and VDAS.rule ne '1' }">
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("M0004");
});
</script>
<div class="right_layout">
	<img src="${pathCommon}/images/${_LANG}/authError.png" alt="" style="width:100%">
</div>
</c:if>