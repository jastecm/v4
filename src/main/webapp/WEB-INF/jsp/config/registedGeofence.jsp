<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<c:if test="${VDAS.rule eq '0' or VDAS.rule eq '1' }">
<script type="text/javascript">
var lazy;
$(document).ready(function(){
	//left menu on
	initMenuSel("M0007");
	
	$("#btn_payment").on("click",function(){		
		var param = {};
		param.productType="geoFence";
		
		$VDAS.instance_post("/user/payRequest.do",param);
	});
	
	
	
	
	if(!lazy)
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 9999
			,loadUrl : "/config/registedGeofenceList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.result;
				var strHtml = "";
				$("#totCnt").text(data.length+"개");
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += "<tr>";
					strHtml += '<td><input type="checkbox" class="selObj" data-key="'+vo.fenceKey+'"/></td>';
					strHtml += '<td>'+vo.alies+'</td>';
					strHtml += '<th>'+vo.addr+'</th>';
					strHtml += '<td><a href="#none" class="btn_map" data-key="'+vo.fenceKey+'" data-lat="'+vo.lat+'" data-lon="'+vo.lon+'" data-addr="'+vo.addr+'" data-alies="'+vo.alies+'"><img src="${pathCommon}/images/gps.jpg" alt=""></a></td>';					
					strHtml += '<td>';
					strHtml += '<div class="btn_group2">';
					strHtml += '<a href="#none" class="onoff '+((vo.push=='1')?'on':'')+'" data-val="1" data-key="'+vo.fenceKey+'" style="line-height: 25px;text-align:center">ON</a>';
					strHtml += '<a href="#none" class="onoff '+((vo.push=='0')?'on':'')+'" data-val="0" data-key="'+vo.fenceKey+'" style="line-height: 25px;text-align:center;">OFF</a>';
					strHtml += '</div>';
					strHtml += '</td>';
					strHtml += '</tr>';	
				}
				
				
				return strHtml;
			}
		});
	
	$("body").on("click",".onoff",function(){
		var _this = $(this);
		$VDAS.http_post("/config/updateGeofenceOnOff.do",{key:$(this).data("key"),val:$(this).data("val")},{
			success : function(r){
				_this.closest("td").find(".onoff").removeClass("on");
				_this.addClass("on");
			}
		});
	});
	
	$("#allChker").on("click",function(){
		if($(this).is(":checked"))
			$(".selObj","#contentsTable tbody").prop("checked",true);
		else $(".selObj","#contentsTable tbody").prop("checked",false);
	});
	
	$(".btn_del").on("click",function(){
		var b = true;
		var selObj = $(".selObj:checked");

		if(b){
			if(selObj.length == 0){
				alert("삭제할 거래처를 선택해주세요.");
				return;
			}
			
			$( "#dialog" ).dialog({
		        width:800,
		        dialogClass:'alert',
		        draggable: false,
		        modal:true,
		        height:"auto",
		        resizable:false,
		        closeOnEscape:true,
		        buttons:{
		            '확인':function(){
		            	
		            	var strKey = "";
		            	$(".selObj:checked").each(function(){
		            		strKey += strKey==""?$(this).data("key"):","+$(this).data("key");
		            	});
		            	
		            	$VDAS.http_post("/config/deleteGeofence.do",{fenceKey:strKey},{
		            		success : function(r){
		            			alert("처리되었습니다.");
		            			$("#dialog").dialog("close");
		            			$.lazyLoader.search(lazy);
		            		}
		            	});
		            	
		               
		            },'취소':function(){
		            	$(this).dialog("close");
		            }
		        }
		    });
		}
	});
	
	$("#btn_regist").on("click",function(){
		$VDAS.instance_post("/config/registGeofence.do",{});
	});
	
	
	$("body").on("click",".btn_map",function(){		

		var lat = $(this).data("lat");
		var lon = $(this).data("lon");
		var addr = $(this).data("addr");
		var alies = $(this).data("alies");
		var key = $(this).data("key");

		$("#mapFrame").attr("src",_defaultPath+"/map/mapPoint.do?lat="+lat+"&lon="+lon);
		$("#updateDialog").attr("title",alies);
		$("#alies").val(alies);
		$("#popAddr").text(addr);
		$("#fenceKey").val(key);
		
		$( "#updateDialog" ).dialog({
	        width:800,
	        dialogClass:'alert',
	        draggable: false,
	        modal:true,
	        height:"auto",
	        resizable:false,
	        closeOnEscape:true,
	        buttons:{
	            '수정':function(){
	            	var p = $("#updateFrm").serialize();
	            	$VDAS.http_post("/config/updateGeofence.do",$("#updateFrm").serialize(),{
	            		success : function(r){
	            			alert("저장되었습니다.");
	            			$.lazyLoader.search(lazy);
	            			$( "#updateDialog" ).dialog("destroy");		
	            		},error : function(r){
	    					if($message[r.responseJSON.result])
	    						alert($message[r.responseJSON.result]);
	    					else {
	    						alert(r.status+"\n"+r.statusText);
	    					}
	    				}
	            	});
	            },'취소':function(){
	            	$(this).dialog("destroy");
	            }
	        }
	    });
	});
	
	
	$(".pop_close").on("click",function(){
		$(this).parent("div").dialog("destroy");
	});
	/*
	
	$("body").on("click",".btn_moveDetail",function(){
		var key = $(this).data("key");
		
		$VDAS.instance_post("/config/registWorkingAreaDetail.do",{workAreaKey:key});
		
	});
	*/
});


var setAddr = function(addr){
	$("#addr").val(addr);
	$("#popAddr").text(addr);
}
var setLonLat = function(lon,lat){
	$("#lon").val(lon);
	$("#lat").val(lat);	
}





</script>
<form id="searchFrm" style="display:none">
</form>
<div class="right_layout">
	<h2 class="tit_sub">
		거래처 등록
		<p>등록한 거래처 1km이내 접근 시, 직원의 운행스케쥴에 기록됩니다.</p>
	</h2>
	<div class="sub_top1">
		<span>등록 거래처 수 <b id="totCnt">0개</b> </span>
		<span>최대 등록가능 거래처 수 <b id="totCnt2">${VDAS.geoMaxCnt}개</b> </span>
		<strong id="nowDate"></strong>
	</div>
	<h3 class="h3tit">거래처 등록</h3>
	<div class="box3">
		<div style="width: 100%;text-align: center;padding-top:0px;font-size:12px;font-weight: 600;color: #828282;text-align:left">
			10개의 거래처를 무료로 등록할 수 있습니다.
			</br>이후 거래처 추가를 원하시면, 부가서비스(10개당 11,000원)을 이용해보세요.
			</br>이 서비스는 최초 1번반 결제하면 계속해서 서비스를 이용할 수 있습니다. <a href="#none" id="btn_payment" style="text-decoration: underline;color:#2a20eb">부가서비스 신청하기</a>
		</div>
		<span><a class="right" href="#none" id="btn_regist">거래처 등록</a></span>
	</div>
	<h3 class="h3tit">거래처 목록</h3>
	<div class="tab_layout">
		<div class="align_right mgt30">
			<a href="#none" class="btn_del"><img src="${pathCommon}/images/${_LANG}/btn_delete.jpg" alt=""></a>
		</div>
		<table class="table1 mgt10" id="contentsTable">
			<colgroup>
				<col width="65">
				<col width="200">
				<col width="*">
				<col width="70">
				<col width="150">
			</colgroup>
			<thead>			
				<tr>
					<th><input type="checkbox" id="allChker" /></th>
					<th>거래처명</th>
					<th>주소</th>
					<th>상세위치</th>
					<th>실시간 진입 이탈 알림</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<div id="dialog" title="거래처 삭제" class="myPopup" style="display:none">
	<div class="pop_close"></div>
	<p class="pop_txt">
		선택된 거래처를 삭제하시겠습니까?		
	</p>
</div>
<div id="updateDialog" title="거래처 상세" class="myPopup" style="display:none">
	<div class="pop_close"></div>
	<div class="repair_layout2" style="padding-top:0px">
		<div class="repair_box1">
			<div class="tit" id="popAddr" style="position: ;top:0px">상세주소</div>
		</div>
	</div> 
	<div style="width:100%;height: 450px;margin-top:40px;">
		<iframe id="mapFrame" style="width:100%;height: 100%"></iframe>
	</div>
	<form id="updateFrm">
		<input type="hidden" name="lat" id="lat">
		<input type="hidden" name="lon" id="lon">
		<input type="hidden" name="addr" id="addr">
		<input type="hidden" name="alies" id="alies">
		<input type="hidden" name="fenceKey" id="fenceKey">
	</form>
</div>
</c:if>
<c:if test="${VDAS.rule ne '0' and VDAS.rule ne '1' }">
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("M0007");
});
</script>
<div class="right_layout">
	<img src="${pathCommon}/images/${_LANG}/authError.png" alt="" style="width:100%">
</div>
</c:if>