<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<c:if test="${VDAS.rule eq '0' or VDAS.rule eq '1' }">
<script type="text/javascript">
$(document).ready(function(){
	
	initMenuSel("M0005");

	$VDAS.ajaxCreateSelect("/com/getGroupCode.do",{},$("#corpGroup"),false,"전체 부서","${rtv.groupKey}","${_LANG}",null,function(){
		$VDAS.ajaxCreateSelect("/com/getGroupUserList.do",{corpGroup:"${rtv.groupKey}"},$("#accountKey"),false,"전체 사용자","${rtv.accountKey}","${_LANG}",null,null);
	});
	
	$("#corpGroup").on("change",function(){
		$VDAS.ajaxCreateSelect("/com/getGroupUserList.do",{corpGroup:$(this).val()},$("#accountKey"),false,"전체 사용자","","${_LANG}",null,null);	
	});

	$(".btn_save").on("click",function(){
		var rtv = true;
		var startH = $("select[name=startH]").val();
		var startM = $("select[name=startM]").val();
		var endH = $("select[name=endH]").val();
		var endM = $("select[name=endM]").val();

		if(startH.length == 0 || startM.length == 0 || endH.length == 0 || endM.length == 0){
			alert("업무시간을 설정해야 합니다.");
			rtv = false;
		}

		var sDate = new Date(1900,1,1,startH,startM,0).getTime();
		var eDate = new Date(1900,1,1,endH,endM,0).getTime();

		if(sDate>=eDate){
			rtv&&alert("업무시간이 잘못 되었습니다.");
			rtv = false;
		}
		
		var chkValid = 0;
		var dayArr = [];
		$("#chkArea input[type=checkbox]").each(function(i){
			if($(this).is(":checked")){
				chkValid++;
				dayArr[i] = $(this).val();
			}else dayArr[i] = null;
		});
		if(chkValid == 0){
			rtv&&alert("업무요일을 설정해야 합니다.");
			rtv = false;
		}
		
		if(rtv){
			var param = {};

        	param.corpGroup = $("#corpGroup").val();		            	
        	param.accountKey = $("#accountKey").val();
        	
        	param.startH = startH;
        	param.startM = startM;
        	param.endH = endH;
        	param.endM = endM;
        	param.day = '';
        	for(var i = 0;i < dayArr.length;i++){
        		param.day += dayArr[i];
        		if(i != dayArr.length - 1)
        			param.day += ',';
        	}
        	console.log(param);
        	/*
        	param.dayMon = dayArr[0];
        	param.dayTue = dayArr[1];
        	param.dayWed = dayArr[2];
        	param.dayThu = dayArr[3];
        	param.dayFri = dayArr[4];
        	param.daySat = dayArr[5];
        	param.daySun = dayArr[6];
	*/		

			<c:if test="${empty rtv.workTimeKey }">
				$VDAS.http_post("/config/addWorkingTime.do",param,{
	        		success : function(r){
	        			alert("저장되었습니다.");
	        			document.location.href = "${path}/config/registedWorkingTime.do";
	        		},error : function(r){
	        			var obj = JSON.parse(r.responseText).result;
	        			alert($message[obj]);
	        		}
	        	});
			</c:if>
			<c:if test="${not empty rtv.workTimeKey }">
				param.workTimeKey = "${rtv.workTimeKey}";
				$VDAS.http_post("/config/updateWorkingTime.do",param,{
	        		success : function(r){
	        			alert("수정되었습니다.");
	        			document.location.href = "${path}/config/registedWorkingTime.do";
	        		},error : function(r){
	        			var obj = JSON.parse(r.responseText).result;
	        			alert($message[obj]);
	        		}
	        	});
			</c:if>
		}
	});
		
});

</script>
<div class="right_layout">
	<div class="detail_view">
		<a href="javascript:window.history.back();" class="btn_close"><img src="${pathCommon}/images/close.jpg" alt="" /></a>
		<h2 class="h2tit">
			업무시간 등록
		</h2>
		<div class="sub_top1 sub_top2 mab0">
			<span>기본업무시간은 09:00~18:00이며, 운행기록부의 기준데이터로 설정되니 정확히 입력해주세요.</span>
		</div>
		<div class="repair_layout2">
			<div class="repair_box1 repair_box1_1">
				<div class="tit">업무시간 등록 대상</div>
				<div class="repair_box2 borB1">
					<div class="repair_box2_1">
						<span class="tit3">소속</span>
						<div class="user_txt1">개인</div>
					</div>
					<div class="repair_box2_1">
						<span class="tit3">등록 대상</span>
						<select class="select form" id="corpGroup" style="width: 160px;">
							<option value="">전체 부서</option>
						</select>
						<select class="select form" id="accountKey" style="width: 160px;">
							<option value="">전체 사용자</option>
						</select>
					</div>
					<br><br>
				</div>
			</div>
		</div>
		<div class="repair_layout2 repair_layout2_5">
			<div class="repair_box1 repair_box1_1">
				<div class="tit">업무시간 상세설정</div>
				<div class="repair_box2">
					<div class="repair_box2_1 repair_box2_2">
						<div>
							<div>
								<span class="tit3">업무시작</span>
								<div class="select_type5">
									<div>
										<select class="sel" name="startH">
											<c:forEach begin="0" end="23" step="1" var="i">
												<option value="${i}" <c:if test='${i eq rtv.startH}'>selected</c:if>>${i}</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<span class="r5">시</span>
								<div class="select_type5">
									<div>
										<select class="sel" name="startM">
											<c:forEach begin="0" end="59" step="1" var="i">
												<option value="${i}" <c:if test='${i eq rtv.startM}'>selected</c:if>>${i}</option>
											</c:forEach>									
										</select>
									</div>
								</div>
								<span class="r5">분&nbsp;&nbsp;~&nbsp;&nbsp;</span>
								<span class="tit3">업무종료</span>
								<div class="select_type5">
									<div>
										<select class="sel" name="endH">
											<c:forEach begin="0" end="23" step="1" var="i">
												<option value="${i}" <c:if test='${i eq rtv.endH}'>selected</c:if>>${i}</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<span class="r5">시</span>
								<div class="select_type5">
									<div>
										<select class="sel" name="endM">
											<c:forEach begin="0" end="59" step="1" var="i">
												<option value="${i}" <c:if test='${i eq rtv.endM}'>selected</c:if>>${i}</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<span class="r5">분</span>
							</div>
							<br>
							<div>
								<span class="tit3">업무요일</span>
								<div class="chk_area1" id="chkArea">
									<input type="checkbox" id="chk1" name="dayMon" value="1" <c:if test='${not empty rtv.dayMon}'>checked</c:if>/><label for="chk1">월</label>
									<input type="checkbox" id="chk2" name="dayTue" value="1" <c:if test='${not empty rtv.dayTue}'>checked</c:if>/><label for="chk2">화</label>
									<input type="checkbox" id="chk3" name="dayWed" value="1" <c:if test='${not empty rtv.dayWed}'>checked</c:if>/><label for="chk3">수</label>
									<input type="checkbox" id="chk4" name="dayThu" value="1" <c:if test='${not empty rtv.dayThu}'>checked</c:if>/><label for="chk4">목</label>
									<input type="checkbox" id="chk5" name="dayFri" value="1" <c:if test='${not empty rtv.dayFri}'>checked</c:if>/><label for="chk5">금</label>
									<input type="checkbox" id="chk6" name="daySat" value="1" <c:if test='${not empty rtv.daySat}'>checked</c:if>/><label for="chk6">토</label>
									<input type="checkbox" id="chk7" name="daySun" value="1" <c:if test='${not empty rtv.daySun}'>checked</c:if>/><label for="chk7">일</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="btn_area2"><a href="javascript:window.history.back();" class="btn_cancel">취소</a>
			<c:if test="${empty rtv.workTimeKey }">
				<a href="#none" class="btn_save">등록완료</a>
			</c:if>
			<c:if test="${not empty rtv.workTimeKey }">
				<a href="#none" class="btn_save">수정</a>
			</c:if>
		</div>
	</div>
</div>
</c:if>
<c:if test="${VDAS.rule ne '0' and VDAS.rule ne '1' }">
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("M0005");
});
</script>
<div class="right_layout">
	<img src="${pathCommon}/images/${_LANG}/authError.png" alt="" style="width:100%">
</div>
</c:if>