<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<fmt:setLocale value="${_LANG}" scope="request" />
<fmt:setBundle basename="properties.messages.rest.message" />

<c:if test="${VDAS.rule eq '0' or VDAS.rule eq '1' }">
<style>.svg-clipped{-webkit-clip-path:url(#svgPath);clip-path:url(#svgPath);}</style>
<svg height="0" width="0">
	<defs>
		<clipPath id="svgPath" clipPathUnits="objectBoundingBox">
  			<circle cx=".5" cy=".5" r=".5" />
   		</clipPath>
	</defs>
</svg>
<script type="text/javascript">
var lazy;
$(document).ready(function(){
	initMenuSel("M0001");
	
	$(".btn_pop7").click(function() {
		
		$( "#dialog" ).dialog({
	        width:800,
	        dialogClass:'alert',
	        draggable: false,
	        modal:true,
	        height:"auto",
	        resizable:false,
	        closeOnEscape:true,
	        buttons:{
	        	'<fmt:message key='registvehicledetail.cancle'/>':function(){
         			$(this).dialog("close");
         		}
	            ,'<fmt:message key='registvehicledetail.confirm'/>':function(){
	            	var selObj = $('.selPopObj:checked',$(this));
	            	if(!selObj||selObj.length == 0)
	            		alert('<fmt:message key='registvehicledetail.txt'/>');
	            	else{
	            		var accountKey = selObj.data('accountkey'); 
		            	var accountNm = selObj.data('accountnm');
		            	var name = selObj.data('name');
		            	$("#fixedUserSearchbox").hide();
		            	$("#fixedUser").val(accountKey);
		            	$("#fixedUserTxt").text(accountNm+"/"+name).show();
		    			
		                $(this).dialog("close");	
	            	}
	            	
	            }
	        }
	    });
		
		$(".btn_close").on("click",function(){
			$( "#dialog" ).dialog("close");
		});
		
		
		if(!lazy)
			lazy = $("#dialog table").lazyLoader({
				searchFrmObj : $("#popupSearchForm")
				,searchFrmVal : {searchOrder:$("#popupSearchForm input[name=searchOrder]")
								,searchUserNm:$("#popupSearchForm input[name=searchUserNm]")
								
				}
				,scrollRow : 4
				,rowHeight : 80
				,limit : 5
				,loadUrl : "/com/getUserList.do"
				,initSearch : true
				,createDataHtml : function(r){
					var data = r.rtvList;
					var strHtml = "";
					for(var i = 0 ; i < data.length ; i++){
						var vo = data[i];
						strHtml += "<tr>";
						strHtml += '<td><input type="radio" name="selPopObj" class="selPopObj" data-accountnm="'+vo.accountNm
						+'" data-accountkey="'+vo.accountKey
						+'" data-corpgroupnm="'+vo.corpGroupNm
						+'" data-name="'+vo.name
						+'"/></td>';
						strHtml += '<th>';
						
						if(vo.imgId&&vo.imgId.length > 0)
							strHtml += '<strong class="car_img"><img class="svg-clipped" style="width:49px;height:49px;" src="${defaultPath}/com/getUserImg.do?uuid='+vo.imgId+'" alt="" /></strong>';
						else strHtml += '<strong class="car_img"><img class="svg-clipped" src="${pathCommon}/images/user_img7.jpg" alt="" style="width:49px;height:49px;"/></strong>';
						strHtml += '<a href="#none">';
						strHtml += '<span>'+vo.accountNm+'</span>';
						strHtml += vo.name;
						strHtml += '</a>';
						strHtml += '</th>';
						strHtml += '<td>';
						strHtml += '<p>'+vo.corpGroupNm+'/'+vo.corpPosition+'</p>';
						strHtml += '</td>';
						strHtml += '<td>';
						strHtml += '<p>'+vo.phone+'</p>';
						strHtml += '<p>'+vo.mobilePhone+'</p>';
						strHtml += '</td>';
						strHtml += '<td>'+vo.descript+'</td>';
						strHtml += '</tr>';	
					}
					
					return strHtml;
				}
			});
		
	});
	
	loadingDevice();
	loadingVehicleDb();
	
	if("${lev}" == "0"){
		$("#deviceSeries").prop("disabled",true);
		$("#deviceId").prop("disabled",true);
		var strHtml = "<input type='hidden' name='deviceId' value='${reqVo.deviceId}' />";
		$("#deviceId").after(strHtml);
		
		$("#manufacture,#modelMaster,#year,#modelHeader,#volume,#transmission,#fuelType").prop("disabled",true);
		//$("#totDistance").prop("readonly",true);
		
	}else if("${lev}" == "1"){ //단말만 못바꿈
		$("#deviceSeries").prop("disabled",true);
		$("#deviceId").prop("disabled",true);
		var strHtml = "<input type='hidden' name='deviceId' value='${reqVo.deviceId}' />";
		$("#deviceId").after(strHtml);
		
		$("#plateNum").val("");
		
		$(".deviceSound").on("click",function(){
			$(".deviceSound").removeClass("on");
			$(this).addClass("on");
			var val = $(this).data("val");
			$("#deviceSound").val(val);
		});
		
	}else if("${lev}" == "2"){ //단말만 바꿀수있음
		$("#addVehicleFrm input").prop("readonly",true);
		$("#manufacture,#modelMaster,#year,#modelHeader,#volume,#transmission,#fuelType").prop("disabled",true);
		
	}else{
		$(".deviceSound").on("click",function(){
			$(".deviceSound").removeClass("on");
			$(this).addClass("on");
			var val = $(this).data("val");
			$("#deviceSound").val(val);
		});
	}

	$(".vehicleBizType").on("click",function(){
		$(".vehicleBizType").removeClass("on");
		$(this).addClass("on");
		var val = $(this).data("val");
		$("#vehicleBizType").val(val);
	});
	
	
	$("#fixedUserChk").on("click",function(){
		if($(this).is(":checked")){
			$(".fixedUserBox").show();
		}else{
			$(".fixedUserBox").hide();
			$("#fixedUserSearchbox").show();
			$("#fixedUser").val("");
        	$("#fixedUserTxt").text("");
		}
	});
	
	$("#btn_next").on("click",function(){
		$("#vehicleBizType").val($(".vehicleBizType.on").data("val"));
		$("#deviceSound").val($(".deviceSound.on").data("val"));
		if($("#fixedUserChk").is(":checked")){
			if(!$("#addVehicleFrm input[name=fixedUser]").val()){
				alert('<fmt:message key='registvehicledetail.txt1'/>');
				return false;
			}
		}
		
		var flag = "${lev}" == "0";
		
		console.log($("#addVehicleFrm").serialize());
		$VDAS.http_post("/config/addTempVehicle.do",$("#addVehicleFrm").serialize(),{
			success:function(r){
				if(flag){
					alert('<fmt:message key='registvehicledetail.txt2'/>');
					$VDAS.instance_post("/config/registedVehicle.do",{});
				}else{
					var deviceId = $("#addVehicleFrm [name=deviceId]").val();
					var vehicleKey = "${reqVo.vehicleKey}";
					$VDAS.instance_post("/config/dvpMng.do",{deviceId:deviceId});					
				}
			},
			error:function(r){
				var obj = JSON.parse(r.responseText).result;
				
				for (var key in obj) {
				  if (obj.hasOwnProperty(key)) {
					  alert($message[obj[key]]);
					  
					  if($("#addVehicleFrm [name="+key+"]")) $("#addVehicleFrm [name="+key+"]").focus();
					  break;
				  }
				}
			}
		});
	});
	
	$("#btn_searchUserPop").on("click",function(){
		$.lazyLoader.search(lazy);
	});
	
	$("#input_searchUserPop").on("keyup",function(e){
		if(e.keyCode == 13) $("#btn_searchUserPop").trigger("click");
	});
	
	
});

function loadingDevice(){
	//url,param,target,listType,defaultVal,initVal,lang,trigger,afterFunk)
	$VDAS.ajaxCreateSelect ("/com/getDeviceSeries.do"
							,{}
							,$("#deviceSeries")
							,1
							,"S"
							,"${reqVo.deviceSeries}"
							,"${_LANG}"
							,null
							,null);
	
	var free = "";
	if("${lev}".length == 0){
		free = "1";
	}else if("${lev}" == "0"){
		free = "";
	}else if("${lev}" == "1"){
		free = "";
	}else if("${lev}" == "2"){
		free = "1";	
	}
	
	var free = "${reqVo.vehicleKey}".length > 0? "" : 1;	
	
	$VDAS.ajaxCreateSelect ("/com/getDeviceList.do"
			,{deviceSeries : "${reqVo.deviceSeries}",free:free}
			,$("#deviceId")
			,null
			,"S"
			,"${reqVo.deviceId}"
			,"${_LANG}"
			,null
			,null);
	
	$("#deviceSeries").on("change",function(){
		var _$this = $(this);
		$VDAS.ajaxCreateSelect ("/com/getDeviceList.do"
				,{deviceSeries : _$this.val(),free:1}
				,$("#deviceId")
				,null
				,"S"
				,""
				,"${_LANG}"
				,null
				,null);
	});
	
}
function loadingVehicleDb(){
	var init = {};
	init.manufacture = "${reqVo.manufacture}"?"${reqVo.manufacture}":"_";
	init.modelMaster = "${reqVo.modelMaster}"?"${reqVo.modelMaster}":"_";
	init.year = "${reqVo.year}"?"${reqVo.year}":"_";
	init.modelHeader = "${reqVo.modelHeader}"?"${reqVo.modelHeader}":"_";
	init.fuelType = "${reqVo.fuelType}"?"${reqVo.fuelType}":"_";
	init.transmission = "${reqVo.transmission}"?"${reqVo.transmission}":"_";
	init.volume = "${reqVo.volume}"?"${reqVo.volume}":"_";
	
	$VDAS.ajaxCreateApiSelect ("/api/vehicleDb/${_LANG}/manufacture"
			,{}
			,$("#manufacture")
			,1
			,"S"
			,init.manufacture
			,"${_LANG}"
			,null
			,null);
	
	$("#manufacture").on("change",function(){
		$("#convCode").val("");
		var _$this = $(this);
		var lev = _$this.data("lev");
		
		$(".vehicleDb").each(function(){
			var thisLev = $(this).data("lev");
			if(lev < thisLev) $(this).html("<option value=''><fmt:message key="registvehicledetail.select2"/></option>");
		});
		
		if(_$this.val())
		$VDAS.ajaxCreateApiSelect ("/api/vehicleDb/${_LANG}/manufacture/"+_$this.val()+"/modelMaster"
				,{}
				,$("#modelMaster")
				,1
				,"S"
				,""
				,"${_LANG}"
				,null
				,null);
		
	});
	
	$VDAS.ajaxCreateApiSelect ("/api/vehicleDb/${_LANG}/manufacture/"+init.manufacture+"/modelMaster"
			,{}
			,$("#modelMaster")
			,1
			,"S"
			,init.modelMaster
			,"${_LANG}"
			,null
			,null);
	
	$("#modelMaster").on("change",function(){
		$("#convCode").val("");
		var _$this = $(this);
		var lev = _$this.data("lev");
		
		$(".vehicleDb").each(function(){
			var thisLev = $(this).data("lev");
			if(lev < thisLev) $(this).html("<option value=''><fmt:message key="registvehicledetail.select3"/></option>");
		});
		
		if(_$this.val())
		$VDAS.ajaxCreateApiSelect ("/api/vehicleDb/${_LANG}/manufacture/"+$("#manufacture").val()+"/modelMaster/"+_$this.val()+"/year"
				,{}
				,$("#year")
				,1
				,"S"
				,""
				,"${_LANG}"
				,null
				,null);
		
	});
	
	$VDAS.ajaxCreateApiSelect ("/api/vehicleDb/${_LANG}/manufacture/"+init.manufacture+"/modelMaster/"+init.modelMaster+"/year"
			,{}
			,$("#year")
			,1
			,"S"
			,init.year
			,"${_LANG}"
			,null
			,null);
	
	$("#year").on("change",function(){
		$("#convCode").val("");
		var _$this = $(this);
		var lev = _$this.data("lev");
		
		$(".vehicleDb").each(function(){
			var thisLev = $(this).data("lev");
			if(lev < thisLev) $(this).html("<option value=''><fmt:message key="registvehicledetail.select4"/></option>");
		});
		
		if(_$this.val())
		$VDAS.ajaxCreateApiSelect ("/api/vehicleDb/${_LANG}/manufacture/"+$("#manufacture").val()+"/modelMaster/"+$("#modelMaster").val()+"/year/"+_$this.val()+"/modelHeader"
				,{}
				,$("#modelHeader")
				,1
				,"S"
				,""
				,"${_LANG}"
				,null
				,null,function(err){
					err.responseJSON?err.responseJSON.result?alert("<fmt:message key="registvehicledetail.txt3_1"/>\n"+(((new Date().getTime()-err.responseJSON.result)/1000).toFixed(0))+"<fmt:message key="registvehicledetail.txt3_2"/>"):null:null;
				});
		
	});
	
	$VDAS.ajaxCreateApiSelect ("/api/vehicleDb/${_LANG}/manufacture/"+init.manufacture+"/modelMaster/"+init.modelMaster+"/year/"+init.year+"/modelHeader"
			,{}
			,$("#modelHeader")
			,1
			,"S"
			,init.modelHeader
			,"${_LANG}"
			,null
			,null);
	
	$("#modelHeader").on("change",function(){
		$("#convCode").val("");
		var _$this = $(this);
		var lev = _$this.data("lev");
		
		$(".vehicleDb").each(function(){
			var thisLev = $(this).data("lev");
			if(lev < thisLev) $(this).html("<option value=''><fmt:message key="registvehicledetail.select"/></option>");
		});
		
		if(_$this.val())
		$VDAS.ajaxCreateApiSelect ("/api/vehicleDb/${_LANG}/manufacture/"+$("#manufacture").val()+"/modelMaster/"+$("#modelMaster").val()+"/year/"+$("#year").val()+"/modelHeader/"+_$this.val()+"/fuelType"
				,{}
				,$("#fuelType")
				,1
				,"S"
				,""
				,"${_LANG}"
				,null
				,null);
		
		if(_$this.val())
		$VDAS.ajaxCreateApiSelect ("/api/vehicleDb/${_LANG}/manufacture/"+$("#manufacture").val()+"/modelMaster/"+$("#modelMaster").val()+"/year/"+$("#year").val()+"/modelHeader/"+_$this.val()+"/volume"
				,{}
				,$("#volume")
				,1
				,"S"
				,""
				,"${_LANG}"
				,null
				,null);
		
		if(_$this.val())
		$VDAS.ajaxCreateApiSelect ("/api/vehicleDb/${_LANG}/manufacture/"+$("#manufacture").val()+"/modelMaster/"+$("#modelMaster").val()+"/year/"+$("#year").val()+"/modelHeader/"+_$this.val()+"/transmission"
				,{}
				,$("#transmission")
				,1
				,"S"
				,""
				,"${_LANG}"
				,null
				,null);
		
	});
	
	$VDAS.ajaxCreateApiSelect ("/api/vehicleDb/${_LANG}/manufacture/"+init.manufacture+"/modelMaster/"+init.modelMaster+"/year/"+init.year+"/modelHeader/"+init.modelHeader+"/fuelType"
			,{}
			,$("#fuelType")
			,1
			,"S"
			,init.fuelType
			,"${_LANG}"
			,null
			,null);
	$VDAS.ajaxCreateApiSelect ("/api/vehicleDb/${_LANG}/manufacture/"+init.manufacture+"/modelMaster/"+init.modelMaster+"/year/"+init.year+"/modelHeader/"+init.modelHeader+"/volume"
			,{}
			,$("#volume")
			,1
			,"S"
			,init.volume
			,"${_LANG}"
			,null
			,null);
	$VDAS.ajaxCreateApiSelect ("/api/vehicleDb/${_LANG}/manufacture/"+init.manufacture+"/modelMaster/"+init.modelMaster+"/year/"+init.year+"/modelHeader/"+init.modelHeader+"/transmission"
			,{}
			,$("#transmission")
			,1
			,"S"
			,init.transmission
			,"${_LANG}"
			,null
			,null);
	
	$("#fuelType,#volume,#transmission").on("change",function(){
		$("#convCode").val("");
		var allchk = true;
		$(".vehicleDb").each(function(){
			
			if(!$(this).val()) {				
				allchk = false;			
			}
		});
		
		if(allchk){
			$VDAS.http_post("/api/vehicleDbCode/${_LANG}/"+$("#manufacture").val()+"/"+$("#modelMaster").val()+"/"+$("#year").val()+"/"+$("#modelHeader").val()+"/"+$("#fuelType").val()+"/"+$("#transmission").val()+"/"+$("#volume").val()+""
					,{}
					,{
						success : function(r){
							var obj = r.result;
							$("#convCode").val(obj.convCode);
						}
					});
			
		}
	});
	
	$("#addLink").on("click",function(){
		document.location.href="http://viewcar.net";
	});
	
}

</script>
<div class="right_layout">
	<form id="addVehicleFrm" method="post">
		<input type="hidden" name="vehicleKey" value="${reqVo.vehicleKey}" >
		<input type="hidden" name="updateLev" value="${lev}" >
		<div class="detail_view">
			<a href="javascript:window.history.back();" class="btn_close"><img src="${pathCommon}/images/close.jpg" alt="" /></a>
			<h2 class="h2tit"><fmt:message key='registvehicledetail.vehicleReg'/></h2>
			<div class="step_box">
				<img src="${pathCommon}/images/${_LANG}/img_caradd_step1.png" alt="" />
			</div>
			
			<div class="repair_layout2">
				<div class="repair_box1 repair_box1_1">
					<div class="tit"><fmt:message key='registvehicledetail.vehicleDefaultInfo'/></div>
					<div class="repair_box2 borB1">
						<div class="repair_box2_1">
							<span class="tit4 txt_right"><fmt:message key='registvehicledetail.corp'/></span>
							<div class="user_txt1">${VDAS.corpNm}</div>
						</div>
						<div class="repair_box2_1">
							<span class="tit4 txt_right"><fmt:message key='registvehicledetail.plateNum'/></span>
							<input type="text" class="w172" name='plateNum' id="plateNum" value='${reqVo.plateNum}' />
						</div>
						<div class="repair_box2_1">
							<span class="tit4 txt_right"><fmt:message key='registvehicledetail.deviceSeries'/></span>
							<select class="select form" style="width: 100px;" id="deviceSeries">
								<option value=""><fmt:message key='registvehicledetail.select'/></option>
							</select>
							<select class="select form" style="width: 240px;" name="deviceId" id="deviceId">
								<option value=""><fmt:message key='registvehicledetail.select2'/></option>
							</select>
						</div>
						<div class="repair_box2_1">
							<span style="padding-left: 30px;font-size: 13px;">※ 구매한 단말기의 배정 신청이 필요합니다. 목록에 구매하신 단말기의 일련번호가 없는 경우, </span>
							<br/>	
							<span style="padding-left: 30px;font-size: 13px;line-height: 25px;"><a href="http://viewcar.net/vdasadd/" target="_blank" style="text-decoration: underline;color:#2a20eb">‘단말기 배정신청’</a>을 진행해주세요. 배정 후에는 이메일로 알려드립니다.</span>
						</div>
						<br/><br/>
					</div>
					<div class="repair_box2 borB1">
						<div class="repair_box2_1">
							<span class="tit4 txt_right"><fmt:message key='registvehicledetail.manufacture'/></span>
							<select class="select form vehicleDb" data-lev='1' style="width: 170px;" id="manufacture">
								<option value="선택"><fmt:message key='registvehicledetail.select3'/></option>
							</select>
							<span class="tit4 txt_right"><fmt:message key='registvehicledetail.modelMaster'/></span>
							<select class="select form vehicleDb" data-lev='2' style="width: 170px;" id="modelMaster">
								<option value="선택"><fmt:message key='registvehicledetail.select4'/></option>
							</select>
						</div>
						<div class="repair_box2_1">
							<span class="tit4 txt_right"><fmt:message key='registvehicledetail.year'/></span>
							<select class="select form vehicleDb" data-lev='3' style="width: 170px;" id="year">
								<option value="선택"><fmt:message key='registvehicledetail.select5'/></option>
							</select>
							<span class="tit4 txt_right"><fmt:message key='registvehicledetail.modelHeader'/></span>
							<select class="select form vehicleDb" data-lev='4' style="width: 170px;" id="modelHeader">
								<option value="선택"><fmt:message key='registvehicledetail.select6'/></option>
							</select>
						</div>
						<div class="repair_box2_1">
							<span class="tit4 txt_right"><fmt:message key='registvehicledetail.volumn'/></span>
							<select class="select form vehicleDb" data-lev='5' style="width: 170px;" id="volume">
								<option value="선택"><fmt:message key='registvehicledetail.select7'/></option>
							</select>
							<span class="tit4 txt_right"><fmt:message key='registvehicledetail.fuelType'/></span>
							<select class="select form vehicleDb" data-lev='5' style="width: 170px;" id="fuelType">							
								<option value="선택"><fmt:message key='registvehicledetail.select8'/></option>
							</select>
						</div>
						<div class="repair_box2_1">
							<span class="tit4 txt_right"><fmt:message key='registvehicledetail.transmission'/></span>
							<select class="select form vehicleDb" data-lev='5' style="width: 170px;" id="transmission">
								<option value="선택"><fmt:message key='registvehicledetail.select9'/></option>
							</select>
							<input type="hidden" id="convCode" name="convCode" value="${reqVo.convCode}"/>
							<span class="tit4 txt_right"><fmt:message key='registvehicledetail.color'/></span>
							<input type="text" style="width: 158px;" name="color" value="${reqVo.color}"/>
						</div>
						<div class="repair_box2_1">
							<span class="tit4 txt_right"><fmt:message key='registvehicledetail.vehicleBizType'/></span>
							<div class="btn_group2">
								<c:set var="vehicleBizTypeOn1" value="on"/>
								<c:set var="vehicleBizTypeOn2" value=""/>
								<c:set var="vehicleBizTypeOn3" value=""/>
								<c:set var="vehicleBizTypeOn4" value=""/>
								<c:set var="vehicleBizType" value="${reqVo.vehicleBizType}"/>
								<c:if test="${not empty reqVo.vehicleBizType}">
									<c:if test="${reqVo.vehicleBizType eq '2'}" >
										<c:set var="vehicleBizTypeOn1" value=""/>
										<c:set var="vehicleBizTypeOn2" value="on"/>
									</c:if>
									<c:if test="${reqVo.vehicleBizType eq '3'}" >
										<c:set var="vehicleBizTypeOn1" value=""/>
										<c:set var="vehicleBizTypeOn3" value="on"/>
									</c:if>
									<c:if test="${reqVo.vehicleBizType eq '4'}" >
										<c:set var="vehicleBizTypeOn1" value=""/>
										<c:set var="vehicleBizTypeOn4" value="on"/>
									</c:if>
								</c:if>
																
								<a href="#none" class="vehicleBizType ${vehicleBizTypeOn1}" data-val='1'><fmt:message key='registvehicledetail.vehicleBizType1'/></a><a href="#none" class='vehicleBizType ${vehicleBizTypeOn2}' data-val='2'><fmt:message key='registvehicledetail.vehicleBizType2'/></a><a href="#none" class='vehicleBizType ${vehicleBizTypeOn3}' data-val='3'><fmt:message key='registvehicledetail.vehicleBizType3'/></a><a href="#none" class='vehicleBizType ${vehicleBizTypeOn4}' data-val='4'><fmt:message key='registvehicledetail.vehicleBizType4'/></a>
								<input type="hidden" id="vehicleBizType" name="vehicleBizType" value="${reqVo.vehicleBizType}"/>
							</div>
							<span class="tit4 txt_right"><fmt:message key='registvehicledetail.descript'/></span>
							<input type="text" style="width: 158px;" name='descript' value="${reqVo.descript}" />
						</div>
						<div class="repair_box2_1">
							<span class="tit4 txt_right"><fmt:message key='registvehicledetail.distance'/></span>
							<div class="input_box km">
							
								<input type="text" placeholder="입력" style="width:130px;" class="numberOnly" name='totDistance' value="<fmt:formatNumber value="${reqVo.totDistance/1000  }" pattern="#" />" id="totDistance"/>
								<span class="km">km</span>
							</div>
							<span class="tit4 txt_right"><fmt:message key='registvehicledetail.deviceSound'/></span>
							<div class="btn_group2">
								<a href="#none" class="deviceSound <c:if test="${reqVo.deviceSound eq '1' }">on</c:if>" data-val='1'>ON</a><a href="#none" class='deviceSound <c:if test="${reqVo.deviceSound eq '0' or empty reqVo.deviceSound}">on</c:if>' data-val='0'>OFF</a>
								<input type="hidden" id="deviceSound" name="deviceSound" value="${reqVo.deviceSound}"/>
							</div>
						</div>
						<br/><br/>
					</div>
				</div>
	
				<div class="repair_box1 repair_box1_1">
					<div class="tit"><fmt:message key='registvehicledetail.vehicleEtcInfo'/></div>
					<div class="repair_box2">
						<div class="repair_box2_1">
							<span class="tit4 txt_right"><fmt:message key='registvehicledetail.vehicleRegDate'/></span>
							<input type="text" style="width:130px;" id="start_date" class="date start_date datePicker" name="vehicleRegDate" value="${reqVo.vehicleRegDate }"/>
							<span class="tit4 txt_right"><fmt:message key='registvehicledetail.bodyNum'/></span>
							<input type="text" class="w159" name="vehicleBodyNum" value="${reqVo.vehicleBodyNum }" />
						</div>
						<div class="repair_box2_1">
							<span class="tit4 txt_right"><fmt:message key='registvehicledetail.vehicleMaintenanceEndDate'/></span>
							<input type="text" id="end_date" class="date end_date datePicker" style="width:130px;" name="vehicleMaintenanceEndDate" value="${reqVo.vehicleMaintenanceEndDate }" />
						</div>
	
						<div class="repair_box2_1 borB1">
							<span class="tit4 txt_right"></span>
							<span class="cal_tip"><fmt:message key='registvehicledetail.txt4'/></span>
							<br/>
							<br/>
						</div>
					</div>
				</div>
	
				<div class="repair_box1 repair_box1_1">
					<div class="tit"><fmt:message key='registvehicledetail.fixedYN'/></div>
					<div class="repair_box2">
						<div class="repair_box2_1">
							<c:if test="${empty reqVo.fixedUser}" >
								<c:set var="styleHidden" value="style=''" />
								<c:set var="styleShow" value="style='display:none'" />
								<c:set var="checked" value="" />
							</c:if>
							<c:if test="${not empty reqVo.fixedUser}" >
								<c:set var="styleHidden" value="style='display:none;'" />
								<c:set var="styleShow" value="style=''" />
								<c:set var="checked" value="checked='checked'" />
							</c:if>
							<div>
								<span class="tit4 txt_right"><fmt:message key='registvehicledetail.fixedSetting'/></span>
								<strong class="tit_text">
									<input type="checkbox" ${checked} id="fixedUserChk">
									<b><fmt:message key='registvehicledetail.txt5'/><br><fmt:message key='registvehicledetail.txt5_1'/></b>
								</strong>
							</div>
							<div class="mab11 fixedUserBox" ${styleShow }>
								<span class="tit4 txt_right"><fmt:message key='registvehicledetail.user'/></span>
								<div class="search_box3" ${styleHidden } id="fixedUserSearchbox">
									<input type="text" placeholder="<fmt:message key='registvehicledetail.input'/>">
									<a href="#none" class="btn_pop7"><img src="${pathCommon}/images/btn_search3.jpg" alt=""></a>
								</div>
								<span class='user_txt1' ${styleShow } id="fixedUserTxt">${reqVo.fixedAccountNm}/${reqVo.fixedUserNm}</span>
								<input type="hidden" name="fixedUser" id='fixedUser' value="${reqVo.fixedUser}" />
							</div>
							<div class='fixedUserBox' ${styleShow }>
								<span class="tit4 txt_right"><fmt:message key='registvehicledetail.fixedContents'/></span>
								<input type="text" placeholder="<fmt:message key='registvehicledetail.input2'/>" class="w414" name="fixedContents" id='fixedContents' value='${reqVo.fixedContents}'>
							</div>
						</div>
					</div>
				</div>
				<div class="btn_area2"><a href="javascript:window.history.back();" class="btn_cancle" ><fmt:message key='registvehicledetail.cancle'/></a><a href="#none" id="btn_next"><fmt:message key='registvehicledetail.regComplete'/></a></div>
			</div>
			
			
			
		</div>
	</form>
</div>
<!-- <div class="black"></div>
<div class="pop7"> -->

<div id="dialog" title="<fmt:message key='registvehicledetail.title'/>" class="myPopup" style="display: none">
	<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt=""></a>		
	<p class="pop_txt">
		<fmt:message key='registvehicledetail.txt6'/>
	</p>
	<div class="p_relative">
		<form id='popupSearchForm' onSubmit="return false;">
			<div class="tab_select">
				<select class="select form" name="searchOrder" style="height:22px;">
					<option value="regDate"><fmt:message key='registvehicledetail.searchOrder'/></option>
				</select>
			</div>
			<strong class="search_area">
				<input type="text" placeholder='<fmt:message key='registvehicledetail.input3'/>' id="input_searchUserPop" name="searchUserNm"/><a href="#none" id="btn_searchUserPop"><img src="${pathCommon}/images/btn_search2.jpg" alt=""></a>
			</strong>
		</form>
	</div>
	<table class="table1 table1_1">
		<colgroup>
			<col width="60">
			<col width="170">
			<col width="150">
			<col width="140">
			<col width="150">
		</colgroup>
		<thead>
			<tr>
				<th ><fmt:message key='registvehicledetail.select10'/></th>
				<th ><fmt:message key='registvehicledetail.userInfo'/></th>
				<th ><fmt:message key='registvehicledetail.corpAndRank'/></th>
				<th ><fmt:message key='registvehicledetail.telephone'/></th>
				<th ><fmt:message key='registvehicledetail.descript1'/></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
</c:if>
<c:if test="${VDAS.rule ne '0' and VDAS.rule ne '1' }">
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("M0001");
});
</script>
<div class="right_layout">
	<img src="${pathCommon}/images/${_LANG}/authError.png" alt="" style="width:100%">
</div>
</c:if>