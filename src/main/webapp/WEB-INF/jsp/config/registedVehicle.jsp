<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 

<fmt:setLocale value="${_LANG}" scope="request" />
<fmt:setBundle basename="properties.messages.rest.message" />

<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<c:if test="${VDAS.rule eq '0' or VDAS.rule eq '1' }">
<script type="text/javascript">
var lazy;
$(document).ready(function(){
	//left menu on
	initMenuSel("M0001");
	
	$(".tabItem").on("click",function(){
		$(".tabItem").removeClass("on");
		$(this).addClass("on");
		$.lazyLoader.search(lazy);
	});
	
	$(".btn_search").on("click",function(){
		$.lazyLoader.search(lazy);
	});
	
	$("#searchText").on("keyup",function(e){
		if(e.keyCode == 13) $(".btn_search").trigger("click");
	});
	
	$("#searchOrder,#searchDevice,#searchUsed").on("change",function(){
		$.lazyLoader.search(lazy);
	});
	
	$("#contentsTable").on("click",".btn_change",function(){
		var vehicleKey = $(this).data("key")?$(this).data("key"):"";
		$VDAS.instance_post("/config/registVehicleDetail.do",{vehicleKey:vehicleKey,updateLev:1});
	});
	
	$("#contentsTable").on("click",".btn_device",function(){
		var vehicleKey = $(this).data("key")?$(this).data("key"):"";
		$VDAS.instance_post("/config/registVehicleDetail.do",{vehicleKey:vehicleKey,updateLev:2});
	});
	
	$("body").on("click",".a_detail",function(){
		var vehicleKey = $(this).data("key")?$(this).data("key"):"";
		var updateLev = $(this).data("key")?"0":"";
		$VDAS.instance_post("/config/registVehicleDetail.do",{vehicleKey:vehicleKey,updateLev:updateLev});
	});
	
	$("#btn_stop").on("click",function(){
		var b = true;
		var selObj = $(".selObj:checked");

		if(b){
			if(selObj.length == 0){
				alert("<fmt:message key="registedvehicle.txt"/>");
				return;
			}
			
			var strMsg = "<fmt:message key="registedvehicle.txt1"/>";
			if(selObj.length == 1){ 
				strMsg = selObj.data("modelmaster")+"/"+selObj.data("platenum")
			}
			
			$( "#dialog #dialog_vehicle" ).text(strMsg);
			
			$( "#dialog" ).dialog({
		        width:800,
		        dialogClass:'alert',
		        draggable: false,
		        modal:true,
		        height:"auto",
		        resizable:false,
		        closeOnEscape:true,
		        buttons:{
		            '확인':function(){
		            	var strKey = "";
		            	$(".selObj:checked").each(function(){
		            		strKey += strKey==""?$(this).data("vehiclekey"):","+$(this).data("vehiclekey");
		            	});
		            	
		            	$VDAS.http_post("/vehicle/inactiveVehicle.do",{vehicleKey:strKey},{
		            		success : function(r){
		            			alert("<fmt:message key="registedvehicle.txt2"/>");
		            			$.lazyLoader.search(lazy);
		            		}
		            	});
		            	
		                $(this).dialog("close");
		            }
		        }
		    });
		}
	});
	
	$("#btn_del").on("click",function(){
		var b = true;
		var selObj = $(".selObj:checked");

		if(b){
			if(selObj.length == 0){
				alert("<fmt:message key="registedvehicle.txt3"/>");
				return;
			}
			
			var strMsg = "<fmt:message key="registedvehicle.txt4"/>";
			if(selObj.length == 1){ 
				strMsg = selObj.data("modelmaster")+"/"+selObj.data("platenum")
			}
			
			$( "#dialog #dialog_vehicle" ).text(strMsg);
			
			
			$( "#dialog2" ).dialog({
		        width:800,
		        dialogClass:'alert',
		        draggable: false,
		        modal:true,
		        height:"auto",
		        resizable:false,
		        closeOnEscape:true,
		        buttons:{
		            '확인':function(){
		            	var strKey = "";
		            	$(".selObj:checked").each(function(){
		            		strKey += strKey==""?$(this).data("vehiclekey"):","+$(this).data("vehiclekey");
		            	});
		            	
		            	$VDAS.http_post("/vehicle/deleteVehicle.do",{vehicleKey:strKey},{
		            		success : function(r){
		            			alert("<fmt:message key="registedvehicle.txt5"/>");
		            			$.lazyLoader.search(lazy);
		            		}
		            	});
		            	
		                $(this).dialog("close");
		            }
		        }
		    });
		}
	});
	
	
    $('.pop_close').click(function(){
        $(this).closest("div.myPopup").dialog("close");
    });
    
	if(!lazy)
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
				searchType : $("#searchType")
				,searchText : $("#searchText")
				,searchOrder : $("#searchOrder")
				,searchDevice : $("#searchDevice")
				,searchUsed : $("#searchUsed")
			},setSearchFunc : function(){
				var tab = $(".tabItem.on").data("tab");
				$("#searchFrm [name=searchState]").val(tab);			
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/com/getVehicleList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.rtvList;
				var strHtml = "";
				for(var i = 0 ; i < data.length ; i++){
					
					var vo = data[i];
					strHtml += "<tr>";
					
					var used = "";
					if(vo.deviceId.length>0)
						used = '1';
					else
						used = '0';
					
					strHtml += '<td><input type="checkbox" class="selObj" data-vehiclekey="'+vo.vehicleKey
					+'" data-modelmaster="'+vo.modelMaster
					+'" data-platenum="'+vo.plateNum
					+'" data-used="'+used
					+'"/></td>';
					strHtml += '<th>';
					strHtml += '<strong class="car_img"><img src="${path}/common/images/vehicle/'+vo.imgS+'.png" alt="" /></strong>';
					strHtml += '<a href="#none">';
					strHtml += '<span>'+vo.modelMaster+'</span>';
					strHtml += vo.plateNum;
					strHtml += '</a>';
					strHtml += '</th>';
					strHtml += '<td><div class="start_end txt_left">';
					strHtml += '<p>'+vo.year+'</p>';
					strHtml += '<p>'+number_format((vo.totDistance/1000).toFixed(1))+'</p>';
					strHtml += '</div></td>';
					strHtml += '<td><div class="start_end txt_left">';
					strHtml += '<p>'+vo.deviceSeries+'</p>';
					strHtml += '<p>'+vo.deviceSn+'</p>';
					strHtml += '</div></td>';
					if(vo.deviceId.length>0)
						strHtml += '<td><fmt:message key='registedvehicle.use'/></td>';
					else
						strHtml += '<td><fmt:message key='registedvehicle.notuse'/></td>';
						
					strHtml += '<td><a href="#none" class="a_detail" data-key='+vo.vehicleKey+'><img src="${path}/common/images/btn4.jpg" alt=""></a></td>';
					if(vo.deviceId.length>0)
						strHtml += '<td><a href="#none" class="btn_change" data-key='+vo.vehicleKey+'><img src="${path}/common/images/btn3.jpg" alt=""></a></td>';
					else
						strHtml += '<td><a href="#none" class="btn_device" data-key='+vo.vehicleKey+'><img src="${path}/common/images/btn3.jpg" alt=""></a></td>';
					
					strHtml += '</tr>';	
				}
				
				return strHtml;
			}
		});
	
	$("#allChker").on("click",function(){
		if($(this).is(":checked"))
			$(".selObj","#contentsTable tbody").prop("checked",true);
		else $(".selObj","#contentsTable tbody").prop("checked",false);
	});
	
	$VDAS.http_post("/vehicle/getVehicleCnt.do",{},{
		success : function(r){
			var data = r.result;
			
			$("#titleCnt1").text(data.total+"<fmt:message key="registedvehicle.cnt1"/>");
			$("#titleCnt2").text(data.used+"<fmt:message key="registedvehicle.cnt1_1"/>");
			$("#titleCnt3").text(data.stop+"<fmt:message key="registedvehicle.cnt1_2"/>");
		}
	});
	 
	$VDAS.ajaxCreateSelect ("/com/getDeviceSeries.do"
			,{}
			,$("#searchDevice")
			,"단말기타입"
			,"S"
			,"${reqVo.deviceSeries}"
			,"${_LANG}"
			,null
			,null);
});







</script>
<form id="searchFrm" style="display:none">
	<input type="hidden" name="searchType" value="" />
	<input type="hidden" name="searchText" value="" />
	<input type="hidden" name="searchOrder" value="" />
	<input type="hidden" name="searchDevice" value="" />
	<input type="hidden" name="searchUsed" value="" />
	<input type="hidden" name="searchState" value="" />
</form>
<div class="right_layout">
	<h2 class="tit_sub">
		<fmt:message key='registedvehicle.txt6'/>
		<p><fmt:message key='registedvehicle.txt6_1'/></p>
	</h2>
	<div class="sub_top1">
		<span><fmt:message key='registedvehicle.regVehicleCnt'/><b id="titleCnt1"></b></span>
		<span><fmt:message key='registedvehicle.use1'/> <b id="titleCnt2"></b></span>
		<span><fmt:message key='registedvehicle.notuse1'/> <b id="titleCnt3"></b></span>
		<strong id="nowDate"></strong>
	</div>
	<h3 class="h3tit"><fmt:message key='registedvehicle.searchVehicle'/></h3>
	<div class="box1">
		<fmt:message key='registedvehicle.txt7'/>
		<a href="#none" class="a_detail" ><fmt:message key='registedvehicle.txt7_1'/></a>
	</div>
	<h3 class="h3tit"><fmt:message key='registedvehicle.searchRegVehicle'/></h3>
	<div class="search_box1">
		<form id="frmSearch" method="post" onSubmit="return false;">
			<div class="search_box1_1">
				<div class="select_type1">
					<div class="select_type1_1">
						<select id="searchType" class="sel">
							<option value=""><fmt:message key='registedvehicle.all'/></option>
							<option value="plateNum"><fmt:message key='registedvehicle.plateNum'/></option>
							<option value="modelMaster"><fmt:message key='registedvehicle.modelMaster'/></option>
							<option value="deviceSn"><fmt:message key='registedvehicle.deviceSn'/></option>
						</select>
					</div>
				</div><input type="text" id="searchText" class="search_input" />
			</div><a href="#none" class="btn_search"><fmt:message key='registedvehicle.search'/></a>
		</form>
	</div>
	<div class="tab_layout">
		<div class="pageTab">
			<ul class="tab_nav">
				<li class="tabItem on" data-tab=''><a href="#none" ><fmt:message key='registedvehicle.allVehicle'/></a></li>
				<li class='tabItem' data-tab='fixed0'><a href="#none" ><fmt:message key='registedvehicle.notFixedVehicle'/></a></li>
				<li class='tabItem' data-tab='fixed1'><a href="#none"><fmt:message key='registedvehicle.fixedVehicle'/></a></li>
			</ul>
		</div>
		<div style="margin-bottom: 10px;">
			<select id="searchOrder" class="select form" style="width:105px;">
				<option value="driving"><fmt:message key='registedvehicle.recentDriving'/></option>
				<option value="regDate"><fmt:message key='registedvehicle.recentReg'/></option>
			</select>
			<div class="select_type2_1">
				<a href="#none" id="btn_stop"><img src="${path}/common/images/${_LANG}/btn_stop.jpg" alt=""></a>
				<a href="#none" id="btn_del"><img src="${path}/common/images/${_LANG}/btn_delete.jpg" alt=""></a>
			</div>
		</div>
		<table class="table1" id="contentsTable">
			<colgroup>
				<col style="width:70px;" />
				<col />
				<col style="width:90px;" />
				<col />
				<col style="width:100px;"/>
				<col style="width:60px;" />
				<col style="width:90px;" />
			</colgroup>
			<thead>				
				<tr>
					<th><input type="checkbox" id="allChker"></th>
					<th><fmt:message key='registedvehicle.vehicleInfo'/></th>
					<th><fmt:message key='registedvehicle.yearAndDistance'/></th>
					<th>
						<select class="th_select" id="searchDevice"></select>
					</th>
					<th>
						<select class="th_select" id="searchUsed">
							<option value=""><fmt:message key='registedvehicle.useType'/></option>
							<option value="1"><fmt:message key='registedvehicle.use2'/></option>
							<option value="0"><fmt:message key='registedvehicle.notuse2'/></option>
						</select>
					</th>
					<th><fmt:message key='registedvehicle.modify'/></th>
					<th><fmt:message key='registedvehicle.vehicleChange'/></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		<div class="txt1">
			<h1><fmt:message key='registedvehicle.txt8'/></h1>
			<p><fmt:message key='registedvehicle.txt8_1'/>  </p>
		</div>
	</div>
</div>
<div id="dialog" title="차량 사용 정지" class="myPopup" style="display: none">
	<div class="pop_close"></div>
	<p class="pop_txt">
		<em id="dialog_vehicle"></em> <fmt:message key='registedvehicle.txt9'/>
	</p>
	<div class="pop_notice">
		<fmt:message key='registedvehicle.txt9_1'/>
	</div>
</div>

<div id="dialog2" title="차량 삭제 " class="myPopup" style="display: none">
	<div class="pop_close"></div>
	<p class="pop_txt">
		<em id="dialog_vehicle"></em> <fmt:message key='registedvehicle.txt10'/>
	</p>
	<div class="pop_notice">
		<fmt:message key='registedvehicle.txt10_1'/>
	</div>
</div>
</c:if>
<c:if test="${VDAS.rule ne '0' and VDAS.rule ne '1' }">
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("M0001");
});
</script>
<div class="right_layout">
	<img src="${pathCommon}/images/${_LANG}/authError.png" alt="" style="width:100%">
</div>
</c:if>