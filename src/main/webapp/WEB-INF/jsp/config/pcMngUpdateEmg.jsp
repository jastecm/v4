<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>  
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	
	var firmVer = "";
	
	$("body").on("keyup","#searchDeviceSn",function(e){
		if(e.keyCode == '13'){
			var sn = $("#searchDeviceSn").val();
			if(sn.length != 4) {
				alert("시리얼번호 뒤 4자리를 입력해주세요.");
				return;
			}
			
			$(".contentsDiv").html('');
			$(".btn_down").hide();
			$(".btn_none").hide();
			
			
			$VDAS.http_post("/com/getVehicleList.do",{searchDeviceSn:"A000"+sn},{
				success:function(r){
					var data = r.rtvList;				
					var strHtml = '';
					
					if(data.length == 0){
						strHtml += '<br/>';
						strHtml += '<br/>';
						strHtml += '결과없음';
					}
					
					for(var i = 0 ; i < data.length ; i++){
						var vo = data[i];
						strHtml += '<br/>';
						strHtml += '<br/>';
						strHtml += 'Gper번호 : <span id="plateNum" style="padding-right:15px;">'+vo.plateNum+'</span>   ';
						strHtml += '시리얼번호 : <span id="deviceSn"style="padding-right:15px;">'+vo.deviceSn+'</span>   ';
						
						var dbNm = vo.dbNm;
						var firmNm = vo.firmNm;
						/*
						$VDAS.http_post("/vehicle/getVerInfoVehicle.do",{vehicleKey:vo.vehicleKey},{
							sync : true
							,success : function(r){
								var d = r.result;
								dbNm = d.vehicleVer;
								firmNm = d.firmware;
							}
						});
						*/
						firmVer = firmNm;
						
						strHtml += 'DB버전 : <span id="dbNm" style="padding-right:15px;">'+dbNm+'</span>   ';
						strHtml += '펌웨어버전 : <span id="firmNm" style="padding-right:15px;">'+firmNm+'</span>   ';
						
						var cKey = vo.connectionKey;
						var convCode = vo.convCode;
						$(".btn_down").data("key",cKey);
						$(".btn_down").data("convcode",convCode);
					}
					
					$(".contentsDiv").append(strHtml);
					
					if(data.length != 0){
						//if(firmVer=="von-S31-v${ver.s31}") $(".btn_none").show();
						//else $(".btn_down").show();
						$(".btn_down").show();	
					}
					
					
					
				}
			});
		}
	});
	
	$("body").on("keyup","#searchText",function(e){
		if(e.keyCode == '13'){
			var sn = $("#searchText").val();
			if(sn.length == 0) {
				alert("값을 입력해주세요.");
				return;
			}
			if(sn.length != 5) {
				alert("Gper번호는 5자리 입니다.");
				return;
			}
			
			
			$(".contentsDiv").html('');
			$(".btn_down").hide();
			$(".btn_none").hide();
			
			$VDAS.http_post("/com/getVehicleList.do",{searchType:'plateNum',searchText:sn},{
				success:function(r){
					var data = r.rtvList;				
					var strHtml = '';
					
					if(data.length == 0){
						strHtml += '<br/>';
						strHtml += '<br/>';
						strHtml += '결과없음';
					}
					
					for(var i = 0 ; i < data.length ; i++){
						var vo = data[i];
						strHtml += '<br/>';
						strHtml += '<br/>';
						strHtml += 'Gper번호 : <span id="plateNum" style="padding-right:15px;">'+vo.plateNum+'</span>   ';
						strHtml += '시리얼번호 : <span id="deviceSn"style="padding-right:15px;">'+vo.deviceSn+'</span>   ';
						
						var dbNm = vo.dbNm;
						var firmNm = vo.firmNm;
						/*
						$VDAS.http_post("/vehicle/getVerInfoVehicle.do",{vehicleKey:vo.vehicleKey},{
							sync : true
							,success : function(r){
								var d = r.result;
								dbNm = d.vehicleVer;
								firmNm = d.firmware;
							}
						});
						*/
						firmVer = firmNm;
						
						strHtml += 'DB버전 : <span id="dbNm" style="padding-right:15px;">'+dbNm+'</span>   ';
						strHtml += '펌웨어버전 : <span id="firmNm" style="padding-right:15px;">'+firmNm+'</span>   ';
						
						var cKey = vo.connectionKey;
						var convCode = vo.convCode;
						$(".btn_down").data("key",cKey);
						$(".btn_down").data("convcode",convCode);
					}
					
					$(".contentsDiv").append(strHtml);
					
					if(data.length != 0){
						//if(firmVer=="von-S31-v${ver.s31}") $(".btn_none").show();
						//else $(".btn_down").show();
						$(".btn_down").show();	
					}
					
				}
			});
		}
	});
	
	$("body").on("click",".btn_down",function(){
		var $_this = $(this);
		var totDistance = 0;
		var deviceSound = 0;
		
		var connectionKey = $(this).data("key");
		var convCode = $(this).data("convcode");
		//var key = "1b07959d-9d08-11e7-b5ba-0002b3da4116";
		var key = "6d6ea5a5-8867-11e7-b9e0-0002b3da4116";		
		$_this.attr("href","pcmgrProto://updateVehicle/?connectionKey="+connectionKey+"&convCode="+convCode+"&totDistance="+totDistance+"&deviceSound="+deviceSound+"&key="+key);
		
	});
	
	if(is_32bits_architecture())
		$(".pcMngDown").attr("href","https://viewcar.co.kr/pcmgrvon41installer32.exe");
	else
		$(".pcMngDown").attr("href","https://viewcar.co.kr/pcmgrvon41installer32.exe");
	
	if(is_32bits_architecture())
		$(".pcDriverDown").attr("href","https://viewcar.co.kr/JASTECVirtualCOMPort_Driver.zip");
	else
		$(".pcDriverDown").attr("href","https://viewcar.co.kr/JASTECVirtualCOMPort_Driver.zip");
	
});
</script>
<div class="top_wrap" style="padding-top: 20px;
    width: 806px;
    margin: 0 auto;
    position: relative;
    padding-left: 194px;
    padding-bottom: 100px;">
	<form id="addVehicleFrm" method="post">
		<h2 class="tit_sub">
			von-S31 단말기 업데이트
			<p>현재 페이지에서 차량 DB,펌웨어를 최신 버전으로 업데이트 가능합니다.</p>
		</h2>
		<h3 class="h3tit" style="margin: 20px 0 10px 0;color: #e81626">공지사항</h3>
		<div class="box3 pcimg_box" style="margin-bottom:0px;border-color: #e81626;background-color: #fff9f9">
			<span style="width: 100%;text-align: center;padding-top:20px;font-size:12px;font-weight: 600;color: #424242">최신 버전을 안내해 드립니다.</span>
			</br>
			<span style="width: 100%;text-align: center;padding-top:20px;font-size:12px;font-weight: 600;color: #424242">2017년 모닝 차량의 경우, DB버전 '모닝',펌웨어버전 'von-S31-v1.11SC'가 맞는지 확인해주세요.</span>
			</br>
			</br>
			<span style="width: 100%;text-align: center;padding-top:20px;font-size:12px;font-weight: 600;color: #e81626">2017 모닝 외 모든 차량</span>
			</br>
			<span style="width: 100%;text-align: center;padding-top:20px;font-size:12px;font-weight: 400;color: #424242">최신 DB버전 : 0013</span>
			</br>
			<span style="width: 100%;text-align: center;padding-top:20px;font-size:12px;font-weight: 400;color: #424242">최신 펌웨어버전 : von-S31-v1.11SC</span>
			</br>
			</br>
			<span style="width: 100%;text-align: center;padding-top:20px;font-size:12px;font-weight: 600;color: #e81626">2017년 모닝</span>
			</br>
			<span style="width: 100%;text-align: center;padding-top:20px;font-size:12px;font-weight: 400;color: #424242">최신 DB버전 : 모닝</span>
			</br>
			<span style="width: 100%;text-align: center;padding-top:20px;font-size:12px;font-weight: 400;color: #424242">최신 펌웨어버전 : von-S31-v1.11SC</span>
		</div>
		
		<h3 class="h3tit" style="margin: 20px 0 10px 0;">업데이트 방법</h3>
		<div class="box3 pcimg_box" style="margin-bottom:0px;">
			<span style="width: 100%;text-align: center;padding-top:20px;font-size:12px;font-weight: 600;color: #424242">① '뷰카 PC매니저' 프로그램을 다운 받아주세요.</span>
			<div style="width: 100%;text-align: center;padding-top:0px;font-size:12px;font-weight: 600;color: #828282;text-align:left">windows7 64bit를 사용하시는 고객님은 <a class="pcDriverDown" href="#none" style="text-decoration: underline;color:#2a20eb">‘윈도우7 USB드라이버’</a>를 다운받아
			</br>내컴퓨터->설정->장치관리자->PORT에서 자스텍 USB드라이버를 업데이트해주세요.
			</br>※ 윈도우(windows7 이상) 지원 / 사용 가능 브라우저 : Chrome, Microsoft Edge</div>
			</br><span style="width: 100%;text-align: center;padding-top:20px;font-size:12px;font-weight: 600;color: #424242">② 업데이트할 단말기를 USB로 연결시켜주세요.</span>
			</br><span style="width: 100%;text-align: center;padding-top:20px;font-size:12px;font-weight: 600;color: #424242">③ A000을 제외한 일련번호 끝 4자리를 입력해주세요.</span>
			<span><a class="pcMngDown right" href="#none" style="top:30%;width:153px;">PC매니저 다운로드</a>
			<a class="pcDriverDown right" href="#none" style="top:60%;width:153px;">USB드라이버 다운로드</a>
			</span>
		</div>
		<div class="box3 pcimg_box" style="padding: 15px 16px 15px 20px;margin-bottom:10px;">
			</br><span style="width: 100%;text-align: center;padding-top:20px;font-size:12px;font-weight: 400;color: #828282">사용하시는 컴퓨터에 보안프로그램이 설치되어 있는 경우, 뷰카 PC매니저가 작동하지 않을 수 있습니다.</span>
			</br><span style="width: 100%;text-align: center;padding-top:20px;font-size:12px;font-weight: 400;color: #828282">뷰카 PC매니저가 동작하지 않을 경우, 보안 프로그램을 정지 후 사용하시거나 보안 프로그램이 설치되지 않은 PC에서 사용해주세요.</span>
			</br><span style="width: 100%;text-align: center;padding-top:20px;font-size:12px;font-weight: 400;color: #828282">이용에 불편을 드려 죄송합니다.</span>
		</div>
		<div class="repair_layout2" style="border:1px solid #c0c0c0;">
			<div style="width: 100%;text-align: center;padding-top:20px;">
				<img src = "${pathCommon}/images/${_LANG}/pcmng.png" alt="" />
			</div>					
			<div style="width: 100%;text-align: center;padding-top:20px;">
				<div style="width: 100%;text-align: center;padding-top:0px;font-size:12px;font-weight: 600;color: #424242">
					등록할 단말기를 USB 연결한 후
					</br>
					</br>
					제품 박스 및 단말기에 기재된 <span style="color:red;">일련번호 또는 Gper번호</span>를 입력 후 <span style="color:red;">키보드’Enter’</span>를 눌러주세요.
				</div>
			</div>
			<div style="width: 100%;text-align: center;padding-top:50px;">
				<ul>
					<li>						
						<div style="width:100px;height:30px;text-align:right;display: inline-block;padding-top:00px;font-size:12px;font-weight: 600;color: #424242;vertical-align: baseline;">일련번호</div>
						<div style="width:100px;height:30px;text-align:right;display: inline-block;padding-top:00px;font-size:12px;font-weight: 600;color: #424242;vertical-align: baseline;">A000 <input style="width:50px;text-align:left;height:25px;border:1px solid #828282" maxlength="4" id="searchDeviceSn"></div>
						<div style="width:50px;height:0px;text-align:right;display: inline-block;padding-top:00px;font-size:12px;font-weight: 600;color: #424242;vertical-align: baseline;"></div>
					</li>
					<li>
						<div style="width:100px;height:30px;text-align:right;display: inline-block;padding-top:00px;font-size:12px;font-weight: 600;color: #424242;vertical-align: baseline;">Gper번호</div>
						<div style="width:100px;height:30px;text-align:right;display: inline-block;padding-top:00px;font-size:12px;font-weight: 600;color: #424242;vertical-align: baseline;"><input style="width:100px;text-align:left;height:25px;border:1px solid #828282" id="searchText"></div>
						<div style="width:50px;height:0px;text-align:right;display: inline-block;padding-top:00px;font-size:12px;font-weight: 600;color: #424242;vertical-align: baseline;"></div>
					</li>
				</ul>
			</div>
			<div class="contentsDiv" style="text-align:center">
			
			</div>							
			<div class="btn_area5" style="margin-top:0px;margin-bottom:30px;">
				<a href="#none" style="display:none;background-color: #ed1c24" class="btn_down">단말기 업데이트 실행</a>
				<a href="#none" style="display:none;background-color: #828282;color: #adadad;" class="btn_none">현재 최신 버전입니다</a>
			</div>
			<!-- <div class="btn_area2"><a href="javascript:window.history.back();" class="btn_cancle">이전 단계</a><a href="#none" id="btn_complate" style="display: none">등록완료</a></div> -->
		</div>
	</form>
</div>




