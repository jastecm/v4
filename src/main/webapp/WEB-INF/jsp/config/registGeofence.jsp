<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<c:if test="${VDAS.rule eq '0' or VDAS.rule eq '1' }">
<script type="text/javascript">
var lazy;
var lazyAddr;
$(document).ready(function(){
	//left menu on
	initMenuSel("M0007");
	
	if("${fenceKey}" == "")
		$("#mapFrame").attr("src",_defaultPath+"/map/mapPoint.do");
	else{
		$("#mapFrame").attr("src",_defaultPath+"/map/mapPoint.do");
		/*
		$VDAS.http_post("/user/getFenceInfo.do",{fenceKey:"${fenceKey}"},{
			success : function(r){
					
			});
		});
		*/
	}
	$("#input_addr").on("keyup",function(e){
		if(e.keyCode == 13) $("#btn_searchAddr").trigger("click");
	});
	
	$("#btn_searchAddr").on("click",function(){
		$("#input_search").val($("#input_addr").val());
		if(lazyAddr)
			$.lazyAddrLoader.search(lazyAddr);
		
		$( "#dialog" ).dialog({
	        width:800,
	        dialogClass:'alert',
	        draggable: false,
	        modal:true,
	        height:"auto",
	        resizable:false,
	        closeOnEscape:true,
	        buttons:{
	            '닫기':function(){
	            	$(this).dialog("close");
	            }
	        }
	    });
		
		if(!lazyAddr)
			lazyAddr = $("#dialog table").lazyAddrLoader({
				searchFrmObj : $("#addrSearchPop")
				,searchFrmVal : {}
				,scrollRow : 5
				,rowHeight : 80
				,limit : 10
				,initSearch : true
				,createDataHtml : function(r){
					var strHtml = "";
					var data = r.juso;
					
					for(var i = 0 ; i < data.length ; i++){
						var vo = data[i];
						
						strHtml += "<tr>";
						strHtml += '<td><div class="start_end txt_left">';
						strHtml += '<p><a class="btnSel" href="#none" data-fulladdr="'+vo.roadAddr+'" data-addr="'+vo.roadAddr+'" data-city="'+vo.siNm+'" data-country="'+vo.sggNm+'" data-road="'+vo.rn+'" data-building="'+vo.buldMnnm+'" data-buildingnm="'+vo.bdNm+'">'+vo.roadAddr+'</a></p>';
						strHtml += '<p><a class="btnSel" href="#none" data-fulladdr="'+vo.roadAddr+'" data-addr="'+vo.roadAddr+'" data-city="'+vo.siNm+'" data-country="'+vo.sggNm+'" data-road="'+vo.rn+'" data-building="'+vo.buldMnnm+'" data-buildingnm="'+vo.bdNm+'">'+vo.jibunAddr+'</a></p>';
						strHtml += '</div></td>';
						strHtml += '</tr>';	
					}
					
					return strHtml;
				}
			});
	});
	
	$("#btn_search").on("click",function(){
		$.lazyAddrLoader.search(lazyAddr);
	}) ;
	
	$(".btn_closeDialog").on("click",function(){
		$("#dialog").dialog("close");
	});
	
	$("#input_search").on("keyup",function(e){
		if(e.keyCode == 13) $("#btn_search").trigger("click");
	});
	
	$("body").on("click",".btnSel",function(e){
		var addr = $(this).data("addr");
		var city = $(this).data("city");
		var country = $(this).data("country");
		var road = $(this).data("road");
		var building = $(this).data("building");
		var buildingnm = $(this).data("buildingnm");
		var fulladdr = $(this).data("fulladdr");
		
		$VDAS.http_post("/com/addrPointSearch.do",{addr:fulladdr},{
			success : function(r){
				var d = r.result;
				var vo = JSON.parse(d);
				var lat = vo.coordinateInfo.coordinate[0].newLat;
				var lon = vo.coordinateInfo.coordinate[0].newLon;
				var mapFrame = $("#mapFrame").get(0).contentWindow;
				mapFrame.move(lon,lat);
				setAddr(fulladdr);
				$("#dialog").dialog("close");
			}
		});
	});
	
	$("#btn_save").on("click",function(){
		$("#alies").val($("#aliesNm").val());
		
		if($("#lat").val().length == 0||$("#lon").val().length == 0||$("#addr").val().length == 0){
			alert("거래처가 정상적으로 선택되지 않았습니다.");
		}else{
			$VDAS.http_post("/config/saveGeofence.do",$("#searchFrm").serialize(),{
				success : function(r){
					alert("저장되었습니다.");
					window.location.href=_defaultPath+"/config/registedGeofence.do";
				},error : function(r){
					if($message[r.responseJSON.result])
						alert($message[r.responseJSON.result]);
					else {
						alert(r.status+"\n"+r.statusText);
					}
				}
			});
			
		}
		
	});
	
});

var setAddr = function(addr){
	$("#addr").val(addr);
	$("#input_addr").val(addr);
}
var setLonLat = function(lon,lat){
	$("#lon").val(lon);
	$("#lat").val(lat);	
}
</script>
<form id="searchFrm" style="display:">
	<input type="hidden" id="addr" name="addr"/>
	<input type="text" id="lat" name="lat"/>
	<input type="text" id="lon" name="lon"/>
	<input type="hidden" id="alies" name="alies"/>
</form>
<div class="right_layout">
	<div class="detail_view">
		<a href="javascript:window.history.back();" class="btn_close"><img src="${pathCommon}/images/close.jpg" alt="" /></a>
		<h2 class="h2tit">
			거래처 등록
		</h2>
		<div class="step_box sub_top1" style="text-align:left;margin-bottom: 0px;">
			<span>등록한 거래처 1km이내 접근 시 , 직원의 운행스케쥴에 기록됩니다.</span>
		</div>
		<div class="repair_layout2">
			<div class="repair_box1">
				<div class="tit">거래처 명</div>				
				<div class="repair_box2">
					<div class="repair_box2_1">
						<input type="text" id="aliesNm" style="width:180px"/>
					</div>
				</div>
			</div>
		</div> 
		<div class="repair_layout2" style="margin-bottom: 20px;">
			<div class="repair_box1">
				<div class="tit">상세주소</div>
				<div class="repair_box2">
					<div class="repair_box2_1">
						<input type="text" id="input_addr" style="width:450px;height: 28px;"/>
						<a href="#none" id="btn_searchAddr" style="display: inline-block;right: 16px;width: 70px;height: 29px;line-height: 28px;text-align: center;background: red;color: #fff;font-weight: 600;font-size: 13px;">상세주소</a>						
					</div>
				</div>
			</div>
		</div> 
		
		<div style="width:100%;height: 500px">
			<iframe id="mapFrame" style="width:100%;height: 100%"></iframe>
		</div>
		<div class="btn_type1" style="padding-top:20px;"><a href="javascript:window.history.back();" class="btn_cancel">취소</a><a href="#none" id="btn_save" class="btn_select">확인</a></div>
	</div>
</div>
<div id="dialog" title="주소검색" class="myPopup" style="display:none">
	<a href="#none" class="btn_close btn_closeDialog"><img src="${pathCommon}/images/close3.jpg" alt=""></a>		
	<div class="p_relative" style="height:30px;">
		<form id='addrSearchPop' onSubmit="return false;">
			<input type="hidden" name="currentPage" value=""/> 
			<input type="hidden" name="countPerPage" value=""/>
			<input type="hidden" name="resultType" value="json"/> 
			<!-- <input type="hidden" name="confmKey" value="TESTJUSOGOKR"/> -->
			<input type="hidden" name="confmKey" value="U01TX0FVVEgyMDE3MDkxOTIyNTQ0NDEwNzM2NTA="/>
			<strong class="search_area" style="width:320px;">
				<input type="text" placeholder="직접검색" id="input_search" name="keyword" style="width:284px;"/><a href="#none" id="btn_search"><img src="${pathCommon}/images/btn_search2.jpg" alt=""></a>				
			</strong>
		</form> 
	</div>
	<table class="table1 table1_1">
		<colgroup>
			<col width="*">
		</colgroup>
		<thead>
			<tr>
				<th>주소</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>

</c:if>
<c:if test="${VDAS.rule ne '0' and VDAS.rule ne '1' }">
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("M0007");
});
</script>
<div class="right_layout">
	<img src="${pathCommon}/images/${_LANG}/authError.png" alt="" style="width:100%">
</div>
</c:if>