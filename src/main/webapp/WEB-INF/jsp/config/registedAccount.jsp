<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<c:if test="${VDAS.rule eq '0' or VDAS.rule eq '1' }">
<style>.svg-clipped{-webkit-clip-path:url(#svgPath);clip-path:url(#svgPath);}</style>
<svg height="0" width="0">
	<defs>
		<clipPath id="svgPath" clipPathUnits="objectBoundingBox">
  			<circle cx=".5" cy=".5" r=".5" />
   		</clipPath>
	</defs>
</svg>
<script type="text/javascript">
var lazy;
$(document).ready(function(){
	
	//left menu on
	initMenuSel("M0002");
	
	$VDAS.ajaxCreateSelect("/com/getGroupCode.do",{},$("#corpGroup"),false,"전체 부서","${rtv.groupKey}","${_LANG}",null,function(){
		$VDAS.ajaxCreateSelect("/com/getGroupUserList.do",{corpGroup:"${rtv.groupKey}"},$("#accountKey"),false,"전체 사용자","${rtv.accountKey}","${_LANG}",null,null);
	});
	
	$("#corpGroup").on("change",function(){
		$VDAS.ajaxCreateSelect("/com/getGroupUserList.do",{corpGroup:$(this).val()},$("#accountKey"),false,"전체 사용자","","${_LANG}",null,null);	
	});
	
	$(".btn_search").on("click",function(){
		alert("todo! - param");
		$.lazyLoader.search(lazy);
	});
	
	$("#searchText").on("keyup",function(e){
		if(e.keyCode == 13) $(".btn_search").trigger("click");
	});
	
	$("#btn_del").on("click",function(){
		
		var chker = $('.selObj:checked');
		var strMsg = "선택된 사용자";
		
		if(chker.length == 0){
			alert("삭제할 사용자를 선택해주세요.");
			return;
		}
		if(chker.length == 1)
			strMsg = chker.data("accountnm")+"/"+chker.data("usernm")
		
			
		$( "#dialog #dialog_vehicle" ).text(strMsg);
		
		$( "#dialog" ).dialog({
	        width:800,
	        dialogClass:'alert',
	        draggable: false,
	        modal:true,
	        height:"auto",
	        resizable:false,
	        closeOnEscape:true,
	        buttons:{
	        	'확인':function(){
	            	var strKey = "";
	            	$(".selObj:checked").each(function(){
	            		strKey += strKey==""?$(this).data("accountkey"):","+$(this).data("accountkey");	            		
	            	});
	            	
	            	$VDAS.http_post("/account/inactiveUser.do",{accountKey:strKey},{
	            		success : function(r){
	            			alert("처리되었습니다.");
	            			$.lazyLoader.search(lazy);
	            		}
	            	});
	            	
	                $(this).dialog("close");
	            }
	        }
	    });
	});
	
	$("#btn_upload").on("click",function(){
		
		$( "#dialog2" ).dialog({
	        width:800,
	        dialogClass:'alert',
	        draggable: false,
	        modal:true,
	        height:"auto",
	        resizable:false,
	        closeOnEscape:true,
	        buttons:{
	        	'확인':function(){
	            	
	        		$("#fileFrm").submit();
	        		
	            }
	        }
	    });
		
		$("#dialog2.ui-dialog-content").css({padding:"0px"});
		
	});
	
	$('#fileFrm').ajaxForm({
		beforeSubmit: function (data,form,option) {
	    	return true;
		}
		,success: function(r){
	    	
			alert("처리되었습니다.");
			$.lazyLoader.search(lazy);
	    	$( "#dialog2" ).dialog("close");
	    	
		},
		error: function(r){
			var obj = JSON.parse(r.responseText).result;
			var index = JSON.parse(r.responseText).index;
			alert($message[obj]+"\n"+(index)+"줄");
		}                               
	});
	
    $('.pop_close').click(function(){
        $(this).closest("div.myPopup").dialog("close");
    });
    
  //move page car detail
	$("body").on("click",".a_detail",function(){
		var accountKey = $(this).data("key")?$(this).data("key"):"";
		$VDAS.instance_post("/config/registAccountDetail.do",{accountKey:accountKey});
	});
  
	if(!lazy)
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {searchType:$("#searchType")
							,searchText:$("#searchText")												
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/com/getUserList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.rtvList;
				var strHtml = "";
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += "<tr>";
					if(vo.rule != "0" && vo.rule != "1"){
						strHtml += '<td><input type="checkbox" name="selObj" class="selObj" data-accountnm="'+vo.accountNm
						+'" data-accountkey="'+vo.accountKey
						+'" data-corpgroupnm="'+vo.corpGroupNm
						+'" data-usernm="'+vo.name
						+'"/></td>';
					} else strHtml += '<td></td>';					
					strHtml += '<th>';
					strHtml += '<strong class="car_img">';
					if(vo.imgId&&vo.imgId.length > 0)
						strHtml += '<img class="svg-clipped" style="width:49px;height:49px;" src="${defaultPath}/com/getUserImg.do?uuid='+vo.imgId+'" alt="" />';
					else strHtml += '<img class="svg-clipped" src="${pathCommon}/images/user_img7.jpg" alt="" style="width:49px;height:49px;"/>';					
					strHtml += '</strong>';
					strHtml += '<a href="#none">';
					strHtml += '<span>'+vo.accountNm+'</span>';
					strHtml += vo.name;
					strHtml += '</a>';
					strHtml += '</th>';
					strHtml += '<td>';
					var ruleNm = "";
					if(vo.rule == "0") ruleNm = "시스템관리자";
					else if(vo.rule == "1") ruleNm = "관리자";										
					else ruleNm = "일반사용자";
					strHtml += '<p>'+ruleNm+'</p>';
					strHtml += '<p>'+vo.corpGroupNm+'/'+vo.corpPosition+'</p>';
					strHtml += '</td>';
					strHtml += '<td>';
					strHtml += '<p>'+vo.phone+'</p>';
					strHtml += '<p>'+vo.mobilePhone+'</p>';
					strHtml += '</td>';
					strHtml += '<td>'+vo.descript+'</td>';
					if(vo.rule != "0" && vo.rule != "1"){
						strHtml += '<td><a href="#none" class="btn_edit" data-key='+vo.accountKey+'><img src="${pathCommon}/images/btn3.jpg" alt=""></a></td>';
					} else strHtml += '<td></td>';
					strHtml += '</tr>';	
				}
				
				
				return strHtml;
			}
		});
	
	
	
	$("body").on("click",".btn_edit",function(){
		var key = $(this).data("key")?$(this).data("key"):"";
		$VDAS.instance_post("/config/registAccountDetail.do",{accountKey:key});				
	});
	
	$VDAS.http_post("/account/getAccountCnt.do",{},{
		success : function(r){
			var data = r.result;
			
			$("#titleCnt1").text(data.total+"명");
		}
	});
});







</script>
<form id="searchFrm" style="display:none">
	<input type="hidden" name="searchType" value="" />
	<input type="hidden" name="searchText" value="" />
</form>
<div class="right_layout">
	<h2 class="tit_sub">
		사용자 등록
		<p>소속직원의 배차신청ㆍ운행기록부 관리, 위험운전 관리등을 위해 사용자 등록이 필요합니다.</p>
	</h2>
	<div class="sub_top1">
		<span>전체 등록 사용자<b id="titleCnt1"></b></span>
		<strong id="nowDate"></strong>
	</div>
	<h3 class="h3tit">사용자등록</h3>
	<div class="box1">
		소속직원의 정보를 상세하게 입력해주세요.
		<br/>
		소속직원이 많아 일일이 등록이 번거로우신 경우 대량등록을 이용하시면 편리합니다.
		<div class="btn_box btn_pst">
			<a href="#none" class="btn_edit">개별 등록</a>
			<a href="#none" id="btn_upload">대량 등록</a>
		</div>
	</div>
	<h3 class="h3tit">사용자 조회</h3>
	<div class="search_box1">
		<form id="frmSearch" method="post" onSubmit="return false;">
			<div class="search_box1_1">
				<div class="select_type1">
					<div class="select_type1_1">
						<select id="searchType" class="sel">
							<option value="">전체</option>
							<option value="corpGroup">부서</option>
							<option value="name">이름</option>
							<option value="accountNm">아이디</option>
						</select>
					</div>
				</div><input type="text" id="searchText" class="search_input" />
			</div><a href="#none" class="btn_search">조회</a>
		</form>
	</div>	
	<div class="tab_layout">
		<div style="margin-bottom: 10px; overflow: hidden;">
			<div class="select_type2_1">
				<a href="#none" class="" id="btn_del"><img src="${pathCommon}/images/${_LANG}/btn_delete.jpg" alt=""></a>
			</div>
		</div>
		<table class="table1" id="contentsTable">
			<colgroup>
				<col style="width:70px;" />
				<col />
				<col style="width:140px;"/>
				<col style="width:130px;"/>
				<col style="width:140px;" />
				<col style="width:80px;" />
			</colgroup>
			<thead>
				<tr>
					<th><input type="checkbox" id="all_checked"></th>
					<th>사용자 정보</th>
					<th>권한/부서/직급</th>
					<th>연락처</th>
					<th>비고</th>
					<th>수정</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<div id="dialog" title="차량 사용 정지" class="myPopup" style="display: none">
	<div class="pop_close"></div>
	<p class="pop_txt">
		<em id="dialog_vehicle"></em> 삭제하시겟습니까?
	</p>
	<div class="pop_notice">
		* 요금과 관련된 사항은 매뉴얼 또는 뷰카 고객센터(1599-8439)로 문의해주세요.
	</div>
</div>
<!-- 사용자등록_대량등록팝업 -->
<div id="dialog2" title="사용자 대량 등록" class="myPopup" style="display: none">
	<div class="pop_close"></div>
	<div class="user_up_1">
		<ul>
			<li>
				<h2><img src="${pathCommon}/images/${_LANG}/user_up_img1.jpg" alt="" /></h2>
				<p class="text1">
					<a href="${path}/com/getImportUserTemplate.do"><img src="${pathCommon}/images/${_LANG}/user_up_img2.jpg" alt="" /></a>
					사용자등록 양식을 다운로드 받아 해당양식에 맞게 기재하신 후 아래에서 업로드하시면 즉시 등록됩니다.
				</p>
			</li>
			<li>
				<h2><img src="${pathCommon}/images/${_LANG}/user_up_img3.jpg" alt="" /></h2>
				<div class="car_info4_1">
					<div class="car_info4_2">
						<span class="tit3">파일업로드</span>
						<fieldset style="border:0">
							<form id="fileFrm" class="P10" action="${path}/com/fileUpload.do" method="post" enctype="multipart/form-data">
								<input type="hidden" name="uploadType" value="excelUploadUser" />
								<div class="btn_file"><input type="file" name="uploadFile" class="multi" accept="xls|xlsx" maxlength="1"/></div>
							</form>
						</fieldset>
					</div>
				</div>				
			</li>
		</ul>
	</div>
</div>
<!--// 사용자등록_대량등록팝업 -->
</c:if>
<c:if test="${VDAS.rule ne '0' and VDAS.rule ne '1' }">
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("M0002");
});
</script>
<div class="right_layout">
	<img src="${pathCommon}/images/${_LANG}/authError.png" alt="" style="width:100%">
</div>
</c:if>