<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>




<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta name="description" content="VIEWCAR - For Your Smart Driving">
		<meta property="og:title" content="VIEWCAR">
		<meta property="og:description" content="VIEWCAR - For Your Smart Driving">
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/js/jqueryui/jquery-ui.css"/>
		<%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/content.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/content_costom.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/style.css" /> --%>
		<link rel="manifest" href="${pageContext.request.contextPath}/common/json/manifest.json">
		<%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/jsDatePick_ltr.min.css" media="all" /> --%>
		
		<%@include file="/WEB-INF/jsp/common/common.jsp"%>
		
		<script src="${pageContext.request.contextPath}/common/js/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/common.js"></script>
		<script src="${pageContext.request.contextPath}/common/js/topup.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jqueryui/jquery-ui.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/common.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.stylish-select.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jsDatePick.jquery.min.1.3.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/util.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.form.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.MultiFile.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/message/${_LANG}.js"></script>
		
		<script type="text/javascript" src="https://www.gstatic.com/firebasejs/4.1.3/firebase.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/commonDev.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/new/js/v4.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/xlsx/xlsx.full.min.js"></script>

             

<script type="text/javascript">
	$(function() {

		function generatorExcel(fileEle,customHeader, cb) {

			var to_json = function(workbook,ch) {
				var parseData = [];
				if(workbook.SheetNames.length > 0){
					var sheet = workbook.Sheets[workbook.SheetNames[0]];
					if(ch.length > 0){
						parseData = XLSX.utils.sheet_to_json(sheet, {
							header : ch,
							range : 1
						});
					}else{
						parseData = XLSX.utils.sheet_to_json(sheet);
					}
				}
				return parseData;

			};

			var fileExtension = function(filename, opts) {
				if (!opts)
					opts = {};
				if (!filename)
					return "";
				var ext = (/[^./\\]*$/.exec(filename) || [ "" ])[0];
				return opts.preserveCase ? ext : ext.toLowerCase();
			};

			if (fileEle.length == 0) {
				cb([]);
				return;
			}
			var fileObj = fileEle[0];
			var f = fileObj.files[0];
			
			if(fileObj.files.length == 0){
				alert('파이을 선택해주세요');
				cb([]);
				return;
			}
			
			var ext = fileExtension(f.name);

			if (ext == 'xls' || ext == 'xlsx') {

			} else {
				alert('업로드 할수 없는 파일 형태 입니다.')
				cb([]);
				return;
			}
			
			var reader = new FileReader();
			reader.onload = function(e) {
				var data = e.target.result;
				
				var workbook = XLSX.read(data, {
					type: 'binary',
					dateNF : 'yyyy-mm-dd',
					cellFormula : false,
					cellHTML : false
				});

				cb(to_json(workbook,customHeader));
			};
			
			if (!FileReader.prototype.readAsBinaryString) {
		        FileReader.prototype.readAsBinaryString = function (fileData) {
		          var binary = "";
		          var reader = new FileReader();
		          reader.onload = function (e) {
		            var bytes = new Uint8Array(reader.result);
		            var length = bytes.byteLength;
		            for (var i = 0; i < length; i++) {
		              binary += String.fromCharCode(bytes[i]);
		            }
		            var workbook = XLSX.read(binary, {
						type: 'binary',
						dateNF : 'yyyy-mm-dd',
						cellFormula : false,
						cellHTML : false
					});
		            
		            cb(to_json(workbook,customHeader));

		          }
		          reader.readAsArrayBuffer(fileData);
		        }
		      }
		    reader.readAsBinaryString(f);

		}

		$('#file').on('change', function() {
			//["A","B","C","D","E","F","G","H"]
			generatorExcel($('#file'),[], function(data) {
				console.log(data)
			});

		});

	});
</script>
	</head>
    <body>
    	<input type="file" id="file"/>
    </body>
</html>