<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
});
</script>
 <!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>제품 A/S접수</h2>
                    <span>제품 A/S접수하시면 신속하게 처리될 수 있도록 하겠습니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="cs-page">
                    <div class="box-layout">
                        <div class="title">
                            <h3 class="tit3">제품 A/S접수</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
                            <div class="info-txt">
                                접수 후, 신속하게 처리될 수 있도록 하겠습니다. 불편을 끼쳐 드려 죄송합니다.
                            </div>
                            <table class="table mgt20">
                                <caption>제품 A/S접수 작성하기</caption>
                                <tbody>
                                    <tr>
                                        <th scope="row">차량번호</th>
                                        <td>
                                            업데이트테스트
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">단말타입</th>
                                        <td>
                                            von-S31
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">단말시리얼</th>
                                        <td>
                                            A0000077
                                            <button class="btn btn02 md">검색</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">내용</th>
                                        <td>
                                            <textarea rows="10" class="w100" placeholder="에디터로 교체해주세요."></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">첨부파일</th>
                                        <td>
                                            <input type="file" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <!-- 하단 버튼 -->
                            <div class="btn-bottom">
                                <button class="btn btn02">취소</button>
                                <button class="btn btn03">확인</button>
                            </div>
                            <!--/ 하단 버튼 -->
                        </div>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->