<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	$("#view-dialog").dialog({
        autoOpen: true,
        show: {
            duration: 500
        },
        width: '960',
        modal: true,
        buttons: {
            "확인": function() {
                $(this).dialog("close");
            }
        }
    });

    $("#view-open").on("click", function() {
        $("#view-dialog").dialog("open");
    });
    
    $('#productAsRegBtn').on('click',function(){
    	$V4.move('/customerCenter/productAs/reg');
    });
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>제품 A/S접수</h2>
                    <span>제품 A/S접수하시면 신속하게 처리될 수 있도록 하겠습니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="cs-page">
                    <div class="">
                        <!-- 서비스불편신고 -->
                        <div class="box-register">
                            접수전, VIEWCAR 스마트폰 어플 고객등록시 고객차량의 차종,모델명,연식 등이 정확히 기재되었는지 확인해주십시오. <br /> 스마트폰 어플리케이션의 최신 어플,펌웨어,차량DB로 업그레이드가 되었는지 확인해주세요.

                            <div class="right">
                                <button id="productAsRegBtn" type="button" class="btn btn01">제품 A/S접수</button>
                            </div>
                        </div>
                        <!--/ 서비스불편신고 -->

                        <h3 class="tit2 mgt50">나의 제품A/S접수</h3>
                        <!-- table 버튼 -->
                        <div class="btn-function title-side">
                            <div class="right">
                                <select name="">
									<option value="">5건씩 보기</option>
								</select>
                            </div>
                        </div>
                        <!--/ table 버튼 -->

                        <table class="table list mgt20">
                            <caption>등록된 서비스 리스트</caption>
                            <colgroup>
                                <col style="width:8%" />
                                <col />
                                <col style="width:18%" />
                                <col style="width:18%" />
                                <col style="width:18%" />
                                <col style="width:18%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">단말기 타입</th>
                                    <th scope="col">단말기 시리얼</th>
                                    <th scope="col">차량번호</th>
                                    <th scope="col">작성일</th>
                                    <th scope="col">상태</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>3</td>
                                    <td>Son-A003423</td>
                                    <td>Af436534</td>
                                    <td><a href="javascript:;" id="view-open">11라1111</a></td>
                                    <td>2018.05.01</td>
                                    <td>답변대기</td>
                                </tr>
                            </tbody>
                        </table>

                        <!-- 페이징 -->
                        <div id="paging">
                            <a href="" class="first icon-spr">제일 처음으로</a>
                            <a href="" class="prev icon-spr">이전으로</a>
                            <span class="current">1</span>
                            <a href="" class="num">2</a>
                            <a href="" class="num">3</a>
                            <a href="" class="num">4</a>
                            <a href="" class="num">5</a>
                            <a href="" class="next icon-spr">다음으로</a>
                            <a href="" class="last icon-spr">제일 마지막으로</a>
                        </div>
                        <!--/ 페이징 -->
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->
        <!-- 게시물보기 팝업 -->
    <div id="view-dialog" title="제품AS접수 제목">
        제품AS접수 내용출력
    </div>
    <!--/ 게시물보기 팝업 -->