<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	$("#view-dialog").dialog({
        autoOpen: true,
        show: {
            duration: 500
        },
        width: '960',
        modal: true,
        buttons: {
            "확인": function() {
                $(this).dialog("close");
            }
        }
    });

    $("#view-open").on("click", function() {
        $("#view-dialog").dialog("open");
    });
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>FAQ</h2>
                    <span>고객님이 자주 묻는 질문과 답변이 있습니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="cs-page">
                    <div class="">
                        <!-- 상단 검색 -->
                        <div class="top-search mgt20">
                            <select name="">
									<option value="">전체</option>
								</select>
                            <input type="text" name="" />
                            <button class="btn btn03">조회</button>
                        </div>
                        <!--/ 상단 검색 -->

                        <h3 class="tit2 mgt30">FAQ 목록</h3>
                        <!-- table 버튼 -->
                        <div class="btn-function title-side">
                            <div class="right">
                                <select name="">
									<option value="">5건씩 보기</option>
								</select>
                            </div>
                        </div>
                        <!--/ table 버튼 -->

                        <table class="table list mgt20">
                            <caption>등록된 서비스 리스트</caption>
                            <colgroup>
                                <col style="width:8%" />
                                <col style="width:20%" />
                                <col />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">
                                        <select name="" class="arrow">
											<option value="">분류</option>
											<option value="">회원ㆍ로그인</option>
											<option value="">운행정보</option>
											<option value="">차량관리</option>
											<option value="">보고서ㆍ결재</option>
											<option value="">오픈 API</option>
											<option value="">환경설정</option>
											<option value="">제휴그룹</option>
										</select>
                                    </th>
                                    <th scope="col">제목</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>로그인관련</td>
                                    <td class="left"><a href="javascript:;" id="view-open">[결제] 1개월권을 구매할 경우 사용일수는 며칠인가요?</a></td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>로그인관련</td>
                                    <td class="left"><a href="javascript:;" id="view-open">[결제] 1개월권을 구매할 경우 사용일수는 며칠인가요?</a></td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>로그인관련</td>
                                    <td class="left"><a href="javascript:;" id="view-open">[결제] 1개월권을 구매할 경우 사용일수는 며칠인가요?</a></td>
                                </tr>
                            </tbody>
                        </table>

                        <!-- 페이징 -->
                        <div id="paging">
                            <a href="" class="first icon-spr">제일 처음으로</a>
                            <a href="" class="prev icon-spr">이전으로</a>
                            <span class="current">1</span>
                            <a href="" class="num">2</a>
                            <a href="" class="num">3</a>
                            <a href="" class="num">4</a>
                            <a href="" class="num">5</a>
                            <a href="" class="next icon-spr">다음으로</a>
                            <a href="" class="last icon-spr">제일 마지막으로</a>
                        </div>
                        <!--/ 페이징 -->
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->
        
         <!-- 게시물보기 팝업 -->
    <div id="view-dialog" title="[결제] 1개월권을 구매할 경우 사용일수는 며칠인가요?">
        1개월 이용권을 결제하셨을 경우 사용일수는 32일 입니다.<br /> (3개월 이용권을 결제하셨을 경우는 사용일수 96일 입니다)<br /> 기존 사용만료기간에 결제하신 사용일수가 추가됩니다.<br /> 감사합니다.

    </div>
    <!--/ 게시물보기 팝업 -->