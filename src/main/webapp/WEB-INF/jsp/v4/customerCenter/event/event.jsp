<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	$("#view-dialog").dialog({
        autoOpen: true,
        show: {
            duration: 500
        },
        width: '960',
        modal: true,
        buttons: {
            "확인": function() {
                $(this).dialog("close");
            }
        }
    });

    $("#view-open").on("click", function() {
        $("#view-dialog").dialog("open");
    });
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>이벤트ㆍ할인정보</h2>
                    <span>ViewCar에서 드리는 특별한 이벤트 및 할인정보를 확인해보세요.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="cs-page">
                    <div class="">
                        <h3 class="tit2 mgt50">공지사항 목록</h3>
                        <!-- table 버튼 -->
                        <div class="btn-function title-side">
                            <div class="right">
                                <select name="">
									<option value="">5건씩 보기</option>
								</select>
                            </div>
                        </div>
                        <!--/ table 버튼 -->

                        <table class="table list mgt20">
                            <caption>등록된 서비스 리스트</caption>
                            <colgroup>
                                <col style="width:8%" />
                                <col style="width:20%" />
                                <col />
                                <col style="width:13%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">
                                        <select name="" class="arrow">
											<option value="">분류</option>
											<option value="">서비스 점검</option>
											<option value="">업데이트</option>
											<option value="">중요</option>
											<option value="">알림</option>
											<option value="">일반</option>
										</select>
                                    </th>
                                    <th scope="col">제목</th>
                                    <th scope="col">작성일</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>3</td>
                                    <td>업데이트</td>
                                    <td class="left"><a href="javascript:;" id="view-open">[업데이트] 단말기 업데이트 안내</a></td>
                                    <td>2018.05.01</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>업데이트</td>
                                    <td class="left"><a href="">[업데이트] 단말기 업데이트 안내</a></td>
                                    <td>2018.05.01</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>업데이트</td>
                                    <td class="left"><a href="">[업데이트] 단말기 업데이트 안내</a></td>
                                    <td>2018.05.01</td>
                                </tr>
                            </tbody>
                        </table>

                        <!-- 페이징 -->
                        <div id="paging">
                            <a href="" class="first icon-spr">제일 처음으로</a>
                            <a href="" class="prev icon-spr">이전으로</a>
                            <span class="current">1</span>
                            <a href="" class="num">2</a>
                            <a href="" class="num">3</a>
                            <a href="" class="num">4</a>
                            <a href="" class="num">5</a>
                            <a href="" class="next icon-spr">다음으로</a>
                            <a href="" class="last icon-spr">제일 마지막으로</a>
                        </div>
                        <!--/ 페이징 -->
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->
        <!-- 게시물보기 팝업 -->
    <div id="view-dialog" title="이벤트 할인정보 게시글 제목">
        이벤트 할인정보 게시글 내용이 노출됩니다.
    </div>
    <!--/ 게시물보기 팝업 -->