<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>단말기 업데이트</h2>
                    <span>단말기의 차량DB, 펌웨어를 최신 버전으로 업데이트가 필요합니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="api-page">
                    <div class="">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                            <div class="item">
                                <span>
									등록 단말기수
									<strong>10</strong>대
								</span>
                            </div>

                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <!-- 업데이트방법 -->
                        <h3 class="tit2 mgt30">업데이트 방법</h3>
                        <div class="box-register">
                            ① PC매니져 프로그램을 다운 받아주세요.<br /> windows7 64bit를 사용하시는 고객님은 '윈도우7 USB드라이버'를 다운받아<br /> 내컴퓨터 -> 설정 -> 장치관리자 -> PORT에서 자스텍 USB드라이버를 업데이트해주세요.<br /> ※윈도우(windows7 이상)지원 / 사용 가능 브라우져 : Chrome, Microsoft edge<br /><br /> ② 업데이트할 단말기를
                            USB로 연결시켜주세요.<br /> ③ 해당 단말기의 업데이트 버튼을 클릭해주세요.

                            <div class="right">
                                <button class="btn btn01">PC매니져 다운로드</button>
                            </div>
                        </div>
                        <!--/ 업데이트방법 -->

                        <!-- 상단 검색 -->
                        <h3 class="tit2 mgt30">등록단말기 조회</h3>
                        <div class="top-search mgt20">
                            <select name="">
								<option value="">전체</option>
								<option value="">차량번호</option>
								<option value="">단말정보</option>
							</select>
                            <input type="text" name="" />
                            <button class="btn btn03">조회</button>
                        </div>
                        <!--/ 상단 검색 -->

                        <!-- table 버튼 -->
                        <div class="btn-function">
                            <div class="right">
                                <select name="">
									<option value="">5건씩 보기</option>
								</select>
                            </div>
                        </div>
                        <!--/ table 버튼 -->

                        <table class="table list mgt20">
                            <caption>등록된 서비스 리스트</caption>
                            <colgroup>
                                <col style="width:14%" />
                                <col style="width:14%" />
                                <col />
                                <col style="width:14%" />
                                <col style="width:14%" />
                                <col style="width:14%" />
                                <col style="width:14%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="col">차량정보</th>
                                    <th scope="col">단말정보</th>
                                    <th scope="col">총주행거리</th>
                                    <th scope="col">부저음설정</th>
                                    <th scope="col">차량DB</th>
                                    <th scope="col">펌웨어</th>
                                    <th scope="col">업데이트</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>FX212<br />70나8681</td>
                                    <td>von-f31<br />A0001857</td>
                                    <td>4017km</td>
                                    <td>
                                        <div class="btn-onoff">
                                            <button class="active">ON</button>
                                            <button>OFF</button>
                                        </div>
                                    </td>
                                    <td>최신</td>
                                    <td>최신</td>
                                    <td><button class="btn btn02 md">업데이트</button></td>
                                </tr>
                            </tbody>
                        </table>

                        <!-- 페이징 -->
                        <div id="paging">
                            <a href="" class="first icon-spr">제일 처음으로</a>
                            <a href="" class="prev icon-spr">이전으로</a>
                            <span class="current">1</span>
                            <a href="" class="num">2</a>
                            <a href="" class="num">3</a>
                            <a href="" class="num">4</a>
                            <a href="" class="num">5</a>
                            <a href="" class="next icon-spr">다음으로</a>
                            <a href="" class="last icon-spr">제일 마지막으로</a>
                        </div>
                        <!--/ 페이징 -->
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->