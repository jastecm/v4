<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	$("#view-dialog").dialog({
		autoOpen: true,
		show: {
			duration: 500
		},
		width: '960',
		modal: true,
		buttons: {
			"확인": function () {
				$(this).dialog("close");
			}
		}
	});

	$("#view-open").on("click", function () {
		$("#view-dialog").dialog("open");
	});
});
</script>
<!-- 본문 -->
		<section id="container">
			<div id="sub-container">
				<!-- 상단 타이틀 -->
				<div class="page-header">
					<h2>공지사항</h2>
					<span>ViewCar에서 공지사항을 알려드립니다.</span>
				</div>
				<!--/ 상단 타이틀 -->

				<!-- 콘텐츠 본문 -->
				<div id="contents-page" class="cs-page">
					<div class="">
						<h3 class="tit2 mgt50">공지사항 목록</h3>
						<!-- table 버튼 -->
						<div class="btn-function title-side">
							<div class="right">
								<select name="">
									<option value="">5건씩 보기</option>
								</select>
							</div>
						</div>
						<!--/ table 버튼 -->

						<table class="table list mgt20">
							<caption>등록된 서비스 리스트</caption>
							<colgroup>
								<col style="width:8%" />
								<col style="width:20%" />
								<col />
								<col style="width:13%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col">No.</th>
									<th scope="col">
										<select name="" class="arrow">
											<option value="">분류</option>
											<option value="">서비스 점검</option>
											<option value="">업데이트</option>
											<option value="">중요</option>
											<option value="">알림</option>
											<option value="">일반</option>
										</select>
									</th>
									<th scope="col">제목</th>
									<th scope="col">작성일</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>3</td>
									<td>업데이트</td>
									<td class="left"><a href="javascript:;" id="view-open">[업데이트] 단말기 업데이트 안내</a></td>
									<td>2018.05.01</td>
								</tr>
								<tr>
									<td>2</td>
									<td>업데이트</td>
									<td class="left"><a href="">[업데이트] 단말기 업데이트 안내</a></td>
									<td>2018.05.01</td>
								</tr>
								<tr>
									<td>1</td>
									<td>업데이트</td>
									<td class="left"><a href="">[업데이트] 단말기 업데이트 안내</a></td>
									<td>2018.05.01</td>
								</tr>
							</tbody>
						</table>

						<!-- 페이징 -->
						<div id="paging">
							<a href="" class="first icon-spr">제일 처음으로</a>
							<a href="" class="prev icon-spr">이전으로</a>
							<span class="current">1</span>
							<a href="" class="num">2</a>
							<a href="" class="num">3</a>
							<a href="" class="num">4</a>
							<a href="" class="num">5</a>
							<a href="" class="next icon-spr">다음으로</a>
							<a href="" class="last icon-spr">제일 마지막으로</a>
						</div>
						<!--/ 페이징 -->
					</div>
				</div>
				<!--/ 콘텐츠 본문 -->
			</div>
		</section>
		<!--/ 본문 -->
		<!-- 게시물보기 팝업 -->
	<div id="view-dialog" title="[업데이트] 단말기 업데이트 안내">
		안녕하세요. 뷰카입니다.<br /><br /> von-S31 단말기 펌웨어 1.13이하 버전에서 운행일자가 +2일 되는 버그가 발생하였습니다.<br /> 3월1일이후 운행기록에서 동일한 증상이 발생하는것으로
		확인되었습니다.<br /> 해당 버그는 윤달 계산공식 오류로 인한 버그이며 단말기 업데이트시에 해당 문제를 해결할 수 있습니다.<br /><br /> 단말기 업데이트 방법<br /> 환경설정->단말기 업데이트<br /><br />
		서비스 이용에 불편을 드려서 대단히 죄송합니다.
	</div>
	<!--/ 게시물보기 팝업 -->