<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>서비스 불편신고</h2>
                    <span>서비스 불편신고 등록을 하거나 신고에 대한 답변을 확인 할 수 있습니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="cs-page">
                    <div class="box-layout">
                        <div class="title">
                            <h3 class="tit3">서비스 불편신고</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
                            <div class="info-txt">
                                불편한 사항이나 오류, 개선, 제안에 관한 사항을 알려주세요. 보다 안정된 서비스가 될 수 있도록 최선을 다하겠습니다.
                            </div>
                            <table class="table mgt20">
                                <caption>서비스 불편신고 작성하기</caption>
                                <tbody>
                                    <tr>
                                        <th scope="row">제목</th>
                                        <td>
                                            <input type="text" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">내용</th>
                                        <td>
                                            <textarea rows="10" class="w100" placeholder="에디터로 교체해주세요."></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">첨부파일</th>
                                        <td>
                                            <input type="file" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <!-- 하단 버튼 -->
                            <div class="btn-bottom">
                                <button class="btn btn02">취소</button>
                                <button class="btn btn03">확인</button>
                            </div>
                            <!--/ 하단 버튼 -->
                        </div>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->