<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	$("#view-dialog").dialog({
        autoOpen: true,
        show: {
            duration: 500
        },
        width: '960',
        modal: true,
        buttons: {
            "확인": function() {
                $(this).dialog("close");
            }
        }
    });

    $("#view-open").on("click", function() {
        $("#view-dialog").dialog("open");
    });
    
    $('#reportInconvenienceRegBtn').on('click',function(){
    	$V4.move('/customerCenter/reportInconvenience/reg');
    });
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>서비스 불편신고</h2>
                    <span>서비스 불편신고 등록을 하거나 신고에 대한 답변을 확인 할 수 있습니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="cs-page">
                    <div class="">
                        <!-- 서비스불편신고 -->
                        <div class="box-register">
                            서비스를 이용하면서 겪은 불편사항, 장애, 오류 및 서비스에 대한 개선방안, 제안 등을 등록 합니다.

                            <div class="right">
                                <button id="reportInconvenienceRegBtn" type="button" class="btn btn01">서비스불편신고</button>
                            </div>
                        </div>
                        <!--/ 서비스불편신고 -->

                        <h3 class="tit2 mgt50">나의 불편신고</h3>
                        <!-- table 버튼 -->
                        <div class="btn-function title-side">
                            <div class="right">
                                <select name="">
									<option value="">5건씩 보기</option>
								</select>
                            </div>
                        </div>
                        <!--/ table 버튼 -->

                        <table class="table list mgt20">
                            <caption>등록된 서비스 리스트</caption>
                            <colgroup>
                                <col style="width:8%" />
                                <col />
                                <col style="width:13%" />
                                <col style="width:13%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">제목</th>
                                    <th scope="col">작성일</th>
                                    <th scope="col">답변</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>3</td>
                                    <td class="left"><a href="javascript:;" id="view-open">결제영수증 출력 안되나요?</a></td>
                                    <td>2018.05.01</td>
                                    <td>답변대기</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td class="left"><a href="javascript:;" id="view-open">결제영수증 출력 안되나요?</a></td>
                                    <td>2018.05.01</td>
                                    <td>답변대기</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td class="left"><a href="javascript:;" id="view-open">결제영수증 출력 안되나요?</a></td>
                                    <td>2018.05.01</td>
                                    <td>답변대기</td>
                                </tr>
                            </tbody>
                        </table>

                        <!-- 페이징 -->
                        <div id="paging">
                            <a href="" class="first icon-spr">제일 처음으로</a>
                            <a href="" class="prev icon-spr">이전으로</a>
                            <span class="current">1</span>
                            <a href="" class="num">2</a>
                            <a href="" class="num">3</a>
                            <a href="" class="num">4</a>
                            <a href="" class="num">5</a>
                            <a href="" class="next icon-spr">다음으로</a>
                            <a href="" class="last icon-spr">제일 마지막으로</a>
                        </div>
                        <!--/ 페이징 -->
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->