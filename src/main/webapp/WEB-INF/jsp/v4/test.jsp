<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>




<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta name="description" content="VIEWCAR - For Your Smart Driving">
		<meta property="og:title" content="VIEWCAR">
		<meta property="og:description" content="VIEWCAR - For Your Smart Driving">
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/js/jqueryui/jquery-ui.css"/>
		<%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/content.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/content_costom.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/style.css" /> --%>
		<link rel="manifest" href="${pageContext.request.contextPath}/common/json/manifest.json">
		<%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/jsDatePick_ltr.min.css" media="all" /> --%>
		
		<%@include file="/WEB-INF/jsp/common/common.jsp"%>
		
		<script src="${pageContext.request.contextPath}/common/js/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/common.js"></script>
		<script src="${pageContext.request.contextPath}/common/js/topup.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jqueryui/jquery-ui.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/common.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.stylish-select.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jsDatePick.jquery.min.1.3.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/util.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.form.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.MultiFile.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/message/${_LANG}.js"></script>
		
		<script type="text/javascript" src="https://www.gstatic.com/firebasejs/4.1.3/firebase.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/commonDev.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/new/js/v4.js"></script>

             

<script type="text/javascript">
$(function(){
	$("#test1").on('click',function(){
		
		var sendData = {
				  "approvalAccountKey": "",
				  "approvalGroupKey": "1",
				  "approvalType": "0",
				  "autoReport": "0",
				  "autoReportContents": "1",
				  "autoReportPurpose": "1",
				  "color": "blue",
				  "connectionKey": "connKey",
				  "convCode": "V091D20A1204",	 //required
				  "corpGroupKey": "string",
				  "deviceSn": "A0000015",	 //required
				  "fixed": "0",
				  "fixedAccountKey": "",
				  "fixedContents": "",
				  "fixedDriverKey": "",
				  "holidayPass": "0",
				  "locationSetting": "1",
				  "plateNum": "대표님차",       //required
				  "totDistance": 0, 	 //required
				  "vehicleBizType": "1",
				  "vehicleBodyNum": "",
				  "vehicleDescript": "",
				  "vehicleInsureNm": "",
				  "vehicleInsurePhone": "",
				  "vehicleKey": "",
				  "vehicleMaintenanceDate": "2000-01-01",
				  "vehiclePayment": 0,
				  "vehicleRegDate": "2000-01-01",
				  "vehicleType": "1"
				}
		$.ajax({
			url: "${defaultPath}/api/1/vehicle"
			,dataType : 'json'
			,contentType: "application/json; charset=UTF-8"
			,type:"POST"
			,data:JSON.stringify(sendData)
			,cache:!1
			,headers : {"Cache-Control":"no-cache"}
			,beforeSend : function(xhr){
				xhr.setRequestHeader("key", $('#token').val()); 
			}
			,success:function(t){
				console.log(t);
				$("#test3").trigger('click');
			}		
			,error:function(t){
				console.log(t);
			}
		});
		
	});
	
$("#test2").on('click',function(){
		
		var sendData = {
				  "approvalAccountKey": "",
				  "approvalGroupKey": "1",
				  "approvalType": "0",
				  "autoReport": "0",
				  "autoReportContents": "1",
				  "autoReportPurpose": "1",
				  "color": "blue",
				  "connectionKey": "connKey",
				  "convCode": "A011D20A1201",	 //required
				  "corpGroupKey": "string",
				  "deviceSn": "A0000066",	 //required
				  "fixed": "0",
				  "fixedAccountKey": "string",
				  "fixedContents": "string",
				  "fixedDriverKey": "string",
				  "holidayPass": "0",
				  "locationSetting": "1",
				  "plateNum": "14조4938",       //required
				  "totDistance": 0, 	 //required
				  "vehicleBizType": "1",
				  "vehicleBodyNum": "ASDF1234",
				  "vehicleDescript": "차량기타정보",
				  "vehicleInsureNm": "보험번호0001",
				  "vehicleInsurePhone": "보험전화번호",
				  "vehicleKey": "",
				  "vehicleMaintenanceDate": "1986-10-24",
				  "vehiclePayment": 0,
				  "vehicleRegDate": "1986-10-24",
				  "vehicleType": "1"
				}
		console.log("test");
		$.ajax({
			url: "${defaultPath}/api/1/vehicleTemp"
			,dataType : 'json'
			,contentType: "application/json; charset=UTF-8"
			,type:"POST"
			,data:JSON.stringify(sendData)
			,cache:!1
			,headers : {"Cache-Control":"no-cache"}
			,beforeSend : function(xhr){
				xhr.setRequestHeader("key", $('#token').val()); 
			}
			,success:function(t){
				alert("생성");
				console.log(t);
			}		
			,error:function(t){
				console.log(t);
			}
		});
		
	});
	
	$("#test3").on('click',function(){
		
		$.ajax({
			url: "${defaultPath}/api/1/vehicle"
			,dataType : 'json'
			,contentType: "application/json; charset=UTF-8"
			,type:"GET"
			,data: {}
			,cache:!1
			,headers : {"Cache-Control":"no-cache"}
			,beforeSend : function(xhr){
				xhr.setRequestHeader("key", $('#token').val()); 
			}
			,success:function(t){
				alert("가져옴");
				console.log(t);
				var data = t.result;
				$('#vehicleList').empty();
				for(var i = 0 ; i < data.length; i++){
					$('#vehicleList').append("<li><span>"+data[i].vehicleKey+"</span><span> - "+data[i].plateNum+"</span><button type='button' class='vehicleDel'>삭제</button><button type='button' class='vehicleUpdate'>수정</button></li>")
				}
			}		
			,error:function(t){
				console.log(t);
			}
		});
		
	});

	$('body').on('click','.vehicleDel',function(){
		debugger
		var vehicleKey = $(this).prev().prev().text();
		
		$.ajax({
			url: "${defaultPath}/api/1/vehicle/"+vehicleKey
			,dataType : 'json'
			,contentType: "application/json; charset=UTF-8"
			,type:"DELETE"
			,data: {}
			,cache:!1
			,headers : {"Cache-Control":"no-cache"}
			,beforeSend : function(xhr){
				xhr.setRequestHeader("key", $('#token').val()); 
			}
			,success:function(t){
				debugger
				alert("삭제 되었습니다.");
				$("#test3").trigger('click');
				
			}		
			,error:function(t){
				console.log(t);
			}
		}).done(function(){
			alert("done")
		});
		
	});
	
	$('body').on('click','.vehicleUpdate',function(){
		debugger
		var vehicleKey = $(this).prev().prev().text();
		
		var sendData = {
				  "approvalAccountKey": "string",
				  "approvalGroupKey": "string",
				  "approvalType": "string",
				  "autoReport": "string",
				  "autoReportContents": "string",
				  "autoReportPurpose": "string",
				  "color": "string",
				  "connectionKey": "string",
				  "convCode": "string",
				  "corpGroupKey": "string",
				  "deviceSn": "string",
				  "fixed": "string",
				  "fixedAccountKey": "string",
				  "fixedContents": "string",
				  "fixedDriverKey": "string",
				  "holidayPass": "string",
				  "locationSetting": "string",
				  "plateNum": "11조1111",
				  "totDistance": 0,
				  "vehicleBizType": "string",
				  "vehicleBodyNum": "string",
				  "vehicleDescript": "string",
				  "vehicleInsureNm": "string",
				  "vehicleInsurePhone": "string",
				  "vehicleKey": "string",
				  "vehicleMaintenanceDate": "string",
				  "vehiclePayment": 0,
				  "vehicleRegDate": "string",
				  "vehicleType": "string"
				}
		
		$.ajax({
			url: "${defaultPath}/api/1/vehicle/"+vehicleKey
			,dataType : 'json'
			,contentType: "application/json; charset=UTF-8"
			,type:"PUT"
			,data: JSON.stringify(sendData)
			,cache:!1
			,headers : {"Cache-Control":"no-cache"}
			,beforeSend : function(xhr){
				xhr.setRequestHeader("key", $('#token').val()); 
			}
			,success:function(t){
				
				
			}		
			,error:function(t){
				console.log(t);
			}
		});
		
	});
	
	
	$('#login').on('click',function(){
		
		$V4.http_post("/api/1/token/",null,{
			requestMethod : "GET"
			,header : {id : $('#userId').val() , pw : $('#userPw').val()}
			,success : function(t){
				console.log(t);
				$('#token').val(t.result);
			},error : function(t){
				console.log(t);
				if(t.responseJSON.result == "token.notActive"){
					var sendData = {
							"id" : $('#userId').val(),
							"pw" : $('#userPw').val(),
							"expire" : $('#expire').val()
					}
					
					$V4.http_post("/api/1/token/",sendData,{
						success:function(t){
							console.log(t);
							$('#token').val(t.result);
							
						}		
						,error:function(t){
							
						}
					});
				}
			}
		});
		/*
		$.ajax({
			url: "${defaultPath}/api/1/token/"
			,dataType : 'text'
			,contentType: "application/json; charset=UTF-8"
			,type:"GET"
			,cache:!1
			,headers : {"Cache-Control":"no-cache"}
			,beforeSend : function(xhr){
				xhr.setRequestHeader("id", $('#userId').val());
				xhr.setRequestHeader("pw", $('#userPw').val()); 
			}
			,success:function(t){
				console.log(t);
				$('#token').val(t.result);
				
			}		
			,error:function(t){
				console.log(t);
				if(t.responseJSON.result == "token.notActive"){
					var sendData = {
							"id" : $('#userId').val(),
							"pw" : $('#userPw').val(),
							"expire" : $('#expire').val()
					}
					
					$.ajax({
						url: "${defaultPath}/api/1/token/"
						,dataType : 'text'
						,contentType: "application/json; charset=UTF-8"
						,type:"POST"
						,data: JSON.stringify(sendData)
						,cache:!1
						,headers : {"Cache-Control":"no-cache"}
						,beforeSend : function(xhr){
							 
						}
						,success:function(t){
							console.log(t);
							$('#token').val(t.result);
							
						}		
						,error:function(t){
							
						}
					});
				}
			}
		});
		*/
	});
	
	$("#test4").on("click",function(){
		$.ajax({
			url: "${defaultPath}/lora/v4/msg"
			,dataType : 'xml'
			,contentType: "application/xml"
			,type:"POST"
			,data: $("#text4").val()
			,cache:!1
			,headers : {"Cache-Control":"no-cache"}			
			,success:function(t){
				console.log(t);
			}		
			,error:function(t){
				console.log(t);
			}
		});
	});
	
	$("#test5").on("click",function(){
		if($.trim($('#token').val()).length == 0) alert("로그인 토큰먼저");
		if($.trim($('#token').val()).length == 0) return false;
		
		var $sw;
		var fcm_config = {
			apiKey : "AAAAoqqBUIs:APA91bGw5qx4h2WwkzfziY_EjKZa2FMGvPIHgIJQgV7MninxALQcFb1iYCGtQrWjUKn92N3o3l1YJlB0TLTHmL_WLJOjKP7h_Z_8WhaIHR9kO1AQxFSRNUKC297RLq8TeOXv-hwGQ7Fl",
			authDomain: "vdaspro-76df5.firebaseapp.com",
			databaseURL: "https://vdaspro-76df5.firebaseio.com",
			projectId: "vdaspro-76df5",
			storageBucket: "",
			messagingSenderId: "698645303435",
			token : "${VDAS.fcmKey}"
		};
		
		firebase.initializeApp(fcm_config);

		const messaging = firebase.messaging();

		var fcmToken;
		if ('serviceWorker' in navigator) {  
			navigator.serviceWorker.register($VDAS.path+'/common/js/sw.js').then(function(registration){
				console.log('ServiceWorker registration successful with scope: ',    registration);
				$sw = registration;
			    
				//Initialize Firebase
				messaging.useServiceWorker($sw);
			    
				messaging.requestPermission()
					.then(function() {
						console.log('Notification permission granted.');

			    		messaging.getToken()
			    		.then(function(currentToken) {
							if (currentToken) {
			    		    	sendTokenToServer(currentToken);
			    		  	} else {
								console.log('No Instance ID token available. Request permission to generate one.');
			    		  	}
			    		}).catch(function(err) {
			    		console.log('An error occurred while retrieving token. ', err);
			    	});
			    })
			    .catch(function(err) {
			    	
				});
			}).catch(function(err) {
		    	console.log('ServiceWorker registration failed: ', err);
		  	});
		} else {  
			console.warn('Service workers aren\'t supported in this browser.');  
		}  
		  
		  
		messaging.onMessage(function(payload) {
			console.log('Received foground message ', payload);
			const notificationTitle = payload.notification.title;
			const notificationOptions = {
			    body: payload.notification.body
			    ,icon: payload.notification.icon
			};
		  	return $sw.showNotification(notificationTitle,notificationOptions);
	  	});


		function sendTokenToServer(t){
			if(fcm_config.fcmKey != t){
				$("#fcmKey").val(t);
				/*
				$VDAS.http_post("/api/v4/push/refresh",{},{
					beforeSend : function(xhr){
						xhr.setRequestHeader("key", $('#token').val());
						xhr.setRequestHeader("fcmKey", t);
					}
					,success : function(r){
						
						$VDAS.http_post("/push/subscribe.do",{topic:"noti",accept:"1"},{
							success : function(r){
								console.log(r);
							}
						});
						$VDAS.http_post("/push/subscribe.do",{topic:"event",accept:"1"},{
							success : function(r){
								console.log(r);
							}
						});
						$VDAS.http_post("/push/subscribe.do",{topic:"test",accept:"1"},{
							success : function(r){
								console.log(r);
							}
						});
					}
					,requestMethod : "PUT"
				});
				*/
			}
		}
	});
	
	$("#test6").on("click",function(){
		var fcmKey = $.trim($("#fcmKey").val());
		if(!fcmKey) alert("required fcmKey");
		if(!fcmKey) return;
		
		$VDAS.http_post("/api/v4/push/refresh",{},{
			beforeSend : function(xhr){
				xhr.setRequestHeader("key", $('#token').val());
				xhr.setRequestHeader("fcmKey", fcmKey);
			}
			,success : function(r){
				
				$VDAS.http_post("/push/subscribe.do",{topic:"noti",accept:"1"},{
					success : function(r){
						console.log(r);
					}
				});
				$VDAS.http_post("/push/subscribe.do",{topic:"event",accept:"1"},{
					success : function(r){
						console.log(r);
					}
				});
				$VDAS.http_post("/push/subscribe.do",{topic:"test",accept:"1"},{
					success : function(r){
						console.log(r);
					}
				});
			}
			,requestMethod : "PUT"
		});
	});
	
	$("#test7").on("click",function(){
		//등록
		var corpType = $("input[name=corpType]:checked").val();
		var url = "";
		var vehicleKey = $("#fence_vehicleKey").val();
		
		if(corpType == "1") url = "/api/v4/geofence";
		if(corpType == "0") url = "/api/v4/vehicle/"+vehicleKey+"/geofence";
		
		if(url.length == 0) {
			alert("select corpType");
			return false;
		}else{
			if(corpType == "0" && vehicleKey.length == 0){
				alert("required vehicleKey");
				return false;
			}
		}
		
		var sendData = {
				  "lat": $("#fence_lat").val()
				  ,"lon": $("#fence_lon").val()
				  ,"addr": $("#fence_addr").val()
				  ,"addrDetail": $("#fence_addrDetail").val()
				  ,"alies": $("#fence_alies").val()
				  ,"distance": $("#fence_distance").val()
				  ,"push": $("#fence_push").val()
				}
		
		$.ajax({
			url: "${defaultPath}"+url
			,dataType : 'json'
			,contentType: "application/json; charset=UTF-8"
			,type:"POST"
			,data: JSON.stringify(sendData)
			,cache:!1
			,headers : {"Cache-Control":"no-cache"}
			,beforeSend : function(xhr){
				xhr.setRequestHeader("key", $('#token').val()); 
			}
			,success:function(t){
				console.log(t);
			}		
			,error:function(t){
				console.log(t);
			}
		});
	});
	
	$("#test8").on("click",function(){
		//수정
		var corpType = $("input[name=corpType]:checked").val();
		var url = "";
		var vehicleKey = $("#fence_vehicleKey").val();
		var fenceKey = $("#fence_fenceKey").val();
		
		if(corpType == "1") url = "/api/v4/geofence/"+fenceKey;
		if(corpType == "0") url = "/api/v4/vehicle/"+vehicleKey+"/geofence/"+fenceKey;
		
		if(url.length == 0) {
			alert("select corpType");
			return false;
		}else{
			if(corpType == "0" && vehicleKey.length == 0){
				alert("required vehicleKey");
				return false;
			}else if(fenceKey.length == 0){
				alert("required fenceKey");
				return false;
			}
		}
		
		var sendData = {
				  "lat": $("#fence_lat").val()
				  ,"lon": $("#fence_lon").val()
				  ,"addr": $("#fence_addr").val()
				  ,"addrDetail": $("#fence_addrDetail").val()
				  ,"alies": $("#fence_alies").val()
				  ,"distance": $("#fence_distance").val()
				  ,"push": $("#fence_push").val()
				}
		
		$.ajax({
			url: "${defaultPath}"+url
			,dataType : 'json'
			,contentType: "application/json; charset=UTF-8"
			,type:"PUT"
			,data: JSON.stringify(sendData)
			,cache:!1
			,headers : {"Cache-Control":"no-cache"}
			,beforeSend : function(xhr){
				xhr.setRequestHeader("key", $('#token').val()); 
			}
			,success:function(t){
				console.log(t);
			}		
			,error:function(t){
				console.log(t);
			}
		});
	});
	
	$("#test9").on("click",function(){
		//삭제
		var corpType = $("input[name=corpType]:checked").val();
		var url = "";
		var vehicleKey = $("#fence_vehicleKey").val();
		var fenceKey = $("#fence_fenceKey").val();
		
		if(corpType == "1") url = "/api/v4/geofence/"+fenceKey;
		if(corpType == "0") url = "/api/v4/vehicle/"+vehicleKey+"/geofence/"+fenceKey;
		
		if(url.length == 0) {
			alert("select corpType");
			return false;
		}else{
			if(corpType == "0" && vehicleKey.length == 0){
				alert("required vehicleKey");
				return false;
			}else if(fenceKey.length == 0){
				alert("required fenceKey");
				return false;
			}
		}
		
		var sendData = {
				  "lat": $("#fence_lat").val()
				  ,"lon": $("#fence_lon").val()
				  ,"addr": $("#fence_addr").val()
				  ,"addrDetail": $("#fence_addrDetail").val()
				  ,"alies": $("#fence_alies").val()
				  ,"distance": $("#fence_distance").val()
				  ,"push": $("#fence_push").val()
				}
		
		$.ajax({
			url: "${defaultPath}"+url
			,dataType : 'json'
			,contentType: "application/json; charset=UTF-8"
			,type:"DELETE"
			,data: JSON.stringify(sendData)
			,cache:!1
			,headers : {"Cache-Control":"no-cache"}
			,beforeSend : function(xhr){
				xhr.setRequestHeader("key", $('#token').val()); 
			}
			,success:function(t){
				console.log(t);
			}		
			,error:function(t){
				console.log(t);
			}
		});
	});
	
	$("#test10").on("click",function(){
		//리스트조회
		var corpType = $("input[name=corpType]:checked").val();
		var url = "";
		var vehicleKey = $("#fence_vehicleKey").val();
		
		if(corpType == "1") url = "/api/v4/geofence";
		if(corpType == "0") url = "/api/v4/vehicle/"+vehicleKey+"/geofence";
		
		if(url.length == 0) {
			alert("select corpType");
			return false;
		}else{
			if(corpType == "0" && vehicleKey.length == 0){
				alert("required vehicleKey");
				return false;
			}
		}
		
		
		
		$.ajax({
			url: "${defaultPath}"+url
			,dataType : 'json'
			,contentType: "application/json; charset=UTF-8"
			,type:"GET"
			//,data: JSON.stringify(sendData)
			,cache:!1
			,headers : {"Cache-Control":"no-cache"}
			,beforeSend : function(xhr){
				xhr.setRequestHeader("key", $('#token').val()); 
			}
			,success:function(t){
				console.log(t);
			}		
			,error:function(t){
				console.log(t);
			}
		});
	});
	
	$("#test11").on("click",function(){
		//싱글조회
		var corpType = $("input[name=corpType]:checked").val();
		var url = "";
		var fenceKey = $("#fence_fenceKey").val();
		
		url = "/api/v4/geofence/"+fenceKey;
		
		if(url.length == 0) {
			alert("select corpType");
			return false;
		}else{
			if(fenceKey.length == 0){
				alert("required fenceKey");
				return false;
			}
		}
		
		
		$.ajax({
			url: "${defaultPath}"+url
			,dataType : 'json'
			,contentType: "application/json; charset=UTF-8"
			,type:"GET"
			//,data: JSON.stringify(sendData)
			,cache:!1
			,headers : {"Cache-Control":"no-cache"}
			,beforeSend : function(xhr){
				xhr.setRequestHeader("key", $('#token').val()); 
			}
			,success:function(t){
				console.log(t);
			}		
			,error:function(t){
				console.log(t);
			}
		});
	});
	

});

</script>
	</head>
    <body>
    	<h1>테스트 화면</h1>
    	로그인하기<br/>
    	아이디 : <input type="text" id="userId" value="test1@naver.com"></br>
    	비번 : <input type="text" id="userPw" value="test123!@#"></br>
    	만료시간 : <input type="text" id="expire" value="999999"></br>
    	<button type="button" id="login">로그인</button>
    	<br/>
    	<br/>
    	<br/>
    	
    	
    	
    	토큰 : <input type="text" id="token" value="" style="width:1000px;"></br>
    	<button type="button" id="test1">차량등록 테스트</button>
    	<button type="button" id="test2">차량등록Temp 테스트</button>
    	<button type="button" id="test3">get vehicle list</button>
    	<div>
    		<ul id="vehicleList">
    			
    		</ul>
    	</div>
    	
    	<br/>
    	<br/>
    	<br/>
    	fcmTest : <input type="text" id="fcmKey" value="" style="width:1000px;"></br>
    	<button type="button" id="test5">fcm key generate</button>
    	<button type="button" id="test6">fcm keyUpdate</button>
    	
    	<br/>
    	geoFenceTest : 
    	lat(double) : <input type="text" id="fence_lat" value="" style="width:100px;">
    	lon(double) : <input type="text" id="fence_lon" value="" style="width:100px;">
    	addr : <input type="text" id="fence_addr" value="" style="width:100px;">
    	addrDetail : <input type="text" id="fence_addrDetail" value="" style="width:100px;">
    	alies : <input type="text" id="fence_alies" value="" style="width:100px;">
    	distance(int,unit m) : <input type="text" id="fence_distance" value="" style="width:100px;">
    	push(1,0) : <input type="text" id="fence_push" value="" style="width:100px;">
    	vehicleKey : <input type="text" id="fence_vehicleKey" value="" style="width:100px;">
    	fenceKey : <input type="text" id="fence_fenceKey" value="" style="width:100px;">
    	<br/>
    	person : <input type="radio" name="corpType" value="0" checked>
    	corp : <input type="radio" name="corpType" value="1">
    	<button type="button" id="test7">등록</button>
    	<button type="button" id="test8">수정</button>
    	<button type="button" id="test9">삭제</button>
    	<button type="button" id="test10">조회List</button>
    	<button type="button" id="test11">조회Single</button>
    	<div>
    		<ul id="fenceList">
    			
    		</ul>
    	</div>
    	<br/>
    	<br/>
    	<div>
    	<textarea id="text4">
    	</textarea>
    	<button type="button" id="test4">lora packet test</button>
    	</div>
    </body>
</html>