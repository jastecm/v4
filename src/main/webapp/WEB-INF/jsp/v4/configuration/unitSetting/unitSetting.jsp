<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
});
</script>
 <!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>단위선택</h2>
                    <span>회원가입시 지정한 지역(국가)를 기준으로 단위를 설정합니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="setting-page">
                    <div class="local-select">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <!-- 국가선택 -->
                        <h3 class="tit2 mgt50">국가선택</h3>
                        <p class="sub-txt">시각정보는 사용자의 선택시간을 기준으로 OBD단말과 서버는 동기화됩니다.</p>
                        <table class="table mgt20">
                            <caption>국가선택</caption>
                            <colgroup>
                                <col style="width:14%" />
                                <col style="width:36%" />
                                <col style="width:14%" />
                                <col style="width:36%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="" colspan="4">
                                        언어 및 지역 선택 &nbsp;&nbsp;
                                        <select name="" style="width:300px">
											<option value="">대한민국 (Republic of Korea) </option>
										</select>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">언어</th>
                                    <td>한국어</td>
                                    <th scope="row">온도</th>
                                    <td>섭시(°C )</td>
                                </tr>
                                <tr>
                                    <th scope="row">일자표시</th>
                                    <td>
                                        <select name="">
											<option value="">2018년 5월 1일</option>
										</select>
                                    </td>
                                    <th scope="row">길이</th>
                                    <td>미터(M)</td>
                                </tr>
                                <tr>
                                    <th scope="row">시간설정</th>
                                    <td>
                                        <div class="division">
                                            <div class="col-6">
                                                <select name="">
													<option value="">한국표준시(UTC+9)</option>
												</select>
                                            </div>
                                            <div class="space">&nbsp;</div>
                                            <div class="col-6">
                                                *24시간제 표시
                                                <input type="checkbox" />
                                            </div>
                                        </div>
                                    </td>
                                    <th scope="row">무게</th>
                                    <td>Kg</td>
                                </tr>
                                <tr>
                                    <th scope="row">통화</th>
                                    <td>
                                        원(₩) &nbsp;&nbsp;&nbsp;* 숫자구분표시
                                        <input type="checkbox" />&nbsp;&nbsp;&nbsp;소수점
                                        <input type="checkbox" />
                                    </td>
                                    <th scope="row">&nbsp;</th>
                                    <td>&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                        <!--/ 국가선택 -->


                        <!-- 자동차 운행시간대 선택 -->
                        <h3 class="tit2 mgt50">자동차 운행시간대 선택</h3>
                        <p class="sub-txt">지도상의 시간대를 선택하면 상단 시간설정이 바뀝니다.</p>
                        <div class="img">
                            <img src="${pageContext.request.contextPath}/common/new/img/local-select-img01.gif" alt="" />
                        </div>
                        <!--/ 자동차 운행시간대 선택 -->

                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->