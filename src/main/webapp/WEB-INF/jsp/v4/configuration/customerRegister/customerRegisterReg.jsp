<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>거래처 등록</h2>
                    <span>등록한 거래처 1km이내 접근 시, 직원의 운행스케쥴에 기록됩니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="setting-page">
                    <div class="box-layout register-client">
                        <div class="title">
                            <h3 class="tit3">거래처 등록</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
                            <div class="info-txt">
                                등록한 거래처 1km이내 접근 시, 직원의 운행스케쥴에 기록됩니다.
                            </div>
                            <table class="table mgt20">
                                <caption>거래처 등록</caption>
                                <tbody>
                                    <tr>
                                        <th scope="row">거래처 명</th>
                                        <td class="left">
                                            <input type="text" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">상세주소</th>
                                        <td class="left">
                                            <div class="division">
                                                <div class="col-10">
                                                    <input type="text" />
                                                </div>
                                                <div class="space">&nbsp;</div>
                                                <div class="col-2">
                                                    <button class="btn btn03 md">상세주소</button>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <!-- Daum 지도 - 지도퍼가기 -->
                            <div id="daumRoughmapContainer1533601510591" class="root_daum_roughmap root_daum_roughmap_landing mgt30"></div>
                            <script charset="UTF-8" class="daum_roughmap_loader_script" src="http://dmaps.daum.net/map_js_init/roughmapLoader.js"></script>
                            <script charset="UTF-8">
                                new daum.roughmap.Lander({
                                    "timestamp": "1533601510591",
                                    "key": "pdpw",
                                    "mapWidth": "1220",
                                    "mapHeight": "500"
                                }).render();
                            </script>
                            <!--/ Daum 지도 - 지도퍼가기 -->

                            <!-- 하단 버튼 -->
                            <div class="btn-bottom">
                                <button class="btn btn02">취소</button>
                                <button class="btn btn03">확인</button>
                            </div>
                            <!--/ 하단 버튼 -->
                        </div>
                        <!--/ 콘텐츠 본문 -->
                    </div>
                </div>
            </div>
        </section>
        <!--/ 본문 -->