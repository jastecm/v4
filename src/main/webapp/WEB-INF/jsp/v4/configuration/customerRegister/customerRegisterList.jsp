<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>거래처 등록</h2>
                    <span>등록한 거래처 1km이내 접근 시, 직원의 운행스케쥴에 기록됩니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="setting-page">
                    <div class="store-list">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                            <div class="item">
                                <span>
									등록 거래처 수
									<strong>10</strong>건
								</span>
                                <span>
									최대 등록가능 거래처 수
									<strong>8</strong>건
								</span>
                            </div>

                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <!-- 거래처등록 -->
                        <h3 class="tit2 mgt30">거래처 등록</h3>
                        <div class="box-register">
                            5개의 거래처를 무료로 등록하실 수 있습니다.<br />
							거래처 추가를 원하시면, 서비스 스토어 에서 부가서비스 신청해주세요.
							&nbsp;&nbsp;&nbsp;
							<a href="" class="link01">서비스스토어 가기 &gt;</a><br />
							프리미엄 등급 고객은 제한 없이  이용하실 수 있습니다. 

                            <div class="right">
                                <button class="btn btn01">거래처 등록</button>
                            </div>
                        </div>
                        <!--/ 거래처등록 -->

                        <!-- 거래처 목록 -->
                        <div class="section01 ">
                            <h3 class="tit2">거래처 목록</h3>
                            <!-- table 버튼 -->
                            <div class="btn-function title-side">
                                <div class="right">
                                    <select name="">
										<option value="">5건씩 보기</option>
									</select>
                                </div>
                            </div>
                            <!--/ table 버튼 -->

                            <table class="table list mgt20">
                                <caption>거래처 목록</caption>
                                <colgroup>
                                    <col style="width:8%" />
                                    <col style="width:20%" />
                                    <col />
                                    <col style="width:10%" />
                                    <col style="width:15%" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th scope="col"><input type="checkbox" /></th>
                                        <th scope="col">거래처명</th>
                                        <th scope="col">주소</th>
                                        <th scope="col">상세위치</th>
                                        <th scope="col">실시간 진입 이탈 알림</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>남부본부(대전)</td>
                                        <td class="left">대전광역시 유성구 신성로 61번길 93-10</td>
                                        <td><button class="btn-img location">상세위치</button></td>
                                        <td>
                                            <div class="btn-onoff">
                                                <button class="active">ON</button>
                                                <button>OFF</button>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <!-- 페이징 -->
                            <div id="paging">
                                <a href="" class="first icon-spr">제일 처음으로</a>
                                <a href="" class="prev icon-spr">이전으로</a>
                                <span class="current">1</span>
                                <a href="" class="num">2</a>
                                <a href="" class="num">3</a>
                                <a href="" class="num">4</a>
                                <a href="" class="num">5</a>
                                <a href="" class="next icon-spr">다음으로</a>
                                <a href="" class="last icon-spr">제일 마지막으로</a>
                            </div>
                            <!--/ 페이징 -->
                        </div>
                        <!--/ 거래처 목록 -->
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->