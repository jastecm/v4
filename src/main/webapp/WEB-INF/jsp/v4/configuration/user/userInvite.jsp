<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	var failCount = 0;
	$('#accountInviteBtn').on('click',function(){
		$('#mailSendStatus').empty();
		var accountId = $('#accountId').val();
		
		if(accountId != "" && failCount == 0){
			
			var splitAccountId = accountId.split(',');
			for(var i = 0 ; i < splitAccountId.length;i++){
				var id = splitAccountId[i];
				
				$('#mailSendStatus').append('<li>'+id+'<span></span></li>');

				var sendData = {
						"accountId" : id
				}
				
				$V4.http_post("/api/1/invite",sendData,{
					sync : true,
		        	success : function(rtv){
		        		$('#mailSendStatus li:last').children('span').text('*성공')
		        	},
		        	error : function(e){
		        		$('#mailSendStatus li:last').children('span').attr('style','color:red').text('*실패, 이미 가입한 아이디입니다. 다른 메일로 등록해주세요')
		        	}
		        });
				
			}
		}
		
	});
	
	$('#accountId').on('keyup',function(){
		var accountId = $('#accountId').val();
		if(accountId == ""){
			$('#status').empty();
			return;
		}
		var splitAccountId = accountId.split(',');
		var regExp = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}$/;
		failCount = 0;
		for(var i = 0 ; i < splitAccountId.length;i++){
			var id = splitAccountId[i];
			if(!regExp.test(id)){
				failCount++;
			}
		}
		if(failCount > 0){
			$('#status').html('<span class="fail">입력값이 정확하지 않습니다. </span>');	
		}else{
			$('#status').html('<span class="success">정상적으로 입력하셨습니다.</span>');	
		}
	});
});
</script>
 <!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>사용자 등록</h2>
                    <span>소속직원의 배차신청ㆍ운행기록부 관리, 위험운전 관리등을 위해 사용자 등록이 필요합니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="setting-page">
					<div class="box-layout invite-user">
						<div class="title">
                            <h3 class="tit3">사용자 초대</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
							<div class="info-txt">
								많은 직원분들을 일일이 등록하시기 힘드시죠!
								<br /> 메일로 간편하게 사용자 등록을 초대하실 수 있습니다.
							</div>

							<!-- 메일주소입력 -->
							<h3 class="tit2 mgt50">메일주소입력</h3>
							<div class="mail-insert">
								<input id="accountId" type="text" placeholder="쉼표(,)로 메일를 구분해주세요. 예제)aaa@bbb.ccc, ddd@eee.ggg," />
							</div>
							<div class="result" id="status">
								<!-- <span class="success">정상적으로 입력하셨습니다.</span>
								<span class="fail">입력값이 정확하지 않습니다. </span> -->
							</div>
							<!--/ 메일주소입력 -->

							<!-- 메일주소입력확인 -->
							<h3 class="tit2 mgt50">메일주소 입력확인</h3>
							<div class="mail-check">
								<!-- 1단계 -->
								<ul id="mailSendStatus">
									<!-- <li>
										dartz@hanmail.net
										<span>*성공</span>
									</li>
									<li>
										dartz@hanmail.net
										<span>*성공</span>
									</li>
									<li>
										dartz@hanmail.net
										<span style="color:red">*실패, 이미 가입한 아이디입니다. 다른 메일로 등록해주세요</span>
									</li> -->
								</ul>
								<!--/ 1단계 -->
							</div>
							<!--/ 메일주소입력확인 -->

							<!-- 하단 버튼 -->
							<div class="btn-bottom">
								<button class="btn btn03" id="accountInviteBtn">사용자 초대</button>
								<button class="btn btn02">취소</button>
							</div>
							<!--/ 하단 버튼 -->
						</div>
					</div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->