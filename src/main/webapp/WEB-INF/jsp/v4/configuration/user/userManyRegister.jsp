<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	$('#confirm').on('click',function(){
		//["name","accountId","accountPw","mobilePhone","mobilePhoneCtyCode","gender"]
		generatorExcel($('#file'),[], function(data) {
			console.log(data)
			
			return false;
			var arr = [];
			var obj = {
				"name" : "테스트",
				"accountId" : "asdf1@naver.com",
				"accountPw" : "shdms!@QW34",
				"corpKey" : "449f2327-8e35-11e8-ab1f-42010a8c0018",
				"mobilePhone" : "01066281024",
				"mobilePhoneCtyCode" : "+82",
				"gender" : "M"
			}
			var obj2 = {
				"name" : "테스트2",
				"accountId" : "test1@naver.com",
				"accountPw" : "shdms!@QW34",
				"corpKey" : "449f2327-8e35-11e8-ab1f-42010a8c0018",
				"mobilePhone" : "01012341234",
				"mobilePhoneCtyCode" : "+82",
				"gender" : "F"
			}
			
			arr.push(obj);
			arr.push(obj2);
			
			$V4.http_post("/api/1/account/array",arr,{
		    	success : function(rtv){
		    		console.log("test");
		    	},
				error : function(e){
					console.log("error");
				}
		    });
			
		});
	});
});

</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>사용자 등록</h2>
                    <span>소속직원의 배차신청ㆍ운행기록부 관리, 위험운전 관리등을 위해 사용자 등록이 필요합니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="setting-page">
					<div class="box-layout register-mass">
					<div class="title">
                            <h3 class="tit3">사용자 대량 등록</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
							<h3 class="tit2">사용자 대량등록</h3>
							<table class="table mgt20">
								<caption>사용자 대량 등록</caption>
								<tbody>
									<tr>
										<th scope="row">사용자등록 양식</th>
										<td>
											<a href="" class="btn-download">사용자 등록양식 다운로드</a>
											<div class="mgt10">사용자등록 양식을 다운로드 받아 해당양식에 맞게 아래에서 업로드하시면 즉시 등록됩니다.</div>
										</td>
									</tr>
									<tr>
										<th scope="row">사용자등록 업로드</th>
										<td>
											<input type="file" id="file" />
										</td>
									</tr>
								</tbody>
							</table>
							<!-- 하단 버튼 -->
							<div class="btn-bottom">
								<button type="button" class="btn btn03" id="confirm">확인</button>
							</div>
							<!--/ 하단 버튼 -->
						</div>
					</div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->
        
        