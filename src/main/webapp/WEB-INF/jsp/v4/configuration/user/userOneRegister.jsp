<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script type="text/javascript">
$(function(){
	
	//로그인한 사용자의 소속을 가져 와야 한다
	
	//corp만 있다
	
	$('#userRegBtn').on('click',function(){
		var name = $('#name').val();
		var mobilePhone = $('#mobilePhone').val();
		
		if( $V4.required(name , "이름은 필수입니다." , $('#name')) ) return false;
		
		var idHead = $('#idHead').val();
		var idTail = $('#idTail').val();
		
		if( $V4.required(idHead , "대표자 이메일은 필수입니다." , $('#idHead')) ) return false;
		if( $V4.required(idTail , "대표자 이메일은 필수입니다." , $('#idTail')) ) return false;
		
		var userId = idHead + "@" + idTail;
		
		regExp = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}$/;
		if(!regExp.test(userId) && (alert("아이디 이메일 형식이 잘못되었습니다.")||1) && $('#idHead').focus()) return false;
		
		var pw = $('#pw').val();
		var re = $('#re').val();
		
		if( $V4.required(pw , "비밀번호는 필수입니다." , $('#pw')) ) return false;
		if( $V4.required(re , "비밀번호 확인은 필수입니다." , $('#re')) ) return false;
		
		
		if(!$('#pwResult').hasClass('safe')){
			alert("대표자 비밀번호 형식이 잘못되었습니다.");
			$('#ceoPw').focus()
			return false;
		}
		
		//같지 않다
		if(pw != re){ alert("비밀번호와 비밀번호 확인이 같지 않습니다."); return false; }
		
		
		if( $V4.required(mobilePhone , "휴대폰 번호는 필수입니다." , $('#mobilePhone')) ) return false;
		
		regExp = /^01[0-9]{8,9}$/;
		if(!regExp.test(mobilePhone) && (alert("휴대폰 번호 형식이 잘못되었습니다.")||1) && $('#mobilePhone').focus()) return false;
		
		var mobilePhoneCtyCode = $('#mobilePhoneCtyCode').val();
		
		var phoneCtyCode = $('#phoneCtyCode').val();
		var phone = $('#phone').val();
		
		regExp = /^0[0-9]{8,10}$|^$/;
		if(!regExp.test(phone) && (alert("전화번호 형식이 잘못되었습니다.")||1) && $('#phone').focus()) return false;
		
		var birth = $('#birth').val();
		var blood = $('#blood').val();
		var bloodRh = $('#bloodRh').val();
		var gender = $('input[name="sex"]:checked').val();
		
		regExp = /^[1-2]{1}[0-9]{3}[0-1]{1}[0-9]{1}[0-3]{1}[0-9]{1}$|^$/;
		
		//birth chk
		if(!regExp.test(birth) && (alert("생년월일 형식이 잘못되엇습니다.")||1)) return false;
		birth = birth?calcDate(new String(birth),"",0,new Date).getTime():null;
		
		var sendData = {
			"name" : name,
			"accountId" : userId,
			"accountPw" : pw,
			"corpKey" : "449f2327-8e35-11e8-ab1f-42010a8c0018",
			"mobilePhone" : mobilePhone,
			"mobilePhoneCtyCode" : mobilePhoneCtyCode,
			"phone" : phone,
			"phoneCtyCode" : phoneCtyCode,
			"birth" : birth,
			"blood"	: blood,
			"bloodRh" : bloodRh,
			"gender" : gender
		}
		
		$V4.http_post("/api/1/account/one",sendData,{
			header : {key : "${_KEY}"}
	    	,success : function(rtv){
	    		alert("저장되었습니다.");
	    		$V4.move("/config/userRegister");
	    	}
	    });
		
	});
	
	
	$("#brith-date").datepicker({
        dateformat: 'yy-mm-dd'
    });

	// 이미지 등록
    $("#register-photo-dialog").dialog({
        autoOpen: false,
        show: {
            duration: 500
        },
        width: '800',
        modal: true
    });

    $("#register-photo-open").on("click", function() {
    	
    	var $el = $('#img');
   	    $el.wrap('<form>').closest('form').get(0).reset();
   	    $el.unwrap();
    	
   	 	$('#imgPreview').empty();
        $("#register-photo-dialog").dialog("open");
    });
    
    $('#img').on('change',function(){
    	var formData = new FormData();
    	formData.append("uploadType","uploadUserProfil");
    	
    	
    	for(var i = 0 ; i < $("#img")[0].files.length;i++){
    		formData.append("img"+i, $("#img")[0].files[i]);	
    	}

		$V4.fileUpload(formData,{
			header : {"key" : "${_KEY}"},
			success : function(data){
				var html = '<img style="width:115px;height:116px;" src="${defaultPath}/api/1/'+data.result.img0+'/${_KEY}/imgDown" alt="" />'
				$('#imgPreview').html(html);
			},error : function(t){
				alert('error');
			}
		});
 
    });
    $('#imgUploadBtn').on('click',function(){
		//이미지가 업로드된 상태에서 확인을 눌러야 들어간다?
    	var clone = $('#imgPreview img').clone(true);
		$('#viewImg').html(clone);
		$( "#register-photo-dialog" ).dialog( "close" );
	});
    
    $('#imgUploadCancleBtn').on('click',function(){
    	$( "#register-photo-dialog" ).dialog( "close" );
    });
    
    $("#pw").on("keyup",function(e){
		var v = $(this).val();
		var chk = 0;	
		
		if(v){
			var regExpAlphabet = new RegExp("(?=.*[a-zA-Z])");
			var regExpNumber = new RegExp("(?=.*[0-9])");
			var regExpSpecialChar = new RegExp("(?=.*[!@#$%^\\&*\\(\\)\\_\\+\\|\\-\\=\\\\\\[\\];'?\\.\\,])");
			regExpAlphabet.test(v)&&chk++;
			regExpNumber.test(v)&&chk++;
			regExpSpecialChar.test(v)&&chk++;
			$("#re").val()==v?$("#reResult").removeClass("warning").addClass("safe").text("비밀번호가 일치합니다."):$("#reResult").removeClass("safe").addClass("warning").text("비밀번호가 일치하지 않습니다.");
		}
		var regExpLength = new RegExp("^.{8,20}$");
		if(chk == 3 && regExpLength.test(v)){
			$("#pwResult").removeClass("warning").addClass("safe").css('color','#00CC00').text("사용가능");
			
		}else{
			if(chk==0) chk = 1;
			$("#pwResult").removeClass("safe").addClass("warning").css('color','#e60012').text("사용불가");
			
		}
		
		$(".password-check").find("div").removeClass("step01").removeClass("step02").removeClass("step03").addClass("step0"+chk);
		
		
		
	});
	
	$("#re").on("keyup",function(e){
		var v = $(this).val();
		if(v.length == 0){
			$('#reResult').hide();
			return;
		}
		$('#reResult').show();
		
		v&&$("#pw").val()==v?$("#reResult").removeClass("warning").addClass("safe").css('color','#00CC00').text("비밀번호가 일치합니다."):$("#reResult").removeClass("safe").addClass("warning").css('color','#e60012').text("비밀번호가 일치하지 않습니다.");
	});
	
	$('#tailChange').on('change',function(){
		$('#idTail').val($(this).val());
	});
    
    
});
</script>
 <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>사용자 등록</h2>
                    <span>소속직원의 배차신청ㆍ운행기록부 관리, 위험운전 관리등을 위해 사용자 등록이 필요합니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="setting-page join-page" style="margin-top: 20px;">
                    <div class="box-layout register-individual">
                        <div class="title">
                            <h3 class="tit3">사용자 등록</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
                            <div class="info-txt">
                                소속직원의 정보를정확히 입력해주세요.
                                <br />
                                <strong>
									<span class="red">*직급 등록시 부서장</span>은 결재등록이 필요하니
									<span class="red">반드시 체크해주세요.</span>
								</strong>
                            </div>

                            <form class="register-form">
                                    <!-- 사진 -->
                                    <div class="left">
                                        <div class="img" id="viewImg">
                                            <img src="${pageContext.request.contextPath}/common/new/img/common/user-img2.png" alt="" />
                                        </div>
                                        <a id="register-photo-open" href="javascript:;">사진등록</a>
                                    </div>
                                    <!--/ 사진 -->

                                    <!-- 정보입력 -->
                                    <div class="right">
                                        <table class="table">
                                            <caption>개인정보 입력</caption>
                                            <tbody>
                                            	<tr>
                                                    <th scope="row"><b style="color:#ed1c24">*</b> 성명</th>
                                                    <td>
                                                        <input type="text" id="name" maxlength="10" />
                                                    </td>
                                                    <th scope="row">소속</th>
                                                    <td>카카오VX</td>
                                                </tr>
                                            	<!-- <tr>
                                                    <th scope="row"><b style="color:#ed1c24">*</b> 아이디</th>
                                                    <td colspan="3">
                                                        <input type="text" id="idHead" name="" /> @
                                                        <input type="text" id="idTail" name="" />
                                                        <select name="" onchange="" id="tailChange">
															<option value="">이메일 선택</option>
															<option value="hanmail.net">다음</option>
															<option value="empal.com">엠파스</option>
															<option value="netian.com">네띠앙</option>
															<option value="naver.com">네이버</option>
															<option value="nate.com">네이트</option>
															<option value="dreamwiz.com">드림위즈</option>
															<option value="lycos.co.kr">라이코스</option>
															<option value="yahoo.co.kr">야후</option>
															<option value="chol.com">천리안</option>
															<option value="korea.com">코리아닷컴</option>
															<option value="paran.com">파란</option>
															<option value="freechal.com">프리챌</option>
															<option value="hitel.net">하이텔</option>
															<option value="hanmir.com">한미르</option>
															<option value="hotmail.com">핫메일</option>
															<option value="hanmir.com">한미르</option>
															<option value="">직접쓰기</option>
														</select>
                                                    </td>
                                                </tr> -->
                                                <tr>
	                                                <th scope="row">아이디</th>
	                                                <td colspan="3">
	                                                    <div class="division">
	                                                        <div class="col-5">
	                                                            <input type="text" id="idHead" />
	                                                        </div>
	                                                        <div class="space">&nbsp;@&nbsp;</div>
	                                                        <div class="col-4">
	                                                            <input type="text" id="idTail" />
	                                                        </div>
	                                                        <div class="space">&nbsp;</div>
	                                                        <div class="col-3">
	                                                            <select name="" onchange="" id="tailChange">
																	<option value="">이메일 선택</option>
																	<option value="hanmail.net">다음</option>
																	<option value="empal.com">엠파스</option>
																	<option value="netian.com">네띠앙</option>
																	<option value="naver.com">네이버</option>
																	<option value="nate.com">네이트</option>
																	<option value="dreamwiz.com">드림위즈</option>
																	<option value="lycos.co.kr">라이코스</option>
																	<option value="yahoo.co.kr">야후</option>
																	<option value="chol.com">천리안</option>
																	<option value="korea.com">코리아닷컴</option>
																	<option value="paran.com">파란</option>
																	<option value="freechal.com">프리챌</option>
																	<option value="hitel.net">하이텔</option>
																	<option value="hanmir.com">한미르</option>
																	<option value="hotmail.com">핫메일</option>
																	<option value="hanmir.com">한미르</option>
																	<option value="">직접쓰기</option>
																</select>
	                                                        </div>
	                                                    </div>
	                                                </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row "><b style="color:#ed1c24">*</b> 비밀번호</th>
                                                    <td>
                                                        <input type="password" name="pw" id="pw" />
                                                        <div class="mgt5">8~20자리 대소문자, 숫자, 특수문자 조합</div>
                                                        <div class="password-check" style="margin-left: 0px;">
                                            				<div class="bar step01"><span>1</span></div>
                                            				<p>비밀번호 안전도 :<span class="warning" id="pwResult">사용불가</span>
                                            				</p>
                                        				</div>
                                                    </td>
                                                    <th scope="row "><b style="color:#ed1c24">*</b> 비밀번호 확인</th>
                                                    <td>
                                                        <input type="password" name="re" id="re" />
                                                        <br/>
                                                        <span class="warning" id="reResult" style="margin-left: 0px;display:none;">비밀번호가 일치하지 않습니다.</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                	<th scope="row"><b style="color:#ed1c24">*</b> 휴대폰 번호</th>
                                                    <td>
                                                        <select id="mobilePhoneCtyCode">
															<option value="+82">+82</option>
														</select>
                                                        <input type="text" id="mobilePhone" class="numberOnly" maxlength="11" class="tel" />
                                                    </td>
                                                    <th scope="row">전화번호</th>
                                                    <td>
                                                        <select id="phoneCtyCode">
															<option value="+82">+82</option>
														</select>
                                                        <input type="text" id="phone" class="numberOnly" maxlength="11" class="tel" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">성별</th>
                                                    <td>
				                                        <input type="radio" id="sex01" name="sex" checked="checked" value="M" />
				                                        <label for="sex01">남자</label> &nbsp;&nbsp;&nbsp;&nbsp;
				                                        <input type="radio" id="sex02" name="sex" value="F" />
				                                        <label for="sex02">여자</label>
				                                    </td>
                                                    <th scope="row">혈액형</th>
				                                    <td>
				                                        <select id="blood" style="width:220px">
															<option value="A">A형</option>
															<option value="B">B형</option>
															<option value="O">O형</option>
															<option value="AB">AB형</option>
														</select>
														</br>
														<select id="bloodRh" style="width:220px">
															<option value="rh+">rh+</option>
															<option value="rh-">rh-</option>
														</select>
				                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">생년월일</th>
                                                    <td>
                                                        <input type="text" id="birth" class="numberOnly" placeholder="ex)20100710"/>
                                                    </td>
                                                    <th scope="row">직급</th>
                                                    <td>
                                                        <select name="" style="width:220px">
															<option value="">선택</option>
															<option value="">대표이사</option>
															<option value="">전무이사</option>
															<option value="">상무이사</option>
															<option value="">이사</option>
															<option value="">부장</option>
															<option value="">차장</option>
															<option value="">과장</option>
															<option value="">대리</option>
															<option value="">사원</option>
														</select>
                                                    </td>
                                                </tr>
                                                 <tr>
                                                    <th scope="row">부서명</th>
                                                    <td>
                                                        <select name="" style="width:220px">
															<option value="">선택</option>
														</select>
                                                    </td>
                                                    <th scope="row" class="help" >부서장 유무
                                                    	<div class="layer">
                                    					<b>부서장 유무</b> 는 부서의 짱이다.
                                    					</div>
                                                    </th>
                                                    <td>
                                                        <input type="checkbox" />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                <!--/ 정보입력 -->

                                <!-- 하단 버튼 -->
                                <div class="btn-bottom">
                                    <button type="button" class="btn btn02">취소</button>
                                    <button type="button" class="btn btn03" id="userRegBtn">등록</button>
                                </div>
                                <!--/ 하단 버튼 -->
                            </form>
                        </div>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>        
        <!-- 개별등록 사진업로드 팝업 -->
    <div id="register-photo-dialog" title="사진업로드">
        <h3 class="tit">사진업로드</h3>
        <table class="table mgt20">
            <caption>사진업로드</caption>
            <tbody>
                <tr>
                    <th scope="row">파일업로드</th>
                    <td>
                        <input type="file" id="img"/>
                        <div class="mgt5">가로세로가 동일한(정사각형) 사진을 등록하셔야 리스트에서 사진이 깨지지 않습니다.</div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">미리보기</th>
                    <td id="imgPreview">

                    </td>
                </tr>
            </tbody>
        </table>
        <!-- 하단 버튼 -->
        <div class="btn-bottom">
            <button class="btn btn02" id="imgUploadCancleBtn">취소</button>
            <button class="btn btn03" id="imgUploadBtn">등록</button>
        </div>
        <!--/ 하단 버튼 -->
    </div>
    <!--/ 개별등록 사진업로드 팝업 -->