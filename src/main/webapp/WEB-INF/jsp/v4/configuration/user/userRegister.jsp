<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	
	var cntObj = {user : {totalCnt : 0 , drivingCnt : 0 , parkingCnt :0 , unusedCnt : 0}}
	
	var countingParam = {offset:0,limit:1,counting:true};
	
	$V4.http_post("/api/v1/account",countingParam,{
		requestMethod:"GET"
		,header:{key:"${_KEY}"}
		,success : function(data){
			$.extend(cntObj.user,data);
			excCounter();
		}
	});
	
	var excCounter = function(){
		$("#allAccountCount").html(cntObj.user.totalCnt);
		//$("#userDriving").html(cntObj.user.drivingCnt);
		//$("#userParking").html(cntObj.user.parkingCnt);
		//$("#userUnused").html(cntObj.user.unusedCnt);
	};
	
	
	var setting = {
			thead : [ {'<input type="checkbox" />' : "5%"}
			, {"사용자 정보" : "25%"}
			, {"권한/부서/직급" : "25%"}
			, {"연락처" : "25%"}
			, {"비고" : "10%"}
			, {"수정" : "10%"}]
			,bodyHtml : (function(data){
				var strHtml = "";
				for(i in data.result){
					var obj = data.result[i];
					
					strHtml += '<tr>'
					strHtml += '<td><input class="accountChk" type="checkbox" data-accountkey='+obj.accountKey+' /></td>';
					strHtml += '<td><img src="${pageContext.request.contextPath}/common/new/img/common/user-default.png" class="user-img" alt="" /> '+obj.accountId+' / '+obj.name+'</td>';
					if(obj.hasOwnProperty('group')){
							var groupNm;
							if(obj.group.hasOwnProperty('groupNm')){
								groupNm = obj.group.groupNm
							}else{
								groupNm = obj.group.parentGroupNm;
							}
							if(obj.hasOwnProperty('corpPosition')){
								strHtml += '<td>권한? <br />'+groupNm+' / '+obj.corpPosition+'</td></td>';	
							}else{
								strHtml += '<td>권한? <br />'+groupNm+'</td></td>';
							}
							
					}else{
						if(obj.hasOwnProperty('corpPosition')){
							strHtml += '<td>권한? <br />'+obj.corpPosition+'</td></td>';	
						}else{
							strHtml += '<td>권한? <br /></td></td>';
						}
						
					}

					strHtml += '<td>'+concatString(obj.mobilePhoneCtyCode,obj.mobilePhone)+' <br /> '+concatString(obj.phoneCtyCode,obj.phone)+'</td>';
					
					if(obj.hasOwnProperty('descript')){
						strHtml += '<td>'+obj.descript+'</td>';	
					}else{
						strHtml += '<td></td>';
					}
					
					strHtml += '<td><button type="button" class="btn-img modify" data-accountkey='+obj.accountKey+'>수정</button></td>';
					strHtml += '</tr>';
				}
				return strHtml;
			})
			,limit : parseInt($(".func_limitChange").val())
    		,pagePerGroup : 10 //pagingGroup size
			,url : "/api/1/account"
			,param : {}
			,http_post_option : {
				requestMethod : "GET"
				,header : {key : "${_KEY}"}
			}
			//login token - test2@naver.com / test123!@# (이아이디가 차량 많음)
			,initSearch : true //생성과 동시에 search
			,loadingBodyViewer : true //로딩중!! 표시됨
		}
	
	//사용자 리스트
	$('#userList').commonList(setting);
	
	//전체 사용자 가져 오는 요청 만들어야됨
	
	
	

	//검색 조회 클릭시 엔터시
	function search(){
		//검색타입 전체,부서,이름,아이디
		var searchType = $('#searchType').val();
		//검색명
		var searchText = $('#searchText').val()
		
		var param = {
			"searchType" : searchType,
			"searchText" : searchText
		};
		
		$("#userList").commonList("search",param);
	}
	
	//사용자 검색	
	$('#userSearch').on('click',function(){
		search();
	});
	
	$('#searchText').on('keyup',function(e){
		if(e.keyCode == 13) search();
	});
	
	//사용자 삭제
	$('#userDelete').on('click',function(){
		var accountKeyList = $('.accountChk:checked');
		
		if(accountKeyList.length == 0){
			alert('삭제할 사용자를 선택해주세요');
			return;
		}
		var list = [];
		
		for(var i = 0 ; i < accountKeyList.length ; i++){
			var key = $(accountKeyList[i]).data('accountkey')
			list.push(key);
		}
		
		$V4.http_post("/api/{ver}/account/"+list.join(','),null,{
			requestMethod : "DELETE"
			,header : {"key" :"${_KEY}" }
			,success : function(rtv){
				
				alert('삭제되었습니다.');
				search();
				
			},error : function(t){
				
			}
		});
		
	});
	
	$('#userInviteBtn').on('click',function(){
		$V4.move("/config/userInvite");
	});
	
	$('#userOneRegisterBtn').on('click',function(){
		$V4.move("/config/userOneRegister");
	});
	
	$('#userManyRegisterBtn').on('click',function(){
		$V4.move("/config/userManyRegister");
	});
	
	$('#excelDown').on('click',function(){
		
		var arr = [];
		var obj = {
			"name" : "테스트",
			"accountId" : "asdf1@naver.com",
			"accountPw" : "shdms!@QW34",
			"corpKey" : "449f2327-8e35-11e8-ab1f-42010a8c0018",
			"mobilePhone" : "01066281024",
			"mobilePhoneCtyCode" : "+82",
			"gender" : "M"
		}
		var obj2 = {
			"name" : "테스트2",
			"accountId" : "test1@naver.com",
			"accountPw" : "shdms!@QW34",
			"corpKey" : "449f2327-8e35-11e8-ab1f-42010a8c0018",
			"mobilePhone" : "01012341234",
			"mobilePhoneCtyCode" : "+82",
			"gender" : "F"
		}
		
		arr.push(obj);
		arr.push(obj2);
		
		var header = {
			"name" : "이름",
			"gender" : "성별",
			"accountId" : "아이디",
			"accountPw" : "비밀번호",
			/* "corpKey" : "코프키", */
			"mobilePhoneCtyCode" : "모바일폰",
			"mobilePhone" : "모바일폰"
		}
				
		writeExcelFile(arr, "테스트파일.xlsx", 'test1' , header);
	});
	
	$(".func_limitChange").on("change",function(){
		var limit = parseInt($(".func_limitChange").val());
		$('#userList').commonList("setLimit",limit).search();
	});

	$(".func_order").on("change",function(){
		var param = {
			"searchOrder" : $(".func_order").val()
		}
		var limit = parseInt($(".func_limitChange").val());
		
		$('#userList').commonList("setParam",param);
		$('#userList').commonList("setLimit",limit).search();
	});
	
});

</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>사용자 등록</h2>
                    <span>소속직원의 배차신청ㆍ운행기록부 관리, 위험운전 관리등을 위해 사용자 등록이 필요합니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="setting-page">
                    <div class="user-list">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                            <div class="item">
                                <span>
									전체 등록 사용자
									<strong id="allAccountCount">0</strong>명
								</span>
                            </div>

                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <!-- 사용자등록 -->
                        <h3 class="tit mgt50">사용자등록</h3>
                        <div class="box-register">
                            소속직원의 정보를 상세하게 입력해주세요.
                            <br /> 소속직원이 많이 일일이 등록이 번거로우신 경우 대량등록을 이용하시면 편리합니다.
                            <div class="right">
                                <button type="button" class="btn btn01" id="userInviteBtn">사용자 초대</button>
                                <button type="button" class="btn btn02" id="userOneRegisterBtn">개별등록</button>
                                <button type="button" class="btn btn02" id="userManyRegisterBtn">대량등록</button>
                            </div>
                        </div>
                        <!--/ 사용자등록 -->

                        <!-- 상단 검색 -->
                        <h3 class="tit2 mgt50">사용자 조회</h3>
                        <div class="top-search mgt20">
                            <select id="searchType">
								<option value="">전체</option>
								<option value="group">부서</option>
								<option value="name">이름</option>
								<option value="id">아이디</option>
							</select>
                            <input type="text" id="searchText" />
                            <button type="button" class="btn btn03" id="userSearch">조회</button>
                        </div>
                        <!--/ 상단 검색 -->

                        <!-- table 버튼 -->
                        <div class="btn-function">
                            <div class="left">
                                <select class="func_order">
                                    <option value="lastestLocationTime">최근 운행순</option>
									<option value="regDate">최근 등록순</option>
								</select>
                            </div>

                            <div class="right">
                                <button type="button" class="excel" id="excelDown">엑셀 다운로드</button>
                                <!-- 사용중지가 왜있는건가 -->
                                <!-- <button class="stop">사용중지</button> -->
                                <button type="button" class="delete" id="userDelete">삭제</button>
                               <select class="func_limitChange">
												<option value="5" selected>5건씩 보기</option>
												<option value="10">10건씩 보기</option>
												<option value="20">20건씩 보기</option>
												<option value="50">20건씩 보기</option>
											</select>
                            </div>
                        </div>
                        <!--/ table 버튼 -->

                        <table class="table list mgt20" id="userList">
                            <caption>사용자 목록</caption>
							<%-- <colgroup>
								<col style="width:5%" />
								<col style="width:25%" />
								<col style="width:25%" />
								<col style="width:25%" />
								<col style="width:10%" />
								<col style="width:10%" />
							</colgroup> --%>

                            <thead>
                            <!--     <tr>
                                    <th scope="col">
                                        <input type="checkbox" />
                                    </th>
                                    <th scope="col">사용자 정보</th>
                                    <th scope="col">권한/부서/직급</th>
                                    <th scope="col">연락처</th>
                                    <th scope="col">비고</th>
                                    <th scope="col">수정</th>
                                </tr> -->
                            </thead>
							
                            <tbody>
                                <!-- <tr>
                                    <td>
                                        <input type="checkbox" />
                                    </td>
                                    <td>
                                        <img src="./img/common/user-default.png" class="user-img" alt="" /> maumgolf / 카카오VX
                                    </td>
                                    <td>
                                        관리자
                                        <br />인사총무팀/팀장</td>
                                    <td>
                                        010-6648-2585
                                        <br /> 010-6648-2585
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <button class="btn-img modify">수정</button>
                                    </td>
                                </tr> -->
                            </tbody>
                        </table>
                  
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->