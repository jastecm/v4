<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	
	//TODO: 사용자 등록 결재 부서장 건수 가져오는거 해야됨 차후
	$V4.http_post("/api/1/approvalConfig", null, {
			requestMethod : "GET",
			header : {
				"key" : "${_KEY}"
			},
			success : function(rtv) {
				var vo = rtv.result;
				console.log(rtv);
				
				//on이고
				if(vo.allocateApproval == "1"){
					//allocate가 있는지 확인
					$('#allocateApproval button').eq(0).addClass('active');
					if(vo.hasOwnProperty('allocate')){
						if(vo.allocate.hasOwnProperty('allocateApprovalStep1')){
							$('#allocateApprovalStep1').val(vo.allocate.allocateApprovalStep1.accountKey);
							if(vo.allocate.allocateApprovalStep1.hasOwnProperty('group')){
								$('#allocateStep1Name').text(nullToString(vo.allocate.allocateApprovalStep1.group.groupNm)+' '+vo.allocate.allocateApprovalStep1.name);	
							}else{
								$('#allocateStep1Name').text(vo.allocate.allocateApprovalStep1.name);
							}
						}else{
							$('#allocateStep1Name').text('차량관리자');
						}
						if(vo.allocate.hasOwnProperty('allocateApprovalStep2')){
							$('#allocateApprovalStep2').val(vo.allocate.allocateApprovalStep2.accountKey);
							if(vo.allocate.allocateApprovalStep2.hasOwnProperty('group')){
								$('#allocateStep2Name').text(nullToString(vo.allocate.allocateApprovalStep2.group.groupNm)+' '+vo.allocate.allocateApprovalStep2.name);	
							}else{
								$('#allocateStep2Name').text(vo.allocate.allocateApprovalStep2.name);
							}
									
						}else{
							$('#allocateStep2Name').text('결재 상신자 소속부서장');
						}
						
						if(vo.allocate.autoAllocateApprovalStep1 == "1"){
							$('#autoAllocateApprovalStep1 button').eq(0).addClass('active');
						}else{
							$('#autoAllocateApprovalStep1 button').eq(1).addClass('active');
						}
						if(vo.allocate.autoAllocateApprovalStep2 == "1"){
							$('#autoAllocateApprovalStep2 button').eq(0).addClass('active');
						}else{
							$('#autoAllocateApprovalStep2 button').eq(1).addClass('active');
						}
					}	
				}else{
					$('#allocateStep1Name').text('차량관리자');
					$('#allocateStep2Name').text('결재 상신자 소속부서장');
					$('#allocateApproval button').eq(1).addClass('active');
					$('#autoAllocateApprovalStep1 button').eq(1).addClass('active');
					$('#autoAllocateApprovalStep2 button').eq(1).addClass('active');
				}
				
				
				if(vo.maintenanceApproval == "1"){
					//allocate가 있는지 확인
					$('#maintenanceApproval button').eq(0).addClass('active');
					if(vo.hasOwnProperty('maintenance')){
						if(vo.maintenance.hasOwnProperty('maintenanceApprovalStep1')){
							$('#maintenanceApprovalStep1').val(vo.maintenance.maintenanceApprovalStep1.accountKey);
							if(vo.maintenance.maintenanceApprovalStep1.hasOwnProperty('group')){
								$('#maintenanceStep1Name').text(nullToString(vo.maintenance.maintenanceApprovalStep1.group.groupNm)+' '+vo.allocate.maintenanceApprovalStep1.name);	
							}else{
								$('#maintenanceStep1Name').text(vo.maintenance.maintenanceApprovalStep1.name);
							}
						}else{
							$('#maintenanceStep1Name').text('차량관리자');
						}
						if(vo.maintenance.hasOwnProperty('maintenanceApprovalStep2')){
							$('#maintenanceApprovalStep2').val(vo.maintenance.maintenanceApprovalStep2.accountKey);
							if(vo.maintenance.maintenanceApprovalStep2.hasOwnProperty('group')){
								$('#maintenanceStep2Name').text(nullToString(vo.maintenance.maintenanceApprovalStep2.group.groupNm)+' '+vo.allocate.maintenanceApprovalStep2.name);	
							}else{
								$('#maintenanceStep2Name').text(vo.maintenance.maintenanceApprovalStep2.name);
							}
						}else{
							$('#maintenanceStep2Name').text('결재 상신자 소속부서장');
						}
						
						if(vo.maintenance.autoMaintenanceApprovalStep1 == "1"){
							$('#autoMaintenanceApprovalStep1 button').eq(0).addClass('active');
						}else{
							$('#autoMaintenanceApprovalStep1 button').eq(1).addClass('active');
						}
						if(vo.maintenance.autoMaintenanceApprovalStep2 == "1"){
							$('#autoMaintenanceApprovalStep2 button').eq(0).addClass('active');
						}else{
							$('#autoMaintenanceApprovalStep2 button').eq(1).addClass('active');
						}
					}	
				}else{
					$('#maintenanceStep1Name').text('차량관리자');
					$('#maintenanceStep2Name').text('결재 상신자 소속부서장');
					$('#maintenanceApproval button').eq(1).addClass('active');
					$('#autoMaintenanceApprovalStep1 button').eq(1).addClass('active');
					$('#autoMaintenanceApprovalStep2 button').eq(1).addClass('active');
				}
				
				if(vo.expensesApproval == "1"){
					//allocate가 있는지 확인
					$('#expensesApproval button').eq(0).addClass('active');
					if(vo.hasOwnProperty('expenses')){
						if(vo.expenses.hasOwnProperty('expensesApprovalStep1')){
							$('#expensesApprovalStep1').val(vo.expenses.expensesApprovalStep1.accountKey);
							if(vo.expenses.expensesApprovalStep1.hasOwnProperty('group')){
								$('#expensesStep1Name').text(nullToString(vo.expenses.expensesApprovalStep1.group.groupNm)+' '+vo.expenses.expensesApprovalStep1.name);	
							}else{
								$('#expensesStep1Name').text(vo.expenses.expensesApprovalStep1.name);
							}
						}else{
							$('#expensesStep1Name').text('차량관리자');
						}
						if(vo.expenses.hasOwnProperty('expensesApprovalStep2')){
							$('#expensesApprovalStep2').val(vo.expenses.expensesApprovalStep2.accountKey);
							if(vo.expenses.expensesApprovalStep2.hasOwnProperty('group')){
								$('#expensesStep2Name').text(nullToString(vo.expenses.expensesApprovalStep2.group.groupNm)+' '+vo.expenses.expensesApprovalStep2.name);	
							}else{
								$('#expensesStep2Name').text(vo.expenses.expensesApprovalStep2.name);
							}
						}else{
							$('#expensesStep2Name').text('결재 상신자 소속부서장');
						}
						
						if(vo.expenses.autoExpensesApprovalStep1 == "1"){
							$('#autoExpensesApprovalStep1 button').eq(0).addClass('active');
						}else{
							$('#autoExpensesApprovalStep1 button').eq(1).addClass('active');
						}
						if(vo.expenses.autoExpensesApprovalStep2 == "1"){
							$('#autoExpensesApprovalStep2 button').eq(0).addClass('active');
						}else{
							$('#autoExpensesApprovalStep2 button').eq(1).addClass('active');
						}
					}	
				}else{
					$('#expensesStep1Name').text('차량관리자');
					$('#expensesStep2Name').text('결재 상신자 소속부서장');
					$('#expensesApproval button').eq(1).addClass('active');
					$('#autoExpensesApprovalStep1 button').eq(1).addClass('active');
					$('#autoExpensesApprovalStep2 button').eq(1).addClass('active');
				}

				if(vo.accidentApproval == "1"){
					$('#accidentApproval button').eq(0).addClass('active');
					if(vo.hasOwnProperty('accident')){
						if(vo.accident.hasOwnProperty('accidentApprovalStep1')){
							$('#accidentApprovalStep1').val(vo.accident.accidentApprovalStep1.accountKey);
							if(vo.accident.accidentApprovalStep1.hasOwnProperty('group')){
								$('#accidentStep1Name').text(nullToString(vo.accident.accidentApprovalStep1.group.groupNm)+' '+vo.accident.accidentApprovalStep1.name);	
							}else{
								$('#accidentStep1Name').text(vo.accident.accidentApprovalStep1.name);
							}
						}else{
							$('#accidentStep1Name').text('차량관리자');
						}
						if(vo.accident.hasOwnProperty('accidentApprovalStep2')){
							$('#accidentApprovalStep2').val(vo.accident.accidentApprovalStep2.accountKey);
							if(vo.accident.accidentApprovalStep2.hasOwnProperty('group')){
								$('#accidentStep2Name').text(nullToString(vo.accident.accidentApprovalStep2.group.groupNm)+' '+vo.accident.accidentApprovalStep2.name);	
							}else{
								$('#accidentStep2Name').text(vo.accident.accidentApprovalStep2.name);
							}
						}else{
							$('#accidentStep2Name').text('결재 상신자 소속부서장');
						}
						if(vo.accident.hasOwnProperty('accidentApprovalStep3')){
							$('#accidentApprovalStep3').val(vo.accident.accidentApprovalStep3.accountKey);
							if(vo.accident.accidentApprovalStep3.hasOwnProperty('group')){
								$('#accidentStep3Name').text(nullToString(vo.accident.accidentApprovalStep3.group.groupNm)+' '+vo.accident.accidentApprovalStep3.name);	
							}else{
								$('#accidentStep3Name').text(vo.accident.accidentApprovalStep3.name);
							}
						}else{
							$('#accidentStep3Name').text('대표이사');
						}
					}
				}else{
					$('#accidentStep1Name').text('차량관리자');
					$('#accidentStep2Name').text('결재 상신자 소속부서장');
					$('#accidentStep3Name').text('대표이사');
					$('#accidentApproval button').eq(1).addClass('active');
				}
				
			},
			error : function(t) {

			}
		});
	
	
	$('#approvalConfigCancelBtn').on('click',function(){
		window.history.back(-1);
	});
	
	$('#approvalConfigSaveBtn').on('click',function(){
		
		var allocateApproval = $('#allocateApproval button.active').data('onoff');
		var allocateApprovalStep1 =  $('#allocateApprovalStep1').val();
		var allocateApprovalStep2 =  $('#allocateApprovalStep2').val();
		var autoAllocateApprovalStep1 = $('#autoAllocateApprovalStep1 button.active').data('onoff');
		var autoAllocateApprovalStep2 = $('#autoAllocateApprovalStep2 button.active').data('onoff');
		
		var maintenanceApproval = $('#maintenanceApproval button.active').data('onoff');
		var maintenanceApprovalStep1 =  $('#maintenanceApprovalStep1').val();
		var maintenanceApprovalStep2 =  $('#maintenanceApprovalStep2').val();
		var autoMaintenanceApprovalStep1 = $('#autoMaintenanceApprovalStep1 button.active').data('onoff');
		var autoMaintenanceApprovalStep2 = $('#autoMaintenanceApprovalStep2 button.active').data('onoff');
		
		var expensesApproval = $('#expensesApproval button.active').data('onoff');
		var expensesApprovalStep1 =  $('#expensesApprovalStep1').val();
		var expensesApprovalStep2 =  $('#expensesApprovalStep2').val();
		var autoExpensesApprovalStep1 = $('#autoExpensesApprovalStep1 button.active').data('onoff');
		var autoExpensesApprovalStep2 = $('#autoExpensesApprovalStep2 button.active').data('onoff');
		
		var accidentApproval = $('#accidentApproval button.active').data('onoff');
		var accidentApprovalStep1 =  $('#accidentApprovalStep1').val();
		var accidentApprovalStep2 =  $('#accidentApprovalStep2').val();
		var accidentApprovalStep3 =  $('#accidentApprovalStep3').val();
		
		
		var sendData = {
			"allocateApproval" : allocateApproval,
			"allocateApprovalStep1" : allocateApprovalStep1,
			"allocateApprovalStep2" : allocateApprovalStep2,
			"autoAllocateApprovalStep1" : autoAllocateApprovalStep1,
			"autoAllocateApprovalStep2" : autoAllocateApprovalStep2,
			"maintenanceApproval" : maintenanceApproval,
			"maintenanceApprovalStep1" : maintenanceApprovalStep1,
			"maintenanceApprovalStep2" : maintenanceApprovalStep2,
			"autoMaintenanceApprovalStep1" : autoMaintenanceApprovalStep1,
			"autoMaintenanceApprovalStep2" : autoMaintenanceApprovalStep2,
			"expensesApproval" : expensesApproval,
			"expensesApprovalStep1" : expensesApprovalStep1,
			"expensesApprovalStep2" : expensesApprovalStep2,
			"autoExpensesApprovalStep1" : autoExpensesApprovalStep1,
			"autoExpensesApprovalStep2" : autoExpensesApprovalStep2,
			"accidentApproval" : accidentApproval,
			"accidentApprovalStep1" : accidentApprovalStep1,
			"accidentApprovalStep2" : accidentApprovalStep2,
			"accidentApprovalStep3" : accidentApprovalStep3
		}
		
		console.log(sendData);
		
		$V4.http_post("/api/1/approvalConfig", sendData, {
			requestMethod : "POST",
			header : {
				"key" : "${_KEY}"
			},
			success : function(rtv) {
				alert('저장되었습니다.')
			},
			error : function(t) {

			}
		});
	});
	
	//onoff이벤트
	$('.btn-onoff button').on('click',function(){
		var self = $(this);
		self.addClass('active')
		
		if(self.index() == 0){
			self.next().removeClass('active');	
		}else{
			self.prev().removeClass('active');	
		}
	});
	
	//기본값 버튼
	$('.userReset').on('click',function(){
		console.log(this);
		var self = $(this);
		
		var step = self.data('step');
		//input 초기화
		self.prev().prev().val('');
		
		if(step == 1){
			self.prev().prev().prev().text('차량관리자');
		}else if(step == 2){
			self.prev().prev().prev().text('결재 상신자 소속부서장');
		}else if(step == 3){
			self.prev().prev().prev().text('대표이사');
		}

		
	});
	
	//차량배차 1단계
	$("#allocateApprovalStep1Pop").data({
			type : "user",
			http_post_option : {
				header : {
					key : "${_KEY}"
				}
			},
			selType : "radio",
				action : {
					ok : function(arrSelect, inputObj, valueObj) {
						$.each(arrSelect, function() {
							var info = base64ToJsonObj($(arrSelect).data("info"));
							$(inputObj).html(info.name);
							$(valueObj).val(info.accountKey);
						});
					}
				},
				targetInput : $("#allocateApprovalStep1").prev(),
				targetInputValue : $("#allocateApprovalStep1")
	}).commonPop();
	
	//차량배차 2단계
	$("#allocateApprovalStep2Pop").data({
		type : "user",
		http_post_option : {
			header : {
				key : "${_KEY}"
			}
		},
		selType : "radio",
			action : {
				ok : function(arrSelect, inputObj, valueObj) {
					$.each(arrSelect, function() {
						var info = base64ToJsonObj($(arrSelect).data("info"));
						$(inputObj).html(info.name);
						$(valueObj).val(info.accountKey);
					});
				}
			},
			targetInput : $("#allocateApprovalStep2").prev(),
			targetInputValue : $("#allocateApprovalStep2")
	}).commonPop();
	
	//정비1단계
	$("#maintenanceApprovalStep1Pop").data({
		type : "user",
		http_post_option : {
			header : {
				key : "${_KEY}"
			}
		},
		selType : "radio",
			action : {
				ok : function(arrSelect, inputObj, valueObj) {
					$.each(arrSelect, function() {
						var info = base64ToJsonObj($(arrSelect).data("info"));
						$(inputObj).html(info.name);
						$(valueObj).val(info.accountKey);
					});
				}
			},
			targetInput : $("#maintenanceApprovalStep1").prev(),
			targetInputValue : $("#maintenanceApprovalStep1")
	}).commonPop();
	
	//정비2단계
	$("#maintenanceApprovalStep2Pop").data({
		type : "user",
		http_post_option : {
			header : {
				key : "${_KEY}"
			}
		},
		selType : "radio",
			action : {
				ok : function(arrSelect, inputObj, valueObj) {
					$.each(arrSelect, function() {
						var info = base64ToJsonObj($(arrSelect).data("info"));
						$(inputObj).html(info.name);
						$(valueObj).val(info.accountKey);
					});
				}
			},
			targetInput : $("#maintenanceApprovalStep2").prev(),
			targetInputValue : $("#maintenanceApprovalStep2")
	}).commonPop();
	
	//경비1단계
	$("#expensesApprovalStep1Pop").data({
		type : "user",
		http_post_option : {
			header : {
				key : "${_KEY}"
			}
		},
		selType : "radio",
			action : {
				ok : function(arrSelect, inputObj, valueObj) {
					$.each(arrSelect, function() {
						var info = base64ToJsonObj($(arrSelect).data("info"));
						$(inputObj).html(info.name);
						$(valueObj).val(info.accountKey);
					});
				}
			},
			targetInput : $("#expensesApprovalStep1").prev(),
			targetInputValue : $("#expensesApprovalStep1")
	}).commonPop();
	
	//경비2단계
	$("#expensesApprovalStep2Pop").data({
		type : "user",
		http_post_option : {
			header : {
				key : "${_KEY}"
			}
		},
		selType : "radio",
			action : {
				ok : function(arrSelect, inputObj, valueObj) {
					$.each(arrSelect, function() {
						var info = base64ToJsonObj($(arrSelect).data("info"));
						$(inputObj).html(info.name);
						$(valueObj).val(info.accountKey);
					});
				}
			},
			targetInput : $("#expensesApprovalStep2").prev(),
			targetInputValue : $("#expensesApprovalStep2")
	}).commonPop();
	
	//사고보고서 1단계
	$("#accidentApprovalStep1Pop").data({
		type : "user",
		http_post_option : {
			header : {
				key : "${_KEY}"
			}
		},
		selType : "radio",
			action : {
				ok : function(arrSelect, inputObj, valueObj) {
					$.each(arrSelect, function() {
						var info = base64ToJsonObj($(arrSelect).data("info"));
						$(inputObj).html(info.name);
						$(valueObj).val(info.accountKey);
					});
				}
			},
			targetInput : $("#accidentApprovalStep1").prev(),
			targetInputValue : $("#accidentApprovalStep1")
	}).commonPop();
	
	//사고보고서 2단계
	$("#accidentApprovalStep2Pop").data({
		type : "user",
		http_post_option : {
			header : {
				key : "${_KEY}"
			}
		},
		selType : "radio",
			action : {
				ok : function(arrSelect, inputObj, valueObj) {
					$.each(arrSelect, function() {
						var info = base64ToJsonObj($(arrSelect).data("info"));
						$(inputObj).html(info.name);
						$(valueObj).val(info.accountKey);
					});
				}
			},
			targetInput : $("#accidentApprovalStep2").prev(),
			targetInputValue : $("#accidentApprovalStep2")
	}).commonPop();
	
	//사고보고서 3단계
	$("#accidentApprovalStep3Pop").data({
		type : "user",
		http_post_option : {
			header : {
				key : "${_KEY}"
			}
		},
		selType : "radio",
			action : {
				ok : function(arrSelect, inputObj, valueObj) {
					$.each(arrSelect, function() {
						var info = base64ToJsonObj($(arrSelect).data("info"));
						$(inputObj).html(info.name);
						$(valueObj).val(info.accountKey);
					});
				}
			},
			targetInput : $("#accidentApprovalStep3").prev(),
			targetInputValue : $("#accidentApprovalStep3")
	}).commonPop();
	
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2 class="md">결재관리</h2>
                    <span>
						배차, 정비, 경비, 사고보고 등 결재관리가 필요한 서비스를 관리합니다.
						<br /> 서비스 등급별 제공되는 결재관리는 차별화됩니다.
						<br /> 설정된 결재는 보고서∙결재 > 결재관리에서 사용할 수 있으며 ViewCAR에서 결재메일도 발송합니다.
					</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="setting-page">
                    <div class="sign-management">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                            <div class="item">
                                <span>
									사용자 등록 결재 부서장
									<strong>1</strong>건
								</span>
                            </div>

                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button type="button" class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <div class="section01 clr">
                            <!-- 차량배차 결재 설정 -->
                            <div class="left">
                                <div class="title help">
                                    <h3 class="tit2">차량배차 결재 설정</h3>
                                    <div class="btn-onoff" id="allocateApproval">
                                        <button type="button" data-onoff="1">ON</button>
                                        <button type="button" data-onoff="0">OFF</button>
                                    </div>
                                    <dl class="help-layer">
                                        <dt>차량배차 결재 설정 Tip</dt>
                                        <dd>
                                            <ul class="txt mgt0">
                                                <li>
                                                    1단계 결재 : 차량관리자 (회원가입시 설정한 관리자입니다. 변경이 필요한 경우 사용자 검색을 통해 지정하세요.)</li>
                                                <li>2단계 결재 : 결재상신자 소속부서장으로 결재권자는
                                                    <span class="red">사용자 등록시 직급에서 부서장을 꼭 선택해주세요. </span></li>
                                            </ul>
                                        </dd>
                                    </dl>
                                </div>

                                <ol class="setting">
                                    <li>
                                        <span class="name">1단계 : </span><span class="name" id="allocateStep1Name"></span>
                                        <input type="hidden" name="allocateApprovalStep1" id="allocateApprovalStep1"/>
                                        <button type="button" class="btn btn04 md" id="allocateApprovalStep1Pop">변경</button>
                                        <button type="button" class="btn btn04 md userReset" data-step='1'>기본값</button>
                                        <span>자동 합의</span>
                                        <div class="btn-onoff" id="autoAllocateApprovalStep1">
                                            <button type="button" data-onoff="1">ON</button>
                                            <button type="button" data-onoff="0">OFF</button>
                                        </div>
                                    </li>
                                    <li>
                                        <span class="name">2단계 : </span><span class="name" id="allocateStep2Name"></span>
                                        <input type="hidden" name="allocateApprovalStep2" id="allocateApprovalStep2"/>
                                        <button type="button" class="btn btn04 md" id="allocateApprovalStep2Pop">변경</button>
                                        <button type="button" class="btn btn04 md userReset" data-step='2'>기본값</button>
                                        <span>자동 결재</span>
                                        <div class="btn-onoff" id="autoAllocateApprovalStep2">
                                            <button type="button" data-onoff="1">ON</button>
                                       	 	<button type="button" data-onoff="0">OFF</button>
                                        </div>
                                    </li>
                                </ol>
                                <ul class="txt">
                                    <li>지정배차 차량은 자동 배차되어 배차 결재되지 않습니다. </li>
                                </ul>
                            </div>
                            <!--/ 차량배차 결재 설정 -->

                            <!-- 차량정비 결재 설정 -->
                            <div class="right">
                                <div class="title help">
                                    <h3 class="tit2">차량정비 결재 설정</h3>
                                    <div class="btn-onoff" id="maintenanceApproval">
                                        <button type="button"  data-onoff="1">ON</button>
                                       	 <button type="button" data-onoff="0">OFF</button>
                                    </div>
                                    <dl class="help-layer">
                                        <dt>차량정비 결재 설정 Tip</dt>
                                        <dd>
                                            <ul class="txt mgt0">
                                                <li>
                                                    1단계 결재 : 차량관리자 (변경이 필요한 경우 사용자 검색을 통해 지정하세요.)</li>
                                                <li>>2단계 결재 : 결재상신자 소속부서장으로 결재권자는
                                                    <span class="red">사용자 등록시 직급에서 부서장을 꼭 선택해주세요. </span></li>
                                            </ul>
                                        </dd>
                                    </dl>
                                </div>

                                <ol class="setting">
                                    <li>
                                        <span class="name">1단계 : </span><span class="name" id="maintenanceStep1Name"></span>
                                        <input type="hidden" name="maintenanceApprovalStep1" id="maintenanceApprovalStep1"/>
                                        <button type="button" class="btn btn04 md" id="maintenanceApprovalStep1Pop">변경</button>
                                        <button type="button" class="btn btn04 md userReset" data-step='1'>기본값</button>
                                        <span>자동 합의</span>
                                        <div class="btn-onoff" id="autoMaintenanceApprovalStep1">
                                            <button type="button" data-onoff="1">ON</button>
                                       	 	<button type="button" data-onoff="0">OFF</button>
                                        </div>
                                    </li>
                                    <li>
                                        <span class="name">2단계 : </span><span class="name" id="maintenanceStep2Name"></span>
                                        <input type="hidden" name="maintenanceApprovalStep2" id="maintenanceApprovalStep2"/>
                                        <button type="button" class="btn btn04 md" id="maintenanceApprovalStep2Pop">변경</button>
                                        <button type="button" class="btn btn04 md userReset" data-step='2'>기본값</button>
                                        <span>자동 결재</span>
                                        <div class="btn-onoff" id="autoMaintenanceApprovalStep2">
                                            <button type="button" data-onoff="1">ON</button>
                                       	 	<button type="button" data-onoff="0">OFF</button>
                                        </div>
                                    </li>
                                </ol>
                            </div>
                            <!--/ 차량정비 결재 설정 -->
                        </div>

                        <div class="section02 clr">
                            <!-- 차량경비 결재 설정 -->
                            <div class="left">
                                <div class="title help">
                                    <h3 class="tit2">차량경비 결재 설정</h3>
                                    <div class="btn-onoff" id="expensesApproval">
                                        <button type="button"  data-onoff="1">ON</button>
                                       	 <button type="button" data-onoff="0">OFF</button>
                                    </div>
                                    <dl class="help-layer">
                                        <dt>차량경비 결재 설정 Tip</dt>
                                        <dd>
                                            <ul class="txt mgt0">
                                                <li>1단계 결재 : 차량관리자 (변경이 필요한 경우 사용자 검색을 통해 지정하세요.)</li>
                                                <li>>2단계 결재 : 결재상신자 소속부서장으로 결재권자는
                                                    <span class="red">사용자 등록시 직급에서 부서장을 꼭 선택해주세요. </span></li>
                                            </ul>
                                        </dd>
                                    </dl>
                                </div>

                                <ol class="setting">
                                    <li>
                                        <span class="name">1단계 : </span><span class="name" id="expensesStep1Name"></span>
                                        <input type="hidden" name="expensesApprovalStep1" id="expensesApprovalStep1"/>
                                        <button type="button" class="btn btn04 md" id="expensesApprovalStep1Pop">변경</button>
                                        <button type="button" class="btn btn04 md userReset" data-step='1'>기본값</button>
                                        <span>자동 합의</span>
                                        <div class="btn-onoff" id="autoExpensesApprovalStep1">
                                            <button type="button" data-onoff="1">ON</button>
                                       	 	<button type="button" data-onoff="0">OFF</button>
                                        </div>
                                    </li>
                                    <li>
                                        <span class="name">2단계 : </span><span class="name" id="expensesStep2Name"></span>
                                        <input type="hidden" name="expensesApprovalStep2" id="expensesApprovalStep2"/>
                                        <button type="button" class="btn btn04 md" id="expensesApprovalStep2Pop">변경</button>
                                        <button type="button" class="btn btn04 md userReset" data-step='2'>기본값</button>
                                        <span>자동 결재</span>
                                        <div class="btn-onoff" id="autoExpensesApprovalStep2">
                                            <button type="button" data-onoff="1">ON</button>
                                       	 	<button type="button" data-onoff="0">OFF</button>
                                        </div>
                                    </li>
                                </ol>
                            </div>
                            <!--/ 차량경비 결재 설정 -->

                            <!-- 사고보고서 결재 설정 -->
                            <div class="right">
                                <div class="title help">
                                    <h3 class="tit2">사고보고서 결재 설정</h3>
                                    <div class="btn-onoff" id="accidentApproval">
                                        <button type="button" data-onoff="1">ON</button>
                                       	<button type="button" data-onoff="0">OFF</button>
                                    </div>
                                    <dl class="help-layer">
                                        <dt>사고보고서 결재 설정 Tip</dt>
                                        <dd>
                                            <ul class="txt mgt0">
                                                <li>
                                                    1단계 결재 : 차량관리자 (변경이 필요한 경우 사용자 검색을 통해 지정하세요.)</li>
                                                <li>2단계 결재 : 결재상신자 소속부서장으로 결재권자는
                                                    <span class="red">사용자 등록시 직급에서 부서장을 꼭 선택해주세요. </span></li>
                                                <li>>3단계 결재 : 회사대표자로 기본 셋팅되어 있습니다. 최종결재권자가 다른경우 변경해주세요.</li>
                                            </ul>
                                        </dd>
                                    </dl>
                                </div>

                                <ol class="setting">
                                    <li>
                                        <span class="name">1단계 : </span><span class="name" id="accidentStep1Name"></span>
                                        <input type="hidden" name="accidentApprovalStep1" id="accidentApprovalStep1"/>
                                        <button type="button" class="btn btn04 md" id="accidentApprovalStep1Pop">변경</button>
                                        <button type="button" class="btn btn04 md userReset" data-step='1'>기본값</button>
                                    </li>
                                    <li>
                                        <span class="name">2단계 : </span><span class="name" id="accidentStep2Name"></span>
                                        <input type="hidden" name="accidentApprovalStep2" id="accidentApprovalStep2"/>
                                        <button type="button" class="btn btn04 md" id="accidentApprovalStep2Pop">변경</button>
                                        <button type="button" class="btn btn04 md userReset" data-step='2'>기본값</button>
                                    </li>
                                    <li>
                                        <span class="name">3단계 : </span><span class="name" id="accidentStep3Name"></span>
                                        <input type="hidden" name="accidentApprovalStep3" id="accidentApprovalStep3"/>
                                        <button type="button" class="btn btn04 md" id="accidentApprovalStep3Pop">변경</button>
                                        <button type="button" class="btn btn04 md userReset" data-step='3'>기본값</button>
                                    </li>
                                </ol>
                                <ul class="txt">
                                    <li>사고보고서는 업무의 특성상 자동 결재기능을 제공하지 않습니다. </li>
                                </ul>
                            </div>
                            <!--/ 사고보고서 결재 설정 -->
                        </div>
                    </div>
                    <div class="btn-bottom">
                        <button type="button" class="btn btn02" id="approvalConfigCancelBtn">취소</button>
                        <button type="button" class="btn btn03" id="approvalConfigSaveBtn">저장</button>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->