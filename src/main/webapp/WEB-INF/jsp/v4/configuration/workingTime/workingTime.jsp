<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	
	//등록된 업무 시간 카운트를 가져 와야한다.
	$V4.http_post("/api/1/workingTimeTotalCnt",null,{
		requestMethod : "GET"
		,header : {"key" :"${_KEY}" }
		,success : function(rtv){
			$('#workingTimeTotalCnt').text(rtv.result);
		},error : function(t){
			
		}
	});
	
	
	//업무시간 등록
	$('#workingTimeRegBtn').on('click',function(){
		$V4.move('/config/workingArea/reg');
	});
	
	$("#workingTimeList").on("click",".btn_moveDetail",function(){
		var workTimeKey = $(this).data('key');
		$V4.move('/config/workingArea/reg',{'workTimeKey' : workTimeKey});
	});
			
	
	var setting = {
			thead : [ {
				'<input type="checkbox" id="allChker" />' : "6%"
			}, {
				"부서" : "17%"
			}, {
				"사용자명" : "17%"
			}, {
				"월" : "5%"
			}, {
				"화" : "5%"
			}, {
				"수" : "5%"
			},{
				"목" : "5%"
			},{
				"금" : "5%"
			},{
				"토" : "5%"
			},{
				"일" : "5%"
			}, {
				"업무시간" : "15%"
			}, {
				"등록확인" : "10%"
			} ]
			,bodyHtml : (function(data){
				var strHtml = "";
				for(i in data.result){
					var obj = data.result[i];
					strHtml += "<tr>"
					strHtml += '<td><input type="checkbox" class="selObj" data-key="'+obj.workTimeKey+'"/></td>';
					strHtml += '<td>'+(obj.corpGroupNm?obj.corpGroupNm:"")+'</td>';
					strHtml += '<td>'+(obj.accountNm?obj.accountNm:"")+(obj.corpPosition?"/"+obj.corpPosition:"")+'</td>';
					strHtml += '<td>'+(obj.dayMon?"&#10003;":"")+'</td>';
					strHtml += '<td>'+(obj.dayTue?"&#10003;":"")+'</td>';
					strHtml += '<td>'+(obj.dayWed?"&#10003;":"")+'</td>';
					strHtml += '<td>'+(obj.dayThu?"&#10003;":"")+'</td>';
					strHtml += '<td>'+(obj.dayFri?"&#10003;":"")+'</td>';
					strHtml += '<td>'+(obj.daySat?"&#10003;":"")+'</td>';
					strHtml += '<td>'+(obj.daySun?"&#10003;":"")+'</td>';
					strHtml += '<td>'+LPAD(obj.startH,'0',2)+':'+LPAD(obj.startM,'0',2)+'~'+LPAD(obj.endH,'0',2)+':'+LPAD(obj.endM,'0',2)+'</td>';
					strHtml += '<td><button type="button" class="btn-img change btn_moveDetail" data-key="'+obj.workTimeKey+'">확인</button></td>';
					
					
					strHtml += "</tr>";
				}
				return strHtml;
			})
			,limit : 2 
			,pagePerGroup : 3 //pagingGroup size
			,url : "/api/1/workingTime"
			,param : {}
			,http_post_option : {
				requestMethod : "GET"
				,header : {key : "${_KEY}"}
			}
			//login token - test2@naver.com / test123!@# (이아이디가 차량 많음)
			,initSearch : true //생성과 동시에 search
			,loadingBodyViewer : true //로딩중!! 표시됨
		}
	
	$('#workingTimeList').commonList(setting);
	
	//검색 조회 클릭시 엔터시
	function search(){
		//검색타입 전체,부서,이름,아이디
		var searchType = $('#searchType').val();
		//검색명
		var searchText = $('#searchText').val()
		
		var param = {
			"searchType" : searchType,
			"searchText" : searchText
		};
		
		$("#workingTimeList").commonList("search",param);
	}
	
	//사용자 검색	
	$('#workingTimeSearch').on('click',debounce(function(){
		search();
	},1000,true));
	
	var db = debounce(search, 1000, true);
	
	$('#searchText').on('keyup',function(e){
		if(e.keyCode == 13) db()
	});
	
	$("#allChker").on("click",function(){
		if($(this).is(":checked"))
			$(".selObj","#workingTimeList tbody").prop("checked",true);
		else $(".selObj","#workingTimeList tbody").prop("checked",false);
	});
	
	$('#workingTimeDelBtn').on('click',function(){
		var selObj = $(".selObj:checked");

		if(selObj.length == 0){
			alert("삭제할 업무시간을 선택해주세요.");
			return;
		}
		
		if(confirm('정말로 삭제하시겠습니까?')){
			
			var strKey = "";
        	$(".selObj:checked").each(function(){
        		strKey += strKey==""?$(this).data("key"):","+$(this).data("key");
        	});
        	
        	$V4.http_post("/api/{ver}/workingTime/"+strKey,null,{
    			requestMethod : "DELETE"
    			,header : {"key" :"${_KEY}" }
    			,success : function(rtv){
    				search();		
    			},error : function(t){
    				
    			}
    		});
		}
	});
	
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>업무시간 설정</h2>
                    <span>소속직원 전체 또는 부서별, 직원별 업무시간을 설정하실 수 있습니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="setting-page">
                    <div class="setting-work">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                            <div class="item">
                                <span>
									업무시간 설정건
									<strong id="workingTimeTotalCnt">0</strong>건
								</span>
                            </div>

                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <!-- 업무시간등록 -->
                        <h3 class="tit2 mgt30">업무시간 등록</h3>
                        <div class="box-register">
                            기본업무시간은 09:00 ~ 18:00이며, 운행기록부의 기준데이터로 설정되니 정확히 입력해주세요.

                            <div class="right">
                                <button class="btn btn01" id="workingTimeRegBtn">업무시간 등록</button>
                            </div>
                        </div>
                        <!--/ 업무시간등록 -->

                        <!-- 상단 검색 -->
                        <h3 class="tit2 mgt30">업무시간 조회</h3>
                        <div class="top-search mgt20">
                            <select id="searchType">
								<option value="">전체</option>
								<option value="group">부서명</option>
								<option value="name">이름</option>
								<option value="id">아이디</option>
							</select>
                            <input type="text" name="" placeholder="검색할 항목을 먼저 선택해주세요." id="searchText"/>
                            <button class="btn btn03" id="workingTimeSearch">조회</button>
                        </div>
                        <!--/ 상단 검색 -->

                        <!-- table 버튼 -->
                        <div class="btn-function">
                            <div class="right">
                                <button type="button" class="delete" id="workingTimeDelBtn">삭제</button>
                                <select name="">
									<option value="">5건씩 보기</option>
								</select>
                            </div>
                        </div>
                        <!--/ table 버튼 -->

                        <table class="table list mgt20" id="workingTimeList">
                            <caption>업무시간 목록</caption>
                          <%--   <colgroup>
                                <col style="width:6%" />
                                <col style="width:17%" />
                                <col style="width:17%" />
                                <col style="width:5%" />
                                <col style="width:5%" />
                                <col style="width:5%" />
                                <col style="width:5%" />
                                <col style="width:5%" />
                                <col style="width:5%" />
                                <col style="width:5%" />
                                <col style="width:15%" />
                                <col style="width:10%" />
                            </colgroup> --%>
                            <thead>
                                <!-- <tr>
                                    <th scope="col">
                                        <input type="checkbox" />
                                    </th>
                                    <th scope="col">부서</th>
                                    <th scope="col">사용자명</th>
                                    <th scope="col">월</th>
                                    <th scope="col">화</th>
                                    <th scope="col">수</th>
                                    <th scope="col">목</th>
                                    <th scope="col">금</th>
                                    <th scope="col">토</th>
                                    <th scope="col">일</th>
                                    <th scope="col">업무시간</th>
                                    <th scope="col">등록확인</th>
                                </tr> -->
                            </thead>
                            <tbody>
                                <!-- <tr>
                                    <td>
                                        <input type="checkbox" />
                                    </td>
                                    <td>개발부서</td>
                                    <td>홍길동</td>
                                    <td>✓</td>
                                    <td>✓</td>
                                    <td>✓</td>
                                    <td>✓</td>
                                    <td>✓</td>
                                    <td>✓</td>
                                    <td>✓</td>
                                    <td>08:00 ~ 20:00</td>
                                    <td>
                                        <button class="btn-img change">확인</button>
                                    </td>
                                </tr> -->
                            </tbody>
                        </table>

                    </div>

                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->