<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">

$(function(){
	//전체부서
	var initGroupSelect = function(){
		$V4.http_post("/api/1/groupTree",null,{
			requestMethod : "GET"
			,header : {"key" :"${_KEY}" }
			,success : function(rtv){
				var list = rtv.result;
				
				var groupKey = "${rtv.groupKey}";
	    		$('#groupList').empty();
	    		var strHtml = "<option value=''>전체 부서</option>";
	    		for(var i = 0 ; i < list.length;i++){
	    			var vo = list[i];
	    			if(vo.groupKey == groupKey){
	    				strHtml += '<option value="'+vo.groupKey+'" selected>'+vo.groupNmList+'</option>';
	    			}else{
	    				strHtml += '<option value="'+vo.groupKey+'">'+vo.groupNmList+'</option>';	
	    			}
	    			
	    		}
	    		$('#groupList').html(strHtml);
			},error : function(t){
				
			}
		});
	}
	//전체 사용자
	
	var initUserSelect = function(groupKey){
		$V4.http_post("/api/1/account",{"groupKey" : groupKey},{
			requestMethod : "GET"
			,header : {"key" :"${_KEY}" }
			,success : function(rtv){
				var list = rtv.result;
	    		$('#userList').empty();
	    		var accountKey = "${rtv.accountKey}";
	    		var strHtml = "<option value=''>전체 사용자</option>";
	    		for(var i = 0 ; i < list.length;i++){
	    			var vo = list[i];
	    			if(vo.accountKey == accountKey){
	    				strHtml += '<option value="'+vo.accountKey+'" selected>'+vo.name+'</option>';
	    			}else{
	    				strHtml += '<option value="'+vo.accountKey+'">'+vo.name+'</option>';	
	    			}
	    			
	    		}
	    		$('#userList').html(strHtml);
			},error : function(t){
				
			}
		});
	};
	
	initGroupSelect();
	initUserSelect();
	
	//workTimeKey가 있으면 해당 정보를 가져온다.
	
	$('#groupList').on('change',function(){
		//해당부서에 사용자를 보여 줘야 한다
		initUserSelect($('#groupList').val());
	});
	
	$('#workingTimeRegBtn').on('click',function(){
		var rtv = true;
		var startH = $("select[name=startH]").val();
		var startM = $("select[name=startM]").val();
		var endH = $("select[name=endH]").val();
		var endM = $("select[name=endM]").val();

		if(startH.length == 0 || startM.length == 0 || endH.length == 0 || endM.length == 0){
			alert("업무시간을 설정해야 합니다.");
			rtv = false;
		}

		var sDate = new Date(1900,1,1,startH,startM,0).getTime();
		var eDate = new Date(1900,1,1,endH,endM,0).getTime();

		if(sDate>=eDate){
			rtv&&alert("업무시간이 잘못 되었습니다.");
			rtv = false;
		}
		
		var chkValid = 0;
		var dayArr = [];
		$("#chkArea input[type=checkbox]").each(function(i){
			if($(this).is(":checked")){
				chkValid++;
				dayArr[i] = $(this).val();
			}else dayArr[i] = null;
		});
		if(chkValid == 0){
			rtv&&alert("업무요일을 설정해야 합니다.");
			rtv = false;
		}
		
		if(rtv){
			var param = {};

        	param.groupKey = $("#groupList").val();		            	
        	param.accountKey = $("#userList").val();
        	
        	param.startH = startH;
        	param.startM = startM;
        	param.endH = endH;
        	param.endM = endM;
        	param.day = '';
        	for(var i = 0;i < dayArr.length;i++){
        		param.day += dayArr[i];
        		if(i != dayArr.length - 1)
        			param.day += ',';
        	}
        	<c:if test="${empty rtv.workTimeKey }">
	        	$V4.http_post("/api/1/workingTime",param,{
	        		header : {"key" :"${_KEY}" }
	            	,success : function(rtv){
	            		alert('등록되었습니다.');
	            		$V4.move('/config/workingArea');
	            	}
	            });
        	</c:if>
        	<c:if test="${not empty rtv.workTimeKey }">
	        	$V4.http_post("/api/1/workingTime/"+"${rtv.workTimeKey}",param,{
	    			requestMethod : "PUT"
	        		,header : {"key" :"${_KEY}" }
	            	,success : function(rtv){
	            		alert('등록되었습니다.');
	            		$V4.move('/config/workingArea');
	            	}
	            });
        	</c:if>	
        	
		}
	});
	
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>업무시간 설정</h2>
                    <span>소속직원 전체 또는 부서별, 직원별 업무시간을 설정하실 수 있습니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="setting-page">
                    <div class="box-layout setting-work">
                        <div class="title">
                            <h3 class="tit3">업무시간 등록</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
                            <div class="info-txt">
                                기본업무시간은 09:00 ~ 18:00이며, 운행기록부의 기준데이터로 설정되니 정확히 입력해주세요.
                            </div>

                            <h3 class="tit2 mgt30">업무시간 등록 대상</h3>
                            <table class="table mgt20">
                                <caption>업무시간 등록 대상</caption>
                                <tbody>
                                    <tr>
                                        <th scope="row">소속</th>
                                        <td class="left">
                                            <input type="text" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">등록 대상</th>
                                        <td class="left">
                                            <div class="division">
                                                <div class="col-6">
                                                    <select name="groupList" id="groupList">
														<option value="">전체 부서</option>
													</select>
                                                </div>
                                                <div class="space">&nbsp;</div>
                                                <div class="col-6">
                                                    <select name="userList" id="userList">
														<option value="">전체 사용자</option>
													</select>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <h3 class="tit2 mgt30">업무시간 상세설정</h3>
                            <table class="table mgt20">
                                <caption>업무시간 상세설정</caption>

                                <tbody>
                                    <tr>
                                        <th scope="row">업무시작</th>
                                        <td class="left">
                                            <div class="division">
                                                <div class="col-6">
                                                    <select name="startH" id="startH">
														<c:forEach begin="0" end="23" step="1" var="i">
															<option value="${i}" <c:if test='${i eq rtv.startH}'>selected</c:if>>${i}</option>
														</c:forEach>
													</select>
                                                </div>
                                                <div class="space">&nbsp;시&nbsp;</div>
                                                <div class="col-6">
                                                    <select name="startM" id="startM">
														<c:forEach begin="0" end="59" step="1" var="i">
															<option value="${i}" <c:if test='${i eq rtv.startM}'>selected</c:if>>${i}</option>
														</c:forEach>
													</select>
                                                </div>
                                                <div class="space">&nbsp;분&nbsp;</div>
                                            </div>
                                        </td>
                                        <th scope="row">업무시작</th>
                                        <td>
                                            <div class="division">
                                                <div class="col-6">
                                                    <select name="endH" id="endH">
														<c:forEach begin="0" end="23" step="1" var="i">
															<option value="${i}" <c:if test='${i eq rtv.endH}'>selected</c:if>>${i}</option>
														</c:forEach>
													</select>
                                                </div>
                                                <div class="space">&nbsp;시&nbsp;</div>
                                                <div class="col-6">
                                                    <select name="endM" id="endM">
														<c:forEach begin="0" end="59" step="1" var="i">
															<option value="${i}" <c:if test='${i eq rtv.endM}'>selected</c:if>>${i}</option>
														</c:forEach>
													</select>
                                                </div>
                                                <div class="space">&nbsp;분&nbsp;</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">등록 대상</th>
                                        <td class="left" colspan="3" id="chkArea">
                                            <input type="checkbox" id="chk1" name="dayMon" value="1" <c:if test='${not empty rtv.dayMon}'>checked</c:if> />&nbsp;월&nbsp;&nbsp;&nbsp;
                                            <input type="checkbox" id="chk1" name="dayTue" value="1" <c:if test='${not empty rtv.dayTue}'>checked</c:if>/>&nbsp;화&nbsp;&nbsp;&nbsp;
                                            <input type="checkbox" id="chk1" name="dayWed" value="1" <c:if test='${not empty rtv.dayWed}'>checked</c:if>/>&nbsp;수&nbsp;&nbsp;&nbsp;
                                            <input type="checkbox" id="chk1" name="dayThu" value="1" <c:if test='${not empty rtv.dayThu}'>checked</c:if>/>&nbsp;목&nbsp;&nbsp;&nbsp;
                                            <input type="checkbox" id="chk1" name="dayFri" value="1" <c:if test='${not empty rtv.dayFri}'>checked</c:if>/>&nbsp;금&nbsp;&nbsp;&nbsp;
                                            <input type="checkbox" id="chk1" name="daySat" value="1" <c:if test='${not empty rtv.daySat}'>checked</c:if>/>&nbsp;토&nbsp;&nbsp;&nbsp;
                                            <input type="checkbox" id="chk1" name="daySun" value="1" <c:if test='${not empty rtv.daySun}'>checked</c:if>/>&nbsp;일
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <!-- 하단 버튼 -->
                            <div class="btn-bottom">
                                <button class="btn btn02">취소</button>
                                <c:if test="${empty rtv.workTimeKey }">
									<button class="btn btn03" id="workingTimeRegBtn">등록완료</button>
								</c:if>
								<c:if test="${not empty rtv.workTimeKey }">
									<button class="btn btn03" id="workingTimeRegBtn">수정</button>
								</c:if>
                                
                            </div>
                            <!--/ 하단 버튼 -->
                        </div>
                        <!--/ 콘텐츠 본문 -->
                    </div>
                </div>
            </div>
        </section>
        <!--/ 본문 -->