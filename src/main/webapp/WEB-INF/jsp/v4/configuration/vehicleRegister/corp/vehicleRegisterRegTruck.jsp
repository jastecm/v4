<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
	$(function() {
		$("#start-date").datepicker({});
        $("#end-date").datepicker({});
        $("#start-date").on("change",function(){
        	if($(this).val()){
        		var v = $(this).datepicker( "getDate" ).getTime();
        		$("#vehicleRegDate").val(v);
        	}else{
        		$("#vehicleRegDate").val("");
        	}
        });
        $("#end-date").on("change",function(){
        	if($(this).val()){
        		var v = $(this).datepicker( "getDate" ).getTime();
        		$("#vehicleMaintenanceDate").val(v);
        	}else{
        		$("#vehicleMaintenanceDate").val("");
        	}
        });
        
		$("#truckRegisterBtn").on("click",function(){
			
        	var v = $('#frm').serializeObject(1);
        	console.log(v);
        	return;
        	if(valid(v)) {
	        	$V4.http_post("/api/v1/truck",$('#frm').serializeObject(1),{
	        		header : {key : "${_KEY}"}
	    	    	,success : function(rtv){
	    	    		console.log(rtv.result);
	    	    		alert('저장되었습니다.');
	    	    		$V4.move("/config/vehicleRegister/list");
	    	    	}
	    	    });
        	}
        });
		
		$('#truckCancle').on('click',function(){
			window.history.back();
		});
		
		var commonPopDefault = function(arrSelect,inputObj,valueObj){
			$.each(arrSelect,function(){
				var info = base64ToJsonObj($(this).data("info"));
				$(inputObj).val(info.name);
				$(valueObj).val(info.accountKey);
			});
		}
		
		
		$("#btn_defaultAccountPop").data({
			type : "user"
			,http_post_option : {header : {key : "${_KEY}"}}
			,selType : "radio"
			,action :{
				ok : commonPopDefault
			}
			,targetInput : $("#defaultAccountText")
			,targetInputValue : $("#defaultAccountKey")
			,debugMode : true
		}).commonPop();
		
	});
	
	function valid(v){
		
		if( $V4.required(v.plateNum , "차량번호는 필수입니다." , $('#plateNum')) ) return false;
		if( $V4.required(v.totDistance , "현재 주행거리는 필수입니다." , $('#totDistance')) ) return false;
		
		return true;
		
	}
</script>
 <section id="container">
 	<form id="frm" onSubmit="return false;">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>차량등록</h2>
                    <span>효율적인 차량관리를 위해 차량을 조회 ㆍ등록관리 합니다. </span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                
                <div id="contents-page" class="setting-page">
                    <div class="box-layout">
                        <div class="title">
                            <h3 class="tit3">차량ㆍ단말기 연동 등록</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <!--
							class="car" 가 들어간 경우 : 구분 - 승용에만 노출
							class="special" 이 들어간 경우 : 구분 - 승합/트럭/특수차량에만 노출 됩니다. 
						-->
                        <div class="contents-wrap">
                            <h3 class="tit2">차량 기본정보</h3>
                            <table class="table mgt20">
                                <caption>차량 기본정보</caption>
                                <colgroup>
                                    <col style="width:16.35%" />
                                    <col style="width:34.65%" />
                                    <col style="width:16.35%" />
                                    <col style="width:34.65%" />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <th scope="row">구분</th>
                                        <td colspan="3">
                                         	트럭
                                        </td>
                                    </tr>
                                    <tr class="car">
                                        <th scope="row">차량번호</th>
                                        <td>
                                            <input type="text" id="plateNum" name="plateNum"/>
                                        </td>
                                        <th scope="row">현재 주행거리</th>
                                        <td>
	                                        <div class="text-end">
	                                            <input type="text" class="numberOnly" id="totDistance" name="totDistance"/>
	                                            <span class="text">(km)</span>
	                                        </div>
                                        </td>
                                    </tr>
                                    
                                </tbody>
                            </table>

                            <h3 class="tit2 mgt50">차량 부가정보</h3>
                            <table class="table mgt20">
                                <caption>차량 부가정보</caption>
                                <colgroup>
                                    <col style="width:16.35%" />
                                    <col style="width:34.65%" />
                                    <col style="width:16.35%" />
                                    <col style="width:34.65%" />
                                </colgroup>
                                <tbody>
                                	<tr>
                                        <th scope="row" class="help">단말기 일련번호
                                        	<div class="layer">
                                        		버스와 상용차량은 전문설치기사가 OBD설치작업을 진행합니다. OBD 일련번호는 설치 전,후 관리자에 의해 배정되며, 이후 선택해주세요.
                                        	</div>
                                        </th>
                                        <td>
                                            <select name="deviceSeries">
												<option value="">선택</option>
												<option value="von-S31">von-S31</option>
												<option value="VC-200">VC-200</option>
											</select>
                                            <input type="text" name="deviceSn" id="deviceSn"/>
                                        </td>
                                        <th scope="row" class="help">기본 운전자
                                    		<div class="layer">
                                    			<b>기본 운전자</b> 미배차운행 등 운전자를 특정하지 못할때 해당 사용자를 운전자로 간주합니다.
                                    		</div>
                                    	</th>
                                        <td>
                                           <input type="text" id="defaultAccountText" style="width:76%"/>
                                            <button type="button" class="btn btn02 md commonPop" id="btn_defaultAccountPop">검색</button>
                                            <input type="hidden" name="defaultAccountKey" id="defaultAccountKey" />
                                        </td>
                                    </tr>
                                	<tr>
                                        <th scope="row">제조사</th>
                                        <td>
                                            <!-- 승용 제외 -->
                                            <input type="text" class="special" id="manufacture" name="manufacture"/>
                                            <!-- 승용 제외 -->
                                        </td>
                                        <th scope="row">차종</th>
                                        <td>
                                            <!-- 승용 제외 -->
                                            <input type="text" class="special" id="modelMaster" name="modelMaster"/>
                                            <!-- 승용 제외 -->
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">연식</th>
                                        <td>
                                            <!-- 승용 제외 -->
                                            <input type="text" class="special" id="year" name="year"/>
                                            <!-- 승용 제외 -->
                                        </td>
                                        <th scope="row">모델명</th>
                                        <td>
                                            <!-- 승용 제외 -->
                                            <input type="text" class="special" id="modelHeader" name="modelHeader"/>
                                            <!-- 승용 제외 -->
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">배기량</th>
                                        <td>
                                            <!-- 승용 제외 -->
                                            <input type="text" class="special" id="volume" name="volume"/>
                                            <!-- 승용 제외 -->
                                        </td>
                                        <th scope="row">기어방식</th>
                                        <td>
                                            <!-- 승용 제외 -->
                                            <input type="text" class="special" id="transmission" name="transmission"/>
                                            <!-- 승용 제외 -->
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">연료</th>
                                        <td>
                                            <input type="text" class="special" id="fuelType" name="fuelType"/>
                                        </td>
                                        <th scope="row" class="special">원동기형식</th>
                                        <td class="special">
                                            <input type="text" id="specifications" name="specifications"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">소유구분</th>
                                        <td colspan="3">
                                            <input type="radio" id="vehicleBizType1" value="1" name="vehicleBizType" checked="checked"/> 소유&nbsp;&nbsp;&nbsp;
                                            <input type="radio" id="vehicleBizType2" value="2" name="vehicleBizType"/> 리스&nbsp;&nbsp;&nbsp;
                                            <input type="radio" id="vehicleBizType3" value="3" name="vehicleBizType"/> 렌터&nbsp;&nbsp;&nbsp;
                                            <input type="radio" id="vehicleBizType4" value="4" name="vehicleBizType"/> 협력&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">차량등록일</th>
                                        <td>
                                            <input type="text" id="start-date" />
                                            <input type="hidden" id="vehicleRegDate" name="vehicleRegDate" />
                                        </td>
                                        <th scope="row">차대번호</th>
                                        <td>
                                            <input type="text" id="vehicleBodyNum" name="vehicleBodyNum"/>       
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">차량검사 만기일</th>
                                        <td>
                                            <input type="text" id="end-date" />
                                            <input type="hidden" id="vehicleMaintenanceDate" name="vehicleMaintenanceDate" />
                                        </td>
                                        <th scope="row" class="help">고정 임차료
                                        	<div class="layer">
                                        		도급료를 입력해주세요.
                                        	</div>
                                        </th>
                                        <td>
                                            <div class="text-end">
                                                <input type="text" class="numberOnly" id="vehiclePayment" name="vehiclePayment" />
                                                <span class="text">(원)</span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">비고</th>
                                        <td colspan="3">
                                           <input type="text" name="vehicleDescript"/>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- 하단 버튼 -->
                            <div class="btn-bottom">
                                <button type="button" class="btn btn02" id="truckCancle">취소</button>
                                <button type="button" class="btn btn03" id="truckRegisterBtn">등록완료</button>
                            </div>
                            <!--/ 하단 버튼 -->
                        </div>
                    </div>
                </div>
            </div>
            </form>
            <!--/ 콘텐츠 본문 -->
        </section>