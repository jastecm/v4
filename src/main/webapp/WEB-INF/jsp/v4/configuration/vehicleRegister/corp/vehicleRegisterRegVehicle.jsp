<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
	
	$(function() {
		var commonPopDefault = function(arrSelect,inputObj,valueObj){
			$.each(arrSelect,function(){
				var info = base64ToJsonObj($(this).data("info"));
				$(inputObj).val(info.name);
				$(valueObj).val(info.accountKey);
			});
		}
		
		var commonUserDefault = {
			type : "user"
			,http_post_option : {header : {key : "${_KEY}"}}
			,selType : "checkbox"
			,action :{					
				ok : commonPopDefault 
			}
			,debugMode : true
		}
		
		$("#btn_commonGroupPop").data({
			type : "group"
			,http_post_option : {header : {key : "${_KEY}"}}
			,selType : "checkbox"
			,action :{				
				ok : function(arrSelect,inputObj,valueObj){
					$.each(arrSelect,function(i){
						var info = base64ToJsonObj($(this).data("info"));
						var strGroupNm = "";
						if(info.groupNm)
							strGroupNm = info.parentGroupNm+"/"+info.groupNm;	
						else strGroupNm = info.parentGroupNm;
						if(i == 0){
							$(inputObj).val(strGroupNm);
							$(valueObj).val(info.groupKey);	
						}else{
							$(inputObj).val($(inputObj).val()+","+strGroupNm);
							$(valueObj).val($(valueObj).val()+","+info.groupKey);
						}
						
					});
				}
			}
			,targetInput : $("#cropGroupNm") //enter search event 대상 , ok.inputObj 에 해당
			,targetInputValue : $("#corpGroupKey") //ok.valueObj 에 해당
			,debugMode : true //ajax load data console에 찍기 - TODO - commonList 에도 추가해야지~
		}).commonPop();
		  
		
		
		//commonUserDefault.targetInput = $("#fiexedAccountText");
		//commonUserDefault.targetInputValue = $("#fixedAccountKey");
		//$("#btn_fixedAccountPop").data(commonUserDefault).commonPop();
		
		//commonUserDefault.targetInput = $("#defaultAccountText");
		//commonUserDefault.targetInputValue = $("#defaultAccountKey");
		//$("#btn_defaultAccountPop").data(commonUserDefault).commonPop();
		
		//죨라 똑같은코드 반복이라면 공통 data 만들어놓고 target만 바꿔서 만들기 
		
		
		$("#btn_fixedAccountPop").data({
			type : "user"
			,http_post_option : {header : {key : "${_KEY}"}}
			,selType : "radio"
			,action :{
				ok : commonPopDefault //여러곳에 똑같은 선택액션은 이렇게써도 ok
			}
			,targetInput : $("#fiexedAccountText") //enter search event 대상 , ok.inputObj 에 해당
			,targetInputValue : $("#fixedAccountKey") //ok.valueObj 에 해당
			,debugMode : true 
		}).commonPop();
		
		$("#btn_defaultAccountPop").data({
			type : "user"
			,http_post_option : {header : {key : "${_KEY}"}}
			,selType : "radio"
			,action :{
				ok : commonPopDefault
			}
			,targetInput : $("#defaultAccountText")
			,targetInputValue : $("#defaultAccountKey")
			,debugMode : true
		}).commonPop();
		
		$("#btn_fixedDriverPop").data({
			type : "user"
			,http_post_option : {header : {key : "${_KEY}"}}
			,selType : "radio"
			,action :{
				ok : commonPopDefault //여러곳에 똑같은 선택액션은 이렇게써도 ok
			}
			,targetInput : $("#fixedDriverText") 
			,targetInputValue : $("#fixedDriverKey")
			,debugMode : true
		}).commonPop(); //바로 써도 ok
		
		$("#btn_approvalAccountPop").data({
			type : "user"
			,http_post_option : {header : {key : "${_KEY}"}}
			,selType : "radio"
			,action :{
				ok : commonPopDefault //여러곳에 똑같은 선택액션은 이렇게써도 ok
			}
			,targetInput : $("#approvalAccountText") 
			,targetInputValue : $("#approvalAccountKey")
			,debugMode : true
		}).commonPop();
		
        $("#start-date").datepicker({});
        $("#end-date").datepicker({});
        $("#register-complete-dialog").dialog({
            autoOpen: false,
            show: {
                duration: 500
            },
            width: '800',
            modal: true
        });
        $("#register-complete-open").on("click", function() {
        	
        	var v = $('#frm').serializeObject(1);
			if(valid(v)) $("#register-complete-dialog").dialog("open"); 
            
        });
        
        loadingVehicleDb();
        
        $("#start-date").on("change",function(){
        	if($(this).val()){
        		var v = $(this).datepicker( "getDate" ).getTime();
        		$("#vehicleRegDate").val(v);
        	}else{
        		$("#vehicleRegDate").val("");
        	}
        });
        $("#end-date").on("change",function(){
        	if($(this).val()){
        		var v = $(this).datepicker( "getDate" ).getTime();
        		$("#vehicleMaintenanceDate").val(v);
        	}else{
        		$("#vehicleMaintenanceDate").val("");
        	}
        });

		// 지정배차 유무
        $("#fixedTable").hide();
        $("input[name='fixed']").on("change",function() {
			var check = $('input[name="fixed"]:checked').val();
        	
        	if(check == "1"){
        		$('#fixedTable').show();
        		$('#defaultDrvier').hide()
        	}else{
        		$('#fixedTable').hide();
        		$('#defaultDrvier').show()
        	}
        	
        });
        
        
        //$('#autoReportTable').hide();
        $('input[name="autoReport"]').on("change",function(){
        	/*
        	var check = $('input[name="autoReport"]:checked').val()
        	
        	if(check == "1"){
        		$('#autoReportTable').show();		
        	}else{
        		$('#autoReportTable').hide();
        	}
        	*/
        });
        
        $('#approvalTable').hide();
        $('input[name="approvalType"]').on("change",function(){
        	var check = $('input[name="approvalType"]:checked').val()
        	
        	if(check == "1"){
        		$('#approvalTable').show();		
        	}else{
        		$('#approvalTable').hide();
        	}
        });
        
        $("#btn_registVehicle").on("click",function(){
        	var v = $('#frm').serializeObject(1);
        	
        	if(valid(v)) {
        		
            	$V4.http_post("/api/v1/vehicle",v,{
            		header : {key : "${_KEY}"}
        	    	,success : function(rtv){
        	    		alert("저장되었습니다.");
        	    		$V4.move("/config/vehicleRegister/list");
        	    		
        	    	}
        	    });
        	}
        });
		$("#btn_registVehicleTemp").on("click",function(){
			var v = $('#frm').serializeObject(1);
        	
        	if(valid(v)) {
        		
            	$V4.http_post("/api/v1/vehicleTemp",v,{
            		header : {key : "${_KEY}"}
        	    	,success : function(rtv){
        	    		alert("저장되었습니다.");
        	    		//alert("단말 pc연동 화면으로 이동"); //rtv.result 가지고 이동, 해당화면에서 rtv.result 링크생성
        	    		$V4.move("/config/vehicleRegister/common/pcManager");
        	    	}
        	    });
        	}
        });
	});
	
	function valid(v){
		if( $V4.required(v.deviceSn , "단말기 일련번호는 필수입니다." , $('#deviceSn')) ) return false;
		if( $V4.required(v.plateNum , "차량번호는 필수입니다." , $('#plateNum')) ) return false;
		if( $V4.required(v.totDistance , "현재 주행거리는 필수입니다." , $('#totDistance')) ) return false;
		
		if( !v.convCode){
			var c = $("#manufacture").val();
			if( $V4.required(c , "제조사는 필수입니다." , $('#manufacture')) ) return false;
			var c = $("#modelMaster").val();
			if( $V4.required(c , "차종은 필수입니다." , $('#modelMaster')) ) return false;
			var c = $("#year").val();
			if( $V4.required(c , "연식은 필수입니다." , $('#year')) ) return false;
			var c = $("#modelHeader").val();
			if( $V4.required(c , "모델명은 필수입니다." , $('#modelHeader')) ) return false;
			var c = $("#volume").val();
			if( $V4.required(c , "배기량은 필수입니다." , $('#volume')) ) return false;
			var c = $("#fuelType").val();
			if( $V4.required(c , "유종은 필수입니다." , $('#fuelType')) ) return false;
			var c = $("#transmission").val();
			if( $V4.required(c , "기어방식은 필수입니다." , $('#transmission')) ) return false;
			
			alert("차량 모델선택이 잘못되었습니다.");
			return false;
		}
		
		if ( v.fixed=='1' && $V4.required(v.fixedAccountKey , "지정배차 소유자가 선택되지 않았습니다." , $('#fiexedAccountText'))) return false;
		if ( v.fixed=='1' && $V4.required(v.fixedDriverKey , "지정배차 수행기사가 선택되지 않았습니다." , $('#fixedDriverText'))) return false;
		if ( v.autoReport=='1' && $V4.required(v.autoReportContents , "자동운행일지 업무내용이 입력되지 않았습니다." , $('#autoReportContents'))) return false;
		if ( v.approvalType=='1' && $V4.required(v.approvalAccountKey , "별도 결재 설정 결재자가 선택되지 않았습니다." , $('#approvalAccountText'))) return false;
		
		return true;
		
		
	}
	
	function loadingVehicleDb(){
    	var init = {};
    	init.manufacture = "${reqVo.manufacture}"?"${reqVo.manufacture}":"_";
    	init.modelMaster = "${reqVo.modelMaster}"?"${reqVo.modelMaster}":"_";
    	init.year = "${reqVo.year}"?"${reqVo.year}":"_";
    	init.modelHeader = "${reqVo.modelHeader}"?"${reqVo.modelHeader}":"_";
    	init.fuelType = "${reqVo.fuelType}"?"${reqVo.fuelType}":"_";
    	init.transmission = "${reqVo.transmission}"?"${reqVo.transmission}":"_";
    	init.volume = "${reqVo.volume}"?"${reqVo.volume}":"_";
    	
    	//function(url,param,target,listType,defaultVal,initVal,trigger,afterFunk,errorFunk)
    	$V4.ajaxCreateApiSelect ("/api/vehicleDb/ko/manufacture"
    			,{}
    			,$("#manufacture")
    			,1
    			,"선택"
    			,init.manufacture
    			,null
    			,null
    			,null);
    	
    	$("#manufacture").on("change",function(){
    		$("#convCode").val("");
    		var _$this = $(this);
    		var lev = _$this.data("lev");
    		
    		$(".vehicleList").each(function(){
    			var thisLev = $(this).data("lev");
    			if(lev < thisLev) $(this).html("<option value=''>선택</option>");
    		});
    		
    		if(_$this.val())
    		$V4.ajaxCreateApiSelect ("/api/vehicleDb/ko/manufacture/"+_$this.val()+"/modelMaster"
    				,{}
    				,$("#modelMaster")
    				,1
    				,"선택"
    				,""
    				,null
    				,null
    				,null);
    		
    	});
    	
    	$V4.ajaxCreateApiSelect ("/api/vehicleDb/ko/manufacture/"+init.manufacture+"/modelMaster"
    			,{}
    			,$("#modelMaster")
    			,1
    			,"선택"
    			,init.modelMaster
    			,"ko"
    			,null
    			,null);
    	
    	$("#modelMaster").on("change",function(){
    		$("#convCode").val("");
    		var _$this = $(this);
    		var lev = _$this.data("lev");
    		
    		$(".vehicleList").each(function(){
    			var thisLev = $(this).data("lev");
    			if(lev < thisLev) $(this).html("<option value=''>선택</option>");
    		});
    		
    		if(_$this.val())
    		$V4.ajaxCreateApiSelect ("/api/vehicleDb/ko/manufacture/"+$("#manufacture").val()+"/modelMaster/"+_$this.val()+"/year"
    				,{}
    				,$("#year")
    				,1
    				,"선택"
    				,""    				
    				,null
    				,null
    				,null);
    		
    	});
    	
    	$V4.ajaxCreateApiSelect ("/api/vehicleDb/ko/manufacture/"+init.manufacture+"/modelMaster/"+init.modelMaster+"/year"
    			,{}
    			,$("#year")
    			,1
    			,"선택"
    			,init.year
    			,null
    			,null
    			,null);
    	
    	$("#year").on("change",function(){
    		$("#convCode").val("");
    		var _$this = $(this);
    		var lev = _$this.data("lev");
    		
    		$(".vehicleList").each(function(){
    			var thisLev = $(this).data("lev");
    			if(lev < thisLev) $(this).html("<option value=''>선택</option>");
    		});
    		
    		if(_$this.val())
    		$V4.ajaxCreateApiSelect ("/api/vehicleDb/ko/manufacture/"+$("#manufacture").val()+"/modelMaster/"+$("#modelMaster").val()+"/year/"+_$this.val()+"/modelHeader"
    				,{}
    				,$("#modelHeader")
    				,1
    				,"선택"
    				,""
    				,null
    				,null
    				,null);
    		
    	});
    	
    	$V4.ajaxCreateApiSelect ("/api/vehicleDb/ko/manufacture/"+init.manufacture+"/modelMaster/"+init.modelMaster+"/year/"+init.year+"/modelHeader"
    			,{}
    			,$("#modelHeader")
    			,1
    			,"선택"
    			,init.modelHeader
    			,null
    			,null
    			,null);
    	
    	$("#modelHeader").on("change",function(){
    		$("#convCode").val("");
    		var _$this = $(this);
    		var lev = _$this.data("lev");
    		
    		$(".vehicleList").each(function(){
    			var thisLev = $(this).data("lev");
    			if(lev < thisLev) $(this).html("<option value=''>선택</option>");
    		});
    		
    		if(_$this.val())
    		$V4.ajaxCreateApiSelect ("/api/vehicleDb/ko/manufacture/"+$("#manufacture").val()+"/modelMaster/"+$("#modelMaster").val()+"/year/"+$("#year").val()+"/modelHeader/"+_$this.val()+"/fuelType"
    				,{}
    				,$("#fuelType")
    				,1
    				,"선택"
    				,""
    				,null
    				,null
    				,null);
    		
    		if(_$this.val())
    		$V4.ajaxCreateApiSelect ("/api/vehicleDb/ko/manufacture/"+$("#manufacture").val()+"/modelMaster/"+$("#modelMaster").val()+"/year/"+$("#year").val()+"/modelHeader/"+_$this.val()+"/volume"
    				,{}
    				,$("#volume")
    				,1
    				,"선택"
    				,""
    				,null
    				,null
    				,null);
    		
    		if(_$this.val())
    		$V4.ajaxCreateApiSelect ("/api/vehicleDb/ko/manufacture/"+$("#manufacture").val()+"/modelMaster/"+$("#modelMaster").val()+"/year/"+$("#year").val()+"/modelHeader/"+_$this.val()+"/transmission"
    				,{}
    				,$("#transmission")
    				,1
    				,"선택"
    				,""
    				,null
    				,null
    				,null);
    		
    	});
    	
    	$V4.ajaxCreateApiSelect ("/api/vehicleDb/ko/manufacture/"+init.manufacture+"/modelMaster/"+init.modelMaster+"/year/"+init.year+"/modelHeader/"+init.modelHeader+"/fuelType"
    			,{}
    			,$("#fuelType")
    			,1
    			,"선택"
    			,init.fuelType
    			,null
    			,null
    			,null);
    	$V4.ajaxCreateApiSelect ("/api/vehicleDb/ko/manufacture/"+init.manufacture+"/modelMaster/"+init.modelMaster+"/year/"+init.year+"/modelHeader/"+init.modelHeader+"/volume"
    			,{}
    			,$("#volume")
    			,1
    			,"선택"
    			,init.volume
    			,null
    			,null
    			,null);
    	$V4.ajaxCreateApiSelect ("/api/vehicleDb/ko/manufacture/"+init.manufacture+"/modelMaster/"+init.modelMaster+"/year/"+init.year+"/modelHeader/"+init.modelHeader+"/transmission"
    			,{}
    			,$("#transmission")
    			,1
    			,"선택"
    			,init.transmission
    			,null
    			,null
    			,null);
    	
    	$("#fuelType,#volume,#transmission").on("change",function(){
    		$("#convCode").val("");
    		var allchk = true;
    		$(".vehicleList").each(function(){
    			
    			if(!$(this).val()) {				
    				allchk = false;			
    			}
    		});
    		
    		if(allchk){
    			$V4.http_post("/api/vehicleDbCode/ko/"+$("#manufacture").val()+"/"+$("#modelMaster").val()+"/"+$("#year").val()+"/"+$("#modelHeader").val()+"/"+$("#fuelType").val()+"/"+$("#transmission").val()+"/"+$("#volume").val()+""
    					,{}
    					,{
    						success : function(r){
    							var obj = r.result;
    							$("#convCode").val(obj.convCode);
    						}
    					});
    			
    		}
    	});
    }
</script>
 <section id="container">
 			<form id="frm" onSubmit="return false;">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>차량등록</h2>
                    <span>효율적인 차량관리를 위해 차량을 조회 ㆍ등록관리 합니다. </span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="setting-page">
                    <div class="box-layout">
                        <div class="title">
                            <h3 class="tit3">차량ㆍ단말기 연동 등록</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <!--
							class="car" 가 들어간 경우 : 구분 - 승용에만 노출
							class="special" 이 들어간 경우 : 구분 - 승합/트럭/특수차량에만 노출 됩니다. 
						-->
                        <div class="contents-wrap">
                        
                        	<!-- 프로세스 표시 (승용)-->
                            <div class="register-step-box mgt50 car">
                                <ol>
                                    <li class="active">차량 등록</li>
                                    <li>단말기 등록</li>
                                </ol>
                            </div>
                            <!--/ 프로세스 표시 (승용) -->
                        
                            <h3 class="tit2 mgt50">차량 기본정보</h3>
                            <table class="table mgt20">
                                <caption>차량 기본정보</caption>
                                <colgroup>
                                    <col style="width:16.35%" />
                                    <col style="width:34.65%" />
                                    <col style="width:16.35%" />
                                    <col style="width:34.65%" />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <th scope="row">구분</th>
                                        <td>
                                           	승용
                                        </td>
                                        <th scope="row">단말기 일련번호</th>
                                        <td>
                                            <select name="deviceSeries">
												<option value="von-S31">von-S31</option>
												<option value="VC-200">VC-200</option>
											</select>
                                            <input type="text" name="deviceSn" id="deviceSn"/>
                                        </td>
                                    </tr>
                                    <tr class="car">
                                        <th scope="row">차량번호</th>
                                        <td>
                                            <input type="text" id="plateNum" name="plateNum"/>
                                        </td>
                                        <th scope="row">현재 주행거리</th>
                                        <td>
                                            <input type="text" id="totDistance" name="totDistance"/> (km)
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">제조사</th>
                                        <td>
                                            <select id="manufacture" data-lev='1' class="vehicleList">
												<option value="">선택</option>
											</select>
                                        </td>
                                        <th scope="row">차종</th>
                                        <td>
                                            <select id="modelMaster" data-lev='2' class="vehicleList">
												<option value="">선택</option>
											</select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">연식</th>
                                        <td>
                                            <select id="year" data-lev='3' class="vehicleList">
												<option value="">선택</option>
											</select>
                                        </td>
                                        <th scope="row">모델명</th>
                                        <td>
                                            <select id="modelHeader" data-lev='4' class="vehicleList">
												<option value="">선택</option>
											</select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">배기량</th>
                                        <td>
                                            <select id="volume" data-lev='5' class="vehicleList">
												<option value="">선택</option>
											</select>
                                        </td>
                                        <th scope="row">유종</th>
                                        <td>
                                            <select id="transmission" data-lev='5' class="vehicleList">
												<option value="">선택</option>
											</select>
                                        </td>
                                    </tr>
                                    <tr class="car">
                                        <th scope="row">기어방식</th>
                                        <td  scope="row">
                                            <select id="fuelType" data-lev='5' class="vehicleList">
												<option value="">선택</option>
											</select>
                                        </td>
                                    	<th scope="row" class="help">위치정보 설정
                                    		<div class="layer">
                                    			<b>위치정보 설정</b> OFF시 GPS위치 정보를 수집하지 않습니다.
                                    		</div>
                                    	</th>
                                        <td>
                                           	<input type="radio" name="locationSetting" value="1" checked="checked" /> ON&nbsp;&nbsp;&nbsp;
                                            <input type="radio" name="locationSetting" value="0" /> OFF&nbsp;&nbsp;&nbsp;  
                                        </td>
                                    </tr>
                                    <!--/ 승용 -->
                                </tbody>
                            </table>
                            <input type="hidden" id="convCode" name="convCode">
                            <h3 class="tit2 mgt50">차량 부가정보</h3>
                            <table class="table mgt20">
                                <caption>차량 부가정보</caption>
                                <colgroup>
                                    <col style="width:16.35%" />
                                    <col style="width:34.65%" />
                                    <col style="width:16.35%" />
                                    <col style="width:34.65%" />
                                </colgroup>
                                <tbody>
                                	<tr>
                                        <th scope="row" class="help">차량소속
                                        	<div class="layer">
                                    			<b>차량 소속</b> 차량 소속을 설정할경우 해당 소속 사람만 사용 및 조회 가능합니다.(미선택시 모두가 사용/조회 가능한 공용차량) 또한 첫번째 등록한 소속의 팀장이 각종 결제의 결제권자가 됩니다.
                                    		</div>
                                        </th>
                                        <td colspan="3">
                                            <input type="text" id="cropGroupNm" style="width: 90%;"/>
                                            <button type="button" class="btn btn02 md" id="btn_commonGroupPop">검색</button>
                                            <input type="hidden" name="corpGroupKey" id="corpGroupKey" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">소유구분</th>
                                        <td>
                                            <input type="radio" id="vehicleBizType1" value="1" name="vehicleBizType" checked="checked"/> 소유&nbsp;&nbsp;&nbsp;
                                            <input type="radio" id="vehicleBizType2" value="2" name="vehicleBizType"/> 리스&nbsp;&nbsp;&nbsp;
                                            <input type="radio" id="vehicleBizType3" value="3" name="vehicleBizType"/> 렌터&nbsp;&nbsp;&nbsp;
                                            <input type="radio" id="vehicleBizType4" value="4" name="vehicleBizType"/> 개인&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <th scope="row" class="help car">
                                            	휴일운행 알림
                                            <div class="layer">
                                                <b>휴일운행 알림</b> : 주간 52시간 근무제 도입 등으로 휴일 기업 차량운행을 근무시간으로 인정사례가 있어 휴일 운행 유무를 운행기록에서 알려주는 기능입니다.
                                            </div>
                                        </th>
                                        <td class="car">
                                            <input type="radio" name="holidayPass" checked="checked" value="1"/> ON&nbsp;&nbsp;&nbsp;  
                                            <input type="radio" name="holidayPass" value="0"/> OFF
                                        </td>  
                                     </tr>
                                    <tr>
                                        <th scope="row">차량등록일</th>
                                        <td>
                                            <input type="text" id="start-date" />
                                            <input type="hidden" id="vehicleRegDate" name="vehicleRegDate" />
                                        </td>
                                        <th scope="row">차대번호</th>
                                        <td>
                                            <input type="text" id="vehicleBodyNum" name="vehicleBodyNum"/>                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">차량검사 만기일</th>
                                        <td>
                                            <input type="text" id="end-date" />
                                            <input type="hidden" id="vehicleMaintenanceDate" name="vehicleMaintenanceDate" />
                                        </td>
                                        
                                        <th scope="row" class="help">고정 임차료
                                        	<div class="layer">
                                        		리스 또는 렌트비용을 입력해주세요.
                                        	</div>
                                        </th>
                                        <td>
                                            <div class="text-end">
                                                <input type="text" class="numberOnly" id="vehiclePayment" name="vehiclePayment" />
                                                <span class="text">(원)</span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">보험사명</th>
                                        <td>
                                            <input type="text" name="vehicleInsureNm"/>
                                        </td>
                                        <th scope="row">긴급출동 연락처</th>
                                        <td>
                                            <input type="text" name="vehicleInsurePhone"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">차량색상</th>
                                        <td>
                                            <input type="text" name="color"/>
                                        </td>
                                        <th scope="row">비고</th>
                                        <td>
                                            <input type="text" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row" class="help">자동 1차결제
                                        	<div class="layer">
                                        		<b>자동 1차결재</b> 결재 시스템 사용시 1차결재(기본값 : 차량 관리자)를 자동으로 결재승인 합니다.
                                        	</div>
                                        </th>
                                        <td colspan="3">
                                            <input type="radio" id="approvalStep1Pass1" value="1" name="approvalStep1Pass" checked="checked"/> ON&nbsp;&nbsp;&nbsp;
                                            <input type="radio" id="approvalStep1Pass2" value="0" name="approvalStep1Pass"/> OFF&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            
                            <h3 class="tit2 mgt50">자동운행일지 설정</h3>
                            <div class="select-mileage">
                                <input type="radio" id="autoReport01" name="autoReport" onclick="" value="1"/>
                                <label for="autoReport01">ON</label> &nbsp;&nbsp;&nbsp;
                                <input type="radio" id="autoReport02" name="autoReport" onclick="" value="0" checked="checked" />
                                <label for="autoReport02">OFF</label>
                            </div>
                            <table id="autoReportTable" class="table mgt20">
                                <caption>자동운행일지</caption>
                                <colgroup>
                                    <col style="width:16.35%" />
                                    <col style="width:34.65%" />
                                    <col style="width:16.35%" />
                                    <col style="width:34.65%" />
                                </colgroup>
                                <tbody>
                                   <tr>
                                      <th scope="row" class="help">
                                          자동운행일지
                                          <div class="layer">
                                                <b>자동운행일지</b> : 설정된 내용으로 자동으로 운행일지를 작성합니다. 
                                          </div>
                                      </th>
                                      <td colspan="3">
                                          <select name="autoReportPurpose">
											<option value="1">업무용</option>
											<option value="2">비업무용</option>
											<option value="3">출퇴근용</option>
										  </select>
                                          <input type="text" placeholder="특정 업무내용 입력" name="autoReportContents" id="autoReportContents"/>
                                      </td>
                                  </tr>
                            </table>
                            <table id="defaultDrvier" class="table mgt20">
								<caption>지정배차 설정</caption>
								<colgroup>
                                    <col style="width:16.35%" />
                                    <col style="width:34.65%" />
                                    <col style="width:16.35%" />
                                    <col style="width:34.65%" />
                                </colgroup>
                                <tbody>
                                    <tr>
                                    	<th scope="row" class="help">기본 운전자
                                    		<div class="layer">
                                    			<b>기본 운전자</b> 미배차운행 등 운전자를 특정하지 못할때 해당 사용자를 운전자로 간주합니다.
                                    		</div>
                                    	</th>
                                        <td colspan="3">
                                            <input type="text" id="defaultAccountText" style="width:88%"/>
                                            <button type="button" class="btn btn01 md commonPop" id="btn_defaultAccountPop">검색</button>
                                            <input type="hidden" name="defaultAccountKey" id="defaultAccountKey" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            
                            
                            <h3 class="tit2 mgt50">지정배차 설정</h3>
                            <div class="select-mileage">
								<input type="radio" id="fixed1" name="fixed" onclick="" value="1"/>
	                            <label for="fixed1">ON</label> &nbsp;&nbsp;&nbsp;
	                            <input type="radio" id="fixed2" name="fixed" onclick="" value="0" checked="checked" />
	                            <label for="fixed2">OFF</label>
	                        </div>
							<table id="fixedTable" class="table mgt20">
								<caption>지정배차 설정</caption>
								<colgroup>
                                    <col style="width:16.35%" />
                                    <col style="width:34.65%" />
                                    <col style="width:16.35%" />
                                    <col style="width:34.65%" />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <th scope="row">소유자</th>
                                        <td colspan="3">
                                            <input type="text" id="fiexedAccountText" style="width: 88%;"/>
                                            <button type="button" class="btn btn01 md commonPop" id="btn_fixedAccountPop">검색</button>
                                            <input type="hidden" id="fixedAccountKey" name="fixedAccountKey"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">수행기사</th>
                                        <td colspan="3">
                                            <input type="text" id="fixedDriverText" style="width: 88%;" />
                                            <button type="button" class="btn btn01 md commonPop" id="btn_fixedDriverPop">검색</button>
                                            <input type="hidden" id="fixedDriverKey" name="fixedDriverKey"/>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            
                            <h3 class="tit2 mgt50">별도결재 설정</h3>
                            <div class="select-mileage">
                                <input type="radio" id="approvalType1" name="approvalType" onclick="" value="1"/>
                                <label for="approvalType1">ON</label> &nbsp;&nbsp;&nbsp;
                                <input type="radio" id="approvalType2" name="approvalType" onclick="" value="0" checked="checked" />
                                <label for="approvalType2">OFF</label>
                                
                                <table id="approvalTable" class="table mgt20">
                                <caption>별도결재 설정</caption>
                                <colgroup>
                                    <col style="width:16.35%" />
                                    <col style="width:34.65%" />
                                    <col style="width:16.35%" />
                                    <col style="width:34.65%" />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <th scope="row" class="help">
                                            별도결재 설정
                                            <div class="layer">
                                                <b>별도 결재설정</b> : 배차신청시 환경설정 > 결재관리를 받지 않고 부서별로 차량 지정, 부서별 별도 관리자를 지정하여 관리하실 수 있습니다.
                                            </div>
                                        </th>
                                        <td colspan="3">
                                            <input type="text" id="approvalAccountText" style="width: 90%;"/>
                                            <button type="button" class="btn btn01 md" id="btn_approvalAccountPop">검색</button>
                                            <input type="hidden" id="approvalAccountKey" name="approvalAccountKey"/>
                                        </td>
                                    </tr>
                            </table>
                            </div>
                            

                            <!-- 하단 버튼 -->
                            <div class="btn-bottom">
                                <button type="button" class="btn btn02">취소</button>
                                <button type="button" class="btn btn03" id="register-complete-open">등록완료</button>
                            </div>
                            <!--/ 하단 버튼 -->
                        </div>
                    </div>
                </div>
            </div>
            <!--/ 콘텐츠 본문 -->
            </form>
        </section>
        
        <!-- dialog -->  
      <div id="register-complete-dialog" title="단말기 등록방법 선택">
        <div class="txt">
            단말기 펌웨어의 차량데이터를 등록해야 정상적으로 서비스 이용이 가능합니다.
            <br />아래 등록방법을 선택해 주세요.
            <ul>
                <li>PC매니저 : PC에 단말기를 연락하여 펌웨어를 업데이트 합니다.
                    <br />약 1분 정도 소요됩니다.</li>
                <li>App을 통한 차량등록 : VIewCAR APP &gt; 업데이트를 통해 블루투스로 등록하고 통상 5분정도 소요됩니다.</li>
            </ul>

        </div>

        <!-- 하단 버튼 -->
        <div class="btn-bottom">
            <button type="button" class="btn btn03" id="btn_registVehicle">차량등록 업데이트 앱</button>
            <button type="button" class="btn btn03" id="btn_registVehicleTemp">차량등록 PC 매니저</button>
        </div>
        <!--/ 하단 버튼 -->
    </div>