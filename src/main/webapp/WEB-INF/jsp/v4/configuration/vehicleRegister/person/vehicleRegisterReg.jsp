<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
	$(function() {
		$("#start-date").datepicker({});
        $("#end-date").datepicker({});
        
        /* $("#mileage-start-date").datepicker({});
        $("#mileage-end-date").datepicker({}); */
        
        $("#register-complete-dialog").dialog({
            autoOpen: false,
            show: {
                duration: 500
            },
            width: '800',
            modal: true
        });

        $("#register-complete-open").on("click", function() {
        	var v = $('#frm').serializeObject(1);
			if(valid(v)) $("#register-complete-dialog").dialog("open");
        });
        
        loadingVehicleDb();
        
        $("#start-date").on("change",function(){
        	if($(this).val()){
        		var v = $(this).datepicker( "getDate" ).getTime();
        		$("#vehicleRegDate").val(v);
        	}else{
        		$("#vehicleRegDate").val("");
        	}
        });
        $("#end-date").on("change",function(){
        	if($(this).val()){
        		var v = $(this).datepicker( "getDate" ).getTime();
        		$("#vehicleMaintenanceDate").val(v);
        	}else{
        		$("#vehicleMaintenanceDate").val("");
        	}
        });
        
        
        $("#btn_regist").on("click",function(){
			
        	var v = $('#frm').serializeObject(1);
        	
        	if(valid(v)) {
	        	$V4.http_post("/api/v1/vehicle",$('#frm').serializeObject(),{
	        		header : {key : "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyS2V5IjoiYWQxMTAyN2YtOWVkMi0xMWU4LWI2ODgtNDIwMTBhOGMwMGI1IiwiZXhwaXJlZCI6MTUzNTE0OTAwMX0.M1SiPnD2DaixqJbRwZ-bVVo-1pXS0BAp977q9MAMPEw"}
	    	    	,success : function(rtv){
	    	    		console.log(rtv.result);
	    	    	}
	    	    });
        	}
        });
		$("#btn_registTemp").on("click",function(){
			
			var v = $('#frm').serializeObject(1);
			
			if(valid(v)) {
				$V4.http_post("/api/v1/vehicleTemp",$('#frm').serializeObject(),{
					header : {key : "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyS2V5IjoiYWQxMTAyN2YtOWVkMi0xMWU4LWI2ODgtNDIwMTBhOGMwMGI1IiwiZXhwaXJlZCI6MTUzNTE0OTAwMX0.M1SiPnD2DaixqJbRwZ-bVVo-1pXS0BAp977q9MAMPEw"}
		    		,success : function(rtv){
	    	    		console.log(rtv.result);
	    	    	}
	    	    });
			}
        });
	});
	
	function valid(v){
		
		if( $V4.required(v.deviceSn , "단말기 일련번호는 필수입니다." , $('#deviceSn')) ) return false;
		if( $V4.required(v.plateNum , "차량번호는 필수입니다." , $('#plateNum')) ) return false;
		if( $V4.required(v.totDistance , "현재 주행거리는 필수입니다." , $('#totDistance')) ) return false;
		
		if( !v.convCode){
			var c = $("#manufacture").val();
			if( $V4.required(c , "제조사는 필수입니다." , $('#manufacture')) ) return false;
			var c = $("#modelMaster").val();
			if( $V4.required(c , "차종은 필수입니다." , $('#modelMaster')) ) return false;
			var c = $("#year").val();
			if( $V4.required(c , "연식은 필수입니다." , $('#year')) ) return false;
			var c = $("#modelHeader").val();
			if( $V4.required(c , "모델명은 필수입니다." , $('#modelHeader')) ) return false;
			var c = $("#volume").val();
			if( $V4.required(c , "배기량은 필수입니다." , $('#volume')) ) return false;
			var c = $("#fuelType").val();
			if( $V4.required(c , "유종은 필수입니다." , $('#fuelType')) ) return false;
			var c = $("#transmission").val();
			if( $V4.required(c , "기어방식은 필수입니다." , $('#transmission')) ) return false;
			
			alert("차량 모델선택이 잘못되었습니다.");
			return false;
		}
		
		return true;
		
	}
	function display1() {
        document.getElementById("mileage").style.display = 'table';
    }
    function display2() {
        document.getElementById("mileage").style.display = 'none';
    }
    
    
    
    
    
    function loadingVehicleDb(){
    	var init = {};
    	init.manufacture = "${reqVo.manufacture}"?"${reqVo.manufacture}":"_";
    	init.modelMaster = "${reqVo.modelMaster}"?"${reqVo.modelMaster}":"_";
    	init.year = "${reqVo.year}"?"${reqVo.year}":"_";
    	init.modelHeader = "${reqVo.modelHeader}"?"${reqVo.modelHeader}":"_";
    	init.fuelType = "${reqVo.fuelType}"?"${reqVo.fuelType}":"_";
    	init.transmission = "${reqVo.transmission}"?"${reqVo.transmission}":"_";
    	init.volume = "${reqVo.volume}"?"${reqVo.volume}":"_";
    	
    	//function(url,param,target,listType,defaultVal,initVal,trigger,afterFunk,errorFunk)
    	$V4.ajaxCreateApiSelect ("/api/vehicleDb/ko/manufacture"
    			,{}
    			,$("#manufacture")
    			,1
    			,"선택"
    			,init.manufacture
    			,null
    			,null
    			,null);
    	
    	$("#manufacture").on("change",function(){
    		$("#convCode").val("");
    		var _$this = $(this);
    		var lev = _$this.data("lev");
    		
    		$(".vehicleList").each(function(){
    			var thisLev = $(this).data("lev");
    			if(lev < thisLev) $(this).html("<option value=''>선택</option>");
    		});
    		
    		if(_$this.val())
    		$V4.ajaxCreateApiSelect ("/api/vehicleDb/ko/manufacture/"+_$this.val()+"/modelMaster"
    				,{}
    				,$("#modelMaster")
    				,1
    				,"선택"
    				,""
    				,null
    				,null
    				,null);
    		
    	});
    	
    	$V4.ajaxCreateApiSelect ("/api/vehicleDb/ko/manufacture/"+init.manufacture+"/modelMaster"
    			,{}
    			,$("#modelMaster")
    			,1
    			,"선택"
    			,init.modelMaster
    			,"ko"
    			,null
    			,null);
    	
    	$("#modelMaster").on("change",function(){
    		$("#convCode").val("");
    		var _$this = $(this);
    		var lev = _$this.data("lev");
    		
    		$(".vehicleList").each(function(){
    			var thisLev = $(this).data("lev");
    			if(lev < thisLev) $(this).html("<option value=''>선택</option>");
    		});
    		
    		if(_$this.val())
    		$V4.ajaxCreateApiSelect ("/api/vehicleDb/ko/manufacture/"+$("#manufacture").val()+"/modelMaster/"+_$this.val()+"/year"
    				,{}
    				,$("#year")
    				,1
    				,"선택"
    				,""    				
    				,null
    				,null
    				,null);
    		
    	});
    	
    	$V4.ajaxCreateApiSelect ("/api/vehicleDb/ko/manufacture/"+init.manufacture+"/modelMaster/"+init.modelMaster+"/year"
    			,{}
    			,$("#year")
    			,1
    			,"선택"
    			,init.year
    			,null
    			,null
    			,null);
    	
    	$("#year").on("change",function(){
    		$("#convCode").val("");
    		var _$this = $(this);
    		var lev = _$this.data("lev");
    		
    		$(".vehicleList").each(function(){
    			var thisLev = $(this).data("lev");
    			if(lev < thisLev) $(this).html("<option value=''>선택</option>");
    		});
    		
    		if(_$this.val())
    		$V4.ajaxCreateApiSelect ("/api/vehicleDb/ko/manufacture/"+$("#manufacture").val()+"/modelMaster/"+$("#modelMaster").val()+"/year/"+_$this.val()+"/modelHeader"
    				,{}
    				,$("#modelHeader")
    				,1
    				,"선택"
    				,""
    				,null
    				,null
    				,null);
    		
    	});
    	
    	$V4.ajaxCreateApiSelect ("/api/vehicleDb/ko/manufacture/"+init.manufacture+"/modelMaster/"+init.modelMaster+"/year/"+init.year+"/modelHeader"
    			,{}
    			,$("#modelHeader")
    			,1
    			,"선택"
    			,init.modelHeader
    			,null
    			,null
    			,null);
    	
    	$("#modelHeader").on("change",function(){
    		$("#convCode").val("");
    		var _$this = $(this);
    		var lev = _$this.data("lev");
    		
    		$(".vehicleList").each(function(){
    			var thisLev = $(this).data("lev");
    			if(lev < thisLev) $(this).html("<option value=''>선택</option>");
    		});
    		
    		if(_$this.val())
    		$V4.ajaxCreateApiSelect ("/api/vehicleDb/ko/manufacture/"+$("#manufacture").val()+"/modelMaster/"+$("#modelMaster").val()+"/year/"+$("#year").val()+"/modelHeader/"+_$this.val()+"/fuelType"
    				,{}
    				,$("#fuelType")
    				,1
    				,"선택"
    				,""
    				,null
    				,null
    				,null);
    		
    		if(_$this.val())
    		$V4.ajaxCreateApiSelect ("/api/vehicleDb/ko/manufacture/"+$("#manufacture").val()+"/modelMaster/"+$("#modelMaster").val()+"/year/"+$("#year").val()+"/modelHeader/"+_$this.val()+"/volume"
    				,{}
    				,$("#volume")
    				,1
    				,"선택"
    				,""
    				,null
    				,null
    				,null);
    		
    		if(_$this.val())
    		$V4.ajaxCreateApiSelect ("/api/vehicleDb/ko/manufacture/"+$("#manufacture").val()+"/modelMaster/"+$("#modelMaster").val()+"/year/"+$("#year").val()+"/modelHeader/"+_$this.val()+"/transmission"
    				,{}
    				,$("#transmission")
    				,1
    				,"선택"
    				,""
    				,null
    				,null
    				,null);
    		
    	});
    	
    	$V4.ajaxCreateApiSelect ("/api/vehicleDb/ko/manufacture/"+init.manufacture+"/modelMaster/"+init.modelMaster+"/year/"+init.year+"/modelHeader/"+init.modelHeader+"/fuelType"
    			,{}
    			,$("#fuelType")
    			,1
    			,"선택"
    			,init.fuelType
    			,null
    			,null
    			,null);
    	$V4.ajaxCreateApiSelect ("/api/vehicleDb/ko/manufacture/"+init.manufacture+"/modelMaster/"+init.modelMaster+"/year/"+init.year+"/modelHeader/"+init.modelHeader+"/volume"
    			,{}
    			,$("#volume")
    			,1
    			,"선택"
    			,init.volume
    			,null
    			,null
    			,null);
    	$V4.ajaxCreateApiSelect ("/api/vehicleDb/ko/manufacture/"+init.manufacture+"/modelMaster/"+init.modelMaster+"/year/"+init.year+"/modelHeader/"+init.modelHeader+"/transmission"
    			,{}
    			,$("#transmission")
    			,1
    			,"선택"
    			,init.transmission
    			,null
    			,null
    			,null);
    	
    	$("#fuelType,#volume,#transmission").on("change",function(){
    		$("#convCode").val("");
    		var allchk = true;
    		$(".vehicleList").each(function(){
    			
    			if(!$(this).val()) {				
    				allchk = false;			
    			}
    		});
    		
    		if(allchk){
    			$V4.http_post("/api/vehicleDbCode/ko/"+$("#manufacture").val()+"/"+$("#modelMaster").val()+"/"+$("#year").val()+"/"+$("#modelHeader").val()+"/"+$("#fuelType").val()+"/"+$("#transmission").val()+"/"+$("#volume").val()+""
    					,{}
    					,{
    						success : function(r){
    							var obj = r.result;
    							$("#convCode").val(obj.convCode);
    						}
    					});
    			
    		}
    	});
    }
    
    
    
    
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>차량등록</h2>
                    <span>효율적인 차량관리를 위해 차량을 조회 ㆍ등록관리 합니다. </span>
                </div>
                <!--/ 상단 타이틀 -->
				<form id="frm" onSubmit="return false;">
                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="setting-page">
                    <div class="box-layout">
                        <div class="title">
                            <h3 class="tit3">차량ㆍ단말기 연동 등록</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
                            <!-- 프로세스 표시 -->
                            <div class="register-step-box">
                                <ol>
                                    <li class="active">차량 등록</li>
                                    <li>단말기 등록</li>
                                </ol>
                            </div>
                            <!--/ 프로세스 표시 -->

                            <h3 class="tit2 mgt50">차량 기본정보</h3>
                            <table class="table mgt20">
                                <caption>차량 기본정보</caption>
                                <colgroup>
                                    <col style="width:16.35%" />
                                    <col style="width:34.65%" />
                                    <col style="width:16.35%" />
                                    <col style="width:34.65%" />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <th scope="row">차량번호</th>
                                        <td>
                                            <input type="text" name="plateNum" id="plateNum" />
                                        </td>
                                        <th scope="row">단말기 일련번호</th>
                                        <td>
                                            <select name="deviceSeries">
												<option value="von-S31">von-S31</option>
												<option value="VC-200">VC-200</option>
											</select>
                                            <input type="text" name="deviceSn" id="deviceSn"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">제조사</th>
                                        <td>
                                            <select id="manufacture" data-lev='1' class="vehicleList">
												<option value="">선택</option>
											</select>
                                        </td>
                                        <th scope="row">차종</th>
                                        <td>
                                            <select id="modelMaster" data-lev='2' class="vehicleList">
												<option value="">선택</option>
											</select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">연식</th>
                                        <td>
                                            <select id="year" data-lev='3' class="vehicleList">
												<option value="">선택</option>
											</select>
                                        </td>
                                        <th scope="row">모델명</th>
                                        <td>
                                            <select id="modelHeader" data-lev='4' class="vehicleList">
												<option value="">선택</option>
											</select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">배기량</th>
                                        <td>
                                            <select id="volume" data-lev='5' class="vehicleList">
												<option value="">선택</option>
											</select>
                                        </td>
                                        <th scope="row">유종</th>
                                        <td>
                                            <select id="transmission" data-lev='5' class="vehicleList">
												<option value="">선택</option>
											</select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">기어방식</th>
                                        <td>
                                            <select id="fuelType" data-lev='5' class="vehicleList">
												<option value="">선택</option>
											</select>
                                        </td>
                                        <th scope="row">현재 주행거리</th>
                                        <td colspan="3">
                                            <input type="text" name="totDistance"/> TODO (km) or (mile)
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <input type="hidden" id="convCode" name="convCode">
                            <h3 class="tit2 mgt50">차량 부가정보</h3>
                            <table class="table mgt20">
                                <caption>차량 부가정보</caption>
                                <colgroup>
                                    <col style="width:16.35%" />
                                    <col style="width:34.65%" />
                                    <col style="width:16.35%" />
                                    <col style="width:34.65%" />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <th scope="row">차량등록일</th>
                                        <td>
                                            <input type="text" id="start-date" />
                                            <input type="hidden" id="vehicleRegDate" name="vehicleRegDate" />
                                        </td>
                                        <th scope="row">차대번호</th>
                                        <td>
                                            <input type="text" id="vehicleBodyNum" name="vehicleBodyNum"/>                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">차량검사 만기일</th>
                                        <td>
                                            <input type="text" id="end-date" />
                                            <input type="hidden" id="vehicleMaintenanceDate" name="vehicleMaintenanceDate" />
                                        </td>
                                        <th scope="row" class="help">고정 임차료
                                        	<div class="layer">
                                        		리스 또는 렌트비용을 입력해주세요.
                                        	</div>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th scope="row">보험사명</th>
                                        <td>
                                            <input type="text" name="vehicleInsureNm"/>
                                        </td>
                                        <th scope="row">긴급출동 연락처</th>
                                        <td>
                                            <input type="text" name="vehicleInsurePhone"/>
                                        </td>
                                    </tr>
                                    <tr>
                                    	<th scope="row">소유구분</th>
                                        <td>
                                            <input type="radio" id="vehicleBizType1" value="1" name="vehicleBizType" checked="checked"/> 소유&nbsp;&nbsp;&nbsp;
                                            <input type="radio" id="vehicleBizType2" value="2" name="vehicleBizType"/> 리스&nbsp;&nbsp;&nbsp;
                                            <input type="radio" id="vehicleBizType3" value="3" name="vehicleBizType"/> 렌터&nbsp;&nbsp;&nbsp;
                                            <input type="radio" id="vehicleBizType4" value="4" name="vehicleBizType"/> 개인&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <th scope="row">차량색상</th>
                                        <td>
                                            <input type="text" name="color"/>
                                        </td>
                                    </tr>
                                    <tr>
                                    	<th scope="row">비고</th>
                                        <td colspan="3">
                                            <input type="text" name="vehicleDescript"/>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <%-- <h3 class="tit2 mgt50">마일리지 보험 특약 (선택정보) 구현불가</h3>
                            <div class="select-mileage">
                                <input type="radio" id="mileage01" name="mileage" onclick="display1()" checked />
                                <label for="mileage01">가입</label> &nbsp;&nbsp;&nbsp;
                                <input type="radio" id="mileage02" name="mileage" onclick="display2()" />
                                <label for="mileage02">미가입</label>
                            </div>
                            <table id="mileage" class="table mgt20">
                                <caption>마일리지 보험 특약</caption>
                                <colgroup>
                                    <col style="width:16.35%" />
                                    <col style="width:34.65%" />
                                    <col style="width:16.35%" />
                                    <col style="width:34.65%" />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <th scope="row">가입한 보험사</th>
                                        <td>
                                            <select name="">
												<option value="">선택</option>
											</select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">보험가입일</th>
                                        <td>
                                            <input type="text" id="mileage-start-date" /> ~
                                            <input type="text" id="mileage-end-date" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">현재 잔여 마일리지</th>
                                        <td>
                                            <input type="text" value="자동계산" readonly /> (km)
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">약관 주행거리</th>
                                        <td>
                                            <input type="text" /> (km)
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">보험 가입 시점 총 주행거리</th>
                                        <td>
                                            <input type="text" /> (km)
                                        </td>
                                    </tr>
                            </table> --%>

                            <!-- 하단 버튼 -->
                            <div class="btn-bottom">
                                <button class="btn btn02">취소</button>
                                <button class="btn btn03" id="register-complete-open">등록완료</button>
                            </div>
                            <!--/ 하단 버튼 -->
                        </div>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
                </form>
            </div>
        </section>
        <!--/ 본문 -->
        
      <!-- dialog -->  
      <div id="register-complete-dialog" title="단말기 등록방법 선택">
        <div class="txt">
            단말기 펌웨어의 차량데이터를 등록해야 정상적으로 서비스 이용이 가능합니다.
            <br />아래 등록방법을 선택해 주세요.
            <ul>
                <li>PC매니저 : PC에 단말기를 연락하여 펌웨어를 업데이트 합니다.
                    <br />약 1분 정도 소요됩니다.</li>
                <li>App을 통한 차량등록 : VIewCAR APP &gt; 업데이트를 통해 블루투스로 등록하고 통상 5분정도 소요됩니다.</li>
            </ul>

        </div>

        <!-- 하단 버튼 -->
        <div class="btn-bottom">
            <button class="btn btn03" id="btn_regist">차량등록 업데이트 앱</button>
            <button class="btn btn03" id="btn_registTemp">차량등록 PC 매니저</button>
        </div>
        <!--/ 하단 버튼 -->
    </div>
    <!-- dialog -->