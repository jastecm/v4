<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
});
</script>
<section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>차량등록</h2>
                    <span>효율적인 차량관리를 위해 차량을 조회 ㆍ등록관리 합니다. </span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="setting-page">
					<div class="register-complete">
						<h3 class="tit2 mgt30">차량ㆍ단말기 연동 등록</h3>

						<!-- 프로세스 표시 -->
						<div class="register-step-box mgt20">
							<ol>
								<li>차량 등록</li>
								<li class="active">단말기 등록</li>
							</ol>
						</div>
						<!--/ 프로세스 표시 -->

						<div class="info-txt">
							<strong>"뷰카 PC매니저"</strong> 프로그램을 다운받아야 단말기 등록을 진행할 수 있습니다.<br />
							window7 64bit를 사용하시는 고객님은 <strong>"윈도우7 USB드라이버"</strong> 를 다운받아<br />
							내컴퓨터 &gt; 설정 &gt; 장치관리자 &gt; PORT에서 자스텍 USB드라이버를 업데이트해주세요.<br /><br />

							윈도우(window7 이상) 지원 / 사용 가능 브라우저 : Chrome, Microsoft edge<br />
							※ 단말기 등록 실행이 안될 경우, 자스텍엠 고객센터 (1599-0439)로 문의해주세요.
						</div>

						<div class="action-txt">
							<p>등록할 단말기를 USB 연결 후 "단말기 등록 실행" 버튼을 클릭해주세요.</p>
							<button class="btn btn01">단말기 등록 실행</button>
						</div>

						<!-- 하단 버튼 -->
						<div class="btn-bottom">
							<button class="btn btn03">이전 단계</button>
						</div>
						<!--/ 하단 버튼 -->
					</div>
					<!--/ 콘텐츠 본문 -->
				</div>
            </div>
        </section>