<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	
	$("#vehicleRegisterBtn").on("click",function(){
		$V4.move("/config/vehicleRegister/reg/corp/vehicle");
	});
	
	$("#vehicleRegisterBtn2").on("click",function(){
		$V4.move("/config/vehicleRegister/reg/corp/bus");
	});
	
	$("#vehicleRegisterBtn3").on("click",function(){
		$V4.move("/config/vehicleRegister/reg/corp/truck");
	});
	
	$("#vehicleRegisterBtn4").on("click",function(){
		$V4.move("/config/vehicleRegister/reg/corp/etcVehicle");
	});
		
	
	$('.page-tab li').on('click',function(){
		var tab = $(this).data('tab');
		
		$('.tabDiv').hide();
		$('.page-tab li').removeClass('on')
		
		$(this).addClass('on')
		$('#'+tab+'Tab').show();
		
		
		
	});

	var vehicleSetting = {
		thead : [
		         {'<input type="checkbox" />' : '5%'},
		         {'차량정보' : '25%'},
		         {'차량소속' : '12%'},
		         {'차량관리자' : '12%'},
		         {'지정배차' : '12%'},
		         {'<select name="" class="arrow fillterDevice" data-target="vehicle"><option value="">선택</option><option value="von-S31">von-S31</option><option value="VC-200">VC-200</option></select>' : '10%'},
		         {'<select name="" class="arrow fillterUsed" data-target="vehicle"><option value="">사용구분</option><option value="1">사용중</option><option value="0">사용중지</option></select>' : '10%'},
		         {'수정' : '7%'},
		         {'차량교체' : '7%'}
		         ]
		,bodyHtml : (function(data){
			var strHtml = "";
			for(i in data.result){
				var obj = data.result[i];
				strHtml += '<tr>';
				strHtml += '<td><input type="checkbox" data-vehiclekey='+obj.vehicleKey+' /></td>';
				strHtml += '<td><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" class="car-img" alt="" /> '+obj.plateNum+'</td>';
				if(obj.groups.length != 0){
					var groupList = [];
					for(var j = 0 ; j < obj.groups.length ; j++){
						if(obj.groups[j].hasOwnProperty('groupNm')){
							groupList.push(obj.groups[j].groupNm);
						}else{
							groupList.push(obj.groups[j].parentGroupNm);
						}
					}
					strHtml += '<td>'+groupList.join('</br>')+'</td>';
				}else{
					strHtml += '<td></td>';
				}
				
				strHtml += '<td>'+simpleUserObjectNameViewer(obj.corp.manager.vehicleManager,"/"," "," ")+'</td>';
				
				if(obj.hasOwnProperty('fixedInfo')){
					
					//obj.fixedInfo.fixedAccount.name
					//obj.fixedInfo.fixedAccount.corpPosition
					//fixedinfo에 그룹이 없음.
					if(obj.fixedInfo.fixedAccount.hasOwnProperty('group')){
						var groupNm;
						if(obj.fixedInfo.fixedAccount.group.hasOwnProperty('groupNm')){
							groupNm = obj.fixedInfo.fixedAccount.group.groupNm
						}else{
							groupNm = obj.fixedInfo.fixedAccount.group.parentGroupNm;
						}
						strHtml += '<td>'+groupNm+'<br />'+obj.fixedInfo.fixedAccount.name+'</td>';
						
					}else{
						strHtml += '<td>'+obj.fixedInfo.fixedAccount.name+'</td>';
					}
					
				}else{
					strHtml += '<td></td>';	
				}
				
				if(obj.hasOwnProperty('device')){
					strHtml += '<td>'+obj.device.deviceSeries+'<br />'+obj.device.deviceSn+'</td>';	
				}else{
					strHtml += '<td></td>';
				}
				
				if(obj.hasOwnProperty('device')){
					strHtml += '<td>사용중</td>';
				}else{
					strHtml += '<td>사용중지</td>';
				}
				strHtml += '<td><button type="button" class="btn-img modify" data-vehiclekey="'+obj.vehicleKey+'" data-vehicletype="1">수정</button></td>';
				strHtml += '<td><button type="button" class="btn-img change" data-vehiclekey="'+obj.vehicleKey+'" data-vehicletype="1">차량교체</button></td>';
				strHtml += '</tr>';
			}
			return strHtml;
		})
		,limit : parseInt($("#func_limitChange").val())
		,pagePerGroup : 10 //pagingGroup size
		,url : "/api/1/vehicle"
		,param : {'vehicleType' : '1' , 'searchOrder' : $("#func_order").val()}
		,http_post_option : {
			requestMethod : "GET"
			,header : {key : "${_KEY}"}
		}
		,initSearch : true //생성과 동시에 search		
		,loadingBodyViewer : true //로딩중!! 표시됨
		,debugMode : true
	}
	
	var busSetting = {
			thead : [
			         {'<input type="checkbox" />' : '5%'},
			         {'차량정보' : '25%'},
			         {'차량소속' : '12%'},
			         {'차량관리자' : '12%'},
			         {'지정배차' : '12%'},
			         {'<select name="" class="arrow fillterDevice" data-target="bus"><option value="">선택</option><option value="von-S31">von-S31</option><option value="VC-200">VC-200</option></select>' : '10%'},
			         {'<select name="" class="arrow fillterUsed" data-target="bus"><option value="">사용구분</option><option value="1">사용중</option><option value="0">사용중지</option></select>' : '10%'},
			         {'수정' : '7%'},
			         {'차량교체' : '7%'}
			         ]
			,bodyHtml : (function(data){
				var strHtml = "";
				for(i in data.result){
					var obj = data.result[i];
					strHtml += '<tr>';
					strHtml += '<td><input type="checkbox" data-vehiclekey='+obj.vehicleKey+' /></td>';
					strHtml += '<td><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" class="car-img" alt="" /> '+obj.plateNum+'</td>';
					strHtml += '<td>총무팀</td>';
					strHtml += '<td>총무팀<br /> 이준혁/대리</td>';
					strHtml += '<td>협력사<br />조이콥</td>';
					strHtml += '<td>Von-S31<br />A000876</td>';
					strHtml += '<td>사용중</td>';
					strHtml += '<td><button type="button" class="btn-img modify" data-vehiclekey="'+obj.vehicleKey+'" data-vehicletype="2">수정</button></td>';
					strHtml += '<td><button type="button" class="btn-img change" data-vehiclekey="'+obj.vehicleKey+'" data-vehicletype="2">차량교체</button></td>';
					strHtml += '</tr>';
				}
				return strHtml;
			})
			,limit : parseInt($("#func_limitChange").val())
			,pagePerGroup : 3 //pagingGroup size
			,url : "/api/1/vehicle"
			,param : {'vehicleType' : '2' , 'searchOrder' : $("#func_order").val()}
			,http_post_option : {
				requestMethod : "GET"
				,header : {key : "${_KEY}"}
			}
			//login token - test2@naver.com / test123!@# (이아이디가 차량 많음)
			,initSearch : true //생성과 동시에 search
			,loadingBodyViewer : true //로딩중!! 표시됨
			,debugMode : true
		}
	
	var truckSetting = {
			thead : [
			         {'<input type="checkbox" />' : '5%'},
			         {'차량정보' : '25%'},
			         {'차량소속' : '12%'},
			         {'차량관리자' : '12%'},
			         {'지정배차' : '12%'},
			         {'<select name="" class="arrow fillterDevice" data-target="truck"><option value="">선택</option><option value="von-S31">von-S31</option><option value="VC-200">VC-200</option></select>' : '10%'},
			         {'<select name="" class="arrow fillterUsed" data-target="truck"><option value="">사용구분</option><option value="1">사용중</option><option value="0">사용중지</option></select>' : '10%'},
			         {'수정' : '7%'},
			         {'차량교체' : '7%'}
			         ]
			,bodyHtml : (function(data){
				var strHtml = "";
				for(i in data.result){
					var obj = data.result[i];
					strHtml += '<tr>';
					strHtml += '<td><input type="checkbox" data-vehiclekey='+obj.vehicleKey+' /></td>';
					strHtml += '<td><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" class="car-img" alt="" /> '+obj.plateNum+'</td>';
					strHtml += '<td>총무팀</td>';
					strHtml += '<td>총무팀<br /> 이준혁/대리</td>';
					strHtml += '<td>협력사<br />조이콥</td>';
					strHtml += '<td>Von-S31<br />A000876</td>';
					strHtml += '<td>사용중</td>';
					strHtml += '<td><button type="button" class="btn-img modify" data-vehiclekey="'+obj.vehicleKey+'" data-vehicletype="3">수정</button></td>';
					strHtml += '<td><button type="button" class="btn-img change" data-vehiclekey="'+obj.vehicleKey+'" data-vehicletype="3">차량교체</button></td>';
					strHtml += '</tr>';
				}
				return strHtml;
			})
			,limit : parseInt($("#func_limitChange").val())
			,pagePerGroup : 10 //pagingGroup size
			,url : "/api/1/vehicle"
			,param : {'vehicleType' : '3' , 'searchOrder' : $("#func_order").val()}
			,http_post_option : {
				requestMethod : "GET"
				,header : {key : "${_KEY}"}
			}
			//login token - test2@naver.com / test123!@# (이아이디가 차량 많음)
			,initSearch : true //생성과 동시에 search
			,loadingBodyViewer : true //로딩중!! 표시됨
			,debugMode : true
		}
	var etcSetting = {
			thead : [
			         {'<input type="checkbox" />' : '5%'},
			         {'차량정보' : '30%'},
			         {'차량소속' : '30%'},
			         {'차량관리자' : '30%'},
			         {'수정' : '5%'}
			         ]
			,bodyHtml : (function(data){
				var strHtml = "";
				for(i in data.result){
					var obj = data.result[i];
					strHtml += '<tr>';
					strHtml += '<td><input type="checkbox" data-vehiclekey='+obj.vehicleKey+' /></td>';
					strHtml += '<td><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" class="car-img" alt="" /> '+obj.plateNum+'</td>';
					strHtml += '<td>총무팀</td>';
					strHtml += '<td>총무팀<br /> 이준혁/대리</td>';
					strHtml += '<td><button type="button" class="btn-img modify" data-vehiclekey="'+obj.vehicleKey+'" data-vehicletype="4">수정</button></td>';
					strHtml += '</tr>';
				}
				return strHtml;
			})
			,limit : parseInt($("#func_limitChange").val()) 
			,pagePerGroup : 10 //pagingGroup size
			,url : "/api/1/vehicle"
			,param : {'vehicleType' : '4' , 'searchOrder' : $("#func_order").val()}
			,http_post_option : {
				requestMethod : "GET"
				,header : {key : "${_KEY}"}
			}
			//login token - test2@naver.com / test123!@# (이아이디가 차량 많음)
			,initSearch : true //생성과 동시에 search
			,loadingBodyViewer : true //로딩중!! 표시됨
			,debugMode : true
		}
	
	$('#vehicleList').commonList(vehicleSetting);
	$('#busList').commonList2(busSetting);
	$('#truckList').commonList3(truckSetting);
	$('#etcList').commonList4(etcSetting);
	
	
	//title counting	
	$('.page-tab li').on('click',function(){
		var tab = $(this).data('tab');  //vehicle , bus ,truck , etc
		
		if(tab == "vehicle"){
			$("#tot").show();
			$("#totCnt").html(cntObj.vehicle.totalCnt);
			$("#used").show();
			$("#usedCnt").html(cntObj.vehicle.usedCnt);
			$("#unused").show();
			$("#unusedCnt").html(cntObj.vehicle.unusedCnt);
		}else if(tab == "bus"){
			$("#tot").show();
			$("#totCnt").html(cntObj.bus.totalCnt);
			$("#used").show();
			$("#usedCnt").html(cntObj.bus.usedCnt);
			$("#unused").show();
			$("#unusedCnt").html(cntObj.bus.unusedCnt);
		}else if(tab == "truck"){
			$("#tot").show();
			$("#totCnt").html(cntObj.truck.totalCnt);
			$("#used").show();
			$("#usedCnt").html(cntObj.truck.usedCnt);
			$("#unused").show();
			$("#unusedCnt").html(cntObj.truck.unusedCnt);
		}else if(tab == "etc"){
			$("#tot").show();
			$("#totCnt").html(cntObj.etc.totalCnt);
			$("#used").hide();
			$("#usedCnt").html(cntObj.etc.usedCnt);
			$("#unused").hide();
			$("#unusedCnt").html(cntObj.etc.unusedCnt);
		}
	});
	
	var countingParam = {offset:0,limit:1,vehicleType:1,counting:true};
	$V4.http_post("/api/v1/vehicle",countingParam,{
		requestMethod:"GET"
		,header:{key:"${_KEY}"}
		,success : function(data){
			$.extend(cntObj.vehicle,data);
			$('.page-tab li.list01').trigger("click");
		}
	});
	countingParam.vehicleType = 2;
	$V4.http_post("/api/v1/vehicle",countingParam,{
		requestMethod:"GET"
		,header:{key:"${_KEY}"}
		,success : function(data){
			$.extend(cntObj.bus,data);
		}
	});
	countingParam.vehicleType = 3;
	$V4.http_post("/api/v1/vehicle",countingParam,{
		requestMethod:"GET"
		,header:{key:"${_KEY}"}
		,success : function(data){
			$.extend(cntObj.truck,data);
		}
	});
	countingParam.vehicleType = 4;
	$V4.http_post("/api/v1/vehicle",countingParam,{
		requestMethod:"GET"
		,header:{key:"${_KEY}"}
		,success : function(data){
			$.extend(cntObj.etc,data);
		}
	});
	
	
	//search
	$("#top_btn_search").on("click",$.debounce(200,1,function(){
		
		var tab = $('.page-tab li.on').data("tab"); //vehicle , bus ,truck , etc
		var param = {searchType:$("#top_searchType").val() , searchText : $("#top_searchText").val() , vehicleType : 0 , searchOrder : $("#func_order").val()}
		var limit = parseInt($("#func_limitChange").val());
		
		param.vehicleType = "1";
		$('#vehicleList').commonList("setParam",param);
		$('#vehicleList').commonList("setLimit",limit).search();
		param.vehicleType = "2";
		$('#busList').commonList2("setParam",param);
		$('#busList').commonList2("setLimit",limit).search();
		param.vehicleType = "3";
		$('#truckList').commonList3("setParam",param);
		$('#busList').commonList3("setLimit",limit).search();
		param.vehicleType = "4";
		$('#etcList').commonList4("setParam",param);
		$('#etcList').commonList4("setLimit",limit).search();
	}));
	
	$("#top_searchText").on("keyup",function(e){
		if(e.keyCode == 13) {
			$("#top_btn_search").trigger("click");
		}
	});
	
	//func
	$("#func_order").on("change",function(){
		$("#top_btn_search").trigger("click");
	});
	
	$("#func_excelDown").on("click",function(){
		alert("엑셀다운 - TODO");
	});
	
	$("#func_unused").on("click",function(){
		var tab = $(".page-tab li.on").data('tab');  //vehicle , bus ,truck , etc
		
		var chk;
		
		if(tab == "vehicle"){
			chk = $("#vehicleList tbody input:checkbox:checked");
		}else if(tab == "bus"){
			chk = $("#busList tbody input:checkbox:checked");
		}else if(tab == "truck"){
			chk = $("#truckList tbody input:checkbox:checked");
		}else if(tab == "etc"){
			chk = $("#etcList tbody input:checkbox:checked");
		}
		
		if(chk.length == 0 ) alert("선택되지 않았습니다.");
		else{
			if(confirm("정말 사용중지 처리하시겠습니까?")){
				var arrDe = $(chk).map(function(i,v) {
					var vehicleKey = $(v).data("vehiclekey");
					var _tr = $(v).closest("tr");
					var def = $.Deferred(); //point1
					$V4.http_post("/api/v1/vehicle/"+vehicleKey+"/inactive",{},{
								header : {key : "${_KEY}"}
								,requestMethod : "DELETE"
								,success : function(data){
									def.resolve({result: 1 , target : $(v) , data : data}); //point3
									//여기선 별다른 ajax return data를 쓸일없으니 만들어 넘김 
								}
								,error : function(data){
									def.resolve({result: 0 , target : $(v) , data : data}); //point3
								},
					});
					return def;
				});
				
				//프로미스 배열중 중 하나라도 reject라면 바로 fail로 이동 - 이때 나머지 프로세스는 완료를 기다리지 않음(즉 첫번째 promise가 실패라면 그 뒤는 성공이든 실패든 fail로 이동)
				//따라서 성공 실패 카운트를 새기위해선 강제로 promise를 성공으로 구라치는수밖에 없음
				//ajax에서 error를 타는순간 fail 처리되므로 ajax promise로는 불가
				//때문에 promise 를 새로 만들어 강제로 해결된 프로미스를 감시하는수밖에 없음 
				$.when.apply($, arrDe).then(
				function() {
					var s = 0;
					var f = 0;
					$.each(arguments,function(i,v){
						if(v.result) {
							s++;
							var used = $(v.target).closest("tr").find("td").eq(6); //사용구분 td
							used.html("사용중지");
						}else{
							f++;
							var used = $(v.target).closest("tr").find("td").eq(6); //사용구분 td
							var rejectMsg = v.data.responseJSON.result;
							used.html(rejectMsg);
						}
					});
				    alert("처리되었습니다.\n성공 : " + s + "\n실패 : " + f)
				    if(f == 0) $("#top_btn_search").trigger("click");
				},function(){ 
					alert("예상치 못한 문제가 발생하였습니다.\n 관리자에게 문의하여 주십시오.");
				});
				
			}
		}
	});
	
	
	$("#func_delete").on("click",function(){
		var tab = $(".page-tab li.on").data('tab');  //vehicle , bus ,truck , etc
		
		var chk;
		
		if(tab == "vehicle"){
			chk = $("#vehicleList tbody input:checkbox:checked");
		}else if(tab == "bus"){
			chk = $("#busList tbody input:checkbox:checked");
		}else if(tab == "truck"){
			chk = $("#truckList tbody input:checkbox:checked");
		}else if(tab == "etc"){
			chk = $("#etcList tbody input:checkbox:checked");
		}
		
		if(chk.length == 0 ) alert("선택되지 않았습니다.");
		else{
			if(confirm("정말 삭제 처리하시겠습니까?\n삭제된 차량의 데이터는 복구할 수 없습니다.")){
				var arrDe = $(chk).map(function(i,v) {
					var vehicleKey = $(v).data("vehiclekey");
					var _tr = $(v).closest("tr");
					var def = $.Deferred(); //point1
					$V4.http_post("/api/v1/vehicle/"+vehicleKey,{},{
								header : {key : "${_KEY}"}
								,requestMethod : "DELETE"
								,success : function(data){
									def.resolve({result: 1 , target : $(v) , data : data}); //point3
									//여기선 별다른 ajax return data를 쓸일없으니 만들어 넘김 
								}
								,error : function(data){
									def.resolve({result: 0 , target : $(v) , data : data}); //point3
								},
					});
					return def;
				});
				
				$.when.apply($, arrDe).then(
				function() {
					var s = 0;
					var f = 0;
					$.each(arguments,function(i,v){
						if(v.result) {
							s++;
							var used = $(v.target).closest("tr").find("td").eq(6); //사용구분 td
							used.html("삭제됨");
						}else{
							f++;
							var used = $(v.target).closest("tr").find("td").eq(6); //사용구분 td
							var rejectMsg = v.data.responseJSON.result;
							used.html(rejectMsg);
						}
					});
				    alert("처리되었습니다.\n성공 : " + s + "\n실패 : " + f)
				    if(f == 0) $("#top_btn_search").trigger("click");
				},function(){ 
					alert("예상치 못한 문제가 발생하였습니다.\n 관리자에게 문의하여 주십시오.");
				});
				
			}
		}
	});
	
	$("#func_limitChange").on("change",function(){
		$("#top_btn_search").trigger("click");
	});
	
	$("table").on("change",".fillterDevice",function(){
		var t = $(this).data("target");
		var param = {};
		if(t=="vehicle"){
			param = $('#vehicleList').commonList("getParam");
			param.searchDeviceSeries = $(this).val();
			$('#vehicleList').commonList("refresh");
		}else if(t=="bus"){
			param = $("#truckList").commonList2("getParam");
			param.searchDeviceSeries = $(this).val();
			$('#truckList').commonList2("refresh");
		}else if(t=="truck"){
			param = $("#busList").commonList3("getParam");
			param.searchDeviceSeries = $(this).val();
			$('#busList').commonList3("refresh");
		}
	});
	
	$("table").on("change",".fillterUsed",function(){
		var t = $(this).data("target");
		var param = {};
		if(t=="vehicle"){
			param = $('#vehicleList').commonList("getParam");
			param.searchUseState = $(this).val();
			$('#vehicleList').commonList("refresh");
		}else if(t=="bus"){
			param = $("#truckList").commonList2("getParam");
			param.searchUseState = $(this).val();
			$('#truckList').commonList2("refresh");
		}else if(t=="truck"){
			param = $("#busList").commonList3("getParam");
			param.searchUseState = $(this).val();
			$('#busList').commonList3("refresh");
		}
	});
	 
	$("table").on("click",".modify",function(){
		var vehicleKey = $(this).data("vehiclekey");
		var vehicleType = $(this).data("vehicletype");
		
		alert("수정!!!\nvehicleKey - " + vehicleKey + "\n" + "vehicleType - " + vehicleType);
	});
	
	$("table").on("click",".change",function(){
		var vehicleKey = $(this).data("vehiclekey");
		var vehicleType = $(this).data("vehicletype");
		
		alert("교체!!!\nvehicleKey - " + vehicleKey + "\n" + "vehicleType - " + vehicleType);
	});
});

var cntObj = {vehicle : {totalCnt : 0 , usedCnt : 0 , unusedCnt : 0}
			,bus : {totalCnt : 0 , usedCnt : 0 , unusedCnt : 0}
			,truck : {totalCnt : 0 , usedCnt : 0 , unusedCnt : 0}
			,etc : {totalCnt : 0 , usedCnt : 0 , unusedCnt : 0}
}
</script>
  <!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>차량등록</h2>
                    <span>효율적인 차량관리를 위해 차량을 조회 ㆍ등록관리 합니다. </span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="setting-page">
                    <div class="vehicle-register-list">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                            <div class="item">
                                <span id="tot">등록 차량수 <strong id="totCnt">0</strong>대</span>
                                <span id="used">사용중 <strong id="usedCnt">0</strong>대</span>
                                <span id="unused">사용중지 <strong id="unusedCnt">0</strong>대</span>
                            </div>

                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <!-- 등록차량관리 -->
                        <h3 class="tit2 mgt50">등록차량관리</h3>
                        <div class="box-register">
                            서비스 관리를 위해 정확한 차량정보를 등록해주세요.
                            <div class="right">
                                <button class="btn btn01" id="vehicleRegisterBtn">차량등록</button>
                                <button class="btn btn01" id="vehicleRegisterBtn2">차량등록버스</button>
                                <button class="btn btn01" id="vehicleRegisterBtn3">차량등록트럭</button>
                                <button class="btn btn01" id="vehicleRegisterBtn4">차량등록작업</button>
                            </div>
                        </div>
                        <!--/ 등록차량관리 -->

                        <!-- 상단 검색 -->
                        <div class="top-search">
                            <select name="" id="top_searchType">
								<option value="">전체</option>
								<option value="plateNum">차량번호</option>
								<option value="modelMaster">차종</option>
								<option value="deviceSn">단말 시리얼</option>
							</select>
                            <input type="text" name="" id="top_searchText"/>
                            <button type="button" class="btn btn03" id="top_btn_search">조회</button>
                        </div>
                        <!--/ 상단 검색 -->
                    </div>

                    <!-- 메뉴 -->
                    <ul class="page-tab col-4">
                        <li class="list01 on" data-tab="vehicle">
                            <a href="javascript:void(0);">승용차</a>
                        </li>
                        <li class="list02" data-tab="bus">
                            <a href="javascript:void(0);">버스</a>
                        </li>
                        <li class="list03" data-tab="truck">
                            <a href="javascript:void(0);">트럭</a>
                        </li>
                        <li class="list04" data-tab="etc">
                            <a href="javascript:void(0);">특수차량</a>
                        </li>
                    </ul>
                    <!--/ 메뉴 -->

                    <!-- table 버튼 -->
                    <div class="btn-function">
                        <div class="left">
                            <select name="" id="func_order">
								<option value="lastestLocationTime">최근 운행순</option>
								<option value="regDate">최근 등록순</option>
							</select>
                        </div>

                        <div class="right">
                            <button class="excel" id="func_excelDown">엑셀 다운로드</button>
                            <button class="stop" id="func_unused">사용중지</button>
                            <button class="delete" id="func_delete">삭제</button>
                            <select name="" id="func_limitChange">
								<option value="5">5건씩 보기</option>
								<option value="10" selected>10건씩 보기</option>
								<option value="50">30건씩 보기</option>
								<option value="100">50건씩 보기</option>
							</select>
                        </div>
                    </div>
                    <!--/ table 버튼 -->
					<div class="tabDiv" id="vehicleTab" >
						<table class="table list mgt20" id="vehicleList" ></table>
					</div>
					
					<div class="tabDiv" id="busTab"  style="display:none;">
						<table class="table list mgt20" id="busList"></table>
					</div>
					
					<div class="tabDiv" id="truckTab" style="display:none;">
						<table class="table list mgt20" id="truckList"></table>
					</div>
					
					<div class="tabDiv" id="etcTab" style="display:none;">
						<table class="table list mgt20" id="etcList"></table>
					</div>
                    
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->
        </section>