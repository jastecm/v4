<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	$("#vehicleRegisterBtn").on("click",function(){
		$V4.move("/config/vehicleRegister/reg/person");
	});
});

</script>

<section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>차량등록</h2>
                    <span>효율적인 차량관리를 위해 차량을 조회 ㆍ등록관리 합니다. </span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="setting-page">
                    <div class="vehicle-register-list">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                            <div class="item">
                                <span>
									등록 차량수
									<strong>15</strong>대
								</span>
                                <span>
									사용중
									<strong>3</strong>대
								</span>
                                <span>
									사용중지
									<strong>12</strong>대
								</span>
                            </div>

                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <!-- 등록차량관리 -->
                        <h3 class="tit2 mgt50">등록차량관리</h3>
                        <div class="box-register">
                            서비스 관리를 위해 정확한 차량정보를 등록해주세요.
                            <div class="right">
                                <button class="btn btn01" id="vehicleRegisterBtn">차량등록</button>
                            </div>
                        </div>
                        <!--/ 등록차량관리 -->

                        <!-- 상단 검색 -->
                        <div class="top-search">
                            <select name="">
								<option value="">전체</option>
								<option value="">차량번호</option>
								<option value="">차종</option>
								<option value="">단말 시리얼</option>
							</select>
                            <input type="text" name="" />
                            <button class="btn btn03">조회</button>
                        </div>
                        <!--/ 상단 검색 -->
                    </div>

                    <!-- 메뉴 -->
                    <ul class="page-tab col-4">
                        <li class="list01 on">
                            <a href="#">승용차</a>
                        </li>
                        <li class="list02">
                            <a href="#">승합(12인승 이상)</a>
                        </li>
                        <li class="list03">
                            <a href="#">트럭</a>
                        </li>
                        <li class="list04">
                            <a href="#">특수차량</a>
                        </li>
                    </ul>
                    <!--/ 메뉴 -->

                    <!-- table 버튼 -->
                    <div class="btn-function">
                        <div class="left">
                            <select name="">
								<option value="">최근 운행순</option>
								<option value="">최근 등록순</option>
							</select>
                        </div>

                        <div class="right">
                            <button class="excel">엑셀 다운로드</button>
                            <button class="stop">사용중지</button>
                            <button class="delete">삭제</button>
                            <select name="">
								<option value="">5건씩 보기</option>
							</select>
                        </div>
                    </div>
                    <!--/ table 버튼 -->

                    <table class="table list mgt20">
                        <caption>차량등록</caption>
						<colgroup>
							<col style="width:5%" />
							<col style="width:25%" />
							<col style="width:20%" />
							<col style="width:20%" />
							<col style="width:10%" />
							<col style="width:10%" />
							<col style="width:10%" />
						</colgroup>
                        <thead>
                            <tr>
                                <th scope="col">
                                    <input type="checkbox" />
                                </th>
                                <th scope="col">차량정보</th>
                                <th scope="col">년식/주행거리</th>
                                <th scope="col">
                                    <select name="" class="arrow">
										<option value="">선택</option>
										<option value="">VC-200</option>
										<option value="">Von-531</option>
									</select>
                                </th>
                                <th scope="col">
                                    <select name="" class="arrow">
										<option value="">사용구분</option>
										<option value="">사용중</option>
										<option value="">사용중지</option>
									</select>
                                </th>
                                <th scope="col">수정</th>
                                <th scope="col">차량교체</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <input type="checkbox" />
                                </td>
                                <td>
                                    <img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" class="car-img" alt="" /> 쏘나타 63호 5703</td>
                                <td>2014.149724.2</td>
                                <td>
                                    Von-S31
                                    <br />A000876</td>
                                <td>사용중</td>
                                <td>
                                    <button class="btn-img modify">수정</button>
                                </td>
                                <td>
                                    <button class="btn-img change">차량교체</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <!-- 페이징 -->
                    <div id="paging">
                        <a href="" class="first icon-spr">제일 처음으로</a>
                        <a href="" class="prev icon-spr">이전으로</a>
                        <span class="current">1</span>
                        <a href="" class="num">2</a>
                        <a href="" class="num">3</a>
                        <a href="" class="num">4</a>
                        <a href="" class="num">5</a>
                        <a href="" class="next icon-spr">다음으로</a>
                        <a href="" class="last icon-spr">제일 마지막으로</a>
                    </div>
                    <!--/ 페이징 -->
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
