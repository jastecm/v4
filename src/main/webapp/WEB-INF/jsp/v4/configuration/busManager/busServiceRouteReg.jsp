<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script src="https://api2.sktelecom.com/tmap/js?version=1&format=javascript&appKey=43e32609-b337-47d5-b058-7813f232c2d5"></script>
<style>
.ui-widget-overlay{
	z-index : 1200
}
	
</style>
<script type="text/javascript">
var map;
var _busRouteKey = "${busRouteKey}";
$(function(){
	var lazySetting = {
    		thead : [
    	             '운행목적명',
    	             '기능'
    	    ]
    		,bodyHtml : (function(data){
    			var strHtml = "";
    			for(i in data.result){
    				var obj = data.result[i];
    				console.log(obj);
    				strHtml += '<tr>';
    				strHtml += '    <td class="left">';
    				strHtml += '        <input type="radio" name="purposeRadio" data-seq='+obj.seq+' data-drivingpurposename="'+obj.drivingPurposeName+'" />';
    				strHtml += '    <span style="width:170px;">'+obj.drivingPurposeName+'</span>';
    				strHtml += '    </td>';
    				strHtml += '    <td>';
    				strHtml += '        <div class="btn-function mgt0">';
    				strHtml += '            <button type="button" data-seq='+obj.seq+' class="write drivingPurposeUpdateBtn">변경</button>';
    				strHtml += '            <button type="button" data-seq='+obj.seq+' class="delete drivingPurposeDeleteBtn">삭제</button>';
    				strHtml += '        </div>';
    				strHtml += '    </td>';
    				strHtml += '</tr>';
    			}
    			return strHtml;
    		})
    		,limit : 10
    		,offset : 0
    		,url : "/api/1/drivingPurpose"
    		,param : {"counting":true}
    		,http_post_option : {
    			requestMethod : "GET"
    			,header : {key : "${_KEY}"}
    		}
    		,initSearch : true //생성과 동시에 search		
    		,loadingBodyViewer : true //로딩중!! 표시됨
    		,debugMode : true
    	}
	
	
	$("#dispatching-date").datepicker({
        dateformat: 'yy-mm-dd'
    });

	//운행목적 팝업 생성
    $("#bus-dialog").dialog({
        autoOpen: false,
        show: {
            duration: 500
        },
        width: '960',
        modal: true
    });
    //운행목적 팝업 선택시 팝업 오픈 및 운행목적 데이터 셋팅
    $("#drivingPurposePop").on("click", function() {
        $('#drivingPurposeList').commonLazy(lazySetting);
    	
    	$("#bus-dialog").dialog("open");
    });

    $("#bus-map-dialog").dialog({
        autoOpen: false,
        show: {
            duration: 500
        },
        width: '960',
        modal: true
    });

    $("#bus-map-open").on("click", function() {
        $("#bus-map-dialog").dialog("open");
    });
    
    //운행목적 팝업 확인 클릭
    $('#drivingPopConfirmBtn').on('click',function(){
    	console.log(this);
    	var radio = $('#drivingPurposeList input[type="radio"]:checked');
    	
    	if(radio.length != 0){
    		$('#drivingPurposeVal').val(radio.data('drivingpurposename'));
    		$('#drivingPurpose').val(radio.data('seq'));
    	}
    	
    	$("#bus-dialog").dialog("close");
    	
    });
    
    //운행목적 팝업 닫기 이벤트
    $('#drivingPopCancelBtn').on('click',function(){
    	$("#bus-dialog").dialog("close");
    });
    
    //운행목적을 새롭게 추가 하기 위한 input tag 만드는 이벤트
    $('#drivingPurposeAddBtn').on('click',function(){
		 //이미 추가를 하였는지
		 if($('#drivingPurposeList tbody tr').length != 0 ){
			 if($('#drivingPurposeList tbody tr:first td').eq(1).children().is('input')){
				 return;
			 }	 
		 }
		 var strHtml = "";
		 strHtml += '<tr>';
		 strHtml += '    <td class="left">';
		 strHtml += '        <input type="radio" name="purposeRadio"/>';
		 strHtml += '        <input type="text" style="width:170px;"/>';
		 strHtml += '    </td>';
		 strHtml += '    <td>';
		 strHtml += '        <div class="btn-function mgt0">';
		 strHtml += '            <button type="button" class="write drivingPurposeAdd">확인</button>';
		 strHtml += '        </div>';
		 strHtml += '    </td>';
		 strHtml += '</tr>';
		 
		 if($('#drivingPurposeList tbody tr').length == 0 ){
			 $('#drivingPurposeList tbody').append(strHtml);
		 }else{
			 $('#drivingPurposeList tbody tr:first').before(strHtml);	 
		 }
	 });
    
    
    //운행목적 팝업에서 확인 버튼을 누르면 운행목적 추가
    $(document).on('click','.drivingPurposeAdd',function(){
		 var thisObj = $(this);
		 var drivingPurposeName =  thisObj.parent().parent().prev().children('input[type="text"]').val();
		 
		 var sendData = {
		 	"drivingPurposeName" : drivingPurposeName
		 }
		 
		 $V4.http_post("/api/1/drivingPurpose", sendData, {
				requestMethod : "POST",
				header : {
					"key" : "${_KEY}"
				},
				success : function(rtv) {
					console.log(rtv);
					$('#drivingPurposeList').commonLazy(lazySetting);
				},
				error : function(t) {
					console.log(t);
				}
		 });
	 });
    
    //운행목적 수정 버튼 클릭시 수정 처리
    $(document).on('click','.drivingPurposeUpdate',function(){
		 var thisObj = $(this);
		 var drivingPurposeName =  thisObj.parent().parent().prev().children('input[type="text"]').val();
		 var seq = thisObj.data('seq');
		 
		 var sendData = {
			"seq" : seq,
		 	"drivingPurposeName" : drivingPurposeName
		 }
		 
		 $V4.http_post("/api/1/drivingPurpose", sendData, {
				requestMethod : "PUT",
				header : {
					"key" : "${_KEY}"
				},
				success : function(rtv) {
					console.log(rtv);
					$('#drivingPurposeList').commonLazy(lazySetting);
				},
				error : function(t) {
					console.log(t);
				}
		 });
	 });
    
    //운행목적 변경 버튼 클릭
    $(document).on('click','.drivingPurposeUpdateBtn',function(){
		 var self =$(this); 
		 var seq = self.data('seq');
		 
		 var strHtml = "";
		 strHtml += '    <td class="left">';
		 strHtml += '        <input type="radio" data-seq='+seq+' name="purposeRadio"/>';
		 strHtml += '        <input type="text" style="width:170px;" value="'+self.parent().parent().prev().children('span').text()+'"/>';
		 strHtml += '    </td>';
		 strHtml += '    <td>';
		 strHtml += '        <div class="btn-function mgt0">';
		 strHtml += '            <button type="button" class="write drivingPurposeUpdate" data-seq='+seq+'>수정</button>';
		 strHtml += '        </div>';
		 strHtml += '    </td>';
		 
		 self.parent().parent().parent().html(strHtml);
		 
	 });
    
    //운행목적 삭제
    $(document).on('click','.drivingPurposeDeleteBtn',function(){
		 if(confirm('해당 운행목적을 정말 삭제하시겠습니까? 매칭된 하위노선도 삭제 됩니다.')){
			 var seq = $(this).data('seq');
			 $V4.http_post("/api/1/drivingPurpose/"+seq, {}, {
					requestMethod : "DELETE",
					header : {
						"key" : "${_KEY}"
					},
					success : function(rtv) {
						console.log(rtv);
						$('#drivingPurposeList').commonLazy(lazySetting);
					},
					error : function(t) {
						console.log(t);
					}
			 });
		 }
	 });
    
    //버스 운행 목적 팝업 상단 검색 조회 버튼 이벤트
    $('#drivingPurposePopSearchBtn').on('click',function(){
    	var searchText = $('#drivingPurposePopSearchInput').val();
    	
    	lazySetting.param = {
    			"searchText" : searchText
    	}
    	
    	$('#drivingPurposeList').commonLazy(lazySetting);
    });
    
    //버스 운행목적 팝업 상단 검색 이벤트
    $('#drivingPurposePopSearchInput').on('keyup',function(){
    	$('#drivingPurposePopSearchBtn').trigger('click');
    });
    
	$(document).on('click','.fileUploadBtn',function(){
		
		$(this).parent().prev().prev().children('input[type="hidden"]').addClass('targetInput')
		$('#commonUpload').click();
	});
	
	$(document).on('click','.fileUploadBtn2',function(){
		
		$(this).prev().addClass('targetInput');
		$('#commonUpload').click();
	});
	
  //공통 파일 change함수 파일선택 후 확인을 누르면 바로 전송하기 위함 이다.
	$('#commonUpload').on('change',function(){
		
		if(window.FileReader){ 
			var filename = $(this)[0].files[0].name;
		} 
		else {  
			var filename = $(this).val().split('/').pop().split('\\').pop();
		}

		$('.targetInput').prev().val(filename);
		
		$('#fileFrm').submit();
	});
	
	
	//공통 파일 전송 폼
	$('#fileFrm').ajaxForm({
		headers : {
			"key" : "${_KEY}"
		},
		beforeSubmit: function (data,form,option) {
	    	return true;
		}
		,success: function(res,status){
	    	if(status == "success"){
	    		if(res.result.uploadFile == 'fail'){
	    			alert('지원하지 않는 확장명으로 업로드에 실패하였습니다.(jpg,jpeg,png 파일만 가능)');
	    		}else{
	    			var img = res.result.uploadFile;
					if(!img || img.length == 0){

					}else{
						$('.targetInput').val(res.result.uploadFile);
						$('.targetInput').removeClass('targetInput');
					}    
	    		}
	    	}
	    	
	    	$('#commonUpload').val('');
		},
		error: function(){
	    	alert("error");
		}                               
	});
	
	var init = function(){
		//업데이트 일때
		if(_busRouteKey != ""){
			//업데이트로 넘어온거다 ajax을 태우고 노선정보를 가지고 온다.
			$V4.http_post("/api/1/busRouteStation", {"busRouteKey":_busRouteKey} , {
				requestMethod : "GET",
				header : {
					"key" : "${_KEY}"
				},
				success : function(rtv) {
					var busRouteVo = rtv.result[0];
					//여기서 쭉쭉 만들어 줘야 된다.

					//노선명
					$('#busRouteName').val(busRouteVo.busRouteName)
					//운행목적
					$('#drivingPurpose').val(busRouteVo.drivingPurpose);
					$('#drivingPurposeVal').val(busRouteVo.drivingPurposeName);
					//노선도
					$('#busRouteImg').val(busRouteVo.busRouteImg);
					$('#busRouteImgName').val(busRouteVo.busRouteImgName);
					$('#busRouteKeyword').val(busRouteVo.busRouteKeyword);
					
					//0보다 크면
					if(busRouteVo.busStation.length > 0){
						createStationBoxUpdate(busRouteVo.busStation);	
					}
					
				},
				error : function(t) {
					console.log(t);
				}
			});
			
			
			
			
		} else {
			$('#busStationAddBtn').trigger('click');
		}
		
	}
	
	//정류장 추가 html 생성
	var createStationBox = function(){
		var uniqBoxId = _.uniqueId("busStationBox_");
		var uniqMapId = uniqBoxId + "_Map";
		
		var strHtml ="";
		
		strHtml += '<li class="list" id='+uniqBoxId+'>';
		strHtml += '    <input type="hidden" name="lon" />';
		strHtml += '    <input type="hidden" name="lat" />';
		strHtml += '    <div>';
		strHtml += '        <h4 class="tit2">정류장</h4>';
		strHtml += '        <table class="table mgt20">';
		strHtml += '            <caption>정류장</caption>';
		strHtml += '            <colgroup>';
		strHtml += '                <col style="width:14.75%" />';
		strHtml += '                <col style="width:35.25%" />';
		strHtml += '                <col style="width:14.75%" />';
		strHtml += '                <col style="width:35.25%" />';
		strHtml += '            </colgroup>';
		strHtml += '            <tbody>';
		strHtml += '                <tr>';
		strHtml += '                    <th scope="row">정류장명</th>';
		strHtml += '                    <td>';
		strHtml += '                        <input type="text" placeholder="입력" name="stationName" />';
		strHtml += '                    </td>';
		strHtml += '                    <th scope="row">정류장 사진</th>';
		strHtml += '                    <td>';
		strHtml += '                        <input type="text" style="width: 75%;" placeholder="사진 첨부" value="" name="busStationImgName" />';
		strHtml += '                        <input type="hidden" value="" name="stationImg" />';
		strHtml += '                        <button type="button" class="btn btn04 md fileUploadBtn2">파일 선택</button>';
		strHtml += '                    </td>';
		strHtml += '                </tr>';
		strHtml += '                <tr>';
		strHtml += '                    <th scope="row">도착시간</th>';
		strHtml += '                    <td colspan="3">';
		strHtml += '                        <div class="division">';
		strHtml += '                            <div class="col-4">';
		strHtml += '                                <input type="text" placeholder="입력" name="arrivalTime" />';
		strHtml += '                            </div>';
		strHtml += '                            <div class="space">&nbsp;</div>';
		strHtml += '                            <div class="col-8">';
		strHtml += '                                24시 기준으로 작성해주세요. (ex:16:23)';
		strHtml += '                            </div>';
		strHtml += '                        </div>';
		strHtml += '                    </td>';
		strHtml += '                </tr>';
		strHtml += '                <tr>';
		strHtml += '                    <th scope="row">상세주소</th>';
		strHtml += '                    <td colspan="3">';
		strHtml += '                        <div class="division">';
		strHtml += '                            <div class="col-10">';
		strHtml += '                                <input type="text" name="address" />';
		strHtml += '                            </div>';
		strHtml += '                            <div class="space">&nbsp;</div>';
		strHtml += '                            <div class="col-2">';
		strHtml += '                                <button type="button" class="btn btn04 md tMapAddrSearch">주소검색</button>';
		strHtml += '                            </div>';
		strHtml += '                        </div>';
		strHtml += '                    </td>';
		strHtml += '                </tr>';
		strHtml += '            </tbody>';
		strHtml += '        </table>';
		strHtml += '        <div class="mgt40" id='+uniqMapId+'>';
		strHtml += '        </div>';
		strHtml += '        <div class="btn-wrap">';
		strHtml += '            <button type="button" class="up orderUpBtn">해당 정류장 위로 이동</button>';
		strHtml += '            <button type="button" class="down orderDownBtn">해당 정류장 아래로 이동</button>';
		strHtml += '            <button type="button" class="delete stationBoxCloseBtn">해당 정류장 삭제</button>';
		strHtml += '        </div>';
		strHtml += '    </div>';
		strHtml += '</li>';
		
		var obj = {
				"strHtml" : strHtml,
				"uniqMapId" : uniqMapId,
				"uniqBoxId" : uniqBoxId
		}
		return obj;
	}
	
	var createStationBoxUpdate = function(busStationList){
		
		var strHtml ="";
		var objArr = [];
		for(var i = 0 ; i < busStationList.length; i++){
			var busStationVo = busStationList[i];
			var uniqBoxId = _.uniqueId("busStationBox_");
			var uniqMapId = uniqBoxId + "_Map";
			
			strHtml += '<li class="list" id='+uniqBoxId+'>';
			strHtml += '    <input type="hidden" name="lon" value='+busStationVo.lon+' />';
			strHtml += '    <input type="hidden" name="lat" value='+busStationVo.lat+' />';
			strHtml += '    <div>';
			strHtml += '        <h4 class="tit2">정류장</h4>';
			strHtml += '        <table class="table mgt20">';
			strHtml += '            <caption>정류장</caption>';
			strHtml += '            <colgroup>';
			strHtml += '                <col style="width:14.75%" />';
			strHtml += '                <col style="width:35.25%" />';
			strHtml += '                <col style="width:14.75%" />';
			strHtml += '                <col style="width:35.25%" />';
			strHtml += '            </colgroup>';
			strHtml += '            <tbody>';
			strHtml += '                <tr>';
			strHtml += '                    <th scope="row">정류장명</th>';
			strHtml += '                    <td>';
			strHtml += '                        <input type="text" placeholder="입력" name="stationName" value="'+busStationVo.stationName+'" />';
			strHtml += '                    </td>';
			strHtml += '                    <th scope="row">정류장 사진</th>';
			strHtml += '                    <td>';
			strHtml += '                        <input type="text" style="width: 75%;" placeholder="사진 첨부" value="" name="busStationImgName" value="'+busStationVo.stationImgName+'" />';
			strHtml += '                        <input type="hidden" value="" name="stationImg" value='+busStationVo.stationImg+' />';
			strHtml += '                        <button type="button" class="btn btn04 md fileUploadBtn2">파일 선택</button>';
			strHtml += '                    </td>';
			strHtml += '                </tr>';
			strHtml += '                <tr>';
			strHtml += '                    <th scope="row">도착시간</th>';
			strHtml += '                    <td colspan="3">';
			strHtml += '                        <div class="division">';
			strHtml += '                            <div class="col-4">';
			strHtml += '                                <input type="text" placeholder="입력" name="arrivalTime" value='+busStationVo.arrivalTime+' />';
			strHtml += '                            </div>';
			strHtml += '                            <div class="space">&nbsp;</div>';
			strHtml += '                            <div class="col-8">';
			strHtml += '                                24시 기준으로 작성해주세요. (ex:16:23)';
			strHtml += '                            </div>';
			strHtml += '                        </div>';
			strHtml += '                    </td>';
			strHtml += '                </tr>';
			strHtml += '                <tr>';
			strHtml += '                    <th scope="row">상세주소</th>';
			strHtml += '                    <td colspan="3">';
			strHtml += '                        <div class="division">';
			strHtml += '                            <div class="col-10">';
			strHtml += '                                <input type="text" name="address" value="'+busStationVo.address+'" />';
			strHtml += '                            </div>';
			strHtml += '                            <div class="space">&nbsp;</div>';
			strHtml += '                            <div class="col-2">';
			strHtml += '                                <button type="button" class="btn btn04 md tMapAddrSearch">주소검색</button>';
			strHtml += '                            </div>';
			strHtml += '                        </div>';
			strHtml += '                    </td>';
			strHtml += '                </tr>';
			strHtml += '            </tbody>';
			strHtml += '        </table>';
			strHtml += '        <div class="mgt40" id='+uniqMapId+'>';
			strHtml += '        </div>';
			strHtml += '        <div class="btn-wrap">';
			strHtml += '            <button type="button" class="up orderUpBtn">해당 정류장 위로 이동</button>';
			strHtml += '            <button type="button" class="down orderDownBtn">해당 정류장 아래로 이동</button>';
			strHtml += '            <button type="button" class="delete stationBoxCloseBtn">해당 정류장 삭제</button>';
			strHtml += '        </div>';
			strHtml += '    </div>';
			strHtml += '</li>';
			
			var obj = {
					"uniqMapId" : uniqMapId,
					"uniqBoxId" : uniqBoxId,
					"lon" : busStationVo.lon,
					"lat" : busStationVo.lat
			}
			
			objArr.push(obj);
		}
		
		$('#busStationList').append(strHtml);
		
		
		for(var j = 0 ; j < objArr.length; j++){
			var uniqMapId = objArr[j].uniqMapId;
			var uniqBoxId = objArr[j].uniqBoxId;			
			var lon = objArr[j].lon;			
			var lat = objArr[j].lat;			
			var busMap = new tMap();
			busMap.createTmap(uniqMapId,'100%','450px',lon,lat);
			busMap.setLocation(lon,lat);
			$("#"+uniqBoxId).data("mapdata",busMap);	
		}
		
	}
	
	$('#busStationAddBtn').on('click',function(){
		var obj = createStationBox();
		$('#busStationList').append(obj.strHtml);
		//생성한 mapId을 가지고 tmap차트를 만들어야 한다?
		var busMap = new tMap();
		busMap.createTmap(obj.uniqMapId,'100%','450px')
		$("#"+obj.uniqBoxId).data("mapdata",busMap);
	});
	
	$(document).on('click','.tMapAddrSearch',function(){
		var thisObj =  $(this);
		var id = thisObj.parents('li').attr('id')
		
		var _fullText = thisObj.parent().prev().prev().children().val();
		var map = $('#'+id).data('mapdata');
		map.fullTextSearch(_fullText);
	});
	
	//정류장 삭제
	$(document).on('click','.stationBoxCloseBtn',function(){
		$(this).parents('li').remove();
	});
	
	//정렬 순서 위
	$(document).on('click','.orderUpBtn',function(){
		var thisObj = $(this);
		var li = thisObj.parents('li');
		
		if(li.prev().length != 0){
			var up = $(li).transition({ y: '-735px' },1000,'ease');
			var down = $(li).prev().transition({ y: '735px' },1000,'ease');
			
			setTimeout(function(){ 
				li.prev().before(li);
				
				$(up).css("transform","");
				$(down).css("transform","");
			}, 1200);
		}
		
	});
	//정렬순서 아래
	$(document).on('click','.orderDownBtn',function(){
		var thisObj = $(this);
		var li = thisObj.parents('li');
		
		if(li.next().length != 0){
			var up = $(li).transition({ y: '735px' },1000,'ease');
			var down = $(li).next().transition({ y: '-735px' },1000,'ease');
			
			setTimeout(function(){ 
				li.next().after(li);
				
				$(up).css("transform","");
				$(down).css("transform","");
			}, 1000);
		}
	});
	
	$('#busRouteAddBtn').on('click',function(){
		
		var sendData = {
				"drivingPurpose" : "",
				"busRouteName" : "",
				"busRouteImg" : "",
				"busRouteKeyword" : "",
				"stationName" : [],
				"arrivalTime" : [],
				"stationImg" : [],
				"address" : [],
				"lon" : [],
				"lat" : []
		}
		
		
		if( $('#drivingPurposeVal').val() == ""){
			alert("운행목적을 선택해주세요");
			return;
		}else{
			sendData.drivingPurpose = $('#drivingPurpose').val()
		}
		
		if( $('#busRouteName').val() == ""){
			alert("노선명을 입력해주세요");
			return;
		}else{
			sendData.busRouteName = $('#busRouteName').val();
		}
		
		sendData.busRouteImg = $('#busRouteImg').val();
		sendData.busRouteKeyword = $('#busRouteKeyword').val();
		
		var timeRegExp = /^([1-9]|[01][0-9]|2[0-3]):([0-5][0-9])$/;
		
		var busStation = $('#busStationList > li');
		for(var i = 0 ; i < busStation.length ; i++ ){
			var li = $(busStation).eq(i);
			
			if( $(li).find('input[name="stationName"]').val() == "" ){
				alert("정류장명 입력해주세요.");
				return;
			}else{
				sendData.stationName.push($(li).find('input[name="stationName"]').val()); 
			}
			
			if( $(li).find('input[name="arrivalTime"]').val() == "" ){
				alert("도착시간을 입력해주세요.");
				return;
			}else{
				if(!timeRegExp.test($(li).find('input[name="arrivalTime"]').val())){
					alert("시간형식을 정확히 입력해주세요");	
					return;
				}
				sendData.arrivalTime.push($(li).find('input[name="arrivalTime"]').val());
			}
			
			if( $(li).find('input[name="address"]').val() == "" ){
				alert("주소를 입력해주세요");
				return;
			}else{
				sendData.address.push($(li).find('input[name="address"]').val());
			}
			
			if( $(li).find('input[name="busStationImgName"]').val() != "" ){
				sendData.stationImg.push($(li).find('input[name="stationImg"]').val());
			}
			
			
			
			
			var mapData = li.data('mapdata');
			li.children('input[name="lat"]').val(mapData.location.lat);
			li.children('input[name="lon"]').val(mapData.location.lon);
			
			if(li.children('input[name="lat"]').val() == ""){
				alert("주소를 입력해주세요");
				return;
			}else{
				sendData.lon.push($(li).find('input[name="lon"]').val());
				sendData.lat.push($(li).find('input[name="lat"]').val());
			}
		}
        
		$V4.http_post("/api/1/busRouteStation", sendData , {
			requestMethod : "POST",
			header : {
				"key" : "${_KEY}"
			},
			success : function(rtv) {
				console.log(rtv);
				$V4.move('/config/busManager/list');
			},
			error : function(t) {
				console.log(t);
			}
		});
		
	});
	
	$('#busRouteModifyBtn').on('click',function(){
		var sendData = {
				"busRouteKey" : _busRouteKey,
				"drivingPurpose" : "",
				"busRouteName" : "",
				"busRouteImg" : "",
				"busRouteKeyword" : "",
				"stationName" : [],
				"arrivalTime" : [],
				"stationImg" : [],
				"address" : [],
				"lon" : [],
				"lat" : []
		}
		
		
		if( $('#drivingPurposeVal').val() == ""){
			alert("운행목적을 선택해주세요");
			return;
		}else{
			sendData.drivingPurpose = $('#drivingPurpose').val()
		}
		
		if( $('#busRouteName').val() == ""){
			alert("노선명을 입력해주세요");
			return;
		}else{
			sendData.busRouteName = $('#busRouteName').val();
		}
		
		sendData.busRouteImg = $('#busRouteImg').val();
		sendData.busRouteKeyword = $('#busRouteKeyword').val();
		
		var timeRegExp = /^([1-9]|[01][0-9]|2[0-3]):([0-5][0-9])$/;
		
		var busStation = $('#busStationList > li');
		for(var i = 0 ; i < busStation.length ; i++ ){
			var li = $(busStation).eq(i);
			
			if( $(li).find('input[name="stationName"]').val() == "" ){
				alert("정류장명 입력해주세요.");
				return;
			}else{
				sendData.stationName.push($(li).find('input[name="stationName"]').val()); 
			}
			
			if( $(li).find('input[name="arrivalTime"]').val() == "" ){
				alert("도착시간을 입력해주세요.");
				return;
			}else{
				if(!timeRegExp.test($(li).find('input[name="arrivalTime"]').val())){
					alert("시간형식을 정확히 입력해주세요");	
					return;
				}
				sendData.arrivalTime.push($(li).find('input[name="arrivalTime"]').val());
			}
			
			if( $(li).find('input[name="address"]').val() == "" ){
				alert("주소를 입력해주세요");
				return;
			}else{
				sendData.address.push($(li).find('input[name="address"]').val());
			}
			
			var mapData = li.data('mapdata');
			li.children('input[name="lat"]').val(mapData.location.lat);
			li.children('input[name="lon"]').val(mapData.location.lon);
			
			if(li.children('input[name="lat"]').val() == ""){
				alert("주소를 입력해주세요");
				return;
			}else{
				sendData.lon.push($(li).find('input[name="lon"]').val());
				sendData.lat.push($(li).find('input[name="lat"]').val());
			}
		}
        
		$V4.http_post("/api/1/busRouteStation", sendData , {
			requestMethod : "PUT",
			header : {
				"key" : "${_KEY}"
			},
			success : function(rtv) {
				$V4.move('/config/busManager/list');
			},
			error : function(t) {
				console.log(t);
			}
		});
		
	});
	
	init();
	
});
var tMap = function(_uuid, _type) {
	this.uuid = _uuid;
	this.type = _type;
	this.appKey = "43e32609-b337-47d5-b058-7813f232c2d5";
	this.mapId;
	this.map

	this.location = {
		lon : "",
		lat : ""
	}

	this.markerLayer;
	this.marker;

	this.iconSize = new Tmap.Size(24, 38); //아이콘 크기 설정
	this.iconOffset = new Tmap.Pixel(-(this.iconSize.w / 2),
			-(this.iconSize.h));//아이콘 중심점 설정
	this.icon = new Tmap.Icon(
			'http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_a.png',
			this.iconSize, this.iconOffset);//마커 아이콘 설정

	this.setLocation = function(_lon,_lat){
		this.location.lon = _lon;
		this.location.lat = _lat;
	}
			
	this.createTmap = function(_mapId, _width, _height, _lon, _let) {
	
		this.map = new Tmap.Map({
			div : _mapId,
			width : _width,
			height : _height,
		});
		
		this.mapId = _mapId;

		this.markerLayer = new Tmap.Layer.Markers();//맵 레이어 생성
		this.map.addLayer(this.markerLayer);//map에 맵 레이어를 추가합니다.

		if (_lon != undefined) {
			this.marker = new Tmap.Marker(new Tmap.LonLat(_lon, _let).transform("EPSG:4326", "EPSG:3857"), this.icon);//마커 생성
			this.markerLayer.addMarker(this.marker);//레이어에 마커 추가
			this.map.setCenter(new Tmap.LonLat(_lon, _let).transform("EPSG:4326", "EPSG:3857"), 15);
		}

		this.map.events.register("click", this, function(e) {
			this.markerLayer.removeMarker(this.marker); // 기존 마커 삭제
			var lonlat = this.map.getLonLatFromViewPortPx(e.xy).transform("EPSG:3857", "EPSG:4326");//클릭 부분의 ViewPortPx를 LonLat 좌표로 변환합니다.
			this.marker = new Tmap.Marker(lonlat.transform("EPSG:4326","EPSG:3857"), this.icon);//마커 생성
			this.markerLayer.addMarker(this.marker);//마커 레이어에 마커 추가
			
			lonlat = lonlat.transform(new Tmap.Projection("EPSG:3857"),new Tmap.Projection("EPSG:4326"));//좌표를 "EPSG:4326"로 좌표변환한 좌표값으로 설정합니다.
			this.location.lon = lonlat.lon;
			this.location.lat = lonlat.lat;
			
			var address = this.reverseGeo(lonlat.lon,lonlat.lat);
		});

		return this;
	}
			
	this.reverseGeo = function(_lon,_lat){
		$.ajax({
			method:"GET",
			//url:"https://apis.skplanetx.com/tmap/geo/reversegeocoding?version=1&format=xml&callback=result", // ReverseGeocoding api 요청 url입니다.
			url:"https://api2.sktelecom.com/tmap/geo/reversegeocoding?version=1&format=xml&callback=result",
			async:false,
			context : this,
			data:{
				"coordType" : "WGS84GEO", //지구 위의 위치를 나타내는 좌표 타입
				"lon" : _lon,
				"lat" : _lat,
				"appKey" : this.appKey, //실행을 위한 키 입니다. 발급받으신 AppKey를 입력하세요.
			},
			//데이터 로드가 성공적으로 완료되었을 때 발생하는 함수입니다.
			success:function(response){
				prtcl = response;
				
				var prtclString = new XMLSerializer().serializeToString(prtcl);//xml to String	
			    xmlDoc = $.parseXML( prtclString ),
			    $xml = $( xmlDoc ),
			    $intRate = $xml.find("addressInfo");
				
			    var fullAddress;
				$intRate.each(function(index, element) {
					fullAddress = element.getElementsByTagName("fullAddress")[0].childNodes[0].nodeValue;
			    });
				
				var result = fullAddress; 
				$('#'+this.mapId).parent().find('input[name="address"]').val(result);
			},
			//요청 실패시 콘솔창에서 에러 내용을 확인할 수 있습니다.
			error:function(request,status,error){
				console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
			}
		});
	};

	this.fullTextSearch = function(_fullText) {
		$.ajax({
					method : "GET",
					url : "https://api2.sktelecom.com/tmap/geo/fullAddrGeo?version=1&format=xml&callback=result",
					async : false,
					context : this,
					data : {
						"coordType" : "WGS84GEO",//지구 위의 위치를 나타내는 좌표 타입입니다.
						"fullAddr" : _fullText, //주소 정보 입니다, 도로명 주소 표준 표기 방법을 지원합니다.  
						"page" : "1",//페이지 번호 입니다.
						"count" : "20",//페이지당 출력 갯수 입니다.
						"appKey" : this.appKey,//실행을 위한 키 입니다. 발급받으신 AppKey를 입력하세요.
					},
					//데이터 로드가 성공적으로 완료되었을 때 발생하는 함수입니다.
					success : function(response) {
						prtcl = response;

						// 3. 마커 찍기
						var prtclString = new XMLSerializer().serializeToString(prtcl);//xml to String	
						xmlDoc = $.parseXML(prtclString), $xml = $(xmlDoc),
								$intRate = $xml.find("coordinate");

						var lon, lat;
						if ($intRate[0].getElementsByTagName("lon").length > 0) {//구주소
							lon = $intRate[0].getElementsByTagName("lon")[0].childNodes[0].nodeValue;
							lat = $intRate[0].getElementsByTagName("lat")[0].childNodes[0].nodeValue;
						} else {//신주소
							lon = $intRate[0]
									.getElementsByTagName("newLon")[0].childNodes[0].nodeValue;
							lat = $intRate[0]
									.getElementsByTagName("newLat")[0].childNodes[0].nodeValue;
						}

						this.location.lon = lon;
						this.location.lat = lat;

						this.marker = new Tmap.Marker(new Tmap.LonLat(lon, lat).transform("EPSG:4326", "EPSG:3857"), this.icon);//설정한 좌표를 "EPSG:3857"로 좌표변환한 좌표값으로 설정합니다.
						this.markerLayer.addMarker(this.marker);//마커 레이어에 마커 추가

						this.map.setCenter(new Tmap.LonLat(lon, lat).transform("EPSG:4326", "EPSG:3857"), 15);//설정한 좌표를 "EPSG:3857"로 좌표변환한 좌표값으로 즁심점을 설정합니다.
					},
					//요청 실패시 콘솔창에서 에러 내용을 확인할 수 있습니다.
					error : function(request, status, error) {
						console.log("code:" + request.status + "\n"
								+ "message:" + request.responseText + "\n"
								+ "error:" + error);
					}
			});
	}
};
</script>
<form id="fileFrm" action="${defaultPath}/api/1/fileUpload" method="post" enctype="multipart/form-data" class="P10" style="display:none">
	<input type="hidden" name="uploadType" value="busRouteServiceImg"/>
		<div class="btn_file">
			<input type="file" name="uploadFile" accept=".jpg,.jpeg,.png" maxlength="1" id="commonUpload" />
		</div>
</form>
<!-- 본문 -->

        <section id="container">
        	<form id="busRouteStationForm" name="busRouteStationForm" method="post" action="/api/1/busRouteStation">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>버스 노선 등록</h2>
                    <span>통근/주말버스를 포함한 모든 버스와 노선을 관리할 수 있습니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="setting-page">
                    <div class="box-layout register-bus-map">
                        <div class="title">
                            <h3 class="tit3">버스 노선 등록</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
                            <div class="info-txt">
                                각 상세노선에 도착시간 및 상세 정류장 사진을 정확히 넣어주세요.
                            </div>

                            <h4 class="tit2 mgt40">노선 지정</h4>
                            <table class="table mgt20">
                                <caption>노선 지정</caption>
                                <colgroup>
                                    <col style="width:14.75%" />
                                    <col style="width:35.25%" />
                                    <col style="width:14.75%" />
                                    <col style="width:35.25%" />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <th scope="row">운행목적</th>
                                        <td>
                                            <div class="division">
                                                <div class="col-7">
                                                    <input type="text" readonly="readonly" id="drivingPurposeVal" value=""/>
                                                    <input type="hidden" placeholder="입력 또는 검색" id="drivingPurpose" name="drivingPurpose" value="">
                                                </div>
                                                <div class="space">&nbsp;</div>
                                                <div class="col-5">
                                                    <button type="button" class="btn btn04 md" id="drivingPurposePop">검색</button>
                                                </div>
                                            </div>
                                        </td>
                                        <th scope="row">키워드</th>
                                        <td>
                                            <input type="text" placeholder="입력" id="busRouteKeyword" name="busRouteKeyword" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">노선도</th>
                                        <td>
                                            <div class="division">
                                                <div class="col-7">
                                                    <input type="text" placeholder="사진 첨부" id="busRouteImgName" value="" />
													<input type="hidden" name="busRouteImg" id="busRouteImg"/>
                                                </div>
                                                <div class="space">&nbsp;</div>
                                                <div class="col-5">
                                                	<button type="button" class="btn btn04 md fileUploadBtn">파일 선택</button>
                                                    <!-- <button type="button" class="btn btn04 md" id="bus-map-open">노선도 도구</button> -->
                                                </div>
                                            </div>
                                        </td>
                                        <th scope="row">노선명</th>
                                        <td>
                                            <input type="text" placeholder="입력" id="busRouteName" name="busRouteName" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="station">
                                <ul id="busStationList">
                                </ul>
                                <div class="btn-add">
                                    <button type="button" id="busStationAddBtn">
										<span></span>정류장 추가
									</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btn-bottom">
                    <button type="button" class="btn btn02">취소</button>
                    <c:if test="${busRouteKey ne null}">
		    			<button type="button" class="btn btn03" id="busRouteModifyBtn">수정</button>
					</c:if>
					<c:if test="${busRouteKey eq null}">
		    			<button type="button" class="btn btn03" id="busRouteAddBtn">등록</button>
					</c:if>
                    
                </div>
            </div>
            <!--/ 콘텐츠 본문 -->
		</form>
        </section>
        
        <!--/ 본문 -->
<!-- 버스운행목적선택 팝업 -->
    <div id="bus-dialog" title="버스 운행목적 선택">
        <div class="top-info clr">
            <span class="left">운행목적을 선택 및 등록합니다.</span>
        </div>
        <!-- 상단 검색 -->
        <div class="top-search">
            <input type="text" id="drivingPurposePopSearchInput" />
            <button type="button" class="btn btn03" id="drivingPurposePopSearchBtn">조회</button>
        </div>
        <!--/ 상단 검색 -->
        <table class="table list mgt50" id="drivingPurposeList">
            
        </table>

        <div class="btn-table-bottom">
            <div class="right">
                <button type="button" class="btn btn01 md" id="drivingPurposeAddBtn">추가</button>
            </div>
        </div>

        <!-- 하단 버튼 -->
        <div class="btn-bottom">
            <button type="button" class="btn btn02" id="drivingPopCancelBtn">취소</button>
            <button type="button" class="btn btn03" id="drivingPopConfirmBtn">확인</button>
        </div>
        <!--/ 하단 버튼 -->
    </div>
<!-- 버스노선도도구 팝업 -->
    <div id="bus-map-dialog" title="버스 노선도 그림 도구">
        <div class="map clr">
            <div class="left"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d25357.461822122736!2d127.10473640699269!3d37.39733480314603!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x357ca7fe934881c7%3A0x30db2f32566ac8fb!2z6rK96riw64-EIOyEseuCqOyLnCDrtoTri7nqtawg7YyQ6rWQ66Gc!5e0!3m2!1sko!2skr!4v1534812939236"
                    width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>

            <div class="right">
                <h3 class="tit">그리기 도구</h3>
                <ul>
                    <li><input type="radio" /> 경로 그리기</li>
                    <li><input type="radio" /> 경로 삭제</li>
                    <li><input type="radio" /> 정류장 그리기</li>
                    <li><input type="radio" /> 정류장 삭제</li>
                </ul>

                <h3 class="tit mgt30">표시 도구</h3>
                <ul>
                    <li><input type="radio" /> 경로포인트 보이기</li>
                    <li><input type="radio" /> 경로 포인트 감추기</li>
                </ul>

                <button type="button" class="btn btn01 md mgt30">도로 경로 적용</button>
                <button type="button" class="btn btn02 md mgt5">TMAP ROAD API 적용</button>
                <button type="button" class="btn btn02 md mgt5">TMAP ROAD API 취소</button>
            </div>
        </div>

        <div class="mgt20">
            <button type="button" class="btn btn03 md">빨간선 삭제</button>&nbsp;&nbsp;※ 빨간선 삭제 후 화면캡쳐 프로그램을 통해 버스노선도를 캡쳐받으세요.

        </div>
    </div>
    <!--/ 버스노선도도구 팝업 -->