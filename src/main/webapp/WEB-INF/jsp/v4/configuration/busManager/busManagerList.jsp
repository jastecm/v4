<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	$("#start-date").datepicker({
        dateformat: 'yy-mm-dd'
    });
    $("#end-date").datepicker({
        dateformat: 'yy-mm-dd'
    });
    
    $(document).on('click','.page-tab li',function(){
	   	 var tab = $(this).data('tab');
	   	 
	   	 $(this).parent().children().removeClass('on');
	   	 $(this).addClass('on');
	   	 
	   	 if(tab == "tab1"){
			 $('#tab1').show();
			 $('#tab2').hide();
	   	 }else if(tab == "tab2"){
	   		$('#tab1').hide();
			$('#tab2').show(); 
	   	 }
    });
    
    $('#busAllocateBtn').on('click',function(){
    	$V4.move('/config/busManager/allocate/reg');
    });
    
    $('#busServiceRouteBtn').on('click',function(){
    	$V4.move('/config/busManager/serviceRoute/reg');
    });

	var thead = [
	             {'<input type="checkbox" />' : '8%'},
	             {'배차시작일시' : '18%'},
	             {'운행목적' : '16%'},
	             {'노선명' : '22%'},
	             {'차량번호' : '10%'},
	             {'운전자' : '10%'},
	             {'연락처' : '16%'},
	             ];

	var thead2 = [
	              {'<input type="checkbox" />' : '8%'},
	              {'운행목적' : '12%'},
	              {'노선명' : '18%'},
	              {'상세노선' : '32%'},
	              {'기점시간' : '9%'},
	              {'종점시간' : '9%'},
	              {'전체노선도' : '12%'},
	              ];
	
	var busAllocateListSetting = {
			thead : thead
     		,bodyHtml : (function(data){
     			
     			if(!$('#busAllocateCnt').hasClass("used")){
     				$('#busAllocateCnt').addClass("used");
     				$('#busAllocateCnt').text(data.totalCnt);	
     			}
     			
     			var strHtml = "";
     			for(i in data.result){
     				var obj = data.result[i];
     				strHtml += '<tr>';
     				strHtml += '    <td><input type="checkbox" data-busmappingkey='+obj.busMappingKey+' /></td>';
     				
     				var convertDate = convertDateUint(new Date(obj.workingStartDate),_unitDate,_timezoneOffset,1);
     				var date = convertDate.split(' ');
					var dayOfWeek = getWeekDay(date[0]);
					strHtml += '	<td>'+date[0]+"("+dayOfWeek+") "+date[1]+'</td>';
					strHtml += '	<td>'+obj.drivingPurposeName+'</td>';
					strHtml += '	<td>'+obj.busRouteName+'</td>';
					strHtml += '	<td>'+obj.plateNum+'</td>';
					strHtml += '	<td>'+convertNullString(obj.busDriverName)+'</td>';
					strHtml += '	<td>'+convertNullString(obj.busDriverPhone)+'</td>';
     				strHtml += '</tr>';
     			}
     			return strHtml;
     		})
     		,limit : parseInt($(".list01 .func_limitChange").val())
     		,pagePerGroup : 10 //pagingGroup size
     		,url : "/api/{ver}/busAllocate"
     		,param : {
     					'searchOrder' : $(".list01 .func_order").val(),
					 }
     		,http_post_option : {
     			requestMethod : "GET"
     			,header : {key : "${_KEY}"}
     		}
     		,initSearch : true //생성과 동시에 search		
     		,loadingBodyViewer : true //로딩중!! 표시됨
     		,debugMode : true
	}
	
	var busRouteListSetting = {
			thead : thead2
     		,bodyHtml : (function(data){
     			
     			if(!$('#busRouteCnt').hasClass("used")){
     				$('#busRouteCnt').addClass("used");
     				$('#busRouteCnt').text(data.totalCnt);	
     			}
     			
     			
     			var strHtml = "";
     			for(i in data.result){
     				var obj = data.result[i];
     				strHtml += '<tr>';
     				strHtml += '    <td><input type="checkbox" data-busroutekey='+obj.busRouteKey+' /></td>';
     				strHtml += '    <td>'+obj.drivingPurposeName+'</td>';
     				strHtml += '    <td>'+obj.busRouteName+'</td>';
     				strHtml += '    <td class="left">'+obj.stationNameGroup+'</td>';
     				strHtml += '    <td>'+obj.firstArrivalTime+'</td>';
     				strHtml += '    <td>'+obj.lastArrivalTime+'</td>';
     				strHtml += '    <td><button type="button" class="btn-img search busRouteModifyBtn" data-busroutekey='+obj.busRouteKey+'>상세보기</button></td>';
     				strHtml += '</tr>';
     			}
     			return strHtml;
     		})
     		,limit : parseInt($(".list02 .func_limitChange").val())
     		,pagePerGroup : 10 //pagingGroup size
     		,url : "/api/1/busRouteStation"
     		,param : {
     					'searchOrder' : $(".list02 .func_order").val(),
					 }
     		,http_post_option : {
     			requestMethod : "GET"
     			,header : {key : "${_KEY}"}
     		}
     		,initSearch : true //생성과 동시에 search		
     		,loadingBodyViewer : true //로딩중!! 표시됨
     		,debugMode : true
	}
	$('#busAllocateList').commonList(busAllocateListSetting);
	$('#busRouteList').commonList2(busRouteListSetting);
	
	$("#top_btn_search").on("click",$.debounce(200,1,function(){
 		
 		var param = {searchType:$("#top_searchType").val() , searchText : $("#top_searchText").val()}
 		
 		var list = $(".page-tab li.on").hasClass("list01")?"list01":$(".page-tab li.on").hasClass("list02")?"list02":"";
 		
 		if(list){
 			var limit = parseInt($("."+list+" .func_limitChange").val());	 
 			param.searchOrder = $("."+list+" .func_order").val();
 			
 			if(list == "list01"){
 				
 			}else if(list =="list02"){
 				$('#busRouteList').commonList2("setParam",param);
 				$('#busRouteList').commonList2("setLimit",limit).search();
 			}
 		}
 		
 	}));
	
	$(".func_order").on("change",function(){
 		$("#top_btn_search").trigger("click");
 	});
	
	$(".list01 .func_limitChange").on("change",function(){
		$(".searchStart").trigger('change');
 	});
	
	$(".list02 .func_limitChange").on("change",function(){
 		$("#top_btn_search").trigger("click");
 	});
	
	//노선 삭제 버튼.
	$('#busRouteDelBtn').on('click',function(){
		var busRouteKeys = [];
		var checkBox = $('#busRouteList input[type="checkbox"]:checked');
		if(checkBox.length != 0){
			for(var i = 0 ; i < checkBox.length ; i++){
				busRouteKeys.push($(checkBox[i]).data('busroutekey'));
			}
			$V4.http_post("/api/1/busRouteStation/"+busRouteKeys.join(','), {}, {
				requestMethod : "DELETE",
				header : {
					"key" : "${_KEY}"
				},
				success : function(rtv) {
					$("#top_btn_search").trigger("click");
				},
				error : function(t) {
					
				}
			});
		}
	});
	
	$(document).on('click','.busRouteModifyBtn',function(){
		//수정 버튼
		var busRouteKey = $(this).data('busroutekey');
		$V4.move("/config/busManager/serviceRoute/reg", {"busRouteKey" : busRouteKey} );
	});
	
	$(".searchStart,.searchEnd").on("change",function(){
		var param = {};
		var limit = parseInt($(".list01 .func_limitChange").val());	 
		param.searchOrder = $(".list01 .func_order").val();
			
		$(".list01 .searchStart").val()&&(param.searchStartDate = $(".list01 .searchStart").val());
		$(".list01 .searchEnd").val()&&(param.searchEndDate = $(".list01 .searchEnd").val());	
		
		$('#busAllocateList').commonList("setParam",param);
		$('#busAllocateList').commonList("setLimit",limit).search();
 	});
	
	$("div.date button").on("click",function(){
		var list = $(".page-tab li.on").hasClass("list01")?"list01":$(".page-tab li.on").hasClass("list02")?"list02":"";
		$(this).closest("div").find("button").removeClass("active");
		$(this).addClass("active");
		var d = $(this).data("day");
		
		if(d == ""){
			$("."+list+" .datePicker").val("");
			$("."+list+" .searchStart,."+list+" .searchEnd").val("");
			
			$(".searchStart").trigger('change');
		}else{
			var rtvStartDate = calcDate(new Date() , 'd' , 0 , new Date());
			var rtvEndDate = calcDate(new Date() , 'd' , 0 , new Date());
			
			if(d == "-0D"){
			}else if(d == "-7D") rtvStartDate = calcDate(new Date() , 'd' , -7 , new Date());
			else if(d == "-1M") rtvStartDate = calcDate(new Date() , 'm' , -1 , new Date());
			else if(d == "-3M") rtvStartDate = calcDate(new Date() , 'm' , -3 , new Date());
			else if(d == "-6M") rtvStartDate = calcDate(new Date() , 'm' , -6 , new Date());
			
			$("."+list+" .datePicker").eq(0).datepicker('setDate', rtvStartDate);
			$("."+list+" .datePicker").eq(1).datepicker('setDate', rtvEndDate);
			
			var v = $("."+list+" .datePicker").eq(0).datepicker( "getDate" ).getTime();
    		var t = $("."+list+" .datePicker").eq(0).data("target");
    		$(t).val(v);
    		
    		var v = $("."+list+" .datePicker").eq(1).datepicker( "getDate" ).getTime();
    		var t = $("."+list+" .datePicker").eq(1).data("target");
    		$(t).val(v);
    		
    		$(t).trigger("change");
		}
	});
	
	$('#busMappingDeleteBtn').on('click',function(){
		var busMappingKeys = [];
		var checkBox = $('#busAllocateList input[type="checkbox"]:checked');
		if(checkBox.length != 0){
			for(var i = 0 ; i < checkBox.length ; i++){
				busMappingKeys.push($(checkBox[i]).data('busmappingkey'));
			}
			$V4.http_post("/api/1/busAllocate/"+busMappingKeys.join(','), {}, {
				requestMethod : "DELETE",
				header : {
					"key" : "${_KEY}"
				},
				success : function(rtv) {
					$(".searchStart").trigger('change');
				},
				error : function(t) {
					
				}
			});
		}
	});
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>버스 노선 등록</h2>
                    <span>통근/주말버스를 포함한 모든 버스와 노선을 관리할 수 있습니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="setting-page">
                    <div class="">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                            <div class="item">
                                <span>
									등록 배차 건수
									<strong id="busAllocateCnt">0</strong>개
								</span>
                                <span>
									등록 버스 노선 수
									<strong id="busRouteCnt">0</strong>개
								</span>
                            </div>

                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <!-- 메뉴 -->
                        <ul class="page-tab col-4">
                            <li class="list01 on" data-tab="tab1">
                                <a href="javascript:void(0);">배차등록</a>
                            </li>
                            <li class="list02" data-tab="tab2">
                                <a href="javascript:void(0);">노선등록</a>
                            </li>
                        </ul>
                        <!--/ 메뉴 -->

						<div id="tab1">
							 <!-- 배차등록 -->
	                        <div class="box-register">
	                            버스노선 등록하신 후 배차 등록을 진행해주세요.
	                            <br />배차등록시 동일차종의 중복배차 검증을 꼭 진행하셔야 합니다.
	                            <div class="right">
	                                <button type="button" class="btn btn01" id="busAllocateBtn">배차 등록</button>
	                            </div>
	                        </div>
	                        <!--/ 배차등록 -->
	
	                        <div class="section01 ">
	                            <!-- table 버튼 -->
	                            <div class="list01 btn-function row-2">
	                                <div class="left">
	                                    <select class="func_order">
											<option value="desc">최근 일자순</option>
											<option value="asc">과거 일자순</option>
											<option value="purpose">운행 목적별</option>
											<option value="route">노선명순</option>
											<option value="platenum">차량번호순</option>
										</select>
	
	                                    <div class="date" id="changeDate">
	                                        <button type="button" class="active" data-day="">전체</button>
	                                        <button type="button" data-day="-0D">오늘</button>
	                                        <button type="button" data-day="-7D">1주일</button>
	                                        <button type="button" data-day="-1M">1개월</button>
	                                        <button type="button" data-day="-3M">3개월</button>
	                                        <button type="button" data-day="-6M">6개월</button>
	                                        <div class="direct">
	                                            <input type="text" class="datePicker" data-target="#list01_searchStart"/> &nbsp;~&nbsp;
	                                            <input type="hidden" id="list01_searchStart" class="searchStart"/>
	                                            <input type="text" class="datePicker" data-target="#list01_searchEnd"/>
	                                            <input type="hidden" id="list01_searchEnd" class="searchEnd"/>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="right">
	                                    <button type="button" class="delete" id="busMappingDeleteBtn">선택삭제</button>
	                                    <select class="func_limitChange">
											<option value="5" selected>5건씩 보기</option>
											<option value="10">10건씩 보기</option>
											<option value="20">20건씩 보기</option>
											<option value="50">20건씩 보기</option>
										</select>
	                                </div>
	                            </div>
	                            <!--/ table 버튼 -->
	
	                            <table class="table list mgt20" id="busAllocateList">
	                                
	                            </table>
	
	                         
	                        </div>
						</div>
						
						<div id="tab2" style="display:none;">
							<!-- 배차등록 -->
	                        <div class="box-register">
	                            운행목적별 버스노선을 등록해주세요.
	                            <div class="right">
	                                <button type="button" class="btn btn01" id="busServiceRouteBtn">노선 등록</button>
	                            </div>
	                        </div>
	                        <!--/ 배차등록 -->
	
	                        <!-- 상단 검색 -->
	                        <h3 class="tit2 mgt30">버스 노선 목록</h3>
	                        <div class="top-search mgt20">
	                            <select id="top_searchType">
									<option value="">선택</option>
									<option value="route">노선명</option>
									<option value="station">정류장명</option>
								</select>
	                            <!-- <span class="search-tit">정류장명</span> -->
	                            <input type="text" id="top_searchText" />
	                            <button type="button" class="btn btn03" id="top_btn_search">조회</button>
	                        </div>
	                        <!--/ 상단 검색 -->
	
	
	                        <!-- table 버튼 -->
	                        <div class="list02 btn-function">
	                            <div class="left">
	                                <select class="func_order">
										<option value="purpose">운행 목적순</option>
										<option value="route">노선명순</option>
										<option value="first">기점시간순</option>
										<option value="last">종점시간순</option>
									</select>
	                            </div>
	                            <div class="right">
	                                <button class="delete" id="busRouteDelBtn">선택삭제</button>
	                                <select class="func_limitChange">
										<option value="5" selected>5건씩 보기</option>
										<option value="10">10건씩 보기</option>
										<option value="20">20건씩 보기</option>
										<option value="50">20건씩 보기</option>
									</select>
	                            </div>
	                        </div>
	                        <!--/ table 버튼 -->
	
	                        <table class="table list mgt20" id="busRouteList">
	                        </table>
						</div>


                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->