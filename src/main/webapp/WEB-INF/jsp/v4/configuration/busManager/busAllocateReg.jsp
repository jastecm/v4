<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	$("#allocateDate").datepicker({
        dateformat: 'yy-mm-dd'
    });
	
	var lazySetting = {
			thead : [ '운행목적명', '기능' ],
			bodyHtml : (function(data) {
				var strHtml = "";
				for (i in data.result) {
					var obj = data.result[i];
					console.log(obj);
					strHtml += '<tr>';
					strHtml += '    <td class="left">';
					strHtml += '        <input type="radio" name="purposeRadio" data-seq='+obj.seq+' data-drivingpurposename="'+obj.drivingPurposeName+'" />';
					strHtml += '    <span style="width:170px;">'
							+ obj.drivingPurposeName + '</span>';
					strHtml += '    </td>';
					strHtml += '    <td>';
					strHtml += '        <div class="btn-function mgt0">';
					strHtml += '            <button type="button" data-seq='+obj.seq+' class="write drivingPurposeUpdateBtn">변경</button>';
					strHtml += '            <button type="button" data-seq='+obj.seq+' class="delete drivingPurposeDeleteBtn">삭제</button>';
					strHtml += '        </div>';
					strHtml += '    </td>';
					strHtml += '</tr>';
				}
				return strHtml;
			}),
			limit : 10,
			offset : 0,
			url : "/api/1/drivingPurpose",
			param : {
				"counting" : true
			},
			http_post_option : {
				requestMethod : "GET",
				header : {
					key : "${_KEY}"
				}
			},
			initSearch : true //생성과 동시에 search		
			,
			loadingBodyViewer : true //로딩중!! 표시됨
			,
			debugMode : true
		}

		//운행목적 팝업 생성
		$("#bus-dialog").dialog({
			autoOpen : false,
			show : {
				duration : 500
			},
			width : '960',
			modal : true
		});

		$("#drivingPurposePop").on("click", function() {
			$('#drivingPurposeList').commonLazy(lazySetting);

			$("#bus-dialog").dialog("open");
		});

		//운행목적 팝업 확인 클릭
		$('#drivingPopConfirmBtn').on('click', function() {
			console.log(this);
			var radio = $('#drivingPurposeList input[type="radio"]:checked');

			if (radio.length != 0) {
				$('#drivingPurposeVal').val(radio.data('drivingpurposename'));
				//$('#drivingPurpose').val(radio.data('seq'));
				var drivingPurpose = radio.data('seq')
				//여기서 확인을 누르면 노선명을 가져 와야한다.!?
				
				$V4.http_post("/api/1/busRouteStation", {"drivingPurpose" : drivingPurpose}, {
					requestMethod : "GET",
					header : {
						"key" : "${_KEY}"
					},
					success : function(rtv) {
						var list = rtv.result;
						$('#busRouteList').empty();
						var strHtml = ""; 
						for(var i = 0 ; i < list.length;i++){
							var obj = list[i];
							strHtml += '<option value="'+obj.busRouteKey+'">'+obj.busRouteName+'&nbsp;&nbsp;&nbsp;출발 : '+obj.firstArrivalTime+'~'+obj.lastArrivalTime+'</option>';
						}
						$('#busRouteList').html(strHtml);
						$("#bus-dialog").dialog("close");
					},
					error : function(t) {
						
					}
				});
			}

		});
		
		//운행목적 팝업 닫기 이벤트
		$('#drivingPopCancelBtn').on('click', function() {
			$("#bus-dialog").dialog("close");
		});
		
		//운행목적을 새롭게 추가 하기 위한 input tag 만드는 이벤트
	    $('#drivingPurposeAddBtn').on('click',function(){
			 //이미 추가를 하였는지
			 if($('#drivingPurposeList tbody tr').length != 0 ){
				 if($('#drivingPurposeList tbody tr:first td').eq(1).children().is('input')){
					 return;
				 }	 
			 }
			 var strHtml = "";
			 strHtml += '<tr>';
			 strHtml += '    <td class="left">';
			 strHtml += '        <input type="radio" name="purposeRadio"/>';
			 strHtml += '        <input type="text" style="width:170px;"/>';
			 strHtml += '    </td>';
			 strHtml += '    <td>';
			 strHtml += '        <div class="btn-function mgt0">';
			 strHtml += '            <button type="button" class="write drivingPurposeAdd">확인</button>';
			 strHtml += '        </div>';
			 strHtml += '    </td>';
			 strHtml += '</tr>';
			 
			 if($('#drivingPurposeList tbody tr').length == 0 ){
				 $('#drivingPurposeList tbody').append(strHtml);
			 }else{
				 $('#drivingPurposeList tbody tr:first').before(strHtml);	 
			 }
		 });
	    
	    
	    //운행목적 팝업에서 확인 버튼을 누르면 운행목적 추가
	    $(document).on('click','.drivingPurposeAdd',function(){
			 var thisObj = $(this);
			 var drivingPurposeName =  thisObj.parent().parent().prev().children('input[type="text"]').val();
			 
			 var sendData = {
			 	"drivingPurposeName" : drivingPurposeName
			 }
			 
			 $V4.http_post("/api/1/drivingPurpose", sendData, {
					requestMethod : "POST",
					header : {
						"key" : "${_KEY}"
					},
					success : function(rtv) {
						console.log(rtv);
						$('#drivingPurposeList').commonLazy(lazySetting);
					},
					error : function(t) {
						console.log(t);
					}
			 });
		 });
	    
	    //운행목적 수정 버튼 클릭시 수정 처리
	    $(document).on('click','.drivingPurposeUpdate',function(){
			 var thisObj = $(this);
			 var drivingPurposeName =  thisObj.parent().parent().prev().children('input[type="text"]').val();
			 var seq = thisObj.data('seq');
			 
			 var sendData = {
				"seq" : seq,
			 	"drivingPurposeName" : drivingPurposeName
			 }
			 
			 $V4.http_post("/api/1/drivingPurpose", sendData, {
					requestMethod : "PUT",
					header : {
						"key" : "${_KEY}"
					},
					success : function(rtv) {
						console.log(rtv);
						$('#drivingPurposeList').commonLazy(lazySetting);
					},
					error : function(t) {
						console.log(t);
					}
			 });
		 });
	    
	    //운행목적 변경 버튼 클릭
	    $(document).on('click','.drivingPurposeUpdateBtn',function(){
			 var self =$(this); 
			 var seq = self.data('seq');
			 
			 var strHtml = "";
			 strHtml += '    <td class="left">';
			 strHtml += '        <input type="radio" data-seq='+seq+' name="purposeRadio"/>';
			 strHtml += '        <input type="text" style="width:170px;" value="'+self.parent().parent().prev().children('span').text()+'"/>';
			 strHtml += '    </td>';
			 strHtml += '    <td>';
			 strHtml += '        <div class="btn-function mgt0">';
			 strHtml += '            <button type="button" class="write drivingPurposeUpdate" data-seq='+seq+'>수정</button>';
			 strHtml += '        </div>';
			 strHtml += '    </td>';
			 
			 self.parent().parent().parent().html(strHtml);
			 
		 });
	    
	    //운행목적 삭제
	    $(document).on('click','.drivingPurposeDeleteBtn',function(){
			 if(confirm('해당 운행목적을 정말 삭제하시겠습니까? 매칭된 하위노선도 삭제 됩니다.')){
				 var seq = $(this).data('seq');
				 $V4.http_post("/api/1/drivingPurpose/"+seq, {}, {
						requestMethod : "DELETE",
						header : {
							"key" : "${_KEY}"
						},
						success : function(rtv) {
							console.log(rtv);
							$('#drivingPurposeList').commonLazy(lazySetting);
						},
						error : function(t) {
							console.log(t);
						}
				 });
			 }
		 });
	    
	    //버스 운행 목적 팝업 상단 검색 조회 버튼 이벤트
	    $('#drivingPurposePopSearchBtn').on('click',function(){
	    	var searchText = $('#drivingPurposePopSearchInput').val();
	    	
	    	lazySetting.param = {
	    			"searchText" : searchText
	    	}
	    	
	    	$('#drivingPurposeList').commonLazy(lazySetting);
	    });
	    
	    //버스 운행목적 팝업 상단 검색 이벤트
	    $('#drivingPurposePopSearchInput').on('keyup',function(){
	    	$('#drivingPurposePopSearchBtn').trigger('click');
	    });
	    
	    $('#allocateDateAddBtn').on('click',function(){
			
			var allocateDate = $('#allocateDate').val();
			
			if(allocateDate != ""){
				//중복으로 들어 가면안된다 중복체크 해주어야 한다.
				var divList = $('#allocateDateList span');
				
				for(var i = 0 ; i < divList.length ; i++){
					var date = $(divList[i]).data('allocatedate');
					//같으면 추가 되지 않도록 한다
					if(allocateDate == date){
						$('#allocateDate').val('');
						return;
					}
				}
				
				var week = ['일', '월', '화', '수', '목', '금', '토'];
				var dayOfWeek = week[new Date(allocateDate).getDay()];
				
				var strHtml = '<span data-allocatedate="'+allocateDate+'">';
				strHtml += allocateDate+'('+dayOfWeek+')&nbsp;'
				strHtml += '<button class="delete-x btn_reset_action">삭제</button>'
				strHtml += '</span>'
				
				$('#allocateDateList').append(strHtml);
				$('#allocateDate').val('');
				
				$('#allocateDateList').addClass('active');
			}
		});
	    
	    $('body').on('click','.btn_reset_action',function(){
			$(this).parent().remove();
			var divList = $('#allocateDateList span');
			if(divList.length == 0){
				$('#allocateDateList').removeClass('active');
			}
		});
	    
	    $('body').on('click','.btn_reset_action2',function(){
			$(this).parent().remove();
			var divList = $('#vehicleListDiv span');
			if(divList.length == 0){
				$('#vehicleListDiv').removeClass('active');
			}
		});
	    
	    $('body').on('click','.btn_reset_action3',function(){
			$(this).parent().parent().remove();
		});
	    
	    $('#vehiclePop').each(function(i){
			$(this).data({
				type : "vehicle",
				http_post_option : {
					header : {
						key : "${_KEY}"
					}
				},
				selType : "checkbox",
				action : {
					ok : function(arrSelect, inputObj, valueObj, _this) {
						var arrDe = $(arrSelect).map(function(i,v) {
							var info = base64ToJsonObj($(v).data('info'));
							console.log(info);
							var plateNum = info.plateNum;
							var vehicleKey = info.vehicleKey;
							
							var divList = $('#vehicleListDiv span');
							var flag = true;
			    			for(var i = 0 ; i < divList.length ; i++){
			    				var key = $(divList[i]).data('vehiclekey');
								//같은것이 있다.
			    				if(vehicleKey == key){
			    					flag = false;
			    				}
			    			}
			    			if(flag){
			    							    			
				    			var strHtml = '<span data-platenum="'+plateNum+'" data-vehiclekey="'+vehicleKey+'">';
				    			strHtml += plateNum+'&nbsp;';
								strHtml += '<button class="delete-x btn_reset_action2">삭제</button>'
								strHtml += '</span>'
				    			
				    			$('#vehicleListDiv').append(strHtml);
								$('#vehicleListDiv').addClass('active');
			    			}
							
							
						});			
					}
				},
				targetInput : $("#test"),
				targetInputValue : $("#test")
			}).commonPop();
		});
	    
	    $('#resetBtn').on('click',function(){
			$('#allocateDateList').empty();
			$('#vehicleListDiv').empty();
			$('#drivingPurposeVal').val('');
			$('#busRouteList').empty();
			//$('#checkList').empty();
		});
	    
	    $('#allocateCheckBtn').on('click',function(){
			var allocateDateList = $('#allocateDateList span');
			var vehicleListDiv = $('#vehicleListDiv span');
			if(allocateDateList.length == 0){
				alert('배차일자를 선택해주세요');
				return;
			}
			
			var busRouteKey = $('#busRouteList').val();
			if(busRouteKey == null || busRouteKey == ""){
				alert('노선을 선택해주세요');
				return;
			}
			
			if(vehicleListDiv.length == 0){
				alert('차량을 선택해주세요');
				return;
			}
			var dateList = [];
			var vehicleList = [];
			var plateNumList = [];
			for(var i = 0 ; i < allocateDateList.length ; i++){
				dateList.push($(allocateDateList[i]).data('allocatedate'));
			}
			
			for(var j = 0 ; j < vehicleListDiv.length ; j++){
				vehicleList.push($(vehicleListDiv[j]).data('vehiclekey'));
				plateNumList.push($(vehicleListDiv[j]).data('platenum'));
			}
			
			var week = ['일', '월', '화', '수', '목', '금', '토'];
			
			$('#checkList').empty();
			for(var k = 0 ; k < dateList.length; k++){
				for(var l = 0 ; l < vehicleList.length; l++){
					
					var dayOfWeek = week[new Date(dateList[k]).getDay()];
					
					var strHtml = '<tr data-busroutekey='+busRouteKey+' data-allocatedate='+dateList[k]+' data-vehiclekey='+vehicleList[l]+'>';
					strHtml += '<td>'+dateList[k]+"("+dayOfWeek+")"+'</td>';
					strHtml += '<td>'+$('#drivingPurposeVal').val()+'</td>';
					strHtml += '<td>'+$('#busRouteList option:selected').text()+'</td>';
					strHtml += '<td>'+plateNumList[l]+'</td>';
					strHtml += '<td>검증 중</td>';
					strHtml += '</tr>';
					$('#checkList').append(strHtml);
					
					var sendData = {
							"busRouteKey" : busRouteKey,
							"allocateDate" : dateList[k],
							"vehicleKey" : vehicleList[l]
					}
					$V4.http_post("/api/1/busAllocateCheck", sendData, {
						sync : true,
						requestMethod : "GET",
						header : {
							"key" : "${_KEY}"
						},
						success : function(rtv) {
							var tdlast = $('#checkList tr:last td:last')
							if(rtv.result == "OK"){
								tdlast.addClass('success').text('검증 성공');
							}else{
								tdlast.addClass('fail').html('검증 실패 <button type="button" class="delete-x btn_reset_action3">삭제</button>');
							}
						},
						error : function(t) {
							console.log(t);
						}
				 });
				
				}
			}
		});
	    
	    
	  //배차등록
		$('#allocateConfirm').on('click',function(){
			
			if($('#checkList .fail').length != 0){
				alert('실패한 배차는 삭제해주시기 바랍니다.');
				return;
			}
			
			var tr = $('#checkList tr');
			
			if(tr.length == 0){
				alert('등록할 배차가 없습니다');
				return;
			}
			
			for(var i = 0 ; i < tr.length; i++){
				var busRouteKey = $(tr[i]).data('busroutekey');
				var allocateDate = $(tr[i]).data('allocatedate');
				var vehicleKey = $(tr[i]).data('vehiclekey');
				
				var sendData = {
						"busRouteKey" : busRouteKey,
						"allocateDate" : allocateDate,
						"vehicleKey" : vehicleKey
				}
				//서버쪽 요청 만들어서 insert하고
				//중복 체크 하는거 테스트 해야됨.
				
				$V4.http_post("/api/1/busAllocate", sendData, {
					sync : true,
					requestMethod : "POST",
					header : {
						"key" : "${_KEY}"
					},
					success : function(rtv) {
						
					},
					error : function(t) {
						console.log(t);
					}
			   });
			}
			alert('등록 되었습니다.');
			$('#checkList').empty();
			//$('#resetBtn').trigger('click');
		});

	});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>버스 노선 등록</h2>
                    <span>통근/주말버스를 포함한 모든 버스와 노선을 관리할 수 있습니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="setting-page">
                    <div class="box-layout register-bus">
                        <div class="title">
                            <h3 class="tit3">버스 배차등록</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
                            <div class="info-txt">
                                배차등록시 동일차종의 중복배차 검증을 꼭 진행하셔야 합니다.
                            </div>
                            <table class="table mgt20">
                                <caption>버스 배차등록</caption>
                                <tbody>
                                    <tr>
                                        <th scope="row">배차일자</th>
                                        <td>
                                            <div class="division">
                                                <div class="col-3">
                                                    <input type="text" id="allocateDate" />
                                                </div>
                                                <div class="space">&nbsp;</div>
                                                <div class="col-9">
                                                    <button type="button" class="btn btn04 md" id="allocateDateAddBtn">추가</button>
                                                </div>
                                            </div>
                                            <div class="selected" id="allocateDateList">
                                            	<!-- <span>2018/08/01(수)<button class="delete-x">삭제</button></span>
                                            	&nbsp;&nbsp;&nbsp;2018/08/01(수)<button class="delete-x">삭제</button>
                                            	&nbsp;&nbsp;&nbsp;2018/08/01(수)<button class="delete-x">삭제</button> -->
                                           	</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">운행목적 선택</th>
                                        <td>
                                        	<div class="division">
                                                <div class="col-3">
                                                    <input type="text" id="drivingPurposeVal" />

                                                </div>
                                                <div class="space">&nbsp;</div>
                                                <div class="col-9">
                                                    <button type="button" class="btn btn04 md" id="drivingPurposePop">선택</button>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">노선명 선택</th>
                                        <td>
                                            <!-- <button type="button" class="btn btn04 md">선택</button>
                                            <div class="selected"><b>노선명 선택</b> [주간]평일출근<button class="delete-x">삭제</button> -->
                                            <select id="busRouteList">
                                            	
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">차량 선택</th>
                                        <td>
                                            <button type="button" class="btn btn04 md" id="vehiclePop">선택</button>
                                            <!-- <div class="selected">42바 2022<button class="delete-x">삭제</button>&nbsp;&nbsp;&nbsp; 44바 2222<button class="delete-x">삭제</button>&nbsp;&nbsp;&nbsp; 12나 3455<button class="delete-x">삭제</button></div> -->
                                            <div class="selected" id="vehicleListDiv">
                                            
                                            </div>
                                            
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <!-- 하단 버튼 -->
                            <div class="btn-bottom">
                                <button type="button" class="btn btn02" id="resetBtn">초기화</button>
                                <button type="button" class="btn btn03" id="allocateCheckBtn">배차 검증 시작</button>
                            </div>
                            <!--/ 하단 버튼 -->

                            <table class="table list mgt40">
                                <caption>배차 목록 검증결과</caption>
                                <colgroup>
                                    <col style="width:19%" />
                                    <col style="width:19%" />
                                    <col style="width:24%" />
                                    <col style="width:19%" />
                                    <col style="width:19%" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th scope="col">배차일자</th>
                                        <th scope="col">운행목적</th>
                                        <th scope="col">노선명</th>
                                        <th scope="col">차량번호</th>
                                        <th scope="col">검증결과</th>
                                    </tr>
                                </thead>
                                <tbody id="checkList">
                                    <!-- <tr>
                                        <td>2018/8/24(목)</td>
                                        <td>[주간] 평일출근</td>
                                        <td>동문삼성@(오스카빌경유)</td>
                                        <td>57나 7544</td>
                                        <td>검증중</td>
                                    </tr>
                                    <tr>
                                        <td>2018/8/24(목)</td>
                                        <td>[주간] 평일출근</td>
                                        <td>동문삼성@(오스카빌경유)</td>
                                        <td>57나 7544</td>
                                        <td class="success">검증 성공</td>
                                    </tr>
                                    <tr>
                                        <td>2018/8/24(목)</td>
                                        <td>[주간] 평일출근</td>
                                        <td>동문삼성@(오스카빌경유)</td>
                                        <td>57나 7544</td>
                                        <td class="fail">검증 실패 <button class="delete-x">삭제</button></td>
                                    </tr> -->
                                </tbody>
                            </table>

                            <!-- 하단 버튼 -->
                            <div class="btn-bottom">
                                <button type="button" class="btn btn02">취소</button>
                                <button type="button" class="btn btn03" id="allocateConfirm">배차 등록</button>
                            </div>
                            <!--/ 하단 버튼 -->
                            </div>
                            </div>
                        </div>
                        <!--/ 콘텐츠 본문 -->
                    </div>
        </section>
        <!--/ 본문 -->
 <!-- 버스운행목적선택 팝업 -->
    <div id="bus-dialog" title="버스 운행목적 선택">
        <div class="top-info clr">
            <span class="left">운행목적을 선택 및 등록합니다.</span>
        </div>
        <!-- 상단 검색 -->
        <div class="top-search">
            <input type="text" id="drivingPurposePopSearchInput" />
            <button type="button" class="btn btn03" id="drivingPurposePopSearchBtn">조회</button>
        </div>
        <!--/ 상단 검색 -->
        <table class="table list mgt50" id="drivingPurposeList">
            
        </table>

        <div class="btn-table-bottom">
            <div class="right">
                <button type="button" class="btn btn01 md" id="drivingPurposeAddBtn">추가</button>
            </div>
        </div>

        <!-- 하단 버튼 -->
        <div class="btn-bottom">
            <button type="button" class="btn btn02" id="drivingPopCancelBtn">취소</button>
            <button type="button" class="btn btn03" id="drivingPopConfirmBtn">확인</button>
        </div>
        <!--/ 하단 버튼 -->
    </div>
<!-- 버스노선도도구 팝업 -->