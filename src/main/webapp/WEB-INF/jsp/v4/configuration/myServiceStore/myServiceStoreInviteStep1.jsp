<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	$('.table th.more').each(function() {
        $(this).click(function() {
            $(this).parent().toggleClass('active');
        });
    });

    $("#address-dialog").dialog({
        autoOpen: false,
        show: {
            duration: 500
        },
        width: '960',
        modal: true
    });

    $("#address-open").on("click", function() {
        $("#address-dialog").dialog("open");
    });

    var ctx = document.getElementById("Chart01").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    100, 60
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)'
                ]
            }],
            labels: [
                '전체',
                '사용률'
            ]
        }
    });

    var ctx = document.getElementById("Chart02").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    112, 19
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)'
                ]
            }],
            labels: [
                '소나타',
                '그랜저'
            ]
        }
    });

    var ctx = document.getElementById("Chart03").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    10, 50
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)'
                ]
            }],
            labels: [
                '40대',
                '30대'
            ]
        }
    });

    var ctx = document.getElementById("Chart04").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    50, 20
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)'
                ]
            }],
            labels: [
                '여성',
                '남성'
            ]
        }
    });
    
    $('#selectInviteBtn').on('click',function(){
    	$V4.move('/config/myServiceStore/invite/step2');
    });
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>My 서비스 스토어</h2>
                    <span>
						나날이 새로워지는 ViewCAR 서비스 스토어가 되겠습니다.
						<br /> 새로운 서비스의 세계로 고객님을 초대합니다.
					</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="setting-page">
                    <div class="box-layout search-store">
                        <div class="title">
                            <h3 class="tit3">서비스 초대</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
                            <div class="info-txt">
                                고객님의 서비스 조건에 맞는 차량 사용자를 초대하십시오.<br /> 차량 사용자의 서비스 수락으로 차량정보를 실시간으로 확인하실 수 있습니다.
                            </div>

                            <div class="section01">
                                <table class="table mgt20">
                                    <caption>검색</caption>
                                    <tbody>
                                        <tr>
                                            <th scope="row" class="icon-spr more">차종구분</th>
                                            <td>
                                                <div class="division">
                                                    <div class="col-6">
                                                        승용, 버스, 트럭
                                                        <select name="">
															<option value="">전체</option>
														</select>
                                                    </div>
                                                    <div class="space">&nbsp;</div>
                                                    <div class="col-6">
                                                        국내차량, 해외차량
                                                        <select name="">
															<option value="">전체</option>
														</select>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row" class="icon-spr more">제조사</th>
                                            <td class="list">
                                                <div class="top5">
                                                    <span><input type="checkbox" /> 현대</span>
                                                    <span><input type="checkbox" /> 기아</span>
                                                    <span><input type="checkbox" /> 쉐보레</span>
                                                    <span><input type="checkbox" /> 아우디</span>
                                                    <span><input type="checkbox" /> 벤츠</span>
                                                </div>
                                                <div class="line">
                                                    <span><input type="radio" /> 가나다순</span>
                                                    <span><input type="radio" /> 등록차량 많은 순</span>
                                                    <div class="mgt10">
                                                        <span><input type="checkbox" /> 현대</span>
                                                        <span><input type="checkbox" /> 기아</span>
                                                        <span><input type="checkbox" /> 쉐보레</span>
                                                        <span><input type="checkbox" /> 아우디</span>
                                                        <span><input type="checkbox" /> 벤츠</span>
                                                        <span><input type="checkbox" /> 현대</span>
                                                        <span><input type="checkbox" /> 기아</span>
                                                        <span><input type="checkbox" /> 쉐보레</span>
                                                        <span><input type="checkbox" /> 아우디</span>
                                                        <span><input type="checkbox" /> 벤츠</span>
                                                        <span><input type="checkbox" /> 현대</span>
                                                        <span><input type="checkbox" /> 기아</span>
                                                        <span><input type="checkbox" /> 쉐보레</span>
                                                        <span><input type="checkbox" /> 아우디</span>
                                                        <span><input type="checkbox" /> 벤츠</span>
                                                        <span><input type="checkbox" /> 현대</span>
                                                        <span><input type="checkbox" /> 기아</span>
                                                        <span><input type="checkbox" /> 쉐보레</span>
                                                        <span><input type="checkbox" /> 아우디</span>
                                                        <span><input type="checkbox" /> 벤츠</span>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row" class="icon-spr more">모델명</th>
                                            <td class="list">
                                                <div class="top5">
                                                    <span><input type="checkbox" /> 쏘나타</span>
                                                    <span><input type="checkbox" /> 모닝</span>
                                                    <span><input type="checkbox" /> 포터</span>
                                                    <span><input type="checkbox" /> 스파크</span>
                                                    <span><input type="checkbox" /> K3</span>
                                                </div>
                                                <div class="line">
                                                    <span><input type="radio" /> 가나다순</span>
                                                    <span><input type="radio" /> 등록차량 많은 순</span>
                                                    <div class="mgt10">
                                                        <span><input type="checkbox" /> 쏘나타</span>
                                                        <span><input type="checkbox" /> 모닝</span>
                                                        <span><input type="checkbox" /> 포터</span>
                                                        <span><input type="checkbox" /> 스파크</span>
                                                        <span><input type="checkbox" /> K3</span>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">출고일</th>
                                            <td class="list">
                                                <span><input type="checkbox" /> ~3년</span>
                                                <span><input type="checkbox" /> 3~5년</span>
                                                <span><input type="checkbox" /> 5~7년</span>
                                                <span><input type="checkbox" /> 7~10년</span>
                                                <span><input type="checkbox" /> 10년~15년</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">거리</th>
                                            <td class="list">
                                                <div class="division">
                                                    <div class="col-6">
                                                        <input type="text" />
                                                    </div>
                                                    <div class="space">&nbsp;</div>
                                                    <div class="col-6">
                                                        <button class="btn btn03 md">조회</button>
                                                    </div>
                                                </div>
                                                <div class="mgt10">
                                                    <span><input type="checkbox" /> ~10km 이내</span>
                                                    <span><input type="checkbox" /> 10km~30km</span>
                                                    <span><input type="checkbox" /> 30km~50km</span>
                                                    <span><input type="checkbox" /> 50km~70km</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">수리이력</th>
                                            <td class="list">
                                                <span><input type="checkbox" /> 6개월 이내 없음</span>
                                                <span><input type="checkbox" /> 1년 이내 없음</span>
                                                <span><input type="checkbox" /> 2년 이내 없음</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">소모품 교체이력</th>
                                            <td class="list">
                                                <span><input type="checkbox" /> 6개월 이내 없음</span>
                                                <span><input type="checkbox" /> 1년 이내 없음</span>
                                                <span><input type="checkbox" /> 2년 이내 없음</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">소유구분</th>
                                            <td class="list">
                                                <span><input type="checkbox" /> 개인차량</span>
                                                <span>	<input type="checkbox" /> 법인차량</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">보험사</th>
                                            <td class="list">
                                                <span><input type="checkbox" /> 흥국화재</span>
                                                <span><input type="checkbox" /> 삼성화재</span>
                                                <span><input type="checkbox" /> 한화손해보험</span>
                                                <span><input type="checkbox" /> 현대해상</span>
                                                <span><input type="checkbox" /> 기타 등등</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">조건 내 키워드 검색</th>
                                            <td>
                                                <div class="division">
                                                    <div class="col-6">
                                                        <input type="text" />
                                                    </div>
                                                    <div class="space">&nbsp;</div>
                                                    <div class="col-6">
                                                        <button class="btn btn03 md">조회</button>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <button class="btn btn01 closed">상세 조회 접기</button>
                            </div>

                            <!-- table 버튼 -->
                            <div class="btn-function">
                                <div class="right">
                                    <button id="selectInviteBtn" type="button" class="invite">선택항목 초대</button>
                                    <select name="">
										<option value="">5건씩 보기</option>
									</select>
                                </div>
                            </div>
                            <!--/ table 버튼 -->

                            <table class="table list mgt20">
                                <caption></caption>
                                <colgroup>
                                    <col style="width:5%" />
                                    <col style="width:5%" />
                                    <col style="width:12%" />
                                    <col />
                                    <col style="width:11%" />
                                    <col style="width:11%" />
                                    <col style="width:11%" />
                                    <col style="width:11%" />
                                </colgroup>

                                <thead>
                                    <tr>
                                        <th scope="col"><input type="checkbox" /></th>
                                        <th scope="col">No.</th>
                                        <th scope="col">구분</th>
                                        <th scope="col">주소</th>
                                        <th scope="col">등록차량수</th>
                                        <th scope="col">차종</th>
                                        <th scope="col">출고년월</th>
                                        <th scope="col">서비스 등록년월</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>1</td>
                                        <td>자스**</td>
                                        <td class="left">경기도 성남시 분당구 ***</td>
                                        <td>100*</td>
                                        <td>그랜저</td>
                                        <td>2017. 10</td>
                                        <td>2017. 10</td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>1</td>
                                        <td>자스**</td>
                                        <td class="left">경기도 성남시 분당구 ***</td>
                                        <td>100*</td>
                                        <td>그랜저</td>
                                        <td>2017. 10</td>
                                        <td>2017. 10</td>
                                    </tr>
                                </tbody>
                            </table>

                            <h3 class="tit2 mgt30">서비스 통계</h3>

                            <div class="chart-list clr">
                                <div class="list">
                                    <div class="tit">전체 이용자 대비 초대율</div>
                                    <canvas id="Chart01" width="200" height="200"></canvas>
                                </div>
                                <div class="list">
                                    <div class="tit">차종별 사용 비율</div>
                                    <canvas id="Chart02" width="200" height="200"></canvas>
                                </div>
                                <div class="list">
                                    <div class="tit">년령별 사용비율</div>
                                    <canvas id="Chart03" width="200" height="200"></canvas>
                                </div>
                                <div class="list">
                                    <div class="tit">성비별 초대비율</div>
                                    <canvas id="Chart04" width="200" height="200"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->
         <!-- 주소지정 팝업 -->
    <div id="address-dialog" title="주소 지정">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d25357.461822122736!2d127.10473640699269!3d37.39733480314603!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x357ca7fe934881c7%3A0x30db2f32566ac8fb!2z6rK96riw64-EIOyEseuCqOyLnCDrtoTri7nqtawg7YyQ6rWQ66Gc!5e0!3m2!1sko!2skr!4v1534812939236"
            width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        <!-- 하단 버튼 -->
        <div class="btn-bottom">
            <button class="btn btn03">확인</button>
        </div>
        <!--/ 하단 버튼 -->
    </div>
    <!--/ 주소지정 팝업 -->