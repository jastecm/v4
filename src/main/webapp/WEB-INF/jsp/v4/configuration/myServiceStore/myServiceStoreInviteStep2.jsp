<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
});
</script>
 <!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>My 서비스 스토어</h2>
                    <span>
						나날이 새로워지는 ViewCAR 서비스 스토어가 되겠습니다.
						<br /> 새로운 서비스의 세계로 고객님을 초대합니다.
					</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="setting-page">
                    <div class="box-layout invite-store">
                        <div class="title">
                            <h3 class="tit3">서비스 초대</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
                            <h3 class="tit2">초대 서비스 선택</h3>
                            <table class="table mgt20">
                                <caption>초대 서비스 선택</caption>
                                <tbody>
                                    <tr>
                                        <th scope="row">초대 서비스 선택</th>
                                        <td>
                                            <select name="">
												<option value="">1. 위험ㆍ심야운전 알림</option>
											</select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <h3 class="tit2 mgt30">초대 인원</h3>
                            <table class="table mgt20">
                                <caption>초대 인원</caption>
                                <tbody>
                                    <tr>
                                        <th scope="row">초대 인원</th>
                                        <td>
                                            총 240명
                                            <button class="btn btn02 md">리스트 보기</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="booking">
                                <strong>바른정비 실시간 진단 및 정비예약</strong>
                                <span class="maker">제작자 : ㈜바름파트너스</span>
                                <div class="info-wrap">
                                    <div class="img">
                                        <img src="./img/img2.jpg" alt="" />
                                        <select name="" style="width:250px">
											<option value="">가입 : 100원/월</option>
										</select>
                                        <select name="" style="width:250px">
											<option value="">가입 : 무료</option>
										</select>
                                    </div>
                                    <div class="info">
                                        내차의 고장 유무 및 고장진단 결과를 확인하여 정비 항목과 금액을 실시간으로 알려줍니다.
                                        <br /> 내차의 고장 유무 및 고장진단 결과를 확인하여 정비 항목과 금액을 실시간으로 알려줍니다.
                                        <br /> 내차의 고장 유무 및 고장진단 결과를 확인하여 정비 항목과 금액을 실시간으로 알려줍니다.
                                        <br />
                                    </div>
                                </div>
                            </div>

                            <h3 class="tit2 mgt30">빵빵 서비스 혜택</h3>
                            <ul class="type-01">
                                <li>매달 차량위치까지 방문하여 순회점검해드립니다.</li>
                                <li>1년 사용시 정비 공임 30% 할인 혜택 드립니다.</li>
                                <li>실시간 차량 고장유무를 모니터링하여 놓치기 쉬운 법인차량 관리의 품격을 높여드립니다.</li>
                            </ul>

                            <h3 class="tit2 mgt30">사용자 초대 이용료 결제</h3>
                            <ul class="type-01 payment">
                                <li><span>현재 보유 포인트</span>100,000 Ⓟ</li>
                                <li><span>결제할 포인트</span>12,700 Ⓟ (총 240명 * 30Ⓟ /건 ) <button class="btn btn02 md">포인트 충전</button></li>
                                <li><span>결재후 보유포인트</span>82,000 Ⓟ </li>
                            </ul>

                            <!-- 하단 버튼 -->
                            <div class="btn-bottom">
                                <button class="btn btn02">취소</button>
                                <button class="btn btn03">초대</button>
                            </div>
                            <!--/ 하단 버튼 -->
                        </div>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->