<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	$("#notice-dialog").dialog({
        autoOpen: true,
        show: {
            duration: 500
        },
        width: '480',
        modal: true
    });

    $("#notice-open").on("click", function() {
        $("#notice-dialog").dialog("open");
    });
    
    $('#serviceStoreInviteBtn').on('click',function(){
    	$V4.move('/config/myServiceStore/invite/step1');
    });
    
    $('#serviceRegBtn').on('click',function(){
    	$V4.move('/config/myServiceStore/reg');
    });
    var ctx = document.getElementById("Chart01").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    100, 60
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)'
                ]
            }],
            labels: [
                '전체',
                '사용률'
            ]
        }
    });

    var ctx = document.getElementById("Chart02").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    112, 19
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)'
                ]
            }],
            labels: [
                '소나타',
                '그랜저'
            ]
        }
    });

    var ctx = document.getElementById("Chart03").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    10, 50
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)'
                ]
            }],
            labels: [
                '40대',
                '30대'
            ]
        }
    });

    var ctx = document.getElementById("Chart04").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    50, 20
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)'
                ]
            }],
            labels: [
                '여성',
                '남성'
            ]
        }
    });

    var ctx = document.getElementById("Chart05").getContext('2d');
    var myBarChart = new Chart(ctx, {
        type: 'bar',
        data: {
            datasets: [{
                data: [
                    20, 30, 40, 15, 20, 30, 40, 15, 20, 30, 40, 15, 20, 30, 40, 15, 20, 30, 40, 15, 20, 30, 40, 15
                ],
                backgroundColor: [
                    'rgba(161, 80, 204, 1)',
                    'rgba(161, 80, 204, 1)',
                    'rgba(161, 80, 204, 1)',
                    'rgba(161, 80, 204, 1)',
                    'rgba(161, 80, 204, 1)',
                    'rgba(161, 80, 204, 1)',
                    'rgba(161, 80, 204, 1)',
                    'rgba(161, 80, 204, 1)',
                    'rgba(161, 80, 204, 1)',
                    'rgba(161, 80, 204, 1)',
                    'rgba(161, 80, 204, 1)',
                    'rgba(161, 80, 204, 1)',
                    'rgba(161, 80, 204, 1)',
                    'rgba(161, 80, 204, 1)',
                    'rgba(161, 80, 204, 1)',
                    'rgba(161, 80, 204, 1)',
                    'rgba(161, 80, 204, 1)',
                    'rgba(161, 80, 204, 1)',
                    'rgba(161, 80, 204, 1)',
                    'rgba(161, 80, 204, 1)',
                    'rgba(161, 80, 204, 1)',
                    'rgba(161, 80, 204, 1)',
                    'rgba(161, 80, 204, 1)',
                    'rgba(161, 80, 204, 1)'
                ],
                label: '시간대별'
            }],
            labels: [
                '00:00',
                '01:00',
                '02:00',
                '03:00',
                '04:00',
                '05:00',
                '06:00',
                '07:00',
                '08:00',
                '09:00',
                '10:00',
                '11:00',
                '12:00',
                '13:00',
                '14:00',
                '15:00',
                '16:00',
                '17:00',
                '18:00',
                '19:00',
                '20:00',
                '21:00',
                '22:00',
                '23:00',
                '24:00'
            ]
        },
        options: {
            scales: {
                xAxes: [{
                    stacked: true,
                }],
                yAxes: [{
                    stacked: true
                }]
            }
        }
    });

    var ctx = document.getElementById("Chart06").getContext('2d');
    var myBarChart = new Chart(ctx, {
        type: 'bar',
        data: {
            datasets: [{
                data: [
                    20, 30, 40, 15, 20, 30, 40
                ],
                backgroundColor: [
                    'rgba(255, 0, 0, 1)',
                    'rgba(161, 80, 204, 1)',
                    'rgba(161, 80, 204, 1)',
                    'rgba(161, 80, 204, 1)',
                    'rgba(161, 80, 204, 1)',
                    'rgba(161, 80, 204, 1)',
                    'rgba(0, 0, 255, 1)',
                ],
                label: '요일별'
            }],
            labels: [
                '일',
                '월',
                '화',
                '수',
                '목',
                '금',
                '토'
            ]
        },
        options: {
            scales: {
                xAxes: [{
                    stacked: true,
                }],
                yAxes: [{
                    stacked: true
                }]
            }
        }
    });
});

</script>
 <!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>My 서비스 스토어</h2>
                    <span>
						ViewCAR 서비스 스토어에서 보유하신 솔루션을 등록하세요.
						<br /> 차량을 좀더 편리하게 좀더 안전하게 만드는 많은 서비스 등록을 기대합니다.
					</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="setting-page">
                    <div class="store-list">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                            <div class="item">
                                <span>
									총 서비스
									<strong>10</strong>건
								</span>
                                <span>
									등록건수
									<strong>8</strong>건
								</span>
                                <span>
									등록대기
									<strong>2</strong>건
								</span>
                            </div>

                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <!-- 거래처등록 -->
                        <h3 class="tit2 mgt30">신규 등록</h3>
                        <div class="box-register">
                            신규 서비스 등록후 ViewCAR와의 연동협의를 위해 담당자가 확인후 연락드리겠습니다.
                            <br /> 등록하신 서비스는 법규 준수 및 품질, 원할한 고객 대응을 위해 등록심의 절차 필요합니다.

                            <div class="right">
                                <button id="serviceStoreInviteBtn" type="button" class="btn btn02">서비스 초대</button>
                                <button id="serviceRegBtn" type="button" class="btn btn01">신규 등록</button>
                            </div>
                        </div>
                        <!--/ 거래처등록 -->

                        <!-- 등록된 서비스 리스트 -->
                        <div class="section01 ">
                            <h3 class="tit2">등록된 서비스 리스트</h3>
                            <!-- table 버튼 -->
                            <div class="btn-function title-side">
                                <div class="right">
                                    <select name="">
										<option value="">5건씩 보기</option>
									</select>
                                </div>
                            </div>
                            <!--/ table 버튼 -->

                            <table class="table list mgt20">
                                <caption>등록된 서비스 리스트</caption>
                                <colgroup>
                                    <col style="width:8%" />
                                    <col style="width:20%" />
                                    <col />
                                    <col style="width:10%" />
                                    <col style="width:10%" />
                                    <col style="width:13%" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th scope="col">구분</th>
                                        <th scope="col">서비스명</th>
                                        <th scope="col">서비스 설명</th>
                                        <th scope="col">등록일</th>
                                        <th scope="col">서비스 개시일</th>
                                        <th scope="col">상태</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>알림</td>
                                        <td>
                                            <a href="">위험ㆍ심야운전 알림</a>
                                        </td>
                                        <td class="left">사용자의 위험운전(급출발, 급가속, 급감속, 급정지,급회전)과 심야운전(밤12시~04시 운행) 발생시 (공유)관리자에게 자동 통보합니다.</td>
                                        <td>2018.05.01</td>
                                        <td>2018.05.01</td>
                                        <td>
                                            <div class="states-value state02">서비스중</div>
                                            <div class="btn-wrap">
                                                <button id="notice-open">중지</button>
                                                <button>삭제</button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>정비</td>
                                        <td>
                                            <a href="">운행스케쥴ㆍ거래처 추가</a>
                                        </td>
                                        <td class="left">차량의 거래처를 5개이상 등록을 원할 경우 위 서비스를 신청해주세요. 거래처 등록, 관리가 필요한 생수기 렌탈, 거래처 물품 배송업체에 적합합니다. </td>
                                        <td>2018.05.01</td>
                                        <td>2018.05.01</td>
                                        <td>
                                            <div class="states-value state03">서비스 대기</div>
                                            <div class="btn-wrap">
                                                <button>개시</button>
                                                <button>중지</button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>정비</td>
                                        <td>
                                            <a href="">운행스케쥴ㆍ거래처 추가</a>
                                        </td>
                                        <td class="left">차량의 거래처를 5개이상 등록을 원할 경우 위 서비스를 신청해주세요. 거래처 등록, 관리가 필요한 생수기 렌탈, 거래처 물품 배송업체에 적합합니다. </td>
                                        <td>2018.05.01</td>
                                        <td>2018.05.01</td>
                                        <td>
                                            <div class="states-value state06">서비스 중지</div>
                                            <div class="btn-wrap">
                                                <button>개시</button>
                                                <button>삭제</button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>정비</td>
                                        <td>
                                            <a href="">운행스케쥴ㆍ거래처 추가</a>
                                        </td>
                                        <td class="left">차량의 거래처를 5개이상 등록을 원할 경우 위 서비스를 신청해주세요. 거래처 등록, 관리가 필요한 생수기 렌탈, 거래처 물품 배송업체에 적합합니다. </td>
                                        <td>2018.05.01</td>
                                        <td>2018.05.01</td>
                                        <td>
                                            <div class="states-value state05">등록대기</div>
                                            <div class="btn-wrap">
                                                <button>수정</button>
                                                <button>삭제</button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>정비</td>
                                        <td>
                                            <a href="">운행스케쥴ㆍ거래처 추가</a>
                                        </td>
                                        <td class="left">차량의 거래처를 5개이상 등록을 원할 경우 위 서비스를 신청해주세요. 거래처 등록, 관리가 필요한 생수기 렌탈, 거래처 물품 배송업체에 적합합니다. </td>
                                        <td>2018.05.01</td>
                                        <td>2018.05.01</td>
                                        <td>
                                            <div class="states-value state04">등록승인</div>
                                            <div class="btn-wrap">
                                                <button>계약동의</button>
                                                <button>삭제</button>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <!-- 페이징 -->
                            <div id="paging">
                                <a href="" class="first icon-spr">제일 처음으로</a>
                                <a href="" class="prev icon-spr">이전으로</a>
                                <span class="current">1</span>
                                <a href="" class="num">2</a>
                                <a href="" class="num">3</a>
                                <a href="" class="num">4</a>
                                <a href="" class="num">5</a>
                                <a href="" class="next icon-spr">다음으로</a>
                                <a href="" class="last icon-spr">제일 마지막으로</a>
                            </div>
                            <!--/ 페이징 -->
                        </div>
                        <!--/ 등록된 서비스 리스트 -->

                        <!-- 등록 서비스 Q&A -->
                        <h3 class="tit2 mgt30">등록 서비스 Q&amp;A</h3>
                        <!-- table 버튼 -->
                        <div class="btn-function title-side">
                            <div class="right">
                                <select name="">
									<option value="">5건씩 보기</option>
								</select>
                            </div>
                        </div>
                        <!--/ table 버튼 -->

                        <table class="table list mgt20">
                            <caption>등록된 서비스 현황</caption>
                            <colgroup>
                                <col style="width:8%" />
                                <col style="width:8%" />
                                <col style="width:12%" />
                                <col style="width:18%" />
                                <col />
                                <col style="width:13%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">선택</th>
                                    <th scope="col">등록일시</th>
                                    <th scope="col">서비스명</th>
                                    <th scope="col">고객 질문</th>
                                    <th scope="col">상태</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>2</td>
                                    <td>알림</td>
                                    <td>2018.05.01 13:00</td>
                                    <td>
                                        <a href="">위험ㆍ심야운전 알림</a>
                                    </td>
                                    <td class="left">
                                        사용자의 위험운전(급출발, 급가속, 급감속, 급정지,급회전)과 심야운전(밤12시~04시 운행) 발생시 (공유)관리자에게 자동 통보합니다.
                                        <div class="reply">
                                            차량의 거래처를 5개이상 등록을 원할 경우 위 서비스를 신청해주세요. 거래처 등록, 관리가 필요한 생수기 렌탈, 거래처 물품 배송업체에 적합합니다.
                                        </div>
                                    </td>
                                    <td>
                                        <div class="states-value state05">답변완료</div>
                                        <div class="btn-wrap">
                                            <button>수정</button>
                                            <button>삭제</button>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>알림</td>
                                    <td>2018.05.01 13:00</td>
                                    <td>
                                        <a href="">위험ㆍ심야운전 알림</a>
                                    </td>
                                    <td class="left">
                                        차량의 거래처를 5개이상 등록을 원할 경우 위 서비스를 신청해주세요. 거래처 등록, 관리가 필요한 생수기 렌탈, 거래처 물품 배송업체에 적합합니다.
                                        <div class="insert-reply">
                                            <textarea></textarea>
                                        </div>
                                    </td>
                                    <td class="insert-reply">
                                        <button class="states-value state06">답변등록</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!--/ 등록 서비스 Q&A -->

                        <!-- 페이징 -->
                        <div id="paging">
                            <a href="" class="first icon-spr">제일 처음으로</a>
                            <a href="" class="prev icon-spr">이전으로</a>
                            <span class="current">1</span>
                            <a href="" class="num">2</a>
                            <a href="" class="num">3</a>
                            <a href="" class="num">4</a>
                            <a href="" class="num">5</a>
                            <a href="" class="next icon-spr">다음으로</a>
                            <a href="" class="last icon-spr">제일 마지막으로</a>
                        </div>
                        <!--/ 페이징 -->

                        <!-- 서비스 통계 -->
                        <h3 class="tit2 mgt30">서비스 통계</h3>
                        <div class="chart-list clr">
                            <div class="list">
                                <div class="tit">전체 이용자 대비 서비스 사용률</div>
                                <canvas id="Chart01" width="200" height="200"></canvas>
                            </div>
                            <div class="list">
                                <div class="tit">차종별 사용 비율</div>
                                <canvas id="Chart02" width="200" height="200"></canvas>
                            </div>
                            <div class="list">
                                <div class="tit">년령별 사용비율</div>
                                <canvas id="Chart03" width="200" height="200"></canvas>
                            </div>
                            <div class="list">
                                <div class="tit">성비별 초대비율</div>
                                <canvas id="Chart04" width="200" height="200"></canvas>
                            </div>
                        </div>

                        <div class="chart-list col-2 clr">
                            <div class="list">
                                <div class="tit">사용빈도 시간대별</div>
                                <canvas id="Chart05" width="400" height="200"></canvas>
                            </div>
                            <div class="list">
                                <div class="tit">사용빈도 요일</div>
                                <canvas id="Chart06" width="400" height="200"></canvas>
                            </div>
                        </div>
                        <!--/ 서비스 통계 -->
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->
        <!-- 사용자알림 팝업 -->
    <div id="notice-dialog" title="사용자 알림">
        <div class="info-txt">
            ‘등록한 서비스를 정말로 취소(정지)하시겠습니까?<br /> 지우신 서비스는 복원되지 않습니다
        </div>
        <!-- 하단 버튼 -->
        <div class="btn-bottom">
            <button class="btn btn03">동의</button>
            <button class="btn btn02">취소</button>
        </div>
        <!--/ 하단 버튼 -->
    </div>
    <!--/ 사용자알림 팝업 -->