<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>My 서비스 스토어</h2>
                    <span>
						ViewCAR 서비스 스토어에서 보유하신 솔루션을 등록하세요.
						<br /> 차량을 좀더 편리하게 좀더 안전하게 만드는 많은 서비스 등록을 기대합니다.
					</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="setting-page">
                    <div class="box-layout register-store">
                        <div class="title">
                            <h3 class="tit3">신규 서비스 등록</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
                            <table class="table">
                                <caption>신규 서비스 등록 입력</caption>
                                <colgroup>
                                    <col style="width:14.75%" />
                                    <col style="width:35.25%" />
                                    <col style="width:14.75%" />
                                    <col style="width:35.25%" />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <th scope="row">제작사</th>
                                        <td>(주)자스텍엠</td>
                                        <th scope="row">도메인</th>
                                        <td>www.jastecm.com</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">서비스 유형</th>
                                        <td colspan="3">
                                            <input type="radio" name="" />&nbsp;알림&nbsp;&nbsp;&nbsp;
                                            <input type="radio" name="" />&nbsp;경고&nbsp;&nbsp;&nbsp;
                                            <input type="radio" name="" />&nbsp;정비&nbsp;&nbsp;&nbsp;
                                            <input type="radio" name="" />&nbsp;보험&nbsp;&nbsp;&nbsp;
                                            <input type="radio" name="" />&nbsp;세차&nbsp;&nbsp;&nbsp;
                                            <input type="radio" name="" />&nbsp;기타
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">과금방식</th>
                                        <td colspan="3">
                                            <input type="radio" name="" />&nbsp;1차량/월&nbsp;&nbsp;&nbsp;
                                            <input type="radio" name="" />&nbsp;1건당/일&nbsp;&nbsp;&nbsp;
                                            <input type="radio" name="" />&nbsp;다운로드 건수
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">서비스 요금</th>
                                        <td colspan="3">
                                            <div class="text-end">
                                                <input type="text" name="" />
                                                <span class="text">원 or (달러)</span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">제목</th>
                                        <td colspan="3">
                                            <div class="text-end">
                                                <input type="text" name="" />
                                                <span class="text">(30byte이내)</span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">등록 이미지</th>
                                        <td colspan="3">
                                            <div class="insert-img">
                                                <ul>
                                                    <li>
                                                        <img src="./img/common/user-img2.png" alt="" />
                                                        <div>
                                                            <button class="btn btn02 sm">등록</button>
                                                            <button class="btn btn03 sm">삭제</button>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <img src="./img/common/user-img2.png" alt="" />
                                                        <div>
                                                            <button class="btn btn02 sm">등록</button>
                                                            <button class="btn btn03 sm">삭제</button>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <img src="./img/common/user-img2.png" alt="" />
                                                        <div>
                                                            <button class="btn btn02 sm">등록</button>
                                                            <button class="btn btn03 sm">삭제</button>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">설명</th>
                                        <td colspan="3">
                                            <textarea rows="3" class="w100"></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            혜택
                                            <br />(초대문구)
                                        </th>
                                        <td colspan="3">
                                            <textarea rows="2" class="w100"></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">시스템 정보</th>
                                        <td class="table" colspan="3">
                                            <table>
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">지원OS</th>
                                                        <td>
                                                            <input type="radio" name="" />&nbsp;Internet Explorer&nbsp;&nbsp;&nbsp;
                                                            <input type="radio" name="" />&nbsp;Microsoft Edge&nbsp;&nbsp;&nbsp;
                                                            <input type="radio" name="" />&nbsp;Chome&nbsp;&nbsp;&nbsp;
                                                            <input type="radio" name="" />&nbsp;Safari&nbsp;&nbsp;&nbsp;
                                                            <input type="radio" name="" />&nbsp;Fox
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">언어</th>
                                                        <td>
                                                            <input type="radio" name="" />&nbsp;English&nbsp;&nbsp;&nbsp;
                                                            <input type="radio" name="" />&nbsp;Korean&nbsp;&nbsp;&nbsp;
                                                            <input type="radio" name="" />&nbsp;Chinese
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">업그레이드 정보</th>
                                        <td colspan="3">
                                            <textarea rows="2" class="w100"></textarea>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <!-- 하단 버튼 -->
                            <div class="btn-bottom">
                                <button class="btn btn02">취소</button>
                                <button class="btn btn03">등록</button>
                            </div>
                            <!--/ 하단 버튼 -->
                        </div>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->