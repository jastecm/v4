<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	$("#select-car-dialog").dialog({
        autoOpen: false,
        show: {
            duration: 500
        },
        width: '960',
        modal: true
    });

    $("#select-car-open").on("click", function() {
        $("#select-car-dialog").dialog("open");
    });
    
    //TODO: 48 71 ServiceSotreController.java 참조 
    //갯수와 월결제에 따라 가입화면이 달라야 한다.
    //$V4.move('/config/serviceStore/month');
    //$V4.move('/config/serviceStore/number');
    
});
</script>
 <!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>서비스 스토어</h2>
                    <span>ViewCAR를 좀더 매력있게 만드는 개방형 서비스 스토어입니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="setting-page">
                    <div class="store-list">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                            <div class="item">
                                <span>
									등록 서비스
									<strong>15</strong>건
								</span>
                                <span>
									부가 서비스
									<strong>3</strong>건
								</span>
                                <span>
									제휴 서비스
									<strong>12</strong>건
								</span>
                            </div>

                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <!-- 상단 검색 -->
                        <div class="top-search">
                            <select name="">
								<option value="">전체</option>
								<option value="">알림</option>
								<option value="">경고</option>
								<option value="">정비</option>
								<option value="">보험</option>
								<option value="">세차</option>
							</select>
                            <input type="text" name="" />
                            <button class="btn btn03">조회</button>
                        </div>
                        <!--/ 상단 검색 -->

                        <!-- 등록된 서비스 현황 -->
                        <div class="section01 ">
                            <h3 class="tit2">등록된 서비스 현황</h3>
                            <!-- table 버튼 -->
                            <div class="btn-function title-side">
                                <div class="right">
                                    <select name="">
										<option value="">5건씩 보기</option>
									</select>
                                </div>
                            </div>
                            <!--/ table 버튼 -->

                            <table class="table list mgt20">
                                <caption>등록된 서비스 현황</caption>
                                <colgroup>
                                    <col style="width:8%" />
                                    <col style="width:8%" />
                                    <col style="width:20%" />
                                    <col style="width:10%" />
                                    <col />
                                    <col style="width:8%" />
                                    <col style="width:13%" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th scope="col">구분</th>
                                        <th scope="col">선택</th>
                                        <th scope="col">서비스명</th>
                                        <th scope="col">등록자</th>
                                        <th scope="col">서비스 설명</th>
                                        <th scope="col">사용요금</th>
                                        <th scope="col">추천</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>제휴</td>
                                        <td>정비</td>
                                        <td>
                                            <a href="">바름정비 정비진단</a>
                                        </td>
                                        <td>바름파트너스</td>
                                        <td class="left">내차의 고장 유무 및 고장진단 결과를 확인하여 정비 항목과 금액을 실시간으로 알려줌</td>
                                        <td>무료</td>
                                        <td>
                                            <div class="grade-star">
                                                <span class="on"></span>
                                                <span class="on"></span>
                                                <span class="on"></span>
                                                <span class="on"></span>
                                                <span class="on"></span>
                                            </div>
                                            <div class="states-value state01" id="select-car-open">가입</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>제휴</td>
                                        <td>정비</td>
                                        <td>
                                            <a href="">바름정비 정비진단</a>
                                        </td>
                                        <td>바름파트너스</td>
                                        <td class="left">내차의 고장 유무 및 고장진단 결과를 확인하여 정비 항목과 금액을 실시간으로 알려줌</td>
                                        <td>무료</td>
                                        <td>
                                            <div class="grade-star">
                                                <span class="on"></span>
                                                <span class="on"></span>
                                                <span class="on"></span>
                                                <span></span>
                                                <span></span>
                                            </div>
                                            <div class="states-value state02">사용중</div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <!-- 페이징 -->
                            <div id="paging">
                                <a href="" class="first icon-spr">제일 처음으로</a>
                                <a href="" class="prev icon-spr">이전으로</a>
                                <span class="current">1</span>
                                <a href="" class="num">2</a>
                                <a href="" class="num">3</a>
                                <a href="" class="num">4</a>
                                <a href="" class="num">5</a>
                                <a href="" class="next icon-spr">다음으로</a>
                                <a href="" class="last icon-spr">제일 마지막으로</a>
                            </div>
                            <!--/ 페이징 -->
                        </div>
                        <!--/ 등록된 서비스 현황 -->

                        <!-- 내가 사용중인 부가 ∙ 제휴서비스 -->
                        <div class="section02">
                            <h3 class="tit2">내가 사용중인 부가 ∙ 제휴서비스</h3>
                            <table class="table list mgt20">
                                <caption>내가 사용중인 부가 ∙ 제휴서비스</caption>
                                <colgroup>
                                    <col style="width:8%" />
                                    <col style="width:8%" />
                                    <col style="width:20%" />
                                    <col />
                                    <col style="width:10%" />
                                    <col style="width:8%" />
                                    <col style="width:13%" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th scope="col">구분</th>
                                        <th scope="col">선택</th>
                                        <th scope="col">서비스명</th>
                                        <th scope="col">서비스 설명</th>
                                        <th scope="col">가입일</th>
                                        <th scope="col">잔여일</th>
                                        <th scope="col">비고</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>제휴</td>
                                        <td>정비</td>
                                        <td>
                                            <a href="">바름정비 정비진단</a>
                                        </td>
                                        <td class="left">내차의 고장 유무 및 고장진단 결과를 확인하여 정비 항목과 금액을 실시간으로 알려줌</td>
                                        <td>2018.05.01</td>
                                        <td>35일</td>
                                        <td>
                                            <div class="states-value state03">정지중</div>
                                            <div class="btn-wrap">
                                                <button>연장</button>
                                                <button>취소</button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>제휴</td>
                                        <td>정비</td>
                                        <td>
                                            <a href="">바름정비 정비진단</a>
                                        </td>
                                        <td class="left">내차의 고장 유무 및 고장진단 결과를 확인하여 정비 항목과 금액을 실시간으로 알려줌</td>
                                        <td>2018.05.01</td>
                                        <td>35일</td>
                                        <td>
                                            <div class="states-value state02">사용중</div>
                                            <div class="btn-wrap">
                                                <button>연장</button>
                                                <button>취소</button>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <!-- 페이징 -->
                            <div id="paging">
                                <a href="" class="first icon-spr">제일 처음으로</a>
                                <a href="" class="prev icon-spr">이전으로</a>
                                <span class="current">1</span>
                                <a href="" class="num">2</a>
                                <a href="" class="num">3</a>
                                <a href="" class="num">4</a>
                                <a href="" class="num">5</a>
                                <a href="" class="next icon-spr">다음으로</a>
                                <a href="" class="last icon-spr">제일 마지막으로</a>
                            </div>
                            <!--/ 페이징 -->
                        </div>
                        <!--/ 내가 사용중인 부가 ∙ 제휴서비스 -->
                    </div>
                </div>
            </div>
            <!--/ 콘텐츠 본문 -->
    </section>
    <!--/ 본문 -->