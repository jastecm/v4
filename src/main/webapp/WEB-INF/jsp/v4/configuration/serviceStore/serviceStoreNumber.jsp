<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>서비스 스토어</h2>
                    <span>ViewCAR를 좀더 매력있게 만드는 개방형 서비스 스토어입니다. 차량을 좀더 안전하게 좀더 편리하게 만드는 다양한 서버스를 만나보세요. </span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="setting-page">
                    <div class="box-layout store-detail store-detail-type2">
                        <div class="title">
                            <h3 class="tit3">운행스케쥴 거래처 추가 서비스</h3>
                            <span class="badge">알림</span>
                            <div class="grade-star">
                                <span class="on"></span>
                                <span class="on"></span>
                                <span class="on"></span>
                                <span></span>
                                <span></span>
                            </div>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
                            <!-- 서비스 등록자 노출 -->
                            <div class="section01">
                                
                                    <div class="img">
                                        <img src="${pageContext.request.contextPath}/common/new/img/img.jpg" alt="" />
                                    </div>
                                    <div class="info">
                                        <h4 class="tit2">서비스 등록자 : 바름파트너스</h4>
                                        <p>
                                            사용자의 위험운전(급출발, 급가속, 급감속, 급정지,급회전)과 심야운전(밤12시~04시 운행) 발생시
                                            <br /> (공유)관리자에게 자동 통보합니다.
                                            <br /> 이 서비스를 통해 미성년 운전자의 사고를 사전에 예방할 수 있습니다.
                                        </p>
                                        <a href="" class="more">더보기</a>
                                    </div>
                                
                                <div class="use-list">
                                    <table class="table list">
                                        <caption></caption>
                                        <thead>
                                            <tr>
                                                <th scope="col">서비스 부과기준</th>
                                                <th scope="col">추가수량</th>
                                                <th scope="col">이용요금</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>지도기반 위치서비스</td>
                                                <td>
                                                    10개
                                                    <button class="minus">-</button>
                                                    <button class="plus">+</button>
                                                </td>
                                                <td>11,000</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="copy">
                                        <span class="price icon-spr">가입 : 1,000원/월</span>
                                        <button>링크복사</button>
                                        <span class="state">가입완료</span>
                                    </div>
                                </div>
                            </div>
                            <!--/ 서비스 등록자 노출 -->

                            <!-- 사용정보 -->
                            <div class="section02">
                                <h4 class="tit2">사용정보</h4>
                                <ul class="clr">
                                    <li>
                                        <img src="${pageContext.request.contextPath}/common/new/img/img.jpg" alt="" />
                                        <span>이미지 설명입니다. 이미지 설명입니다. 이미지 설명입니다. 이미지 설명입니다. </span>
                                    </li>
                                    <li>
                                        <img src="${pageContext.request.contextPath}/common/new/img/img.jpg" alt="" />
                                        <span>이미지 설명입니다. 이미지 설명입니다. 이미지 설명입니다. 이미지 설명입니다. </span>
                                    </li>
                                    <li>
                                        <img src="${pageContext.request.contextPath}/common/new/img/img.jpg" alt="" />
                                        <span>이미지 설명입니다. 이미지 설명입니다. 이미지 설명입니다. 이미지 설명입니다. </span>
                                    </li>
                                </ul>
                            </div>
                            <!--/ 사용정보 -->

                            <!-- 사용자 평가 -->
                            <div class="section03 clr">
                                <h4 class="tit2">사용자 평가</h4>
                                <div class="user-assessment">
                                    <div class="grade-star">
                                        <span class="on"></span>
                                        <span class="on"></span>
                                        <span class="on"></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                    <span class="member">
										(총
										<strong>16명</strong>
										평가 기준)
									</span>

                                    <div class="result">
                                        <div class="tit">※ 평가별점</div>
                                        <div class="list">
                                            <span>최고</span>
                                            <div class="grade-star">
                                                <span class="on"></span>
                                                <span class="on"></span>
                                                <span class="on"></span>
                                                <span class="on"></span>
                                                <span class="on"></span>
                                            </div>
                                            <div class="bar point-2"></div>
                                            <span>2</span>
                                        </div>
                                        <div class="list">
                                            좋음
                                            <div class="grade-star">
                                                <span class="on"></span>
                                                <span class="on"></span>
                                                <span class="on"></span>
                                                <span class="on"></span>
                                                <span></span>
                                            </div>
                                            <div class="bar point-3"></div>
                                            3
                                        </div>
                                        <div class="list">
                                            보통
                                            <div class="grade-star">
                                                <span class="on"></span>
                                                <span class="on"></span>
                                                <span class="on"></span>
                                                <span></span>
                                                <span></span>
                                            </div>
                                            <div class="bar point-5"></div>
                                            5
                                        </div>
                                        <div class="list">
                                            별로
                                            <div class="grade-star">
                                                <span class="on"></span>
                                                <span class="on"></span>
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                            </div>
                                            <div class="bar point-4"></div>
                                            4
                                        </div>
                                        <div class="list">
                                            나쁨
                                            <div class="grade-star">
                                                <span class="on"></span>
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                            </div>
                                            <div class="bar point-1"></div>
                                            1
                                        </div>
                                    </div>
                                </div>
                                <h4 class="tit2">내 평가점수</h4>
                                <div class="my-assessment">
                                    <div class="grade-star">
                                        <span class="on"></span>
                                        <span class="on"></span>
                                        <span class="on"></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </div>
                            </div>
                            <!--/ 사용자 평가 -->
                        </div>
                    </div>
                </div>
            </div>
            <!--/ 콘텐츠 본문 -->
        </section>
    <!--/ 본문 -->