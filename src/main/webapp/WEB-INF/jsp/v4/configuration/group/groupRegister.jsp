<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ju" uri="/WEB-INF/tld/JsonUtils.tld"%>

<script type="text/javascript">
 var currentBtnObj;
	$(function() {
		//그룹 생성
		
		$('#groupNm').on('keyup',function(e){
			if(e.keyCode == 13){
				$('#groupRegBtn').trigger('click');
			}
		});
		$('#groupRegBtn').on('click', function() {

			var groupNm = $('#groupNm').val();
			if ($V4.required(groupNm, "등록부서명은 필수입니다.", $('#groupNm')))
				return false;

			var groupType = $('input[name="groupType"]:checked').val();

			var parentGroupKey = "";

			if (groupType == "bottom") {
				parentGroupKey = $('#parentGroup').val();
			}

			var sendData = {
				"groupNm" : groupNm,
				"parentGroupKey" : parentGroupKey
			};

			$V4.http_post("/api/v1/group", sendData, {
				header : {
					"key" : "${_KEY}"
				},
				success : function(rtv) {
					alert('등록되었습니다.');
					$('#groupNm').val('');
					$('#groupList').commonList("refresh");
					parentGroupInit();
				}
			});
		});

		$('input[name="groupType"]').on('change', function() {
			console.log(this);

			var groupType = $(this).val();

			//상위부서 라디오 버튼 클릭
			if (groupType == "top") {
				$('#parentGroup').hide();
				//하위부서 라디오 버튼 클릭
			} else if (groupType == "bottom") {
				$('#parentGroup').show();
			} else {
				//x
			}
		});

		var parentGroupInit = function() {
			$V4.http_post("/api/1/parentGroupTree", null, {
				requestMethod : "GET",
				header : {
					"key" : "${_KEY}"
				},
				success : function(rtv) {
					var list = rtv.result;
					$('#parentGroup').empty();
					var strHtml = "";
					for (var i = 0; i < list.length; i++) {
						var vo = list[i];
						strHtml += '<option value="'+vo.groupKey+'">'
								+ vo.parentGroupNm + '</option>';
					}
					$('#parentGroup').html(strHtml);
				},
				error : function(t) {

				}
			});

		}

		parentGroupInit();

		var setting = {
			thead : [ {
				'<input type="checkbox" />' : "10%"
			}, {
				"상급부서명" : "15%"
			}, {
				"부서명" : "15%"
			}, {
				"부서장" : "15%"
			}, {
				"구성원" : "15%"
			}, {
				"구성차량수" : "15%"
			}, {
				"등록일" : "15%"
			} ],
			bodyHtml : (function(data) {
				console.log(data);
				var strHtml = "";
				for (i in data.result) {
					var obj = data.result[i];
					strHtml += "<tr>"
					strHtml += "<td><input type='checkbox' value='"+obj.groupKey+"' class='delChk' /></td>";

					//상급부서
					if (obj.groupNm == null) {
						strHtml += "<td>"
								+ "<span>"+obj.parentGroupNm+"</span>"
								+ "&nbsp;<button class='btn-img modify groupRename' data-key='"+obj.groupKey+"'>수정</button></td>";
					} else {
						//상급부서
						strHtml += "<td><span data-key='"+obj.parentGroupKey+"'>" + obj.parentGroupNm + "</span></td>";
					}

					if (obj.parentGroupKey == null) {
						strHtml += "<td></td>";
					} else {
						strHtml += "<td>"
								+ "<span>"+obj.groupNm+"</span>"
								+ "&nbsp;<button class='btn-img modify groupRename' data-key='"+obj.groupKey+"'>수정</button></td>";
					}

					if(obj.hasOwnProperty('manager')){
						strHtml += "<td>"
							+ obj.manager.name
							+ '<button class="btn sm round leaderSelector" data-key="'+obj.groupKey+'">변경</button>'
							+ "</td>";
					}else{
						strHtml += "<td>"
							+ '<button class="btn sm round leaderSelector" data-key="'+obj.groupKey+'">선택</button>'
							+ "</td>";
					}

					strHtml += "<td>"
							+ obj.userCount
							+ '명&nbsp;<button class="btn-img plus userPlus" data-key="'+obj.groupKey+'">추가</button><button class="btn-img minus userMinus" data-key="'+obj.groupKey+'">삭제</button>'
							+ "</td>";
					strHtml += "<td>"
							+ obj.vehicleCount
							+ '대&nbsp;<button class="btn-img plus vehiclePlus" data-key="'+obj.groupKey+'">추가</button><button class="btn-img minus vehicleMinus" data-key="'+obj.groupKey+'">삭제</button>'
							+ "</td>";
					strHtml += "<td>" + convertDateUint(new Date(obj.regDate),_unitDate,_timezoneOffset,0) + "</td>";
					
					strHtml += "</tr>";
				}
				return strHtml;
			}),
			limit : parseInt($(".func_limitChange").val())
    		,pagePerGroup : 10 //pagingGroup size
			,
			url : "/api/1/groupTree",
			param : {},
			http_post_option : {
				requestMethod : "GET",
				header : {
					key : "${_KEY}"
				}
			}
			//login token - test2@naver.com / test123!@# (이아이디가 차량 많음)
			,
			initSearch : true //생성과 동시에 search
			,
			loadingBodyViewer : true
			,afterFunk : function(){
				$(".leaderSelector").each(function(i){
					$(this).data({
						type : "user",
						http_post_option : {
							header : {
								key : "${_KEY}"
							}
						},
						selType : "radio",
						action : {
							ok : function(arrSelect, inputObj, valueObj, _this) {
								var info = base64ToJsonObj($(arrSelect).data("info"));
								var accountKey = info.accountKey;
								//var accountKey = $(arrSelect).data('accountkey');
								//여기서 수정을 업데이트 쳐야된다!?
								var self=  $(_this);
								var groupKey = self.data('key');
								
								var sendData = {
								  		"managerAccountKey": accountKey,
								}
								
								$V4.http_post("/api/1/group/"+groupKey,sendData,{
									requestMethod : "PUT"
									,header : {"key" :"${_KEY}" }
									,context : this
									,success : function(rtv){
										
										$('#groupList').commonList("refresh");
										
									},error : function(t){
										
									}
								});
								
							}
						},
						targetInput : $("#test"),
						targetInputValue : $("#test")
					}).commonPop();
				});
				
				$('.userPlus').each(function(i){
					$(this).data({
						type : "user",
						http_post_option : {
							header : {
								key : "${_KEY}"
							}
						},
						selType : "checkbox",
						action : {
							ok : function(arrSelect, inputObj, valueObj, _this) {
								
								var groupKey = $(_this).data('key');
								$.each(arrSelect,function(i,v){
									$(v).data('groupkey',groupKey);
								});
								
								var def = $.Deferred(); //point1
								var arrDe = $(arrSelect).map(function(i,v) {
									var groupKey = $(v).data('groupkey')
									var accountKey = base64ToJsonObj($(v).data('info')).accountKey;
									$V4.http_post("/api/1/groupKey/"+groupKey+"/accountKey/"+accountKey,{},{
										header : {key : "${_KEY}"}
										,requestMethod : "POST"
										,success : function(data){
											def.resolve({result: 1 , target : $(v) , data : data}); //point3
											//여기선 별다른 ajax return data를 쓸일없으니 만들어 넘김 
										}
										,error : function(data){
											def.resolve({result: 0 , target : $(v) , data : data}); //point3
										},
									});
									return def; 
								});			
								
								$.when.apply($, arrDe).then(function() {
									$('#groupList').commonList("refresh");
								},function(){ 
									alert("예상치 못한 문제가 발생하였습니다.\n 관리자에게 문의하여 주십시오.");
								});
								
							}
						},
						targetInput : $("#test"),
						targetInputValue : $("#test")
					}).commonPop();
				});
				
				$('.userMinus').each(function(i){
					$(this).data({
						type : "user",
						extendParam : {
							"searchGroupKey" : $(this).data('key')
						},
						http_post_option : {
							header : {
								key : "${_KEY}"
							}
						},
						selType : "checkbox",
						action : {
							ok : function(arrSelect, inputObj, valueObj, _this) {
								var groupKey = $(_this).data('key');
								$.each(arrSelect,function(i,v){
									$(v).data('groupkey',groupKey);
								});
								
								var def = $.Deferred(); //point1
								var arrDe = $(arrSelect).map(function(i,v) {
									var groupKey = $(v).data('groupkey')
									var accountKey = base64ToJsonObj($(v).data('info')).accountKey;
									$V4.http_post("/api/{ver}/groupKey/"+groupKey+"/accountKey/"+accountKey,{},{
										header : {key : "${_KEY}"}
										,requestMethod : "DELETE"
										,success : function(data){
											def.resolve({result: 1 , target : $(v) , data : data}); //point3
											//여기선 별다른 ajax return data를 쓸일없으니 만들어 넘김 
										}
										,error : function(data){
											def.resolve({result: 0 , target : $(v) , data : data}); //point3
										},
									});
									return def; 
								});			
								
								$.when.apply($, arrDe).then(function() {
									$('#groupList').commonList("refresh");
								},function(){ 
									alert("예상치 못한 문제가 발생하였습니다.\n 관리자에게 문의하여 주십시오.");
								});
							}
						},
						targetInput : $("#test"),
						targetInputValue : $("#test")
					}).commonPop();
				});
				
				$('.vehiclePlus').each(function(i){
					$(this).data({
						type : "vehicle",
						http_post_option : {
							header : {
								key : "${_KEY}"
							}
						},
						selType : "checkbox",
						action : {
							ok : function(arrSelect, inputObj, valueObj, _this) {
								
								var groupKey = $(_this).data('key');
								$.each(arrSelect,function(i,v){
									$(v).data('groupkey',groupKey);
								});
								
								var def = $.Deferred(); //point1
								var arrDe = $(arrSelect).map(function(i,v) {
									var groupKey = $(v).data('groupkey')
									var vehicleKey = base64ToJsonObj($(v).data('info')).vehicleKey;
									//var vehicleKey = $(v).data('vehiclekey');
									$V4.http_post("/api/{ver}/groupKey/"+groupKey+"/vehicleKey/"+vehicleKey,{},{
										header : {key : "${_KEY}"}
										,requestMethod : "POST"
										,success : function(data){
											def.resolve({result: 1 , target : $(v) , data : data}); //point3
											//여기선 별다른 ajax return data를 쓸일없으니 만들어 넘김 
										}
										,error : function(data){
											def.resolve({result: 0 , target : $(v) , data : data}); //point3
										},
									});
									return def; 
								});			
								
								$.when.apply($, arrDe).then(function() {
									$('#groupList').commonList("refresh");
								},function(){ 
									alert("예상치 못한 문제가 발생하였습니다.\n 관리자에게 문의하여 주십시오.");
								});
								
							}
						},
						targetInput : $("#test"),
						targetInputValue : $("#test")
					}).commonPop();
				});
				
				$('.vehicleMinus').each(function(i){
					$(this).data({
						type : "vehicle",
						extendParam : {
							"searchGroupKey" : $(this).data('key')
						},
						http_post_option : {
							header : {
								key : "${_KEY}"
							}
						},
						selType : "checkbox",
						action : {
							ok : function(arrSelect, inputObj, valueObj, _this) {
								var groupKey = $(_this).data('key');
								$.each(arrSelect,function(i,v){
									$(v).data('groupkey',groupKey);
								});
								
								var def = $.Deferred(); //point1
								var arrDe = $(arrSelect).map(function(i,v) {
									var groupKey = $(v).data('groupkey')
									var vehicleKey = base64ToJsonObj($(v).data('info')).vehicleKey;
									//var vehicleKey = $(v).data('vehiclekey');
									$V4.http_post("/api/{ver}/groupKey/"+groupKey+"/vehicleKey/"+vehicleKey,{},{
										header : {key : "${_KEY}"}
										,requestMethod : "DELETE"
										,success : function(data){
											def.resolve({result: 1 , target : $(v) , data : data}); //point3
											//여기선 별다른 ajax return data를 쓸일없으니 만들어 넘김 
										}
										,error : function(data){
											def.resolve({result: 0 , target : $(v) , data : data}); //point3
										},
									});
									return def; 
								});			
								
								$.when.apply($, arrDe).then(function() {
									$('#groupList').commonList("refresh");
								},function(){ 
									alert("예상치 못한 문제가 발생하였습니다.\n 관리자에게 문의하여 주십시오.");
								});
							}
						},
						targetInput : $("#test"),
						targetInputValue : $("#test")
					}).commonPop();
				});
				
			}
		//로딩중!! 표시됨
		}

		$('#groupList').commonList(setting);

		$("#managerChangePop").data(
				{
					type : "user",
					http_post_option : {
						header : {
							key : "${_KEY}"
						}
					},
					selType : "radio",
					action : {
						ok : function(arrSelect, inputObj, valueObj) {
							console.log("ok 버튼 실행");
							$.each(arrSelect, function() {
								$(inputObj).html($(this).data("accountnm"));
								$(valueObj).val($(this).data("accountkey"));
								var accountKey = $(this).data("accountkey");
								//바로 여기서 ajax을 탄다 accountKey를 가지고
								//corp에 vehicleManager를 업데이트 한다.
								var accountnm = $(this).data("accountnm")
								$('#name').text(accountnm);
								$V4.http_post("/api/{ver}/corp/manager/"
										+ accountKey, {}, {
									requestMethod : "PUT",
									header : {
										"key" : "${_KEY}"
									},
									success : function(rtv) {
										alert('변경되었습니다.');
									},
									error : function(t) {

									}
								});
							});
						}
					},
					targetInput : $("#name"),
					targetInputValue : $("#test")
				}).commonPop();

		var getCorpVehicleManager = function() {
			var vehicleManager = ${ju:toJson(V4.corp.manager.vehicleManager)};
			$('#name').text(simpleUserObjectNameViewer(vehicleManager,"/"," "," "));
			/*
			$('#name').text(
					simpleUserNameViewer(
						getProperty(vehicleManager,"group.parentGorupNm")						
						,"/"
						,getProperty(vehicleManager,"group.groupNm")
						," "
						,getProperty(vehicleManager,"corpPosition")
						," "
						,getProperty(vehicleManager,"name")
					)
			);
			*/
		};

		getCorpVehicleManager();
		
		$('body').on('click','.groupRename',function(){
			console.log('test');
			var self = $(this);
			var groupKey = self.data('key');
			var text = self.prev().text();
			self.prev().replaceWith('<input type="text" style="width:62%" class="renameInput" value="'+text+'" />')
			self.replaceWith('<button class="btn sm round groupRenameOk" style="min-width: 10px;" data-key="'+groupKey+'">수정</button>');
		});

		$('body').on('keyup','.renameInput',function(e){
			if(e.keyCode == 13){
				$('.groupRenameOk').trigger('click');	
			}
		});
		$('body').on('click','.groupRenameOk',function(){
			var self=  $(this);
			var groupKey = self.data('key');
			var changeName = self.prev().val();
			
			$(this).data('changeName',changeName);
			var sendData = {
					  "groupNm": changeName
			}
			
			$V4.http_post("/api/1/group/"+groupKey,sendData,{
				requestMethod : "PUT"
				,header : {"key" :"${_KEY}" }
				,context : this
				,success : function(rtv){
					var self = $(this.context);
					var changeName = self.data('changeName');
					var groupKey = self.data('key');
					/* $('span[data-key="'+groupKey+'"]').text(changeName); */
					self.prev().replaceWith('<span>'+changeName+'</span>');
					self.replaceWith('<button class="btn-img modify groupRename" data-key="'+groupKey+'">수정</button>');
					
					$('#groupList').commonList("refresh");
					
				},error : function(t){
					
				}
			});
		});
		
		$('#groupDeleteBtn').on('click',function(){
			var delCheck = $('.delChk:checked');
			
			var def = $.Deferred(); //point1
			var arrDe = $(delCheck).map(function(i,v) {
				var groupKey = $(v).val();
				$V4.http_post("/api/1/group/"+groupKey,{},{
					header : {key : "${_KEY}"}
					,requestMethod : "DELETE"
					,success : function(data){
						def.resolve({result: 1 , target : $(v) , data : data}); //point3
						//여기선 별다른 ajax return data를 쓸일없으니 만들어 넘김 
					}
					,error : function(data){
						def.resolve({result: 0 , target : $(v) , data : data}); //point3
					},
				});
				return def; 
			});			
			
			$.when.apply($, arrDe).then(function() {
				$('#groupList').commonList("refresh");
			},function(){ 
				alert("예상치 못한 문제가 발생하였습니다.\n 관리자에게 문의하여 주십시오.");
			});
			
		});
		
		$(".func_limitChange").on("change",function(){
			var limit = parseInt($(".func_limitChange").val());
			$('#groupList').commonList("setLimit",limit).search();
		});

	});
</script>
<!-- 본문 -->
<section id="container">
	<div id="sub-container">
		<!-- 상단 타이틀 -->
		<div class="page-header">
			<h2>부서등록</h2>
			<span>부서를 등록하거나 수정할 수 있습니다.</span>
		</div>
		<!--/ 상단 타이틀 -->

		<!-- 콘텐츠 본문 -->
		<div id="contents-page" class="">
			<div class="department-list">
				<!-- 상단 상태값 -->
				<div class="top-state clr">
					<div class="time">
						<span>2018/05/27</span> <span>19:53</span> <span>현재</span>
						<button class="btn btn04">새로고침</button>
					</div>
				</div>
				<!--/ 상단 상태값 -->

				<!-- 부서등록 -->
				<div class="section01">
					<h3 class="tit">부서등록</h3>
					<div>
						<input type="radio" id="groupType01" name="groupType"
							style="width: 15px" checked="checked" value="top"> <label
							for="groupType01">상위부서등록</label> &nbsp;&nbsp;&nbsp;&nbsp; <input
							type="radio" id="groupType02" name="groupType"
							style="width: 15px" value="bottom"> <label
							for="groupType02">하위부서등록</label> <br> <br> <select
							id="parentGroup" style="width: 330px; display: none">

						</select> <input type="text" name="groupNm" id="groupNm"
							placeholder="등록 부서명 입력" />
						<button class="btn btn01 md" type="button" id="groupRegBtn">등록</button>
						<!-- <button type="button" id="leaderChangePop" style="display:none"/> -->
					</div>
				</div>
				<!--/ 부서등록 -->

				<!-- 차량관리자 정보 -->
				<div class="section02">
					<h3 class="tit2">차량관리자 정보</h3>
					<div>
						<!-- <span id="parentGroupNm">경영지원본부</span>
                                <span id="groupNm">충무팀</span>
                                <span id="corpPosition">대리</span> -->
						<span id="name"></span>
						<button type="button" class="btn btn04 md" id="managerChangePop">변경</button>
					</div>
				</div>
				<!--/ 차량관리자 정보 -->

				<!-- 등록부서 조회 -->
				<div class="section03 ">
					<h3 class="tit2">등록부서 조회</h3>
					<!-- table 버튼 -->
					<div class="btn-function title-side">
						<div class="right">
							<button type="button" class="delete" id="groupDeleteBtn">선택삭제</button>
							<select name="" class="func_limitChange">
								<option value="5" selected>5건씩 보기</option>
								<option value="10">10건씩 보기</option>
								<option value="20">20건씩 보기</option>
								<option value="50">20건씩 보기</option>
							</select>
						</div>
					</div>
					<!--/ table 버튼 -->

					<table class="table list mgt20" id="groupList">
						<caption>등록된 서비스 현황</caption>

						<thead>

						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
				<!--/ 등록부서 조회 -->
			</div>
		</div>
		<!--/ 콘텐츠 본문 -->
	</div>
</section>
<!--/ 본문 -->
