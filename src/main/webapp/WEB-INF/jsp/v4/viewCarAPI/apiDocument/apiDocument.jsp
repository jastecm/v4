<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	$(".tab-menu>li>a").click(function() {
        $(this).parent().addClass("on").siblings().removeClass("on");
        return false;
    });
});
</script>
 <!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>API문서</h2>
                    <span>오픈 API (Open Application Programming Interface)는 인터넷 이용자가 일방적으로 사용자 화면 등을 제공받는데 그치지 않고<br />직접 응용 프로그램과 서비스를 개발할 수 있도록 공개된 개발자를 위한 인터페이스입니다. </span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="api-page">
                    <div class="api-document">
                        <!-- 상단 검색 -->
                        <div class="top-search mgt20">
                            <span class="search-tit">디바이스 선택</span>
                            <select name="">
								<option value="">Von-S31</option>
							</select>
                        </div>
                        <!--/ 상단 검색 -->

                        <!-- 탭메뉴 -->
                        <ul class="page-tab tab-menu col-4">
                            <!-- 모바일 (APP)  -->
                            <li class="list01 on">
                                <a href="#">모바일(APP)</a>
                                <div class="tab-list">
                                    <h3 class="tit2">차량 등록ㆍ조회ㆍ삭제</h3>
                                    <p class="mgt10">차량을 등록, 조회, 사용중지 등 다양한 차량관련 API입니다.</p>
                                    <ul class="list">
                                        <li class="get">
                                            <span class="method">GET</span>
                                            <span class="url">/api/{ver}/monthVehicle</span>
                                            <span class="txt">차량 리스트 조회</span>
                                        </li>
                                        <li class="get">
                                            <span class="method">GET</span>
                                            <span class="url">/api/{ver}/myVehicle</span>
                                            <span class="txt">차량 리스트 조회</span>
                                        </li>
                                        <li class="put">
                                            <span class="method">PUT</span>
                                            <span class="url">/api/{ver}/vehiceLocation/{vehicleKey}</span>
                                            <span class="txt">차량 위치 갱신</span>
                                        </li>
                                        <li class="get">
                                            <span class="method">GET</span>
                                            <span class="url">/api/{ver}/vehicelLocationChangeBeCan/{vehiceKey}</span>
                                            <span class="txt">차량 위치 갱신전 체크</span>
                                        </li>
                                        <li class="get">
                                            <span class="method">GET</span>
                                            <span class="url">/api/{ver}/vehicle</span>
                                            <span class="txt">차량 리스트 조회</span>
                                        </li>
                                        <li class="post">
                                            <span class="method">POST</span>
                                            <span class="url">/api/{ver}/vehicle</span>
                                            <span class="txt">차량 등록</span>
                                        </li>
                                        <li class="put">
                                            <span class="method">PUT</span>
                                            <span class="url">/api/{ver}/vehicle</span>
                                            <span class="txt">차량 갱신 - todo</span>
                                        </li>
                                        <li class="delete">
                                            <span class="method">DELETE</span>
                                            <span class="url">/api/{ver}/vehicle/{vehiceKey}</span>
                                            <span class="txt">차량 삭제</span>
                                        </li>
                                        <li class="get">
                                            <span class="method">GET</span>
                                            <span class="url">/api/{ver}/vehicle/{vehiceKey}</span>
                                            <span class="txt">차량 조회</span>
                                        </li>
                                        <li class="delete">
                                            <span class="method">DELETE</span>
                                            <span class="url">/api/{ver}/vehicle/{vehiceKey}/inactive</span>
                                            <span class="txt">차량 사용 중지</span>
                                        </li>
                                        <li class="put">
                                            <span class="method">PUT</span>
                                            <span class="url">/api/{ver}/vehicle/{vehiceKey}/{field}/{val}</span>
                                            <span class="txt">차량 갱신 - todo</span>
                                        </li>
                                    </ul>

                                    <h3 class="tit2 mgt30">차량 운행정보</h3>
                                    <p class="mgt10">차량 운행내역 조회, 주행목적 변경 등 운행정보 관련 API 입니다.</p>
                                    <ul class="list">
                                        <li class="get">
                                            <span class="method">GET</span>
                                            <span class="url">/api/{ver}/vehicle/{vehiceKey}/summary/{YYYYMM}</span>
                                            <span class="txt">차량 주행정보 조회</span>
                                        </li>
                                        <li class="get">
                                            <span class="method">GET</span>
                                            <span class="url">/api/{ver}/vehicle/{vehiceKey}/trip/{tripKey}</span>
                                            <span class="txt">차량 주행정보 조회</span>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <!--/ 모바일 (APP)  -->

                            <!-- 웹 (Web)  -->
                            <li class="list02">
                                <a href="#">웹 (Web)</a>
                                <div class="tab-list">
                                    <h3 class="tit2">차량 등록ㆍ조회ㆍ삭제</h3>
                                    <p class="mgt10">차량을 등록, 조회, 사용중지 등 다양한 차량관련 API입니다.</p>
                                    <ul class="list">
                                        <li class="get">
                                            <span class="method">GET</span>
                                            <span class="url">/api/{ver}/monthVehicle</span>
                                            <span class="txt">차량 리스트 조회</span>
                                        </li>
                                        <li class="get">
                                            <span class="method">GET</span>
                                            <span class="url">/api/{ver}/myVehicle</span>
                                            <span class="txt">차량 리스트 조회</span>
                                        </li>
                                        <li class="put">
                                            <span class="method">PUT</span>
                                            <span class="url">/api/{ver}/vehiceLocation/{vehicleKey}</span>
                                            <span class="txt">차량 위치 갱신</span>
                                        </li>
                                        <li class="get">
                                            <span class="method">GET</span>
                                            <span class="url">/api/{ver}/vehicelLocationChangeBeCan/{vehiceKey}</span>
                                            <span class="txt">차량 위치 갱신전 체크</span>
                                        </li>
                                        <li class="get">
                                            <span class="method">GET</span>
                                            <span class="url">/api/{ver}/vehicle</span>
                                            <span class="txt">차량 리스트 조회</span>
                                        </li>
                                        <li class="post">
                                            <span class="method">POST</span>
                                            <span class="url">/api/{ver}/vehicle</span>
                                            <span class="txt">차량 등록</span>
                                        </li>
                                        <li class="put">
                                            <span class="method">PUT</span>
                                            <span class="url">/api/{ver}/vehicle</span>
                                            <span class="txt">차량 갱신 - todo</span>
                                        </li>
                                        <li class="delete">
                                            <span class="method">DELETE</span>
                                            <span class="url">/api/{ver}/vehicle/{vehiceKey}</span>
                                            <span class="txt">차량 삭제</span>
                                        </li>
                                        <li class="get">
                                            <span class="method">GET</span>
                                            <span class="url">/api/{ver}/vehicle/{vehiceKey}</span>
                                            <span class="txt">차량 조회</span>
                                        </li>
                                        <li class="delete">
                                            <span class="method">DELETE</span>
                                            <span class="url">/api/{ver}/vehicle/{vehiceKey}/inactive</span>
                                            <span class="txt">차량 사용 중지</span>
                                        </li>
                                        <li class="put">
                                            <span class="method">PUT</span>
                                            <span class="url">/api/{ver}/vehicle/{vehiceKey}/{field}/{val}</span>
                                            <span class="txt">차량 갱신 - todo</span>
                                        </li>
                                    </ul>

                                    <h3 class="tit2 mgt30">차량 운행정보</h3>
                                    <p class="mgt10">차량 운행내역 조회, 주행목적 변경 등 운행정보 관련 API 입니다.</p>
                                    <ul class="list">
                                        <li class="get">
                                            <span class="method">GET</span>
                                            <span class="url">/api/{ver}/vehicle/{vehiceKey}/summary/{YYYYMM}</span>
                                            <span class="txt">차량 주행정보 조회</span>
                                        </li>
                                        <li class="get">
                                            <span class="method">GET</span>
                                            <span class="url">/api/{ver}/vehicle/{vehiceKey}/trip/{tripKey}</span>
                                            <span class="txt">차량 주행정보 조회</span>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <!--/ 웹 (Web)  -->
                        </ul>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->