<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
});
</script>
 <!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>개발자 지원</h2>
                    <span>오픈 API (Open Application Programming Interface)는 인터넷 이용자가 일방적으로 사용자 화면 등을 제공받는데 그치지 않고<br />직접 응용 프로그램과 서비스를 개발할 수 있도록 공개된 개발자를 위한 인터페이스입니다. </span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="api-page">
                    <div class="apply-developer">
                        <h3 class="tit2">ViewCAR에서 제공하는 Open API 서비스는 ?</h3>
                        <p class="section01 mgt30 icon-spr">
                            자스텍M이 개발한 OBD 단말을 통해 수집되는 차량센서 및 드라이빙 데이터, 분석 정보를 웹, <br /> 모바일 서비스 개발자를 좀더 쉽고 편리하게 또는 다른 IoT 서비스와 결합하여 활용할 수 있도록 공개한 서비스입니다. <br /><br /> 다만, 단말 보안 및 전송보안, 서버 보안을 위해 접근 토근(Access Token)을 획득해야만 사용할 수 있습니다.
                        </p>

                        <h3 class="tit2 mgt40">ViewCAR Open API의 특징</h3>
                        <ul class="section02">
                            <li>복잡한 In-Vehicle Networks 정보는 ViewCAR에서 알아서 제공합니다.</li>
                            <li>블루투스형, 통신형(3G/4G) 등 다양한 OBD 단말을 지원합니다. </li>
                            <li>OBD 단말부터 서버까지 빈틈없는 보안관리로 고객 정보를 최우선의 가치로 보호합니다.</li>
                            <li>다양한 차량센서와 인터페이스를 제공하고, 손쉽게 연동할 수 있도록 SDK를 제공합니다.</li>
                            <li>차량 센서 및 운행데이터는 ViewCAR에서 보다 가치있는 데이터로 제공합니다.</li>
                        </ul>

                        <h3 class="tit2 mgt40">ViewCAR Open API 사용절차</h3>
                        <ul class="section03">
                            <li class="list01"><span>기업회원 가입</span></li>
                            <li class="list02"><span>서비스ㆍ등급 선택</span></li>
                            <li class="list03"><span>승인ㆍ인증키 발급</span></li>
                            <li class="list04"><span>개발을 위한 Open API 정보 활용</span></li>
                        </ul>

                        <h3 class="tit2 mgt40">ViewCAR 서비스 등급별 지원 기능</h3>
                        <table class="table list border mgt20">
                            <caption>등록된 서비스 리스트</caption>
                            <colgroup>
                                <col style="width:12%" />
                                <col />
                                <col style="width:12%" />
                                <col style="width:12%" />
                                <col style="width:12%" />
                                <col style="width:12%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="col" colspan="2">구분</th>
                                    <th scope="col">Basic</th>
                                    <th scope="col">Standard</th>
                                    <th scope="col">Premium</th>
                                    <th scope="col">비고</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th rowspan="9" scope="row">운행정보</th>
                                    <td>차량ㆍ사용자</td>
                                    <td>○</td>
                                    <td>○</td>
                                    <td>○</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>차량배차</td>
                                    <td></td>
                                    <td></td>
                                    <td>○</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>미배차운행</td>
                                    <td></td>
                                    <td></td>
                                    <td>○</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>운행일지</td>
                                    <td></td>
                                    <td>○</td>
                                    <td>○</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>버스(셔틀)노선정보</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>부가서비스</td>
                                </tr>
                                <tr>
                                    <td>상세운행기록</td>
                                    <td>○</td>
                                    <td>○</td>
                                    <td>○</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>차량위치</td>
                                    <td>○</td>
                                    <td>○</td>
                                    <td>○</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>차량주요알림</td>
                                    <td>○</td>
                                    <td>○</td>
                                    <td>○</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>사고차량 조회</td>
                                    <td>○</td>
                                    <td>○</td>
                                    <td>○</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th rowspan="7" scope="row">차량관리</th>
                                    <td>운행스케쥴</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>부가서비스</td>
                                </tr>
                                <tr>
                                    <td>실시간 자가 진단</td>
                                    <td>○</td>
                                    <td>○</td>
                                    <td>○</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>고장ㆍ소모품 현황</td>
                                    <td>○</td>
                                    <td>○</td>
                                    <td>○</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>별점ㆍ과태료</td>
                                    <td></td>
                                    <td>○</td>
                                    <td>○</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>정비현황 조회ㆍ등록</td>
                                    <td></td>
                                    <td></td>
                                    <td>○</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>차량경비</td>
                                    <td></td>
                                    <td>○</td>
                                    <td>○</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>도급비</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>부가서비스</td>
                                </tr>
                                <tr>
                                    <th rowspan="5" scope="row">보고서ㆍ결제</th>
                                    <td>차량보고서 조회ㆍ제출</td>
                                    <td></td>
                                    <td></td>
                                    <td>○</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>차량사고보고</td>
                                    <td></td>
                                    <td>○</td>
                                    <td>○</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>결재관리</td>
                                    <td>○</td>
                                    <td>○</td>
                                    <td>○</td>
                                    <td>버시스별 권한 관리</td>
                                </tr>
                                <tr>
                                    <td>리포트</td>
                                    <td>○</td>
                                    <td>○</td>
                                    <td>○</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>국세청 운행일지</td>
                                    <td></td>
                                    <td>○</td>
                                    <td>○</td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>

                        <!-- 하단 버튼 -->
                        <div class="btn-bottom">
                            <button class="btn btn03">ViewCAR 오픈 API 신청 </button>
                        </div>
                        <!--/ 하단 버튼 -->
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->