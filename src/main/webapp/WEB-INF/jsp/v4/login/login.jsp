<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	
	$("#btn_regist,#btn_regist2").on("click",function(){
		$V4.move("/regist/step0");
	});
	
	$('.btn-login').on('click',function(){
		var t = $('.list01.on').data("target");
		if(!t) t= $('.list02.on').data("target");
		
		if(t == 'account'){
			var header = {"id" : $('#userId').val(),"pw" : $('#userPw').val(),"expire" : 99999999}
			var url = "/api/1/token";
		}else if(t=='phone'){
			var header = {"id" : $('#phoneId').val(),"pw" : $('#phonePw').val(),"expire" : 99999999}
			var url = "/api/1/token/MP";
		}else {
			return;
		}
		
		$V4.http_post(url,null,{
			requestMethod : "GET"
			,header : header
			,success : function(t){
				$V4.http_post("/login/registToken",{},{
					header : {key : t.result}
					,success : function(r){
						$V4.move("/main");
					}
				});
			},error : function(t){
				if(t.responseJSON.result == "token.notActive" || t.responseJSON.result == "token.tokenExpired"){
					$V4.http_post(url,header,{
						success:function(t){
							$V4.http_post("/login/registToken",{},{
								header : {key : t.result}
								,success : function(r){
									$V4.move("/main");
								}
							});
						}							
					});
				}else{
					alert("login fail");
				}
			}
		});
	});
	
	$(".evEnter").on("keyup",function(e){
		if(e.keyCode == 13) $('#btn_login_id').trigger("click");
	});
	
	$(".evEnterPhone").on("keyup",function(e){
		if(e.keyCode == 13) $('#btn_login_phone').trigger("click");
	});
});

</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>로그인</h2>
                    <span></span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="login-page">
                    <ul class="login-tab">
                        <li class="list01 on"  data-target="account">
                            <a href="#" class="tab">이메일 로그인</a>
                            <div class="tab-list">
                                <input type="text" id="userId" class="evEnter" placeholder="이메일" />
                                <input type="password" id="userPw" class="evEnter" placeholder="비밀번호" />
                                <button class="btn btn03 btn-login" id="btn_login_id" >로그인</button>
                                <div class="clr">
                                    <!-- <div class="id-save">
                                        <input type="checkbox" name="" />
                                        <label for="">로그인 상태유지</label>
                                    </div> -->
                                    <a href="#" onClick="alert('TODO');">아이디/비밀번호 찾기</a>
                                </div>
                                <a href="#" class="btn btn04 btn-join" id="btn_regist">회원가입</a>
                            </div>
                        </li>
                        
                        <li class="list02" data-target="phone">
                            <a href="#" class="tab">인증된 휴대폰 No</a>
                            <div class="tab-list">
                                <input type="text" id="phoneId" class="evEnterPhone" placeholder="휴대폰번호" />
                                <input type="password" id="phonePw" class="evEnterPhone" placeholder="비밀번호" />
                                <button class="btn btn03 btn-login" id="btn_login_phone">로그인</button>
                                <div class="clr">
                                    <!-- <div class="id-save">
                                        <input type="checkbox" name="" />
                                        <label for="">로그인 상태유지</label>
                                    </div> -->
                                    <a href="#" onClick="alert('TODO');">아이디/비밀번호 찾기</a>
                                </div>
                                <a href="#" class="btn btn04 btn-join" id="btn_regist2">회원가입</a>
                            </div>
                        </li>
                    </ul>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->