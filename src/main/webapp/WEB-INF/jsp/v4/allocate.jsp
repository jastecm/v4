<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>




<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta name="description" content="VIEWCAR - For Your Smart Driving">
		<meta property="og:title" content="VIEWCAR">
		<meta property="og:description" content="VIEWCAR - For Your Smart Driving">
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/js/jqueryui/jquery-ui.css"/>
		<%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/content.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/content_costom.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/style.css" /> --%>
		<link rel="manifest" href="${pageContext.request.contextPath}/common/json/manifest.json">
		<%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/jsDatePick_ltr.min.css" media="all" /> --%>
		
		<%@include file="/WEB-INF/jsp/common/common.jsp"%>
		
		<script src="${pageContext.request.contextPath}/common/js/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/common.js"></script>
		<script src="${pageContext.request.contextPath}/common/js/topup.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jqueryui/jquery-ui.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/common.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.stylish-select.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jsDatePick.jquery.min.1.3.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/util.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.form.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.MultiFile.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/message/${_LANG}.js"></script>
		
		<script type="text/javascript" src="https://www.gstatic.com/firebasejs/4.1.3/firebase.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/commonDev.js"></script>

             

<script type="text/javascript">
Date.prototype.getUnixTime = function() { return this.getTime()/1000|0 };

$(function(){
$('#login').on('click',function(){
		
		$.ajax({
			url: "${defaultPath}/api/1/token/"
			,dataType : 'json'
			,contentType: "application/json; charset=UTF-8"
			,type:"GET"
			,cache:!1
			,headers : {"Cache-Control":"no-cache"}
			,beforeSend : function(xhr){
				xhr.setRequestHeader("id", $('#userId').val());
				xhr.setRequestHeader("pw", $('#userPw').val()); 
			}
			,success:function(t){
				console.log(t);
				$('#token').val(t.result);
				
			}		
			,error:function(t){
				console.log(t);
				if(t.responseJSON.result == "token.notActive"){
					var sendData = {
							"id" : $('#userId').val(),
							"pw" : $('#userPw').val(),
							"expire" : $('#expire').val()
					}
					
					$.ajax({
						url: "${defaultPath}/api/1/token/"
						,dataType : 'json'
						,contentType: "application/json; charset=UTF-8"
						,type:"POST"
						,data: JSON.stringify(sendData)
						,cache:!1
						,headers : {"Cache-Control":"no-cache"}
						,beforeSend : function(xhr){
							 
						}
						,success:function(t){
							console.log(t);
							$('#token').val(t.result);
							
						}		
						,error:function(t){
							
						}
					});
				}
			}
		});
	});
	
	//배차 가능 차량 조회
	$('#availableVehicles').on('click',function(){
		
		var start = new Date($('#start').val()).getUnixTime();
		var end = new Date($('#end').val()).getUnixTime();
		
		$.ajax({
			url: "${defaultPath}/api/1/allocate/search/"+start+"/"+end
			,dataType : 'json'
			,contentType: "application/json; charset=UTF-8"
			,type:"GET"
			,cache:!1
			,headers : {"Cache-Control":"no-cache"}
			,beforeSend : function(xhr){
				xhr.setRequestHeader("key", $('#token').val()); 
			}
			,success:function(t){
				var strHtml = "";
				
				var data = t.result;
				
				for(var i = 0 ; i < data.length; i++){
					strHtml += "<li class='vehicle' data-vehiclekey="+data[i].vehicleKey+">"+data[i].plateNum+" "+data[i].vehicleKey+"</li>";
				}
				
				$('#vehicleList').html(strHtml);
				console.log(t);
			}		
			,error:function(t){
				console.log(t);
			}
		});
	});
	
	$(document).on('click','.vehicle',function(){
		var vehicleKey = $(this).data('vehiclekey');
		
		$('#vehicleKey').val(vehicleKey);
	});
	
	$('#regVehicle').on('click',function(){
		var start = new Date($('#start').val()).getUnixTime();
		var end = new Date($('#end').val()).getUnixTime();
		
		var sendData = {
				  "endDate": end,
				  "isHolyDay": "0",
				  "purpose": "1",
				  "startDate": start,
				  "vehicleKey": $('#vehicleKey').val()
		}
		
		$.ajax({
			url: "${defaultPath}/api/1/allocate"
			,dataType : 'json'
			,contentType: "application/json; charset=UTF-8"
			,type:"POST"
			,data:JSON.stringify(sendData)
			,cache:!1
			,headers : {"Cache-Control":"no-cache"}
			,beforeSend : function(xhr){
				xhr.setRequestHeader("key", $('#token').val()); 
			}
			,success:function(t){
				console.log(t);
				alert("배차신청이 완료되었습니다.");
			}		
			,error:function(t){
				alert(t.responseJSON.result);
			}
		});
		
	});
	
	//$('#start').val(getToday(1));
	//$('#end').val(getToday(2));
	
	$('#getAllocateBtn').on('click',function(){
		
		
		$.ajax({
			url: "${defaultPath}/api/1/approval"
			,dataType : 'json'
			,contentType: "application/json; charset=UTF-8"
			,type:"GET"
			,data: {
				
			}
			,cache:!1
			,headers : {"Cache-Control":"no-cache"}
			,beforeSend : function(xhr){
				xhr.setRequestHeader("key", $('#token').val()); 
			}
			,success:function(t){
				alert("가져옴");
				var data = t.result;
				$('#allocateList').empty();
				var str = "";
				for(var i = 0 ; i < data.length; i++){
					str += "<li>"
					str += "<span>배차키 : "+data[i].allocateKey+"</span>"
					str += "<span>   시작시간 : "+data[i].startDate+"</span>"
					str += "<span>   종료시간 : "+data[i].endDate+"</span>"
					str += "<span>   차량번호 : "+data[i].plateNum+"</span>"
					str += "</li>"
				}
				$('#allocateList').html(str);
			}		
			,error:function(t){
				console.log(t);
			}
		});
	});
	
});

var getToday = function(addHour){
	var today = new Date();
	today.setHours(today.getHours() + addHour);
	
	var year = today.getFullYear();
	var month = today.getMonth() + 1
	var day = today.getDate();
	var hour = today.getHours();
	var minute = today.getMinutes();
	
	return year+"/"+month+"/"+day+" "+hour+":"+minute;
};	

</script>
	</head>
    <body>
    	<h1>배차 테스트 화면</h1>
    	로그인하기<br/>
    	아이디 : <input type="text" id="userId" value="pne1818@naver.com"></br>
    	비번 : <input type="text" id="userPw" value="asdf!@#123"></br>
    	만료시간 : <input type="text" id="expire" value="9999999"></br>
    	<button type="button" id="login">로그인</button>
    	<br/>
    	<br/>
    	<br/>
    	
    	토큰 : <input type="text" id="token" value="" style="width:1000px;"></br>
    	<br/>
    	<input type="text" name="start" id="start" value="2018/07/14 14:20"> ~ <input type="text" name="end" id="end" value="2018/07/14 16:20">
    	<br> 
    	<button type="button" id="availableVehicles">배차가능한 차량 리스트 호출</button></br>
    	<ul id="vehicleList">
    		
    	</ul>
    	<br>
    	배차신청할 vehicleKey : <input type="text" id="vehicleKey" style="width:700px;"><br>
    	<button type="button" id="regVehicle">배차신청</button>
    	
    	<br><br>
    	
    	<button type="button" id="getAllocateBtn">배차정보 리스트를 조회</button>
    	<ul id="allocateList">
    	
    	</ul>
    	
    </body>
</html>