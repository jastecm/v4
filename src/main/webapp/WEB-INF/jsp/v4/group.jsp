<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>




<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta name="description" content="VIEWCAR - For Your Smart Driving">
		<meta property="og:title" content="VIEWCAR">
		<meta property="og:description" content="VIEWCAR - For Your Smart Driving">
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/js/jqueryui/jquery-ui.css"/>
		<%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/content.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/content_costom.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/style.css" /> --%>
		<link rel="manifest" href="${pageContext.request.contextPath}/common/json/manifest.json">
		<%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/jsDatePick_ltr.min.css" media="all" /> --%>
		
		<%@include file="/WEB-INF/jsp/common/common.jsp"%>
		
		<script src="${pageContext.request.contextPath}/common/js/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/common.js"></script>
		<script src="${pageContext.request.contextPath}/common/js/topup.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jqueryui/jquery-ui.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/common.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.stylish-select.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jsDatePick.jquery.min.1.3.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/util.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.form.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.MultiFile.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/message/${_LANG}.js"></script>
		
		<script type="text/javascript" src="https://www.gstatic.com/firebasejs/4.1.3/firebase.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/commonDev.js"></script>

             

<script type="text/javascript">
$(function(){
$('#login').on('click',function(){
		
		$.ajax({
			url: "${defaultPath}/api/1/token/"
			,dataType : 'json'
			,contentType: "application/json; charset=UTF-8"
			,type:"GET"
			,cache:!1
			,headers : {"Cache-Control":"no-cache"}
			,beforeSend : function(xhr){
				xhr.setRequestHeader("id", $('#userId').val());
				xhr.setRequestHeader("pw", $('#userPw').val()); 
			}
			,success:function(t){
				console.log(t);
				$('#token').val(t.result);
				
			}		
			,error:function(t){
				console.log(t);
				if(t.responseJSON.result == "token.notActive"){
					var sendData = {
							"id" : $('#userId').val(),
							"pw" : $('#userPw').val(),
							"expire" : $('#expire').val()
					}
					
					$.ajax({
						url: "${defaultPath}/api/1/token/"
						,dataType : 'json'
						,contentType: "application/json; charset=UTF-8"
						,type:"POST"
						,data: JSON.stringify(sendData)
						,cache:!1
						,headers : {"Cache-Control":"no-cache"}
						,beforeSend : function(xhr){
							 
						}
						,success:function(t){
							console.log(t);
							$('#token').val(t.result);
							
						}		
						,error:function(t){
							
						}
					});
				}
			}
		});
	});
	
	//그룹 전체조회
	$('#groupSelectAll').on('click',function(){
		$.ajax({
			url: "${defaultPath}/api/1/groupTree"
			,dataType : 'json'
			,contentType: "application/json; charset=UTF-8"
			,type:"GET"
			,data: {}
			,cache:!1
			,headers : {"Cache-Control":"no-cache"}
			,beforeSend : function(xhr){
				xhr.setRequestHeader("key", $('#token').val()); 
			}
			,success:function(t){
				alert("가져옴");
				console.log(t);
				var data = t.result;
				$('#groupList').empty();
				var str = "";
				for(var i = 0 ; i < data.length; i++){
					str += "<li>"
					str += "<span>그룹키 : "+data[i].groupKey+"</span>"
					str += "<span>   그룹명 : "+data[i].groupNm+"</span>"
					str += "<span>   부서장키 : "+data[i].managerAccountKey+"</span>"
					str += "<span>   부모키 : "+data[i].parentGroupKey+"</span>"
					str += "<span><button type='button' class='groupDel' data-key='"+data[i].groupKey+"' >삭제</button></span>"
					str += "</li>"
				}
				$('#groupList').html(str);
			}		
			,error:function(t){
				console.log(t);
			}
		});
	});
	
	$('body').on('click','.groupDel',function(){
		var groupKey = $(this).data('key');
		
		$.ajax({
			url: "${defaultPath}/api/1/group/"+groupKey
			,dataType : 'json'
			,contentType: "application/json; charset=UTF-8"
			,type:"DELETE"
			,data: {}
			,cache:!1
			,headers : {"Cache-Control":"no-cache"}
			,beforeSend : function(xhr){
				xhr.setRequestHeader("key", $('#token').val()); 
			}
			,success:function(t){
				console.log(t);
			}		
			,error:function(t){
				console.log(t);
				$('#groupSelectAll').trigger('click');
			}
		}).done(function(){
			console.log(t);
		});
	});
	
	//그룹생성
	$('#groupCreate').on('click',function(){
		
		var groupNm = $('#groupNm').val();
		var managerAccountKey = $('#managerAccountKey').val();
		var parentGroupKey = $('#parentGroupKey').val();
		
		var sendData = {
				"groupNm" : groupNm,
				"managerAccountKey" : managerAccountKey,
				"parentGroupKey" : parentGroupKey
		};
		
		$.ajax({
			url: "${defaultPath}/api/1/group"
			,dataType : 'json'
			,contentType: "application/json; charset=UTF-8"
			,type:"POST"
			,data:JSON.stringify(sendData)
			,cache:!1
			,headers : {"Cache-Control":"no-cache"}
			,beforeSend : function(xhr){
				xhr.setRequestHeader("key", $('#token').val()); 
			}
			,success:function(t){
				$('#groupSelectAll').trigger('click');
			}		
			,error:function(t){
				console.log(t);
			}
		});
	});

});

</script>
	</head>
    <body>
    	<h1>그룹 테스트 화면</h1>
    	로그인하기<br/>
    	아이디 : <input type="text" id="userId" value="pne1818@naver.com"></br>
    	비번 : <input type="text" id="userPw" value="asdf!@#123"></br>
    	만료시간 : <input type="text" id="expire" value="9999999"></br>
    	<button type="button" id="login">로그인</button>
    	<br/>
    	<br/>
    	<br/>
    	
    	토큰 : <input type="text" id="token" value="" style="width:1000px;"></br>
    	<br/>
    	
    	그룹명 : <input type="text" id="groupNm" value=""><br/>
    	부서장키 : <input type="text" id="managerAccountKey" value=""><br/>
    	상위부서키 : <input type="text" id="parentGroupKey" value=""><br/>
    	
    	    	
    	<button type="button" id="groupCreate">그룹 생성</button></br>
    	<button type="button" id="groupSelectAll">그룹 전체 조회</button></br>
    	<ul id="groupList">
    	
    	</ul>
    	
    </body>
</html>