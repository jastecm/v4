<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>실시간 자가진단</h2>
                    <span>
						8가지 주요 증상별  점검항목을 알려드립니다.<br />
						ViewCAR의 차량 센서데이터와 함께 전문 정비기사와 상담하세요.
					</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="vehicle-page">
                    <div class="self-diagnosis">
                        <div class="section01 mgt40">
                            <h3 class="tit2">아래 중 이상유형을 선택해주세요</h3>
                            <ul>
                                <li class="type01">
                                    <input type="radio" name="type" id="type01" checked/>
                                    <label for="type01">미작동</label>
                                </li>
                                <li class="type02">
                                    <input type="radio" name="type" id="type02" />
                                    <label for="type02">소리로<br />나타나는이상 </label>
                                </li>
                                <li class="type03">
                                    <input type="radio" name="type" id="type03" />
                                    <label for="type03">몸으로<br />느껴지는 이상</label>
                                </li>
                                <li class="type04">
                                    <input type="radio" name="type" id="type04" />
                                    <label for="type04">눈에<br />보이는이상 </label>
                                </li>
                                <li class="type05">
                                    <input type="radio" name="type" id="type05" />
                                    <label for="type05">냄새로<br />느껴지는 이상</label>
                                </li>
                                <li class="type06">
                                    <input type="radio" name="type" id="type06" />
                                    <label for="type06">에어컨이상</label>
                                </li>
                                <li class="type07">
                                    <input type="radio" name="type" id="type07" />
                                    <label for="type07">브레이크이상</label>
                                </li>
                                <li class="type08">
                                    <input type="radio" name="type" id="type08" />
                                    <label for="type08">경고등 정보</label>
                                </li>
                            </ul>
                        </div>

                        <div class="section02 mgt40">
                            <h3 class="tit2">선택항목에서의 이상 증상을 선택해주세요.</h3>
                            <div>
                                <ul>
                                    <li>
                                        <input type="radio" name="symptom" id="symptom01" checked />
                                        <label for="symptom01">에어컨 바람이 나오지 않습니다.</label>
                                    </li>
                                    <li>
                                        <input type="radio" name="symptom" id="symptom02" />
                                        <label for="symptom02">에어컨 작동시 "끼이익" 소리가 납니다.</label>
                                    </li>
                                    <li>
                                        <input type="radio" name="symptom" id="symptom03" />
                                        <label for="symptom03">에어컨을 켜면 곰팡이 냄새가 납니다.</label>
                                    </li>
                                    <li>
                                        <input type="radio" name="symptom" id="symptom04" />
                                        <label for="symptom04">에어컨을 켜면 온도 게이지가 올라갑니다.</label>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="section02 mgt40">
                            <h3 class="tit2">아래 질문에 답변해주세요</h3>
                            <div>
                                <strong>휴즈 점검을 했습니까?</strong>
                                <ul>
                                    <li>
                                        <input type="radio" name="answer" id="answer01" />
                                        <label for="answer01">예. 휴즈 점검을 했습니다.</label>
                                    </li>
                                    <li>
                                        <input type="radio" name="answer" id="answer02" checked />
                                        <label for="answer02">아니오 휴즈점검을 하지 않았습니다.</label>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="section03 mgt40">
                            <h3 class="tit2">실시간 자가진단결과는 아래와 같습니다.</h3>
                            <div>
                                <ul>
                                    <li>
                                        <strong>선택증상</strong> 에어컨 바람이 나오지 않습니다.
                                    </li>
                                    <li>
                                        <strong>진단결과</strong> 에어컨 휴즈 불량이 의심스럽습니다.
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="section04 mgt40">
                            <h3 class="tit2">위 진단결과에 따른 수리비 예상견적은 아래와 같습니다.</h3>
                            <div>
                                <ul>
                                    <li>
                                        <strong>예상부품비</strong> 10,000원
                                    </li>
                                    <li>
                                        <strong>공임비</strong> 5,000원
                                    </li>
                                    <li>
                                        <strong>추천 정비업소</strong>
                                        <span>바름정비 (서울 강북구 송천동점)</span>
                                        <span>정비사 임종순</span>
                                        <span>정비경력 20년</span>
                                        <button class="btn btn02 sm round">위치보기</button>
                                    </li>
                                </ul>
                            </div>
                            <p class="txt">※ 표준공임비는  『정품부품가격 + 표준공임시간 X 시간당 공임비 (국산차 50,000원) 』 산출식으로 계산됩니다. </p>
                        </div>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->