<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
#itemList td span.s2 {
    display: inline-block;
    vertical-align: top;
    line-height: 58px;
    width: 140px;
    background: #eaeaea;
}
#itemList span.s2 strong.c2 {
display:block; 
left:0; 
top:0; 
height:22px; 
background:#92cc70;
}
</style>
<script type="text/javascript">
var _vehicleKey = "${vehicleKey}";

var hiddenPartsKey;
var partNm;
var val;
var max;

$(function() {
	
	$V4.http_post("/api/1/vehicle/"+_vehicleKey, {}, {
		requestMethod : "GET",
		header : {
			"key" : "${_KEY}"
		},
		success : function(rtv) {
			console.log(rtv);
			var data = rtv.result;
			
			if(data.drivingState == 'unused'){
				$('#drivingState').addClass('state07').text("사용중지");
			}else if(data.drivingState == 'parking'){
				$('#drivingState').addClass('state08').text("주차중");
			}else if(data.drivingState == 'driving'){
				$('#drivingState').addClass('state04').text("주행중");
			}
			
			if(data.hasOwnProperty('device')){
				//단말기가 장착된거다
				$('#deviceSeries').text(data.device.deviceSeries+"/"+data.device.deviceSn);
			}
			$('#corpName').text(data.corp.corpName);
			$('#plateNum').text(data.plateNum);
			$('#totDistance').text(number_format(Math.round(data.totDistance / 1000))+" km");
			$('#volumn').text(data.vehicleModel.volume+" cc");
			$('#transmission').text(data.vehicleModel.transmission);
			$('#manufactureAndmodelHeader').text(data.vehicleModel.manufacture+"/"+data.vehicleModel.modelHeader);
			$('#modelMasterAndyear').text(data.vehicleModel.modelMaster+"/"+data.vehicleModel.year);
			$('#fuelType').text(data.vehicleModel.fuelType);
			
			//color 없음
			//차량등록일
			//차대번호
			//자동차검사만기일
			//비고
			
		},
		error : function(t) {
			console.log(t);
		}
	});
	
	var initItemList = function(){
		$V4.http_post("/api/1/vehicle/"+_vehicleKey+"/item", {}, {
			requestMethod : "GET",
			header : {
				"key" : "${_KEY}"
			},
			success : function(rtv) {
				console.log(rtv);
				var data = rtv.result;
				$('#itemList').empty();
				var strHtml = "";
				for(var i = 0 ; i < data.length;i++){
					var obj = data[i];
					console.log(obj);
					strHtml += '<tr>';
					strHtml += '	<td><input type="checkbox" data-itemkey='+obj.itemKey+'/></td>';
					strHtml += '    <td>'+obj.name+'</td>';
					/* strHtml += '    <td>'+obj.per+'</td>'; */
					strHtml += '<td><span class="s2"><strong class="c2 graph" style="width:'+obj.per+'%"></span></strong></td>'
					
					strHtml += '    <td><span>'+obj.val+'</span> km <button class="btn-img modify itemModify" data-key='+obj.itemKey+' data-type="val">수정</button></td>';
					strHtml += '    <td><span>'+obj.max+'</span> km <button class="btn-img modify itemModify" data-key='+obj.itemKey+' data-type="max">수정</button></td>';
					strHtml += '    <td>'+obj.emptyRemainDay+' 일</td>';
					strHtml += '    <td>'+obj.warningRemainDay+'일</td>';
					strHtml += '</tr>';
				}	
				$('#itemList').html(strHtml);
				
			},
			error : function(t) {
				console.log(t);
			}
		});
	}
	
	initItemList();
	
	
	$('body').on('click','.itemModify',function(){
		var self = $(this);
		var itemKey = self.data('key');
		var text = self.prev().text();
		self.prev().replaceWith('<input type="text" style="width: 100px;" class="numberOnly itemInput" value="'+text+'" />')
		self.replaceWith('<button class="btn sm round itemOk" style="min-width: 10px;" data-key="'+itemKey+'">수정</button>');
	});

	$('body').on('keyup','.itemInput',function(e){
		if(e.keyCode == 13){
			$('.itemOk').trigger('click');	
		}
	});
	$('body').on('click','.itemOk',function(){
		var self=  $(this);
		var itemKey = self.data('key');
		var changeValue = self.prev().val();
		//console.log(value);		
		//$(this).data('changeValue',changeValue);
		
		var type = self.data('type')
		
		//수정이 남은거리
		if(type == 'val'){
			
		}else{//교환주기max
			
		}
			
		
		self.prev().replaceWith('<span>'+changeValue+'</span>');
		self.replaceWith('<button class="btn-img modify itemModify" data-key="'+itemKey+'">수정</button>');
	});
	
	$('#itemAdd').on('click',function(){
		var strHtml = "";
		strHtml += '<tr>';
		strHtml += '	<td></td>';
		strHtml += '    <td><input type="text" readonly="readonly" style="width:200px;" />';
		strHtml += '	<input type="hidden" name="partsKey" />';
		strHtml += ' 	<button type="button" class="btn sm round partsPopup" style="width: 53px;min-width: 0px;">선택</button>';
		strHtml += '	</td>';
		strHtml += '	<td><span class="s2"><strong class="c2 graph" style="width:0%"></span></strong></td>'
		strHtml += '    <td><input type="text" name="val"/></td>';
		strHtml += '    <td><input type="text" name="max"/></td>';
		strHtml += '    <td></td>';
		strHtml += '    <td><button type="button" class="btn sm round partsAdd" style="width: 53px;min-width: 0px;">적용</button><button type="button" class="btn sm round partsCancel" style="width: 53px;min-width: 0px;">취소</button></td>';
		strHtml += '</tr>';
		$('#itemList tr').last().after(strHtml);
		
	});
	$('#itemDelete').on('click',function(){
		var chkbox = $('#itemList input[type="checkbox"]:checked');
		for(var i = 0 ; i < chkbox.length;i++){
			var itemKey = $(chkbox[i]).data('itemkey')
			$V4.http_post("/api/1/vehicle/"+_vehicleKey+"/item/"+itemKey, {}, {
				sync : true,
				requestMethod : "DELETE",
				header : {
					"key" : "${_KEY}"
				},
				success : function(rtv) {
					
				},
				error : function(t) {
					console.log(t);
				}
			});
		}
		
		initItemList();
	});
	
	 var thead = [
	              '',
	              'partsTypeNm',
	              'partsNum',
	              'partsNm'
	     ];
	 
	 var lazySetting = {
    		thead : thead
    		,bodyHtml : (function(data){
    			var strHtml = "";
    			
    			$('#partsCnt').text(data.totalCnt);
    			
    			
    			for(i in data.result){
    				var obj = data.result[i];
    				strHtml += '<tr>';
    				strHtml += '	<td>';
    				
    				var jsonData = {
        					"partsKey" : obj.partsKey,
        					"defaultMax" : obj.defaultMax,
        					"partsNm" : obj.partsNm
        			}
    				
    				strHtml += '	    <input type="radio" name="part" data-info="'+jsonObjToBase64(jsonData)+'" />';
    				strHtml += '	</td>';
    				strHtml += '	<td>'+obj.partsTypeNm+'</td>';
    				strHtml += '	<td>'+obj.partsNum+'</td>';
    				strHtml += '	<td>'+obj.partsNm+'</td>';
    				strHtml += '</tr>';
    			}
    			return strHtml;
    		})
    		,limit : 10
    		,offset : 0
    		,url : "/api/1/parts"
    		,param : {}
    		,http_post_option : {
    			requestMethod : "GET"
    			,header : {key : "${_KEY}"}
    		}
    		,initSearch : true //생성과 동시에 search		
    		,loadingBodyViewer : true //로딩중!! 표시됨
    		,debugMode : true
    	}
	
	 $("#parts-dialog").dialog({
	        autoOpen: false,
	        show: {
	            duration: 500
	        },
	        width: '960',
	        modal: true
	 });

	 $(document).on('click','.partsPopup',function(){
		 var self = $(this);
		 
		 hiddenPartsKey = $(this).prev();
		 partNm = $(this).prev().prev();
		 val = self.parent().parent().find('input[name="val"]');
		 max = self.parent().parent().find('input[name="max"]');
		 
		 $('#partsList').commonLazy(lazySetting);
		 initPartsDistinct();
		 $("#parts-dialog").dialog("open") 
	 });
	 
	 var initPartsDistinct = function(){
		 
		 $V4.http_post("/api/1/partsDistinct", {}, {
				requestMethod : "GET",
				header : {
					"key" : "${_KEY}"
				},
				success : function(rtv) {
					var data = rtv.result;
					$('#partsDistinctList').empty();
					$('#partsDistinctList').append('<option value="">전체</option>');
					for(var i = 0 ; i < data.length ; i++){
						var obj = data[i];
						$('#partsDistinctList').append('<option value="'+obj.partsType+'">'+obj.partsTypeNm+'</option>')
						
					}
					//<option value="">전체</option>
				},
				error : function(t) {
					console.log(t);
				}
			});
	 }
	 
	 $('#popUpSearchBtn').on('click',function(){
		 
		 var partsType = $('#partsDistinctList').val();
		 var searchPartsNm = $('#partsSearchInput').val();
		 
		 lazySetting.param = {
				 "partsType" : partsType,
				 "searchPartsNm" : searchPartsNm
		  };
		 
		 $('#partsList').commonLazy(lazySetting);
	 });
	 
	 $('#partsPopupCancelBtn').on('click',function(){
		 $("#parts-dialog").dialog("close");
	 });
	 
	 //팝업 선택
 	 $('#partsPopupSelectBtn').on('click',function(){
 		
 		var self = $(this);
 		var selectObj = $('#partsList input[name="part"]:checked');
 		var info = selectObj.data('info');
 		var jsonData = base64ToJsonObj(info);
 		
 		var partsKey = jsonData.partsKey;
 		var defaultMax = jsonData.defaultMax;
 		var partsNm = jsonData.partsNm;
 		
 		hiddenPartsKey.val(partsKey);
 		partNm.val(partsNm);
 		val.val(defaultMax);
 		max.val(defaultMax);
 		
 		$("#parts-dialog").dialog("close");
 		
	 });
	 
	 
	 //파츠 추가
	 $(document).on('click','.partsAdd',function(){
		 var tr = $(this).parent().parent();
		 var partsKey = tr.find('input[name="partsKey"]').val();
		 var val = tr.find('input[name="val"]').val();
		 var max = tr.find('input[name="max"]').val();
		 
		 $V4.http_post("/api/1/vehicle/"+_vehicleKey+"/item", {
			 "partsKey" : partsKey,
			 "val" : val,
			 "max" : max
		 }, {
				requestMethod : "POST",
				header : {
					"key" : "${_KEY}"
				},
				success : function(rtv) {
					alert('추가 되었습니다.');
					initItemList();
				},
				error : function(t) {
					console.log(t);
				}
			});
		 
	 });
	 
	 //취소
 	 $(document).on('click','.partsCancel',function(){
 		$(this).parent().parent().remove();
	 });
	 
	    
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>고장ㆍ소모품 현황</h2>
                    <span> 등록차량의 고장 및 소모품 교체 알림내역을 조회할 수 있습니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="driving-page">
                    <div class="box-layout register-vehicle-detail">
                        <div class="title">
                            <h3 class="tit3">소모품 상세정보</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
                            <!-- 상단 상태값 -->
                            <div class="top-state clr">
                                <div class="time">
                                    <span>2018/05/27</span>
                                    <span>19:53</span>
                                    <span>현재</span>
                                    <button class="btn btn04">새로고침</button>
                                </div>
                            </div>
                            <!--/ 상단 상태값 -->

                            <div class="vehicle-info">
                                <div class="img">
                                    <img src="${pageContext.request.contextPath}/common/new/img/vehicle/L47.png" alt="" />
                                </div>
                                <div class="info">
                                    <table class="table mgt20">
                                        <caption>등록차량 정보</caption>
                                        <colgroup>
                                            <col style="width:21.5%" />
                                            <col style="width:28.5%" />
                                            <col style="width:21.5%" />
                                            <col style="width:28.5%" />
                                        </colgroup>
                                        <tbody>
                                        	<c:if test="${V4.corp.corpType eq '4'}">
                                            <tr>
                                                <th scope="row">소속</th>
                                                <td colspan="3" id="corpName"></td>
                                            </tr>
                                            </c:if>
                                            <tr>
                                                <th scope="row">ViewCAR모델명/일련번호</th>
                                                <td colspan="3" id="deviceSeries"></td>
                                            </tr>
                                        </tbody>
                                        <tbody>
                                            <tr>
                                                <th scope="row">차량번호</th>
                                                <td id="plateNum"></td>
                                                <th scope="row">배기량</th>
                                                <td id="volumn"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">기어방식</th>
                                                <td id="transmission"></td>
                                                <th scope="row">총주행거리</th>
                                                <td id="totDistance"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">제조사/차종</th>
                                                <td id="manufactureAndmodelHeader"></td>
                                                <th scope="row">모델명/년식</th>
                                                <td id="modelMasterAndyear"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">유종</th>
                                                <td id="fuelType"></td>
                                                <th scope="row">색상</th>
                                                <td id="color"><!-- 검정 --></td>
                                            </tr>
                                        </tbody>
                                        <tbody>
                                            <tr>
                                                <th scope="row">차량등록일</th>
                                                <td><!-- 2011-12-22 --></td>
                                                <th scope="row">차대번호</th>
                                                <td><!-- KN234266ND523423G --></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">자동차검사만기일</th>
                                                <td><!-- 2011-12-22 --></td>
                                                <th scope="row">비고</th>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                             <div class="mgt20">
                            	<h3 class="tit2 mgt40">과거 차량정비/소모품 교환 이력</h3>
                                        <!-- table 버튼 -->
                                        <div class="btn-function title-side">
                                            <div class="right">
                                            	<button type="button" class="write" id="itemAdd">추가</button>
                                            	<button type="button" class="delete" id="itemDelete">선택삭제</button>
                                            </div>
                                        </div>
                                        <!--/ table 버튼 -->

                                        <table class="table list mgt20">
                                            <caption>등록된 서비스 리스트</caption>
                                            <colgroup>
                                                <col style="width:10%" />
                                                <col />
                                                <col style="width:12%" />
                                                <col style="width:15%" />
                                                <col style="width:15%" />
                                                <col style="width:12%" />
                                                <col style="width:12%" />
                                            </colgroup>
                                            <thead>
                                                <tr>
                                                	<th scope="col"><input type="checkbox"/></th>
                                                    <th scope="col">소모품명</th>
                                                    <th scope="col"></th>
                                                    <th scope="col">남은거리</th>
                                                    <th scope="col">교환주기</th>
                                                    <th scope="col">예상교환주기일</th>
                                                    <th scope="col">10%남은일</th>
                                                </tr>
                                            </thead>
                                            <tbody id="itemList">
                                            	<tr>
                                                    <td colspan="8">데이터가 없습니다.</td>
                                                </tr>
                                            </tbody>
                                        </table>
                        		</div>
                        </div>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->
<!-- 부품 팝업 -->
<div id="parts-dialog" title="부품 선택">
        <div class="top-info clr">
            <span class="left">등록된 모든 부품을 표시합니다.</span>
            <span class="right car">
				총 부품 대수
				<strong id="partsCnt">0</strong>개
			</span>
        </div>
        <!-- 상단 검색 -->
        <div class="top-search">
            <select id="partsDistinctList">
				
			</select>
            <input type="text" id="partsSearchInput" />
            <button type="button" class="btn btn03" id="popUpSearchBtn">조회</button>
        </div>
        <!--/ 상단 검색 -->

        <div class="scroll-table mgt50">
            <table class="table list" id="partsList">
            </table>
        </div>

        <!-- 하단 버튼 -->
        <div class="btn-bottom">
            <button type="button" class="btn btn02" id="partsPopupCancelBtn">취소</button>
            <button type="button" class="btn btn03" id="partsPopupSelectBtn">선택</button>
        </div>
        <!--/ 하단 버튼 -->
    </div>
    <!-- 부품 팝업 -->