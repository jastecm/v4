<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	// 달력
    $("#start-date").datepicker({
        dateformat: 'yy-mm-dd'
    });

    $("#end-date").datepicker({
        dateformat: 'yy-mm-dd'
    });

    // 탭메뉴
    $(".tab-menu>li>a").click(function() {
        $(this).parent().addClass("on").siblings().removeClass("on");
        return false;
    });

    $("#breakdown-dialog").dialog({
        autoOpen: true,
        show: {
            duration: 500
        },
        width: '960',
        modal: true
    });

    $("#breakdown-open").on("click", function() {
        $("#breakdown-dialog").dialog("open");
    });
    
    $(document).on('click','.itemDetail',function(){
    	//vehicleKey를 넘겨야함
    	//$V4.move('/vehicleMng/troubleAndConsumables/detail',{"vehicleKey":"vehicleKey"});
    });
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>고장ㆍ소모품현황 조회</h2>
                    <span>등록차량의 고장 및 소모품 교체 알림내역을 조회할 수 있습니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="vehicle-page">
                    <div class="breakdown-list">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                            <div class="item">
                                <span>
									등록 차량수
									<strong>10</strong>대
								</span>
                                <span>
									정비필요 차량수
									<strong>8</strong>대
								</span>
                                <span>
									소모품 교환필요 차량수
									<strong>2</strong>대
								</span>
                            </div>

                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <!-- 상단 검색 -->
                        <h3 class="tit2 mgt30">차량정비 및 소모품 현황 조회</h3>
                        <div class="top-search mgt20">
                            <select name="">
								<option value="">전체</option>
								<option value="">차량 10년이상</option>
								<option value="">소모품 교체이력 없음</option>
								<option value="">3년 OEM 보증기간 만료</option>
							</select>
                            <input type="text" name="" />
                            <button class="btn btn03">조회</button>
                        </div>
                        <!--/ 상단 검색 -->

                        <!-- table 버튼 -->
                        <div class="btn-function row-2">
                            <div class="left">
                                <select name="">
									<option value="">최근 운행순</option>
								</select>

                                <div class="date">
                                    <button class="active">전체</button>
                                    <button>오늘</button>
                                    <button>1주일</button>
                                    <button>1개월</button>
                                    <button>3개월</button>
                                    <button>6개월</button>
                                    <div class="direct">
                                        <input type="text" id="start-date" /> &nbsp;~&nbsp;
                                        <input type="text" id="end-date" />
                                    </div>
                                    <!-- 기획서에는 직접입력이 셀렉트박스로 되어 있는데, 사용자가 어떻게 날짜를 입력 해야 하는지 모르겠어서 임의로 변경했습니다.-->
                                </div>
                            </div>
                            <div class="right">
                                <select name="">
									<option value="">5건씩 보기</option>
								</select>
                            </div>
                        </div>
                        <!--/ table 버튼 -->

                        <ul class="page-tab tab-menu col-4">
                            <!-- 차량 현황 -->
                            <li class="list01 on">
                                <a href="#">정비필요 차량</a>
                                <div class="tab-list">
                                    <table class="table list mgt20">
                                        <caption>등록된 서비스 리스트</caption>
                                        <colgroup>
                                            <col style="width:10%" />
                                            <col style="width:20%" />
                                            <col style="width:10%" />
                                            <col style="width:10%" />
                                            <col />
                                            <col style="width:10%" />
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th scope="col">회사명</th>
                                                <th scope="col">차량정보</th>
                                                <th scope="col">년식/주행거리</th>
                                                <th scope="col">소속/지원</th>
                                                <th scope="col">고장내역</th>
                                                <th scope="col">상세보기</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>㈜자스텍엠</td>
                                                <td>
                                                    <div class="car-img">
                                                        <span class="img"><img src="./img/vehicle/S103.png" alt="" /></span>
                                                        <span class="num">쏘나타 63호 5703</span>
                                                    </div>
                                                </td>
                                                <td>2011년<br />7,219.8km</td>
                                                <td>총무팀</td>
                                                <td class="left">
                                                    <div class="driving-info">
                                                        <ul>
                                                            <li>
                                                                <strong class="title">고장코드</strong>
                                                                <span>P1024454</span>
                                                            </li>
                                                            <li>
                                                                <strong class="title">고장내용</strong>
                                                                <span>흡입 기류량 또는 부피 회로 간혈 작동</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                                <td><button type="button" class="btn-img repair itemDetail">위치</button></td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <!-- 페이징 -->
                                    <div id="paging">
                                        <a href="" class="first icon-spr">제일 처음으로</a>
                                        <a href="" class="prev icon-spr">이전으로</a>
                                        <span class="current">1</span>
                                        <a href="" class="num">2</a>
                                        <a href="" class="num">3</a>
                                        <a href="" class="num">4</a>
                                        <a href="" class="num">5</a>
                                        <a href="" class="next icon-spr">다음으로</a>
                                        <a href="" class="last icon-spr">제일 마지막으로</a>
                                    </div>
                                    <!--/ 페이징 -->
                                </div>
                            </li>

                            <li class="list02">
                                <a href="#">소모품교환 필요 차량</a>
                                <div class="tab-list">
                                    <table class="table list mgt20">
                                        <caption>등록된 서비스 리스트</caption>
                                        <colgroup>
                                            <col style="width:10%" />
                                            <col style="width:20%" />
                                            <col style="width:10%" />
                                            <col style="width:10%" />
                                            <col />
                                            <col style="width:10%" />
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th scope="col">회사명</th>
                                                <th scope="col">차량정보</th>
                                                <th scope="col">년식/주행거리</th>
                                                <th scope="col">소속/지원</th>
                                                <th scope="col">소모품항목</th>
                                                <th scope="col">상세보기</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>㈜자스텍엠</td>
                                                <td>
                                                    <div class="car-img">
                                                        <span class="img"><img src="./img/vehicle/S103.png" alt="" /></span>
                                                        <span class="num">쏘나타 63호 5703</span>
                                                    </div>
                                                </td>
                                                <td>2011년<br />7,219.8km</td>
                                                <td>총무팀</td>
                                                <td>소모품항목</td>
                                                <td><button class="btn-img repair" id="breakdown-open">상세보기</button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </li>
                        </ul>

                        <!-- 안내 -->
                        <div class="box-info">
                            <h3 class="hidden">안내</h3>
                            <h4 class="mgt0">소모품상태 상세내용</h4>
                            <ul>
                                <li>표 우측에 있는 상세보기 버튼을 누르면 소모품상태 상세내용을 볼 수 있습니다.</li>
                            </ul>
                            <h4>고장이력</h4>
                            <ul>
                                <li>내용을 누르면 점검이력을 확인할 수 있습니다. </li>
                            </ul>
                        </div>
                        <!--/ 안내 -->
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->
          <!-- 배차정보 팝업 -->
    <div id="breakdown-dialog" title="고장내역">
        <table class="table list mgt20">
            <caption>고장내역 리스트</caption>
            <colgroup>
                <col style="width:18%" />
                <col style="width:10%" />
                <col style="width:15%" />
                <col />
            </colgroup>
            <thead>
                <tr>
                    <th scope="col">진단일시</th>
                    <th scope="col">고장코드</th>
                    <th scope="col">관련부품</th>
                    <th scope="col">고장내용</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>2018/05/20 21:12</td>
                    <td>P104</td>
                    <td>엔진</td>
                    <td class="left">흡인 기류랑 또는 부피 A 회로 간혈 작동</td>
                </tr>
            </tbody>
        </table>

        <!-- 하단 버튼 -->
        <div class="btn-bottom">
            <button class="btn btn03">확인</button>
        </div>
        <!--/ 하단 버튼 -->
    </div>
    <!--/ 배차정보 팝업 -->