<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	$("#start-date").datepicker({
        dateformat: 'yy-mm-dd'
    });

    $("#end-date").datepicker({
        dateformat: 'yy-mm-dd'
    });
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2></h2>
                    <span></span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="vehicle-page">
                    <div class="box-layout driving-schedule">
                        <div class="title">
                            <h3 class="tit3">운행 스케쥴 상세정보</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
                            <!-- 상단 상태값 -->
                            <div class="top-state clr">
                                <div class="item">
                                    <span>운행여부</span>
                                    <strong class="states-value state08">미배차</strong>
                                </div>

                                <div class="time">
                                    <span>2018/05/27</span>
                                    <span>19:53</span>
                                    <span>현재</span>
                                    <button type="button" class="btn btn04">새로고침</button>
                                </div>
                            </div>
                            <!--/ 상단 상태값 -->

                            <div class="section01">
                                <div class="left"><img src="./img/common/user-img3.png" alt="" /></div>
                                <div class="right">
                                    <table class="table mgt20">
                                        <caption>운행스케쥴 상세정보</caption>
                                        <colgroup>
                                            <col style="width:20%" />
                                            <col style="width:30%" />
                                            <col style="width:20%" />
                                            <col style="width:30%" />
                                        </colgroup>
                                        <tbody>
                                            <tr>
                                                <th scope="row">소속/지점</th>
                                                <td>카카오VX</td>
                                                <th scope="row">전화번호</th>
                                                <td>010-4086-1495</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">부서/이름</th>
                                                <td>팀원/오승준</td>
                                                <th scope="row">휴대폰</th>
                                                <td>010-4086-1495</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">&nbsp;</th>
                                                <td>&nbsp;</td>
                                                <th scope="row">이메일</th>
                                                <td>osj1490@maumgolf.com</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <!-- table 버튼 -->
                            <div class="btn-function row-2">
                                <div class="left">
                                    <select name="">
												<option value="">최근 운행순</option>
											</select>

                                    <div class="date">
                                        <button type="button" class="active">전체</button>
                                        <button type="button">오늘</button>
                                        <button type="button">1주일</button>
                                        <button type="button">1개월</button>
                                        <button type="button">3개월</button>
                                        <button type="button">6개월</button>
                                        <div class="direct">
                                            <input type="text" id="start-date" /> &nbsp;~&nbsp;
                                            <input type="text" id="end-date" />
                                        </div>
                                        <!-- 기획서에는 직접입력이 셀렉트박스로 되어 있는데, 사용자가 어떻게 날짜를 입력 해야 하는지 모르겠어서 임의로 변경했습니다.-->
                                    </div>
                                </div>
                                <div class="right">
                                    <button type="button" class="excel">엑셀 다운로드</button>
                                    <select name="">
										<option value="">5건씩 보기</option>
									</select>
                                </div>
                            </div>
                            <!--/ table 버튼 -->

                            <table class="table list mgt20">
                                <caption>등록된 서비스 리스트</caption>
                                <colgroup>
                                    <col style="width:10%" />
                                    <col style="width:17%" />
                                    <col style="width:15%" />
                                    <col />
                                    <col style="width:25%" />
                                    <col style="width:8%" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th scope="col">
                                            <select name="" class="arrow">
												<option value="">상태</option>
											</select>
                                        </th>
                                        <th scope="col">시간</th>
                                        <th scope="col">거래처명</th>
                                        <th scope="col">주소</th>
                                        <th scope="col">이용차량</th>
                                        <th scope="col">삭제</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>이탈</td>
                                        <td>2018/04/08 11:10</td>
                                        <td>야탑물류.</td>
                                        <td class="left">경기도 성남시 분당구 판교로 70</td>
                                        <td>
                                            <div class="car-img">
                                                <span class="img"><img src="./img/vehicle/S103.png" alt="" /></span>
                                                <span class="num">쏘나타 63호 5703</span>
                                            </div>
                                        </td>
                                        <td><button type="button" class="btn-img delete">삭제</button></td>
                                    </tr>
                                </tbody>
                            </table>

                            <!-- 페이징 -->
                            <div id="paging">
                                <a href="" class="first icon-spr">제일 처음으로</a>
                                <a href="" class="prev icon-spr">이전으로</a>
                                <span class="current">1</span>
                                <a href="" class="num">2</a>
                                <a href="" class="num">3</a>
                                <a href="" class="num">4</a>
                                <a href="" class="num">5</a>
                                <a href="" class="next icon-spr">다음으로</a>
                                <a href="" class="last icon-spr">제일 마지막으로</a>
                            </div>
                            <!--/ 페이징 -->
                        </div>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->