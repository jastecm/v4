<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	 $("#start-date").datepicker({
         dateformat: 'yy-mm-dd'
     });

     $("#end-date").datepicker({
         dateformat: 'yy-mm-dd'
     });
     
     $(document).on('click','.scheduleDetail',function(){
    	 $V4.move("/vehicleMng/drivingSchedule/detail");
     });
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>운행 스케쥴</h2>
                    <span>등록한 거래처 1km이내 접근 시, 직원의 운행스케쥴에 기록됩니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="vehicle-page">
                    <div class="">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                            <div class="item">
                                <span>
									등록 거래처 수
									<strong>10</strong>개
								</span>
                                <span>
									등록 차량
									<strong>8</strong>대
								</span>
                                <span>
									등록 사용자
									<strong>2</strong>명
								</span>
                            </div>

                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button type="button" class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <!-- 상단 검색 -->
                        <div class="top-search mgt20">
                            <select name="">
								<option value="">전체</option>
							</select>
                            <input type="text" name="" />
                            <button type="button" class="btn btn03">조회</button>
                        </div>
                        <!--/ 상단 검색 -->


                        <!-- table 버튼 -->
                        <div class="btn-function row-2">
                            <div class="left">
                                <select name="">
										<option value="">최근 운행순</option>
									</select>

                                <div class="date">
                                    <button type="button" class="active">전체</button>
                                    <button type="button">오늘</button>
                                    <button type="button">1주일</button>
                                    <button type="button">1개월</button>
                                    <button type="button">3개월</button>
                                    <button type="button">6개월</button>
                                    <div class="direct">
                                        <input type="text" id="start-date" /> &nbsp;~&nbsp;
                                        <input type="text" id="end-date" />
                                    </div>
                                    <!-- 기획서에는 직접입력이 셀렉트박스로 되어 있는데, 사용자가 어떻게 날짜를 입력 해야 하는지 모르겠어서 임의로 변경했습니다.-->
                                </div>
                            </div>
                            <div class="right">
                                <select name="">
										<option value="">5건씩 보기</option>
									</select>
                            </div>
                        </div>
                        <!--/ table 버튼 -->

                        <table class="table list mgt20">
                            <caption>등록된 서비스 리스트</caption>
                            <colgroup>
                                <col style="width:25%" />
                                <col style="width:25%" />
                                <col style="width:25%" />
                                <col style="width:25%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="col">사용자 정보</th>
                                    <th scope="col">부서/직급</th>
                                    <th scope="col">최근 이용차량</th>
                                    <th scope="col">운행스케쥴</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="user-img">
                                            <span class="img"><img src="./img/common/user-default.png"alt="" /></span>
                                            <span class="name">maumgolf / 카카오VX</span>
                                        </div>
                                    </td>
                                    <td>중부설치팀/팀원</td>
                                    <td>
                                        <div class="car-img">
                                            <span class="img"><img src="./img/vehicle/S103.png" alt="" /></span>
                                            <span class="num">쏘나타 63호 5703</span>
                                        </div>
                                    </td>
                                    <td>
                                    	<button type="button" class="btn-img change scheduleDetail">상세보기</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <!-- 페이징 -->
                        <div id="paging">
                            <a href="" class="first icon-spr">제일 처음으로</a>
                            <a href="" class="prev icon-spr">이전으로</a>
                            <span class="current">1</span>
                            <a href="" class="num">2</a>
                            <a href="" class="num">3</a>
                            <a href="" class="num">4</a>
                            <a href="" class="num">5</a>
                            <a href="" class="next icon-spr">다음으로</a>
                            <a href="" class="last icon-spr">제일 마지막으로</a>
                        </div>
                        <!--/ 페이징 -->
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->