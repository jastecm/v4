<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
var _maintenanceKey = '${maintenanceKey}';
var _vehicleKey = '${vehicleKey}';
$(function(){
	
	if(_maintenanceKey == ""){
		$('#maintenanceReqBtn').show();	
	}
	
	$('#maintenanceReqCancelBtn').on('click',function(){
		window.history.back(-1);
	});
	
	if(_vehicleKey != ""){
		$V4.http_post("/api/1/vehicle/"+_vehicleKey, {}, {
			requestMethod : "GET",
			header : {
				"key" : "${_KEY}"
			},
			success : function(rtv) {
				var data = rtv.result;
				$('#plateNum').val(data.plateNum);
				$('#vehicleKey').val(data.vehicleKey);
				
				var vehicleGroup = "";
				if(data.groups){
					for(i in data.groups){
						if(i!=0) vehicleGroup += "</br>";
						vehicleGroup += (data.groups[i].parentGroupNm)?(convertNullString(data.groups[i].parentGroupNm)):"";
						vehicleGroup += (data.groups[i].groupNm)?("/"+convertNullString(data.groups[i].groupNm)):"";
					}
					
				}
				$('#vehicleGroup').html(vehicleGroup);
				
				var vehicleManager = getProperty(data,'corp.manager.vehicleManager.corpPosition')+" "+getProperty(data,'corp.manager.vehicleManager.name');
				$('#vehicleManager').html(vehicleManager);
				
				
				$('#manufactureAndmodelHeader').text(data.vehicleModel.manufacture+"/"+data.vehicleModel.modelHeader);
				$('#modelMaster').text(data.vehicleModel.modelMaster);
				$('#year').text(data.vehicleModel.year);
				$('#vehicleBizType').text(vehicleBizTypeName(data.vehicleBizType,_lang));
				
				
			},
			error : function(t) {
				console.log(t);
			}
		});
		
		
		$V4.http_post("/api/1/maintenance", {"maintenanceKey" : _maintenanceKey, "counting" : false}, {
			requestMethod : "GET",
			header : {
				"key" : "${_KEY}"
			},
			success : function(rtv) {
				var data = rtv.result[0];
				console.log(rtv);
				
				$('#periodicInspection').val(convertDateUint(new Date(data.periodicInspection),_unitDate,_timezoneOffset,false));
				$('#nextPeriodicInspection').val(convertDateUint(new Date(data.nextPeriodicInspection),_unitDate,_timezoneOffset,false));
				$('#accountKey').val(getProperty(data,'account.accountKey'));
				$('#name').val(getProperty(data,'account.name'));
				
				$("#groupKey").val(getProperty(data,'group.groupKey'));
				$("#groupNm").val(getProperty(data,'group.groupNm') ? getProperty(data,'group.groupNm') :  getProperty(data,'group.parentGroupNm'));
				
				$('#maintenanceReqDate').val(convertDateUint(new Date(data.maintenanceReqDate),_unitDate,_timezoneOffset,false));
				$('#completeReqDate').val(convertDateUint(new Date(data.completeReqDate),_unitDate,_timezoneOffset,false));
				
				$('#troubleDesc').val(convertNullString(getProperty(data,'troubleDesc')));
				
				
			},
			error : function(t) {
				console.log(t);
			}
		});
		
	}
	
    $('#vehiclePopBtn').data({
		type : "vehicle",
		http_post_option : {
			header : {
				key : "${_KEY}"
			}
		},
		selType : "radio",
		action : {
			ok : function(arrSelect, inputObj, valueObj, _this) {
				var info = base64ToJsonObj($(arrSelect).data("info"));
				$(inputObj).val(info.plateNum);
				$(valueObj).val(info.vehicleKey);
				
				$V4.http_post("/api/1/vehicle/"+info.vehicleKey, {}, {
					requestMethod : "GET",
					header : {
						"key" : "${_KEY}"
					},
					success : function(rtv) {
						var data = rtv.result;
						
						var vehicleGroup = "";
						if(data.groups){
	    					for(i in data.groups){
	    						if(i!=0) vehicleGroup += "</br>";
	    						vehicleGroup += (data.groups[i].parentGroupNm)?(convertNullString(data.groups[i].parentGroupNm)):"";
	    						vehicleGroup += (data.groups[i].groupNm)?("/"+convertNullString(data.groups[i].groupNm)):"";
	    					}
	    					
	    				}
						$('#vehicleGroup').html(vehicleGroup);
						
						var vehicleManager = getProperty(data,'corp.manager.vehicleManager.corpPosition')+" "+getProperty(data,'corp.manager.vehicleManager.name');
						$('#vehicleManager').html(vehicleManager);
						
						
						$('#manufactureAndmodelHeader').text(data.vehicleModel.manufacture+"/"+data.vehicleModel.modelHeader);
						$('#modelMaster').text(data.vehicleModel.modelMaster);
						$('#year').text(data.vehicleModel.year);
						$('#vehicleBizType').text(vehicleBizTypeName(data.vehicleBizType,_lang));
						
						
					},
					error : function(t) {
						console.log(t);
					}
				});
				
				
			}
		},
		targetInput : $('#plateNum'),
		targetInputValue : $('#vehicleKey')
	}).commonPop();
    
    $("#userPop").data({
		type : "user",
		http_post_option : {
			header : {
				key : "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyS2V5IjoiNDRiZWRkMjItOGUzNS0xMWU4LWFiMWYtNDIwMTBhOGMwMDE4IiwiZXhwaXJlZCI6MTU0NDgyNTAwOSwiY29ycEtleSI6IjczNGZiMDk4LThlMzUtMTFlOC1hYjFmLTQyMDEwYThjMDAxOCIsImNvcnBUeXBlIjoiMiIsInNlcnZpY2VHcmFkZSI6IjEifQ.nPtZwAXmHu2ffm5LS27dm3MnfUK2xyMPwrO6lZvPgow"
			}
		},
		selType : "radio",
			action : {
				ok : function(arrSelect, inputObj, valueObj) {
					$.each(arrSelect, function() {
						var info = base64ToJsonObj($(arrSelect).data("info"));
						$(inputObj).val(info.name);
						$(valueObj).val(info.accountKey);
					});
				}
			},
			targetInput : $("#name"),
			targetInputValue : $("#accountKey")
	}).commonPop();
    
    $('#groupPop').data({
		type : "group",
		http_post_option : {
			header : {
				key : "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyS2V5IjoiNDRiZWRkMjItOGUzNS0xMWU4LWFiMWYtNDIwMTBhOGMwMDE4IiwiZXhwaXJlZCI6MTU0NDgyNTAwOSwiY29ycEtleSI6IjczNGZiMDk4LThlMzUtMTFlOC1hYjFmLTQyMDEwYThjMDAxOCIsImNvcnBUeXBlIjoiMiIsInNlcnZpY2VHcmFkZSI6IjEifQ.nPtZwAXmHu2ffm5LS27dm3MnfUK2xyMPwrO6lZvPgow"
			}
		},
		selType : "radio",
			action : {
				ok : function(arrSelect, inputObj, valueObj) {
					$.each(arrSelect, function() {
						var info = base64ToJsonObj($(arrSelect).data("info"));
						$(inputObj).val(info.groupNm ? info.groupNm : info.parentGroupNm);
						$(valueObj).val(info.groupKey);
					});
				}
			},
			targetInput : $("#groupNm"),
			targetInputValue : $("#groupKey")
	}).commonPop();
    
    $('#maintenanceReqBtn').on('click',function(){
    	
    	if( $V4.requiredMsg($('#vehicleKey').val() , "차량선택은 필수입니다.") ) return false;
    	
    	if( $V4.requiredMsg($('#periodicInspection').val() , "정기검사일은 필수입니다.") ) return false;
    	if( $V4.requiredMsg($('#nextPeriodicInspection').val() , "다음검사일은 필수입니다.") ) return false;
    	
    	if( $V4.requiredMsg($('#accountKey').val() , "사용자 선택은 필수입니다.") ) return false;
    	if( $V4.requiredMsg($('#groupKey').val() , "사용부서 선택은 필수입니다.") ) return false;
    	
    	var vehicleKey = $('#vehicleKey').val();
    	var periodicInspection = $('#periodicInspection').val();
    	var nextPeriodicInspection = $('#nextPeriodicInspection').val();
    	var accountKey = $('#accountKey').val();
    	var groupKey = $('#groupKey').val();
    	
    	var maintenanceReqDate = $('#maintenanceReqDate').val();
    	var completeReqDate = $('#completeReqDate').val();
    	var troubleDesc = $('#troubleDesc').val();
    	
    	var sendData = {
    			"vehicleKey" : vehicleKey,
    			"periodicInspection" : periodicInspection,
    			"nextPeriodicInspection" : nextPeriodicInspection,
    			"accountKey" : accountKey,
    			"groupKey" : groupKey,
    			"maintenanceReqDate" : maintenanceReqDate,
    			"completeReqDate" : completeReqDate,
    			"troubleDesc" : troubleDesc
    	}
    	
    	//정비 요청 등록
    	
    	$V4.http_post("/api/1/maintenance", sendData, {
			requestMethod : "POST",
			header : {
				"key" : "${_KEY}"
			},
			success : function(rtv) {
				console.log(rtv);
				$V4.move('/vehicleMng/vehicleMaintenance');
			},
			error : function(t) {
				console.log(t);
			}
		});
    	
    });
    
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>정비현황 조회ㆍ등록</h2>
                    <span>등록차량의 차량 정비 및 소모품 현황을 조회ㆍ등록할 수 있습니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="vehicle-page">
                    <div class="box-layout request-vehicle-repair">
                        <div class="title">
                            <h3 class="tit3">등록차량 정비 요청</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
                            <div class="info-txt">
                                등록차량의 정비 이력을 등록합니다. 차량선택을 먼저 해주세요.
                            </div>

                            <div class="vehicle-info">
                                <div class="img">
                                    <img src="${pageContext.request.contextPath}/common/new/img/vehicle/L47.png" alt="" />
                                </div>
                                <div class="info">
                                    <table class="table mgt20">
                                        <caption>등록차량 정보</caption>
                                        <colgroup>
                                            <col style="width:21.5%" />
                                            <col style="width:28.5%" />
                                            <col style="width:21.5%" />
                                            <col style="width:28.5%" />
                                        </colgroup>
                                        <tbody>
                                            <tr>
                                                <th scope="row">차량선택</th>
                                                <td colspan="3">
                                                    <div class="division">
                                                        <div class="col-9">
                                                            <input type="text" id="plateNum" />
                                                			<input type="hidden" id="vehicleKey" />
                                                        </div>
                                                        <div class="space">&nbsp;</div>
                                                        <div class="col-3">
                                                            <button type="button" class="btn btn01 md" id="vehiclePopBtn">검색</button>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tbody>
                                            <tr>
                                                <th scope="row">차량 소속</th>
                                                <td id="vehicleGroup"></td>
                                                <th scope="row">차량 관리자</th>
                                                <td id="vehicleManager"></td>
                                            </tr>
                                            <!-- <tr>
                                                <th scope="row">통보자1</th>
                                                <td></td>
                                                <th scope="row">통보자2</th>
                                                <td></td>
                                            </tr> -->
                                        </tbody>

                                        <tbody>
                                            <tr>
                                                <th scope="row">제조사/차종</th>
                                                <td colspan="3" id="manufactureAndmodelHeader"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">모델명</th>
                                                <td id="modelMaster"></td>
                                                <th scope="row">재원</th>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">연식</th>
                                                <td id="year">&nbsp;</td>
                                                <th scope="row">소유구분</th>
                                                <td id="vehicleBizType"></td>
                                            </tr>
                                        </tbody>

                                        <tbody>
                                            <tr>
                                                <th scope="row">정기검사일</th>
                                                <td><input type="text" class="datePicker" id="periodicInspection" /></td>
                                                <th scope="row">다음검사일</th>
                                                <td><input type="text" class="datePicker" id="nextPeriodicInspection" /></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <h4 class="tit2 mgt30">요청 정보</h4>
                            <table class="table mgt20">
                                <caption>요청 정보 등록</caption>
                                <tbody>
                                    <tr>
                                        <th scope="row">요청자</th>
                                        <td>
                                            <div class="division">
                                                <div class="col-8">
                                                    <input type="text" readonly="readonly" id="name" />
                                                    <input type="hidden" id="accountKey" />
                                                </div>
                                                <div class="space">&nbsp;</div>
                                                <div class="col-4">
                                                    <button type="button" class="btn btn02 md" id="userPop">검색</button>
                                                </div>
                                            </div>
                                        </td>
                                        <th scope="row">사용부서</th>
                                        <td>
                                            <div class="division">
                                                <div class="col-8">
                                                    <input type="text" readonly="readonly" id="groupNm" />
                                                    <input type="hidden" id="groupKey" />
                                                </div>
                                                <div class="space">&nbsp;</div>
                                                <div class="col-4">
                                                    <button type="button" class="btn btn02 md" id="groupPop">검색</button>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">정비요청일</th>
                                        <td>
                                            <input type="text" class="datePicker" id="maintenanceReqDate" />
                                        </td>
                                        <th scope="row">완료요청일</th>
                                        <td>
                                            <input type="text" class="datePicker" id="completeReqDate" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">고장내역</th>
                                        <td colspan="3">
                                            <textarea rows="5" class="w100" id="troubleDesc"></textarea>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <!-- 하단 버튼 -->
                            <div class="btn-bottom">
                                <button type="button" class="btn btn02" id="maintenanceReqCancelBtn">취소</button>
                                <button type="button" class="btn btn03" id="maintenanceReqBtn" style="display:none;">등록완료</button>
                            </div>
                            <!--/ 하단 버튼 -->
                        </div>
                    </div>
                    <!--/ 콘텐츠 본문 -->
                </div>
            </div>
        </section>
        <!--/ 본문 -->