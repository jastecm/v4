<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	
    $("#register-repair-dialog").dialog({
        autoOpen: false,
        show: {
            duration: 500
        },
        width: '960',
        modal: true
    });

    $("#register-repair-open").on("click", function() {
        $("#register-repair-dialog").dialog("open");
    });

    // 탭메뉴
    $(".tab-menu>li>a").click(function() {
		var tab = $(this).data("tab");
		
		$('.tab-menu li').removeClass("on");
		$('.tab-menu li.'+tab).addClass("on");
    });

    
    $('#vehicleMaintenanceRegBtn').on('click',function(){
    	//여기서 나뉜다. 탭에 따라서
    	$V4.move('/vehicleMng/vehicleMaintenance/reg');
    });
    
    $(document).on('click','.partsDetail',function(){
    	var maintenanceKey = $(this).data('maintenancekey');
    	var vehicleKey = $(this).data('vehiclekey');
    	
    	
    	$V4.move('/vehicleMng/vehicleMaintenance/parts',{"maintenanceKey" : maintenanceKey,"vehicleKey" : vehicleKey});
    });
    
    $(document).on('click','.partsDetailView',function(){
    	var maintenanceKey = $(this).data('maintenancekey');
    	var vehicleKey = $(this).data('vehiclekey');
    	
    	
    	$V4.move('/vehicleMng/vehicleMaintenance/parts',{"maintenanceKey" : maintenanceKey,"vehicleKey" : vehicleKey,"mode":"view"});
    });
    
    $(document).on('click','.approvalDetail',function(){
    	var maintenanceKey = $(this).data('maintenancekey');
    	var vehicleKey = $(this).data('vehiclekey');
    	
    	
    	$V4.move('/vehicleMng/vehicleMaintenance/reg',{"maintenanceKey" : maintenanceKey,"vehicleKey" : vehicleKey});
    });
    
    
    
    
    var thead = [
                 {"상태":"15%"},
                 {"차량정보":"8%"},
                 {"등록일":"9%"},
                 {"요청일":"9"},
                 {"고장내역":"28%"},
                 {"수리금액":"10%"},
                 {"정비업소":"13%"},
                 {"상세":"8%"}
     ];
    <c:if test="${V4.corp.corpType eq '4'}">    
    var thead2 = [
                  {"회사명":"10%"},
                  {"차량정보":"15%"},
                  {"정비일자":"10%"},
                  {"정비항목":"20%"},
                  {"부품금액":"9%"},
                  {"공임금액":"10%"},
                  {"등급":"5%"},
                  {"정비업소":"13%"},
                  {"상세":"8%"}
                  ];
    </c:if>
    <c:if test="${V4.corp.corpType ne '4'}">
    var thead2 = [
                  {"차량정보":"15%"},
                  {"정비일자":"10%"},
                  {"정비항목":"22%"},
                  {"부품금액":"11%"},
                  {"공임금액":"12%"},
                  {"등급":"7%"},
                  {"정비업소":"15%"},
                  {"상세":"8%"}
                  ];
    </c:if>
    var approvalSetting = {
    		thead : thead
    		,bodyHtml : (function(data){
    			var strHtml = "";
    			for(i in data.result){
    				var obj = data.result[i];
    				strHtml += '<tr>';
    				strHtml += '    <td>';
    				
    				var state = getProperty(obj,'approval.state');
					var maxStep = getProperty(obj,'approval.maxStep');
					
					if(state != maxStep && state == "0"){
						strHtml += '        <div class="states-value state07">정비요청</div><br />';
					}else if(state != maxStep && state == "1"){
						strHtml += '        <div class="states-value state06">1차승인</div><br />';	
					}else if(state == maxStep){
						if(obj.repairDate){
							strHtml += '        <div class="states-value state02">정비완료</div><br />';
						}else{
							strHtml += '        <div class="states-value state02">결재완료</div><br />';
						}
							
					}else if(state == "-1"){
						strHtml += '        <div class="states-value state08">반려</div><br />';	
					}
    				
    				/* strHtml += '        <button class="states-value state03">취소</button>'; */
    				strHtml += '    </td>';
    				strHtml += '    <td>'+getProperty(obj,'vehicle.vehicleModel.modelMaster')+'</br>'+getProperty(obj,'vehicle.plateNum')+'</td>';
    				strHtml += '    <td>'+convertDateUint(new Date(obj.maintenanceReqDate),_unitDate,_timezoneOffset,false)+'</td>';
    				strHtml += '    <td>'+convertDateUint(new Date(obj.completeReqDate),_unitDate,_timezoneOffset,false)+'</td>';
    				strHtml += '    <td class="left">'+convertNullString(obj.troubleDesc)+'</td>';
    				if(obj.detail.length != 0){
    					var partsPay = arraySum(obj.detail,"partsPay");
    					var wagePay = arraySum(obj.detail,"wagePay");
    					strHtml += '    <td>'+(partsPay + wagePay)+'원</td>';
    				}else{
    					strHtml += '    <td>-</td>';	
    				}
    				
    				strHtml += '    <td>'+convertNullString(obj.garage)+'</td>';
    				strHtml += '    <td><button type="button" class="btn-img repair approvalDetail" data-maintenancekey='+obj.maintenanceKey+' data-vehiclekey='+obj.vehicle.vehicleKey+'>상세보기</button></td>';
    				strHtml += '</tr>';
    			}
    			return strHtml;
    		})
    		,limit : parseInt($(".list01 .func_limitChange").val())
    		,pagePerGroup : 10 //pagingGroup size
    		,url : "/api/1/maintenance"
    		,param : {
    					'searchOrder' : $(".list01 .func_order").val()
    				 }
    		,http_post_option : {
    			requestMethod : "GET"
    			,header : {key : "${_KEY}"}
    		}
    		,initSearch : true //생성과 동시에 search		
    		,loadingBodyViewer : true //로딩중!! 표시됨
    		,debugMode : true
    	}
    var carSetting = {
    		thead : thead2
    		,bodyHtml : (function(data){
    			var strHtml = "";
    			for(i in data.result){
    				var obj = data.result[i];
    				strHtml += '<tr>';
    				<c:if test="${V4.corp.corpType eq '4'}">    
	   				strHtml += '	<td>'+obj.corp.corpName+'</td>';
    				</c:if>
    				strHtml += '    <td>';
    				strHtml += '        <div class="car-img">';
    				strHtml += '            <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" alt="" /></span>';
    				strHtml += '            <span class="num">'+getProperty(obj,'vehicle.vehicleModel.modelMaster')+' '+getProperty(obj,'vehicle.plateNum')+'</span>';
    				strHtml += '        </div>';
    				strHtml += '    </td>';
    				strHtml += '    <td>'+convertDateUint(new Date(obj.maintenanceReqDate),_unitDate,_timezoneOffset,false)+'</td>';
    				if(obj.detail.length != 0){
    					if(obj.detail.length == 1){
    						strHtml += '    <td class="left">'+obj.detail[0].partsNm+'</td>';
    						strHtml += '    <td>'+convertNullString(getProperty(obj.detail[0],'partsPay'),0)+'</td>';//부품금액
        					strHtml += '    <td>'+convertNullString(getProperty(obj.detail[0],'wagePay'),0)+'</td>';//공임금액
        					var quality = convertNullString(getProperty(obj.detail[0],'partsQuality'),0)
        					if(quality == "0"){
        						strHtml += '    <td>A</td>';
        					}else if(quality == "1"){
        						strHtml += '    <td>B</td>';
        					}else if(quality == "2"){
        						strHtml += '    <td>C</td>';
        					}else {
        						strHtml += '    <td></td>';
        					}
    					}else{
    						strHtml += '    <td class="left">'+obj.detail[0].partsNm+' 외 '+(obj.detail.length - 1)+'건</td>';
    						strHtml += '    <td>'+arraySum(obj.detail,"partsPay")+' 원</td>';//부품금액
        					strHtml += '    <td>'+arraySum(obj.detail,"wagePay")+' 원</td>';//공임금액
            				strHtml += '    <td>'+avgGrade(obj.detail)+'</td>';//등급?인데 어케 A가 나오냐
    					}
    				}else{
    					strHtml += '    <td class="left"></td>';
    					strHtml += '    <td></td>';
    					strHtml += '    <td></td>';
        				strHtml += '    <td></td>';
    				}
    				
    				strHtml += '    <td>'+convertNullString(obj.garage)+'</td>';
    				if(obj.repairDate){
    					strHtml += '    <td><button type="button" class="btn-img repair partsDetailView" data-maintenancekey='+obj.maintenanceKey+' data-vehiclekey='+obj.vehicle.vehicleKey+'>상세보기</button></td>';
    				}else{
    					strHtml += '    <td><button type="button" class="btn btn02 sm round partsDetail" data-maintenancekey='+obj.maintenanceKey+' data-vehiclekey='+obj.vehicle.vehicleKey+'>정비등록</button></td>';	
    				}
    				
    				
    				strHtml += '</tr>';
    			}
    			return strHtml;
    		})
    		,limit : parseInt($(".list02 .func_limitChange").val())
    		,pagePerGroup : 10 //pagingGroup size
    		,url : "/api/1/maintenance"
    		,param : {
    					'searchOrder' : $(".list02 .func_order").val(),
    				    'vehicleType' : '1'
    				 }
    		,http_post_option : {
    			requestMethod : "GET"
    			,header : {key : "${_KEY}"}
    		}
    		,initSearch : true //생성과 동시에 search		
    		,loadingBodyViewer : true //로딩중!! 표시됨
    		,debugMode : true
    	}
    
    var vanSetting = {
    		thead : thead2
    		,bodyHtml : (function(data){
    			var strHtml = "";
    			for(i in data.result){
    				var obj = data.result[i];
    				strHtml += '<tr>';
    				<c:if test="${V4.corp.corpType eq '4'}">    
	   				strHtml += '	<td>'+obj.corp.corpName+'</td>';
    				</c:if>
    				strHtml += '    <td>';
    				strHtml += '        <div class="car-img">';
    				strHtml += '            <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" alt="" /></span>';
    				strHtml += '            <span class="num">'+getProperty(obj,'vehicle.vehicleModel.modelMaster')+' '+getProperty(obj,'vehicle.plateNum')+'</span>';
    				strHtml += '        </div>';
    				strHtml += '    </td>';
    				strHtml += '    <td>'+convertDateUint(new Date(obj.maintenanceReqDate),_unitDate,_timezoneOffset,false)+'</td>';
    				if(obj.detail.length != 0){
    					if(obj.detail.length == 1){
    						strHtml += '    <td class="left">'+obj.detail[0].partsNm+'</td>';
    						strHtml += '    <td>'+convertNullString(getProperty(obj.detail[0],'partsPay'),0)+'</td>';//부품금액
        					strHtml += '    <td>'+convertNullString(getProperty(obj.detail[0],'wagePay'),0)+'</td>';//공임금액
        					var quality = convertNullString(getProperty(obj.detail[0],'partsQuality'),0)
        					if(quality == "0"){
        						strHtml += '    <td>A</td>';
        					}else if(quality == "1"){
        						strHtml += '    <td>B</td>';
        					}else if(quality == "2"){
        						strHtml += '    <td>C</td>';
        					}else {
        						strHtml += '    <td></td>';
        					}
    					}else{
    						strHtml += '    <td class="left">'+obj.detail[0].partsNm+' 외 '+(obj.detail.length - 1)+'건</td>';
    						strHtml += '    <td>'+arraySum(obj.detail,"partsPay")+' 원</td>';//부품금액
        					strHtml += '    <td>'+arraySum(obj.detail,"wagePay")+' 원</td>';//공임금액
            				strHtml += '    <td>'+avgGrade(obj.detail)+'</td>';//등급?인데 어케 A가 나오냐
    					}
    				}else{
    					strHtml += '    <td class="left"></td>';
    					strHtml += '    <td></td>';
    					strHtml += '    <td></td>';
        				strHtml += '    <td></td>';
    				}
    				
    				strHtml += '    <td>'+convertNullString(obj.garage)+'</td>';
    				if(obj.repairDate){
    					strHtml += '    <td><button type="button" class="btn-img repair partsDetailView" data-maintenancekey='+obj.maintenanceKey+' data-vehiclekey='+obj.vehicle.vehicleKey+'>상세보기</button></td>';
    				}else{
    					strHtml += '    <td><button type="button" class="btn btn02 sm round partsDetail" data-maintenancekey='+obj.maintenanceKey+' data-vehiclekey='+obj.vehicle.vehicleKey+'>정비등록</button></td>';	
    				}
    				
    				
    				strHtml += '</tr>';
    			}
    			return strHtml;
    		})
    		,limit : parseInt($(".list03 .func_limitChange").val())
    		,pagePerGroup : 10 //pagingGroup size
    		,url : "/api/1/maintenance"
    		,param : {
    					'searchOrder' : $(".list03 .func_order").val(),
    				    'vehicleType' : '2'
    				 }
    		,http_post_option : {
    			requestMethod : "GET"
    			,header : {key : "${_KEY}"}
    		}
    		,initSearch : true //생성과 동시에 search		
    		,loadingBodyViewer : true //로딩중!! 표시됨
    		,debugMode : true
    	}
    
    var truckSetting = {
    		thead : thead2
    		,bodyHtml : (function(data){
    			var strHtml = "";
    			for(i in data.result){
    				var obj = data.result[i];
    				strHtml += '<tr>';
    				<c:if test="${V4.corp.corpType eq '4'}">    
	   				strHtml += '	<td>'+obj.corp.corpName+'</td>';
    				</c:if>
    				strHtml += '    <td>';
    				strHtml += '        <div class="car-img">';
    				strHtml += '            <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" alt="" /></span>';
    				strHtml += '            <span class="num">'+getProperty(obj,'vehicle.vehicleModel.modelMaster')+' '+getProperty(obj,'vehicle.plateNum')+'</span>';
    				strHtml += '        </div>';
    				strHtml += '    </td>';
    				strHtml += '    <td>'+convertDateUint(new Date(obj.maintenanceReqDate),_unitDate,_timezoneOffset,false)+'</td>';
    				if(obj.detail.length != 0){
    					if(obj.detail.length == 1){
    						strHtml += '    <td class="left">'+obj.detail[0].partsNm+'</td>';
    						strHtml += '    <td>'+convertNullString(getProperty(obj.detail[0],'partsPay'),0)+'</td>';//부품금액
        					strHtml += '    <td>'+convertNullString(getProperty(obj.detail[0],'wagePay'),0)+'</td>';//공임금액
        					var quality = convertNullString(getProperty(obj.detail[0],'partsQuality'),0)
        					if(quality == "0"){
        						strHtml += '    <td>A</td>';
        					}else if(quality == "1"){
        						strHtml += '    <td>B</td>';
        					}else if(quality == "2"){
        						strHtml += '    <td>C</td>';
        					}else {
        						strHtml += '    <td></td>';
        					}
    					}else{
    						strHtml += '    <td class="left">'+obj.detail[0].partsNm+' 외 '+(obj.detail.length - 1)+'건</td>';
    						strHtml += '    <td>'+arraySum(obj.detail,"partsPay")+' 원</td>';//부품금액
        					strHtml += '    <td>'+arraySum(obj.detail,"wagePay")+' 원</td>';//공임금액
            				strHtml += '    <td>'+avgGrade(obj.detail)+'</td>';//등급?인데 어케 A가 나오냐
    					}
    				}else{
    					strHtml += '    <td class="left"></td>';
    					strHtml += '    <td></td>';
    					strHtml += '    <td></td>';
        				strHtml += '    <td></td>';
    				}
    				
    				strHtml += '    <td>'+convertNullString(obj.garage)+'</td>';
    				if(obj.repairDate){
    					strHtml += '    <td><button type="button" class="btn-img repair partsDetailView" data-maintenancekey='+obj.maintenanceKey+' data-vehiclekey='+obj.vehicle.vehicleKey+'>상세보기</button></td>';
    				}else{
    					strHtml += '    <td><button type="button" class="btn btn02 sm round partsDetail" data-maintenancekey='+obj.maintenanceKey+' data-vehiclekey='+obj.vehicle.vehicleKey+'>정비등록</button></td>';	
    				}
    				
    				
    				strHtml += '</tr>';
    			}
    			return strHtml;
    		})
    		,limit : parseInt($(".list04 .func_limitChange").val())
    		,pagePerGroup : 10 //pagingGroup size
    		,url : "/api/1/maintenance"
    		,param : {
    					'searchOrder' : $(".list04 .func_order").val(),
    				    'vehicleType' : '3'
    				 }
    		,http_post_option : {
    			requestMethod : "GET"
    			,header : {key : "${_KEY}"}
    		}
    		,initSearch : true //생성과 동시에 search		
    		,loadingBodyViewer : true //로딩중!! 표시됨
    		,debugMode : true
    	}
    
    var etcSetting = {
    		thead : thead2
    		,bodyHtml : (function(data){
    			var strHtml = "";
    			for(i in data.result){
    				var obj = data.result[i];
    				strHtml += '<tr>';
    				<c:if test="${V4.corp.corpType eq '4'}">    
	   				strHtml += '	<td>'+obj.corp.corpName+'</td>';
    				</c:if>
    				strHtml += '    <td>';
    				strHtml += '        <div class="car-img">';
    				strHtml += '            <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" alt="" /></span>';
    				strHtml += '            <span class="num">'+getProperty(obj,'vehicle.vehicleModel.modelMaster')+' '+getProperty(obj,'vehicle.plateNum')+'</span>';
    				strHtml += '        </div>';
    				strHtml += '    </td>';
    				strHtml += '    <td>'+convertDateUint(new Date(obj.maintenanceReqDate),_unitDate,_timezoneOffset,false)+'</td>';
    				if(obj.detail.length != 0){
    					if(obj.detail.length == 1){
    						strHtml += '    <td class="left">'+obj.detail[0].partsNm+'</td>';
    						strHtml += '    <td>'+convertNullString(getProperty(obj.detail[0],'partsPay'),0)+'</td>';//부품금액
        					strHtml += '    <td>'+convertNullString(getProperty(obj.detail[0],'wagePay'),0)+'</td>';//공임금액
        					var quality = convertNullString(getProperty(obj.detail[0],'partsQuality'),0)
        					if(quality == "0"){
        						strHtml += '    <td>A</td>';
        					}else if(quality == "1"){
        						strHtml += '    <td>B</td>';
        					}else if(quality == "2"){
        						strHtml += '    <td>C</td>';
        					}else {
        						strHtml += '    <td></td>';
        					}
    					}else{
    						strHtml += '    <td class="left">'+obj.detail[0].partsNm+' 외 '+(obj.detail.length - 1)+'건</td>';
    						strHtml += '    <td>'+arraySum(obj.detail,"partsPay")+' 원</td>';//부품금액
        					strHtml += '    <td>'+arraySum(obj.detail,"wagePay")+' 원</td>';//공임금액
            				strHtml += '    <td>'+avgGrade(obj.detail)+'</td>';//등급?인데 어케 A가 나오냐
    					}
    				}else{
    					strHtml += '    <td class="left"></td>';
    					strHtml += '    <td></td>';
    					strHtml += '    <td></td>';
        				strHtml += '    <td></td>';
    				}
    				
    				strHtml += '    <td>'+convertNullString(obj.garage)+'</td>';
    				if(obj.repairDate){
    					strHtml += '    <td><button type="button" class="btn-img repair partsDetailView" data-maintenancekey='+obj.maintenanceKey+' data-vehiclekey='+obj.vehicle.vehicleKey+'>상세보기</button></td>';
    				}else{
    					strHtml += '    <td><button type="button" class="btn btn02 sm round partsDetail" data-maintenancekey='+obj.maintenanceKey+' data-vehiclekey='+obj.vehicle.vehicleKey+'>정비등록</button></td>';	
    				}
    				
    				
    				strHtml += '</tr>';
    			}
    			return strHtml;
    		})
    		,limit : parseInt($(".list05 .func_limitChange").val())
    		,pagePerGroup : 10 //pagingGroup size
    		,url : "/api/1/maintenance"
    		,param : {
    					'searchOrder' : $(".list05 .func_order").val(),
    				    'vehicleType' : '4'
    				 }
    		,http_post_option : {
    			requestMethod : "GET"
    			,header : {key : "${_KEY}"}
    		}
    		,initSearch : true //생성과 동시에 search		
    		,loadingBodyViewer : true //로딩중!! 표시됨
    		,debugMode : true
    	}
    
    $('#approvalList').commonList(approvalSetting);
    $('#carList').commonList2(carSetting);
    $('#vanList').commonList3(vanSetting);
    $('#truckList').commonList4(truckSetting);
    $('#etcList').commonList5(etcSetting);
    
	$("#top_btn_search").on("click",$.debounce(200,1,function(){
		
		var param = {searchType:$("#top_searchType").val() , searchText : $("#top_searchText").val()}
		
		var list = $('.page-tab .on').children('a').data('tab');
		
		if(list){
			var limit = parseInt($("."+list+" .func_limitChange").val());	 
			param.searchOrder = $("."+list+" .func_order").val();
			
			$("."+list+" .searchStart").val()&&(param.searchStartDate = $("."+list+" .searchStart").val());
			$("."+list+" .searchEnd").val()&&(param.searchEndDate = $("."+list+" .searchEnd").val());
			
			if(list == "list01"){
				//$('#vehicleList').commonList("setParam",param);
				//$('#vehicleList').commonList("setLimit",limit).search();
			}else if(list =="list02"){
				param.vehicleType = "1";
				$('#carList').commonList2("setParam",param);
				$('#carList').commonList2("setLimit",limit).search();
			}else if(list == "list03"){
				param.vehicleType = "2";
				$('#vanList').commonList3("setParam",param);
				$('#vanList').commonList3("setLimit",limit).search();
			}else if(list == "list04"){
				param.vehicleType = "3";
				$('#truckList').commonList4("setParam",param);
				$('#truckList').commonList4("setLimit",limit).search();
			}else if(list == "list05"){
				param.vehicleType = "4";
				$('#etcList').commonList5("setParam",param);
				$('#etcList').commonList5("setLimit",limit).search();
			}
		}
		
	}));
	
	$(".func_order").on("change",function(){
		$("#top_btn_search").trigger("click");
	});
	
	$(".func_limitChange").on("change",function(){
		$("#top_btn_search").trigger("click");
	});
	
	$(".searchStart,.searchEnd").on("change",function(){
		$("#top_btn_search").trigger("click");
	});
	
	$("div.date button").on("click",function(){
		var list = $('.page-tab .on').children('a').data('tab');
		$(this).closest("div").find("button").removeClass("active");
		$(this).addClass("active");
		var d = $(this).data("day");
		
		if(d == ""){
			$("."+list+" .datePicker").val("");
			$("."+list+" .searchStart,."+list+" .searchEnd").val("");
			
			$("#top_btn_search").trigger("click");
		}else{
			
			var rtvStartDate = calcDate(new Date() , 'd' , 0 , new Date());
			var rtvEndDate = calcDate(new Date() , 'd' , 0 , new Date());
			
			if(d == "-0D"){
			}else if(d == "-7D") rtvStartDate = calcDate(new Date() , 'd' , -7 , new Date());
			else if(d == "-1M") rtvStartDate = calcDate(new Date() , 'm' , -1 , new Date());
			else if(d == "-3M") rtvStartDate = calcDate(new Date() , 'm' , -3 , new Date());
			else if(d == "-6M") rtvStartDate = calcDate(new Date() , 'm' , -6 , new Date());
			
			$("."+list+" .datePicker").eq(0).datepicker('setDate', rtvStartDate);
			$("."+list+" .datePicker").eq(1).datepicker('setDate', rtvEndDate);
			
			var v = $("."+list+" .datePicker").eq(0).datepicker( "getDate" ).getTime();
    		var t = $("."+list+" .datePicker").eq(0).data("target");
    		$(t).val(v);
    		
    		var v = $("."+list+" .datePicker").eq(1).datepicker( "getDate" ).getTime();
    		var t = $("."+list+" .datePicker").eq(1).data("target");
    		$(t).val(v);
    		
    		$(t).trigger("change");
		}
		
	});
	
	$("#top_searchText").on("keyup",function(e){
		if(e.keyCode == 13) {
			$("#top_btn_search").trigger("click");
		}
	});
});
</script>
 <!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>정비현황 조회ㆍ등록</h2>
                    <span>등록차량의 차량 정비 및 소모품 현황을 조회ㆍ등록할 수 있습니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="vehicle-page">
                    <div class="">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                            <div class="item">
                                <span>
									정비수
									<strong>10</strong>건
								</span>
                                <span>
									정비건수
									<strong>8</strong>건
								</span>
                            </div>

                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <!-- 차량정비등록 -->
                        <h3 class="tit2 mgt30">차량정비 등록</h3>
                        <div class="box-register">
                            차량정비 및 소모품 교체이력을 등록합니다.<br /> 뷰카 가입 정비업소 교체이력은 시스템 연동으로 자동으로 관리됩니다.

                            <ul class="type-01">
                                <li>차량정비 등록 결재를 관리할 수 있습니다.</li>
                                <li>1차 승인 : 차량담당자 (상신자 소속 부서장)</li>
                                <li>2차 승인 : 차량관리자 (인사, 총무소속 부서장)</li>
                            </ul>
                            <br /> 정비결재가 필요없는 승용, 승합 정비내역은 아래 승용, 승합 탭 메뉴에서 차량 검색후 ＂정비내역 입력”메뉴를 통해 등록하세요.

                            <div class="right">
                                <button type="button" class="btn btn01" id="vehicleMaintenanceRegBtn">정비요청</button>
                            </div>
                        </div>
                        <!--/ 차량정비등록 -->
                        
                         <!-- 상단 검색 -->
                        <div class="top-search">
                            <select id="top_searchType">
								<option value="">전체</option>
								<option value="plateNum">차량번호</option>
							</select>
                            <input type="text" id="top_searchText"/>
                            <button type="button" class="btn btn03" id="top_btn_search">조회</button>
                        </div>
                        <!--/ 상단 검색 -->

                        <ul class="page-tab tab-menu col-5">
                            <!-- 정비 결재 -->
                            <li class="list01 on">
                                <a href="javascript:void(0);" data-tab="list01">정비 결재</a>
                                <div class="tab-list">
                                    <!-- table 버튼 -->
                                    <div class="btn-function row-2">
                                        <div class="left">
                                            <select class="func_order">
                                            	<option value="lastestLocationTime">최근 운행순</option>
												<option value="regDate">최근 등록순</option>
											</select>

                                            <div class="date">
                                                <button type="button" class="active" data-day="">전체</button>
                                                <button type="button" data-day="-0D">오늘</button>
                                                <button type="button" data-day="-7D">1주일</button>
                                                <button type="button" data-day="-1M">1개월</button>
                                                <button type="button" data-day="-3M">3개월</button>
                                                <button type="button" data-day="-6M">6개월</button>
                                                <div class="direct">
                                                    <input type="text" class="datePicker" data-target="#list01_searchStart"/> &nbsp;~&nbsp;
                                                    <input type="hidden" id="list01_searchStart" class="searchStart"/>
                                                    <input type="text" class="datePicker" data-target="#list01_searchEnd"/>
                                                    <input type="hidden" id="list01_searchEnd" class="searchEnd"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="right">
                                            <button class="excel">엑셀 다운로드</button>
                                            <select class="func_limitChange">
												<option value="5" selected>5건씩 보기</option>
												<option value="10">10건씩 보기</option>
												<option value="20">20건씩 보기</option>
												<option value="50">20건씩 보기</option>
											</select>
                                        </div>
                                    </div>
                                    <!--/ table 버튼 -->

                                    <table class="table list mgt20" id="approvalList">
                                        <%-- <caption>정비현황 조회ㆍ등록 리스트</caption>
                                        <colgroup>
                                            <col style="width:15%" />
                                            <col style="width:8%" />
                                            <col style="width:9%" />
                                            <col style="width:9%" />
                                            <col />
                                            <col style="width:10%" />
                                            <col style="width:13%" />
                                            <col style="width:8%" />
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th scope="col">상태</th>
                                                <th scope="col">차량정보</th>
                                                <th scope="col">등록일</th>
                                                <th scope="col">요청일</th>
                                                <th scope="col">고장내역</th>
                                                <th scope="col">수리금액</th>
                                                <th scope="col">정비업소</th>
                                                <th scope="col">상세</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="states-value state07">정비요청</div><br />
                                                    <button class="states-value state03">취소</button>
                                                </td>
                                                <td>카니발<br />60무 5042</td>
                                                <td>2018.05.01</td>
                                                <td>2018.05.01</td>
                                                <td class="left">헤드라이트 및 안계등, 브레이크 등 고장교체</td>
                                                <td>100,000원</td>
                                                <td>바름정비<br />강북점</td>
                                                <td><button class="btn-img repair">상세보기</button></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="states-value state02">정비완료</div><br /> 2018/05/09 15:18<br /> 이준혁
                                                </td>
                                                <td>카니발<br />60무 5042</td>
                                                <td>2018.05.01</td>
                                                <td>2018.05.01</td>
                                                <td class="left">헤드라이트 및 안계등, 브레이크 등 고장교체</td>
                                                <td>100,000원</td>
                                                <td>바름정비<br />강북점</td>
                                                <td><button class="btn-img repair">상세보기</button></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="states-value state08">반려</div><br /> 2018/05/09 15:18<br /> 이준혁
                                                </td>
                                                <td>카니발<br />60무 5042</td>
                                                <td>2018.05.01</td>
                                                <td>2018.05.01</td>
                                                <td class="left">헤드라이트 및 안계등, 브레이크 등 고장교체</td>
                                                <td>100,000원</td>
                                                <td>바름정비<br />강북점</td>
                                                <td><button class="btn-img repair">상세보기</button></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="states-value state04">2차승인</div><br /> 2018/05/09 15:18<br /> 이준혁
                                                </td>
                                                <td>카니발<br />60무 5042</td>
                                                <td>2018.05.01</td>
                                                <td>2018.05.01</td>
                                                <td class="left">헤드라이트 및 안계등, 브레이크 등 고장교체</td>
                                                <td>100,000원</td>
                                                <td>바름정비<br />강북점</td>
                                                <td><button class="btn-img repair">상세보기</button></td>
                                            </tr>
                                        </tbody> --%>
                                    </table>

                                    <!-- 안내 -->
                                    <div class="box-info">
                                        <h3 class="hidden">안내</h3>
                                        <h4 class="mgt0">정비 결재 표기 안내</h4>
                                        <ul>
                                            <li><strong>정비요청</strong>사용자가 정비요청을 등록한 상태로 상신자 소속부서장에게 통보되었습니다.</li>
                                            <li><strong>1차 승인</strong>사용자 소속부서장의 승인이 완료되었습니다.</li>
                                            <li><strong>2차 승인</strong>차량관리자(인사, 총무부서)의 승인이 완료되었습니다. 정비결재 우측 차량별 차량 정비내역을 입력하세요. </li>
                                            <li><strong>반려</strong>정비요청이 반려된 상태입니다.</li>
                                            <li><strong>취소</strong>사용자(신청자)가 정비요청을 취소한 상태입니다. 1차 승인 전까지 사용자는 취소할 수 있습니다.</li>
                                        </ul>
                                    </div>
                                    <!--/ 안내 -->
                                </div>
                            </li>
                            <!--/ 정비 결재 -->

                            <!-- 승용차 -->
                            <li class="list02">
                                <a href="javascript:void(0);" data-tab="list02">승용차</a>
                                <div class="tab-list">
                                    <!-- table 버튼 -->
                                    <div class="btn-function row-2">
                                        <div class="left">
                                            <select class="func_order">
                                            	<option value="lastestLocationTime">최근 운행순</option>
												<option value="regDate">최근 등록순</option>
											</select>

                                            <div class="date">
                                                <button type="button" class="active" data-day="">전체</button>
                                                <button type="button" data-day="-0D">오늘</button>
                                                <button type="button" data-day="-7D">1주일</button>
                                                <button type="button" data-day="-1M">1개월</button>
                                                <button type="button" data-day="-3M">3개월</button>
                                                <button type="button" data-day="-6M">6개월</button>
                                                <div class="direct">
                                                    <input type="text" class="datePicker" data-target="#list02_searchStart"/> &nbsp;~&nbsp;
                                                    <input type="hidden" id="list02_searchStart" class="searchStart"/>
                                                    <input type="text" class="datePicker" data-target="#list02_searchEnd"/>
                                                    <input type="hidden" id="list02_searchEnd" class="searchEnd"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="right">
                                            <button class="excel">엑셀 다운로드</button>
                                            <select class="func_limitChange">
												<option value="5" selected>5건씩 보기</option>
												<option value="10">10건씩 보기</option>
												<option value="20">20건씩 보기</option>
												<option value="50">20건씩 보기</option>
											</select>
                                        </div>
                                    </div>
                                    <!--/ table 버튼 -->

                                    <table class="table list mgt20" id="carList">
                                            <!-- <tr>
                                                <td>리스트2</td>
                                                <td>
                                                    <div class="car-img">
                                                        <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" alt="" /></span>
                                                        <span class="num">쏘나타 63호 5703</span>
                                                    </div>
                                                </td>
                                                <td>2018.05.01 01:02</td>
                                                <td class="left">1212 외 1건</td>
                                                <td>2,000원</td>
                                                <td>2,200원</td>
                                                <td>A</td>
                                                <td>111</td>
                                                <td><button class="btn-img repair">상세보기</button></td>
                                            </tr>
                                            <tr>
                                                <td>㈜자스텍엠</td>
                                                <td>
                                                    <div class="car-img">
                                                        <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" alt="" /></span>
                                                        <span class="num">쏘나타 63호 5703</span>
                                                    </div>
                                                </td>
                                                <td>2018.05.01 01:02</td>
                                                <td class="left">1212 외 1건</td>
                                                <td>2,000원</td>
                                                <td>2,200원</td>
                                                <td>A</td>
                                                <td>111</td>
                                                <td><button class="btn btn02 sm round">정비등록</button></td>
                                            </tr> -->
                                    </table>

                                    <div class="btn-table-bottom">
                                        <div class="right">
                                            <button class="btn btn01" id="register-repair-open">정비내역 대량등록</button>
                                        </div>
                                    </div>

                                    <!-- 안내 -->
                                    <div class="box-info">
                                        <h3 class="hidden">안내</h3>
                                        <h4 class="mgt0">정비등록 안내사항</h4>
                                        <ul>
                                            <li>결재 승인된 차량의 정비내역을 등록해주세요.</li>
                                            <li>등록된 정비내역은 다시 수정하실 수 없습니다.</li>
                                            <li><button class="btn-img repair">상세보기</button> 을 클릭하시면 상세한 정비내역을 보실 수 있습니다. 등록된 정비내역은 등록차량 상세페이지에서 이력관리됩니다.</li>
                                        </ul>
                                    </div>
                                    <!--/ 안내 -->
                                </div>
                            </li>
                            <!--/ 승용차 -->

                            <!-- 승합 -->
                            <li class="list03">
                                <a href="javascript:void(0);" data-tab="list03">승합(12인승 이상)</a>
                                <div class="tab-list">
                                    <!-- table 버튼 -->
                                    <div class="btn-function row-2">
                                        <div class="left">
                                            <select class="func_order">
                                            	<option value="lastestLocationTime">최근 운행순</option>
												<option value="regDate">최근 등록순</option>
											</select>

                                            <div class="date">
                                                <button type="button" class="active" data-day="">전체</button>
                                                <button type="button" data-day="-0D">오늘</button>
                                                <button type="button" data-day="-7D">1주일</button>
                                                <button type="button" data-day="-1M">1개월</button>
                                                <button type="button" data-day="-3M">3개월</button>
                                                <button type="button" data-day="-6M">6개월</button>
                                                <div class="direct">
                                                    <input type="text" class="datePicker" data-target="#list03_searchStart"/> &nbsp;~&nbsp;
                                                    <input type="hidden" id="list03_searchStart" class="searchStart"/>
                                                    <input type="text" class="datePicker" data-target="#list03_searchEnd"/>
                                                    <input type="hidden" id="list03_searchEnd" class="searchEnd"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="right">
                                            <button class="excel">엑셀 다운로드</button>
                                            <select class="func_limitChange">
												<option value="5" selected>5건씩 보기</option>
												<option value="10">10건씩 보기</option>
												<option value="20">20건씩 보기</option>
												<option value="50">20건씩 보기</option>
											</select>
                                        </div>
                                    </div>
                                    <!--/ table 버튼 -->

                                    <table class="table list mgt20" id="vanList">
                                       
                                    </table>

                                    <div class="btn-table-bottom">
                                        <div class="right">
                                            <button class="btn btn01">정비내역 대량등록</button>
                                        </div>
                                    </div>

                                    <!-- 안내 -->
                                    <div class="box-info">
                                        <h3 class="hidden">안내</h3>
                                        <h4 class="mgt0">정비등록 안내사항</h4>
                                        <ul>
                                            <li>결재 승인된 차량의 정비내역을 등록해주세요.</li>
                                            <li>등록된 정비내역은 다시 수정하실 수 없습니다.</li>
                                            <li><button class="btn-img repair">상세보기</button> 을 클릭하시면 상세한 정비내역을 보실 수 있습니다. 등록된 정비내역은 등록차량 상세페이지에서 이력관리됩니다.</li>
                                        </ul>
                                    </div>
                                    <!--/ 안내 -->
                                </div>
                            </li>
                            <!--/ 승합 -->

                            <!-- 트럭 -->
                            <li class="list04">
                                <a href="javascript:void(0)" data-tab="list04">트럭</a>
                                <div class="tab-list">
                                    <!-- table 버튼 -->
                                    <div class="btn-function row-2">
                                        <div class="left">
                                            <select class="func_order">
                                            	<option value="lastestLocationTime">최근 운행순</option>
												<option value="regDate">최근 등록순</option>
											</select>

                                            <div class="date">
                                                <button type="button" class="active" data-day="">전체</button>
                                                <button type="button" data-day="-0D">오늘</button>
                                                <button type="button" data-day="-7D">1주일</button>
                                                <button type="button" data-day="-1M">1개월</button>
                                                <button type="button" data-day="-3M">3개월</button>
                                                <button type="button" data-day="-6M">6개월</button>
                                                <div class="direct">
                                                    <input type="text" class="datePicker" data-target="#list04_searchStart"/> &nbsp;~&nbsp;
                                                    <input type="hidden" id="list04_searchStart" class="searchStart"/>
                                                    <input type="text" class="datePicker" data-target="#list04_searchEnd"/>
                                                    <input type="hidden" id="list04_searchEnd" class="searchEnd"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="right">
                                            <button class="excel">엑셀 다운로드</button>
                                            <select class="func_limitChange">
												<option value="5" selected>5건씩 보기</option>
												<option value="10">10건씩 보기</option>
												<option value="20">20건씩 보기</option>
												<option value="50">20건씩 보기</option>
											</select>
                                        </div>
                                    </div>
                                    <!--/ table 버튼 -->

                                    <table class="table list mgt20" id="truckList">
                                        
                                    </table>

                                    <div class="btn-table-bottom">
                                        <div class="right">
                                            <button class="btn btn01">정비내역 대량등록</button>
                                        </div>
                                    </div>
                                    
                                    <!-- 안내 -->
                                    <div class="box-info">
                                        <h3 class="hidden">안내</h3>
                                        <h4 class="mgt0">정비등록 안내사항</h4>
                                        <ul>
                                            <li>결재 승인된 차량의 정비내역을 등록해주세요.</li>
                                            <li>등록된 정비내역은 다시 수정하실 수 없습니다.</li>
                                            <li><button class="btn-img repair">상세보기</button> 을 클릭하시면 상세한 정비내역을 보실 수 있습니다. 등록된 정비내역은 등록차량 상세페이지에서 이력관리됩니다.</li>
                                        </ul>
                                    </div>
                                    <!--/ 안내 -->
                                </div>
                            </li>
                            <!--/ 트럭 -->

                            <!-- 특수차량 -->
                            <li class="list05">
                                <a href="javascript:void(0);" data-tab="list05">특수차량</a>
                                <div class="tab-list">
                                    <!-- table 버튼 -->
                                    <div class="btn-function row-2">
                                        <div class="left">
                                            <select class="func_order">
                                            	<option value="lastestLocationTime">최근 운행순</option>
												<option value="regDate">최근 등록순</option>
											</select>

                                            <div class="date">
                                                <button type="button" class="active" data-day="">전체</button>
                                                <button type="button" data-day="-0D">오늘</button>
                                                <button type="button" data-day="-7D">1주일</button>
                                                <button type="button" data-day="-1M">1개월</button>
                                                <button type="button" data-day="-3M">3개월</button>
                                                <button type="button" data-day="-6M">6개월</button>
                                                <div class="direct">
                                                    <input type="text" class="datePicker" data-target="#list05_searchStart"/> &nbsp;~&nbsp;
                                                    <input type="hidden" id="list05_searchStart" class="searchStart"/>
                                                    <input type="text" class="datePicker" data-target="#list05_searchEnd"/>
                                                    <input type="hidden" id="list05_searchEnd" class="searchEnd"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="right">
                                            <button class="excel">엑셀 다운로드</button>
                                            <select class="func_limitChange">
												<option value="5" selected>5건씩 보기</option>
												<option value="10">10건씩 보기</option>
												<option value="20">20건씩 보기</option>
												<option value="50">20건씩 보기</option>
											</select>
                                        </div>
                                    </div>
                                    <!--/ table 버튼 -->

                                    <table class="table list mgt20" id="etcList">
                                        
                                    </table>

                                    <div class="btn-table-bottom">
                                        <div class="right">
                                            <button class="btn btn01">정비내역 대량등록</button>
                                        </div>
                                    </div>

                                    <!-- 안내 -->
                                    <div class="box-info">
                                        <h3 class="hidden">안내</h3>
                                        <h4 class="mgt0">정비등록 안내사항</h4>
                                        <ul>
                                            <li>결재 승인된 차량의 정비내역을 등록해주세요.</li>
                                            <li>등록된 정비내역은 다시 수정하실 수 없습니다.</li>
                                            <li>표 우측에 있는 상세버튼을 클릭하시면 상세한 정비내역을 보실 수 있습니다.</li>
                                            <li>등록된 정비내역은 등록차량 상세정보에서 이력관리됩니다.</li>
                                        </ul>
                                    </div>
                                    <!--/ 안내 -->
                                </div>
                            </li>
                            <!--/ 특수차량 -->
                        </ul>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->
        <!-- 정비내역 대량등록 팝업 -->
    <div id="register-repair-dialog" title="정비내역 대량등록">
        <h3 class="tit">정비내역 대량등록</h3>
        <table class="table mgt20">
            <caption>정비내역 대량등록</caption>
            <tbody>
                <tr>
                    <th scope="row">대랑등록양식</th>
                    <td>
                        <div class="btn-function mgt0"><button class="download">경비 등록양식</button></div>
                        <div class="mgt5">정비등록 양식을 다운로드 받아 해당양식에 맞게 기재하신 후 아래에서 업로드하시면 즉시 등록됩니다.</div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">대량등록 업로드</th>
                    <td><input type="file" /></td>
                </tr>
            </tbody>
        </table>
        <!-- 하단 버튼 -->
        <div class="btn-bottom">
            <button class="btn btn03">확인</button>
        </div>
        <!--/ 하단 버튼 -->
    </div>
    <!--/ 정비내역 대량등록 팝업 -->