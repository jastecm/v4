<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
var hiddenPartsKey;
var partNm;
var _maintenanceKey = '${maintenanceKey}';
var _vehicleKey = '${vehicleKey}';
var _mode = '${mode}';
$(function(){
	
	$('#maintenanceRegCancelBtn').on('click',function(){
		window.history.back(-1);
	});
	if(_mode != "view"){
		$('#maintenanceRegBtn').show();
	}	
	if(_mode == "view"){
		//부품을 셋팅해야된다
		$V4.http_post("/api/1/maintenance", {
			"vehicleType" : "1",
			"counting" : false,
			"maintenanceKey" : _maintenanceKey
		}, {
			requestMethod : "GET",
			header : {
				"key" : "${_KEY}"
			},
			success : function(rtv) {
				var data = rtv.result[0];
				$('#garage').val(getProperty(data,'garage'));
				$('#distance').val(getProperty(data,'distance'));
				var repairDate = convertDateUint(new Date(data.repairDate),_unitDate,_timezoneOffset,true)
				var date = repairDate.split(' ')[0];
				$('#repairDate').val(date);
				var hourMinute = repairDate.split(' ')[1];
				var hour = hourMinute.split(':')[0];
				var minute = hourMinute.split(':')[1];
				$('#repairHour option[value="'+hour+'"]').prop('selected',true);
				$('#repairMinute option[value="'+minute+'"]').prop('selected',true);
				
				$('#maintenanceHistory').val(convertNullString(getProperty(data,'maintenanceHistory')));
				
				if(data.detail != 0){
					$('#vehiclePartsList').empty();
					for(var i = 0 ; i < data.detail.length;i++){
						var obj = data.detail[i];
						
						var strHtml = "";
						strHtml += '<tr class="removeParts">';
						strHtml += '    <th scope="row">정비항목</th>';
						strHtml += '    <td>';
						strHtml += '        <div class="division">';
						strHtml += '            <div class="col-2">';
						strHtml += '                <input type="text" readonly="readonly" name="partsNm" value="'+convertNullString(obj.partsNm)+'">';
						strHtml += '                <input type="hidden" name="partsKey" value="'+obj.partsKey+'">';
						strHtml += '            </div>';
						strHtml += '            <div class="col-1">';
						strHtml += '                <button type="button" class="btn btn02 md partsPopBtn">선택</button>';
						strHtml += '            </div>';
						strHtml += '            <div class="space">&nbsp;</div>';
						strHtml += '            <div class="col-9"><input type="text" name="memo" value="'+convertNullString(obj.memo)+'" /></div>';
						strHtml += '            <div class="space">&nbsp;</div>';
						/* strHtml += '            <div class="col-2">';
						strHtml += '                <button type="button" class="btn btn03 md delParts">삭제</button>';
						strHtml += '            </div>'; */
						strHtml += '        </div>';
						strHtml += '        <div class="detail">';
						strHtml += '            <div class="list">';
						strHtml += '                <span>부품등급</span>';
						strHtml += '                <select name="partsQuality">';
						if(obj.partsQuality == "0"){
							strHtml += '					<option value="0" selected>A</option>';
							strHtml += '                    <option value="1">B</option>';
							strHtml += '                    <option value="2">C</option>';	
						}else if(obj.partsQuality == "1"){
							strHtml += '					<option value="0">A</option>';
							strHtml += '                    <option value="1" selected>B</option>';
							strHtml += '                    <option value="2">C</option>';
						}else if(obj.partsQuality == "2"){
							strHtml += '					<option value="0">A</option>';
							strHtml += '                    <option value="1">B</option>';
							strHtml += '                    <option value="2" selected>C</option>';
						}
						
						strHtml += '				</select>';
						strHtml += '            </div>';
						strHtml += '            <div class="list">';
						strHtml += '                <span>수량</span>';
						strHtml += '                <input type="text" class="numberOnly" name="partsCnt" value="'+convertNullString(getProperty(data.detail[i],'partsCnt'),0)+'" />';
						strHtml += '            </div>';
						strHtml += '            <div class="list">';
						strHtml += '                <span>부품금액(원)</span>';
						strHtml += '                <input type="text" class="numberOnly partsPay" name="partsPay" value="'+convertNullString(getProperty(data.detail[i],'partsPay'),0)+'" />';
						strHtml += '            </div>';
						strHtml += '            <div class="list">';
						strHtml += '                <span>공임(원)</span>';
						strHtml += '                <input type="text" class="numberOnly wagesPay" name="wagePay" value="'+convertNullString(getProperty(data.detail[i],'wagePay'),0)+'" />';
						strHtml += '            </div>';
						strHtml += '        </div>';
						strHtml += '    </td>';
						strHtml += '</tr>';
						
						$('#vehiclePartsList').append(strHtml);
					}
					
					
					$('.partsPay,.wagesPay').trigger('change');
				}
				
			},
			error : function(t) {
				console.log(t);
			}
		});
	}
	
	
	$V4.http_post("/api/1/vehicle/"+_vehicleKey, {}, {
		requestMethod : "GET",
		header : {
			"key" : "${_KEY}"
		},
		success : function(rtv) {
			var data = rtv.result;
			if(data.hasOwnProperty('device')){
				//단말기가 장착된거다
				$('#deviceSeries').text(data.device.deviceSeries+"/"+data.device.deviceSn);
			}
			$('#corpName').text(data.corp.corpName);
			$('#plateNum').text(data.plateNum);
			$('#totDistance').text(number_format(Math.round(data.totDistance / 1000))+" km");
			$('#volumn').text(data.vehicleModel.volume+" cc");
			$('#transmission').text(data.vehicleModel.transmission);
			$('#manufactureAndmodelHeader').text(data.vehicleModel.manufacture+"/"+data.vehicleModel.modelHeader);
			$('#modelMasterAndyear').text(data.vehicleModel.modelMaster+"/"+data.vehicleModel.year);
			$('#fuelType').text(data.vehicleModel.fuelType);
			//color 없음
		},
		error : function(t) {
			console.log(t);
		}
	});
	 
	$('#addParts').on('click',function(){
		
		var strHtml = "";
		strHtml += '<tr class="removeParts">';
		strHtml += '    <th scope="row">정비항목</th>';
		strHtml += '    <td>';
		strHtml += '        <div class="division">';
		strHtml += '            <div class="col-2">';
		strHtml += '                <input type="text" readonly="readonly" name="partsNm">';
		strHtml += '                <input type="hidden" name="partsKey">';
		strHtml += '            </div>';
		strHtml += '            <div class="col-1">';
		strHtml += '                <button type="button" class="btn btn02 md partsPopBtn">선택</button>';
		strHtml += '            </div>';
		strHtml += '            <div class="space">&nbsp;</div>';
		strHtml += '            <div class="col-7"><input type="text" name="memo" /></div>';
		strHtml += '            <div class="space">&nbsp;</div>';
		strHtml += '            <div class="col-2">';
		strHtml += '                <button type="button" class="btn btn03 md delParts">삭제</button>';
		strHtml += '            </div>';
		strHtml += '        </div>';
		strHtml += '        <div class="detail">';
		strHtml += '            <div class="list">';
		strHtml += '                <span>부품등급</span>';
		strHtml += '                <select name="partsQuality">';
		strHtml += '					<option value="0">A</option>';
		strHtml += '                    <option value="1">B</option>';
		strHtml += '                    <option value="2">C</option>';
		strHtml += '				</select>';
		strHtml += '            </div>';
		strHtml += '            <div class="list">';
		strHtml += '                <span>수량</span>';
		strHtml += '                <input type="text" class="numberOnly" name="partsCnt" />';
		strHtml += '            </div>';
		strHtml += '            <div class="list">';
		strHtml += '                <span>부품금액(원)</span>';
		strHtml += '                <input type="text" class="numberOnly partsPay" name="partsPay" />';
		strHtml += '            </div>';
		strHtml += '            <div class="list">';
		strHtml += '                <span>공임(원)</span>';
		strHtml += '                <input type="text" class="numberOnly wagesPay" name="wagePay" />';
		strHtml += '            </div>';
		strHtml += '        </div>';
		strHtml += '    </td>';
		strHtml += '</tr>';
		
		$('#vehiclePartsList').append(strHtml);
	});
	
	$(document).on('click','.delParts',function(){
		$(this).parents('.removeParts').remove();
	});
	
	$(document).on('click','.partsPopBtn',function(){
 		 var self = $(this);
 		 hiddenPartsKey = self.parent().prev().children('input[name="partsKey"]');
 		 partNm = self.parent().prev().children('input[name="partsNm"]');
			 
		 $('#partsList').commonLazy(lazySetting);
		 initPartsDistinct();
		 $("#parts-dialog").dialog("open") 
		
	});
	
	var thead = [
	              '',
	              '분류',
	              '부품코드',
	              '부품명'
	     ];
	 
	 var lazySetting = {
   		thead : thead
   		,bodyHtml : (function(data){
   			var strHtml = "";
   			
   			$('#partsCnt').text(data.totalCnt);
   			
   			
   			for(i in data.result){
   				var obj = data.result[i];
   				strHtml += '<tr>';
   				strHtml += '	<td>';
   				
   				var jsonData = {
       					"partsKey" : obj.partsKey,
       					"defaultMax" : obj.defaultMax,
       					"partsNm" : obj.partsNm
       			}
   				
   				strHtml += '	    <input type="radio" name="part" data-info="'+jsonObjToBase64(jsonData)+'" />';
   				strHtml += '	</td>';
   				strHtml += '	<td>'+obj.partsTypeNm+'</td>';
   				strHtml += '	<td>'+obj.partsNum+'</td>';
   				strHtml += '	<td>'+obj.partsNm+'</td>';
   				strHtml += '</tr>';
   			}
   			return strHtml;
   		})
   		,limit : 10
   		,offset : 0
   		,url : "/api/1/parts"
   		,param : {}
   		,http_post_option : {
   			requestMethod : "GET"
   			,header : {key : "${_KEY}"}
   		}
   		,initSearch : true //생성과 동시에 search		
   		,loadingBodyViewer : true //로딩중!! 표시됨
   		,debugMode : true
   	}
	
	 $("#parts-dialog").dialog({
	        autoOpen: false,
	        show: {
	            duration: 500
	        },
	        width: '960',
	        modal: true
	 });
	 
	 var initPartsDistinct = function(){
		 
		 $V4.http_post("/api/1/partsDistinct", {}, {
				requestMethod : "GET",
				header : {
					"key" : "${_KEY}"
				},
				success : function(rtv) {
					var data = rtv.result;
					$('#partsDistinctList').empty();
					$('#partsDistinctList').append('<option value="">전체</option>');
					for(var i = 0 ; i < data.length ; i++){
						var obj = data[i];
						$('#partsDistinctList').append('<option value="'+obj.partsType+'">'+obj.partsTypeNm+'</option>')
						
					}
					//<option value="">전체</option>
				},
				error : function(t) {
					console.log(t);
				}
			});
	 }
	 
	 $('#popUpSearchBtn').on('click',function(){
		 
		 var partsType = $('#partsDistinctList').val();
		 var searchPartsNm = $('#partsSearchInput').val();
		 
		 lazySetting.param = {
				 "partsType" : partsType,
				 "searchPartsNm" : searchPartsNm
		  };
		 
		 $('#partsList').commonLazy(lazySetting);
	 });
	 
	 $('#partsPopupCancelBtn').on('click',function(){
		 $("#parts-dialog").dialog("close");
	 });
	 
	 //팝업 선택
	 $('#partsPopupSelectBtn').on('click',function(){
		
		var self = $(this);
		var selectObj = $('#partsList input[name="part"]:checked');
		var info = selectObj.data('info');
		var jsonData = base64ToJsonObj(info);
		
		var partsKey = jsonData.partsKey;
		var defaultMax = jsonData.defaultMax;
		var partsNm = jsonData.partsNm;
		
		hiddenPartsKey.val(partsKey);
		partNm.val(partsNm);
		
		$("#parts-dialog").dialog("close");
		
	 });
	 
	//부품비 계산 이벤트
		$(document).on('change','.partsPay',function(){
			var thisObj = $(this);
			//숫자일경우
			if(checkNumber(thisObj.val())){
				var cost = 0;
				$('.partsPay').each(function(_index,_input){
					if(!_.isNaN(parseInt(_input.value))){
						cost +=	parseInt(_input.value);
					}
				});
				$('#partsCostDisplay').html(number_format(cost));
				$('#partsCostDisplay').data("cost",cost)
			}else{
				//숫자 아닐경우
				/* alert('숫자만 입력하시기 바랍니다.'); */
				thisObj.val('');
				thisObj.focus();
				$( ".partsCost" ).trigger( "change" );
			}
			
			var a = parseInt($('#partsCostDisplay').data('cost'));
			var b = parseInt($('#wagesPayDisplay').data('cost'));
			
			$('#totalPriceDisplay').html(number_format(a+b));
		});

		//공임비 계산 이벤트
		$(document).on('change','.wagesPay',function(){
			var thisObj = $(this);
			
			//숫자일경우
			if(checkNumber(thisObj.val())){
				var cost = 0;
				$('.wagesPay').each(function(_index,_input){
					if(!_.isNaN(parseInt(_input.value))){
						cost +=	parseInt(_input.value);
					}
					 
				});
				
				$('#wagesPayDisplay').html(number_format(cost));
				$('#wagesPayDisplay').data('cost',cost);
			}else{
				//숫자 아닐경우
				/* alert('숫자만 입력하시기 바랍니다.'); */
				thisObj.val('');
				thisObj.focus();
				$( ".wagesPay" ).trigger( "change" );
			}
			
			var a = parseInt($('#partsCostDisplay').data('cost'));
			var b = parseInt($('#wagesPayDisplay').data('cost'));
			
			$('#totalPriceDisplay').html(number_format(a+b));
			
		});
	 //정비 등록
	 $('#maintenanceRegBtn').on('click',function(){
		 
		 if( $V4.requiredMsg($('#garage').val() , "정비소명은 필수입니다.") ) return false;
		 if( $V4.requiredMsg($('#distance').val() , "주행거리는 필수입니다.") ) return false;
		 if( $V4.requiredMsg($('#repairDate').val() , "정비일시는 필수입니다.") ) return false;
		 if( $V4.requiredMsg($('#maintenanceHistory').val() , "정비내역은 필수입니다.") ) return false;
		 
		 var garage = $('#garage').val();
		 var distance = $('#distance').val()
		 var repairDate = $('#repairDate').val();
		 var repairHour = $('#repairHour').val();
		 var repairMinute =  $('#repairMinute').val();
		 var maintenanceHistory = $('#maintenanceHistory').val();
		 
		 repairDate = $('#repairDate').val() + " " + $('#repairHour').val() + ":" + $('#repairMinute').val();

		 var sendData = {
				"maintenanceKey" : _maintenanceKey,
				"garage" : garage,
				"distance" : distance,
				"repairDate" : repairDate,
				"maintenanceHistory" : maintenanceHistory,
				"partsKey" : [],
				"memo" : [],
				"partsQuality" : [],
				"partsCnt" : [],
				"partsPay" : [],
				"wagePay" : []
		 }
		 
		 $('#vehiclePartsList tr').each(function(index){
			var obj = $(this);
			
			var partsKey = obj.find('input[name="partsKey"]').val();
			if(partsKey == ""){
				alert('정비항목을 선택해주세요');
				return false;
			}
			sendData.partsKey.push(partsKey);
			var memo = obj.find('input[name="memo"]').val();
			sendData.memo.push(memo);
			var partsQuality = obj.find('select[name="partsQuality"]').val();
			sendData.partsQuality.push(partsQuality);
			var partsCnt = obj.find('input[name="partsCnt"]').val();
			sendData.partsCnt.push(partsCnt ? parseInt(partsCnt) : 0);
			var partsPay = obj.find('input[name="partsPay"]').val();
			sendData.partsPay.push(partsPay ? parseInt(partsPay) : 0);
			var wagePay = obj.find('input[name="wagePay"]').val();
			sendData.wagePay.push(wagePay ? parseInt(wagePay) : 0);
		 });
		 
		 
		 $V4.http_post("/api/1/maintenanceDetail", sendData, {
				requestMethod : "POST",
				header : {
					"key" : "${_KEY}"
				},
				success : function(rtv) {
					console.log(rtv);
					//$V4.move('/vehicleMng/vehicleMaintenance');
				},
				error : function(t) {
					console.log(t);
				}
			});
		 
	 });
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>정비현황 조회ㆍ등록</h2>
                    <span>등록차량의 차량 정비 및 소모품 현황을 조회ㆍ등록할 수 있습니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="vehicle-page">
                    <div class="box-layout register-vehicle-repair">
                        <div class="title">
                            <h3 class="tit3">차량정비 등록</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
                            <div class="info-txt">
                                등록차량의 차량정비 및 소모품 교체이력을 입력합니다. 차량선택을 먼저 해주세요.
                            </div>

                            <div class="vehicle-info">
                                <div class="img">
                                    <img src="${pageContext.request.contextPath}/common/new/img/vehicle/L47.png" alt="" />
                                </div>
                                <div class="info">
                                    <table class="table mgt20">
                                        <caption>등록차량 정보</caption>
                                        <colgroup>
                                            <col style="width:21.5%" />
                                            <col style="width:28.5%" />
                                            <col style="width:21.5%" />
                                            <col style="width:28.5%" />
                                        </colgroup>
                                        <tbody>
                                            <tr>
                                                <th scope="row">차량선택</th>
                                                <td colspan="3" id="plateNum">
                                                
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">ViewCAR모델명/일련번호</th>
                                                <td colspan="3" id="deviceSeries"></td>
                                            </tr>
                                        </tbody>
                                        <tbody>
                                            <tr>
                                                <th scope="row">소속/지점</th>
                                                <td id="corpName"></td>
                                                <th scope="row">제조사</th>
                                                <td id="manufactureAndmodelHeader"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">배기량</th>
                                                <td id="volumn"></td>
                                                <th scope="row">모델명/년식</th>
                                                <td id="modelMasterAndyear"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">기어방식</th>
                                                <td id="transmission"></td>
                                                <th scope="row">총 주행거리</th>
                                                <td id="totDistance"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">유종</th>
                                                <td id="fuelType"></td>
                                                <th scope="row">색상</th>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <h4 class="tit2 mgt30">운행기록 현황</h4>
                            <table class="table mgt20">
                                <caption>운행기록 현황 등록</caption>
                                <tbody>
                                    <tr>
                                        <th scope="row">차량정비</th>
                                        <td class="table" colspan="3">
                                            <table>
                                                <caption>차량정비</caption>
                                                <colgroup>
                                                    <col style="width:14.75%" />
                                                    <col style="width:35.25%" />
                                                    <col style="width:14.75%" />
                                                    <col style="width:35.25%" />
                                                </colgroup>
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">정비소</th>
                                                        <td><input type="text" name="garage" id="garage"/></td>
                                                        <th scope="row">주행거리</th>
                                                        <td><input type="text" name="distance" id="distance" /></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">정비일시</th>
                                                        <td colspan="3">
                                                            <div class="division">
                                                                <div class="col-3">
                                                                    <input type="text" id="repairDate" class="datePicker" />
                                                                </div>
                                                                <div class="space">&nbsp;-&nbsp;</div>
                                                                <div class="col-9">
                                                                    <select name="repairHour" id="repairHour" style="width:50px">
																		<option value="00">00</option>
																		<option value="01">01</option>
																		<option value="02">02</option>
																		<option value="03">03</option>
																		<option value="04">04</option>
																		<option value="05">05</option>
																		<option value="06">06</option>
																		<option value="07">07</option>
																		<option value="08">08</option>
																		<option value="09">09</option>
																		<option value="10">10</option>
																		<option value="11">11</option>
																		<option value="12">12</option>
																		<option value="13">13</option>
																		<option value="14">14</option>
																		<option value="15">15</option>
																		<option value="16">16</option>
																		<option value="17">17</option>
																		<option value="18">18</option>
																		<option value="19">19</option>
																		<option value="20">20</option>
																		<option value="21">21</option>
																		<option value="22">22</option>
																		<option value="23">23</option>
																	</select> 시 &nbsp;&nbsp;&nbsp;
                                                                    <select name="repairMinute" id="repairMinute" style="width:50px">
																		<option value="00">00</option>
																		<option value="01">01</option>
																		<option value="02">02</option>
																		<option value="03">03</option>
																		<option value="04">04</option>
																		<option value="05">05</option>
																		<option value="06">06</option>
																		<option value="07">07</option>
																		<option value="08">08</option>
																		<option value="09">09</option>
																		<option value="10">10</option>
																		<option value="11">11</option>
																		<option value="12">12</option>
																		<option value="13">13</option>
																		<option value="14">14</option>
																		<option value="15">15</option>
																		<option value="16">16</option>
																		<option value="17">17</option>
																		<option value="18">18</option>
																		<option value="19">19</option>
																		<option value="20">20</option>
																		<option value="21">21</option>
																		<option value="22">22</option>
																		<option value="23">23</option>
																		<option value="24">24</option>
																		<option value="25">25</option>
																		<option value="26">26</option>
																		<option value="27">27</option>
																		<option value="28">28</option>
																		<option value="29">29</option>
																		<option value="30">30</option>
																		<option value="31">31</option>
																		<option value="32">32</option>
																		<option value="33">33</option>
																		<option value="34">34</option>
																		<option value="35">35</option>
																		<option value="36">36</option>
																		<option value="37">37</option>
																		<option value="38">38</option>
																		<option value="39">39</option>
																		<option value="40">40</option>
																		<option value="41">41</option>
																		<option value="42">42</option>
																		<option value="43">43</option>
																		<option value="44">44</option>
																		<option value="45">45</option>
																		<option value="46">46</option>
																		<option value="47">47</option>
																		<option value="48">48</option>
																		<option value="49">49</option>
																		<option value="50">50</option>
																		<option value="51">51</option>
																		<option value="52">52</option>
																		<option value="53">53</option>
																		<option value="54">54</option>
																		<option value="55">55</option>
																		<option value="56">56</option>
																		<option value="57">57</option>
																		<option value="58">58</option>
																		<option value="59">59</option>
																	</select> 분
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">정비내역</th>
                                                        <td colspan="3"><textarea rows="5" class="w100" id="maintenanceHistory"></textarea></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">상세내역</th>
                                        <td class="table" colspan="3">
                                            <table>
                                                <caption>상세내역</caption>
                                                <colgroup>
                                                    <col style="width:14.75%" />
                                                    <col />
                                                </colgroup>
                                                <tbody id="vehiclePartsList">
                                                    <tr>
                                                        <th scope="row">정비항목</th>
                                                        <td>
                                                            <div class="division">
                                                                <div class="col-2">
																	<input type="text" readonly="readonly" name="partsNm">
																	<input type="hidden" name="partsKey">
                                                                </div>
                                                                <div class="col-1">
                                                                	<button type="button" class="btn btn02 md partsPopBtn">선택</button>
                                                                </div>
                                                                <div class="space">&nbsp;</div>
                                                                <div class="col-7"><input type="text" name="memo" /></div>
                                                                <div class="space">&nbsp;</div>
                                                                <div class="col-2">
                                                                	<button type="button" class="btn btn02 md" id="addParts">추가</button>
                                                                </div>
                                                            </div>

                                                            <div class="detail">
                                                                <div class="list">
                                                                    <span>부품등급</span>
                                                                    <select name="partsQuality">
																		<option value="0">A</option>
																		<option value="1">B</option>
																		<option value="2">C</option>
																	</select>
                                                                </div>
                                                                <div class="list">
                                                                    <span>수량</span>
                                                                    <input type="text" class="numberOnly" name="partsCnt" />
                                                                </div>
                                                                <div class="list">
                                                                    <span>부품금액(원)</span>
                                                                    <input type="text" class="numberOnly partsPay" name="partsPay" />
                                                                </div>
                                                                <div class="list">
                                                                    <span>공임(원)</span>
                                                                    <input type="text" class="numberOnly wagesPay" name="wagePay" />
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="total-price">
                                <div class="total"><span>총 결제금액</span> : <strong id="totalPriceDisplay" data-cost="0">0</strong>원</div>
                                <div class="items">(부품 + 공임 : <span id="partsCostDisplay" data-cost="0">0</span>원 + <span id="wagesPayDisplay" data-cost="0">0</span>원)</div>
                                <div class="info">[총 결제금액은 상세내역을 입력하면 자동 합산됩니다.]</div>
                            </div>

                            <!-- 하단 버튼 -->
                            <div class="btn-bottom">
                                <button type="button" class="btn btn02" id="maintenanceRegCancelBtn">취소</button>
                                <button type="button" class="btn btn03" id="maintenanceRegBtn" style="display:none">등록완료</button>
                            </div>
                            <!--/ 하단 버튼 -->
                        </div>
                    </div>
                    <!--/ 콘텐츠 본문 -->
                </div>
            </div>
        </section>
        <!--/ 본문 -->
<!-- 부품 팝업 -->
<div id="parts-dialog" title="부품 선택">
        <div class="top-info clr">
            <span class="left">등록된 모든 부품을 표시합니다.</span>
            <span class="right car">
				총 부품 대수
				<strong id="partsCnt">0</strong>개
			</span>
        </div>
        <!-- 상단 검색 -->
        <div class="top-search">
            <select id="partsDistinctList">
				
			</select>
            <input type="text" id="partsSearchInput" />
            <button type="button" class="btn btn03" id="popUpSearchBtn">조회</button>
        </div>
        <!--/ 상단 검색 -->

        <div class="scroll-table mgt50">
            <table class="table list" id="partsList">
            </table>
        </div>

        <!-- 하단 버튼 -->
        <div class="btn-bottom">
            <button type="button" class="btn btn02" id="partsPopupCancelBtn">취소</button>
            <button type="button" class="btn btn03" id="partsPopupSelectBtn">선택</button>
        </div>
        <!--/ 하단 버튼 -->
    </div>
    <!-- 부품 팝업 -->