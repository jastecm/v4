<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ju" uri="/WEB-INF/tld/JsonUtils.tld"%>
<script type="text/javascript">
$(function(){
	
	//로그인한 사용자의 정보이다.
	var account = ${ju:toJson(V4)};
	
	$('#corpNm').text(account.corp.corpName);
	$('#phone').text(account.phoneCtyCode+' '+account.phone);
	$('#mobilePhone').text(account.mobilePhoneCtyCode+' '+account.mobilePhone);
	var groupNm = account.group.groupNm ? account.group.groupNm : account.group.parentGroupNm;  
	$('#groupNmAndName').text(groupNm+"/"+account.name)
	$('#accountId').text(account.accountId);
	
	//차량 선택
	$('#vehiclePopBtn').data({
		type : "vehicle",
		http_post_option : {
			header : {
				key : "${_KEY}"
			}
		},
		selType : "radio",
		action : {
			ok : function(arrSelect, inputObj, valueObj, _this) {
				var info = base64ToJsonObj($(arrSelect).data("info"));
				$(inputObj).val(info.plateNum);
				$(valueObj).val(info.vehicleKey);
			}
		},
		targetInput : $('#plateNum'),
		targetInputValue : $('#vehicleKey')
	}).commonPop();
	
	$('#img').on('change',function(){
    	var formData = new FormData();
    	formData.append("uploadType","penalty");
    	
    	for(var i = 0 ; i < $("#img")[0].files.length;i++){
    		formData.append("img"+i, $("#img")[0].files[i]);	
    	}
		
		$V4.fileUpload(formData,{
			header : {"key" : "${_KEY}"},
			success : function(data){
				$('#billImg').val(data.result.img0);
			},error : function(t){
				alert('error');
			}
		});
 
    });
	
	$('#penaltyCancel').on('click',function(){
		window.history.back(-1);
	});
	
	$('#penaltyRegBtn').on('click',function(){
		if( $V4.requiredMsg($('#business').val() , "업무명 필수입니다.") ) return false;
		
		if( $V4.requiredMsg($('#vehicleKey').val() , "차량선택은 필수입니다.") ) return false;
		if( $V4.requiredMsg($('#crackDownDate').val() , "단속일시는 필수입니다.") ) return false;
		
		if( $V4.requiredMsg($('#penaltyType').val() , "구분은 필수입니다.") ) return false;
		if( $V4.requiredMsg($('#violationType').val() , "항목은 필수입니다.") ) return false;
		
		if( $V4.requiredMsg($('#reason').val() , "사유는 필수입니다.") ) return false;
		if( $V4.requiredMsg($('#location').val() , "단속장소는 필수입니다.") ) return false;
		
		if( $V4.requiredMsg($('#jurisdiction').val() , "관할는 필수입니다.") ) return false;
		if( $V4.requiredMsg($('#billImg').val() , "고지서는 필수입니다.") ) return false;
		if( $V4.requiredMsg($('#paymentType').val() , "납부 여부는 필수입니다.") ) return false;
		if( $V4.requiredMsg($('#deadLineDate').val() , "납부 마감일 필수입니다.") ) return false;
		
		var business = $('#business').val();
		var vehicleKey = $('#vehicleKey').val();
		var date = $('#crackDownDate').val();
		var hour = $('#crackDownHour').val();
		var minute = $('#crackDownMinute').val();
		var crackDownDate = date + ' ' + hour + ':' + minute;
		
		var penaltyType = $('#penaltyType').val();
		var violationType = $('#violationType').val();
		var reason = $('#reason').val();
		var location = $('#location').val();
		var jurisdiction = $('#jurisdiction').val()
		var billImg = $('#billImg').val()
		var paymentType = $('#paymentType').val()
		var deadLineDate = $('#deadLineDate').val()
		
		var sendData = {
			"business" : business,
			"vehicleKey" : vehicleKey,
			"crackDownDate" : crackDownDate,
			"penaltyType" : penaltyType,
			"violationType" : violationType,
			"reason" : reason,
			"location" : location,
			"jurisdiction" : jurisdiction,
			"billImg" : billImg,
			"paymentType" : paymentType,
			"deadLineDate" : deadLineDate
		}
		
		$V4.http_post("/api/1/penalty", sendData, {
			requestMethod : "POST",
			header : {
				"key" : "${_KEY}"
			},
			success : function(rtv) {
				$V4.move('/vehicleMng/penalty');
			},
			error : function(t) {
				console.log(t);
			}
		});
		
		
	});
});
</script>
 <!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>벌점/과태료 조회ㆍ등록</h2>
                    <span>등록차량의 운행중 발생한 벌점ㆍ과태료를 등록하거나 조회합니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="vehicle-page">
                    <div class="box-layout register-fine">
                        <div class="title">
                            <h3 class="tit3">벌점/과태료 등록</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
                            <div class="info-txt">
                                아래 항목을 순서대로 정확하게 입력한 뒤, 하단의 배차신청등록버튼을 누르면 신청이 완료됩니다.
                            </div>

                            <div class="section01">
                                <div class="left">
                                	<img src="${pageContext.request.contextPath}/common/new/img/common/user-img3.png" alt="" />
                                </div>
                                <div class="right">
                                    <table class="table user mgt20">
                                        <caption>운행스케쥴 상세정보</caption>
                                        <colgroup>
                                            <col style="width:20%" />
                                            <col style="width:30%" />
                                            <col style="width:20%" />
                                            <col style="width:30%" />
                                        </colgroup>
                                        <tbody>
                                            <tr>
                                                <th scope="row">소속</th>
                                                <td id="corpNm"></td>
                                                <th scope="row">전화번호</th>
                                                <td id="phone"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">부서/이름</th>
                                                <td id="groupNmAndName"></td>
                                                <th scope="row">휴대폰</th>
                                                <td id="mobilePhone"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">이메일</th>
                                                <td id="accountId"></td>
                                                <th scope="row">&nbsp;</th>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </tbody>

                                       <!--  <tbody>
                                            <tr>
                                                <th scope="row">개시일시</th>
                                                <td>2018/05/19 05:58</td>
                                                <th scope="row">종료일시</th>
                                                <td>2018/05/19 05:58</td>
                                            </tr>
                                        </tbody> -->

                                        <tbody>
                                            <tr>
                                                <th scope="row">업무명</th>
                                                <td colspan="3"><input type="text" id="business"/></td>
                                            </tr>
                                            <!-- <tr>
                                                <th scope="row">활동계획</th>
                                                <td colspan="3"><textarea rows="3" class="w100"></textarea></td>
                                            </tr> -->
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <h3 class="tit2 mgt30">벌점/과태료</h3>
                            <table class="table mgt20">
                                <caption>벌점/과태료 등록</caption>

                                <tbody>
                                	<tr>
                                        <th scope="row">차량</th>
                                        <td colspan="5">
                                            <div class="division">
                                                <div class="col-10">
                                                    <input type="text" readonly="readonly" id="plateNum" />
                                        			<input type="hidden" id="vehicleKey" />
                                                </div>
                                                <div class="space">&nbsp;</div>
                                                <div class="col-2">
                                                    <button type="button" class="btn btn01 md" id="vehiclePopBtn">검색</button>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">단속일시</th>
                                        <td colspan="3">
                                            <div class="division">
                                                <div class="col-3">
                                                    <input type="text" id="crackDownDate" class="datePicker" />
                                                </div>
                                                <div class="space">&nbsp;-&nbsp;</div>
                                                <div class="col-9">
                                                    <select id="crackDownHour" style="width:50px">
														<option value="00">00</option>
														<option value="01">01</option>
														<option value="02">02</option>
														<option value="03">03</option>
														<option value="04">04</option>
														<option value="05">05</option>
														<option value="06">06</option>
														<option value="07">07</option>
														<option value="08">08</option>
														<option value="09">09</option>
														<option value="10">10</option>
														<option value="11">11</option>
														<option value="12">12</option>
														<option value="13">13</option>
														<option value="14">14</option>
														<option value="15">15</option>
														<option value="16">16</option>
														<option value="17">17</option>
														<option value="18">18</option>
														<option value="19">19</option>
														<option value="20">20</option>
														<option value="21">21</option>
														<option value="22">22</option>
														<option value="23">23</option>
													</select> 시 &nbsp;&nbsp;&nbsp;
                                                    <select id="crackDownMinute" style="width:50px">
														<option value="00">00</option>
														<option value="01">01</option>
														<option value="02">02</option>
														<option value="03">03</option>
														<option value="04">04</option>
														<option value="05">05</option>
														<option value="06">06</option>
														<option value="07">07</option>
														<option value="08">08</option>
														<option value="09">09</option>
														<option value="10">10</option>
														<option value="11">11</option>
														<option value="12">12</option>
														<option value="13">13</option>
														<option value="14">14</option>
														<option value="15">15</option>
														<option value="16">16</option>
														<option value="17">17</option>
														<option value="18">18</option>
														<option value="19">19</option>
														<option value="20">20</option>
														<option value="21">21</option>
														<option value="22">22</option>
														<option value="23">23</option>
														<option value="24">24</option>
														<option value="25">25</option>
														<option value="26">26</option>
														<option value="27">27</option>
														<option value="28">28</option>
														<option value="29">29</option>
														<option value="30">30</option>
														<option value="31">31</option>
														<option value="32">32</option>
														<option value="33">33</option>
														<option value="34">34</option>
														<option value="35">35</option>
														<option value="36">36</option>
														<option value="37">37</option>
														<option value="38">38</option>
														<option value="39">39</option>
														<option value="40">40</option>
														<option value="41">41</option>
														<option value="42">42</option>
														<option value="43">43</option>
														<option value="44">44</option>
														<option value="45">45</option>
														<option value="46">46</option>
														<option value="47">47</option>
														<option value="48">48</option>
														<option value="49">49</option>
														<option value="50">50</option>
														<option value="51">51</option>
														<option value="52">52</option>
														<option value="53">53</option>
														<option value="54">54</option>
														<option value="55">55</option>
														<option value="56">56</option>
														<option value="57">57</option>
														<option value="58">58</option>
														<option value="59">59</option>
													</select> 분
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">단속내용</th>
                                        <td class="table" colspan="3">
                                            <table>
                                                <caption>단속내용</caption>
                                                <colgroup>
                                                    <col style="width:14.75%" />
                                                    <col style="width:35.25%" />
                                                    <col style="width:14.75%" />
                                                    <col style="width:35.25%" />
                                                </colgroup>
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">구분</th>
                                                        <td>
                                                            <select id="penaltyType">
																<option value="">선택</option>
																<option value="1">과태료</option>
																<option value="2">벌점</option>
															</select>
                                                        </td>
                                                        <th scope="row">항목</th>
                                                        <td>
                                                            <select id="violationType">
																<option value="">선택</option>
																<option value="1">신호위반</option>
																<option value="2">주차위반</option>
																<option value="3">속도위반</option>
															</select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">사유</th>
                                                        <td colspan="3"><textarea id="reason" rows="5" class="w100" placeholder="위반사유를 자세하게 입력하세요."></textarea></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">단속장소</th>
                                                        <td><input type="text" id="location" /></td>
                                                        <th scope="row">관할</th>
                                                        <td><input type="text" id="jurisdiction" /></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">고지서</th>
                                        <td colspan="3">
                                        	<input type="file" id="img" />
                                        	<input type="hidden" id="billImg"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">납부</th>
                                        <td class="table" colspan="3">
                                            <table>
                                                <caption>납부여부 확인</caption>
                                                <colgroup>
                                                    <col style="width:14.75%" />
                                                    <col style="width:35.25%" />
                                                    <col style="width:14.75%" />
                                                    <col style="width:35.25%" />
                                                </colgroup>
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">여부</th>
                                                        <td>
                                                            <select id="paymentType">
																<option value="">선택</option>
																<option value="1">납부예정</option>
																<option value="2">납부완료</option>
																<option value="3">미납</option>
															</select>
                                                        </td>
                                                        <th scope="row">마감일</th>
                                                        <td><input type="text" class="datePicker" id="deadLineDate" /></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <!-- 하단 버튼 -->
                            <div class="btn-bottom">
                                <button type="button" class="btn btn02" id="penaltyCancel">취소</button>
                                <button type="button" class="btn btn03" id="penaltyRegBtn">등록완료</button>
                            </div>
                            <!--/ 하단 버튼 -->
                        </div>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->