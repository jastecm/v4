<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
    $('#penaltyRegBtn').on('click',function(){
    	$V4.move('/vehicleMng/penalty/reg');
    });

    var thead = [
                 {'<input type="checkbox" />' : '8%'},
                 {'<select id="penaltyType" class="arrow"><option value="">구분</option><option value="1">과태료</option><option value="2">벌점</option></select>' : '10%'},
                 {'<select id="violationType" class="arrow"><option value="">항목</option><option value="1">신호위반</option><option value="2">주차위반</option><option value="3">속도위반</option></select>' : '10%'},
                 {'단속정보' : '32%'},
                 {'차량정보' : '10%'},
                 {'사용자' : '10%'},
                 {'고지서' : '10%'},
                 {'<select id="paymentType" class="arrow"><option value="">납부여부</option><option value="1">납부예정</option><option value="2">납부완료</option><option value="3">미납</option></select>' : '10%'},
    ];
    
    var penaltySetting = {
    		thead : thead
    		,bodyHtml : (function(data){
    			var strHtml = "";
    			$('#totalCnt').text(data.totalCnt);
    			$('#penaltyPoint').text(data.penaltyPoint);
    			$('#penaltyPrice').text(data.penaltyPrice);
    			for(i in data.result){
    				var obj = data.result[i];
    				strHtml += '<tr>';
    				strHtml += '    <td><input type="checkbox" data-penaltykey="'+obj.penaltyKey+'" /></td>';
    				if(obj.penaltyType == "1"){
    					strHtml += '    <td>벌점</td>';	
    				}else if(obj.penaltyType == "2"){
    					strHtml += '    <td>과태료</td>';
    				}else {
    					strHtml += '    <td></td>';
    				}
    				
    				if(obj.violationType == "1"){
    					strHtml += '    <td>신호위반</td>';
    				}else if(obj.violationType == "2"){
    					strHtml += '    <td>주차위반</td>';
    				}else if(obj.violationType == "3"){
    					strHtml += '    <td>속도위반</td>';
    				}else{
    					strHtml += '    <td></td>';
    				}
    				
    				strHtml += '    <td class="left">';
    				strHtml += '        <div class="driving-info with-btn">';
    				strHtml += '            <ul>';
    				strHtml += '                <li>';
    				strHtml += '                    <strong class="title w40">일시</strong>';
    				strHtml += '                    <span>'+convertDateUint(new Date(obj.crackDownDate),_unitDate,_timezoneOffset,true)+'</span>';
    				strHtml += '                </li>';
    				strHtml += '                <li>';
    				strHtml += '                    <strong class="title w40">장소</strong>';
    				strHtml += '                    <span>'+obj.location+'</span>';
    				strHtml += '                </li>';
    				strHtml += '            </ul>';
    				strHtml += '        </div>';
    				strHtml += '    </td>';
    				strHtml += '    <td>'+getProperty(obj,'vehicle.vehicleModel.modelMaster')+'/<br />'+obj.plateNum+'</td>';
    				strHtml += '    <td>'+getProperty(obj,'account.corpPosition')+'/<br />'+getProperty(obj,'account.name')+'</td>';
    				strHtml += '    <td><button type="button" class="btn-img download" data-key="'+getProperty(obj,'file.fileKey')+'">고지서 다운로드</button></td>';
    				if(obj.paymentType == "1"){
    					strHtml += '    <td>납부예정</td>';	
    				}else if(obj.paymentType == "2"){
    					strHtml += '    <td>납부완료</td>';
    				}else if(obj.paymentType == "3"){
    					strHtml += '    <td>미납</td>';
    				}else{
    					strHtml += '    <td></td>';
    				}
    				
    				strHtml += '</tr>';
    			}
    			return strHtml;
    		})
    		,limit : parseInt($(".func_limitChange").val())
    		,pagePerGroup : 10 //pagingGroup size
    		,url : "/api/1/penalty"
    		,param : {'searchOrder' : $(".func_order").val()}
    		,http_post_option : {
    			requestMethod : "GET"
    			,header : {key : "${_KEY}"}
    		}
    		,initSearch : true //생성과 동시에 search		
    		,loadingBodyViewer : true //로딩중!! 표시됨
    		,debugMode : true
    }
    $('#penaltyList').commonList(penaltySetting);
    
    $(document).on('click','.download',function(){
    	var imgId = $(this).data('key');
    	location.href = "/api/1/"+imgId+"/${_KEY}/imgDown";
    	
    });
    
	$('#penaltyType,#violationType,#paymentType').on('change',function(){
		$("#top_btn_search").trigger("click");
    });
    
	$("#top_btn_search").on("click",$.debounce(200,1,function(){
		
		var param = {searchType:$("#top_searchType").val() , searchText : $("#top_searchText").val()}
		
		
		var limit = parseInt($(".func_limitChange").val());	 
		param.searchOrder = $(".func_order").val();
		
		param.penaltyType = $('#penaltyType').val();
		param.violationType = $('#violationType').val(); 
		param.paymentType = $('#paymentType').val(); 
		
		$(".searchStart").val()&&(param.searchStartDate = $(".searchStart").val());
		$(".searchEnd").val()&&(param.searchEndDate = $(".searchEnd").val());
		
		$('#penaltyList').commonList("setParam",param);
		$('#penaltyList').commonList("setLimit",limit).search();
		
	}));
	
    $(".func_order").on("change",function(){
		$("#top_btn_search").trigger("click");
	});
	
	$(".func_limitChange").on("change",function(){
		$("#top_btn_search").trigger("click");
	});
	
    $(".searchStart,.searchEnd").on("change",function(){
		$("#top_btn_search").trigger("click");
	});
    
    $("div.date button").on("click",function(){
		$(this).closest("div").find("button").removeClass("active");
		$(this).addClass("active");
		var d = $(this).data("day");
		
		if(d == ""){
			$(".datePicker").val("");
			$(".searchStart, .searchEnd").val("");
			
			$("#top_btn_search").trigger("click");
		}else{
			
			var rtvStartDate = calcDate(new Date() , 'd' , 0 , new Date());
			var rtvEndDate = calcDate(new Date() , 'd' , 0 , new Date());
			
			if(d == "-0D"){
			}else if(d == "-7D") rtvStartDate = calcDate(new Date() , 'd' , -7 , new Date());
			else if(d == "-1M") rtvStartDate = calcDate(new Date() , 'm' , -1 , new Date());
			else if(d == "-3M") rtvStartDate = calcDate(new Date() , 'm' , -3 , new Date());
			else if(d == "-6M") rtvStartDate = calcDate(new Date() , 'm' , -6 , new Date());
			
			$(".datePicker").eq(0).datepicker('setDate', rtvStartDate);
			$(".datePicker").eq(1).datepicker('setDate', rtvEndDate);
			
			var v = $(".datePicker").eq(0).datepicker( "getDate" ).getTime();
    		var t = $(".datePicker").eq(0).data("target");
    		$(t).val(v);
    		
    		var v = $(".datePicker").eq(1).datepicker( "getDate" ).getTime();
    		var t = $(".datePicker").eq(1).data("target");
    		$(t).val(v);
    		
    		$(t).trigger("change");
		}
		
	});
    
    $("#top_searchText").on("keyup",function(e){
		if(e.keyCode == 13) {
			$("#top_btn_search").trigger("click");
		}
	});
    
    $('#penaltyDelBtn').on('click',function(){
		var penaltyKeys = [];
		var checkBox = $('#penaltyList input[type="checkbox"]:checked');
		if(checkBox.length != 0){
			for(var i = 0 ; i < checkBox.length ; i++){
				penaltyKeys.push($(checkBox[i]).data('penaltykey'));
			}
			$V4.http_post("/api/1/penalty/"+penaltyKeys.join(','), {}, {
				requestMethod : "DELETE",
				header : {
					"key" : "${_KEY}"
				},
				success : function(rtv) {
					$("#top_btn_search").trigger("click");
				},
				error : function(t) {
					
				}
			});
		}
	});
});
</script>
 <!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>벌점/과태료 조회ㆍ등록</h2>
                    <span>등록차량의 운행중 발생한 벌점ㆍ과태료를 등록하거나 조회합니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="vehicle-page">
                    <div class="">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                            <div class="item">
                                <span>
									등록 차량수
									<strong id="totalCnt">0</strong>대
								</span>
                                <span>
									벌점 부과차량
									<strong id="penaltyPoint">0</strong>대
								</span>
                                <span>
									과태료 부과차량
									<strong id="penaltyPrice">0</strong>대
								</span>
                            </div>

                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <!-- 벌점과태료등록 -->
                        <h3 class="tit2 mgt30">벌점/과태료 등록</h3>
                        <div class="box-register">
                            등록차량의 운행중 발생한 벌점/과태료 내용을 등록합니다.

                            <div class="right">
                                <button type="button" class="btn btn01" id="penaltyRegBtn">벌점/과태료 등록</button>
                            </div>
                        </div>
                        <!--/ 벌점과태료등록 -->

                        <!-- 상단 검색 -->
                        <h3 class="tit2 mgt30">벌점/과태료 조회</h3>
                        <div class="top-search mgt20">
                            <select id="top_searchType">
								<option value="">전체</option>
								<option value="plateNum">차량번호</option>
							</select>
                            <input type="text" id="top_searchText"/>
                            <button type="button" class="btn btn03" id="top_btn_search">조회</button>
                        </div>
                        <!--/ 상단 검색 -->

                        <!-- table 버튼 -->
                        <div class="btn-function row-2">
                            <div class="left">
                                <select class="func_order">
									<option value="regDate">최근 등록순</option>
								</select>
                                <div class="date">
                                   <button type="button" class="active" data-day="">전체</button>
                                   <button type="button" data-day="-0D">오늘</button>
                                   <button type="button" data-day="-7D">1주일</button>
                                   <button type="button" data-day="-1M">1개월</button>
                                   <button type="button" data-day="-3M">3개월</button>
                                   <button type="button" data-day="-6M">6개월</button>
                                   <div class="direct">
                                       <input type="text" class="datePicker" data-target="#searchStart"/> &nbsp;~&nbsp;
                                       <input type="hidden" id="searchStart" class="searchStart"/>
                                       <input type="text" class="datePicker" data-target="#searchEnd"/>
                                       <input type="hidden" id="searchEnd" class="searchEnd"/>
                                   </div>
                               </div>
                            </div>
                            <div class="right">
                                <button type="button" class="img-btn delete" id="penaltyDelBtn">삭제</button>
                                <select class="func_limitChange">
									<option value="5" selected>5건씩 보기</option>
									<option value="10">10건씩 보기</option>
									<option value="20">20건씩 보기</option>
									<option value="50">20건씩 보기</option>
								</select>
                            </div>
                        </div>
                        <!--/ table 버튼 -->

                        <table class="table list mgt20" id="penaltyList">
                        </table>

                        <!-- 안내 -->
                        <div class="box-info">
                            <h3 class="hidden">안내</h3>
                            <h4 class="mgt0">납부 표기 안내</h4>
                            <ul>
                                <li><strong>납부완료</strong>과태료가 정상적으로 납부되었습니다.</li>
                                <li><strong>납부예정</strong>납부에정일이 남아있는 미납상태입니다.</li>
                                <li><strong>미납</strong>납부예정일이 지났습니다.</li>
                            </ul>
                        </div>
                        <!--/ 안내 -->
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->