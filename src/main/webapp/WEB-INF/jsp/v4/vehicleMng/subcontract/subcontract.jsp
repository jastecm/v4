<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	 $("#start-date").datepicker({
         dateformat: 'yy-mm-dd'
     });

     $("#end-date").datepicker({
         dateformat: 'yy-mm-dd'
     });
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>도급비</h2>
                    <span>
						차량 운행내역을 토대로 통학, 통근버스 및 학원차량의 정확한 도급비 계산을 하실 수 있습니다.<br />
						운행일시를 기준으로 오피넷 평균유가 정보를 반영하여 신뢰성있는 정보를 제공합니다.
					</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="vehicle-page">
                    <div class="">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                            <div class="item">
                                <span>
									운행건수
									<strong>10</strong>건
								</span>
                            </div>

                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <!-- 상단 검색 -->
                        <div class="top-search mgt20">
                            <select name="">
								<option value="">전체</option>
							</select>
                            <input type="text" name="" />
                            <button class="btn btn03">조회</button>
                        </div>
                        <!--/ 상단 검색 -->

                        <!-- table 버튼 -->
                        <div class="btn-function row-2">
                            <div class="left">
                                <select name="">
									<option value="">최근 운행순</option>
								</select>

                                <div class="date">
                                    <button class="active">전체</button>
                                    <button>오늘</button>
                                    <button>1주일</button>
                                    <button>1개월</button>
                                    <button>3개월</button>
                                    <button>6개월</button>
                                    <div class="direct">
                                        <input type="text" id="start-date" /> &nbsp;~&nbsp;
                                        <input type="text" id="end-date" />
                                    </div>
                                    <!-- 기획서에는 직접입력이 셀렉트박스로 되어 있는데, 사용자가 어떻게 날짜를 입력 해야 하는지 모르겠어서 임의로 변경했습니다.-->
                                </div>
                            </div>
                            <div class="right">
                                <button class="excel">엑셀 다운로드</button>
                                <select name="">
									<option value="">5건씩 보기</option>
								</select>
                            </div>
                        </div>
                        <!--/ table 버튼 -->

                        <table class="table list mgt20">
                            <caption>등록된 서비스 리스트</caption>
                            <colgroup>
                                <col style="width:25%" />
                                <col />
                                <col style="width:10%" />
                                <col style="width:20%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="col">차량번호</th>
                                    <th scope="col">운행시각</th>
                                    <th scope="col">운행경로</th>
                                    <th scope="col">노선명</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="car-img">
                                            <span class="img"><img src="./img/vehicle/S103.png" alt="" /></span>
                                            <span class="num">쏘나타 63호 5703</span>
                                        </div>
                                    </td>
                                    <td class="left">
                                        <div class="driving-info">
                                            <ul>
                                                <li>
                                                    <strong class="title w40">출발</strong>
                                                    <span>2018/05/19 05:58 충청남도 서산시 대산읍 도곡2로</span>
                                                </li>
                                                <li>
                                                    <strong class="title w40">도착</strong>
                                                    <span>2018/05/19 05:58 출청남도 서산시 충의로 262</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                    <td><button class="btn-img location">위치확인</button></td>
                                    <td>고운하이츠/기숙사</td>
                                </tr>
                            </tbody>
                        </table>

                        <!-- 페이징 -->
                        <div id="paging">
                            <a href="" class="first icon-spr">제일 처음으로</a>
                            <a href="" class="prev icon-spr">이전으로</a>
                            <span class="current">1</span>
                            <a href="" class="num">2</a>
                            <a href="" class="num">3</a>
                            <a href="" class="num">4</a>
                            <a href="" class="num">5</a>
                            <a href="" class="next icon-spr">다음으로</a>
                            <a href="" class="last icon-spr">제일 마지막으로</a>
                        </div>
                        <!--/ 페이징 -->
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->