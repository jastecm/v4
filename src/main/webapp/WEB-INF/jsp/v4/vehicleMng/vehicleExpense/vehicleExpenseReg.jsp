<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	var createBox = function(){
		var strHtml = "";
		strHtml += '<li>';
		strHtml += '    <div class="top-search mgt20">';
		strHtml += '        <input type="text" style="min-width:10.714rem" class="datePicker" name="expensesDate" />';
		strHtml += '        <input type="text" style="min-width:10.714rem" name="plateNum"/>';
		strHtml += '        <input type="hidden" name="vehicleKey" />';
		strHtml += '        <button type="button" class="btn btn02 vehiclePopBtn">차량 선택</button>';
		strHtml += '    </div>';
		strHtml += '    <table class="table mgt20">';
		strHtml += '        <caption>경비 등록</caption>';
		strHtml += '        <tbody>';
		strHtml += '            <tr>';
		strHtml += '                <th scope="row">유류대</th>';
		strHtml += '                <td>';
		strHtml += '                    <div class="division">';
		strHtml += '                        <div class="col-4">';
		strHtml += '                            <div class="text-end">';
		strHtml += '                                <input type="text" name="oilPrice" class="numberOnly cost" placeholder="금액입력" />';
		strHtml += '                                <span class="text">(원)</span>';
		strHtml += '                            </div>';
		strHtml += '                        </div>';
		strHtml += '                        <div class="space">&nbsp;</div>';
		strHtml += '                        <div class="col-4">';
		strHtml += '                            <div class="text-end">';
		strHtml += '                                <input type="text" name="oilLiquid" class="placeholder="리터입력" />';
		strHtml += '                                <span class="text">(리터)</span>';
		strHtml += '                            </div>';
		strHtml += '                        </div>';
		strHtml += '                        <div class="space">&nbsp;</div>';
		strHtml += '                        <div class="col-4">';
		strHtml += '                            <input type="hidden" name="oilImg" />';
		strHtml += '                            <input type="file" />';
		strHtml += '                        </div>';
		strHtml += '                    </div>';
		strHtml += '                </td>';
		strHtml += '            </tr>';
		strHtml += '            <tr>';
		strHtml += '                <th scope="row">통행료</th>';
		strHtml += '                <td>';
		strHtml += '                    <div class="division">';
		strHtml += '                        <div class="col-4">';
		strHtml += '                            <div class="text-end">';
		strHtml += '                                <input type="text" name="passageMoney" class="numberOnly cost" placeholder="금액입력" />';
		strHtml += '                                <span class="text">(원)</span>';
		strHtml += '                            </div>';
		strHtml += '                        </div>';
		strHtml += '                        <div class="space">&nbsp;</div>';
		strHtml += '                        <div class="col-8">';
		strHtml += '                            <input type="hidden" name="passageMoneyImg" />';
		strHtml += '                            <input type="file" />';
		strHtml += '                        </div>';
		strHtml += '                    </div>';
		strHtml += '                </td>';
		strHtml += '            </tr>';
		strHtml += '            <tr>';
		strHtml += '                <th scope="row">주차</th>';
		strHtml += '                <td>';
		strHtml += '                    <div class="division">';
		strHtml += '                        <div class="col-4">';
		strHtml += '                            <div class="text-end">';
		strHtml += '                                <input type="text" name="parking" class="numberOnly cost" placeholder="금액입력" />';
		strHtml += '                                <span class="text">(원)</span>';
		strHtml += '                            </div>';
		strHtml += '                        </div>';
		strHtml += '                        <div class="space">&nbsp;</div>';
		strHtml += '                        <div class="col-8">';
		strHtml += '                            <input type="hidden" name="parkingImg" />';
		strHtml += '                            <input type="file" />';
		strHtml += '                        </div>';
		strHtml += '                    </div>';
		strHtml += '                </td>';
		strHtml += '            </tr>';
		strHtml += '            <tr>';
		strHtml += '                <th scope="row">세차</th>';
		strHtml += '                <td>';
		strHtml += '                    <div class="division">';
		strHtml += '                        <div class="col-4">';
		strHtml += '                            <div class="text-end">';
		strHtml += '                                <input type="text" name="carWash" class="numberOnly cost" placeholder="금액입력" />';
		strHtml += '                                <span class="text">(원)</span>';
		strHtml += '                            </div>';
		strHtml += '                        </div>';
		strHtml += '                        <div class="space">&nbsp;</div>';
		strHtml += '                        <div class="col-8">';
		strHtml += '                            <input type="hidden" name="carWashImg" />';
		strHtml += '                            <input type="file" />';
		strHtml += '                        </div>';
		strHtml += '                    </div>';
		strHtml += '                </td>';
		strHtml += '            </tr>';
		strHtml += '            <tr>';
		strHtml += '                <th scope="row">용품</th>';
		strHtml += '                <td>';
		strHtml += '                    <div class="division">';
		strHtml += '                        <div class="col-4">';
		strHtml += '                            <div class="text-end">';
		strHtml += '                                <input type="text" name="accessories" class="numberOnly cost" placeholder="금액입력" />';
		strHtml += '                                <span class="text">(원)</span>';
		strHtml += '                            </div>';
		strHtml += '                        </div>';
		strHtml += '                        <div class="space">&nbsp;</div>';
		strHtml += '                        <div class="col-8">';
		strHtml += '                            <input type="hidden" name="accessoriesImg" />';
		strHtml += '                            <input type="file" />';
		strHtml += '                        </div>';
		strHtml += '                    </div>';
		strHtml += '                </td>';
		strHtml += '            </tr>';
		strHtml += '            <tr>';
		strHtml += '                <th scope="row">렌탈료</th>';
		strHtml += '                <td>';
		strHtml += '                    <div class="division">';
		strHtml += '                        <div class="col-4">';
		strHtml += '                            <div class="text-end">';
		strHtml += '                                <input type="text" name="rental" class="numberOnly cost" placeholder="금액입력" />';
		strHtml += '                                <span class="text">(원)</span>';
		strHtml += '                            </div>';
		strHtml += '                        </div>';
		strHtml += '                        <div class="space">&nbsp;</div>';
		strHtml += '                        <div class="col-8">';
		strHtml += '                            <input type="hidden" name="rentalImg" />';
		strHtml += '                            <input type="file" />';
		strHtml += '                        </div>';
		strHtml += '                    </div>';
		strHtml += '                </td>';
		strHtml += '            </tr>';
		strHtml += '            <tr>';
		strHtml += '                <th scope="row">보험</th>';
		strHtml += '                <td>';
		strHtml += '                    <div class="division">';
		strHtml += '                        <div class="col-4">';
		strHtml += '                            <div class="text-end">';
		strHtml += '                                <input type="text" name="insurance" class="numberOnly cost" placeholder="금액입력" />';
		strHtml += '                                <span class="text">(원)</span>';
		strHtml += '                            </div>';
		strHtml += '                        </div>';
		strHtml += '                        <div class="space">&nbsp;</div>';
		strHtml += '                        <div class="col-8">';
		strHtml += '                            <input type="hidden" name="insuranceImg" />';
		strHtml += '                            <input type="file" />';
		strHtml += '                        </div>';
		strHtml += '                    </div>';
		strHtml += '                </td>';
		strHtml += '            </tr>';
		strHtml += '            <tr>';
		strHtml += '                <th scope="row">기타</th>';
		strHtml += '                <td>';
		strHtml += '                    <div class="division">';
		strHtml += '                        <div class="col-4">';
		strHtml += '                            <div class="text-end">';
		strHtml += '                                <input type="text" name="etc" class="numberOnly cost" placeholder="금액입력" />';
		strHtml += '                                <span class="text">(원)</span>';
		strHtml += '                            </div>';
		strHtml += '                        </div>';
		strHtml += '                        <div class="space">&nbsp;</div>';
		strHtml += '                        <div class="col-8">';
		strHtml += '                            <input type="hidden" name="etcImg" />';
		strHtml += '                            <input type="file" />';
		strHtml += '                        </div>';
		strHtml += '                    </div>';
		strHtml += '                </td>';
		strHtml += '            </tr>';
		strHtml += '            <tr>';
		strHtml += '                <th scope="row">메모</th>';
		strHtml += '                <td><textarea rows="5" class="w100" placeholder="메모" name="memo"></textarea></td>';
		strHtml += '            </tr>';
		strHtml += '        </tbody>';
		strHtml += '    </table>';
		strHtml += '</li>';
		return strHtml;
	};
	$('#addBtn').on('click',function(){
		$('#expensesList').append(createBox());
		
		$('.datePicker').datepicker({
			 showButtonPanel: true // 하단 today, done  버튼기능 추가 표시 (기본은 false)		 	
		});
		
		var li = $('#expensesList li').last();
		
		li.find('.vehiclePopBtn').data({
			type : "vehicle",
			http_post_option : {
				header : {
					key : "${_KEY}"
				}
			},
			selType : "radio",
			action : {
				ok : function(arrSelect, inputObj, valueObj, _this) {
					var info = base64ToJsonObj($(arrSelect).data("info"));
					$(inputObj).val(info.plateNum);
					$(valueObj).val(info.vehicleKey);
				}
			},
			targetInput : li.find('input[name="plateNum"]'),
			targetInputValue : li.find('input[name="vehicleKey"]')
		}).commonPop();
	});
	
	$(document).on('focusout','.cost',function(){
		
		var arrNormalTotal = 0;
		var oilPriceTotal = 0;
		var passageMoneyTotal = 0;
		var parkingTotal = 0;
		var carWashTotal = 0;
		var accessoriesTotal = 0;
		var rentalTotal = 0;
		var insuranceTotal = 0;
		var etcTotal = 0;
		var arrNormal = [];
		
		$('#expensesList li').each(function(index){
			var obj = $(this);
			
			var oilPrice = obj.find('input[name="oilPrice"]').val();
			var passageMoney = obj.find('input[name="passageMoney"]').val()
			var parking = obj.find('input[name="parking"]').val()
			var carWash = obj.find('input[name="carWash"]').val()
			var accessories = obj.find('input[name="accessories"]').val()
			var rental = obj.find('input[name="rental"]').val()
			var insurance = obj.find('input[name="insurance"]').val()
			var etc = obj.find('input[name="etc"]').val()
			
			if(!isNaN(oilPrice) && oilPrice != ""){
				oilPriceTotal += parseInt(oilPrice)
				arrNormalTotal += parseInt(oilPrice);
			}
			if(!isNaN(passageMoney) && passageMoney != ""){
				passageMoneyTotal += parseInt(passageMoney)
				arrNormalTotal += parseInt(passageMoney);
			}
			if(!isNaN(parking) && parking != ""){
				parkingTotal += parseInt(parking)
				arrNormalTotal += parseInt(parking);
			}
			if(!isNaN(carWash) && carWash != ""){
				carWashTotal += parseInt(carWash)
				arrNormalTotal += parseInt(carWash);
			}
			if(!isNaN(accessories) && accessories != ""){
				accessoriesTotal += parseInt(accessories)
				arrNormalTotal += parseInt(accessories);
			}
			if(!isNaN(rental) && rental != ""){
				rentalTotal += parseInt(rental)
				arrNormalTotal += parseInt(rental);
			}
			if(!isNaN(insurance) && insurance != ""){
				insuranceTotal += parseInt(insurance)
				arrNormalTotal += parseInt(insurance);
			}
			if(!isNaN(etc) && etc != ""){
				etcTotal += parseInt(etc)
				arrNormalTotal += parseInt(etc);
			}
		});
		
		if(oilPriceTotal > 0) arrNormal.push("유류대" + " " + number_format(oilPriceTotal) + "원");
		if(passageMoneyTotal > 0) arrNormal.push("통행료" + " " + number_format(passageMoneyTotal) + "원");
		if(parkingTotal > 0) arrNormal.push("주차" + " " + number_format(parkingTotal) + "원");
		if(carWashTotal > 0) arrNormal.push("세차" + " " + number_format(carWashTotal) + "원");
		if(accessoriesTotal > 0) arrNormal.push("용품" + " " + number_format(accessoriesTotal) + "원");
		if(rentalTotal > 0) arrNormal.push("렌탈료" + " " + number_format(rentalTotal) + "원");
		if(insuranceTotal > 0) arrNormal.push("보험" + " " + number_format(insuranceTotal) + "원");
		if(etcTotal > 0) arrNormal.push("기타" + " " + number_format(etcTotal) + "원");
		
		$('#totalPrice1').html(arrNormal.join(" + "));
		$('#totalPrice3').html(number_format(arrNormalTotal));
		
	});
	
	$(document).on('change','input[type="file"]',function(){
		$(this).prev().addClass('targetInput');

		var formData = new FormData(); 
		formData.append("uploadFile",$(this)[0].files[0]);
		formData.append("uploadType","expenses");
		
		$.ajax({ url: '${defaultPath}/api/1/fileUpload', 
			data: formData, 
			headers : {
				"key" : "${_KEY}"
			},
			processData: false, 
			contentType: false, 
			type: 'POST', 
			success: function(res){ 
				$('.targetInput').val(res.result.uploadFile);
				$('.targetInput').removeClass('targetInput');
			} 
		});
	});
	
	$('#expensesRegBtn').on('click',function(){
		
			var sendData = [];
			
			var valid = true;
			$('#expensesList li').each(function(index){
				var data = {};
				var obj = $(this);
				var expensesDate = $(this).find('input[name="expensesDate"]').val();
				var vehicleKey = $(this).find('input[name="vehicleKey"]').val();
				
				if(expensesDate == ""){
					alert("경비 일자를 입력하지 않으셨습니다.");
					valid = false;
					return;
				}
				if(vehicleKey == ""){
					alert("차량을 선택하지 않으셨습니다.");
					valid = false;
					return;
				}
				
				var unixDate = new Date(expensesDate).getTime();
				
				data.expensesDate = unixDate;
				data.vehicleKey = vehicleKey;
				
				if(obj.find('input[name="oilPrice"]').val() == ""){
					data.oilPrice = 0;
				}else{
					data.oilPrice = obj.find('input[name="oilPrice"]').val();
				}
				
				if(obj.find('input[name="oilLiquid"]').val() == ""){
					data.oilLiquid = 0;
				}else{
					data.oilLiquid = obj.find('input[name="oilLiquid"]').val();
				}
				
				if(obj.find('input[name="oilImg"]').val() != ""){
					data.oilImg = obj.find('input[name="oilImg"]').val();	
				}else{
					data.oilImg = null;
				}
				
				
				if(obj.find('input[name="passageMoney"]').val() == ""){
					data.passageMoney = 0;
				}else{
					data.passageMoney = obj.find('input[name="passageMoney"]').val();
				}
				
				if(obj.find('input[name="passageMoneyImg"]').val() != ""){
					data.passageMoneyImg = obj.find('input[name="passageMoneyImg"]').val();	
				}else{
					data.passageMoneyImg = null;
				}
				
				if(obj.find('input[name="parking"]').val() == ""){
					data.parking = 0;
				}else{
					data.parking = obj.find('input[name="parking"]').val();
				}
				
				if(obj.find('input[name="parkingImg"]').val() != ""){
					data.parkingImg = obj.find('input[name="parkingImg"]').val();	
				}else{
					data.parkingImg = null;
				}
				
				
				if(obj.find('input[name="carWash"]').val() == ""){
					data.carWash = 0;
				}else{
					data.carWash = obj.find('input[name="carWash"]').val();
				}
				
				if(obj.find('input[name="carWashImg"]').val() != ""){
					data.carWashImg = obj.find('input[name="carWashImg"]').val();
				}else{
					data.carWashImg = null;
				}
				
				if(obj.find('input[name="accessories"]').val() == ""){
					data.accessories = 0;
				}else{
					data.accessories = obj.find('input[name="accessories"]').val();
				}
				
				if(obj.find('input[name="accessoriesImg"]').val() != ""){
					data.accessoriesImg = obj.find('input[name="accessoriesImg"]').val();
				}else{
					data.accessoriesImg = null;
				}
				
				
				if(obj.find('input[name="rental"]').val() == ""){
					data.rental = 0;
				}else{
					data.rental = obj.find('input[name="rental"]').val();
				}
				
				if(obj.find('input[name="rentalImg"]').val() != ""){
					data.rentalImg = obj.find('input[name="rentalImg"]').val();
				}else{
					data.rentalImg = null;
				}
				
				
				if(obj.find('input[name="insurance"]').val() == ""){
					data.insurance = 0;
				}else{
					data.insurance = obj.find('input[name="insurance"]').val();
				}
				
				if(obj.find('input[name="insuranceImg"]').val() != ""){
					data.insuranceImg = obj.find('input[name="insuranceImg"]').val();
				}else{
					data.insuranceImg = null;
				}
				
				
				if(obj.find('input[name="etc"]').val() == ""){
					data.etc = 0;
				}else{
					data.etc = obj.find('input[name="etc"]').val();
				}
				
				if(obj.find('input[name="etcImg"]').val() != ""){
					data.etcImg = obj.find('input[name="etcImg"]').val();
				}else{
					data.etcImg = null;
				}
				
				if(obj.find('input[name="memo"]').val() != ""){
					data.memo = obj.find('input[name="memo"]').val();
				}else{
					data.memo = null;
				}
				
				//type이 1이면
				data.type = "1";
				
				sendData.push(data);
			});
			
			if(valid){
				$V4.http_post("/api/1/expenses", sendData, {
					requestMethod : "POST",
					header : {
						"key" : "${_KEY}"
					},
					success : function(rtv) {
						console.log(rtv);
					},
					error : function(t) {
						console.log(t);
					}
			 });
			}
		
	});
	
	$('#addBtn').trigger('click');
});
</script>
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>차량경비 조회</h2>
                    <span>등록차량의 모든 경비내역을 관리합니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="vehicle-page">
                    <div class="box-layout register-vehicle-cost">
                        <div class="title">
                            <h3 class="tit3">차량 경비 개별 등록</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
                       		<ul id="expensesList">
                       			
                       		</ul>
                            <div class="btn-add">
                                <button type="button" id="addBtn">
									<span>추가</span>
								</button>
                            </div>

                            <div class="total-price">
                                <div class="price">
                                    <span>일반경비 합계</span>
                                    <strong id="totalPrice1"></strong>
                                    
                                </div>

                                <div class="price">
                                    <span>총 합계</span>
                                    <strong id="totalPrice3">0</strong>원
                                </div>
                            </div>

                            <!-- 하단 버튼 -->
                            <div class="btn-bottom">
                                <button type="button" class="btn btn02" id="cancel">취소</button>
                                <button type="button" class="btn btn03" id="expensesRegBtn">등록</button>
                            </div>
                            <!--/ 하단 버튼 -->
                        </div>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->