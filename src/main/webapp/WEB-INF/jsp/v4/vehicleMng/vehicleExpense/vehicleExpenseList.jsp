<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
.monthDate { height:25px; border:1px solid #c0c0c0; padding:0 30px 0 10px; background:#FFF url('./common/images/cal2.jpg') no-repeat right center/28px 25px; font-size:12px; text-align:left;}s
</style>
<script type="text/javascript">
$(function(){
	
	$('#searchMonth').monthpicker({
		years: getMonthPickerYear(5),
		topOffset: 6
	});
	
	$('#searchMonth').val(new Date().yyyymm());
	
	var initPrice = function(){
		$V4.http_post("/api/1/expensesTotal/"+new Date($('#searchMonth').val()).getTime() / 1000, {}, {
			requestMethod : "GET",
			header : {
				"key" : "${_KEY}"
			},
			success : function(rtv) {
				var obj = rtv.result;
				
				$('#accessories').text(number_format(obj.accessories));
				$('#carWash').text(number_format(obj.carWash));
				$('#etc').text(number_format(obj.etc));
				$('#oilLiquid').text(number_format(obj.oilLiquid));
				$('#oilPrice').text(number_format(obj.oilPrice));
				$('#parking').text(number_format(obj.parking));
				$('#passageMoney').text(number_format(obj.passageMoney));
				$('#rental').text(number_format(obj.rental));
				$('#totalPrice').text(number_format(obj.totalPrice));
				$('#insurance').text(number_format(obj.insurance));
				
			},
			error : function(t) {
				alert('에러 관리자에게 문의 바랍니다.')
			}
	 	});
	}

	initPrice();
	
	$("#top_btn_search").on("click",$.debounce(200,1,function(){
		initPrice();
		
		var param = {searchType:$("#top_searchType").val() , searchText : $("#top_searchText").val(),expensesDate : new Date($('#searchMonth').val()).getTime() / 1000};
		
		var list = $(".tab-menu li.on").hasClass("list01")?"list01":$(".tab-menu li.on").hasClass("list02")?"list02":"";
		
		if(list){
			var limit = parseInt($("."+list+" .func_limitChange").val());	 
			param.searchOrder = $("."+list+" .func_order").val();
			
			/* $("."+list+" .searchStart").val()&&(param.searchStartDate = $("."+list+" .searchStart").val());
			$("."+list+" .searchEnd").val()&&(param.searchEndDate = $("."+list+" .searchEnd").val()); */
			
			if(list == "list01"){
				$('#expensesApprovalList').commonList("setParam",param);
				$('#expensesApprovalList').commonList("setLimit",limit).search();
			}else if(list =="list02"){
				$('#expensesList').commonList2("setParam",param);
				$('#expensesList').commonList2("setLimit",limit).search();
			}
		}
		
	}));
	
    $("#register-cost-dialog").dialog({
        autoOpen: false,
        show: {
            duration: 500
        },
        width: '960',
        modal: true
    });

    $("#register-cost-open").on("click", function() {
        $("#register-cost-dialog").dialog("open");
    });


    // 탭메뉴
   /*  $(".tab-menu>li>a").click(function() {
        $(this).parent().addClass("on").siblings().removeClass("on");
        return false;
    }); */
    
    $('#expenseRegBtn').on('click',function(){
    	$V4.move('/vehicleMng/vehicleExpense/reg');
    });
    
    
    $('#expensesBulkConfirm').on('click',function(){
    	var header = ['expensesDate',
    	              'plateNum',
    	              'oilPrice',
    	              'oilLiquid',
    	              'passageMoney',
    	              'parking',
    	              'carWash',
    	              'accessories',
    	              'rental',
    	              'insurance',
    	              'etc',
    	              'memo'];
    	
    	
    	generatorExcel($('#file'),header, true, function(data) {
			$V4.http_post("/api/1/expensesBulk", data, {
				requestMethod : "POST",
				header : {
					"key" : "${_KEY}"
				},
				success : function(rtv) {
					console.log(rtv);
				},
				error : function(t) {
					alert('에러 관리자에게 문의 바랍니다.')
				}
		 	});
		});
    });
    
    
    $('.tab-menu li a').on('click',function(){
		var tab = $(this).data("tab");
		
		$('.tab-menu li').removeClass("on");
		$('.tab-menu li.'+tab).addClass("on");
		
		$("#top_btn_search").trigger('click');
	});
   
    var thead = [
                 {"상태" : "15%"},
                 {"차량정보" : "15%"},
                 {"사용자" : "10%"},
                 {"일시" : "10%"},
                 {"항목" : "15%"},
                 {"합계금액" : "10%"},
                 {"메모" : "15%"},
                 {"상세" : "10%"},
     ];
    
    var approvalSetting = {
    		thead : thead
    		,bodyHtml : (function(data){
    			var strHtml = "";
    			for(i in data.result){
    				var obj = data.result[i];
    				strHtml += '<tr>';
    				
    				if(getProperty(obj,'corp.approval.expensesApproval') == "1"){
    					strHtml += '    <td>';
    					var state = getProperty(obj,'approval.state');
    					var maxStep = getProperty(obj,'approval.maxStep');
    					
    					if(state != maxStep && state == "0"){
    						strHtml += '        <div class="states-value state07">결재요청</div><br />';	
    					}else if(state != maxStep && state == "1"){
    						strHtml += '        <div class="states-value state06">1차승인</div><br />';	
    					}else if(state == maxStep){
    						strHtml += '        <div class="states-value state02">결재완료</div><br />';	
    					}else if(state == "-1"){
    						strHtml += '        <div class="states-value state08">반려</div><br />';	
    					}
        				//결재 취소가 필요없는거 같다 일단 주석
        				//strHtml += '        <button type="button" class="states-value state03">취소</button>';
        				strHtml += '    </td>';
    				}
    				
    				strHtml += '    <td>';
    				strHtml += '        <div class="car-img">';
    				strHtml += '            <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" alt="" /></span><br />';
    				strHtml += '            <span class="num">'+getProperty(obj,'vehicle.vehicleModel.modelMaster')+' '+getProperty(obj,'vehicle.plateNum')+'</span>';
    				strHtml += '        </div>';
    				strHtml += '    </td>';
    				strHtml += '    <td>'+getProperty(obj,'account.corpPosition')+'<br />'+getProperty(obj,'account.name')+'</td>';
    				strHtml += '    <td>'+convertDateUint(new Date(obj.expensesDate),_unitDate,_timezoneOffset,1)+'</td>';
    				strHtml += '    <td>유류비 외 3건</td>';
    				strHtml += '    <td>'+number_format(obj.totalSum)+'원</td>';
    				strHtml += '    <td class="left">'+convertNullString(getProperty(obj,'memo'),'-')+'</td>';
    				strHtml += '    <td><button type="button" class="btn-img change expensesDetail" data-expenseskey='+obj.expensesKey+'>상세보기</button></td>';
    				strHtml += '</tr>';
    			}
    			return strHtml;
    		})
    		,limit : parseInt($(".list01 .func_limitChange").val())
    		,pagePerGroup : 10 //pagingGroup size
    		,url : "/api/1/expenses"
    		,param : {
    					'searchOrder' : $(".list01 .func_order").val(),
    					'expensesDate' : new Date($('#searchMonth').val()).getTime() / 1000
    				 }
    		,http_post_option : {
    			requestMethod : "GET"
    			,header : {key : "${_KEY}"}
    		}
    		,initSearch : true //생성과 동시에 search		
    		,loadingBodyViewer : true //로딩중!! 표시됨
    		,debugMode : true
    	}
    
    var thead2 = [
                  {"작성구분":"10%"},
                  {"차량정보":"15%"},
                  {"사용자":"10%"},
                  {"일시":"10%"},
                  {"항목":"20%"},
                  {"합계금액":"10%"},
                  {"메모":"15%"},
                  {"상세":"10%"}
                  ];
    
    var expensesSetting = {
    		thead : thead
    		,bodyHtml : (function(data){
    			var strHtml = "";
    			for(i in data.result){
    				var obj = data.result[i];
    				
    				
    				
    				strHtml += '<tr>';
					if(obj.type == "1"){
						strHtml += '    <td>웹</td>';	
    				}else{
    					strHtml += '    <td>모바일</td>';
    				}
    				
					strHtml += '    <td>';
    				strHtml += '        <div class="car-img">';
    				strHtml += '            <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" alt="" /></span><br />';
    				strHtml += '            <span class="num">'+getProperty(obj,'vehicle.vehicleModel.modelMaster')+' '+getProperty(obj,'vehicle.plateNum')+'</span>';
    				strHtml += '        </div>';
    				strHtml += '    </td>';
    				strHtml += '    <td>'+getProperty(obj,'account.corpPosition')+'<br />'+getProperty(obj,'account.name')+'</td>';
    				strHtml += '    <td>'+convertDateUint(new Date(obj.expensesDate),_unitDate,_timezoneOffset,1)+'</td>';
    				strHtml += '    <td>유류비 외 3건</td>';
    				strHtml += '    <td>'+number_format(obj.totalSum)+'원</td>';
    				strHtml += '    <td class="left">'+convertNullString(getProperty(obj,'memo'),'')+'</td>';
    				strHtml += '    <td><button type="button" class="btn-img change expensesDetail" data-expenseskey='+obj.expensesKey+'>상세보기</button></td>';
    				strHtml += '</tr>';
    			}
    			return strHtml;
    		})
    		,limit : parseInt($(".list02 .func_limitChange").val())
    		,pagePerGroup : 10 //pagingGroup size
    		,url : "/api/1/expenses"
    		,param : {
    					'searchOrder' : $(".list02 .func_order").val(),
    					'expensesDate' : new Date($('#searchMonth').val()).getTime() / 1000
    				 }
    		,http_post_option : {
    			requestMethod : "GET"
    			,header : {key : "${_KEY}"}
    		}
    		,initSearch : true //생성과 동시에 search		
    		,loadingBodyViewer : true //로딩중!! 표시됨
    		,debugMode : true
    	}
    
    
    
    $('#expensesApprovalList').commonList(approvalSetting);
    $('#expensesList').commonList2(expensesSetting);
    
    $(".func_order").on("change",function(){
		$("#top_btn_search").trigger("click");
	});
	
	$(".func_limitChange").on("change",function(){
		$("#top_btn_search").trigger("click");
	});
	
	$("#top_searchText").on("keyup",function(e){
		if(e.keyCode == 13) {
			$("#top_btn_search").trigger("click");
		}
	});
	
	$(document).on('click','.expensesDetail',function(){
		var key = $(this).data('expenseskey');
		
	});
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>차량경비 조회</h2>
                    <span>등록차량의 모든 경비내역을 관리합니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="vehicle-page">
                    <div class="vehicle-cost">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <!-- 거래처등록 -->
                        <h3 class="tit2 mgt30">경비 등록</h3>
                        <div class="box-register">
                            새로운 경비를 등록하시려면 우측 경비등록 버튼을 눌러 결재신청하세요.<br /> 1차 승인 : 차량담당자 (상신자 소속 부서장)<br /> 2차 승인 : 차량관리자 (인사, 총무소속 부서장)<br />

                            <div class="right">
                                <button type="button" class="btn btn01" id="expenseRegBtn">개별 등록</button>
                                <button type="button" class="btn btn02" id="register-cost-open">대량 등록</button>
                            </div>
                        </div>
                        <!--/ 거래처등록 -->

                        <!-- 상단 검색 -->
                        <h3 class="tit2 mgt30">경비 신청 조회ㆍ수정</h3>
                        <div class="top-search mgt20 monthAppend">
                            <input type="text" name="" class="monthDate" id="searchMonth" style="min-width:10.714rem" autocomplete="off"/>
                            <select id="top_searchType">
								<option value="">선택</option>
								<option value="plateNum">차량번호</option>
								<option value="name">사용자명</option>
							</select>
                            <input type="text" id="top_searchText" style="min-width: 250px;">
                            <button type="button" class="btn btn03" id="top_btn_search">조회</button>
                        </div>
                        <!--/ 상단 검색 -->

                        <div class="cost-list">
                            <h4 class="tit2">전체 일반 차량</h4>
                            <div class="btn-function">
                                <button class="excel">엑셀 다운로드</button>
                            </div>
                            <table class="table mgt20">
                                <caption>경비 상세항목</caption>
                                <colgroup>
                                    <col style="width:14%" />
                                    <col style="width:36%" />
                                    <col style="width:14%" />
                                    <col style="width:36%" />
                                </colgroup>
                                <tfoot>
                                    <tr>
                                        <th scope="row">총액</th>
                                        <td colspan="3"><span id="totalPrice">0</span>원</td>
                                    </tr>
                                </tfoot>

                                <tbody>
                                    <tr>
                                        <th scope="row">유류대</th>
                                        <td><span id="oilPrice">0</span>원</td>
                                        <th scope="row">세차</th>
                                        <td><span id="carWash">0</span>원</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">리터</th>
                                        <td><span id="oilLiquid">0</span>ℓ</td>
                                        <th scope="row">용품</th>
                                        <td><span id="accessories">0</span>원</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">통행료</th>
                                        <td><span id="passageMoney">0</span>원</td>
                                        <th scope="row">렌탈료</th>
                                        <td><span id="rental">0</span>원</td>
                                    </tr>
                                    <tr>
                                    	<th scope="row">주차</th>
                                        <td><span id="parking">0</span>원</td>
                                        <th scope="row">보험</th>
                                        <td><span id="insurance">0</span>원</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">기타</th>
                                        <td><span id="etc">0</span>원</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <ul class="page-tab tab-menu col-4">
                            <!-- 경비결재 -->
                            <li class="list01 on">
                                <a href="javascript:void(0);" data-tab="list01">경비결재</a>
                                <div class="tab-list">
                                    <!-- table 버튼 -->
                                    <div class="btn-function">
                                        <div class="left">
                                            <select class="func_order">
												<option value="regDate">최근 등록순</option>
											</select>
                                        </div>
                                        <div class="right">
                                           <select class="func_limitChange">
												<option value="5" selected>5건씩 보기</option>
												<option value="10">10건씩 보기</option>
												<option value="20">20건씩 보기</option>
												<option value="50">20건씩 보기</option>
											</select>
                                        </div>
                                    </div>
                                    <!--/ table 버튼 -->

                                    <table class="table list mgt20" id="expensesApprovalList">
                                    </table>
                                </div>
                            </li>
                            <!--/ 경비결재 -->

                            <!-- 지출내역 -->
                            <li class="list02">
                                <a href="javascript:void(0)" data-tab="list02">지출 내역</a>
                                <div class="tab-list">
                                    <!-- table 버튼 -->
                                    <div class="btn-function row-2">
                                        <div class="left">
                                            <select name="">
													<option value="">최근 등록순</option>
												</select>
                                        </div>
                                        <div class="right">
                                            <button type="button" class="excel">엑셀 다운로드</button>
                                            <select class="func_limitChange">
												<option value="5" selected>5건씩 보기</option>
												<option value="10">10건씩 보기</option>
												<option value="20">20건씩 보기</option>
												<option value="50">20건씩 보기</option>
											</select>
                                        </div>
                                    </div>
                                    <!--/ table 버튼 -->

                                    <table class="table list mgt20" id="expensesList">
                                        
                                    </table>
                                </div>
                            </li>
                            <!--/ 지출내역 -->
                        </ul>

                        <!-- 안내 -->
                        <div class="box-info">
                            <h3 class="hidden">안내</h3>
                            <h4 class="mgt0">경비 결재 표기 안내</h4>
                            <ul>
                                <li><strong>결재요청</strong>사용자가 차량경비를 등록한 상태로 상신자 소속부서장에게 통보되었습니다.</li>
                                <li><strong>1차 승인</strong>사용자 소속부서장의 승인이 완료되었습니다.</li>
                                <li><strong>2차 승인</strong>차량관리자(인사, 총무부서)의 승인이 완료되었습니다.</li>
                                <li><strong>반려</strong>경비 결재 요청이 반려된 상태입니다.</li>
                                <li><strong>취소</strong>사용자(신청자)가 결재요청을 취소한 상태입니다. 1차 승인 전까지 사용자는 취소할 수 있습니다.</li>
                            </ul>
                        </div>
                        <!--/ 안내 -->
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->
        
          <!-- 경비 대량등록 팝업 -->
    <div id="register-cost-dialog" title="경비 대량등록">
        <h3 class="tit">경비 대량등록</h3>
        <table class="table mgt20">
            <caption>경비 대량등록</caption>
            <tbody>
                <tr>
                    <th scope="row">대랑등록양식</th>
                    <td>
                        <div class="btn-function mgt0"><button class="download">경비 등록양식</button></div>
                        <div class="mgt5">경비등록 양식을 다운로드 받아 해당양식에 맞게 기재하신 후 아래에서 업로드하시면 즉시 등록됩니다.</div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">대량등록 업로드</th>
                    <td>
                    	<input type="file" id="file" />
                    </td>
                </tr>
            </tbody>
        </table>
        <!-- 하단 버튼 -->
        <div class="btn-bottom">
            <button type="button" class="btn btn03" id="expensesBulkConfirm">확인</button>
        </div>
        <!--/ 하단 버튼 -->
    </div>
    <!--/ 경비 대량등록 팝업 -->