<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	$("#map-dialog").dialog({
        autoOpen: false,
        show: {
            duration: 500
        },
        width: '960',
        modal: true
    });

    $("#map-open").on("click", function() {
        $("#map-dialog").dialog("open");
    });
    
    $(document).on('click','.rentCarDetailBtn',function(){
    	$V4.move('/vehicleMng/rentCarMng/detail');
    });
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>렌트카 관리</h2>
                    <span>렌트카, 순회정비 업체 등 많은 차량을 관리하는 회사 편의제공을 위해 구성, 제공합니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="vehicle-page">
                    <div class="">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                            <div class="item">
                                <span>
									등록 차량수
									<strong>10</strong>대
								</span>
                            </div>

                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <!-- 상단 검색 -->
                        <div class="top-search mgt20">
                            <select name="">
								<option value="">전체</option>
							</select>
                            <input type="text" name="" />
                            <button class="btn btn03">조회</button>
                        </div>
                        <!--/ 상단 검색 -->


                        <!-- table 버튼 -->
                        <div class="btn-function">
                            <div class="right">
                                <select name="">
										<option value="">5건씩 보기</option>
									</select>
                            </div>
                        </div>
                        <!--/ table 버튼 -->

                        <table class="table list mgt20">
                            <caption>등록된 서비스 리스트</caption>
                            <colgroup>
                                <col style="width:15%" />
                                <col style="width:7%" />
                                <col style="width:10%" />
                                <col style="width:7%" />
                                <col style="width:10%" />
                                <col style="width:17%" />
                                <col style="width:17%" />
                                <col style="width:5%" />
                                <col style="width:5%" />
                                <col style="width:7%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="col">일시</th>
                                    <th scope="col">상태</th>
                                    <th scope="col" class="order"><a href="javascript:;">차량번호</a></th>
                                    <th scope="col" class="order"><a href="javascript:;">사용자명</a></th>
                                    <th scope="col" class="order"><a href="javascript:;">거리</a></th>
                                    <th scope="col">출발지</th>
                                    <th scope="col">도착지</th>
                                    <th scope="col">경로</th>
                                    <th scope="col">위치</th>
                                    <th scope="col">내역(월)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>2018.05.09 17:00</td>
                                    <td>운행중</td>
                                    <td>33호 2833</td>
                                    <td>홍길동</td>
                                    <td>19km</td>
                                    <td class="left">서울 영등포구</td>
                                    <td class="left">서울 서대문구</td>
                                    <td><button class="btn-img location" id="map-open">경로보기</button></td>
                                    <td><button class="btn-img search">위치보기</button></td>
                                    <td><button type="button" class="btn-img change rentCarDetailBtn">내역 보기</button></td>
                                </tr>
                            </tbody>
                        </table>

                        <!-- 페이징 -->
                        <div id="paging">
                            <a href="" class="first icon-spr">제일 처음으로</a>
                            <a href="" class="prev icon-spr">이전으로</a>
                            <span class="current">1</span>
                            <a href="" class="num">2</a>
                            <a href="" class="num">3</a>
                            <a href="" class="num">4</a>
                            <a href="" class="num">5</a>
                            <a href="" class="next icon-spr">다음으로</a>
                            <a href="" class="last icon-spr">제일 마지막으로</a>
                        </div>
                        <!--/ 페이징 -->
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->
        <!-- 지도 팝업 -->
    <div id="map-dialog" title="지도 팝업">


    </div>
    <!--/ 지도 팝업 -->