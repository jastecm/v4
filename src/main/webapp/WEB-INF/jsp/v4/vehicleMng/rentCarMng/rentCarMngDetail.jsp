<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	 $("#map-dialog").dialog({
         autoOpen: false,
         show: {
             duration: 500
         },
         width: '960',
         modal: true
     });

     $("#map-open").on("click", function() {
         $("#map-dialog").dialog("open");
     });
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>렌트카 관리</h2>
                    <span>렌트카, 순회정비 업체 등 많은 차량을 관리하는 회사 편의제공을 위해 구성, 제공합니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="vehicle-page">
                    <div class="box-layout">
                        <div class="title">
                            <h3 class="tit3">렌트카 상세 운행내역</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
                            <!-- table 버튼 -->
                            <div class="btn-function mgt0">
                                <div class="left">
                                    <select name="">
										<option value="">2018년</option>
									</select>
                                    <select name="">
										<option value="">3월</option>
									</select>
                                </div>
                                <div class="right">
                                    <button class="excel">엑셀 다운로드</button>
                                    <select name="">
										<option value="">5건씩 보기</option>
									</select>
                                </div>
                            </div>
                            <!--/ table 버튼 -->

                            <table class="table list mgt20">
                                <caption>등록된 서비스 리스트</caption>
                                <colgroup>
                                    <col style="width:15%" />
                                    <col style="width:10%" />
                                    <col style="width:9%" />
                                    <col style="width:10%" />
                                    <col style="width:23%" />
                                    <col style="width:23%" />
                                    <col style="width:10%" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th scope="col">일시</th>
                                        <th scope="col">차량번호</th>
                                        <th scope="col" class="order"><a href="javascript:;">사용자명</a></th>
                                        <th scope="col" class="order"><a href="javascript:;">거리</a></th>
                                        <th scope="col">출발지</th>
                                        <th scope="col">도착지</th>
                                        <th scope="col">경로</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>2018.05.09 17:00</td>
                                        <td>33호 2833</td>
                                        <td>홍길동</td>
                                        <td>19km</td>
                                        <td class="left">서울 영등포구</td>
                                        <td class="left">서울 서대문구</td>
                                        <td><button class="btn-img location" id="map-open">경로보기</button></td>
                                    </tr>
                                </tbody>
                            </table>

                            <!-- 페이징 -->
                            <div id="paging">
                                <a href="" class="first icon-spr">제일 처음으로</a>
                                <a href="" class="prev icon-spr">이전으로</a>
                                <span class="current">1</span>
                                <a href="" class="num">2</a>
                                <a href="" class="num">3</a>
                                <a href="" class="num">4</a>
                                <a href="" class="num">5</a>
                                <a href="" class="next icon-spr">다음으로</a>
                                <a href="" class="last icon-spr">제일 마지막으로</a>
                            </div>
                            <!--/ 페이징 -->
                        </div>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->
         <!-- 지도 팝업 -->
    <div id="map-dialog" title="지도 팝업">


    </div>
    <!--/ 지도 팝업 -->