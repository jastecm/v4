<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>




<script type="text/javascript">
$(function(){
	
	
	$("#btn_configVehicle").on("click",function(){
		$V4.move("/config/vehicleRegister/list/person");
	});
	
	$("#btn_configVehicleCorp").on("click",function(){
		$V4.move("/config/vehicleRegister/list/corp");
	});
	
	$("#btn_group").on("click",function(){
		$V4.move("/config/groupRegister");	
	});
	
	$("#btn_user").on("click",function(){
		$V4.move("/config/userRegister");	
	});
});

</script>

<section id="container">
	<!-- 메인 콘텐츠 -->
	<div id="main">
		<div class="section01 clr">
			<!-- section01 좌측영역 -->
			<div class="section-left">
				<p class="hidden">
					내 손안의 스마트 드라이빙 <strong>편리한 차량운행 관리 ViewCAR PRO VDAS</strong> 이젠 뷰카와
					함께 간편하고 쉽게 차량운행관리를 해보세요.
				</p>
				<a href="#" target="_blank"> 
				<img src="${pageContext.request.contextPath}/common/new/img/main/btn-google.gif" alt="구글플레이 다운받기" />
				</a> <a href="#" target="_blank"> 
				<img src="${pageContext.request.contextPath}/common/new/img/main/btn-apple.gif" alt="애플스토어 다운받기" />
				</a>
			</div>
			<!--/ section01 좌측영역 -->

			<!-- section01 우측영역 -->
			<div class="section-right">
				<div class="top">
					<a href="#" class="icon-spr btn01" id="btn_group">운행조회(부서)</a> <span> 
					<a href="#" id="btn_configVehicle" class="icon-spr btn02">배차현황(임시차량리스트-개인)</a> 
					<a href="#" class="icon-spr btn03" id="btn_configVehicleCorp">운행일지(임시차량리스트 - 법인)</a>
					</span>
				</div>
				<div class="bottom">
					<span> <a href="#" id="btn_user">경비조회(유저리스트)</a> <a href="#" id="btn_groupPop">차량정비현황</a> <a
						href="#" id="btn_userPop">드라이빙 분석</a> <a href="#">차량정비현황</a>
					</span>
					<div class="latest-board">
						<h2>
							<a href="">공지</a>
						</h2>
						<ul>
							<li><a href="">서버이전 및 점검 작업으로 인한 서비스 중...</a></li>
							<li><a href="">뷰카이용자대상 봄 맞이 이벤트 안내</a></li>
							<li><a href="">2.1버전 업데이트 사항 및 관련 Q&amp;A</a></li>
						</ul>
						<a href="" class="icon-spr more">더보기</a>
					</div>
				</div>
				<!--/ section01 우측영역 -->
			</div>
		</div>
	</div>
	<!--/ 메인 콘텐츠 -->
</section>


