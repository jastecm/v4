<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	$("#bir-date").datepicker({
        dateformat: 'yy-mm-dd'
    });
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>회원정보 설정</h2>
                    <span>서비스의 정상적인 이용을 위해 정확한 회원정보를 입력합니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="my-page">
                    <div class="modify-individual">
                        <h3 class="tit2">기본정보 <span class="red">(필수입력)</span></h3>
                        <div class="user-info2">
                            <div class="img">
                                <img src="${pageContext.request.contextPath}/common/new/img/common/user-img2.png" alt="" />
                                <div class="file">
                                    <input type="file" id="file" />
                                    <label for="file">사진등록</label>
                                </div>
                            </div>
                            <div class="info">
                                <table class="table mgt20">
                                    <caption>기본정보</caption>
                                    <tbody>
										<tr>
											<th scope="row">아이디</th>
											<td colspan="2">
												triplog@jastecm.com
												&nbsp;&nbsp;&nbsp;<div class="states-value state08">인증 완료</div>
											</td>
										</tr>
										<tr>
											<th scope="row">비밀번호</th>
											<td colspan="2"><button class="btn btn02 md">변경</td>
										</tr>
                                        <tr>
                                            <th scope="row">이름</th>
                                            <td colspan="2">홍길동</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">휴대폰 번호</th>
                                            <td>
                                                <div class="division">
                                                    <div class="col-3">
                                                        <select name="">
																<option value="">+82</option>
															</select>
                                                    </div>
                                                    <div class="space">&nbsp;</div>
                                                    <div class="col-8">
                                                        <input type="text" name="" />
                                                    </div>
                                                    <div class="space">&nbsp;</div>
                                                    <div class="col-1">
                                                        <button class="btn btn04 md">인증</button>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="division">
                                                    <div class="col-10">
                                                        <input type="text" name="" />
                                                    </div>
                                                    <div class="space">&nbsp;</div>
                                                    <div class="col-2">
                                                        <button class="btn btn04 md">재전송</button>
                                                    </div>
                                                </div>
                                                <div class="division mgt5">
                                                    <div class="col-10 time">
                                                        <input type="text" name="" placeholder="인증번호 입력" />
                                                        <span>1:28</span>
                                                    </div>
                                                    <div class="space">&nbsp;</div>
                                                    <div class="col-2">
                                                        <button class="btn btn04 md">확인</button>
                                                    </div>
                                                </div>
                                                <div class="mgt3 red">인증번호가 일치하지 않습니다.</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="txt01" colspan="3">※ 휴대폰 인증시 휴대폰번호로 로그인하실 수 있으며, ID/비밀번호 찾기 수단으로 활용됩니다.</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <h3 class="tit2 mgt30">기본정보 (선택입력)</h3>
                        <table class="table mgt20">
                            <caption>기본정보</caption>
                            <tbody>
                                <tr>
                                    <td class="txt01" colspan="2">차량사고 발생시, 긴급SOS를 긴급연락처로 전송할 정보를 입력해주세요. <br /> 해당정보는 회원정보 수정페이지에서 변경하실 수 있습니다.
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">생년월일</th>
                                    <td><input type="text" id="bir-date" /></td>
                                </tr>
                                <tr>
                                    <th scope="row">혈액형</th>
                                    <td>
                                        <select name="">
											<option value="">RH+ A형</option>
										</select>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">성별</th>
                                    <td>
                                        <input type="radio" id="sex01" name="sex" />
                                        <label for="sex01">남자</label> &nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="radio" id="sex02" name="sex" />
                                        <label for="sex02">여자</label>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <p class="txt">서비스의 정상적 이용을 위해서는 OBD-II 단말 구매가 필요하며 <a href="">여기</a>를 누르시면 관련 구매정보를 보실 수 있습니다.</p>

                        <!-- 하단 버튼 -->
                        <div class="btn-bottom">
                            <button class="btn btn02">취소</button>
                            <button class="btn btn03">수정완료</button>
                        </div>
                        <!--/ 하단 버튼 -->
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->