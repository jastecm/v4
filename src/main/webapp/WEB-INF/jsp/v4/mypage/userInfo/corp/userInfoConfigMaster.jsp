<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>회원정보 설정</h2>
                    <span>서비스의 정상적인 이용을 위해 정확한 회원정보를 입력합니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="my-page">
                    <div class="modify-corporation">
                        <h3 class="tit2 mgt30">기본정보</h3>
                        <table class="table mgt20">
                            <caption>기본정보</caption>
                            <tbody>
                                <tr>
                                    <th scope="row">로그인 정보</th>
                                    <td class="table">
                                        <table>
                                            <caption>로그인 정보</caption>
                                            <colgroup>
                                                <col style="width:14.75%" />
                                                <col />
                                            </colgroup>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">아이디</th>
                                                    <td>
                                                        triplog@jastecm.com
                                                        <button class="btn btn04 md">변경</button>
                                                        <span class="states-value state04 md">인증 완료</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">비밀번호</th>
                                                    <td><button class="btn btn04 md">변경</button></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">기업정보
                                        <span class="necessary">(필수입력)</span>
                                    </th>
                                    <td class="table">
                                        <table>
                                            <caption>기업정보</caption>
                                            <colgroup>
                                                <col style="width:14.75%" />
                                                <col style="width:35.25%" />
                                                <col style="width:14.75%" />
                                                <col style="width:35.25%" />
                                            </colgroup>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">기업명</th>
                                                    <td>
                                                        <input type="text" name="" />
                                                    </td>
                                                    <th scope="row">사업자등록번호</th>
                                                    <td>
                                                        <div class="division">
                                                            <div class="col-4">
                                                                <input type="text" name="" maxlength="3" />
                                                            </div>
                                                            <div class="space">&nbsp;-&nbsp;</div>
                                                            <div class="col-4">
                                                                <input type="text" name="" maxlength="2" />
                                                            </div>
                                                            <div class="space">&nbsp;-&nbsp;</div>
                                                            <div class="col-4">
                                                                <input type="text" name="" maxlength="5" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">대표자명</th>
                                                    <td>
                                                        <input type="text" name="" />
                                                    </td>
                                                    <th scope="row">휴대폰 번호</th>
                                                    <td>
                                                        <div class="division">
                                                            <div class="col-3">
                                                                <select name="">
																	<option value="">+82</option>
																</select>
                                                            </div>
                                                            <div class="space">&nbsp;</div>
                                                            <div class="col-9">
                                                                <input type="text" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">대표자 이메일</th>
                                                    <td colspan="3">
                                                        <div class="division">
                                                            <div class="col-5">
                                                                <input type="text" />
                                                                <div class="red mgt3">인증메일을 발송하였습니다.</div>
                                                            </div>
                                                            <div class="space">&nbsp;</div>
                                                            <div class="col-7">
                                                                <button class="btn btn04 md">인증</button>
                                                                <button class="btn btn02 md">메일주소 인증하기</button>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">기업유형</th>
                                                    <td colspan="3">
                                                        <div class="type">
                                                            <div class="corporation">단체/사업자회원</div>
                                                            <div class="rent">렌터카</div>
                                                            <div class="partner">제휴.파트너</div>
                                                            <div class="txt">가입시 선택된 기업유형은 변경하실 수 없습니다.<br/>기업 유형을 변경코자 하시는 경우는 별도 회원가입이 필요합니다.</div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <th scope="row">부가정보 (선택입력)</th>
                                    <td class="table">
                                        <table>
                                            <caption>부가정보</caption>
                                            <colgroup>
                                                <col style="width:14.75%" />
                                                <col style="width:35.25%" />
                                                <col style="width:14.75%" />
                                                <col style="width:35.25%" />
                                            </colgroup>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">업태</th>
                                                    <td><input type="text" /></td>
                                                    <th scope="row">종목</th>
                                                    <td><input type="text" /></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

						<h3 class="tit2 mgt30">차량관리자 정보</h3>
                        <div class="user-info2">
							<div class="same"><input type="checkbox" /> 대표자와 동일</div>
                            <div class="img">
                                <img src="${pageContext.request.contextPath}/common/new/img/common/user-img2.png" alt="" />
                                <div class="file">
                                    <input type="file" id="file" />
                                    <label for="file">사진등록</label>
                                </div>
                            </div>
                            <div class="info">
                                <table class="table mgt20">
                                    <caption>기본정보</caption>
                                    <colgroup>
                                        <col style="width:14.75%" />
                                        <col style="width:35.25%" />
                                        <col style="width:14.75%" />
                                        <col style="width:35.25%" />
                                    </colgroup>
                                    <tbody>
                                        <tr>
                                            <th scope="row">관리자명</th>
                                            <td colspan="3">
                                                홍길동
                                                <button class="btn btn04 md">관리자 변경</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">휴대폰 번호</th>
                                            <td colspan="2">
                                                <div class="division">
                                                    <div class="col-3">
                                                        <select name="">
															<option value="">+82</option>
														</select>
                                                    </div>
                                                    <div class="space">&nbsp;</div>
                                                    <div class="col-8">
                                                        <input type="text" name="" />
                                                    </div>
                                                    <div class="space">&nbsp;</div>
                                                    <div class="col-1">
                                                        <button class="btn btn04 md">인증</button>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="division">
                                                    <div class="col-10">
                                                        <input type="text" name="" />
                                                    </div>
                                                    <div class="space">&nbsp;</div>
                                                    <div class="col-2">
                                                        <button class="btn btn04 md">재전송</button>
                                                    </div>
                                                </div>
                                                <div class="division mgt5">
                                                    <div class="col-10 time">
                                                        <input type="text" name="" placeholder="인증번호 입력" />
                                                        <span>1:28</span>
                                                    </div>
                                                    <div class="space">&nbsp;</div>
                                                    <div class="col-2">
                                                        <button class="btn btn04 md">확인</button>
                                                    </div>
                                                </div>
                                                <div class="mgt3 red">인증번호가 일치하지 않습니다.</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">부서명</th>
                                            <td>인사지원팀</td>
                                            <th scope="row">직급</th>
                                            <td><input type="text" /></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">성별</th>
                                            <td>
                                                <input type="radio" id="sex01" name="sex" />
                                                <label for="sex01">남자</label> &nbsp;&nbsp;&nbsp;&nbsp;
                                                <input type="radio" id="sex02" name="sex" />
                                                <label for="sex02">여자</label>
                                            </td>
                                            <th scope="row">혈액형</th>
                                            <td>
                                                <select name="">
													<option value="">RH+ A형</option>
												</select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">생년월일</th>
                                            <td><input type="text" id="bir-date" /></td>
                                            <th scope="row">&nbsp;</th>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <p class="txt">서비스의 정상적 이용을 위해서는 OBD-II 단말 구매가 필요하며 <a href="">여기</a>를 누르시면 관련 구매정보를 보실 수 있습니다.</p>


                        <!-- 하단 버튼 -->
                        <div class="btn-bottom">
                            <button class="btn btn02">취소</button>
                            <button class="btn btn03">수정완료</button>
                        </div>
                        <!--/ 하단 버튼 -->
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->