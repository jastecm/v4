<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	
	$('#passwordConfirm').on('click',function(){
		$V4.move('/mypage/userInfo');
	});
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>회원정보 설정</h2>
                    <span>서비스의 정상적인 이용을 위해 정확한 회원정보를 입력합니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="my-page">
                    <div class="check-pw">
                        <div class="info-txt">
                            외부로부터 triplog@jastecm.com님의 정보를 안전하게 보호하기 위해 비밀번호를 다시 한 번 확인 합니다.<br /> 항상 비밀번호는 타인에게 노출되지 않도록 주의해 주세요.
                        </div>

                        <table class="table">
                            <caption>운행스케쥴 상세정보</caption>
                            <tbody>
                                <tr>
                                    <th scope="row">아이디</th>
                                    <td>triplog@jastecm.com</td>
                                </tr>
                                <tr>
                                    <th scope="row">비밀번호</th>
                                    <td><input type="password" /></td>
                                </tr>
                            </tbody>
                        </table>

                        <!-- 하단 버튼 -->
                        <div class="btn-bottom">
                            <button type="button" class="btn btn03" id="passwordConfirm">확인</button>
                        </div>
                        <!--/ 하단 버튼 -->
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->