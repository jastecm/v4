<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>비밀번호 변경</h2>
                    <span>변경할 새로운 비밀번호를 입력해주세요.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="my-page">
                    <div class="change-pw">
                        <h3 class="tit">비밀번호 입력</h3>
                        <table class="table mgt20">
                            <caption>비밀번호 입력</caption>
                            <tbody>
                                <tr>
                                    <th scope="row">현재 비밀번호</th>
                                    <td><input type="password" /></td>
                                </tr>
                                <tr>
                                    <th scope="row">새로운 비밀번호</th>
                                    <td><input type="password" /></td>
                                </tr>
                                <tr>
                                    <th scope="row">비밀번호 확인</th>
                                    <td><input type="password" /></td>
                                </tr>
                            </tbody>
                        </table>

                        <!-- 하단 버튼 -->
                        <div class="btn-bottom">
                            <button class="btn btn02">취소</button>
                            <button class="btn btn03">변경</button>
                        </div>
                        <!--/ 하단 버튼 -->
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->