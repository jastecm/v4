<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2 class="md">가족관리</h2>
                    <span>
							최대 5명의 가족 구성원을 초대하여  가족간  차량을 그룹으로 관리하실 수 있습니다.<br />
							차량의 상태 및 고장 유무, 차량위치,  운행내역 등을 공유할 수 있습니다. <br />
							가족 대표자가 ViewCAR 사용요금을 대표 결제하실 수 있습니다.   
						</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="my-page">
                    <div class="box-layout invite-vehicle-user">
                        <div class="title">
                            <h3 class="tit3">차량 사용자 초대</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
                            <div class="info-txt">
                                회원가입한 사용자만 초대하실 수 있습니다.<br /> 초대한 사용자의 이메일 주소 또는 휴대전화로 링크가 전송됩니다. <br /> 메일(문자)의 수락 버튼을 클릭한 경우만 정상적인 이용이 가능합니다.
                            </div>

                            <div class="invite">
                                <div class="select">
                                    <input type="radio" name="" /> 이메일 로그인&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="radio" name="" /> 인증된 휴대전화
                                </div>
                                <ul>
                                    <!-- 반복 -->
                                    <li>
                                        <span class="tit">이메일/휴대전화</span>
                                        <input type="text" />
                                        <button type="button">추가</button>
                                    </li>
                                    <!--/ 반복 -->
                                    <li>
                                        <span class="tit">이메일/휴대전화</span>
                                        <input type="text" />
                                        <button type="button">추가</button>
                                    </li>
                                </ul>

                                <div class="check"><input type="checkbox" /> 위 초대자에게 관리자 권한을 부여합니다.</div>

                                <!-- 하단 버튼 -->
                                <div class="btn-bottom">
                                    <button class="btn btn03">초대링크 전송</button>
                                </div>
                                <!--/ 하단 버튼 -->
                            </div>
                        </div>
                    </div>
                    <!--/ 콘텐츠 본문 -->
                </div>
        </section>
        <!--/ 본문 -->