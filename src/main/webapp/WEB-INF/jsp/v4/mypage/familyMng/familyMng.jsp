<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	$('#familyInviteBtn').on('click',function(){
		$V4.move('/mypage/familyMng/invite');
	});
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2 class="md">가족관리</h2>
                    <span>
						최대 5명의 가족 구성원을 초대하여  가족간  차량을 그룹으로 관리하실 수 있습니다.<br />
						차량의 상태 및 고장 유무, 차량위치,  운행내역 등을 공유할 수 있습니다. <br />
						가족 대표자가 ViewCAR 사용요금을 대표 결제하실 수 있습니다.   
					</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="my-page">
                    <div class="family-management">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                            <div class="item">
                                <span>
									등록차량
									<strong>10</strong>대
								</span>
                                <span>
									관리자
									<strong>8</strong>대
								</span>
                                <span>
									사용자
									<strong>2</strong>대
								</span>
                            </div>

                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->


                        <!-- table 버튼 -->
                        <div class="btn-function row-2">
                            <div class="left">
                                <button type="button" class="btn btn03 md" id="familyInviteBtn">선택차량 사용자 초대</button>
                            </div>

                            <div class="right">
                                <select name="">
									<option value="">5건씩 보기</option>
								</select>
                            </div>
                        </div>
                        <!--/ table 버튼 -->

                        <table class="table list mgt20">
                            <caption>가족관리 차량 리스트</caption>
                            <colgroup>
                                <col style="width:8%" />
                                <col style="width:8%" />
                                <col />
                                <col style="width:10%" />
                                <col style="width:10%" />
                                <col style="width:20%" />
                                <col style="width:10%" />
                                <col style="width:15%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="col">선택</th>
                                    <th scope="col">차종</th>
                                    <th scope="col">차량정보</th>
                                    <th scope="col">OBO 종류</th>
                                    <th scope="col">OBO 단말번호</th>
                                    <th scope="col">사용자</th>
                                    <th scope="col">등록일</th>
                                    <th scope="col">상태</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>승용</td>
                                    <td>
                                        <div class="car-img">
                                            <span class="img"><img src="./img/vehicle/S103.png" alt="" /></span>
                                            <span class="num">쏘나타 63호 5703</span>
                                        </div>
                                    </td>
                                    <td>Von-S31</td>
                                    <td>A000876</td>
                                    <td>
                                        <div class="user">
                                            <div>
                                                <span class="name">흥길동(나)</span>
                                                <button class="btn btn05 sm round">관리자</button>
                                            </div>
                                            <div>
                                                <span class="name">홍삼동</span>
                                                <button class="btn btn03 sm round">사용자삭제</button>
                                            </div>
                                            <div>
                                                <span class="name">강이쁜</span>
                                                <button class="btn btn03 sm round">사용자삭제</button>
                                            </div>
                                        </div>
                                    </td>
                                    <td>2018.05.01</td>
                                    <td>활성화</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>승용</td>
                                    <td>
                                        <div class="car-img">
                                            <span class="img"><img src="./img/vehicle/S103.png" alt="" /></span>
                                            <span class="num">쏘나타 63호 5703</span>
                                        </div>
                                    </td>
                                    <td>Von-S31</td>
                                    <td>A000876</td>
                                    <td>
                                        <div class="user">
                                            <div>
                                                <span class="name">흥길동(나)</span>
                                                <button class="btn btn06 sm round">사용자</button>
                                            </div>
                                            <div>
                                                <span class="name">강이쁜</span>
                                                <button class="btn btn05 sm round">관리자</button>
                                            </div>
                                        </div>
                                    </td>
                                    <td>2018.05.01</td>
                                    <td>
                                        활성화
                                        <div class="mgt3"><button class="btn btn01 sm round">합성청구 요청</button></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>승용</td>
                                    <td>
                                        <div class="car-img">
                                            <span class="img"><img src="./img/vehicle/S103.png" alt="" /></span>
                                            <span class="num">쏘나타 63호 5703</span>
                                        </div>
                                    </td>
                                    <td>Von-S31</td>
                                    <td>A000876</td>
                                    <td>
                                        <div class="user">
                                            <div>
                                                <span class="name">흥길동(나)</span>
                                                <button class="btn btn05 sm round">공유 관리자</button>
                                            </div>
                                            <div>
                                                <span class="name">홍삼동</span>
                                                <button class="btn btn05 sm round">관리자</button>
                                            </div>
                                            <div>
                                                <span class="name">강이쁜</span>
                                                <button class="btn btn03 sm round">사용자삭제</button>
                                            </div>
                                        </div>
                                    </td>
                                    <td>2018.05.01</td>
                                    <td>
                                        활성화
                                        <div class="mgt3"><button class="btn btn03 sm round">합성청구 취소</button></div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <!-- 페이징 -->
                        <div id="paging">
                            <a href="" class="first icon-spr">제일 처음으로</a>
                            <a href="" class="prev icon-spr">이전으로</a>
                            <span class="current">1</span>
                            <a href="" class="num">2</a>
                            <a href="" class="num">3</a>
                            <a href="" class="num">4</a>
                            <a href="" class="num">5</a>
                            <a href="" class="next icon-spr">다음으로</a>
                            <a href="" class="last icon-spr">제일 마지막으로</a>
                        </div>
                        <!--/ 페이징 -->
                    </div>
                </div>
            </div>
            <!--/ 콘텐츠 본문 -->
        </section>
        <!--/ 본문 -->