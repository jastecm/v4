<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
					<h2>이용요금 조회ㆍ결제</h2>
					<span>서비스 이용요금을 조회하거나 결제하실 수 있습니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="my-page">
                    <div class="box-layout detail-service-fee">
                        <div class="title">
                            <h3 class="tit3">이용요금 결제 상세내역</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
                            <div class="info-txt">
                                이용요금 결제 상세 내역입니다.
                            </div>

							<h4 class="tit2 mgt40">결제정보</h4>

							<!-- table 버튼 -->
							<div class="btn-function mgt20">
								<div class="left">
									<span class="txt"><strong>총 단말기</strong> 1대</span>
								</div>
							</div>
							<!--/ table 버튼 -->

							<table class="table list mgt20">
								<caption>단말기 리스트</caption>
								<colgroup>
									<col style="width:35%" />
									<col style="width:15%" />
									<col style="width:35%" />
									<col style="width:15%" />
								</colgroup>
					
								<thead>
									<tr>
										<th scope="col">단말기정보</th>
										<th scope="col">이용기간</th>
										<th scope="col">적용기간</th>
										<th scope="col">이용요금</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>von-S31 / A000152</td>
										<td>1개월</td>
										<td>2018-03-19 ~ 2018-04-19</td>
										<td>5,500</td>
									</tr>
								</tbody>
							</table>
					
							<div class="payment-info">
								<ul>
									<li>
										<span class="tit">결제할 금액</span>5,500ⓟ
									</li>
									<li>
										<span class="tit">현재 포인트</span>5,000ⓟ
									</li>
									<li>
										<span class="tit">할인금액</span>-100ⓟ
									</li>
									<li class="red">
										<span class="tit">결제후 잔액</span>400ⓟ
									</li>
								</ul>
							</div>

							<!-- 하단 버튼 -->
							<div class="btn-bottom">
								<button class="btn btn01">닫기</button>
							</div>
							<!--/ 하단 버튼 -->
                        </div>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->