<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	
	$('#serviceFeeChargeBtn').on('click',function(){
		$V4.move('/mypage/serviceFee/detail');
	});
	
	$("#start-date1").datepicker({
		dateformat: 'yy-mm-dd'
	});
	$("#end-date1").datepicker({
		dateformat: 'yy-mm-dd'
	});

	$("#start-date2").datepicker({
		dateformat: 'yy-mm-dd'
	});
	$("#end-date2").datepicker({
		dateformat: 'yy-mm-dd'
	});

	$("#start-date3").datepicker({
		dateformat: 'yy-mm-dd'
	});
	$("#end-date3").datepicker({
		dateformat: 'yy-mm-dd'
	});

	$("#start-date4").datepicker({
		dateformat: 'yy-mm-dd'
	});
	$("#end-date4").datepicker({
		dateformat: 'yy-mm-dd'
	});

	// 탭메뉴
	$(".tab-menu>li>a").click(function() {
		$(this).parent().addClass("on").siblings().removeClass("on");
		return false;
	});

	$("#payment-dialog").dialog({
		autoOpen: true,
		show: {
			duration: 500
		},
		width: '960',
		modal: true
	});

	$("#payment-open").on("click", function() {
		$("#payment-dialog").dialog("open");
	});

	$("#point-charge-dialog").dialog({
		autoOpen: true,
		show: {
			duration: 500
		},
		width: '640',
		modal: true
	});

	$("#point-charge-open").on("click", function() {
		$("#point-charge-dialog").dialog("open");
	});

	$("#point-exchange-dialog").dialog({
		autoOpen: true,
		show: {
			duration: 500
		},
		width: '640',
		modal: true
	});

	$("#point-exchange-open").on("click", function() {
		$("#point-exchange-dialog").dialog("open");
	});
});
</script>
<!-- 본문 -->
		<section id="container">
			<div id="sub-container">
				<!-- 상단 타이틀 -->
				<div class="page-header">
					<h2>이용요금 조회ㆍ결제</h2>
					<span>서비스 이용요금을 조회하거나 결제하실 수 있습니다.</span>
				</div>
				<!--/ 상단 타이틀 -->

				<!-- 콘텐츠 본문 -->
				<div id="contents-page" class="my-page">
					<div class="service-fee">
						<!-- 상단 상태값 -->
						<div class="top-state clr">
							<div class="item">
								<span>
									등록차량수
									<strong>10</strong>대
								</span>
							</div>

							<div class="time">
								<span>2018/05/27</span>
								<span>19:53</span>
								<span>현재</span>
								<button class="btn btn04">새로고침</button>
							</div>
						</div>
						<!--/ 상단 상태값 -->

						<!-- 탭메뉴 -->
						<ul class="page-tab tab-menu col-4">
							<!-- 서비스현황 -->
							<li class="list01 on">
								<a href="#">서비스 현황</a>
								<div class="tab-list">
									<!-- 이용요금결제 -->
									<div class="box-register">
										<div class="grade">
											<div>
												<span class="tit">서비스 등급</span> Standard 등급 <button class="btn btn04 sm round">등급 변경</button>
											</div>
											<div>
												<span class="tit">선택 단말</span> (LoRa 통신형 OBD-II 단말, 제조사 : (주)자스텍엠
											</div>
										</div>
										이용요금에는 통신요금이 포함되어있어 구매 후에는 서비스 해지가 불가능합니다. 신중히 구매해주세요. <br /> 부가세와 결제 수수료가 포함되어있습니다. <br /> 환불 및 기타 결제 사항은 Easypay 규정을 준수합니다.

										<div class="right">
											<button type="button" class="btn btn01" id="serviceFeeChargeBtn">이용요금 결제</button>
										</div>
									</div>
									<!--/ 이용요금결제 -->

									<!-- table 버튼 -->
									<div class="btn-function row-2">
										<div class="left">
											<select name="">
												<option value="">이용 만기순</option>
												<option value="">최근 등록순</option>
											</select>

											<div class="date">
												<button class="active">전체</button>
												<button>오늘</button>
												<button>1주일</button>
												<button>1개월</button>
												<button>3개월</button>
												<button>6개월</button>
												<div class="direct">
													<input type="text" id="start-date1" /> &nbsp;~&nbsp;
													<input type="text" id="end-date1" />
												</div>
												<!-- 기획서에는 직접입력이 셀렉트박스로 되어 있는데, 사용자가 어떻게 날짜를 입력 해야 하는지 모르겠어서 임의로 변경했습니다.-->
											</div>
										</div>
										<div class="right">
											<select name="">
												<option value="">5건씩 보기</option>
											</select>
										</div>
									</div>
									<!--/ table 버튼 -->

									<table class="table list mgt20">
										<caption>서비스현황 리스트</caption>
										<colgroup>
											<col style="width:8%" />
											<col style="width:10%" />
											<col style="width:15%" />
											<col />
											<col style="width:25%" />
											<col style="width:18%" />
										</colgroup>
										<thead>
											<tr>
												<th scope="col"><input type="checkbox" /></th>
												<th scope="col">
													<select name="" class="arrow">
														<option value="">개통</option>
														<option value="">미개통</option>
													</select>
												</th>
												<th scope="col">단말기정보</th>
												<th scope="col">등록차량</th>
												<th scope="col">관리자</th>
												<th scope="col">이용만기일</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><input type="checkbox" /></td>
												<td>개통</td>
												<td>von-S31<br />A0006567</td>
												<td>
													<div class="car-img">
														<span class="img"><img src="./img/vehicle/S103.png" alt="" /></span>
														<span class="num">쏘나타 63호 5703</span>
													</div>
												</td>
												<td>홍길동(나)</td>
												<td>2018.05.01</td>
											</tr>
											<tr>
												<td><input type="checkbox" /></td>
												<td>개통</td>
												<td>von-S31<br />A0006567</td>
												<td>
													<div class="car-img">
														<span class="img"><img src="./img/vehicle/S103.png" alt="" /></span>
														<span class="num">쏘나타 63호 5703</span>
													</div>
												</td>
												<td>
													홍길동(나) / 홍삼동
													<div class="mgt5"><button class="btn btn03 sm round">합산결제</button></div>
												</td>
												<td>2018.05.01</td>
											</tr>
										</tbody>
									</table>

									<!-- 페이징 -->
									<div id="paging">
										<a href="" class="first icon-spr">제일 처음으로</a>
										<a href="" class="prev icon-spr">이전으로</a>
										<span class="current">1</span>
										<a href="" class="num">2</a>
										<a href="" class="num">3</a>
										<a href="" class="num">4</a>
										<a href="" class="num">5</a>
										<a href="" class="next icon-spr">다음으로</a>
										<a href="" class="last icon-spr">제일 마지막으로</a>
									</div>
									<!--/ 페이징 -->
								</div>
							</li>
							<!--/ 서비스현황 -->

							<!-- 서비스스토어내역 -->
							<li class="list02">
								<a href="#">서비스스토어 내역</a>
								<div class="tab-list">
									<!-- 이용요금결제 -->
									<div class="box-register">
										<div class="grade">
											<div>
												<span class="tit">서비스 등급</span> Standard 등급 <button class="btn btn04 sm round">등급 변경</button>
											</div>
										</div>
										서비스 스토어 상품/서비스에 따라 사용요금 부과기준은 다릅니다.
									</div>
									<!--/ 이용요금결제 -->

									<!-- table 버튼 -->
									<div class="btn-function row-2">
										<div class="left">
											<select name="">
												<option value="">결제자순</option>
												<option value="">최근결제일순</option>
												<option value="">이용만기일순</option>
												<option value="">결제금액순</option>
											</select>

											<div class="date">
												<button class="active">전체</button>
												<button>오늘</button>
												<button>1주일</button>
												<button>1개월</button>
												<button>3개월</button>
												<button>6개월</button>
												<div class="direct">
													<input type="text" id="start-date2" /> &nbsp;~&nbsp;
													<input type="text" id="end-date2" />
												</div>
												<!-- 기획서에는 직접입력이 셀렉트박스로 되어 있는데, 사용자가 어떻게 날짜를 입력 해야 하는지 모르겠어서 임의로 변경했습니다.-->
											</div>
										</div>
										<div class="right">
											<select name="">
												<option value="">5건씩 보기</option>
											</select>
										</div>
									</div>
									<!--/ table 버튼 -->

									<table class="table list mgt20">
										<caption>서비스스토어 리스트</caption>
										<colgroup>
											<col />
											<col style="width:13%" />
											<col style="width:10%" />
											<col style="width:10%" />
											<col style="width:10%" />
											<col style="width:10%" />
											<col style="width:13%" />
										</colgroup>
										<thead>
											<tr>
												<th scope="col">서비스명</th>
												<th scope="col">결제자</th>
												<th scope="col">차량대수</th>
												<th scope="col">최근결제일</th>
												<th scope="col">이용만기일</th>
												<th scope="col">결제금액</th>
												<th scope="col">비고</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="left">
													<a href="javascript:;" class="service-name">
														<b>운행스케쥴 거래처 추가 서비스</b> 거래처 추가는 10개 단위로 가능하며 10개당 11,000원 (부가세 포함)이 부가됩니다.
													</a>
												</td>
												<td>㈜자스텍엠</td>
												<td><a href="javascript:;">10</a></td>
												<td>2018/05/20</td>
												<td>2018.05.01</td>
												<td>10,000</td>
												<td>
													<button class="btn btn03 sm round" id="payment-open">요금 결제</button>
													<div class="mgt3"><button class="btn btn02 sm round">서비스 해지</button></div>
												</td>
											</tr>
										</tbody>
									</table>

									<!-- 페이징 -->
									<div id="paging">
										<a href="" class="first icon-spr">제일 처음으로</a>
										<a href="" class="prev icon-spr">이전으로</a>
										<span class="current">1</span>
										<a href="" class="num">2</a>
										<a href="" class="num">3</a>
										<a href="" class="num">4</a>
										<a href="" class="num">5</a>
										<a href="" class="next icon-spr">다음으로</a>
										<a href="" class="last icon-spr">제일 마지막으로</a>
									</div>
									<!--/ 페이징 -->
								</div>
							</li>
							<!--/ 서비스스토어내역 -->

							<!-- 결제내역 -->
							<li class="list03">
								<a href="#">결제 내역</a>
								<div class="tab-list">
									<!-- 이용요금결제 -->
									<div class="box-register">
										<div class="grade">
											<div>
												<span class="tit">서비스 등급</span> Standard 등급 <button class="btn btn04 sm round">등급 변경</button>
											</div>
											<div>
												<span class="tit">선택 단말</span> (LoRa 통신형 OBD-II 단말, 제조사 : (주)자스텍엠
											</div>
										</div>
									</div>
									<!--/ 이용요금결제 -->

									<!-- table 버튼 -->
									<div class="btn-function row-2">
										<div class="left">
											<select name="">
												<option value="">높은 결제일순</option>
												<option value="">높은 금액순</option>
											</select>

											<div class="date">
												<button class="active">전체</button>
												<button>오늘</button>
												<button>1주일</button>
												<button>1개월</button>
												<button>3개월</button>
												<button>6개월</button>
												<div class="direct">
													<input type="text" id="start-date3" /> &nbsp;~&nbsp;
													<input type="text" id="end-date3" />
												</div>
												<!-- 기획서에는 직접입력이 셀렉트박스로 되어 있는데, 사용자가 어떻게 날짜를 입력 해야 하는지 모르겠어서 임의로 변경했습니다.-->
											</div>
										</div>
										<div class="right">
											<select name="">
													<option value="">5건씩 보기</option>
												</select>
										</div>
									</div>
									<!--/ table 버튼 -->

									<table class="table list mgt20">
										<caption>결제내역 리스트</caption>
										<colgroup>
											<col style="width:15%" />
											<col style="width:15%" />
											<col />
											<col style="width:20%" />
										</colgroup>
										<thead>
											<tr>
												<th scope="col">결제일</th>
												<th scope="col">서비스 형태</th>
												<th scope="col">내역</th>
												<th scope="col">총 결제금액 (부가세포함)</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>2018/05/20 17:47</td>
												<td>서비스 이용요금</td>
												<td class="left">
													<a href="">[총 2대] 49보 4022 (A0000234) 외 2건</a>
												</td>
												<td>11,000 ⓟ</td>
											</tr>
										</tbody>
									</table>

									<!-- 페이징 -->
									<div id="paging">
										<a href="" class="first icon-spr">제일 처음으로</a>
										<a href="" class="prev icon-spr">이전으로</a>
										<span class="current">1</span>
										<a href="" class="num">2</a>
										<a href="" class="num">3</a>
										<a href="" class="num">4</a>
										<a href="" class="num">5</a>
										<a href="" class="next icon-spr">다음으로</a>
										<a href="" class="last icon-spr">제일 마지막으로</a>
									</div>
									<!--/ 페이징 -->
								</div>
							</li>
							<!--/ 결제내역 -->

							<!-- 포인트현황 -->
							<li class="list04">
								<a href="#">포인트 현황</a>
								<div class="tab-list">
									<!-- 이용요금결제 -->
									<div class="box-register">
										<div class="grade">
											<div>
												<span class="tit">ViewCAR 포인트 잔액</span> 20,000 ⓟ
											</div>
										</div>
										1ⓟ는 1원 가치입니다.<br /> 환전은 안전한 거래를 위해 환전 요청후 익일 2시후 지연 입금됩니다.
										<div class="right">
											<button class="btn btn06" id="point-charge-open">충전</button>
											<button class="btn btn01" id="point-exchange-open">환전</button>
										</div>
									</div>
									<!--/ 이용요금결제 -->

									<!-- table 버튼 -->
									<div class="btn-function row-2">
										<div class="left">
											<select name="">
												<option value="">전체</option>
												<option value="">충전</option>
												<option value="">환전</option>
											</select>

											<div class="date">
												<button class="active">전체</button>
												<button>오늘</button>
												<button>1주일</button>
												<button>1개월</button>
												<button>3개월</button>
												<button>6개월</button>
												<div class="direct">
													<input type="text" id="start-date4" /> &nbsp;~&nbsp;
													<input type="text" id="end-date4" />
												</div>
												<!-- 기획서에는 직접입력이 셀렉트박스로 되어 있는데, 사용자가 어떻게 날짜를 입력 해야 하는지 모르겠어서 임의로 변경했습니다.-->
											</div>
										</div>
										<div class="right">
											<select name="">
												<option value="">5건씩 보기</option>
											</select>
										</div>
									</div>
									<!--/ table 버튼 -->

									<table class="table list mgt20">
										<caption>결제내역 리스트</caption>
										<colgroup>
											<col />
											<col style="width:16%" />
											<col style="width:16%" />
											<col style="width:16%" />
											<col style="width:16%" />
											<col style="width:16%" />
										</colgroup>
										<thead>
											<tr>
												<th scope="col">결제일시</th>
												<th scope="col">내역</th>
												<th scope="col">수단</th>
												<th scope="col">입금포인트</th>
												<th scope="col">출금포인트</th>
												<th scope="col">잔액</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>2018/05/20 17:47</td>
												<td>포인트 충전</td>
												<td>신용카드</td>
												<td>10,000</td>
												<td>100,000</td>
												<td>110,000</td>
											</tr>
										</tbody>
									</table>

									<!-- 페이징 -->
									<div id="paging">
										<a href="" class="first icon-spr">제일 처음으로</a>
										<a href="" class="prev icon-spr">이전으로</a>
										<span class="current">1</span>
										<a href="" class="num">2</a>
										<a href="" class="num">3</a>
										<a href="" class="num">4</a>
										<a href="" class="num">5</a>
										<a href="" class="next icon-spr">다음으로</a>
										<a href="" class="last icon-spr">제일 마지막으로</a>
									</div>
									<!--/ 페이징 -->
								</div>
							</li>
							<!--/ 포인트현황 -->
						</ul>
					</div>
				</div>
				<!--/ 콘텐츠 본문 -->
			</div>
		</section>
		<!--/ 본문 -->
		
<!-- 이용요금결제 팝업 -->
	<div id="payment-dialog" title="이용요금 결제">
		<div class="info-txt">
			<span class="left">이용금액과 기간으 확인한 후, 결제해주세요.<br >1년 이용권 구매시 20%, 3개월 이용권 구매시 5%, 할인이 적용됩니다.</span>
		</div>

		<h3 class="tit mgt30">결제정보</h3>

		<div class="top-info clr mgt20">
            <span class="left"><b>총 단말기</b> 1대</span>
        </div>
	
		<table class="table list mgt20">
			<caption>단말기 리스트</caption>
			<colgroup>
				<col style="width:10%" />
				<col  />
				<col style="width:20%" />
				<col style="width:20%" />
				<col style="width:20%" />
			</colgroup>

			<thead>
				<tr>
					<th scope="col">개통상태</th>
					<th scope="col">등록차량</th>
					<th scope="col">단말기정보</th>
					<th scope="col">이용기간</th>
					<th scope="col">이용요금</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>개통</td>
					<td>
						<div class="car-img">
							<span class="img"><img src="./img/vehicle/S103.png" alt="" /></span>
							<span class="num">쏘나타 63호 5703</span>
						</div>
					</td>
					<td>von-S31<br />A000152</td>
					<td>
						<select name="">
							<option value="">1개월</option>
						</select>
					</td>
					<td>5,500</td>
				</tr>
			</tbody>
		</table>

		<div class="change-period">
			<span class="tit">전체 이용기간 변경</span>
			<select name="">
				<option value="">1개월</option>
			</select>
		</div>

		<div class="payment-info">
			<div class="left">
				<ul>
					<li>
						<span class="tit">결제할 금액</span>5,500ⓟ
					</li>
					<li>
						<span class="tit">현재 포인트</span>5,000ⓟ
					</li>
					<li>
						<span class="tit">할인금액</span>-100ⓟ
					</li>
					<li class="red">
						<span class="tit">부족 포인트</span>-500ⓟ <button class="btn btn05 sm round">충전</button>
					</li>
				</ul>
			</div>

			<div class="right">
				<ul>
					<li>
						<span class="tit">결제할 금액</span>5,500ⓟ
					</li>
					<li>
						<span class="tit">현재 포인트</span>15,000ⓟ
					</li>
					<li>
						<span class="tit">할인금액</span>-500ⓟ
					</li>
					<li class="red">
						<span class="tit">결제후 포인트</span>9,000ⓟ
					</li>
				</ul>
			</div>
		</div>

		<!-- 하단 버튼 -->
		<div class="btn-bottom">
			<button class="btn btn02">취소</button>
			<button class="btn btn03" disabled>결제하기</button>
		</div>
		<!--/ 하단 버튼 -->
	</div>
	<!--/ 이용요금결제 팝업 -->

	<!-- ViewCAR포인트충전 팝업 -->
	<div id="point-charge-dialog" title="ViewCAR 포인트 충전">
		<div class="info-txt">
			<span class="left">
				ViewCAR에서는 사용하시는 모든 결제수단은 포인트로 통합 관리됩니다.<br />
				서비스 사용료 결제에서부터 제휴서비스 수익까지 포인트를 통해 지급됩니다.<br />
				언제든지 충전 또는 현금으로 환전하실 수 있습니다. 
			</span>
		</div>

		<ul class="point-info">
			<li>
				<span class="tit">현재 보유 포인트</span>
				<span class="point">100,000ⓟ</span>
			</li>
			<li>
				<span class="tit">충전할 포인트</span>
				<span class="point">
					<select name="">
						<option value="">5,000</option>
					</select>
				</span>
			</li>
			<li>
				<span class="tit">충전후 보유보인트</span>
				<span class="point">105,000ⓟ</span>
			</li>
		</ul>

		<h3 class="tit mgt30">결제수단</h3>
		<div class="mgt20">
			<input type="radio" /> 신용카드&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="radio" /> 계좌이체
		</div>
		<p class="txt">
			※ 신용카드로 직접 충전한 포인트는 법률로 금지된 부정사용 방지를 위해 환전하실 수 없으며 카드 승인/매입 취소합니다.
		</p>

		<!-- 하단 버튼 -->
		<div class="btn-bottom">
			<button class="btn btn02">취소</button>
			<button class="btn btn03">포인트 충전</button>
		</div>
		<!--/ 하단 버튼 -->
	</div>
	<!--/ ViewCAR포인트충전 팝업 -->

	<!-- ViewCAR포인트환전 팝업 -->
	<div id="point-exchange-dialog" title="ViewCAR 포인트 환전 신청">
			<div class="info-txt">
				<span class="left">
					ViewCAR 포인트를 현금으로 환전 신청하시겠습니까?<br />
					최소 5,000원 포인트 이상 환전하실 수 있습니다. 
				</span>
			</div>
	
			<ul class="point-info">
				<li>
					<span class="tit">현재 보유 포인트</span>
					<span class="point">100,000ⓟ</span>
				</li>
				<li>
					<span class="tit">환전 요청 포인트</span>
					<span class="point">
						<select name="">
							<option value="">5,000</option>
						</select>
					</span>
				</li>
				<li>
					<span class="tit">환전후 보유보인트</span>
					<span class="point">105,000ⓟ</span>
				</li>
			</ul>
	
			<h3 class="tit mgt30">환전 요청자 정보</h3>
			<ul class="exchange-user">
				<li>
					<span class="tit">사용자명</span>
					홍길동
				</li>
				<li>
					<span class="tit">은행명</span>
					<select name="">
						<option value="">국민은행</option>
					</select>
				</li>
				<li>
					<span class="tit">은행계좌번호</span>
					<input type="text" />
				</li>
			</ul>
			<p class="txt">
				<b>※ 최소 5,000원 포인트 이상 환전하실 수 있습니다.</b>
				<br />
				※ 환전 신청은 안전한 거래를 위해 신청자 본인의 계좌로만 입금되며, 환전 요청일로부터 익월 2시 이후 지연 입금됩니다. 
			</p>
	
			<!-- 하단 버튼 -->
			<div class="btn-bottom">
				<button class="btn btn02">취소</button>
				<button class="btn btn03">환전 신청</button>
			</div>
			<!--/ 하단 버튼 -->
		</div>
		<!--/ ViewCAR포인트환전 팝업 -->