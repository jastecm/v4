<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>




<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta name="description" content="VIEWCAR - For Your Smart Driving">
		<meta property="og:title" content="VIEWCAR">
		<meta property="og:description" content="VIEWCAR - For Your Smart Driving">
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/js/jqueryui/jquery-ui.css"/>
		<%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/content.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/content_costom.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/style.css" /> --%>
		<link rel="manifest" href="${pageContext.request.contextPath}/common/json/manifest.json">
		<%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/jsDatePick_ltr.min.css" media="all" /> --%>
		
		<%@include file="/WEB-INF/jsp/common/common.jsp"%>
		
		<script src="${pageContext.request.contextPath}/common/js/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/common.js"></script>
		<script src="${pageContext.request.contextPath}/common/js/topup.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jqueryui/jquery-ui.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/common.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.stylish-select.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jsDatePick.jquery.min.1.3.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/util.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.form.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.MultiFile.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/message/${_LANG}.js"></script>
		
		<script type="text/javascript" src="https://www.gstatic.com/firebasejs/4.1.3/firebase.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/commonDev.js"></script>

             

<script type="text/javascript">
Date.prototype.getUnixTime = function() { return this.getTime()/1000|0 };

$(function(){
$('#login').on('click',function(){
		
		$.ajax({
			url: "${defaultPath}/api/1/token/"
			,dataType : 'json'
			,contentType: "application/json; charset=UTF-8"
			,type:"GET"
			,cache:!1
			,headers : {"Cache-Control":"no-cache"}
			,beforeSend : function(xhr){
				xhr.setRequestHeader("id", $('#userId').val());
				xhr.setRequestHeader("pw", $('#userPw').val()); 
			}
			,success:function(t){
				console.log(t);
				$('#token').val(t.result);
				
			}		
			,error:function(t){
				console.log(t);
				if(t.responseJSON.result == "token.notActive"){
					var sendData = {
							"id" : $('#userId').val(),
							"pw" : $('#userPw').val(),
							"expire" : $('#expire').val()
					}
					
					$.ajax({
						url: "${defaultPath}/api/1/token/"
						,dataType : 'json'
						,contentType: "application/json; charset=UTF-8"
						,type:"POST"
						,data: JSON.stringify(sendData)
						,cache:!1
						,headers : {"Cache-Control":"no-cache"}
						,beforeSend : function(xhr){
							 
						}
						,success:function(t){
							console.log(t);
							$('#token').val(t.result);
							
						}		
						,error:function(t){
							
						}
					});
				}
			}
		});
	});
	
	
	$('#reg').on('click',function(){
		
		var sendData = {
			"vehicleKey" : $('#vehicleKey').val(),
			"periodicInspection" : $('#periodicInspection').val(),
			"nextPeriodicInspection" : $('#nextPeriodicInspection').val(),
			"reqAccount" : $('#reqAccount').val(),
			"groupKey" : $('#groupKey').val(),
			"maintenanceReqDate" : $('#maintenanceReqDate').val(),
			"completeReqDate" : $('#completeReqDate').val(),
			"troubleDesc" : $('#troubleDesc').val(),
			"partsKey" : $('#partsKey').val(),
			"partsQuality" : $('#partsQuality').val(),
			"partsCnt" : $('#partsCnt').val(),
			"partsPay" : $('#partsPay').val(),
			"wage" : $('#wage').val()
		}
		
		$.ajax({
			url: "${defaultPath}/api/1/maintenanceEtc"
			,dataType : 'json'
			,contentType: "application/json; charset=UTF-8"
			,type:"POST"
			,data:JSON.stringify(sendData)
			,cache:!1
			,headers : {"Cache-Control":"no-cache"}
			,beforeSend : function(xhr){
				xhr.setRequestHeader("key", $('#token').val()); 
			}
			,success:function(t){
				$('#groupSelectAll').trigger('click');
			}		
			,error:function(t){
				console.log(t);
			}
		});
	});
});


</script>
	</head>
    <body>
    	<h1>정비 테스트 화면</h1>
    	로그인하기<br/>
    	아이디 : <input type="text" id="userId" value="pne1818@naver.com"></br>
    	비번 : <input type="text" id="userPw" value="asdf!@#123"></br>
    	만료시간 : <input type="text" id="expire" value="9999999"></br>
    	<button type="button" id="login">로그인</button>
    	<br/>
    	<br/>
    	<br/>
    	
    	토큰 : <input type="text" id="token" value="" style="width:1000px;"></br>
    	<br/>
    	
    	차량키 : <input type="text" id="vehicleKey" name="vehicleKey" value="a01ba2bf-7f4c-11e8-af9f-42010a8c0018"/>
    	<br>
    	정기검사일 : <input type="text" id="periodicInspection" name="periodicInspection" value="2018/07/16"/>
    	<br>
    	다음검사일 : <input type="text" id="nextPeriodicInspection" name="nextPeriodicInspection" value="2018/07/17"/>
    	<br>
    	요청자 : <input type="text" id="reqAccount" name="reqAccount" value="ee0b559a-7aad-11e8-bf1c-42010a8c0020"/>
    	<br>
    	사용부서 : <input type="text" id="groupKey" name="groupKey" value="b8453dc7-858d-11e8-af9f-42010a8c0018"/>
    	<br>
		정비요청일 : <input type="text" id="maintenanceReqDate" name="maintenanceReqDate" value="2018/07/16"/>
		<br>
		완료요청일 : <input type="text" id="completeReqDate" name="completeReqDate" value="2018/07/16"/>
		<br>
		고장내역 : <input type="text" id="troubleDesc" name="troubleDesc" value="고장이 나브럿어~?"/>
		
		
		=======================step2===============================
		<br>
		정비소 : <input type="text" id="clinicNm" name="clinicNm"/>
		<br>
		주행거리 : <input type="text" id="distance" name="distance"/>
		<br>
		정비날짜 : <input type="text" id="clinicDate" name="clinicDate"/>
		<br>
		정비내역 : <input type="text" id="repairContent" name="repairContent"/>
		<br>
		정비항목 : <input type="text" id="partsKey" name="partsKey" value="A0001,A0002,A0003"/>
		<br>
		부품등급 : <input type="text" id="partsQuality" name="partsQuality" value="A,B,C"/>
		<br>
		수량 : <input type="text" id="partsCnt" name="partsCnt" value="1,2,3"/>
		<br>
		부품금액 : <input type="text" id="partsPay" name="partsPay" value="1000,2000,3000"/>
		<br>
		공임 : <input type="text" id="wage" name="wage" value="3000,2000,1000"/>
		<br>
		
		=======================step2===============================
		<br>
    	<button type="button" id="reg">정비등록</button>
    </body>
</html>