<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>

<html>

<head>

<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery-1.11.2.min.js" charset="utf-8" ></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/sockjs.min.js" ></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/util.js" ></script>


<script type="text/javascript">

//var sock = new SockJS('${pageContext.request.contextPath}/echo',[],{sessionId : function(){return "asdf";}});
var sock = new SockJS('${pageContext.request.contextPath}/echo',[],{sessionId : 16});

sock.onopen = function() {

    $('#console').append('websocket opened' + '<br>');

};

sock.onmessage = function(message) {
	var xx = message.data;
	//var xx2 = replaceAll(xx,"radio ","<br>   - radio");
	var yy = replaceAll(xx,"payload ","<br>   - payload");
	var zz = replaceAll(yy,"parse ","<br>   - parse");
	//var zz2 = replaceAll(zz,"packet ","<br>   - packet");
    $('#console').prepend(zz + '<br><br>');

};

sock.onclose = function(event) {

    $('#console').append('<br>websocket closed : ' + event);

};

function messageSend() {

    sock.send($('#message').val());

}

</script>

</head>

<body>


<input type="text" id="message" />

<input type="button" value="전송" onclick="messageSend();" />

<div id="console" />


</body>

</html>

