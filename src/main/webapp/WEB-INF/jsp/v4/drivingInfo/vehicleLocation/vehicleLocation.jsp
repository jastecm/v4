<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
	$(function() {
		// 탭메뉴
		$(".tab-menu>li>a").click(function() {
			$(this).parent().addClass("on").siblings().removeClass("on");
			return false;
		});

		$('#searchTarget').on('change', function() {
			var searchTarget = $(this).val();

			//차량
			if (searchTarget == 'vehicle') {
				//차량을 선택하면 차량 팝업이 나와야 한다.
				$('#vehicleTr').show();
				$('#userTr').hide();
				//사용자
			} else if (searchTarget == "user") {
				$('#vehicleTr').hide();
				$('#userTr').show();
			} else {
				//전체
				$('#vehicleTr').hide();
				$('#userTr').hide();
			}
		});

		$("#btn_searchUserPop").data({
			type : "user",
			http_post_option : {
				header : {
					key : "${_KEY}"
				}
			},
			selType : "radio",
			action : {
				ok : function(arrSelect, inputObj, valueObj) {
					$.each(arrSelect, function() {
						var info = base64ToJsonObj($(this).data("info"));
						$(inputObj).val(info.name);
						$(valueObj).val(info.accountKey);
					});
				}
			},
			targetInput : $("#userText"),
			targetInputValue : $("#userKey"),
			debugMode : true
		}).commonPop(); //바로 써도 ok

		$("#btn_searchVehiclePop").data({
			type : "vehicle",
			http_post_option : {
				header : {
					key : "${_KEY}"
				}
			},
			selType : "radio",
			action : {
				ok : function(arrSelect, inputObj, valueObj) {
					$.each(arrSelect, function() {
						var info = base64ToJsonObj($(this).data("info"));
						$(inputObj).val(info.plateNum);
						$(valueObj).val(info.vehicleKey);
					});
				} //여러곳에 똑같은 선택액션은 이렇게써도 ok
			},
			targetInput : $("#vehicleText"),
			targetInputValue : $("#vehicleKey"),
			debugMode : true
		}).commonPop(); //바로 써도 ok

		$('#vehicleLocationSearchBtn').on('click', function() {

			//타겟을 가지고 조회할때 키를 가져다가 넘겨야 된다.
			//$('#searchTarget')
		});

		//조회
		$('#mapSearchBtn').on('click', function() {

			var singleMap = $("#singleMap").get(0).contentWindow;

			//전체 주행중 주차중
			var drivingType = $('input[name="drivingType"]:checked').val();
			if (drivingType != "") {
				singleMap.searchDrivingState = drivingType;
			} else {
				singleMap.searchDrivingState = "";
			}

			//전체 배차 미배차
			var allocateType = $('input[name="allocateType"]:checked').val();
			if (allocateType != "") {
				singleMap.searchAllocateSate = allocateType;
			} else {
				singleMap.searchAllocateSate = "";
			}

			var searchTarget = $('#searchTarget').val();

			//차량
			if (searchTarget == "vehicle") {
				var vehicleKey = $('#vehicleKey').val();
				singleMap.vehicleKeys = vehicleKey;
			} else if (searchTarget == "user") {
				var userKey = $('#userKey').val();
				singleMap.searchDriverKey = userKey;
			}

			singleMap._o = singleMap.createOption();
			singleMap.outerReset()

		});
		
		//최근
		$("#locationStandard").on("change",function(){
			var searchTime = $(this).val();
			
			var singleMap = $("#singleMap").get(0).contentWindow;
			singleMap.lastestTime = searchTime;
			singleMap._o = singleMap.createOption();
		});
		
		//다중 맵삭제
		$(document).on('click','.map_del',function(){
			$(this).parents('.left').remove();
		});
		
		//다중 맵 추가
		$(document).on('click','.map_add',function(){
			
			
			var searchType = $('input[name="r_a"]:checked').val();
			
			
			var vehicleKeys = "";
			var searchDriverKey = "";
			if(searchType == "user"){
				var accountKey = $('#accountKey').val();
				if(accountKey == ""){
					alert('사용자를 선택해주세요');
					return;
				}
				searchDriverKey = accountKey;
			}else if(searchType == "vehicle"){
				var vehicleKey = $('#vehicleKey').val();
				if(vehicleKey == ""){
					alert('차량을 선택해주세요');
					return;
				}
				vehicleKeys = vehicleKey;
			}
			
			
			var childrenLength = $('.multiple').children().length;
			var strHtml = "";
			
			strHtml += '<div class="left" style="margin-right: 5px;margin-left: 5px;">';
			if(searchType == "user"){
				strHtml += '	<iframe id="multiMap'+childrenLength+'" src="/map/openMap?searchType=vehicle&searchDriverKey='+searchDriverKey+'"	width="100%" height="300" frameborder="0" style="border: 0" allowfullscreen></iframe>';	
			}else{
				strHtml += '	<iframe id="multiMap'+childrenLength+'" src="/map/openMap?searchType=vehicle&vehicleKeys='+vehicleKeys+'"	width="100%" height="300" frameborder="0" style="border: 0" allowfullscreen></iframe>';
			}
			
			strHtml += '	<div class="select" style="height:50px">';
			/* strHtml += '		<input type="radio" id="account'+childrenLength+'" name="r_'+childrenLength+'" value="user" checked />';
			strHtml += '		<label for="account'+childrenLength+'">&nbsp;사용자</label>&nbsp;&nbsp; ';
			strHtml += '		<input type="radio" id="vehicle'+childrenLength+'" name="r_'+childrenLength+'" value="vehicle" />';
			strHtml += '		<label for="vehicle'+childrenLength+'">&nbsp;차량</label> ';
			strHtml += '		<select name="userSelect'+childrenLength+'" id="userSelect'+childrenLength+'">';
			strHtml += '			<option value="">전체 사용자</option>';
			strHtml += '		</select> ';
			strHtml += '		<select name="vehicleSelect'+childrenLength+'" id="vehicleSelect'+childrenLength+'" style="display: none">';
			strHtml += '			<option value="">전체 차량</option>';
			strHtml += '		</select>'; */
			strHtml += '		<button type="button" class="delete map_del">삭제</button>';
			strHtml += '	</div>';
			strHtml += '</div>';

			$('.multiple').children().eq(childrenLength - 1).before(strHtml);
			initSearchBox();
			
		});
		
		
		
		
		$(document).on('change','.multiple input[type="radio"]',function(){
			var self = $(this);
			var type = self.val();
			//사용자
			if(type=="user"){
				$('#userDiv').show();
				$('#vehicleDiv').hide();
			}else{//차량
				$('#userDiv').hide();
				$('#vehicleDiv').show();
			}
		});
		
		$("#userPop").data({
			type : "user",
			http_post_option : {
				header : {
					key : "${_KEY}"
				}
			},
			selType : "radio",
				action : {
					ok : function(arrSelect, inputObj, valueObj) {
						$.each(arrSelect, function() {
							var info = base64ToJsonObj($(arrSelect).data("info"));
							$(inputObj).val(info.name);
							$(valueObj).val(info.accountKey);
						});
					}
				},
				targetInput : $("#name"),
				targetInputValue : $("#accountKey")
		}).commonPop();
		
		$("#vehiclePop").data({
			type : "vehicle",
			http_post_option : {
				header : {
					key : "${_KEY}"
				}
			},
			selType : "radio",
				action : {
					ok : function(arrSelect, inputObj, valueObj) {
						$.each(arrSelect, function() {
							var info = base64ToJsonObj($(arrSelect).data("info"));
							$(inputObj).val(info.plateNum);
							$(valueObj).val(info.vehicleKey);
						});
					}
				},
				targetInput : $("#plateNum"),
				targetInputValue : $("#vehicleKey")
		}).commonPop();
		
		
		var initSearchBox = function(){
			$('#accountR').prop('checked',true)
			$('#userDiv').show();
			$('#vehicleDiv').hide();
			
			$('#name').val('');
			$('#plateNum').val('');
			$('#accountKey').val('');
			$('#vehicleKey').val('');
		};
		
	});

	function ShowHide(n) {
		if (n == "view1") {
			view1.style.display = "";
			view2.style.display = "none";
		} else if (n == "view2") {
			view1.style.display = "none";
			view2.style.display = "";
		}
	}

	function ShowHide2(n) {
		if (n == "view3") {
			view3.style.display = "";
			view4.style.display = "none";
		} else if (n == "view4") {
			view3.style.display = "none";
			view4.style.display = "";
		}
	}
</script>
<!-- 본문 -->
<section id="container">
	<div id="sub-container">
		<!-- 상단 타이틀 -->
		<div class="page-header">
			<h2>차량 위치조회</h2>
			<span>실시간 차량위치, 상태 정보 및 여러 대의 차량을 동시에 모니터링 할 수 있습니다.</span>
		</div>
		<!--/ 상단 타이틀 -->

		<!-- 콘텐츠 본문 -->
		<div id="contents-page" class="driving-page">
			<div class="vehicle-location">
				<!-- 상단 상태값 -->
				<div class="top-state clr">
					<div class="item">
						<span> 총 서비스 <strong>10</strong>건
						</span> <span> 등록건수 <strong>8</strong>건
						</span> <span> 등록대기 <strong>2</strong>건
						</span>
					</div>

					<div class="time">
						<span>2018/05/27</span> <span>19:53</span> <span>현재</span>
						<button class="btn btn04">새로고침</button>
					</div>
				</div>
				<!--/ 상단 상태값 -->

				<ul class="page-tab tab-menu col-4">
					<!-- 차량 현황 -->
					<li class="list01 on"><a href="#">일반 차량</a>
						<div class="tab-list">
							<table class="table mgt20">
								<caption>배차 신청</caption>
								<tbody>
									<tr>
										<th scope="row">차량</th>
										<td><input name="drivingType" type="radio" value=""
											checked />&nbsp;전체&nbsp;&nbsp; <input name="drivingType"
											type="radio" value="1" />&nbsp;주행중&nbsp;&nbsp; <input
											name="drivingType" type="radio" value="0" />&nbsp;주차중&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

											<input name="allocateType" type="radio" value="" checked />&nbsp;전체&nbsp;&nbsp;
											<input name="allocateType" type="radio" value="1" />&nbsp;배차&nbsp;&nbsp;
											<input name="allocateType" type="radio" value="0" />&nbsp;미배차
										</td>
									</tr>
									<tr>
										<th scope="row">사용자</th>
										<td><select name="searchTarget" id="searchTarget">
												<option value="">전체</option>
												<option value="vehicle">차량</option>
												<option value="user">사용자</option>
										</select></td>
									</tr>
									<tr id="userTr" style="display: none;">
										<th scope="row"></th>
										<td><input type="text" id="userText" style="width: 86%;"
											placeholder="사용자 검색" />
											<button type="button" class="btn btn01 md commonPop"
												id="btn_searchUserPop">검색</button> <input type="hidden"
											id="userKey" name="userKey" /></td>
									</tr>
									<tr id="vehicleTr" style="display: none;">
										<th scope="row"></th>
										<td><input type="text" id="vehicleText"
											style="width: 86%;" placeholder="차량 검색" />
											<button type="button" class="btn btn01 md commonPop"
												id="btn_searchVehiclePop">검색</button> <input type="hidden"
											id="vehicleKey" name="vehicleKey" /></td>
									</tr>
									<tr>
										<th scope="row">지역</th>
										<td>
											<div class="division">
												<div class="col-6">
													<select name="">
														<option value="">전체 시/도</option>
													</select>
												</div>
												<div class="space">&nbsp;</div>
												<div class="col-6">
													<select name="">
														<option value="">전체 시/군/구</option>
													</select>
												</div>
											</div>
										</td>
									</tr>
								</tbody>
							</table>

							<!-- 하단 버튼 -->
							<div class="btn-bottom">
								<button type="button" class="btn btn03" id="mapSearchBtn">조회</button>
							</div>
							<!--/ 하단 버튼 -->

							<div class="map">
								<iframe id="singleMap" src="/map/openMap?searchType=vehicle"
									width="100%" height="450" frameborder="0" style="border: 0"
									allowfullscreen></iframe>
							</div>

							<!-- table 버튼 -->
							<div class="btn-function mgt10">
								<div class="left">
									<span class="txt"><strong>상세경로 기준</strong></span> &nbsp;&nbsp;
									<select name="locationStandard" id="locationStandard">
										<option value="24">최근 24시간</option>
										<option value="12">최근 12시간</option>
										<option value="6">최근 6시간</option>
									</select>
								</div>
							</div>
							<!--/ table 버튼 -->
						</div></li>

					<li class="list02"><a href="#">다중 차량</a>
						<div class="tab-list">
							<div class="multiple clr">
								<div class="left" style="margin-right: 5px;margin-left: 5px;">
									<iframe id="multiMap0" src="/map/openMap?searchType=vehicle"	width="100%" height="300" frameborder="0" style="border: 0"
										allowfullscreen></iframe>
									<div class="select" style="height:50px">
										<!-- <input type="radio" id="account0" name="r_0" value="user" checked />
										<label for="account0">&nbsp;사용자</label>&nbsp;&nbsp; 
										<input type="radio" id="vehicle0" name="r_0" value="vehicle" />
										<label for="vehicle0">&nbsp;차량</label> 
										<select name="userSelect0" id="userSelect0">
											<option value="">전체 사용자</option>
										</select> 
										<select name="vehicleSelect0" id="vehicleSelect0" style="display: none">
											<option value="">전체 차량</option>
										</select> -->
										<button type="button" class="delete map_del">삭제</button>
									</div>
								</div>

								<div class="right" style="margin-right: 5px;margin-left: 5px;">
									<strong>추가 관제할<br />사용자 혹은 차량을 선택해주세요.
									</strong>

									<div class="select">
										<input type="radio" id="accountR" name="r_a" value="user" checked />
										<label for="accountR">&nbsp;사용자</label>&nbsp;&nbsp; 
										<input type="radio" id="vehicleR" name="r_a" value="vehicle"/>
										<label for="vehicleR">&nbsp;차량</label>
										<div class="mgt20">
											<div id="userDiv">
                                                    <input type="text" name="name" id="name" placeholder="사용자 검색" />
                                                    <input type="hidden" name="accountKey" id="accountKey" />
                                                    <button type="button" class="btn btn04 md" id="userPop">검색</button>
											</div>
											<div id="vehicleDiv" style="display: none">
												 <input type="text" name="plateNum" id="plateNum" placeholder="차량 검색" />
                                                 <input type="hidden" name="vehicleKey" id="vehicleKey" />
                                                 <button type="button" class="btn btn04 md" id="vehiclePop">검색</button>
											</div>
										</div>
									</div>

									<div class="btn-bottom">
										<button type="button" class="btn btn01 map_add">추가</button>
									</div>
								</div>
							</div>
						</div></li>
				</ul>

				<!-- 안내 -->
				<div class="box-info">
					<h3 class="hidden">안내</h3>
					<div class="badge-info">
						<dl>
							<dt>[운행중]</dt>
							<dd>
								<div class="badge icon01">배차중</div>
								<div class="badge icon02">미배차중</div>
							</dd>
						</dl>

						<dl>
							<dt>[주차중]</dt>
							<dd>
								<div class="badge icon03">배차중</div>
								<div class="badge icon04">미배차중</div>
							</dd>
						</dl>
					</div>
					<h4>위치 안내</h4>
					<ul>
						<li>위치확인을 클릭하시면 사용자 휴대폰 App을 통해 최종 등록된 주차위치가 아래 지도에 표시됩니다. <br />
							(사용자의 GPS 활용 미동의, GPS 오동작, 지하주차장 이동 등 GPS 수신상태로 고르지 못할 경우 위치조회가
							정확하지 않을 수 있습니다.)
						</li>
					</ul>
				</div>
				<!--/ 안내 -->
			</div>
		</div>
		<!--/ 콘텐츠 본문 -->
	</div>
</section>
<!--/ 본문 -->