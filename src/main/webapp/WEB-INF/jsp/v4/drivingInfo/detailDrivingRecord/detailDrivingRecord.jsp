<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	 // 달력
    $("#start-date").datepicker({
        dateformat: 'yy-mm-dd'
    });

    $("#end-date").datepicker({
        dateformat: 'yy-mm-dd'
    });
    
	<c:if test="${V4.corp.corpType eq '4'}">  
	var thead = [
	         {'회사명' : '10%'},
	         {'차량 및 사용자' : '25%'},
	         {'운행정보' : '41%'},
	         {'<select name="" class="arrow"><option value="">차량상태</option><option value="">정상</option><option value="">고장</option></select>' : '12%'},
	         {'<select name="" class="arrow"><option value="">경고</option><option value="">지역</option><option value="">시간</option></select>' : '12%'}
	];
	</c:if>
	<c:if test="${V4.corp.corpType ne '4'}">
	var thead = [
             {'차량 및 사용자' : '25%'},
	         {'운행정보' : '51%'},
	         {'<select name="" class="arrow"><option value="">차량상태</option><option value="">정상</option><option value="">고장</option></select>' : '12%'},
	         {'<select name="" class="arrow"><option value="">경고</option><option value="">지역</option><option value="">시간</option></select>' : '12%'}
	    ];
	</c:if>
	///api/{ver}/trip
	
	
    
    var drivingDetailSetting = {
    		thead : thead
    		,bodyHtml : (function(data){
    			var strHtml = "";
    			for(i in data.result){
    				var obj = data.result[i];
    				console.log(obj);
    				strHtml += '<tbody>';
    				strHtml += '    <tr>';
    				<c:if test="${V4.corp.corpType eq '4'}">
    				strHtml += '        <td></td>'; //회사명
    				</c:if>
    				strHtml += '        <td>';
    				strHtml += '            <div class="car-img">';
    				strHtml += '                <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" alt="" /></span><br />';
    				strHtml += '                <span class="num">'+getProperty(obj,"vehicle.vehicleModel.modelMaster")+' '+getProperty(obj,"vehicle.plateNum")+'</span>';
    				strHtml += '            </div>';
    				strHtml += '        </td>';
    				strHtml += '        <td class="left">';
    				strHtml += '            <div class="driving-info with-btn">';
    				strHtml += '                <ul>';
    				strHtml += '                    <li>';
    				strHtml += '                        <strong class="title w40">출발</strong>';
    				strHtml += '                        <span>'+convertDateUint(new Date(obj.startDate),_unitDate,_timezoneOffset,1)+' '+getProperty(obj,"startAddr")+' </span>';
    				strHtml += '                    </li>';
    				strHtml += '                    <li>';
    				strHtml += '                        <strong class="title w40">도착</strong>';
    				strHtml += '                        <span>'+convertDateUint(new Date(obj.endDate),_unitDate,_timezoneOffset,1)+' '+convertNullString(getProperty(obj,"endAddr"),'위치 정보가 없습니다.')+'</span>';
    				strHtml += '                    </li>';
    				strHtml += '                </ul>';
    				strHtml += '                <button type="button" class="btn-img location vehicleLocation" data-tripkey='+obj.tripKey+' data-vehiclekey='+obj.vehicleKey+'>위치</button>';
    				strHtml += '            </div>';
    				strHtml += '        </td>';
    				if(obj.dtcOn == "0"){
    					strHtml += '		<td class="car-state-normal">●</td>';	
    				}else{
    					strHtml += '		<td class="car-state-error">●</td>';
    				}
    				
    				
    				strHtml += '        <td>';
    				strHtml += '            이수지역<br />';
    				strHtml += '            <button class="states-value state04 drivingDetail" data-state="close">상세기록</button>';
    				strHtml += '        </td>';
    				strHtml += '    </tr>';
    				strHtml += '    <tr class="detail" style="display:none">';
    				<c:if test="${V4.corp.corpType eq '4'}">
    				strHtml += '        <td>'+getProperty(obj,'vehicle.corp.corpName')+'</td>';
    				</c:if>
    				
    				
    				
    				strHtml += '        <td>';
    				strHtml += '            <div class="user-img">';
    				strHtml += '                <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/common/user-default.png"alt="" /></span><br />';
    				
    				if(obj.driver){
    					strHtml += '                <span class="name">'+obj.driver.name+'</span>';	
    				}else{
    					strHtml += '                <span class="name"></span>';
    				}
    				
    				
    				strHtml += '            </div>';
    				strHtml += '        </td>';
    				strHtml += '        <td colspan="3" class="left">';
    				strHtml += '            <div class="car-info">';
    				strHtml += '                <ul class="grade">';
    				strHtml += '                    <li><span>안전점수</span>'+calScore(parseFloat(obj.safeScore).toFixed(1))+'</li>';
    				strHtml += '                    <li><span>연비점수</span>'+calScore((obj.avgFco/obj.fuelEfficiency*100).toFixed(1))+'</li>';
    				strHtml += '                    <li><span>에코점수</span>'+calScore(parseFloat(obj.ecoScore).toFixed(1))+'</li>';
    				strHtml += '                </ul>';
    				strHtml += '                <ul class="spec">';
    				strHtml += '                    <li><span>주행시간</span>'+secondToTime(obj.drivingTime)+'</li>';
    				strHtml += '                    <li><span>평균속도</span>'+obj.meanSpeed+' km/h</li>';
    				strHtml += '                    <li><span>냉각수</span>'+obj.coolantTemp+' ℃</li>';
    				strHtml += '                    <li><span>주행거리</span>'+minusError(number_format((obj.distance/1000).toFixed(1)))+' km</li>';
    				strHtml += '                    <li><span>최고속도</span>'+obj.highSpeed+' km/h</li>';
    				strHtml += '                    <li><span>배터리</span>'+number_format(parseFloat(obj.ecuVolt).toFixed(1))+' v</li>';
    				strHtml += '                    <li><span>연비</span>'+number_format(parseFloat(obj.avgFco).toFixed(1))+' km/ℓ</li>';
    				strHtml += '                    <li><span>연료</span>'+number_format((obj.fco/1000).toFixed(1))+' ℓ</li>';
    				strHtml += '                    <li><span>발전기</span>'+number_format(parseFloat(obj.deviceVolt).toFixed(1))+' v</li>';
    				strHtml += '                    <li><span>co2배출량</span>'+number_format(obj.co2Emission)+' ㎎</li>';
    				strHtml += '                    <li><span>퓨얼컷</span>'+number_format(obj.fuelCutTime)+' 초</li>';
    				strHtml += '                    <li><span>공회전</span>'+number_format(obj.idleTime)+' 초</li>';
    				strHtml += '                    <li><span>웜업시간</span>'+number_format(obj.warmupTime)+' 초</li>';
    				strHtml += '                </ul>';
    				strHtml += '            </div>';
    				strHtml += '        </td>';
    				strHtml += '    </tr>';
    				strHtml += '</tbody>';
    			}
    			return strHtml;
    		})
    		,limit : parseInt($(".func_limitChange").val())
    		,pagePerGroup : 10 //pagingGroup size
    		,url : "/api/1/trip"
    		,param : {"counting" : true}
    		,http_post_option : {
    			requestMethod : "GET"
    			,header : {key : "${_KEY}"}
    		}
    		,initSearch : true //생성과 동시에 search		
    		,loadingBodyViewer : true //로딩중!! 표시됨
    		,debugMode : true
    	}
    
    $('#drivingDetailList').commonList(drivingDetailSetting);
	
    $(document).on('click','.drivingDetail',function(){
    	var self = $(this);
    	//close면 닫혀있는상태 열어준다
    	var state = self.data('state');
    	if(state == "close"){
    		self.parent().parent().next().show();	
    		self.data('state','open');
    	}else{//open 상태면 닫아준다.
    		self.parent().parent().next().hide();
    		self.data('state','close');
    	}
    });
    
    $(document).on('click','.vehicleLocation',function(){
		var tripKey = $(this).data('tripkey');
		var vehicleKey = $(this).data('vehiclekey');
		
		console.log(tripKey);
		
		//searchType이 trip 인경우
		//tripKey랑 vehicleKey를 넘겨주어야 한다.
		$('#mapFrame').attr('src','/map/openMap?searchType=trip&tripKey='+tripKey+'&vehicleKey='+vehicleKey);
		$("#vehicleLocationDialog").dialog("open");
	});
	
	$("#vehicleLocationDialog").dialog({
	    autoOpen: false,
	    show: {
	        duration: 500
	    },
	    width: '960',
	    modal: true
	});
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>상세운행기록</h2>
                    <span>등록차량의 특정시점 사용자 및 운행 기록을 조회하실 수 있습니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="driving-page">
                    <div class="search-bus-route">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                            <div class="item">
                                <span>
									총 보유 운행건수
									<strong id="totCnt"></strong>건
								</span>
                            </div>

                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <!-- 상단 검색 -->
                        <div class="top-search mgt20">
                            <select name="">
								<option value="">전체</option>
							</select>
                            <input type="text" name="" />
                            <button class="btn btn03">조회</button>
                        </div>
                        <!--/ 상단 검색 -->

                        <h4 class="tit2 mgt40">운행일시</h4>
                        <table class="table mgt10">
                            <caption>운행기록</caption>
                            <colgroup>
                                <col style="width:16.35%" />
                                <col />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <th scope="row">기간</th>
                                    <td>
                                        <input type="checkbox" name="" />&nbsp;전체&nbsp;&nbsp;
                                        <input type="checkbox" name="" />&nbsp;오늘&nbsp;&nbsp;
                                        <input type="checkbox" name="" />&nbsp;1주일&nbsp;&nbsp;
                                        <input type="checkbox" name="" />&nbsp;1개월&nbsp;&nbsp;
                                        <input type="checkbox" name="" />&nbsp;3개월&nbsp;&nbsp;
                                        <input type="checkbox" name="" />&nbsp;6개월
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">날짜</th>
                                    <td>
                                        <div class="division">
                                            <div class="col-6">
                                                <input type="text" name="" id="start-date" />
                                            </div>
                                            <div class="space">&nbsp;-&nbsp;</div>
                                            <div class="col-6">
                                                <input type="text" name="" id="end-date" />
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">요일</th>
                                    <td>
                                        <input type="checkbox" name="" />&nbsp;월&nbsp;&nbsp;
                                        <input type="checkbox" name="" />&nbsp;화&nbsp;&nbsp;
                                        <input type="checkbox" name="" />&nbsp;수&nbsp;&nbsp;
                                        <input type="checkbox" name="" />&nbsp;목&nbsp;&nbsp;
                                        <input type="checkbox" name="" />&nbsp;금&nbsp;&nbsp;
                                        <input type="checkbox" name="" />&nbsp;토&nbsp;&nbsp;
                                        <input type="checkbox" name="" />&nbsp;일
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">시간</th>
                                    <td>
                                        <input type="text" name="" style="width:50px" />&nbsp;시&nbsp;&nbsp;
                                        <input type="text" name="" style="width:50px" />&nbsp;분&nbsp;&nbsp;-&nbsp;&nbsp;
                                        <input type="text" name="" style="width:50px" />&nbsp;시&nbsp;&nbsp;
                                        <input type="text" name="" style="width:50px" />&nbsp;분&nbsp;&nbsp;
                                    </td>
                                </tr>
                            </tbody>
                        </table>


                        <h4 class="tit2 mgt40">운행정보</h4>
                        <table class="table mgt10">
                            <caption>운행정보</caption>
                            <colgroup>
                                <col style="width:16.35%" />
                                <col style="width:33.65%" />
                                <col style="width:16.35%" />
                                <col style="width:33.65%" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <th scope="row">최고속도</th>
                                    <td>
                                        <div class="division">
                                            <div class="col-6">
                                                <input type="text" name="" />
                                            </div>
                                            <div class="space">&nbsp;-&nbsp;</div>
                                            <div class="col-6">
                                                <input type="text" name="" />
                                            </div>
                                        </div>
                                    </td>
                                    <th scope="row">연비</th>
                                    <td>
                                        <div class="division">
                                            <div class="col-6">
                                                <input type="text" name="" />
                                            </div>
                                            <div class="space">&nbsp;-&nbsp;</div>
                                            <div class="col-6">
                                                <input type="text" name="" />
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">시간</th>
                                    <td>
                                        <div class="division">
                                            <div class="col-6">
                                                <input type="text" name="" />
                                            </div>
                                            <div class="space">&nbsp;-&nbsp;</div>
                                            <div class="col-6">
                                                <input type="text" name="" />
                                            </div>
                                        </div>
                                    </td>
                                    <th scope="row">&nbsp;</th>
                                    <td>&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>

                        <h4 class="tit2 mgt40">운행지역</h4>
                        <table class="table mgt10">
                            <colgroup>
                                <col style="width:16.35%" />
                                <col style="width:33.65%" />
                                <col style="width:16.35%" />
                                <col style="width:33.65%" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <th scope="row">시도</th>
                                    <td>
                                        <select name="">
											<option value="">전체 시/도</option>
										</select>
                                    </td>
                                    <th scope="row">시/군/구</th>
                                    <td>
                                        <select name="">
											<option value="">전체 시/군/구</option>
										</select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>


                        <!-- table 버튼 -->
                        <div class="btn-function">
                            <div class="left">
                                <div class="txt">
                                    검색조건대상건수 : <strong class="red">22,764건</strong>
                                </div>
                            </div>
                            <div class="right">
                                <select class="func_limitChange">
									<option value="5" selected>5건씩 보기</option>
									<option value="10">10건씩 보기</option>
									<option value="20">20건씩 보기</option>
									<option value="50">20건씩 보기</option>
								</select>
                            </div>
                        </div>
                        <!--/ table 버튼 -->

                        <table class="table list mgt20" id="drivingDetailList">
                            <%-- <caption>운행기록 리스트</caption>
                            <colgroup>
                                <col style="width:10%" />
                                <col style="width:25%" />
                                <col />
                                <col style="width:12%" />
                                <col style="width:12%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="col">회사명</th>
                                    <th scope="col">차량 및 사용자</th>
                                    <th scope="col">운행정보</th>
                                    <th scope="col">
                                        <select name="" class="arrow">
											<option value="">차량상태</option>
											<option value="">정상</option>
											<option value="">고장</option>
										</select>
                                    </th>
                                    <th scope="col">
                                        <select name="" class="arrow">
											<option value="">경고</option>
											<option value="">지역</option>
											<option value="">시간</option>
										</select>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>
                                        <div class="car-img">
                                            <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" alt="" /></span><br />
                                            <span class="num">쏘나타 63호 5703</span>
                                        </div>
                                    </td>
                                    <td class="left">
                                        <div class="driving-info with-btn">
                                            <ul>
                                                <li>
                                                    <strong class="title w40">출발</strong>
                                                    <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                </li>
                                                <li>
                                                    <strong class="title w40">도착</strong>
                                                    <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                </li>
                                            </ul>
                                            <button class="btn-img location">위치</button>
                                        </div>
                                    </td>
                                    <td class="car-state-normal">●</td>
                                    <td>
                                        이수지역<br />
                                        <button class="states-value state04 btn-toggle" onClick="javascript:detail(1)">상세기록</button>
                                    </td>
                                </tr>
                                <tr id="details1" class="detail" style="display:none">
                                    <td>자스텍엠</td>
                                    <td>
                                        <div class="user-img">
                                            <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/common/user-default.png"alt="" /></span><br />
                                            <span class="name">maumgolf / 카카오VX</span>
                                        </div>
                                    </td>
                                    <td colspan="3" class="left">
                                        <div class="car-info">
                                            <ul class="grade">
                                                <li><span>안전점수</span>E(0.0점)</li>
                                                <li><span>연비점수</span>A(89.6점)</li>
                                                <li><span>에코점수</span>E(0.0점)</li>
                                            </ul>

                                            <ul class="spec">
                                                <li><span>주행시간</span>0시간 10분 01초</li>
                                                <li><span>평균속도</span>0 km/h</li>
                                                <li><span>냉각수</span>0 ℃</li>
                                                <li><span>주행거리</span>5.4 km</li>
                                                <li><span>최고속도</span>0 km/h</li>
                                                <li><span>배터리</span>0.0 v</li>
                                                <li><span>연비</span>11.4 km/ℓ</li>
                                                <li><span>연료</span>0.5 ℓ</li>
                                                <li><span>발전기</span>0.0 v</li>
                                                <li><span>co2배출량</span>0 ㎎</li>
                                                <li><span>퓨얼컷</span>0 초</li>
                                                <li><span>공회전</span>0 초</li>
                                                <li><span>웜업시간</span>0 초</li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            </tbody> --%>
                        </table>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->
<!-- 차량 위치 팝업 -->
<div id="vehicleLocationDialog" title="경로 확인">
    <div class="map clr">
        <div class="left">
        <iframe id="mapFrame" src="about:blank" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>
    <div class="mgt20">
    	※ 사용자의 GPS 활용 미동의, GPS 오동작, 지하주차장 등과 GPS 수신상태로 고르지 못할 경우 위치가 정확하지 않을 수 있습니다.
    	</br>
		위치가 정확하지 않은 경우 정확한 위치를 직접 지도에서 찾아 표시하거나 입력해주세요.
    </div>
</div>
<!-- 차량 위치 팝업 -->   