<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	$("#start-date").datepicker({
        dateformat: 'yy-mm-dd'
    });

    $("#end-date").datepicker({
        dateformat: 'yy-mm-dd'
    });
    
   /*  <c:if test="${V4.corp.corpType eq '4'}">    
    var thead = [
                 {'회사명' : '10%'},
                 {'날짜' : '17%'},
                 {'<select name="" class="arrow"><option value="">알림</option><option value="">고장,소모품</option><option value="">단말탈부착</option><option value="">정기검사일 알림</option></select>' : '20%'},
                 {'내용' : '53%'}
		         ];
    </c:if> */
    /* <c:if test="${V4.corp.corpType ne '4'}">msg */
    var thead = [
				 {'<input type="checkbox">' : '10%'},
                 {'날짜' : '17%'},
                 {'<select name="" class="arrow"><option value="">알림</option><option value="">고장,소모품</option><option value="">단말탈부착</option><option value="">정기검사일 알림</option></select>' : '20%'},
                 {'내용' : '53%'}
		         ];
    /* </c:if> */
    
    var alarmSetting = {
    		thead : thead
    		,bodyHtml : (function(data){
    			var strHtml = "";
    			for(i in data.result){
    				var obj = data.result[i];
    				strHtml += '<tr>';
    				strHtml += '<td><input type="checkbox" data-mailboxkey="'+obj.mailBoxKey+'"></td>';
    				strHtml += '	<td>'+convertDateUint(new Date(obj.regDate),_unitDate,_timezoneOffset,1)+'</td>';
    				strHtml += '	<td>'+obj.title+'</td>';
    				strHtml += '	<td class="left">'+obj.msg+'</td>';
    				strHtml += '</tr>';
    			}
    			return strHtml;
    		})
    		,limit : parseInt($(".func_limitChange").val())
    		,pagePerGroup : 10 //pagingGroup size
    		,url : "/api/1/mailBox"
    		,param : {}
    		,http_post_option : {
    			requestMethod : "GET"
    			,header : {key : "${_KEY}"}
    		}
    		,initSearch : true //생성과 동시에 search		
    		,loadingBodyViewer : true //로딩중!! 표시됨
    		,debugMode : true
    	}
    
    $('#alarmList').commonList(alarmSetting);
    
    
    $(".func_limitChange").on("change",function(){
		$("#top_btn_search").trigger("click");
	});
    
	$("#top_btn_search").on("click",$.debounce(200,1,function(){
		
		var param = {searchType:$("#top_searchType").val() , searchText : $("#top_searchText").val()}
		
		var limit = parseInt($(".func_limitChange").val());	 
		$('#alarmList').commonList("setParam",param);
		$('#alarmList').commonList("setLimit",limit).search();
		
	}));
	
	//선택삭제
	$('#alarmDeleteBtn').on('click',function(){
		console.log(this);
		
		
		var checkBox = $('#alarmList input[type="checkbox"]:checked');
		
		var keyArr = [];
		for(var i = 0 ; i < checkBox.length; i++){
			var mailBoxKey = $(checkBox[i]).data('mailboxkey');
			keyArr.push(mailBoxKey);
		}
		
		if(keyArr.length != 0){
			var sendMailBoxKey = keyArr.join(',');
			
			 $V4.http_post("/api/1/mailBox/"+sendMailBoxKey,{},{
			       requestMethod:"DELETE"
				  ,header:{key:"${_KEY}"}
				  ,success : function(data){
						alert('삭제 되었습니다.');
						$("#top_btn_search").trigger("click");
				  }
			 }); 	
		}
		
		
	});
	//전체삭제
	$('#alarmDeleteAllBtn').on('click',function(){
		
		if(confirm("알림을 전체 삭제 하시겠습니까?")){
			$V4.http_post("/api/1/mailBox",{},{
			       requestMethod:"DELETE"
				  ,header:{key:"${_KEY}"}
				  ,success : function(data){
						alert('삭제 되었습니다.');
						$("#top_btn_search").trigger("click");
				  }
			 }); 
		}
		
	});
    
	$('#top_searchText').on('keyup',function(e){
		if(e.keyCode == 13){
			$("#top_btn_search").trigger("click");
		}
	});
	
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>차량 주요알림</h2>
                    <span>차량과 관련된 모든 알림을 확인할 수 있습니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="driving-page">
                    <div class="">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <!-- 상단 검색 -->
                        <div class="top-search mgt20">
                            <select name="" id="top_searchType">
								<option value="">전체</option>
								<option value="title">알림</option>
								<option value="msg">내용</option>
							</select>
                            <input type="text" name="" id="top_searchText" />
                            <button type="button" class="btn btn03" id="top_btn_search">조회</button>
                        </div>
                        <!--/ 상단 검색 -->

						<!-- table 버튼 -->
                        <div class="btn-function row-2">
                            <div class="left">
                            </div>
                            <div class="right">
                            	<button type="button" class="delete" id="alarmDeleteBtn">선택삭제</button>
                            	<button type="button" class="delete" id="alarmDeleteAllBtn">전체삭제</button>
                                <select class="func_limitChange">
									<option value="5" selected>5건씩 보기</option>
									<option value="10">10건씩 보기</option>
									<option value="20">20건씩 보기</option>
									<option value="50">20건씩 보기</option>
								</select>
                            </div>
                        </div>
                        <!--/ table 버튼 -->

						<table class="table list mgt20" id="alarmList">
							
						</table>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->