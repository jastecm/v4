<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	$("#start-date").datepicker({
        dateformat: 'yy-mm-dd'
    });

    $("#end-date").datepicker({
        dateformat: 'yy-mm-dd'
    });
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>미배차 운행</h2>
                    <span>배차 신청없이 운행된 기록이며, 후결재가 필요합니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="driving-page">
                    <div class="box-layout register-vehicle-schedule">
                        <div class="title">
                            <h3 class="tit3">배차 정보</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
                            <div class="info-txt">
                                아래 항목을 순서대로 정확하게 입력한 뒤, 하단의 배차신청등록버튼을 누르면 신청이 완료됩니다.
                            </div>

                            <table class="table mgt20">
                                <caption>배차 신청</caption>
                                <tbody>
                                    <tr>
                                        <th scope="row">사용자</th>
                                        <td>
                                            <div class="division">
                                                <div class="col-3">
                                                    <input type="text" placeholder="입력 및 검색" />
                                                </div>
                                                <div class="space">&nbsp;</div>
                                                <div class="col-9">
                                                    <button class="btn btn04 md" id="bus-open">검색</button>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">용도</th>
                                        <td>
                                            <select name="">
												<option value="">업무용</option>
											</select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">사용목적</th>
                                        <td>
                                            <input type="text" placeholder="짧고 명확하게 기업. ex) 자사방문, 영업 등" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">동승자</th>
                                        <td>
                                            <input type="text" placeholder="이름/부서/직급" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">목적지</th>
                                        <td>
                                            <input type="text" placeholder="상세 방문 장소" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">활용계획</th>
                                        <td>
                                            <textarea class="w100" placeholder="구체적인 활용 계획"></textarea>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <table class="table list mgt20">
                                <caption>배차 가능 차량 목록</caption>
                                <colgroup>
                                    <col style="width:40%" />
                                    <col style="width:60%" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th scope="col">차량정보</th>
                                        <th scope="col">배차시작/종료</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="car-img">
                                                <span class="img"><img src="./img/vehicle/S103.png" alt="" /></span>
                                                <span class="num">쏘나타 63호 5703</span>
                                            </div>
                                        </td>
                                        <td class="left">
                                            <div class="driving-info">
                                                <ul>
                                                    <li>
                                                        <strong class="title w40">시작</strong>
                                                        <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                    </li>
                                                    <li>
                                                        <strong class="title w40">종료</strong>
                                                        <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <!-- 하단 버튼 -->
                            <div class="btn-bottom">
                                <button class="btn btn02">취소</button>
                                <button class="btn btn03">후결신청</button>
                            </div>
                            <!--/ 하단 버튼 -->
                        </div>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->