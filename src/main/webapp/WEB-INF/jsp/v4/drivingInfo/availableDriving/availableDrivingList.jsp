<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	$('#bulkRegBtn').on('click',function(){
		$V4.move('/drivingInfo/availableDriving/reg');
	});
	
	var thead = [
                 '<input type="checkbox" />',
                 '차량 및 사용자',
                 '운행정보',
                 '경로',
                 '<select name="" class="arrow"><option value="">전체</option><option value="">신청가능</option><option value="">진행중</option></select>'
        ];
	
	var lazySetting = {
    		thead : thead
    		,bodyHtml : (function(data){
    			var strHtml = "";
    			for(i in data.result){
    				var obj = data.result[i];
    				console.log(obj);
    				
    				strHtml += '<tr>';
    				strHtml += '    <td><input type="checkbox" data-tripkey='+obj.tripKey+' /></td>';
    				strHtml += '    <td>';
    				strHtml += '        <div class="car-img">';
    				strHtml += '            <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" alt="" /></span><br />';
    				strHtml += '            <span class="num">'+getProperty(obj,"vehicle.vehicleModel.modelMaster")+' '+getProperty(obj,"vehicle.plateNum")+'</span><br />';
    				
    				
    				//미배차인데 사용자가 누군지 알수가 없다.
    				if(obj.account){
     					if(obj.account.group){
     						var accountName = obj.account.name;
     						var accountPosition = obj.account.corpPosition;
     						var accountGroupNm = convertNullString(obj.account.group.groupNm);
     						strHtml += '    <span class="num">'+accountGroupNm+'/'+accountName+'/'+accountPosition+'</span>';
     					}
     				}else{
     					strHtml += '            <span class="num"></span>';
     				}
    				
    				strHtml += '        </div>';
    				strHtml += '    </td>';
    				strHtml += '    <td class="left">';
    				strHtml += '        <div class="driving-info">';
    				strHtml += '            <ul>';
    				strHtml += '                <li>';
    				strHtml += '                    <strong class="title">출발</strong>';
    				strHtml += '                    <span>'+convertDateUint(new Date(obj.startDate),_unitDate,_timezoneOffset,1)+'</span>';
    				strHtml += '                </li>';
    				strHtml += '                <li>';
    				strHtml += '                    <strong class="title">도착</strong>';
    				strHtml += '                    <span>'+convertDateUint(new Date(obj.endDate),_unitDate,_timezoneOffset,1)+'</span>';
    				strHtml += '                </li>';
    				strHtml += '                <li>';
    				strHtml += '                    <strong class="title">주행시간</strong>';
    				strHtml += '                    <span>'+secondToTime(obj.drivingTime)+'</span>';
    				strHtml += '                </li>';
    				strHtml += '                <li>';
    				strHtml += '                    <strong class="title">주행거리</strong>';
    				strHtml += '                    <span>'+minusError(number_format((obj.distance/1000).toFixed(1)))+'km</span>';
    				strHtml += '                </li>';
    				strHtml += '            </ul>';
    				strHtml += '        </div>';
    				strHtml += '    </td>';
    				strHtml += '    <td><button type="button" class="btn-img location vehicleLocation" data-tripkey='+obj.tripKey+' data-vehiclekey='+obj.vehicleKey+'>경로</button></td>';
    				strHtml += '    <td><span class="important">후결재필요</span></td>';
    				strHtml += '</tr>';
    				
    			}
    			return strHtml;
    		})
    		,limit : parseInt($(".func_limitChange").val())
    		,offset : 0
    		,url : "/api/1/trip"
    		,param : {"searchNeedAllocate":"1","counting":true}
    		,http_post_option : {
    			requestMethod : "GET"
    			,header : {key : "${_KEY}"}
    		}
    		,initSearch : true //생성과 동시에 search		
    		,loadingBodyViewer : true //로딩중!! 표시됨
    		,debugMode : true
    	}
    
    $('#contentsTable').commonLazy(lazySetting);
	
	//미배차 운행 경로 보기
	$(document).on('click','.vehicleLocation',function(){
		var tripKey = $(this).data('tripkey');
		var vehicleKey = $(this).data('vehiclekey');
		
		console.log(tripKey);
		
		//searchType이 trip 인경우
		//tripKey랑 vehicleKey를 넘겨주어야 한다.
		$('#mapFrame').attr('src','/map/openMap?searchType=trip&tripKey='+tripKey+'&vehicleKey='+vehicleKey);
		$("#vehicleLocationDialog").dialog("open");
	});
	
	$("#vehicleLocationDialog").dialog({
	    autoOpen: false,
	    show: {
	        duration: 500
	    },
	    width: '960',
	    modal: true
	});
	
});
</script>
<form id="searchFrm" style="display:none">
	<input type="hidden" name="searchType" value="" />
	<input type="hidden" name="searchText" value="" />
	<input type="hidden" name="searchNeedAllocate" value="1"/>
</form>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>미배차 운행</h2>
                    <span>배차 신청없이 운행된 기록이며, 후결재가 필요합니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="driving-page">
                    <div class="none-dispatch">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                            <div class="item">
                                <span>
									미배차 운행건수
									<strong>10</strong>건
								</span>
                            </div>

                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <!-- 상단 검색 -->
                        <div class="top-search mgt20">
                            <select name="">
								<option value="">전체</option>
							</select>
                            <input type="text" name="" />
                            <button class="btn btn03">조회</button>
                        </div>
                        <!--/ 상단 검색 -->

                        <!-- table 버튼 -->
                        <div class="btn-function row-2">
                            <div class="left">
                                <select name="">
									<option value="">최근 운행순</option>
								</select>

                                <div class="date">
                                    <button class="active">전체</button>
                                    <button>오늘</button>
                                    <button>1주일</button>
                                    <button>1개월</button>
                                    <button>3개월</button>
                                    <button>6개월</button>
                                    <div class="direct">
                                        <input type="text" id="start-date" /> &nbsp;~&nbsp;
                                        <input type="text" id="end-date" />
                                    </div>
                                    <!-- 기획서에는 직접입력이 셀렉트박스로 되어 있는데, 사용자가 어떻게 날짜를 입력 해야 하는지 모르겠어서 임의로 변경했습니다.-->
                                </div>
                            </div>
                            <div class="right">
                               <select class="func_limitChange">
									<option value="5" selected>5건씩 보기</option>
									<option value="10">10건씩 보기</option>
									<option value="20">20건씩 보기</option>
									<option value="50">20건씩 보기</option>
								</select>
                            </div>
                        </div>
                        <!--/ table 버튼 -->

                        <div class="scroll-table mgt20">
                            <table class="table list" id="contentsTable">
                                <%-- <caption>매배차 운행 목록</caption>
                                <colgroup>
                                    <col style="width:8%" />
                                    <col style="width:20%" />
                                    <col />
                                    <col style="width:10%" />
                                    <col style="width:13%" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th scope="col"><input type="checkbox" /></th>
                                        <th scope="col">차량 및 사용자</th>
                                        <th scope="col">운행정보</th>
                                        <th scope="col">경로</th>
                                        <th scope="col">
                                            <select name="" class="arrow">
												<option value="">전체</option>
												<option value="">신청가능</option>
												<option value="">진행중</option>
											</select>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>
                                            <div class="car-img">
                                                <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" alt="" /></span><br />
                                                <span class="num">쏘나타 63호 5703</span><br />
                                                <span class="num">총무팀/김계복/팀장</span>
                                            </div>
                                        </td>
                                        <td class="left">
                                            <div class="driving-info">
                                                <ul>
                                                    <li>
                                                        <strong class="title">출발</strong>
                                                        <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                    </li>
                                                    <li>
                                                        <strong class="title">도착</strong>
                                                        <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                    </li>
                                                    <li>
                                                        <strong class="title">주행시간</strong>
                                                        <span>0시간 14분 17초</span>
                                                    </li>
                                                    <li>
                                                        <strong class="title">주행거리</strong>
                                                        <span>3.4km</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td><button class="btn-img location">경로</button></td>
                                        <td><span class="important">후결재필요</span></td>
                                    </tr>
                                </tbody> --%>
                            </table>
                        </div>

                        <!-- 하단 버튼 -->
                        <div class="btn-bottom">
                            <button id="bulkRegBtn" type="button" class="btn btn01">선택항목 일괄후결신청</button>
                        </div>
                        <!--/ 하단 버튼 -->
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->
        
<!-- 차량 위치 팝업 -->
<div id="vehicleLocationDialog" title="경로 확인">
    <div class="map clr">
        <div class="left">
        <iframe id="mapFrame" src="about:blank" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>
    <div class="mgt20">
    	※ 사용자의 GPS 활용 미동의, GPS 오동작, 지하주차장 등과 GPS 수신상태로 고르지 못할 경우 위치가 정확하지 않을 수 있습니다.
    	</br>
		위치가 정확하지 않은 경우 정확한 위치를 직접 지도에서 찾아 표시하거나 입력해주세요.
    </div>
</div>
<!-- 차량 위치 팝업 -->   