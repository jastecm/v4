<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">

var lazy;
var start = "";
var end = "";
$(function(){
	
	var now = new Date();
	$('#startDate').val(now.yyyymmdd());
	var startMinute = now.getMinutes();
	var startHour = now.getHours();
	if(startMinute >= 0 && startMinute <= 10  ){
		startMinute = "11";
	}else if(startMinute >= 11 &&  startMinute <= 20){
		startMinute = "21";
	}else if(startMinute >= 21 && startMinute <= 30){
		startMinute = "31";                      
	}else if(startMinute >= 31 && startMinute <= 40){
		startMinute = "41";                      
	}else if(startMinute >= 41 && startMinute <= 50){
		startMinute = "51";                      
	}else if(startMinute >= 51 && startMinute <= 59){
		startMinute = "01";          
		startHour = startHour + 1;
	}
	
	$('#startHour option[value="'+(startHour <= 9 ? "0"+startHour : startHour)+'"]').prop("selected", true);
	$('#startMinute option[value="'+startMinute+'"]').prop("selected", true);
	
	
	$("#startDate").datepicker({
        dateformat: 'yy-mm-dd'
    });

    $("#endDate").datepicker({
        dateformat: 'yy-mm-dd'
    });
    
    $("#userPop").data({
		type : "user",
		http_post_option : {
			header : {
				key : "${_KEY}"
			}
		},
		selType : "radio",
			action : {
				ok : function(arrSelect, inputObj, valueObj) {
					$.each(arrSelect, function() {
						var info = base64ToJsonObj($(arrSelect).data("info"));
						$(inputObj).val(info.name);
						$(valueObj).val(info.accountKey);
					});
				}
			},
			targetInput : $("#name"),
			targetInputValue : $("#accountKey")
	}).commonPop();
    
    
    $('#availableVehiclesBtn').on('click',function(){
    	
    	
    	var startDate = $('#startDate').val();
    	var endDate = $('#endDate').val();
    	if(startDate == ""){
    		alert('배차 시작날짜를 입력해주세요');
    		return;
    	}
    	if(endDate == ""){
    		alert('배차 종료날짜를 입력해주세요');
    		return;
    	}
    	
    	start = new Date($('#startDate').val() + " " + $('#startHour').val() + ":" + $('#startMinute').val()).getTime();
    	end = new Date($('#endDate').val() + " " + $('#endHour').val() + ":" + $('#endMinute').val()).getTime();
    	var now = new Date().getTime();
    	
    	if(end < start){
    		alert('시작시간이 종료시간보다 크거나 같습니다.');
    		return;
    	}
    	
    	if(start < now){
    		alert('시작시간이 현재시간보다 작거나 같습니다');
    		return;
    	}
    	
    	
    	$V4.http_post("/api/1/allocate/search/"+start+"/"+end, null, {
			requestMethod : "GET",
			header : {
				"key" : "${_KEY}"
			},
			success : function(rtv) {
				var list = rtv.result;
				console.log(list);
				$('#vehicleList').empty();
				
				if(list.length == 0){
					$('#vehicleList').html('<tr><td colspan="5">검색결과가 없습니다.</td></tr>');
					return;
				}
				
				var strHtml = "";
				for (var i = 0; i < list.length; i++) {
					var vo = list[i];
					//여기서 리스트를 만들어 준다아아..
					strHtml += '<tr>';
					strHtml += '	<td><input type="radio" name="vehicleKey" data-vehiclekey="'+vo.vehicleKey+'"></td>';
					strHtml += '	<td>'+vo.vehicleModel.modelMaster+'/'+vo.plateNum+'</td>';
					if(vo.groups && vo.groups.length != 0){
						strHtml += '	<td>';
						for(var i = 0 ; i < vo.groups.length ; i++){
							group = vo.groups[i];
							if(i == 0){
								strHtml += (group.parentGroupNm)?(convertNullString(group.parentGroupNm)):"";
								strHtml += (group.groupNm)?("/"+convertNullString(group.groupNm)):"";
							}else{
								strHtml += "</br>";
								strHtml += (group.parentGroupNm)?(convertNullString(group.parentGroupNm)):"";
								strHtml += (group.groupNm)?("/"+convertNullString(group.groupNm)):"";
							}
								 
						}
						strHtml += '</td>';
					}else{
						strHtml += '	<td></td>';
					}
					//strHtml += '	<td>'+nullToString(vo.vehicleManagerGroupName)+' '+nullToString(vo.vehicleManagerName)+'/'+nullToString(vo.corpPosition)+'</td>';
					strHtml += '	<td>'+'test'+'</td>';
					strHtml += '	<td><button class="btn-img location">위치</button></td>';
					strHtml += '</tr>';
				}
				$('#vehicleList').html(strHtml);
			},
			error : function(t) {
				
			}
		});
    });
    
    $('#vehicleAllocateCancelBtn').on('click',function(){
    	window.history.back(-1);
    });
    
    $('#vehicleAllocateRegBtn').on('click',function(){
    	if(start == ""){
    		alert('시작일자를 조회해주세요');
    		return;
    	}
    	if(end == ""){
    		alert('시작일자를 조회해주세요');
    		return;
    	}
    	
    	var accountKey = $('#accountKey').val();
    	
    	if(accountKey == ""){
    		alert('사용자를 선택해주세요');
    		return;
    	}
    	
    	
    	var vehicleRadioCheck = $('#vehicleList input[name="vehicleKey"]:checked');
    	
    	if(vehicleRadioCheck.length == 0){
    		alert('차량을 선택해 주세요');
    		return;
    	}
    	
    	var vehicleKey = vehicleRadioCheck.data('vehiclekey');
    	
    	var purpose = $('#purpose').val();
    	var title = $('#title').val();
    	var passenger = $('#passenger').val();
    	var destination = $('#destination').val();
    	var contents = $('#contents').val();
    	
    	var sendData = {
    		"accountKey" : accountKey,    		
    		"startDate" : start,
    		"endDate" : end,
    		"purpose" : purpose,
    		"title" : title,
    		"passenger" : passenger,
    		"destination" : destination,
    		"contents" : contents
    	}
    	
    	$V4.http_post("/api/1/allocate/"+vehicleKey, sendData, {
			requestMethod : "POST",
			header : {
				"key" : "${_KEY}"
			},
			success : function(rtv) {
				console.log(rtv);
			},
			error : function(t) {
				console.log(t);
			}
		});
    	
    });
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>차량 배차</h2>
                    <span>배차 신청 내역을 조회하거나, 실시간 배차 현황을 확인할 수 있습니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="driving-page">
                    <div class="box-layout register-vehicle-schedule">
                        <div class="title">
                            <h3 class="tit3">배차 신청하기</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
                            <div class="info-txt">
                                아래 항목을 순서대로 정확하게 입력한 뒤, 하단의 배차신청등록버튼을 누르면 신청이 완료됩니다.
                            </div>

                            <table class="table mgt20">
                                <caption>배차 신청</caption>
                                <tbody>
                                    <tr>
                                        <th scope="row">사용자</th>
                                        <td>
                                            <div class="division">
                                                <div class="col-3">
                                                    <input type="text" name="name" id="name" readonly="readonly" placeholder="" />
                                                    <input type="hidden" name="accountKey" id="accountKey" />
                                                </div>
                                                <div class="space">&nbsp;</div>
                                                <div class="col-9">
                                                    <button type="button" class="btn btn04 md" id="userPop">검색</button>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">용도</th>
                                        <td>
                                            <select name="purpose" id="purpose">
												<option value="1">업무용</option>
												<option value="2">출퇴근용</option>
												<option value="3">비업무용</option>
												
											</select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">사용목적</th>
                                        <td>
                                            <input type="text" name="title" id="title" placeholder="짧고 명확하게 기업. ex) 자사방문, 영업 등" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">동승자</th>
                                        <td>
                                            <input type="text" name="passenger" id="passenger" placeholder="이름/부서/직급" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">목적지</th>
                                        <td>
                                            <input type="text" name="destination" id="destination" placeholder="상세 방문 장소" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">활용계획</th>
                                        <td>
                                            <textarea class="w100" name="contents" id="contents" placeholder="구체적인 활용 계획"></textarea>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="info-txt mgt40">
                                배차신청할 개시/종료 일지를 설정한 후 검색버튼을 누르면 해당일에 배차가 가능한 차량리스트가 제공됩니다.
                            </div>

                            <div class="search-schedule">
                                <input type="text" id="startDate" placeholder="배차기간 설정" />
                                <select name="startHour" id="startHour">
                                	<option value="00">00</option>
									<option value="01">01</option>
									<option value="02">02</option>
									<option value="03">03</option>
									<option value="04">04</option>
									<option value="05">05</option>
									<option value="06">06</option>
									<option value="07">07</option>
									<option value="08">08</option>
									<option value="09">09</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
									<option value="13">13</option>
									<option value="14">14</option>
									<option value="15">15</option>
									<option value="16">16</option>
									<option value="17">17</option>
									<option value="18">18</option>
									<option value="19">19</option>
									<option value="20">20</option>
									<option value="21">21</option>
									<option value="22">22</option>
									<option value="23">23</option>
								</select> 시&nbsp;&nbsp;
                                <select name="startMinute" id="startMinute">
									<option value="01">00</option>
									<option value="11">10</option>
									<option value="21">20</option>
									<option value="31">30</option>
									<option value="41">40</option>
									<option value="51">50</option>
								</select> 분 &nbsp;~&nbsp;
                                <input type="text" id="endDate" placeholder="" />
                                <select name="endHour" id="endHour">
									<option value="00">00</option>
									<option value="01">01</option>
									<option value="02">02</option>
									<option value="03">03</option>
									<option value="04">04</option>
									<option value="05">05</option>
									<option value="06">06</option>
									<option value="07">07</option>
									<option value="08">08</option>
									<option value="09">09</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
									<option value="13">13</option>
									<option value="14">14</option>
									<option value="15">15</option>
									<option value="16">16</option>
									<option value="17">17</option>
									<option value="18">18</option>
									<option value="19">19</option>
									<option value="20">20</option>
									<option value="21">21</option>
									<option value="22">22</option>
									<option value="23">23</option>
								</select> 시&nbsp;&nbsp;
                                <select name="endMinute" id="endMinute">
									<option value="01">00</option>
									<option value="11">10</option>
									<option value="21">20</option>
									<option value="31">30</option>
									<option value="41">40</option>
									<option value="51">50</option>
								</select> 분
                                <button type="button" class="btn btn03 md" id="availableVehiclesBtn">검색</button>
                            </div>

                            <table class="table list mgt20">
                                <caption>배차 가능 차량 목록</caption>
                                <thead>
                                    <tr>
                                        <th scope="col">차량선택</th>
                                        <th scope="col">차량정보</th>
                                        <th scope="col">차량소속</th>
                                        <th scope="col">차량관리자</th>
                                        <th scope="col">차량위치</th>
                                    </tr>
                                </thead>
                                <tbody id="vehicleList">
                                	<!-- <tr>
                                		<td><input type="radio"></td>
                                		<td>그랜저/14조4938</td>
                                		<td>영업팀</br>인사지원팀</td>
                                		<td>인사지원팀</br>박노은/부장</td>
                                		<td>차량위치</td>
                                	</tr> -->
                                  	<tr>
                                        <td colspan="5">검색결과가 없습니다.</td>
                                    </tr>
                                </tbody>
                            </table>

                            <!-- 하단 버튼 -->
                            <div class="btn-bottom">
                                <button type="button" class="btn btn02" id="vehicleAllocateCancelBtn">취소</button>
                                <button type="button" class="btn btn03" id="vehicleAllocateRegBtn">배차신청완료</button>
                            </div>
                            <!--/ 하단 버튼 -->
                        </div>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->