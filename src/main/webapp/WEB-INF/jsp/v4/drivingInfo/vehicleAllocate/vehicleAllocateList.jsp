<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	 $("#start-date").datepicker({
         dateformat: 'yy-mm-dd'
     });

     
     $(document).on('click','.page-tab li',function(){
    	 var tab = $(this).data('tab');
    	 
    	 $(this).parent().children().removeClass('on');
    	 $(this).addClass('on');
    	 
    	 if(tab == "tab1"){
		 	$('#tab1').show();
		 	$('#tab2').hide();
    	 }else if(tab == "tab2"){
    		$('#tab1').hide();
 		 	$('#tab2').show(); 
    	 }
    	 
     });
     
     $('#vehicleAllocateRegBtn').on('click',function(){
    	 $V4.move("/drivingInfo/vehicleAllocate/reg");
     });
     
     $("#vehicle-schedule-dialog").dialog({
         autoOpen: false,
         show: {
             duration: 500
         },
         width: '960',
         modal: true
     });
     
     //팝업닫기
     $('#popClose').on('click',function(){
    	 $("#vehicle-schedule-dialog").dialog("close");	 
     });
     
     $(document).on('click','.allocateDetail',function(){
    	 
    	var allocateKey = $(this).data('allocatekey');
    	 
   		$V4.http_post("/api/1/allocate",{"searchAllocateKey":allocateKey,"allocateStatus":"allStep"},{
		requestMethod:"GET"
		,header:{key:"${_KEY}"}
		,success : function(data){
			//1개만 넘어 온다.
			console.log(data);
			
			var obj = data.result[0];
			
			var name = getProperty(obj,"account.name");
			var corpPosition = getProperty(obj,"account.corpPosition");
			
			$('#userInfo').val(corpPosition+" "+name);
			
			//사용목적
			var title = getProperty(obj,"title");
			//용도 1. 업무용, 2.출퇴근용, 3.비업무용
			var purpose = getProperty(obj,"purpose");
			//동승자
			var passenger = getProperty(obj,"passenger");
			//목적지
			var destination = getProperty(obj,"destination");
			//활용계획
			var contents = getProperty(obj,"contents");
			
			
			$('#title').val(title);
			
			if(purpose == "1"){
				$('#purpose').val("업무용");
			}else if(purpose == "2"){
				$('#purpose').val("출퇴근용");
			}else if(purpose == "3"){
				$('#purpose').val("비업무용");
			}else{
				//
			}
			
			$('#passenger').val(passenger);
			$('#destination').val(destination);
			$('#contents').val(contents);
			
			var startDate = convertDateUint(new Date(obj.startDate),_unitDate,_timezoneOffset,1)
			var endDate = convertDateUint(new Date(obj.endDate),_unitDate,_timezoneOffset,1)
			
			$('#allocateStartDate').text(startDate);
			$('#allocateEndDate').text(endDate);

			//상신자
			var regInfo = "";
			if(obj.regAccount){
					if(obj.regAccount.group){
						regInfo += (obj.regAccount.group.parentGroupNm)?(convertNullString(obj.regAccount.group.parentGroupNm)):"";
						regInfo += (obj.regAccount.group.groupNm)?("/"+convertNullString(obj.regAccount.group.groupNm)):"";
					}
					regInfo += '/';
					regInfo += convertNullString(obj.regAccount.corpPosition);
					regInfo += ' ';
					regInfo += convertNullString(obj.regAccount.name);
			}
			//상신일시			
			var regDate = convertDateUint(new Date(obj.regDate),_unitDate,_timezoneOffset,1)
			$('#regInfo').text(regInfo);
			$('#regDate').text(regDate);
			
			var step1Info = "";
			var step1Date = "";
			if(obj.approval){
				if(obj.approval.step1){
					if(obj.approval.step1.group){
						step1Info += (obj.approval.step1.group.parentGroupNm)?(convertNullString(obj.approval.step1.group.parentGroupNm)):"";
						step1Info += (obj.approval.step1.group.groupNm)?("/"+convertNullString(obj.approval.step1.group.groupNm)):"";
					}
					step1Info += '/';
					step1Info += convertNullString(obj.approval.step1.corpPosition);
					step1Info += ' ';
					step1Info += convertNullString(obj.approval.step1.name);
				}
				if(obj.approval.step1Date){
					step1Date = convertDateUint(new Date(obj.approval.step1Date),_unitDate,_timezoneOffset,1)
				}
				
			}
			
			$('#step1Info').text(step1Info);
			$('#step1Date').text(step1Date);
			
			var step2Info = "";
			var step2Date = "";
			if(obj.approval){
				if(obj.approval.step2){
					if(obj.approval.step2.group){
						step2Info += (obj.approval.step2.group.parentGroupNm)?(convertNullString(obj.approval.step2.group.parentGroupNm)):"";
						step2Info += (obj.approval.step2.group.groupNm)?("/"+convertNullString(obj.approval.step2.group.groupNm)):"";
					}
					step2Info += '/';
					step2Info += convertNullString(obj.approval.step2.corpPosition);
					step2Info += ' ';
					step2Info += convertNullString(obj.approval.step2.name);
				}
				if(obj.approval.step2Date){
					step2Date = convertDateUint(new Date(obj.approval.step2Date),_unitDate,_timezoneOffset,1)
				}
				
			}
			
			$('#step2Info').text(step2Info);
			$('#step2Date').text(step2Date);
			
			
			//팝업하단 차량정보 넣어 주어야함.
			var plateNum = obj.vehicle.plateNum;
			var modelMaster = obj.vehicle.vehicleModel.modelMaster;
			var groupsName = "";
			if(obj.groups){
				for(i in obj.groups){
					if(i!=0) strHtml += "</br>";
					groupsName += (obj.groups[i].parentGroupNm)?(convertNullString(obj.groups[i].parentGroupNm)):"";
					groupsName += (obj.groups[i].groupNm)?("/"+convertNullString(obj.groups[i].groupNm)):"";
				}
			}
			
			var managerName = obj.vehicle.corp.manager.vehicleManager.name;
			var managerPosition = obj.vehicle.corp.manager.vehicleManager.corpPosition;
			var managerGroupNm = "";
			if(obj.vehicle.corp.manager.vehicleManager.group){
				var managerGroupNm = obj.vehicle.corp.manager.vehicleManager.group.groupNm
			}
			
			$('#plateNum').text(modelMaster+" "+plateNum);
			$('#vehicleGroups').text(groupsName);
			$('#vehicleManager').html(managerGroupNm+"</br>"+managerName+"/"+managerPosition);
			
			var vehicleKey = getProperty(obj,'vehicle.vehicleKey');
			
			$('.vehicleLocationOpen').data('vehiclekey',vehicleKey);
			
			$("#vehicle-schedule-dialog").dialog("open");	
			
		}
	});
    	 
   		$(document).on('click','.vehicleLocationOpen',function(){
   			
   			var vehicleKey = $(this).data('vehiclekey'); 
   			
   			/* http://localhost:8080/map/openMap?searchType=vehicle&vehicleKeys=108d1259-bcc0-11e8-8564-42010a8c0091 */
   			
   			//sample
   			//$('#mapFrame').attr('src','/map/openMap?searchType=vehicle&vehicleKeys=108d1259-bcc0-11e8-8564-42010a8c0091');
   			$('#mapFrame').attr('src','/map/openMap?searchType=vehicle&vehicleKeys='+vehicleKey);
   			
   			$("#vehicleLocationDialog").dialog("open");
   		});

   		$("#vehicleLocationDialog").dialog({
   		    autoOpen: false,
   		    show: {
   		        duration: 500
   		    },
   		    width: '960',
   		    modal: true
   		});	 
    	 
     });
 
     var thead = [
                  {'<select id="allocateState" name="" class="arrow"><option value="">배차상태</option><option value="waiting">배차대기</option><option value="processing">배차중</option><option value="end">배차종료</option></select>':'15%'},
				  {'차량정보' : '20%'},                  
				  {'사용자' : '15%'},
                  {'사용(예정)일시/업무명' : '40%'},
                  {'상세':'10%'}
                  ];
     
     var allocateListSetting = {
    		thead : thead
    		,bodyHtml : (function(data){
    			var strHtml = "";
    			for(i in data.result){
    				var obj = data.result[i];
    				console.log(obj);
    				strHtml += '<tr>';
    				strHtml += '    <td>';
    				
    				var nowTime = new Date().getTime();
    				
    				//현재 시각 < 시작시간               배차대기
    				//시작시간 < 현재시각 < 종료시각     배차중
    				//현재시각 > 종료시각                배차종료
    				
    				if(nowTime < obj.startDate){
    					strHtml += '        <div class="states-value state02">배차대기</div>'; // 2 4 7
    				}else if(obj.startDate < nowTime && nowTime < obj.endDate){
    					strHtml += '        <div class="states-value state04">배차중</div>'; // 2 4 7
    				}else if(nowTime > obj.endDate){
    					strHtml += '        <div class="states-value state07">배차종료</div>'; // 2 4 7
    				}else{
    					strHtml += '        <div class="states-value"></div>'; // 2 4 7
    				}
    				
    				strHtml += '    </td>';
    				strHtml += '    <td>';
    				strHtml += '        <div class="car-img">';
    				strHtml += '            <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" alt="" /></span>';
    				strHtml += '            <span class="num">'+getProperty(obj,'vehicle.vehicleModel.modelMaster')+' '+getProperty(obj,'vehicle.plateNum')+'</span>';
    				strHtml += '        </div>';
    				strHtml += '    </td>';
					    			
    				strHtml += '<td>';  
   					if(obj.account){
   						if(obj.account.group){
   							strHtml += (obj.account.group.parentGroupNm)?(convertNullString(obj.account.group.parentGroupNm)):"";
   							strHtml += (obj.account.group.groupNm)?("/"+convertNullString(obj.account.group.groupNm)):"";
   						}
   						strHtml += '/';
   						strHtml += convertNullString(obj.account.corpPosition);
   						strHtml +='<br />';
   						strHtml += convertNullString(obj.account.name);
   					}
   					strHtml += '</td>';
    				strHtml += '    <td class="left">';
    				strHtml += '        <div class="driving-info">';
    				strHtml += '            <ul>';
    				strHtml += '                <li>';
    				strHtml += '                    <strong class="title">시작/종료</strong>';
    				strHtml += '                    <span>'+convertDateUint(new Date(obj.startDate),_unitDate,_timezoneOffset,1)+' ~ '+convertDateUint(new Date(obj.endDate),_unitDate,_timezoneOffset,1)+'</span>';
    				strHtml += '                </li>';
    				strHtml += '                <li>';
    				strHtml += '                    <strong class="title">업무명</strong>';
    				
    				strHtml += '                    <span>	'+getProperty(obj,'title')+'</span>';
    				strHtml += '                </li>';
    				strHtml += '            </ul>';
    				strHtml += '        </div>';
    				strHtml += '    </td>';
    				strHtml += '    <td><button type="button" class="btn-img change allocateDetail" data-allocatekey='+obj.allocateKey+'>상세보기</button></td>';
    				strHtml += '</tr>';
    			}
    			return strHtml;
    		})
    		,limit : parseInt($(".list01 .func_limitChange").val())
    		,pagePerGroup : 10 //pagingGroup size
    		,url : "/api/{ver}/allocate"
    		,param : {'searchOrder' : $(".list01 .func_order").val()}
    		,http_post_option : {
    			requestMethod : "GET"
    			,header : {key : "${_KEY}"}
    		}
    		,initSearch : true //생성과 동시에 search		
    		,loadingBodyViewer : true //로딩중!! 표시됨
    		,debugMode : true
    	}
     var thead2 = [
                   {'<select id="approvalState" name="" class="arrow"><option value="allStep">상태</option><option value="step0">상신</option><option value="step1">1차합의</option><option value="complate">승인</option><option value="reserve">반려</option></select>':'15%'},
                   {"신청자":"12%"},
                   {"사용자":"12%"},
                   {"차량정보":"15%"},
                   {"사용(예정)일시/업무명":"31%"},
                   {"상세":"15%"}
                   ];
     
     var approvalListSetting = {
     		thead : thead2
     		,bodyHtml : (function(data){
     			var strHtml = "";
     			for(i in data.result){
     				var obj = data.result[i];
     				console.log(obj);
     				strHtml += '<tr>';
     				strHtml += '    <td>';
     				
     				//상신 state != maxStep && state == 0 
     				
     				//1차합의 state != maxStep && state == 1
     				
     				//승인 state == maxStep
     				
     				//반려 state == -1
     				if(obj.approval){
     					//상신
     					if(obj.approval.state != obj.approval.maxStep && obj.approval.state == "0"){
     						strHtml += '        <div class="states-value state02">상신</div>';
     						//상신인 경우만 취소 버튼이 같이 있는다
     						strHtml += '        </br>';
     						strHtml += '        <div style="cursor: pointer;" class="states-value state03 allocateCancel" data-allocatekey='+obj.allocateKey+'>취소</div>';
     					
     						var regDate = convertDateUint(new Date(obj.regDate),_unitDate,_timezoneOffset,1); //상신시간
     						var name = obj.account.name; //상신자명
     						var groupNm = convertNullString(obj.account.group.groupNm);
     						strHtml += '        <div>'+regDate+'<br /> '+groupNm+'/'+name+'</div>';
     					//1차합의
     					}else if(obj.approval.state != obj.approval.maxStep && obj.approval.state == "1"){
     						strHtml += '        <div class="states-value state04">1차합의</div>';
     						var step1Date = convertDateUint(new Date(obj.approval.step1Date),_unitDate,_timezoneOffset,1); //1차 합의 시간
     						var step1Name = obj.approval.step1.name;
     						var step1GroupNm = "";
     						if(obj.approval.step1.group){
     							step1GroupNm = convertNullString(obj.approval.step1.group.groupNm);
     						}
     						strHtml += '        <div>'+step1Date+'<br /> '+step1GroupNm+'/'+step1Name+'</div>';
     					//승인
     					}else if(obj.approval.state == obj.approval.maxStep){
     						strHtml += '        <div class="states-value state07">승인</div>';
     						var step2Date = convertDateUint(new Date(obj.approval.step2Date),_unitDate,_timezoneOffset,1); //1차 합의 시간
     						var step2Name = obj.approval.step2.name;
     						var step2GroupNm = "";
     						if(obj.approval.step2.group){
     							step2GroupNm = convertNullString(obj.approval.step2.group.groupNm);
     						}
     						strHtml += '        <div>'+step2Date+'<br /> '+step2GroupNm+'/'+step2Name+'</div>';
     					//반려
     					}else if(obj.approval.state == "-1"){
     						strHtml += '        <div class="states-value state08">반려</div>';
     						
     						
     						
     						var reserveDate = convertDateUint(new Date(obj.approval.reserveDate),_unitDate,_timezoneOffset,1); //1차 합의 시간
     						var reserveName = obj.approval.reserve.name;
     						var reserveGroupNm = "";
     						if(obj.approval.reserve.group){
     							reserveGroupNm = convertNullString(obj.approval.reserve.group.groupNm);
     						}
     						strHtml += '        <div>'+reserveDate+'<br /> '+reserveName+'/'+reserveGroupNm+'</div>';
     					}
     					
     				}
     				/* strHtml += '        <div class="states-value state05">후결상신</div>';
     				 */
     				strHtml += '    </td>';
     				
     				if(obj.regAccount){
     					if(obj.regAccount.group){
     						var regAccountName = obj.regAccount.name;
     						var regAccountPosition = obj.regAccount.corpPosition;
     						var regAccountGroupNm = convertNullString(obj.regAccount.group.groupNm);
     						strHtml += '    <td>'+regAccountGroupNm+'<br>'+regAccountName+'/'+regAccountPosition+'</td>';
     					}
     				}else{
     					strHtml += '    <td></td>';	
     				}
     				
     				
     				if(obj.account){
     					if(obj.account.group){
     						var accountName = obj.account.name;
     						var accountPosition = obj.account.corpPosition;
     						var accountGroupNm = convertNullString(obj.account.group.groupNm);
     						strHtml += '    <td>'+accountGroupNm+'<br>'+accountName+'/'+accountPosition+'</td>';
     					}
     				}else{
     					strHtml += '    <td></td>';	
     				}
     				
     				strHtml += '    <td>';
     				strHtml += '        <div class="car-img">';
     				strHtml += '            <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" alt="" /></span><br />';
     				
     				strHtml += '            <span class="num">'+getProperty(obj,'vehicle.vehicleModel.modelMaster')+' '+getProperty(obj,'vehicle.plateNum')+'</span>';
     				strHtml += '        </div>';
     				strHtml += '    </td>';
     				strHtml += '    <td class="left">';
     				strHtml += '        <div class="driving-info">';
     				strHtml += '            <ul>';
     				strHtml += '                <li>';
     				strHtml += '                    <strong class="title">시작/종료</strong>';
     				strHtml += '                    <span>'+convertDateUint(new Date(obj.startDate),_unitDate,_timezoneOffset,1)+' ~ '+convertDateUint(new Date(obj.endDate),_unitDate,_timezoneOffset,1)+'</span>';
     				strHtml += '                </li>';
     				strHtml += '                <li>';
     				strHtml += '                    <strong class="title">업무명</strong>';
     				strHtml += '                    <span>'+getProperty(obj,'title')+'</span>';
     				strHtml += '                </li>';
     				strHtml += '            </ul>';
     				strHtml += '        </div>';
     				strHtml += '    </td>';
     				strHtml += '    <td><button type="button" class="btn-img change allocateDetail" data-allocatekey='+obj.allocateKey+'>상세보기</button></td>';
     				strHtml += '</tr>';
     				
     			}
     			return strHtml;
     		})
     		,limit : parseInt($(".list02 .func_limitChange").val())
     		,pagePerGroup : 10 //pagingGroup size
     		,url : "/api/{ver}/allocate"
     		,param : {
     					'searchOrder' : $(".list02 .func_order").val(),
						'allocateStatus' : 'allStep'
					 }
     		,http_post_option : {
     			requestMethod : "GET"
     			,header : {key : "${_KEY}"}
     		}
     		,initSearch : true //생성과 동시에 search		
     		,loadingBodyViewer : true //로딩중!! 표시됨
     		,debugMode : true
     	}
     
     $('#allocateList').commonList(allocateListSetting);
     $('#approvalList').commonList2(approvalListSetting);
     
     $("#top_btn_search").on("click",$.debounce(200,1,function(){
 		
 		var param = {searchType:$("#top_searchType").val() , searchText : $("#top_searchText").val()}
 		
 		var list = $(".page-tab li.on").hasClass("list01")?"list01":$(".page-tab li.on").hasClass("list02")?"list02":"";
 		
 		
 		if(list){
 			var limit = parseInt($("."+list+" .func_limitChange").val());	 
 			param.searchOrder = $("."+list+" .func_order").val();
 			
 			var startDateTime = new Date($('#start-date').val()).getTime();
 			$('#start-date').val()&&(param.searchFixedStartDate = startDateTime)
 			$("."+list+" .searchStart").val()&&(param.searchStartDate = $("."+list+" .searchStart").val());
 			$("."+list+" .searchEnd").val()&&(param.searchEndDate = $("."+list+" .searchEnd").val());
 			
 			if(list == "list01"){
 				param.allocateStatus = $('#allocateList .arrow').val();
 				$('#allocateList').commonList("setParam",param);
 				$('#allocateList').commonList("setLimit",limit).search();
 			}else if(list =="list02"){
 				param.allocateStatus =  $('#approvalList .arrow').val();
 				$('#approvalList').commonList2("setParam",param);
 				$('#approvalList').commonList2("setLimit",limit).search();
 			}
 		}
 		
 	}));
     
     $(".func_order").on("change",function(){
 		$("#top_btn_search").trigger("click");
 	});
     
     $(".func_limitChange").on("change",function(){
 		$("#top_btn_search").trigger("click");
 	});
 	
 	$(".searchStart,.searchEnd").on("change",function(){
 		$("#top_btn_search").trigger("click");
 	});
 	$("div.date button").on("click",function(){
		var list = $(".page-tab li.on").hasClass("list01")?"list01":$(".page-tab li.on").hasClass("list02")?"list02":"";
		$(this).closest("div").find("button").removeClass("active");
		$(this).addClass("active");
		var d = $(this).data("day");
		
		if(d == ""){
			$("."+list+" .datePicker").val("");
			$("."+list+" .searchStart,."+list+" .searchEnd").val("");
			
			$("#top_btn_search").trigger("click");
		}else{
			
			var rtvStartDate = calcDate(new Date() , 'd' , 0 , new Date());
			var rtvEndDate = calcDate(new Date() , 'd' , 0 , new Date());
			
			if(d == "-0D"){
			}else if(d == "-7D") rtvStartDate = calcDate(new Date() , 'd' , -7 , new Date());
			else if(d == "-1M") rtvStartDate = calcDate(new Date() , 'm' , -1 , new Date());
			else if(d == "-3M") rtvStartDate = calcDate(new Date() , 'm' , -3 , new Date());
			else if(d == "-6M") rtvStartDate = calcDate(new Date() , 'm' , -6 , new Date());
			
			$("."+list+" .datePicker").eq(0).datepicker('setDate', rtvStartDate);
			$("."+list+" .datePicker").eq(1).datepicker('setDate', rtvEndDate);
			
			var v = $("."+list+" .datePicker").eq(0).datepicker( "getDate" ).getTime();
    		var t = $("."+list+" .datePicker").eq(0).data("target");
    		$(t).val(v);
    		
    		var v = $("."+list+" .datePicker").eq(1).datepicker( "getDate" ).getTime();
    		var t = $("."+list+" .datePicker").eq(1).data("target");
    		$(t).val(v);
    		
    		$(t).trigger("change");
		}
	});
 	
 	$("#top_searchText").on("keyup",function(e){
		if(e.keyCode == 13) {
			$("#top_btn_search").trigger("click");
		}
	});
    var cntObj ={
   		"totalCnt" : 0, //총 배차 수
   		"waitingCnt" : 0, // 배차 대기
   		"processingCnt" : 0, // 배차 중
   		"endCnt" : 0, // 배차 종료
   		"step0Cnt" : 0, //상신중
   		"step1Cnt" : 0, //1차합의 
   		"step2Cnt" : 0, //승인건
   		"reserveCnt" : 0 // 반려건
    }
 	
 	var countingParam = {offset:0,limit:1,counting:true};
	$V4.http_post("/api/1/allocate",countingParam,{
		requestMethod:"GET"
		,header:{key:"${_KEY}"}
		,success : function(data){
			$.extend(cntObj,data);
			
			$('#totalCnt').text(cntObj.totalCnt);
			$('#waitingCnt').text(cntObj.waitingCnt);
			$('#processingCnt').text(cntObj.processingCnt);
			$('#endCnt').text(cntObj.endCnt);
			
			$('#step0Cnt').text(cntObj.step0Cnt);
			$('#step1Cnt').text(cntObj.step1Cnt);
			$('#step2Cnt').text(cntObj.step2Cnt);
			$('#reserveCnt').text(cntObj.reserveCnt);
		}
	});
	
	
	$('#allocateState').on('change',function(){
		$("#top_btn_search").trigger("click");
	});
	$('#approvalState').on('change',function(){
		$("#top_btn_search").trigger("click");
	});
	
	$(document).on('click','.allocateCancel',function(){
		
		var allocateKey = $(this).data('allocatekey');
		
		$V4.http_post("/api/{ver}/allocate/"+allocateKey,{},{
			requestMethod:"DELETE"
			,header:{key:"${_KEY}"}
			,success : function(data){
				alert('취소 되었습니다.');
				$("#top_btn_search").trigger("click");
			}
		});
	});
	
});

var setAddr = function(addr,addrDetail){
	$('#address').val(convertNullString(addr)+" "+convertNullString(addrDetail));
}

</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>차량 배차</h2>
                    <span>배차 신청 내역을 조회하거나, 실시간 배차 현황을 확인할 수 있습니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="driving-page">
                    <div class="vehicle-schedule">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                        	<div class="item">
                                <span>
									총 배차 수 <strong id="totalCnt">0</strong>건
								</span>
                                <span>
									배차대기 <strong id="waitingCnt">0</strong>건
								</span>
                                <span>
									배차중 <strong id="processingCnt">0</strong>건
								</span>
                                <span>
									배차종료 <strong id="endCnt">0</strong>건
								</span>
                                <span class="division">
									상신중 <strong id="step0Cnt">0</strong>건
								</span>
                                <span>
									1차합의 <strong id="step1Cnt">0</strong>건
								</span>
                                <span>
									승인 <strong id="step2Cnt">0</strong>건
								</span>
                                <span>
									반려 <strong id="reserveCnt">0</strong>건
								</span>
                            </div>
                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <!-- 거래처등록 -->
                        <h3 class="tit2 mgt30">배차 신청하기</h3>
                        <div class="box-register">
                            배차 신청 후 차량 사용이 가능합니다. 배차 신청을 꼭 하신 후 운행해주세요.<br /> 고정배차인 임직원은 운행일지를 이용해주세요.
                            <ul class="type-01">
                                <li>1차합의: 차량 관리자 (ex.인사부서 또는 총무부서 소속 관리자) </li>
                                <li>2차 승인 : 차량 사용자(상신자) 소속부서장</li>
                                <li>후결상신: 미배차운행의 후결재가 차량담당자에게 통보되었습니다. (담당부서장 승인 결재 필요)</li>
                                <li>후결승인: 차량 담당자(차량소속 부서장)</li>
                            </ul>
                            <br /> ※ 배차 신청 없이 운행하면, 운전자 정보가 없으므로 차량 담당자(차량소속 부서장)에게 통보가 되며, <br /> 차량 담당자(차량소속 부서장)가 운행한 것으로 표기됩니다. <br /> 이럴 경우, ‘운행관리 -> 상세 운행 검색 -> 미배차’ 메뉴에서 자신의 운행을 확인 후, 후결을 진행해주세요.
                            <div class="right">
                                <button class="btn btn01" id="vehicleAllocateRegBtn">신규 등록</button>
                            </div>
                        </div>
                        <!--/ 거래처등록 -->

                        <!-- 상단 검색 -->
                        <h3 class="tit2 mgt30">신청 목록</h3>
                        <div class="top-search mgt20">
                            <input type="text" name="" id="start-date" placeholder="배차 시작일" />
                            <select id="top_searchType">
								<option value="">선택</option>
								<option value="plateNum">차량번호</option>
								<option value="name">사용자명</option>
							</select>
                            <input type="text" id="top_searchText"/>
                            <button type="button" class="btn btn03" id="top_btn_search">조회</button>
                        </div>
                        <!--/ 상단 검색 -->

                        <!-- 메뉴 -->
                        <ul class="page-tab col-4">
                            <li class="list01 on" data-tab="tab1">
                                <a href="javascript:void(0);">배차완료</a>
                            </li>
                            <li class="list02" data-tab="tab2">
                                <a href="javascript:void(0);">결재이력</a>
                            </li>
                        </ul>
                        <!--/ 메뉴 -->

						<div id="tab1">
							 <!-- table 버튼 -->
	                        <div class="list01 btn-function row-2">
	                            <div class="left">
	                                <select class="func_order">
                                        <option value="lastestLocationTime">최근 운행순</option>
										<option value="regDate">최근 등록순</option>
									</select>
	                                <div class="date">
                                        <button type="button" class="active" data-day="">전체</button>
                                        <button type="button" data-day="-0D">오늘</button>
                                        <button type="button" data-day="-7D">1주일</button>
                                        <button type="button" data-day="-1M">1개월</button>
                                        <button type="button" data-day="-3M">3개월</button>
                                        <button type="button" data-day="-6M">6개월</button>
                                        <div class="direct">
                                            <input type="text" class="datePicker" data-target="#list01_searchStart"/> &nbsp;~&nbsp;
                                            <input type="hidden" id="list01_searchStart" class="searchStart"/>
                                            <input type="text" class="datePicker" data-target="#list01_searchEnd"/>
                                            <input type="hidden" id="list01_searchEnd" class="searchEnd"/>
                                        </div>
                                    </div>
	                            </div>
	                            <div class="right">
	                                <button class="excel">엑셀 다운로드</button>
	                                <select class="func_limitChange">
										<option value="5" selected>5건씩 보기</option>
										<option value="10">10건씩 보기</option>
										<option value="20">20건씩 보기</option>
										<option value="50">20건씩 보기</option>
									</select>
	                            </div>
	                        </div>
	                        <!--/ table 버튼 -->
	                        
	                        <table class="table list mgt20" id="allocateList">
	                            <%-- <caption>등록된 서비스 리스트</caption>
	                            <colgroup>
	                                <col style="width:15%" />
	                                <col style="width:20%" />
	                                <col style="width:15%" />
	                                <col />
	                                <col style="width:10%" />
	                            </colgroup>
	                            <thead>
	                                <tr>
	                                    <th scope="col">
	                                        <select name="" class="arrow">
	                                        	<option value="">배차상태</option>
												<option value="waiting">배차대기</option>
												<option value="processing">배차중</option>
												<option value="end">배차종료</option>
											</select>
	                                    </th>
	                                    <th scope="col">차량정보</th>
	                                    <th scope="col">사용자</th>
	                                    <th scope="col">사용(예정)일시/업무명</th>
	                                    <th scope="col">상세</th>
	                                </tr>
	                            </thead>
	                            <tbody>
	                                <tr>
	                                    <td>
	                                        <div class="states-value state07">배차종료</div>
	                                    </td>
	                                    <td>
	                                        <div class="car-img">
	                                            <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" alt="" /></span>
	                                            <span class="num">쏘나타 63호 5703</span>
	                                        </div>
	                                    </td>
	                                    <td>총무팀 / 김계복</td>
	                                    <td class="left">
	                                        <div class="driving-info">
	                                            <ul>
	                                                <li>
	                                                    <strong class="title">시작/종료</strong>
	                                                    <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
	                                                </li>
	                                                <li>
	                                                    <strong class="title">업무명</strong>
	                                                    <span>	대산공장 촬영출방</span>
	                                                </li>
	                                            </ul>
	                                        </div>
	                                    </td>
	                                    <td><button type="button" class="btn-img change allocateDetail">상세보기</button></td>
	                                </tr>
	                            </tbody> --%>
	                        </table>
                        </div>
                        <div id="tab2" style="display:none;">
                        
                        	 <!-- table 버튼 -->
	                        <div class="list02 btn-function row-2">
	                            <div class="left">
	                                <select class="func_order">
                                        <option value="lastestLocationTime">최근 운행순</option>
										<option value="regDate">최근 등록순</option>
									</select>
	                                <div class="date">
                                        <button type="button" class="active" data-day="">전체</button>
                                        <button type="button" data-day="-0D">오늘</button>
                                        <button type="button" data-day="-7D">1주일</button>
                                        <button type="button" data-day="-1M">1개월</button>
                                        <button type="button" data-day="-3M">3개월</button>
                                        <button type="button" data-day="-6M">6개월</button>
                                        <div class="direct">
                                            <input type="text" class="datePicker" data-target="#list02_searchStart"/> &nbsp;~&nbsp;
                                            <input type="hidden" id="list02_searchStart" class="searchStart"/>
                                            <input type="text" class="datePicker" data-target="#list02_searchEnd"/>
                                            <input type="hidden" id="list02_searchEnd" class="searchEnd"/>
                                        </div>
                                    </div>
	                            </div>
	                            <div class="right">
	                                <button class="excel">엑셀 다운로드</button>
	                                <select class="func_limitChange">
										<option value="5" selected>5건씩 보기</option>
										<option value="10">10건씩 보기</option>
										<option value="20">20건씩 보기</option>
										<option value="50">20건씩 보기</option>
									</select>
	                            </div>
	                        </div>
	                        <!--/ table 버튼 -->
                        
	                        <table class="table list mgt20" id="approvalList">
	                            <%-- <caption>등록된 서비스 리스트</caption>
	                            <colgroup>
	                                <col style="width:15%" />
	                                <col style="width:12%" />
	                                <col style="width:12%" />
	                                <col style="width:15%" />
	                                <col />
	                                <col style="width:10%" />
	                            </colgroup>
	                            <thead>
	                                <tr>
	                                    <th scope="col">
	                                        <select name="" class="arrow">
												<option value="allStep">상태</option>
												<option value="step0">상신</option>
												<option value="step1">1차합의</option>
												<option value="complate">승인</option>
												<option value="reserve">반려</option>
											</select>
	                                    </th>
	                                    <th scope="col">신청자</th>
	                                    <th scope="col">사용자</th>
	                                    <th scope="col">차량정보</th>
	                                    <th scope="col">사용(예정)일시/업무명</th>
	                                    <th scope="col">상세</th>
	                                </tr>
	                            </thead>
	                            <tbody>
	                                <tr>
	                                    <td>
	                                        <div class="states-value state02">상신</div>
	                                        <div>2018/05/18 16:50<br /> 커뮤니키이션팀/변재한</div>
	                                    </td>
	                                    <td>커뮤니케이션팀<br>변재한/대리</td>
	                                    <td>커뮤니케이션팀<br>변재한/대리</td>
	                                    <td>
	                                        <div class="car-img">
	                                            <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" alt="" /></span><br />
	                                            <span class="num">쏘나타 63호 5703</span>
	                                        </div>
	                                    </td>
	                                    <td class="left">
	                                        <div class="driving-info">
	                                            <ul>
	                                                <li>
	                                                    <strong class="title">시작/종료</strong>
	                                                    <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
	                                                </li>
	                                                <li>
	                                                    <strong class="title">업무명</strong>
	                                                    <span>	대산공장 촬영출방</span>
	                                                </li>
	                                            </ul>
	                                        </div>
	                                    </td>
	                                    <td><button type="button" class="btn-img change allocateDetail">상세보기</button></td>
	                                </tr>
	                                <tr>
	                                    <td>
	                                        <div class="states-value state02">상신</div>
	                                        <div class="states-value state05">후결상신</div>
	                                        <div class="states-value state04">1차합의</div>
	                                        <div class="states-value state07">후결승인</div>
	                                        <div class="states-value state08">반려</div>
	                                        <div class="states-value state03">취소</div>
	                                        <div>2018/05/18 16:50<br /> 커뮤니키이션팀/변재한</div>
	                                    </td>
	                                    <td>커뮤니케이션팀<br>변재한/대리</td>
	                                    <td>커뮤니케이션팀<br>변재한/대리</td>
	                                    <td>
	                                        <div class="car-img">
	                                            <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" alt="" /></span><br />
	                                            <span class="num">쏘나타 63호 5703</span>
	                                        </div>
	                                    </td>
	                                    <td class="left">
	                                        <div class="driving-info">
	                                            <ul>
	                                                <li>
	                                                    <strong class="title">시작/종료</strong>
	                                                    <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
	                                                </li>
	                                                <li>
	                                                    <strong class="title">업무명</strong>
	                                                    <span>	대산공장 촬영출방</span>
	                                                </li>
	                                            </ul>
	                                        </div>
	                                    </td>
	                                    <td><button type="button" class="btn-img change allocateDetail">상세보기</button></td>
	                                </tr>
	                            </tbody> --%>
	                        </table>
	                     </div>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->
        
        
<!-- 배차정보 팝업 -->
    <div id="vehicle-schedule-dialog" title="배차 정보">
        <table class="table">
            <caption>배차정보 리스트</caption>
            <tbody>
                <tr>
                    <th scope="row">사용자</th>
                    <td>
                        <div class="division">
                            <div class="col-3">
                                <input type="text" readonly="readonly" id="userInfo"/>
                            </div>
                            <!-- <div class="space">&nbsp;</div> -->
                            <!-- <div class="col-9">
                                <button class="btn btn04 md">검색</button>
                            </div> -->
                        </div>
                    </td>
                </tr>
            </tbody>

            <tbody>
                <tr>
                    <th scope="row">용도</th>
                    <td>
                    	<input type="text" readonly="readonly" id="purpose" />
                    </td>
                </tr>
                <tr>
                    <th scope="row">사용목적</th>
                    <td><input type="text" readonly="readonly" id="title" /></td>
                </tr>
                <tr>
                    <th scope="row">동승자</th>
                    <td><input type="text" readonly="readonly" id="passenger" /></td>
                </tr>
                <tr>
                    <th scope="row">목적지</th>
                    <td><input type="text" readonly="readonly" id="destination"/></td>
                </tr>
                <tr>
                    <th scope="row">활용계획</th>
                    <td><textarea rows="5" class="w100" readonly="readonly" id="contents"></textarea></td>
                </tr>
            </tbody>

            <tbody>
                <tr>
                    <th scope="row">사용일시</th>
                    <td class="left">
                        <div class="driving-info">
                            <ul>
                                <li>
                                    <strong class="title w50">시작</strong>
                                    <span id="allocateStartDate"></span>
                                </li>
                                <li>
                                    <strong class="title w50">종료</strong>
                                    <span id="allocateEndDate"></span>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">상신</th>
                    <td class="left">
                        <div class="driving-info">
                            <ul>
                                <li>
                                    <strong class="title w50">시작</strong>
                                    <span id="regInfo"></span>
                                </li>
                                <li>
                                    <strong class="title w50">일시</strong>
                                    <span id="regDate"></span>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">1차합의</th>
                    <td class="left">
                        <div class="driving-info">
                            <ul>
                                <li>
                                    <strong class="title w50">승인자</strong>
                                    <span id="step1Info"></span>
                                </li>
                                <li>
                                    <strong class="title w50">일시</strong>
                                    <span id="step1Date"></span>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">2차승인</th>
                    <td class="left">
                        <div class="driving-info">
                            <ul>
                                <li>
                                    <strong class="title w50">승인자</strong>
                                    <span id="step2Info"></span>
                                </li>
                                <li>
                                    <strong class="title w50">일시</strong>
                                    <span id="step2Date"></span>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>

        <table class="table list mgt20">
            <caption>배차정보</caption>
            <colgroup>
                <%-- <col style="width:8%" /> --%>
                <col style="width:20%" />
                <col />
                <col style="width:20%" />
                <col style="width:20%" />
            </colgroup>
            <thead>
                <tr>
                    <!-- <th scope="col">차량선택</th> -->
                    <th scope="col">차량정보</th>
                    <th scope="col">차량소속</th>
                    <th scope="col">차량관리자</th>
                    <th scope="col">차량위치</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <!-- <td><input type="radio" /></td> -->
                    <td>
                        <div class="car-img">
                            <span class="img"><img src="./img/vehicle/S103.png" alt="" /></span>
                            <span class="num" id="plateNum"></span>
                        </div>
                    </td>
                    <td id="vehicleGroups"></td>
                    <td id="vehicleManager"></td>
                    <td>
                        <button type="button" class="btn-img location vehicleLocationOpen">위치</button>
                    </td>
                </tr>
            </tbody>
        </table>

        <!-- 하단 버튼 -->
        <div class="btn-bottom">
            <button type="button" class="btn btn03" id="popClose">확인</button>
        </div>
        <!--/ 하단 버튼 -->
    </div>
    <!--/ 배차정보 팝업 -->
<!-- 차량 위치 팝업 -->
<div id="vehicleLocationDialog" title="위치 확인">
    <div class="map clr">
        <div class="left">
        <iframe id="mapFrame" src="about:blank" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>
    <div class="mgt20">
    	<span class="tit" style="color: #333; font-weight: 400; line-height: 40px;">주소</span>
	    <input type="text" id="address" value="" readonly="readonly" style="margin-left: 15px;width:95%;height:40px;background-color:#f8f8f8;padding-left: 10px; border: 1px solid #e3e3e3; line-height: 30px; border-radius: 0; -webkit-appearance: none; -webkit-appearance: none;">
    </div>
    <div class="mgt20">
    	※ 사용자의 GPS 활용 미동의, GPS 오동작, 지하주차장 등과 GPS 수신상태로 고르지 못할 경우 위치가 정확하지 않을 수 있습니다.
    	</br>
		위치가 정확하지 않은 경우 정확한 위치를 직접 지도에서 찾아 표시하거나 입력해주세요.
    </div>
</div>
<!-- 차량 위치 팝업 -->    