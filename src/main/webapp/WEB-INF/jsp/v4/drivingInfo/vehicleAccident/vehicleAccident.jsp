<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	$("#start-date").datepicker({
        dateformat: 'yy-mm-dd'
    });

    $("#end-date").datepicker({
        dateformat: 'yy-mm-dd'
    });
});
</script>
 <!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>사고차량 조회</h2>
                    <span>등록차량의 중대 교통사교를 조회ㆍ관리하고 사고시 긴급출동기관으로 신고합니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="driving-page">
                    <div class="">
                        <!-- 상단 상태값 -->
						<div class="item">
							<span>
								총건수
								<strong>10</strong>건
							</span>
						</div>
						
                        <div class="top-state clr">
                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <!-- 상단 검색 -->
                        <div class="top-search mgt20">
                            <select name="">
								<option value="">전체</option>
							</select>
                            <input type="text" name="" />
                            <button class="btn btn03">조회</button>
                        </div>
                        <!--/ 상단 검색 -->

						<!-- table 버튼 -->
                        <div class="btn-function">
                            <div class="left">
                                <div class="date">
                                    <button class="active">전체</button>
                                    <button>오늘</button>
                                    <button>1주일</button>
                                    <button>1개월</button>
                                    <button>3개월</button>
                                    <button>6개월</button>
                                    <div class="direct">
                                        <input type="text" id="start-date" /> &nbsp;~&nbsp;
                                        <input type="text" id="end-date" />
                                    </div>
                                    <!-- 기획서에는 직접입력이 셀렉트박스로 되어 있는데, 사용자가 어떻게 날짜를 입력 해야 하는지 모르겠어서 임의로 변경했습니다.-->
                                </div>
                            </div>
                            <div class="right">
                                <select name="">
									<option value="">5건씩 보기</option>
								</select>
                            </div>
                        </div>
                        <!--/ table 버튼 -->

						<table class="table list mgt20">
							<caption>사고차량 조회 리스트</caption>
							<colgroup>
								<col style="width:10%" />
								<col style="width:10%" />
								<col style="width:10%" />
								<col style="width:8%" />
								<col style="width:8%" />
								<col style="width:12%" />
								<col />
								<col style="width:10%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col">회사명</th>
									<th scope="col">차량정보</th>
									<th scope="col">사용자</th>
									<th scope="col">구분</th>
									<th scope="col">성별</th>
									<th scope="col">연락처</th>
									<th scope="col">사고정보</th>
									<th scope="col">위치보기</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>자스텍엠</td>
									<td>아반떼<br />55주4983</td>
									<td>개인<br />조경대</td>
									<td>긴급 버튼</td>
									<td>M/A</td>
									<td>010-9230-1845</td>
									<td class="left">
										<div class="driving-info">
											<ul>
												<li>
													<strong class="title">접수일시</strong>
													<span>2018/05/19 05:58</span>
												</li>
												<li>
													<strong class="title">사고장소</strong>
													<span>알수없는 장소</span>
												</li>
											</ul>
										</div>
									</td>
									<td><button class="btn-img location">위치</button></td>
								</tr>
							</tbody>
						</table>

						<!-- 페이징 -->
						<div id="paging">
							<a href="" class="first icon-spr">제일 처음으로</a>
							<a href="" class="prev icon-spr">이전으로</a>
							<span class="current">1</span>
							<a href="" class="num">2</a>
							<a href="" class="num">3</a>
							<a href="" class="num">4</a>
							<a href="" class="num">5</a>
							<a href="" class="next icon-spr">다음으로</a>
							<a href="" class="last icon-spr">제일 마지막으로</a>
						</div>
						<!--/ 페이징 -->
                    </div>

					<!-- 안내 -->
					<div class="box-info">
						<h3 class="hidden">안내</h3>
						<h4 class="mgt0">사고지역 위치보기 안내</h4>
						<ul>
							<li>
							위치보기 버튼을 클릭하면 사용자 휴대폰 App에 기록된 사고위치가 아래 지도에 표시됩니다.<br />
							사용자의 GPS 활용 미동의, GPS 오동작, 지하주차장 등과 GPS수신 상태로 고르지 못할 경우 위치가 정확하지 않을 수 있습니다.
							</li>
						</ul>
					</div>
					<!--/ 안내 -->

					<div class="mgt40">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d25357.461822122736!2d127.10473640699269!3d37.39733480314603!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x357ca7fe934881c7%3A0x30db2f32566ac8fb!2z6rK96riw64-EIOyEseuCqOyLnCDrtoTri7nqtawg7YyQ6rWQ66Gc!5e0!3m2!1sko!2skr!4v1534812939236" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->
