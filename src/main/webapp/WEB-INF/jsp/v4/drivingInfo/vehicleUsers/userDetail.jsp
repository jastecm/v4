<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<!-- 캘린더는 여기만 쓴다 여기에 선언하자. -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/common/js/calendar/fullcalendar.min.css" rel='stylesheet' />
<link rel="stylesheet" href="${pageContext.request.contextPath}/common/js/calendar/fullcalendar.print.min.css" rel='stylesheet' media='print' />

<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/calendar/moment.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/calendar/fullcalendar.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/calendar/locale-all.js"></script>
<style>
	#calendar {
    max-width: 100%;
    margin: 0 auto;
  }
</style>
<script type="text/javascript">
$(function() {
    // 달력
    $("#search-date").datepicker({
        dateformat: 'yy-mm-dd'
    });
    $("#search-date2").datepicker({
        dateformat: 'yy-mm-dd'
    });
    $("#start-date").datepicker({
        dateformat: 'yy-mm-dd'
    });
    $("#end-date").datepicker({
        dateformat: 'yy-mm-dd'
    });

    // 탭메뉴
    $(".tab-menu>li>a").click(function() {
        $(this).parent().addClass("on").siblings().removeClass("on");
        return false;
    });
    
    $('#calendar').fullCalendar({
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,basicWeek,basicDay'
        },
        defaultDate: '2018-03-12',
        navLinks: true, // can click day/week names to navigate views
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        events: [
          {
            title: 'All Day Event',
            start: '2018-03-01'
          },
          {
            title: 'Long Event',
            start: '2018-03-07',
            end: '2018-03-10'
          },
          {
            id: 999,
            title: 'Repeating Event',
            start: '2018-03-09T16:00:00'
          },
          {
            id: 999,
            title: 'Repeating Event',
            start: '2018-03-16T16:00:00'
          },
          {
            title: 'Conference',
            start: '2018-03-11',
            end: '2018-03-13'
          },
          {
            title: 'Meeting',
            start: '2018-03-12T10:30:00',
            end: '2018-03-12T12:30:00'
          },
          {
            title: 'Lunch',
            start: '2018-03-12T12:00:00'
          },
          {
            title: 'Meeting',
            start: '2018-03-12T14:30:00'
          },
          {
            title: 'Happy Hour',
            start: '2018-03-12T17:30:00'
          },
          {
            title: 'Dinner',
            start: '2018-03-12T20:00:00'
          },
          {
            title: 'Birthday Party',
            start: '2018-03-13T07:00:00'
          },
          {
            title: 'Click for Google',
            url: 'http://google.com/',
            start: '2018-03-28'
          }
        ]
      });
});

// 차량운행내역 조회
function detail(id) {
    var obj = document.getElementById("details" + id);
    if (obj) {
        obj.style.display = obj.style.display == "none" ? "table-row" : "none";
    }
}

function detail2(id) {
    var obj = document.getElementById("details2-" + id);
    if (obj) {
        obj.style.display = obj.style.display == "none" ? "table-row" : "none";
    }
}
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>차량ㆍ사용자</h2>
                    <span>등록된 차량ㆍ사용자의 상태를 실시간으로 확인할 수 있습니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="driving-page">
                    <div class="box-layout register-vehicle-detail">
                        <div class="title">
                            <h3 class="tit3">등록사용자 상세정보</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
                            <!-- 상단 상태값 -->
                            <div class="top-state clr">
                                <!-- <div class="item">
                                    <span>운행여부</span>
                                    <strong class="states-value state07">주차중</strong>
                                </div> -->

                                <div class="time">
                                    <span>2018/05/27</span>
                                    <span>19:53</span>
                                    <span>현재</span>
                                    <button class="btn btn04">새로고침</button>
                                </div>
                            </div>
                            <!--/ 상단 상태값 -->

                            <div class="vehicle-info">
                                <div class="img">
                                    <img style="width: 200px;" src="${pageContext.request.contextPath}/common/new/img/common/user-img2.png" alt="" />
                                </div>
                                <div class="info" style="margin-left: 230px;">
                                    <table class="table mgt20">
                                        <caption>등록차량 정보</caption>
                                        <colgroup>
                                            <col style="width:21.5%" />
                                            <col style="width:28.5%" />
                                            <col style="width:21.5%" />
                                            <col style="width:28.5%" />
                                        </colgroup>
                                        <tbody>
                                            <tr>
                                                <th scope="row">소속</th>
                                                <td colspan="3">viewcar</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">부서/이름</th>
                                                <td colspan="3">영업지원팀/홍길동</td>
                                            </tr>
                                        </tbody>
                                        <tbody>
                                            <tr>
                                                <th scope="row">전화번호</th>
                                                <td>031-111-1111</td>
                                                <th scope="row">휴대폰</th>
                                                <td>010-1234-1234</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">이메일</th>
                                                <td>honggildong@naver.com</td>
                                                <!-- <th scope="row">총주행거리</th>
                                                <td>949.2 km</td> -->
                                            </tr>
                                            <!-- <tr>
                                                <th scope="row">제조사/차종</th>
                                                <td>르노삼성/SM5 노바 LE</td>
                                                <th scope="row">모델명/년식</th>
                                                <td>SM5/2018</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">유종</th>
                                                <td>가솔린</td>
                                                <th scope="row">색상</th>
                                                <td>검정</td>
                                            </tr> -->
                                        </tbody>
                                        <tbody>
                                        	<tr>
                                        		<td colspan="4" style="border:0;font-weight: bold;color: #222;">사용차량 정보</td>
                                        	</tr>
                                        </tbody>
                                        <tbody>
                                            <tr>
                                                <th scope="row">차량선택</th>
                                                <td>그랜서/12조1234</td>
                                                <th scope="row">ViewCar 모델명/일련번호</th>
                                                <td>von-S31/A0001921</td>
                                            </tr>
                                            <!-- <tr>
                                                <th scope="row">자동차검사만기일</th>
                                                <td>2011-12-22</td>
                                                <th scope="row">비고</th>
                                                <td>&nbsp;</td>
                                            </tr> -->
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <ul class="page-tab tab-menu col-4">
                                <!-- 차량 현황 -->
                                <li class="list01 on">
                                    <a href="#">배차캘린더</a>
                                    <div class="tab-list">
                                    	<div id='calendar'></div>
                                    </div>
                                </li>

                                <li class="list02">
                                    <a href="#">운행 및 경비 현황</a>
                                    <div class="tab-list">
                                        <h3 class="tit2 help">
                                            차량운행 및 경비 요약
                                            <dl class="help-layer">
                                                <dt>기본정보 Tip</dt>
                                                <dd>
                                                    <b>툴팁테스트</b> : 툴팁내용
                                                </dd>
                                            </dl>
                                        </h3>
                                        <!-- table 버튼 -->
                                        <div class="btn-function title-side">
                                            <div class="right">
                                                <div class="car-condition">
                                                    차량상태
                                                    <span>정상</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ table 버튼 -->

                                        <!-- 상단 검색 -->
                                        <div class="top-search mgt20">
                                            <input type="text" id="search-date" name="" />
                                            <button class="btn btn03">조회</button>
                                        </div>
                                        <!--/ 상단 검색 -->

                                        <div class="chart clr">
                                            <!-- 
												기존 차트 사용
												<i class="icon up"></i>는 삭제

												<div id="myChart01" class="myChart">
													<div class="level">A</div>
												</div>
											-->
                                            <div id="myChart01" class="myChart">
                                                <div class="level">A</div>
                                                <svg width="154" height="154" viewBox="0 0 154 154" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
													<path d="M 77.05653919646207 6.000022511839745 A 71 71 0 1 1 77.05048238843345 6.000017946984954 Z M 77.0305027389267 34.10001084402331 A 42.9 42.9 0 1 0 77.03416241589046 34.100013602224294 Z"
													    fill="#fff"></path>
													<g opacity="0.9999998780673678">
														<path stroke-width="3" stroke="" fill="#a150cb" data-order="0" d="M 77 10 A 67 67 0 1 1 40.77702597423142 20.63603852878601 L 51.64391818196199 37.54522697015021 A 46.9 46.9 0 1 0 77 30.1 Z"></path>
														<path stroke-width="3" stroke="" fill="#ea2b7e" data-order="1" d="M 40.77702597423142 20.63603852878601 A 67 67 0 0 1 58.12387167916317 12.7139845719514 L 63.78671017541422 31.99978920036598 A 46.9 46.9 0 0 0 51.64391818196199 37.54522697015021 Z"></path>
														<path stroke-width="3" stroke="" fill="#68b2c2" data-order="2" d="M 58.12387167916317 12.7139845719514 A 67 67 0 0 1 76.99994866960334 10.000000000019654 L 76.99996406872233 30.100000000013765 A 46.9 46.9 0 0 0 63.78671017541422 31.99978920036598 Z"></path>
													</g>
												</svg>
                                            </div>
                                            <!--/ 기존 차트 사용 -->

                                            <!-- 차트 점수 -->
                                            <div class="chart-explain">
                                                <ul>
                                                    <li class="purple">
                                                        연비
                                                        <span>A</span>
                                                    </li>
                                                    <li class="pink">
                                                        안전점수
                                                        <span>A</span>
                                                    </li>
                                                    <li class="blue">
                                                        에코점수
                                                        <span>A</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!--/ 차트 점수 -->

                                            <!-- 차량 운행 정보 -->
                                            <div class="detail">
                                                <table class="table mgt20">
                                                    <caption>차량 운행 정보</caption>
                                                    <tbody>
                                                        <tr>
                                                            <th scope="row">거리</th>
                                                            <td>149.2 km</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">시간</th>
                                                            <td>5시간 25분 10초</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">연비 소모량</th>
                                                            <td>18.2 ℓ</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">주유비</th>
                                                            <td>29,400 원 (전국평균유가 기준)</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!--/ 차량 운행 정보 -->
                                        </div>

                                        <div class="total-price">
                                            총 운영비
                                            <span>0원</span>
                                        </div>

                                        <!-- 차량 경비 내역 -->
                                        <div class="vehicle-expenses">
                                            <table class="table mgt20">
                                                <caption>차량 경비 내역</caption>
                                                <colgroup>
                                                    <col style="width:14.75%" />
                                                    <col style="width:35.25%" />
                                                    <col style="width:14.75%" />
                                                    <col style="width:35.25%" />
                                                </colgroup>
                                                <thead>
                                                    <tr>
                                                        <th scope="col">항목</th>
                                                        <th scope="col">총 비용(전월대비)</th>
                                                        <th scope="col">항목</th>
                                                        <th scope="col">총 비용(전월대비)</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <tr>
                                                        <th scope="row">주유</th>
                                                        <td>
                                                            50,000원
                                                            <span>
																(70,000원
																<i class="up">▲</i>
																)
															</span>
                                                        </td>
                                                        <th scope="row">정비</th>
                                                        <td>
                                                            50,000원
                                                            <span>
																(100,000원
																<i class="down">▼</i>
																)
															</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">주차</th>
                                                        <td>
                                                            50,000원
                                                            <span>
																(70,000원
																<i>-</i>
																)
															</span>
                                                        </td>
                                                        <th scope="row">통행료</th>
                                                        <td>
                                                            50,000원
                                                            <span>
																(70,000원
																<i>-</i>
																)
															</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">렌탈료</th>
                                                        <td>
                                                            50,000원
                                                            <span>
																(70,000원
																<i>-</i>
																)
															</span>
                                                        </td>
                                                        <th scope="row">세차</th>
                                                        <td>
                                                            50,000원
                                                            <span>
																(70,000원
																<i>-</i>
																)
															</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">과태료</th>
                                                        <td>
                                                            50,000원
                                                            <span>
																(70,000원
																<i>-</i>
																)
															</span>
                                                        </td>
                                                        <th scope="row">기타</th>
                                                        <td>
                                                            50,000원
                                                            <span>
																(70,000원
																<i>-</i>
																)
															</span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <h3 class="tit2 mgt40">차량운행내역 조회</h3>

                                        <!-- table 버튼 -->
                                        <div class="btn-function mgt20">
                                            <div class="left">
                                                <select name="">
													<option value="">최근 운행순</option>
													<option value="">최근 등록순</option>
												</select>

                                                <div class="date">
                                                    <button class="active">전체</button>
                                                    <button>오늘</button>
                                                    <button>1주일</button>
                                                    <button>1개월</button>
                                                    <button>3개월</button>
                                                    <button>6개월</button>
                                                    <div class="direct">
                                                        <input type="text" id="start-date" /> &nbsp;~&nbsp;
                                                        <input type="text" id="end-date" />
                                                    </div>
                                                    <!-- 기획서에는 직접입력이 셀렉트박스로 되어 있는데, 사용자가 어떻게 날짜를 입력 해야 하는지 모르겠어서 임의로 변경했습니다.-->
                                                </div>
                                            </div>
                                            <div class="right">
                                                <select name="">
													<option value="">5건씩 보기</option>
												</select>
                                            </div>
                                        </div>
                                        <!--/ table 버튼 -->

                                        <table class="table list mgt20">
                                            <caption>운행기록 리스트</caption>
                                            <colgroup>
                                                <col style="width:10%" />
                                                <col style="width:25%" />
                                                <col />
                                                <col style="width:12%" />
                                            </colgroup>
                                            <thead>
                                                <tr>
                                                    <th scope="col">회사명</th>
                                                    <th scope="col">차량 및 사용자</th>
                                                    <th scope="col">운행정보</th>
                                                    <th scope="col">
                                                        <select name="" class="arrow">
															<option value="">경고</option>
															<option value="">지역</option>
															<option value="">시간</option>
														</select>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <!-- tbody가 반복 -->
                                            <tbody>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <div class="car-img">
                                                            <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" alt="" /></span><br />
                                                            <span class="num">쏘나타 63호 5703</span>
                                                        </div>
                                                    </td>
                                                    <td class="left">
                                                        <div class="driving-info with-btn">
                                                            <ul>
                                                                <li>
                                                                    <strong class="title w40">출발</strong>
                                                                    <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                                </li>
                                                                <li>
                                                                    <strong class="title w40">도착</strong>
                                                                    <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                                </li>
                                                            </ul>
                                                            <button class="btn-img location">위치</button>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        이수지역<br />
                                                        <button class="states-value state04 btn-toggle" onClick="javascript:detail(1)">상세기록</button>
                                                    </td>
                                                </tr>
                                                <tr id="details1" class="detail" style="display:none">
                                                    <td>자스텍엠</td>
                                                    <td>
                                                        <div class="user-img">
                                                            <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/common/user-default.png"alt="" /></span><br />
                                                            <span class="name">maumgolf / 카카오VX</span>
                                                        </div>
                                                    </td>
                                                    <td colspan="3" class="left">
                                                        <div class="car-info">
                                                            <ul class="grade">
                                                                <li><span>안전점수</span>E(0.0점)</li>
                                                                <li><span>연비점수</span>A(89.6점)</li>
                                                                <li><span>에코점수</span>E(0.0점)</li>
                                                            </ul>

                                                            <ul class="spec">
                                                                <li><span>주행시간</span>0시간 10분 01초</li>
                                                                <li><span>평균속도</span>0 km/h</li>
                                                                <li><span>냉각수</span>0 ℃</li>
                                                                <li><span>주행거리</span>5.4 km</li>
                                                                <li><span>최고속도</span>0 km/h</li>
                                                                <li><span>배터리</span>0.0 v</li>
                                                                <li><span>연비</span>11.4 km/ℓ</li>
                                                                <li><span>연료</span>0.5 ℓ</li>
                                                                <li><span>발전기</span>0.0 v</li>
                                                                <li><span>co2배출량</span>0 ㎎</li>
                                                                <li><span>퓨얼컷</span>0 초</li>
                                                                <li><span>공회전</span>0 초</li>
                                                                <li><span>웜업시간</span>0 초</li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                            <!--/ tbody가 반복 -->

                                            <tbody>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <div class="car-img">
                                                            <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" alt="" /></span><br />
                                                            <span class="num">쏘나타 63호 5703</span>
                                                        </div>
                                                    </td>
                                                    <td class="left">
                                                        <div class="driving-info with-btn">
                                                            <ul>
                                                                <li>
                                                                    <strong class="title w40">출발</strong>
                                                                    <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                                </li>
                                                                <li>
                                                                    <strong class="title w40">도착</strong>
                                                                    <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                                </li>
                                                            </ul>
                                                            <button class="btn-img location">위치</button>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        이수지역<br />
                                                        <button class="states-value state04 btn-toggle" onClick="javascript:detail(2)">상세기록</button>
                                                    </td>
                                                </tr>
                                                <tr id="details2" class="detail" style="display:none">
                                                    <td>자스텍엠</td>
                                                    <td>
                                                        <div class="user-img">
                                                            <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/common/user-default.png"alt="" /></span><br />
                                                            <span class="name">maumgolf / 카카오VX</span>
                                                        </div>
                                                    </td>
                                                    <td colspan="3" class="left">
                                                        <div class="car-info">
                                                            <ul class="grade">
                                                                <li><span>안전점수</span>E(0.0점)</li>
                                                                <li><span>연비점수</span>A(89.6점)</li>
                                                                <li><span>에코점수</span>E(0.0점)</li>
                                                            </ul>

                                                            <ul class="spec">
                                                                <li><span>주행시간</span>0시간 10분 01초</li>
                                                                <li><span>평균속도</span>0 km/h</li>
                                                                <li><span>냉각수</span>0 ℃</li>
                                                                <li><span>주행거리</span>5.4 km</li>
                                                                <li><span>최고속도</span>0 km/h</li>
                                                                <li><span>배터리</span>0.0 v</li>
                                                                <li><span>연비</span>11.4 km/ℓ</li>
                                                                <li><span>연료</span>0.5 ℓ</li>
                                                                <li><span>발전기</span>0.0 v</li>
                                                                <li><span>co2배출량</span>0 ㎎</li>
                                                                <li><span>퓨얼컷</span>0 초</li>
                                                                <li><span>공회전</span>0 초</li>
                                                                <li><span>웜업시간</span>0 초</li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <!-- 페이징 -->
                                        <div id="paging">
                                            <a href="" class="first icon-spr">제일 처음으로</a>
                                            <a href="" class="prev icon-spr">이전으로</a>
                                            <span class="current">1</span>
                                            <a href="" class="num">2</a>
                                            <a href="" class="num">3</a>
                                            <a href="" class="num">4</a>
                                            <a href="" class="num">5</a>
                                            <a href="" class="next icon-spr">다음으로</a>
                                            <a href="" class="last icon-spr">제일 마지막으로</a>
                                        </div>
                                        <!--/ 페이징 -->
                                    </div>
                                </li>

                                <!-- 운행결로 및 위험운전 -->
                                <li class="list03">
                                    <a href="#">운행경로 및 위험운전</a>
                                    <div class="tab-list">
                                        <!-- 상단 검색 -->
                                        <div class="top-search mgt20">
                                            <input type="text" name="" id="search-date2" />
                                            <button class="btn btn03">조회</button>
                                        </div>
                                        <!--/ 상단 검색 -->

                                        <div class="mgt30">
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d25357.461822122736!2d127.10473640699269!3d37.39733480314603!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x357ca7fe934881c7%3A0x30db2f32566ac8fb!2z6rK96riw64-EIOyEseuCqOyLnCDrtoTri7nqtawg7YyQ6rWQ66Gc!5e0!3m2!1sko!2skr!4v1534812939236"
                                                width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                        </div>

                                        <div class="violation-list">
                                            <div class="inner">
                                                <div class="list01">급출발<span> 0회</span></div>
                                                <div class="list02">급정지<span> 0회</span></div>
                                                <div class="list03">급가속<span> 0회</span></div>
                                                <div class="list04">급감속<span> 0회</span></div><br />
                                                <div class="list05">급회전<span> 0회</span></div>
                                                <div class="list06">급유턴<span> 0회</span></div>
                                                <div class="list07">과속<span> 0회</span></div>
                                                <div class="list08">장기과속<span> 0회</span></div>
                                            </div>
                                            <p>주행기록중 차량 사용자의 10대 위험운전행동이 특정지점(POI)로 표시되어 있습니다.</p>
                                        </div>


                                        <h3 class="tit2 mgt40">차량운행내역 조회</h3>
                                        <table class="table list mgt20">
                                            <caption>운행기록 리스트</caption>
                                            <colgroup>
                                                <col style="width:25%" />
                                                <col />
                                                <col style="width:12%" />
                                            </colgroup>
                                            <thead>
                                                <tr>
                                                    <th scope="col">차량 및 사용자</th>
                                                    <th scope="col">운행정보</th>
                                                    <th scope="col">
                                                        <select name="" class="arrow">
																<option value="">경고</option>
																<option value="">지역</option>
																<option value="">시간</option>
															</select>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <!-- tbody가 반복 -->
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="car-img">
                                                            <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" alt="" /></span><br />
                                                            <span class="num">쏘나타 63호 5703</span>
                                                        </div>
                                                    </td>
                                                    <td class="left">
                                                        <div class="driving-info with-btn">
                                                            <ul>
                                                                <li>
                                                                    <strong class="title w40">출발</strong>
                                                                    <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                                </li>
                                                                <li>
                                                                    <strong class="title w40">도착</strong>
                                                                    <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                                </li>
                                                            </ul>
                                                            <button class="btn-img location">위치</button>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        이수지역<br />
                                                        <button class="states-value state04 btn-toggle" onClick="javascript:detail2(1)">상세기록</button>
                                                    </td>
                                                </tr>
                                                <tr id="details2-1" class="detail" style="display:none">
                                                    <td>
                                                        <div class="user-img">
                                                            <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/common/user-default.png"alt="" /></span><br />
                                                            <span class="name">maumgolf / 카카오VX</span>
                                                        </div>
                                                    </td>
                                                    <td colspan="3" class="left">
                                                        <div class="car-info">
                                                            <ul class="grade">
                                                                <li><span>안전점수</span>E(0.0점)</li>
                                                                <li><span>연비점수</span>A(89.6점)</li>
                                                                <li><span>에코점수</span>E(0.0점)</li>
                                                            </ul>

                                                            <ul class="spec">
                                                                <li><span>주행시간</span>0시간 10분 01초</li>
                                                                <li><span>평균속도</span>0 km/h</li>
                                                                <li><span>냉각수</span>0 ℃</li>
                                                                <li><span>주행거리</span>5.4 km</li>
                                                                <li><span>최고속도</span>0 km/h</li>
                                                                <li><span>배터리</span>0.0 v</li>
                                                                <li><span>연비</span>11.4 km/ℓ</li>
                                                                <li><span>연료</span>0.5 ℓ</li>
                                                                <li><span>발전기</span>0.0 v</li>
                                                                <li><span>co2배출량</span>0 ㎎</li>
                                                                <li><span>퓨얼컷</span>0 초</li>
                                                                <li><span>공회전</span>0 초</li>
                                                                <li><span>웜업시간</span>0 초</li>
                                                            </ul>

                                                            <p class="current">현재 주행중입니다.</p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                            <!--/ tbody가 반복 -->

                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="car-img">
                                                            <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" alt="" /></span><br />
                                                            <span class="num">쏘나타 63호 5703</span>
                                                        </div>
                                                    </td>
                                                    <td class="left">
                                                        <div class="driving-info with-btn">
                                                            <ul>
                                                                <li>
                                                                    <strong class="title w40">출발</strong>
                                                                    <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                                </li>
                                                                <li>
                                                                    <strong class="title w40">도착</strong>
                                                                    <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                                </li>
                                                            </ul>
                                                            <button class="btn-img location">위치</button>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        이수지역<br />
                                                        <button class="states-value state04 btn-toggle" onClick="javascript:detail2(2)">상세기록</button>
                                                    </td>
                                                </tr>
                                                <tr id="details2-2" class="detail" style="display:none">
                                                    <td>
                                                        <div class="user-img">
                                                            <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/common/user-default.png"alt="" /></span><br />
                                                            <span class="name">maumgolf / 카카오VX</span>
                                                        </div>
                                                    </td>
                                                    <td colspan="3" class="left">
                                                        <div class="car-info">
                                                            <ul class="grade">
                                                                <li><span>안전점수</span>E(0.0점)</li>
                                                                <li><span>연비점수</span>A(89.6점)</li>
                                                                <li><span>에코점수</span>E(0.0점)</li>
                                                            </ul>

                                                            <ul class="spec">
                                                                <li><span>주행시간</span>0시간 10분 01초</li>
                                                                <li><span>평균속도</span>0 km/h</li>
                                                                <li><span>냉각수</span>0 ℃</li>
                                                                <li><span>주행거리</span>5.4 km</li>
                                                                <li><span>최고속도</span>0 km/h</li>
                                                                <li><span>배터리</span>0.0 v</li>
                                                                <li><span>연비</span>11.4 km/ℓ</li>
                                                                <li><span>연료</span>0.5 ℓ</li>
                                                                <li><span>발전기</span>0.0 v</li>
                                                                <li><span>co2배출량</span>0 ㎎</li>
                                                                <li><span>퓨얼컷</span>0 초</li>
                                                                <li><span>공회전</span>0 초</li>
                                                                <li><span>웜업시간</span>0 초</li>
                                                            </ul>

                                                            <p class="current">현재 주행중입니다.</p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <!-- 페이징 -->
                                        <div id="paging">
                                            <a href="" class="first icon-spr">제일 처음으로</a>
                                            <a href="" class="prev icon-spr">이전으로</a>
                                            <span class="current">1</span>
                                            <a href="" class="num">2</a>
                                            <a href="" class="num">3</a>
                                            <a href="" class="num">4</a>
                                            <a href="" class="num">5</a>
                                            <a href="" class="next icon-spr">다음으로</a>
                                            <a href="" class="last icon-spr">제일 마지막으로</a>
                                        </div>
                                        <!--/ 페이징 -->
                                    </div>
                                </li>
                                <!--/ 운행결로 및 위험운전 -->
                            </ul>
                        </div>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->