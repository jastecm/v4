<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<!-- 캘린더는 여기만 쓴다 여기에 선언하자. -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/common/js/calendar/fullcalendar.min.css" rel='stylesheet' />
<link rel="stylesheet" href="${pageContext.request.contextPath}/common/js/calendar/fullcalendar.print.min.css" rel='stylesheet' media='print' />

<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/calendar/moment.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/calendar/fullcalendar.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/calendar/locale-all.js"></script>


<%-- <script type="text/javascript" src="${pageContext.request.contextPath}/common/js/index.js"></script> --%>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jqplot/jquery.jqplot.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jqplot/jqplot.barRenderer.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jqplot/jqplot.highlighter.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jqplot/jqplot.cursor.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jqplot/jqplot.pointLabels.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jqplot/jqplot.categoryAxisRenderer.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jqplot/jqplot.canvasAxisTickRenderer.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jqplot/jqplot.canvasTextRenderer.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jqplot/jqplot.json2.js"></script>

<style>
	#calendar {
    max-width: 100%;
    margin: 0 auto;
  }
</style>
<script type="text/javascript">
var _vehicleKey = "${vehicleKey}";
$(function() {
	
	var initItemChart = function(){
		//정비이력 및 차량소모품 상태 그래프
		$V4.http_post("/api/1/vehicle/"+_vehicleKey+"/item", {}, {
			requestMethod : "GET",
			header : {
				"key" : "${_KEY}"
			},
			success : function(rtv) {
				console.log(rtv);
				var data = rtv.result;
				var dataCnt = data.length;
				var $div = $("#plot");
				$div.html("").css("height",dataCnt*35);
				var plot;
				var	chartTick = [],chartData = [],chartLabel =[],chartColor = [];
				for(var i = dataCnt-1 ; i >= 0 ; i--){
					chartTick.push(data[i].name);
					chartData.push(data[i].val>0?(data[i].val*100)/data[i].max:0);
					chartLabel.push(data[i].name);
					chartColor.push(chartData[dataCnt-1-i]<10?'#f47321':'#92cc70');
				}
				
				plot = $div.jqplot('plot',[chartData],{	    	    
					animate: !$.jqplot.use_excanvas
					,grid:{
						drawGridlines : false
						,drawBorder : false
						,shadow : false
					}
				   	,seriesColors:chartColor
			       	,seriesDefaults:{
						renderer:$.jqplot.BarRenderer
			           	,rendererOptions: {
							animation: {
								speed: 2500
							}
							,varyBarColor: true
							,barDirection: 'horizontal'
						}
					}      
			        ,cursor: {
			            show: false
			            ,zoom: false
			            ,looseZoom: false
			            ,showTooltip: false
			        }
			 		,axes: {
				        yaxis: {
				        	renderer: $.jqplot.CategoryAxisRenderer
				        	,tickRenderer: $.jqplot.CanvasAxisTickRenderer
				            ,ticks: chartTick
				    	}
				        ,xaxis: {                    
				               min:0
				               ,max:100
				               ,tickInterval : 10
				        }
				    }
			        ,series:chartLabel
				});
				
			},
			error : function(t) {
				console.log(t);
			}
		});
	}
	
	//차량정보를 가져 온다.
	$V4.http_post("/api/1/vehicle/"+_vehicleKey, {}, {
		requestMethod : "GET",
		header : {
			"key" : "${_KEY}"
		},
		success : function(rtv) {
			console.log(rtv);
			var data = rtv.result;
			
			if(data.drivingState == 'unused'){
				$('#drivingState').addClass('state07').text("사용중지");
			}else if(data.drivingState == 'parking'){
				$('#drivingState').addClass('state08').text("주차중");
			}else if(data.drivingState == 'driving'){
				$('#drivingState').addClass('state04').text("주행중");
			}
			
			if(data.hasOwnProperty('device')){
				//단말기가 장착된거다
				$('#deviceSeries').text(data.device.deviceSeries+"/"+data.device.deviceSn);
			}
			$('#corpName').text(data.corp.corpName);
			$('#plateNum').text(data.plateNum);
			$('#totDistance').text(number_format(Math.round(data.totDistance / 1000))+" km");
			$('#volumn').text(data.vehicleModel.volume+" cc");
			$('#transmission').text(data.vehicleModel.transmission);
			$('#manufactureAndmodelHeader').text(data.vehicleModel.manufacture+"/"+data.vehicleModel.modelHeader);
			$('#modelMasterAndyear').text(data.vehicleModel.modelMaster+"/"+data.vehicleModel.year);
			$('#fuelType').text(data.vehicleModel.fuelType);
			
			//color 없음
			//차량등록일
			//차대번호
			//자동차검사만기일
			//비고
			
		},
		error : function(t) {
			console.log(t);
		}
	});
	
	$('#searchDate').attr('value', new Date().yyyymm());
	$('#searchDate2').attr('value', new Date().yyyymmdd());
	
	/* $('#searchDate').monthpicker({
		years: getMonthPickerYear(5),
		topOffset: 6
	}); */ 
	
	$(".monthPicker").datepicker({ 
        dateFormat: 'mm-yy',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,

        onClose: function(dateText, inst) {  
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val(); 
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val(); 
            $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
        }
    });

    $(".monthPicker").focus(function () {
        $(".ui-datepicker-calendar").hide();
        $("#ui-datepicker-div").position({
            my: "center top",
            at: "center bottom",
            of: $(this)
        });    
    });
	
    
    //월별통계
    $V4.http_post("/api/1/vehicle/"+_vehicleKey+"/summary/"+new Date().yyyymm2(), {}, {
		requestMethod : "GET",
		header : {
			"key" : "${_KEY}"
		},
		success : function(rtv) {
			console.log(rtv);
			var data = rtv.result;
			
			var summary = data.summary
			summary.distance; //거리
			summary.drivingTime;//시간
			summary.fco //연료소모량
			summary.oilPrice //주유비
			
			var drivingTime = summary?summary.drivingTime:0;
			var score4 = summary?summary.score4:0;
			var safeScore = summary?summary.safeScore:0;
			var ecoScore = summary?summary.ecoScore:0;
			
			var fuelRank = calRank(summary.score4);
			var safeRank = calRank(summary.safeScore);
			var ecoRank = calRank(summary.ecoScore);
			
			$('#fuelRank').text(fuelRank);
			$('#safeRank').text(safeRank);
			$('#ecoRank').text(ecoRank);
			
			$("#distance").text(""+number_format(parseFloat(summary?(summary.distance/1000):0).toFixed(1))+" km");
			$("#drivingTime").text(""+parseInt(drivingTime/3600)+'시간 '+LPAD(''+parseInt((drivingTime%3600)/60),'0',2)+'분 '+LPAD(''+parseInt((drivingTime%3600)%60),'0',2)+'초');
			$("#fco").text(""+number_format(parseFloat(summary?(summary.fco/1000):0).toFixed(1))+" ℓ");
			$("#oilPrice").text(""+number_format(summary?summary.oilPrice:0)+" 원");
			
		},
		error : function(t) {
			console.log(t);
		}
	});
    
	
    // 달력
    /* $("#search-date").datepicker({
        dateformat: 'yy-mm-dd'
    }); */
    $("#searchDate2").datepicker({
        dateformat: 'yy-mm-dd'
    });
    $("#start-date").datepicker({
        dateformat: 'yy-mm-dd'
    });
    $("#end-date").datepicker({
        dateformat: 'yy-mm-dd'
    });

    // 탭메뉴
    $(".tab-menu>li>a").click(function() {
        $(this).parent().addClass("on").siblings().removeClass("on");
        var tab = $(this).data('tab');
        if(tab == "4"){
        	initItemChart();
        }else if(tab == "3"){
        	var searchDate = replaceAll($('#searchDate2').val(),'/','');
        	//$('#mapFrame').attr('src','/map/openMap?searchType=date&searchDate='+searchDate+'&vehicleKey=108d1259-bcc0-11e8-8564-42010a8c0091');
        	$('#mapFrame').attr('src','/map/openMap?searchType=date&searchDate='+searchDate+'&vehicleKey='+_vehicleKey);
        	
        }
        
        
        return false;
    });
    
    $('#drivingLocationSearchBtn').on('click',function(){
    	var searchDate = replaceAll($('#searchDate2').val(),'/','');
    	//$('#mapFrame').attr('src','/map/openMap?searchType=date&searchDate='+searchDate+'&vehicleKey=108d1259-bcc0-11e8-8564-42010a8c0091');
    	var mapFrame = $("#mapFrame").get(0).contentWindow;
    	mapFrame.searchDate = searchDate;
    	mapFrame._o = mapFrame.createOption();
    	mapFrame.outerReset()
    });
    
    $('#calendar').fullCalendar({
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,basicWeek,basicDay'
        },
        defaultDate: '2018-03-12',
        navLinks: true, // can click day/week names to navigate views
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        events: [
          {
            title: 'All Day Event',
            start: '2018-03-01'
          },
          {
            title: 'Long Event',
            start: '2018-03-07',
            end: '2018-03-10'
          },
          {
            id: 999,
            title: 'Repeating Event',
            start: '2018-03-09T16:00:00'
          },
          {
            id: 999,
            title: 'Repeating Event',
            start: '2018-03-16T16:00:00'
          },
          {
            title: 'Conference',
            start: '2018-03-11',
            end: '2018-03-13'
          },
          {
            title: 'Meeting',
            start: '2018-03-12T10:30:00',
            end: '2018-03-12T12:30:00'
          },
          {
            title: 'Lunch',
            start: '2018-03-12T12:00:00'
          },
          {
            title: 'Meeting',
            start: '2018-03-12T14:30:00'
          },
          {
            title: 'Happy Hour',
            start: '2018-03-12T17:30:00'
          },
          {
            title: 'Dinner',
            start: '2018-03-12T20:00:00'
          },
          {
            title: 'Birthday Party',
            start: '2018-03-13T07:00:00'
          },
          {
            title: 'Click for Google',
            url: 'http://google.com/',
            start: '2018-03-28'
          }
        ]
      });
    <c:if test="${V4.corp.corpType eq '4'}">  
    var thead = [
	         {'회사명' : '10%'},
             {'차량 및 사용자' : '25%'},
             {'운행정보' : '53%'},
             {'<select name="" class="arrow"><option value="">경고</option><option value="">지역</option><option value="">시간</option></select>' : '12%'}
    ];
    </c:if>
    <c:if test="${V4.corp.corpType ne '4'}">
    var thead = [
                 {'차량 및 사용자' : '30%'},
                 {'운행정보' : '58%'},
                 {'<select name="" class="arrow"><option value="">경고</option><option value="">지역</option><option value="">시간</option></select>' : '12%'}
        ];
    </c:if>
    
    var vehicleSetting = {
    		thead : thead
    		,bodyHtml : (function(data){
    			var strHtml = "";
    			for(i in data.result){
    				var obj = data.result[i];
    				
    				console.log(obj);
    				strHtml += '<tbody>';
    				strHtml += '    <tr>';
    				<c:if test="${V4.corp.corpType eq '4'}">
    				strHtml += '        <td></td>'; //회사명
    				</c:if>
    				strHtml += '        <td>';
    				strHtml += '            <div class="car-img">';
    				strHtml += '                <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" alt="" /></span><br />';
    				strHtml += '                <span class="num">'+getProperty(obj,"vehicle.vehicleModel.modelMaster")+' '+getProperty(obj,"vehicle.plateNum")+'</span>';
    				strHtml += '            </div>';
    				strHtml += '        </td>';
    				strHtml += '        <td class="left">';
    				strHtml += '            <div class="driving-info with-btn">';
    				strHtml += '                <ul>';
    				strHtml += '                    <li>';
    				strHtml += '                        <strong class="title w40">출발</strong>';
    				strHtml += '                        <span>'+convertDateUint(new Date(obj.startDate),_unitDate,_timezoneOffset,1)+' '+convertNullString(getProperty(obj,"startAddr"),'위치 정보가 없습니다.')+' </span>';
    				strHtml += '                    </li>';
    				strHtml += '                    <li>';
    				strHtml += '                        <strong class="title w40">도착</strong>';
    				strHtml += '                        <span>'+convertDateUint(new Date(obj.endDate),_unitDate,_timezoneOffset,1)+' '+convertNullString(getProperty(obj,"endAddr"),'위치 정보가 없습니다.')+'</span>';
    				strHtml += '                    </li>';
    				strHtml += '                </ul>';
    				strHtml += '                <button class="btn-img location">위치</button>';
    				strHtml += '            </div>';
    				strHtml += '        </td>';
    				strHtml += '        <td>';
    				strHtml += '            이수지역<br />';
    				strHtml += '            <button class="states-value state04 drivingDetail" data-state="close">상세기록</button>';
    				strHtml += '        </td>';
    				strHtml += '    </tr>';
    				strHtml += '    <tr class="detail" style="display:none">';
    				<c:if test="${V4.corp.corpType eq '4'}">
    				strHtml += '        <td>'+getProperty(obj,'vehicle.corp.corpName')+'</td>';
    				</c:if>
    				
    				
    				
    				strHtml += '        <td>';
    				strHtml += '            <div class="user-img">';
    				strHtml += '                <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/common/user-default.png"alt="" /></span><br />';
    				
    				if(obj.driver){
    					strHtml += '                <span class="name">'+obj.driver.name+'</span>';	
    				}else{
    					strHtml += '                <span class="name"></span>';
    				}
    				
    				
    				strHtml += '            </div>';
    				strHtml += '        </td>';
    				strHtml += '        <td colspan="3" class="left">';
    				strHtml += '            <div class="car-info">';
    				strHtml += '                <ul class="grade">';
    				strHtml += '                    <li><span>안전점수</span>'+calScore(parseFloat(obj.safeScore).toFixed(1))+'</li>';
    				strHtml += '                    <li><span>연비점수</span>'+calScore((obj.avgFco/obj.fuelEfficiency*100).toFixed(1))+'</li>';
    				strHtml += '                    <li><span>에코점수</span>'+calScore(parseFloat(obj.ecoScore).toFixed(1))+'</li>';
    				strHtml += '                </ul>';
    				strHtml += '                <ul class="spec">';
    				strHtml += '                    <li><span>주행시간</span>'+secondToTime(obj.drivingTime)+'</li>';
    				strHtml += '                    <li><span>평균속도</span>'+obj.meanSpeed+' km/h</li>';
    				strHtml += '                    <li><span>냉각수</span>'+obj.coolantTemp+' ℃</li>';
    				strHtml += '                    <li><span>주행거리</span>'+minusError(number_format((obj.distance/1000).toFixed(1)))+' km</li>';
    				strHtml += '                    <li><span>최고속도</span>'+obj.highSpeed+' km/h</li>';
    				strHtml += '                    <li><span>배터리</span>'+number_format(parseFloat(obj.ecuVolt).toFixed(1))+' v</li>';
    				strHtml += '                    <li><span>연비</span>'+number_format(parseFloat(obj.avgFco).toFixed(1))+' km/ℓ</li>';
    				strHtml += '                    <li><span>연료</span>'+number_format((obj.fco/1000).toFixed(1))+' ℓ</li>';
    				strHtml += '                    <li><span>발전기</span>'+number_format(parseFloat(obj.deviceVolt).toFixed(1))+' v</li>';
    				strHtml += '                    <li><span>co2배출량</span>'+number_format(obj.co2Emission)+' ㎎</li>';
    				strHtml += '                    <li><span>퓨얼컷</span>'+number_format(obj.fuelCutTime)+' 초</li>';
//     				strHtml += '                    <li><span>공회전</span>'+number_format(obj.idleTime)+' 초</li>';
    				strHtml += '                    <li><span>웜업시간</span>'+number_format(obj.warmupTime)+' 초</li>';
    				strHtml += '                </ul>';
    				strHtml += '            </div>';
    				strHtml += '        </td>';
    				strHtml += '    </tr>';
    				strHtml += '</tbody>';
    			}
    			return strHtml;
    		})
    		,limit : parseInt($(".func_limitChange").val())
    		,pagePerGroup : 10 //pagingGroup size
    		,url : "/api/1/vehicle/"+_vehicleKey+"/trip"
    		,param : {}
    		,http_post_option : {
    			requestMethod : "GET"
    			,header : {key : "${_KEY}"}
    		}
    		,initSearch : true //생성과 동시에 search		
    		,loadingBodyViewer : true //로딩중!! 표시됨
    		,debugMode : true
    	}
    
    $('#vehicleList').commonList(vehicleSetting);
    
    
    //상세기록 버튼 toggle형태로 만듬
    $(document).on('click','.drivingDetail',function(){
    	var self = $(this);
    	
    	//close면 닫혀있는상태 열어준다
    	var state = self.data('state');
    	
    	if(state == "close"){
    		self.parent().parent().next().show();	
    		self.data('state','open');
    	}else{//open 상태면 닫아준다.
    		self.parent().parent().next().hide();
    		self.data('state','close');
    	}
    	
    	
    });
    
    $('#itemDetail').on('click',function(){
    	$V4.move('/vehicleMng/troubleAndConsumables/detail',{"vehicleKey":_vehicleKey});
    });
    
});

function outerSuddenSummaryCall(summary){
	$('#rapidStart').text(summary[0]+"회");
	$('#rapidStop').text(summary[1]+"회");
	$('#rapidAccel').text(summary[2]+"회");
	$('#rapidDeaccel').text(summary[3]+"회");
	$('#rapidTurn').text(summary[4]+"회");
	$('#rapidUtern').text(summary[5]+"회");
	$('#overSpeed').text(summary[6]+"회");
	$('#overSpeedLong').text(summary[7]+"회");
};
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>차량ㆍ사용자</h2>
                    <span>등록된 차량ㆍ사용자의 상태를 실시간으로 확인할 수 있습니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="driving-page">
                    <div class="box-layout register-vehicle-detail">
                        <div class="title">
                            <h3 class="tit3">등록차량 상세정보</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
                            <!-- 상단 상태값 -->
                            <div class="top-state clr">
                                <div class="item">
                                    <span>운행여부</span>
                                    <strong id="drivingState" class="states-value"></strong>
                                </div>

                                <div class="time">
                                    <span>2018/05/27</span>
                                    <span>19:53</span>
                                    <span>현재</span>
                                    <button class="btn btn04">새로고침</button>
                                </div>
                            </div>
                            <!--/ 상단 상태값 -->

                            <div class="vehicle-info">
                                <div class="img">
                                    <img src="${pageContext.request.contextPath}/common/new/img/vehicle/L47.png" alt="" />
                                </div>
                                <div class="info">
                                    <table class="table mgt20">
                                        <caption>등록차량 정보</caption>
                                        <colgroup>
                                            <col style="width:21.5%" />
                                            <col style="width:28.5%" />
                                            <col style="width:21.5%" />
                                            <col style="width:28.5%" />
                                        </colgroup>
                                        <tbody>
                                        	<c:if test="${V4.corp.corpType eq '4'}">
                                            <tr>
                                                <th scope="row">소속</th>
                                                <td colspan="3" id="corpName"></td>
                                            </tr>
                                            </c:if>
                                            <tr>
                                                <th scope="row">ViewCAR모델명/일련번호</th>
                                                <td colspan="3" id="deviceSeries"></td>
                                            </tr>
                                        </tbody>
                                        <tbody>
                                            <tr>
                                                <th scope="row">차량번호</th>
                                                <td id="plateNum"></td>
                                                <th scope="row">배기량</th>
                                                <td id="volumn"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">기어방식</th>
                                                <td id="transmission"></td>
                                                <th scope="row">총주행거리</th>
                                                <td id="totDistance"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">제조사/차종</th>
                                                <td id="manufactureAndmodelHeader"></td>
                                                <th scope="row">모델명/년식</th>
                                                <td id="modelMasterAndyear"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">유종</th>
                                                <td id="fuelType"></td>
                                                <th scope="row">색상</th>
                                                <td id="color"><!-- 검정 --></td>
                                            </tr>
                                        </tbody>
                                        <tbody>
                                            <tr>
                                                <th scope="row">차량등록일</th>
                                                <td><!-- 2011-12-22 --></td>
                                                <th scope="row">차대번호</th>
                                                <td><!-- KN234266ND523423G --></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">자동차검사만기일</th>
                                                <td><!-- 2011-12-22 --></td>
                                                <th scope="row">비고</th>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <ul class="page-tab tab-menu col-4">
                                <!-- 차량 현황 -->
                                <li class="list01 on">
                                    <a href="javascript:void(0);" data-tab="1">배차캘린더</a>
                                    <div class="tab-list">
                                    	<div id='calendar'></div>
                                    </div>
                                </li>

                                <li class="list02">
                                    <a href="javascript:void(0);" data-tab="2">운행 및 경비 현황</a>
                                    <div class="tab-list">
                                        <h3 class="tit2 help">
                                            차량운행 및 경비 요약
                                            <dl class="help-layer">
                                                <dt>기본정보 Tip</dt>
                                                <dd>
                                                    <b>툴팁테스트</b> : 툴팁내용
                                                </dd>
                                            </dl>
                                        </h3>
                                        <!-- table 버튼 -->
                                        <div class="btn-function title-side">
                                            <div class="right">
                                                <div class="car-condition">
                                                    차량상태
                                                    <span>정상</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ table 버튼 -->

                                        <!-- 상단 검색 -->
                                        <div class="top-search mgt20">
                                            <input type="text" class="monthPicker" id="searchDate" name="searchDate" />
                                            <button class="btn btn03">조회</button>
                                        </div>
                                        <!--/ 상단 검색 -->

                                        <div class="chart clr">
                                            <!-- 
												기존 차트 사용
												<i class="icon up"></i>는 삭제

												<div id="myChart01" class="myChart">
													<div class="level">A</div>
												</div>
											-->
                                            <div id="myChart01" class="myChart">
                                                <div class="level">A</div>
                                                <svg width="154" height="154" viewBox="0 0 154 154" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
													<path d="M 77.05653919646207 6.000022511839745 A 71 71 0 1 1 77.05048238843345 6.000017946984954 Z M 77.0305027389267 34.10001084402331 A 42.9 42.9 0 1 0 77.03416241589046 34.100013602224294 Z"
													    fill="#fff"></path>
													<g opacity="0.9999998780673678">
														<path stroke-width="3" stroke="" fill="#a150cb" data-order="0" d="M 77 10 A 67 67 0 1 1 40.77702597423142 20.63603852878601 L 51.64391818196199 37.54522697015021 A 46.9 46.9 0 1 0 77 30.1 Z"></path>
														<path stroke-width="3" stroke="" fill="#ea2b7e" data-order="1" d="M 40.77702597423142 20.63603852878601 A 67 67 0 0 1 58.12387167916317 12.7139845719514 L 63.78671017541422 31.99978920036598 A 46.9 46.9 0 0 0 51.64391818196199 37.54522697015021 Z"></path>
														<path stroke-width="3" stroke="" fill="#68b2c2" data-order="2" d="M 58.12387167916317 12.7139845719514 A 67 67 0 0 1 76.99994866960334 10.000000000019654 L 76.99996406872233 30.100000000013765 A 46.9 46.9 0 0 0 63.78671017541422 31.99978920036598 Z"></path>
													</g>
												</svg>
                                            </div>
                                            <!--/ 기존 차트 사용 -->

                                            <!-- 차트 점수 -->
                                            <div class="chart-explain">
                                                <ul>
                                                    <li class="purple">
                                                        연비
                                                        <span id="fuelRank"></span>
                                                    </li>
                                                    <li class="pink">
                                                        안전점수
                                                        <span id="safeRank"></span>
                                                    </li>
                                                    <li class="blue">
                                                        에코점수
                                                        <span id="ecoRank"></span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!--/ 차트 점수 -->

                                            <!-- 차량 운행 정보 -->
                                            <div class="detail">
                                                <table class="table mgt20">
                                                    <caption>차량 운행 정보</caption>
                                                    <tbody>
                                                        <tr>
                                                            <th scope="row">거리</th>
                                                            <td id="distance">149.2 km</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">시간</th>
                                                            <td id="drivingTime">5시간 25분 10초</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">연비 소모량</th>
                                                            <td id="fco">18.2 ℓ</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">주유비</th>
                                                            <td id="oilPrice">29,400 원 (전국평균유가 기준)</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!--/ 차량 운행 정보 -->
                                        </div>

                                        <div class="total-price">
                                            총 운영비
                                            <span>0원</span>
                                        </div>

                                        <!-- 차량 경비 내역 -->
                                        <div class="vehicle-expenses">
                                            <table class="table mgt20">
                                                <caption>차량 경비 내역</caption>
                                                <colgroup>
                                                    <col style="width:14.75%" />
                                                    <col style="width:35.25%" />
                                                    <col style="width:14.75%" />
                                                    <col style="width:35.25%" />
                                                </colgroup>
                                                <thead>
                                                    <tr>
                                                        <th scope="col">항목</th>
                                                        <th scope="col">총 비용(전월대비)</th>
                                                        <th scope="col">항목</th>
                                                        <th scope="col">총 비용(전월대비)</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <tr>
                                                        <th scope="row">주유</th>
                                                        <td>
                                                            50,000원
                                                            <span>
																(70,000원
																<i class="up">▲</i>
																)
															</span>
                                                        </td>
                                                        <th scope="row">정비</th>
                                                        <td>
                                                            50,000원
                                                            <span>
																(100,000원
																<i class="down">▼</i>
																)
															</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">주차</th>
                                                        <td>
                                                            50,000원
                                                            <span>
																(70,000원
																<i>-</i>
																)
															</span>
                                                        </td>
                                                        <th scope="row">통행료</th>
                                                        <td>
                                                            50,000원
                                                            <span>
																(70,000원
																<i>-</i>
																)
															</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">렌탈료</th>
                                                        <td>
                                                            50,000원
                                                            <span>
																(70,000원
																<i>-</i>
																)
															</span>
                                                        </td>
                                                        <th scope="row">세차</th>
                                                        <td>
                                                            50,000원
                                                            <span>
																(70,000원
																<i>-</i>
																)
															</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">과태료</th>
                                                        <td>
                                                            50,000원
                                                            <span>
																(70,000원
																<i>-</i>
																)
															</span>
                                                        </td>
                                                        <th scope="row">기타</th>
                                                        <td>
                                                            50,000원
                                                            <span>
																(70,000원
																<i>-</i>
																)
															</span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <h3 class="tit2 mgt40">차량운행내역 조회</h3>

                                        <!-- table 버튼 -->
                                        <div class="btn-function mgt20">
                                           <!--  <div class="left">
                                                <select name="">
													<option value="">최근 운행순</option>
													<option value="">최근 등록순</option>
												</select>

                                                <div class="date">
                                                    <button class="active">전체</button>
                                                    <button>오늘</button>
                                                    <button>1주일</button>
                                                    <button>1개월</button>
                                                    <button>3개월</button>
                                                    <button>6개월</button>
                                                    <div class="direct">
                                                        <input type="text" id="start-date" /> &nbsp;~&nbsp;
                                                        <input type="text" id="end-date" />
                                                    </div>
                                                    기획서에는 직접입력이 셀렉트박스로 되어 있는데, 사용자가 어떻게 날짜를 입력 해야 하는지 모르겠어서 임의로 변경했습니다.
                                                </div>
                                            </div> -->
                                            <div class="right">
                                                <select class="func_limitChange">
													<option value="5" selected>5건씩 보기</option>
													<option value="10">10건씩 보기</option>
													<option value="20">20건씩 보기</option>
													<option value="50">20건씩 보기</option>
												</select>
                                            </div>
                                        </div>
                                        <!--/ table 버튼 -->

                                        <table class="table list mgt20" id="vehicleList">
                                          
                                        </table>

                                    </div>
                                </li>

                                <!-- 운행결로 및 위험운전 -->
                                <li class="list03">
                                    <a href="javascript:void(0);" data-tab="3">운행경로 및 위험운전</a>
                                    <div class="tab-list">
                                        <!-- 상단 검색 -->
                                        <div class="top-search mgt20">
                                            <input type="text" name="searchDate2" id="searchDate2" />
                                            <button id="drivingLocationSearchBtn" type="button" class="btn btn03">조회</button>
                                        </div>
                                        <!--/ 상단 검색 -->

                                        <div class="mgt30">
                                            <iframe id="mapFrame" src="about:blank" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                        </div>

                                        <div class="violation-list">
                                            <div class="inner">
                                                <div class="list01">급출발<span id="rapidStart"> 0회</span></div>
                                                <div class="list02">급정지<span id="rapidStop"> 0회</span></div>
                                                <div class="list03">급가속<span id="rapidAccel"> 0회</span></div>
                                                <div class="list04">급감속<span id="rapidDeaccel"> 0회</span></div><br />
                                                <div class="list05">급회전<span id="rapidTurn"> 0회</span></div>
                                                <div class="list06">급유턴<span id="rapidUtern"> 0회</span></div>
                                                <div class="list07">과속<span id="overSpeed"> 0회</span></div>
                                                <div class="list08">장기과속<span id="overSpeedLong"> 0회</span></div>
                                            </div>
                                            <p>주행기록중 차량 사용자의 10대 위험운전행동이 특정지점(POI)로 표시되어 있습니다.</p>
                                        </div>


                                      <%--   <h3 class="tit2 mgt40">차량운행내역 조회</h3>
                                        <table class="table list mgt20">
                                            <caption>운행기록 리스트</caption>
                                            <colgroup>
                                                <col style="width:25%" />
                                                <col />
                                                <col style="width:12%" />
                                            </colgroup>
                                            <thead>
                                                <tr>
                                                    <th scope="col">차량 및 사용자</th>
                                                    <th scope="col">운행정보</th>
                                                    <th scope="col">
                                                        <select name="" class="arrow">
																<option value="">경고</option>
																<option value="">지역</option>
																<option value="">시간</option>
															</select>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <!-- tbody가 반복 -->
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="car-img">
                                                            <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" alt="" /></span><br />
                                                            <span class="num">쏘나타 63호 5703</span>
                                                        </div>
                                                    </td>
                                                    <td class="left">
                                                        <div class="driving-info with-btn">
                                                            <ul>
                                                                <li>
                                                                    <strong class="title w40">출발</strong>
                                                                    <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                                </li>
                                                                <li>
                                                                    <strong class="title w40">도착</strong>
                                                                    <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                                </li>
                                                            </ul>
                                                            <button class="btn-img location">위치</button>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        이수지역<br />
                                                        <button class="states-value state04 btn-toggle" onClick="javascript:detail2(1)">상세기록</button>
                                                    </td>
                                                </tr>
                                                <tr id="details2-1" class="detail" style="display:none">
                                                    <td>
                                                        <div class="user-img">
                                                            <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/common/user-default.png"alt="" /></span><br />
                                                            <span class="name">maumgolf / 카카오VX</span>
                                                        </div>
                                                    </td>
                                                    <td colspan="3" class="left">
                                                        <div class="car-info">
                                                            <ul class="grade">
                                                                <li><span>안전점수</span>E(0.0점)</li>
                                                                <li><span>연비점수</span>A(89.6점)</li>
                                                                <li><span>에코점수</span>E(0.0점)</li>
                                                            </ul>

                                                            <ul class="spec">
                                                                <li><span>주행시간</span>0시간 10분 01초</li>
                                                                <li><span>평균속도</span>0 km/h</li>
                                                                <li><span>냉각수</span>0 ℃</li>
                                                                <li><span>주행거리</span>5.4 km</li>
                                                                <li><span>최고속도</span>0 km/h</li>
                                                                <li><span>배터리</span>0.0 v</li>
                                                                <li><span>연비</span>11.4 km/ℓ</li>
                                                                <li><span>연료</span>0.5 ℓ</li>
                                                                <li><span>발전기</span>0.0 v</li>
                                                                <li><span>co2배출량</span>0 ㎎</li>
                                                                <li><span>퓨얼컷</span>0 초</li>
                                                                <li><span>공회전</span>0 초</li>
                                                                <li><span>웜업시간</span>0 초</li>
                                                            </ul>

                                                            <p class="current">현재 주행중입니다.</p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                            <!--/ tbody가 반복 -->

                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="car-img">
                                                            <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" alt="" /></span><br />
                                                            <span class="num">쏘나타 63호 5703</span>
                                                        </div>
                                                    </td>
                                                    <td class="left">
                                                        <div class="driving-info with-btn">
                                                            <ul>
                                                                <li>
                                                                    <strong class="title w40">출발</strong>
                                                                    <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                                </li>
                                                                <li>
                                                                    <strong class="title w40">도착</strong>
                                                                    <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                                </li>
                                                            </ul>
                                                            <button class="btn-img location">위치</button>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        이수지역<br />
                                                        <button class="states-value state04 btn-toggle" onClick="javascript:detail2(2)">상세기록</button>
                                                    </td>
                                                </tr>
                                                <tr id="details2-2" class="detail" style="display:none">
                                                    <td>
                                                        <div class="user-img">
                                                            <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/common/user-default.png"alt="" /></span><br />
                                                            <span class="name">maumgolf / 카카오VX</span>
                                                        </div>
                                                    </td>
                                                    <td colspan="3" class="left">
                                                        <div class="car-info">
                                                            <ul class="grade">
                                                                <li><span>안전점수</span>E(0.0점)</li>
                                                                <li><span>연비점수</span>A(89.6점)</li>
                                                                <li><span>에코점수</span>E(0.0점)</li>
                                                            </ul>

                                                            <ul class="spec">
                                                                <li><span>주행시간</span>0시간 10분 01초</li>
                                                                <li><span>평균속도</span>0 km/h</li>
                                                                <li><span>냉각수</span>0 ℃</li>
                                                                <li><span>주행거리</span>5.4 km</li>
                                                                <li><span>최고속도</span>0 km/h</li>
                                                                <li><span>배터리</span>0.0 v</li>
                                                                <li><span>연비</span>11.4 km/ℓ</li>
                                                                <li><span>연료</span>0.5 ℓ</li>
                                                                <li><span>발전기</span>0.0 v</li>
                                                                <li><span>co2배출량</span>0 ㎎</li>
                                                                <li><span>퓨얼컷</span>0 초</li>
                                                                <li><span>공회전</span>0 초</li>
                                                                <li><span>웜업시간</span>0 초</li>
                                                            </ul>

                                                            <p class="current">현재 주행중입니다.</p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table> --%>

                                        <!-- 페이징 -->
                                        <!-- <div id="paging">
                                            <a href="" class="first icon-spr">제일 처음으로</a>
                                            <a href="" class="prev icon-spr">이전으로</a>
                                            <span class="current">1</span>
                                            <a href="" class="num">2</a>
                                            <a href="" class="num">3</a>
                                            <a href="" class="num">4</a>
                                            <a href="" class="num">5</a>
                                            <a href="" class="next icon-spr">다음으로</a>
                                            <a href="" class="last icon-spr">제일 마지막으로</a>
                                        </div> -->
                                        <!--/ 페이징 -->
                                    </div>
                                </li>
                                <!--/ 운행결로 및 위험운전 -->

                                <!-- 정비이력 및 차량소모품 상태 -->
                                <li class="list04">
                                    <a href="javascript:void(0);" data-tab="4">정비이력 및 차량소모품 상태</a>
                                    <div class="tab-list">
                                        <h3 class="tit2 mgt40">차량 소모품 상태</h3>

                                        <!-- table 버튼 -->
                                        <div class="btn-function title-side">
                                            <div class="right">
                                                <button type="button" class="repair" id="itemDetail">소모품 상세정보</button>
                                            </div>
                                        </div>
                                        <!--/ table 버튼 -->

                                        <div style="padding:50px 0">
                                        	<div id="plot" style="width:100%;height:0px"></div>
                                        </div>

                                        <dl class="states">
                                            <dt>차량 소모품 상태</dt>
                                            <dd>
                                                <span class="state01">녹색</span> 여유
                                                <span class="state02">주황색</span> 교환필요
                                            </dd>
                                        </dl>

                                        <h3 class="tit2 mgt40">과거 차량정비/소모품 교환 이력</h3>

                                        <!-- table 버튼 -->
                                        <div class="btn-function title-side">
                                            <div class="right">
                                                <select name="">
													<option value="">5건씩 보기</option>
												</select>
                                            </div>
                                        </div>
                                        <!--/ table 버튼 -->

                                        <table class="table list mgt20">
                                            <caption>등록된 서비스 리스트</caption>
                                            <thead>
                                                <tr>
                                                    <th scope="col">차량정보</th>
                                                    <th scope="col">정비일자</th>
                                                    <th scope="col">정비항목</th>
                                                    <th scope="col">부품금액</th>
                                                    <th scope="col">공임금액</th>
                                                    <th scope="col">등급</th>
                                                    <th scope="col">정비업소</th>
                                                    <th scope="col">상세</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>티구안 49보4022</td>
                                                    <td>2018/01/16, 01:02</td>
                                                    <td>123213 외 1건</td>
                                                    <td>50,000 원</td>
                                                    <td>25,000 원</td>
                                                    <td>A</td>
                                                    <td>A정비소</td>
                                                    <td><button class="btn-img repair">상세</button></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="8">고객님의 사용 등급의 업그레이드가 필요합니다.</td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <!-- 페이징 -->
                                        <div id="paging">
                                            <a href="" class="first icon-spr">제일 처음으로</a>
                                            <a href="" class="prev icon-spr">이전으로</a>
                                            <span class="current">1</span>
                                            <a href="" class="num">2</a>
                                            <a href="" class="num">3</a>
                                            <a href="" class="num">4</a>
                                            <a href="" class="num">5</a>
                                            <a href="" class="next icon-spr">다음으로</a>
                                            <a href="" class="last icon-spr">제일 마지막으로</a>
                                        </div>
                                        <!--/ 페이징 -->
                                    </div>
                                </li>
                                <!--/ 정비이력 및 차량소모품 상태 -->
                            </ul>
                        </div>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->
