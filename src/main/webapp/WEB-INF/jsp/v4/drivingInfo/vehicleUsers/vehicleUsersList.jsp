<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	

    // 탭메뉴
    $('.tab-menu li a').on('click',function(){
		var tab = $(this).data("tab");
		
		$('.tab-menu li').removeClass("on");
		$('.tab-menu li.'+tab).addClass("on");
		
		if($(this).hasClass("list01")){
			$("#top_searchType").html("");
			$("#top_searchType").append("<option value=''>전체</option>");
			$("#top_searchType").append("<option value='plateNum'>차량번호</option>");
			$("#top_searchType").append("<option value='modelMaster'>차종</option>");
			$("#top_searchType").append("<option value='deviceSn'>시리얼번호</option>	");
		}else{
			$("#top_searchType").html("");
			$("#top_searchType").append("<option value=''>전체</option>");
			$("#top_searchType").append("<option value='name'>사용자명</option>");
			/* $("#top_searchType").append("<option value='accountId'>사용자ID</option>"); */
			$("#top_searchType").append("<option value='corpPosition'>직급</option>");
			$("#top_searchType").append("<option value='gorupNm'>부서명</option>");
		}
		
	});
    
    <c:if test="${V4.corp.corpType eq '4'}">    
    var thead = [
		         {'회사명' : '10%'},
                 {'차량정보' : '20%'},
                 {'소속부서' : '15%'},
                 {'최근 사용자' : '15%'},
                 {'최근 운행정보' : '20%'},
                 {'<select name="" class="arrow func_drivingState"><option value="">운행유무</option><option value="driving">주행중</option><option value="parking">주차중</option><option value="unused">사용중지</option>' : '12%'},
                 {'차량상태' : '8%'}
		         ];
    </c:if>
    <c:if test="${V4.corp.corpType ne '4'}">
    var thead = [
                 {'차량정보' : '20%'},
                 {'소속부서' : '15%'},
                 {'최근 사용자' : '15%'},
                 {'최근 운행정보' : '30%'},
                 {'<select name="" class="arrow func_drivingState"><option value="">운행유무</option><option value="driving">주행중</option><option value="parking">주차중</option><option value="unused">사용중지</option>' : '12%'},
                 {'차량상태' : '8%'}
		         ];
    </c:if>
    var vehicleSetting = {
    		thead : thead
    		,bodyHtml : (function(data){
    			var strHtml = "";
    			for(i in data.result){
    				var obj = data.result[i];
    				strHtml += '<tr>';
    				<c:if test="${V4.corp.corpType eq '4'}">    
    				strHtml += '<td>'+obj.corpName+'</td>';
    				</c:if>
    				if(obj.vehicleType == '1'){
    					strHtml += '<td><div class="car-img vehicleDetail" style="cursor: pointer;" data-vehiclekey='+obj.vehicleKey+'><span class="img"><img src="${pageContext.request.contextPath}/common/images/vehicle/'+obj.vehicleModel.imgS+'.png" alt="" /></span><span class="num">'+obj.vehicleModel.modelMaster+' '+obj.plateNum+'</span></div></td>';	
    				}else if(obj.vehicleType == '2'){
    					strHtml += '<td><div class="car-img vehicleDetail" style="cursor: pointer;" data-vehiclekey='+obj.vehicleKey+'><span class="img"><img src="${pageContext.request.contextPath}/common/images/img_bus_small.png" alt="" /></span><span class="num">'+obj.vehicleModel.modelMaster+' '+obj.plateNum+'</span></div></td>';    					
    				}else if(obj.vehicleType == '3'){
    					strHtml += '<td><div class="car-img vehicleDetail" style="cursor: pointer;" data-vehiclekey='+obj.vehicleKey+'><span class="img"><img src="${pageContext.request.contextPath}/common/images/img_truck_small.png" alt="" /></span><span class="num">'+obj.vehicleModel.modelMaster+' '+obj.plateNum+'</span></div></td>';
    				}else if(obj.vehicleType == '4'){
    					strHtml += '<td><div class="car-img vehicleDetail" style="cursor: pointer;" data-vehiclekey='+obj.vehicleKey+'><span class="img"><img src="${pageContext.request.contextPath}/common/images/img_forklift_small.png" alt="" /></span><span class="num">'+obj.vehicleModel.modelMaster+' '+obj.plateNum+'</span></div></td>';
    				}
    				
    				strHtml += '<td>';    				
    				if(obj.groups){
    					for(i in obj.groups){
    						if(i!=0) strHtml += "</br>";
    						strHtml += (obj.groups[i].parentGroupNm)?(convertNullString(obj.groups[i].parentGroupNm)):"";
							strHtml += (obj.groups[i].groupNm)?("/"+convertNullString(obj.groups[i].groupNm)):"";
    					}
    					
    				}
    				strHtml += '</td>';
    				
    				strHtml += '<td>';    				
    				if(obj.latestStartTrip){
    					if(obj.latestStartTrip.driver){
    						if(obj.latestStartTrip.driver.group){
    							strHtml += (obj.latestStartTrip.driver.group.parentGroupNm)?(convertNullString(obj.latestStartTrip.driver.group.parentGroupNm)):"";
    							strHtml += (obj.latestStartTrip.driver.group.groupNm)?("/"+convertNullString(obj.latestStartTrip.driver.group.groupNm)):"";
    						}
    						strHtml += '/';
    						strHtml += convertNullString(obj.latestStartTrip.driver.corpPosition);
    						strHtml +='<br />';
    						strHtml += convertNullString(obj.latestStartTrip.driver.name);
    					}
    				}
    				strHtml += '</td>';
    				
    				strHtml += '<td>';
    				strHtml += '<div class="driving-info"><ul><li>';
    				strHtml += '<strong class="title">시작/종료</strong>';
    				strHtml += '<span>';
    				if(obj.latestStartTrip) strHtml += convertDateUint(new Date(obj.latestStartTrip.startDate),_unitDate,_timezoneOffset,1);
    				strHtml += ' ~ ';
    				if(obj.latestStartTrip) strHtml += convertDateUint(new Date(obj.latestStartTrip.endDate),_unitDate,_timezoneOffset,1);
    				strHtml += '</span>';
    				strHtml += '</li><li>';
    				strHtml += '<strong class="title">이동거리</strong>';
    				strHtml += '<span>';
    				if(obj.latestStartTrip) strHtml += kmToUnit(obj.latestStartTrip.distance/1000,_unitLength)+' '+_unitLength;
    				else strHtml += '0 '+_unitLength;
    				strHtml += '</span>';
    				
    				if(obj.latestStartTrip&&obj.latestStartTrip.holyday == '1')
    					strHtml += '<strong class="reference">[참고]휴일 운행</strong>';
    				strHtml += '</span>';
    				strHtml += '</li></ul></div></td>';
    				
    				strHtml += '<td>';
    				
    				if(obj.drivingState == 'unused'){
    					strHtml += '<div class="states-value state07">사용중지</div>'; //검정	
    				}else if(obj.drivingState == 'parking'){
    					strHtml += '<div class="states-value state08">주차중</div>'; //주황
    					strHtml += '<div class="states-value state03 vehicleLocationOpen" style="cursor:pointer" data-vehiclekey='+obj.vehicleKey+'>위치확인</div>'; //빨강
    				}else if(obj.drivingState == 'driving'){
    					strHtml += '<div class="states-value state04">운행중</div>'; //파랑
    					strHtml += '<div class="states-value state03 vehicleLocationOpen" style="cursor:pointer" data-vehiclekey='+obj.vehicleKey+'>위치확인</div>'; //빨강
    				}
    				
    				strHtml += '</td>';
    				
    				if(obj.latestDtcCnt > 0) strHtml += '<td class="car-state-error">●</td>';
    				else strHtml += '<td class="car-state-normal">●</td>';
    				
    				strHtml += '</tr>';
    			}
    			return strHtml;
    		})
    		,limit : parseInt($(".list01 .func_limitChange").val())
    		,pagePerGroup : 10 //pagingGroup size
    		,url : "/api/v1/vehicle"
    		,param : {'searchOrder' : $(".list01 .func_order").val()}
    		,http_post_option : {
    			requestMethod : "GET"
    			,header : {key : "${_KEY}"}
    		}
    		,initSearch : true //생성과 동시에 search		
    		,loadingBodyViewer : true //로딩중!! 표시됨
    		,debugMode : true
    	}
    
    <c:if test="${V4.corp.corpType eq '4'}">    
    var thead2 = [
		         {'회사명' : '10%'},
                 {'사용자정보' : '15%'},
                 {'소속' : '10%'},
                 {'최근사용차량' : '17%'},
                 {'최근주행' : '36%'},
                 {'<select name="" class="arrow func_drivingState"><option value="">운행유무</option><option value="driving">주행중</option><option value="parking">주차중</option><option value="unused">사용중지</option>' : '12%'},
		         ];
    </c:if>
    <c:if test="${V4.corp.corpType ne '4'}">
    var thead2 = [
                 {'사용자정보' : '15%'},
                 {'소속' : '10%'},
                 {'최근사용차량' : '17%'},
                 {'최근주행' : '46%'},
                 {'<select name="" class="arrow func_drivingState"><option value="">운행유무</option><option value="driving">주행중</option><option value="parking">주차중</option><option value="unused">사용중지</option>' : '12%'},
		         ];
    </c:if>
    var userSetting = {
    		thead : thead2
    		,bodyHtml : (function(data){
    			var strHtml = "";
    			for(i in data.result){
    				var obj = data.result[i];
    				strHtml += '<tr>';
    				<c:if test="${V4.corp.corpType eq '4'}">    
    				strHtml += '<td>'+obj.corp.corpName+'</td>';
    				</c:if>
    			
    				strHtml += '<td class="userDetail" style="cursor: pointer;" data-accountkey='+obj.accountKey+'><div class="user-img"><span class="img"><img src="${pageContext.request.contextPath}/common/new/img/common/user-default.png"alt="" /></span><br /><span class="name">'+obj.name+'</span></div></td>';
    				
    				strHtml += '<td>';    				
					if(obj.group){
						strHtml += (obj.group.parentGroupNm)?(convertNullString(obj.group.parentGroupNm)):"";
						strHtml += (obj.group.groupNm)?("/"+convertNullString(obj.group.groupNm)):"";
					}
					strHtml +='<br />';
					strHtml += convertNullString(obj.corpPosition);
    				strHtml += '</td>';
    				
    				strHtml += '<td>';
    				
    				if(obj.latestTrip){
    					if(obj.latestTrip.vehicle.vehicleType == '1'){
        					strHtml += '<div class="car-img"><span class="img"><img src="${pageContext.request.contextPath}/common/images/vehicle/'+obj.latestTrip.vehicle.vehicleModel.imgS+'.png" alt="" /></span><span class="num">'+obj.latestTrip.vehicle.vehicleModel.modelMaster+' '+obj.latestTrip.vehicle.plateNum+'</span></div>';	
        				}else if(obj.latestTrip.vehicle.vehicleType == '2'){
        					strHtml += '<div class="car-img"><span class="img"><img src="${pageContext.request.contextPath}/common/images/img_bus_small.png" alt="" /></span><span class="num">'+obj.latestTrip.vehicle.modelMaster+' '+obj.latestTrip.vehicle.plateNum+'</span></div>';    					
        				}else if(obj.latestTrip.vehicle.vehicleType == '3'){
        					strHtml += '<div class="car-img"><span class="img"><img src="${pageContext.request.contextPath}/common/images/img_truck_small.png" alt="" /></span><span class="num">'+obj.latestTrip.vehicle.vehicleModel.modelMaster+' '+obj.latestTrip.vehicle.plateNum+'</span></div>';
        				}else if(obj.latestTrip.vehicle.vehicleType == '4'){
        					strHtml += '<div class="car-img"><span class="img"><img src="${pageContext.request.contextPath}/common/images/img_forklift_small.png" alt="" /></span><span class="num">'+obj.latestTrip.vehicle.vehicleModel.modelMaster+' '+obj.latestTrip.vehicle.plateNum+'</span></div>';
        				}
    				}
    				
    				strHtml += '</td>';
    				
    				strHtml += '<td>';
    				
    				strHtml += '<div class="driving-info"><ul><li>';
    				strHtml += '<strong class="title">시작/종료</strong>';
    				strHtml += '<span>';
    				if(obj.latestTrip) strHtml += convertDateUint(new Date(obj.latestTrip.startDate),_unitDate,_timezoneOffset,1);
    				strHtml += ' ~ ';
    				if(obj.latestTrip) strHtml += convertDateUint(new Date(obj.latestTrip.endDate),_unitDate,_timezoneOffset,1);
    				strHtml += '</span>';
    				strHtml += '</li><li>';
    				strHtml += '<strong class="title">이동거리</strong>';
    				strHtml += '<span>';
    				if(obj.latestTrip) strHtml += kmToUnit(obj.latestTrip.distance/1000,_unitLength)+' '+_unitLength;
    				else strHtml += '0 '+_unitLength;
    				strHtml += '</span>';
    				
    				if(obj.latestTrip&&obj.latestTrip.holyday == '1')
    					strHtml += '<strong class="reference">[참고]휴일 운행</strong>';
    				strHtml += '</span>';
    				strHtml += '</li></ul></div></td>';
    				
    				strHtml += '<td>';
    				if(obj.latestTrip){
    					if(obj.latestTrip.tripState == '3'){
    						strHtml += '<div class="states-value state08">주차중</div>'; //주황
        					strHtml += '<div class="states-value state03">위치확인</div>'; //빨강	
    					}else{
    						if(obj.latestTrip.endDate <= new Date().getTime()-(1000*60*10)){
    							strHtml += '<div class="states-value state08">주차중</div>'; //주황
            					strHtml += '<div class="states-value state03">위치확인</div>'; //빨강
    						}else{
    							strHtml += '<div class="states-value state04">운행중</div>'; //파랑
            					strHtml += '<div class="states-value state03">위치확인</div>'; //빨강	
    						}
    					}
    				}else{
    					strHtml += '<div class="states-value state07">주행없음</div>'; //검정
    				}
    				strHtml += '</td>';
    				
    				strHtml += '</tr>';
    			}
    			return strHtml;
    		})
    		,limit : parseInt($(".list02 .func_limitChange").val())
    		,pagePerGroup : 10 //pagingGroup size
    		,url : "/api/1/account"
    		,param : {'searchOrder' : $(".list02 .func_order").val()}
    		,http_post_option : {
    			requestMethod : "GET"
    			,header : {key : "${_KEY}"}
    		}
    		,initSearch : true //생성과 동시에 search		
    		,loadingBodyViewer : true //로딩중!! 표시됨
    		,debugMode : true
    	}
    
    
    
    $('#vehicleList').commonList(vehicleSetting);
    $('#userList').commonList2(userSetting);
    
	$("#top_btn_search").on("click",$.debounce(200,1,function(){
		
		var param = {searchType:$("#top_searchType").val() , searchText : $("#top_searchText").val()}
		
		var list = $(".tab-menu li.on").hasClass("list01")?"list01":$(".tab-menu li.on").hasClass("list02")?"list02":"";
		
		
		if(list){
			var limit = parseInt($("."+list+" .func_limitChange").val());	 
			param.searchOrder = $("."+list+" .func_order").val();
			
			$("."+list+" .searchStart").val()&&(param.searchStartDate = $("."+list+" .searchStart").val());
			$("."+list+" .searchEnd").val()&&(param.searchEndDate = $("."+list+" .searchEnd").val());
			$("."+list+" .func_drivingState").val()&&(param.searchDrivingState = $("."+list+" .func_drivingState").val());
			
			if(list == "list01"){
				$('#vehicleList').commonList("setParam",param);
				$('#vehicleList').commonList("setLimit",limit).search();
			}else if(list =="list02"){
				$('#userList').commonList2("setParam",param);
				$('#userList').commonList2("setLimit",limit).search();
			}
		}
		
	}));
    
	$(".func_order").on("change",function(){
		$("#top_btn_search").trigger("click");
	});
	
	$(".func_limitChange").on("change",function(){
		$("#top_btn_search").trigger("click");
	});
	
	$(".searchStart,.searchEnd").on("change",function(){
		$("#top_btn_search").trigger("click");
	});
	
	$("div.date button").on("click",function(){
		var list = $(".tab-menu li.on").hasClass("list01")?"list01":$(".tab-menu li.on").hasClass("list02")?"list02":"";
		$(this).closest("div").find("button").removeClass("active");
		$(this).addClass("active");
		var d = $(this).data("day");
		
		if(d == ""){
			$("."+list+" .datePicker").val("");
			$("."+list+" .searchStart,."+list+" .searchEnd").val("");
			
			$("#top_btn_search").trigger("click");
		}else{
			
			var rtvStartDate = calcDate(new Date() , 'd' , 0 , new Date());
			var rtvEndDate = calcDate(new Date() , 'd' , 0 , new Date());
			
			if(d == "-0D"){
			}else if(d == "-7D") rtvStartDate = calcDate(new Date() , 'd' , -7 , new Date());
			else if(d == "-1M") rtvStartDate = calcDate(new Date() , 'm' , -1 , new Date());
			else if(d == "-3M") rtvStartDate = calcDate(new Date() , 'm' , -3 , new Date());
			else if(d == "-6M") rtvStartDate = calcDate(new Date() , 'm' , -6 , new Date());
			
			$("."+list+" .datePicker").eq(0).datepicker('setDate', rtvStartDate);
			$("."+list+" .datePicker").eq(1).datepicker('setDate', rtvEndDate);
			
			var v = $("."+list+" .datePicker").eq(0).datepicker( "getDate" ).getTime();
    		var t = $("."+list+" .datePicker").eq(0).data("target");
    		$(t).val(v);
    		
    		var v = $("."+list+" .datePicker").eq(1).datepicker( "getDate" ).getTime();
    		var t = $("."+list+" .datePicker").eq(1).data("target");
    		$(t).val(v);
    		
    		$(t).trigger("change");
		}
		
	});
	
	$("table").on("change",".func_drivingState",function(){
		$("#top_btn_search").trigger("click");
	});
	
	$("#top_searchText").on("keyup",function(e){
		if(e.keyCode == 13) {
			$("#top_btn_search").trigger("click");
		}
	});
    

	var cntObj = {vehicle : {totalCnt : 0 , drivingCnt : 0 ,parkingCnt : 0 , unusedCnt : 0}
				,user : {totalCnt : 0 , drivingCnt : 0 , parkingCnt :0 , unusedCnt : 0}				
	}
	
	var countingParam = {offset:0,limit:1,counting:true};
	$V4.http_post("/api/v1/vehicle",countingParam,{
		requestMethod:"GET"
		,header:{key:"${_KEY}"}
		,success : function(data){
			$.extend(cntObj.vehicle,data);
			excCounter();
		}
	});
	
	$V4.http_post("/api/v1/account",countingParam,{
		requestMethod:"GET"
		,header:{key:"${_KEY}"}
		,success : function(data){
			$.extend(cntObj.user,data);
			excCounter();
		}
	});
	
	var excCounter = function(){
		$("#vehicleTot").html(cntObj.vehicle.totalCnt);
		$("#vehicleDriving").html(cntObj.vehicle.drivingCnt);
		$("#vehicleParking").html(cntObj.vehicle.parkingCnt);
		$("#vehicleUnused").html(cntObj.vehicle.unusedCnt);
		$("#userTot").html(cntObj.user.totalCnt);
		$("#userDriving").html(cntObj.user.drivingCnt);
		$("#userParking").html(cntObj.user.parkingCnt);
		$("#userUnused").html(cntObj.user.unusedCnt);
	};
	
	$(document).on('click','.vehicleDetail',function(){
		
		var vehicleKey = $(this).data('vehiclekey');
		
		$V4.move("/drivingInfo/vehicleUsersDetail/vehicle",{
			"vehicleKey" : vehicleKey
		});
		
	});
	
	$(document).on('click','.userDetail',function(){
		
		$V4.move("/drivingInfo/vehicleUsersDetail/user");
		
	});
	
	$(document).on('click','.vehicleLocationOpen',function(){
		
		var vehicleKey = $(this).data('vehiclekey'); 
		
		/* http://localhost:8080/map/openMap?searchType=vehicle&vehicleKeys=108d1259-bcc0-11e8-8564-42010a8c0091 */
		
		//sample
		//$('#mapFrame').attr('src','/map/openMap?searchType=vehicle&vehicleKeys=108d1259-bcc0-11e8-8564-42010a8c0091');
		$('#mapFrame').attr('src','/map/openMap?searchType=vehicle&vehicleKeys='+vehicleKey);
		
		$("#vehicleLocationDialog").dialog("open");
	});
	
	$("#vehicleLocationDialog").dialog({
        autoOpen: false,
        show: {
            duration: 500
        },
        width: '960',
        modal: true
    });
	
});

var setAddr = function(addr,addrDetail){
	$('#address').val(convertNullString(addr)+" "+convertNullString(addrDetail));
}
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>차량ㆍ사용자</h2>
                    <span>등록된 차량ㆍ사용자의 상태를 실시간으로 확인할 수 있습니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="driving-page">
                    <div class="vehicle-status">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                            <div class="item">
                                <span>
									등록차량수<strong id="vehicleTot">0</strong>대
								</span>
                                <span>
									운행중<strong id="vehicleDriving">0</strong>대
								</span>
                                <span>
									주차중<strong id="vehicleParking">0</strong>대
								</span>
                                <span>
									사용중지<strong id="vehicleUnused">0</strong>대
								</span>
                                <span class="division">
									등록사용자수<strong id="userTot">0</strong>명
								</span>
                                <span>
									운행중<strong id="userDriving">0</strong>명
								</span>
                                <span>
									미운행중<strong id="userParking">0</strong>명
								</span>
								<span>
									운행없음<strong id="userUnused">0</strong>명
								</span>
                            </div>

                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <!-- 상단 검색 -->
                        <div class="top-search">
                            <select id="top_searchType">
								<option value="">전체</option>
								<option value="plateNum">차량번호</option>
								<option value="modelMaster">차종</option>
								<option value="deviceSn">시리얼번호</option>
							</select>
                            <input type="text" id="top_searchText"/>
                            <button type="button" class="btn btn03" id="top_btn_search">조회</button>
                        </div>
                        <!--/ 상단 검색 -->

                        <!-- 탭메뉴 -->
                        <ul class="page-tab tab-menu col-4">
                            <!-- 차량 현황 -->
                            <li class="list01 on">
                                <a href="#none" data-tab="list01">차량</a>
                                <div class="tab-list">
                                    <!-- table 버튼 -->
                                    <div class="btn-function">
                                        <div class="left">
                                            <select class="func_order">
                                            	<option value="lastestLocationTime">최근 운행순</option>
												<option value="regDate">최근 등록순</option>
											</select>

                                            <div class="date">
                                                <button type="button" class="active" data-day="">전체</button>
                                                <button type="button" data-day="-0D">오늘</button>
                                                <button type="button" data-day="-7D">1주일</button>
                                                <button type="button" data-day="-1M">1개월</button>
                                                <button type="button" data-day="-3M">3개월</button>
                                                <button type="button" data-day="-6M">6개월</button>
                                                <div class="direct">
                                                    <input type="text" class="datePicker" data-target="#list01_searchStart"/> &nbsp;~&nbsp;
                                                    <input type="hidden" id="list01_searchStart" class="searchStart"/>
                                                    <input type="text" class="datePicker" data-target="#list01_searchEnd"/>
                                                    <input type="hidden" id="list01_searchEnd" class="searchEnd"/>
                                                </div>
                                                <!-- 기획서에는 직접입력이 셀렉트박스로 되어 있는데, 사용자가 어떻게 날짜를 입력 해야 하는지 모르겠어서 임의로 변경했습니다.-->
                                            </div>
                                        </div>
                                        <div class="right">
                                            <select class="func_limitChange">
												<option value="5" selected>5건씩 보기</option>
												<option value="10">10건씩 보기</option>
												<option value="20">20건씩 보기</option>
												<option value="50">20건씩 보기</option>
											</select>
                                        </div>
                                    </div>
                                    <!--/ table 버튼 -->

                                    <table class="table list mgt20" id="vehicleList"></table>
                                </div>
                            </li>
                            <!--/ 차량 현황 -->

                            <!-- 사용자 현황 -->
                            <li class="list02" >
                                <a href="#none" data-tab="list02">사용자</a>
                                <div class="tab-list">
                                    <!-- table 버튼 -->
                                    <div class="btn-function">
                                        <div class="left">
                                            <select class="func_order">
                                            	<option value="lastestLocationTime">최근 운행순</option>
												<option value="regDate">최근 등록순</option>
											</select>

                                            <div class="date">
                                                <button type="button" class="active" data-day="">전체</button>
                                                <button type="button" data-day="-0D">오늘</button>
                                                <button type="button" data-day="-7D">1주일</button>
                                                <button type="button" data-day="-1M">1개월</button>
                                                <button type="button" data-day="-3M">3개월</button>
                                                <button type="button" data-day="-6M">6개월</button>
                                                <div class="direct">
                                                    <input type="text" class="datePicker" data-target="#list02_searchStart"/> &nbsp;~&nbsp;
                                                    <input type="hidden" id="list02_searchStart" class="searchStart"/>
                                                    <input type="text" class="datePicker" data-target="#list02_searchEnd"/>
                                                    <input type="hidden" id="list02_searchEnd" class="searchEnd"/>
                                                </div>
                                            </div>
                                            <!-- 직접입력이 셀렉트박스로 되어 있는데, 사용자가 어떻게 날짜를 입력 해야 하는지 모르겠어서 임의로 변경했습니다.-->
                                        </div>
                                        <div class="right">
                                            <select class="func_limitChange">
												<option value="2" selected>2건씩 보기</option>
												<option value="5">5건씩 보기</option>
												<option value="10">10건씩 보기</option>
												<option value="20">20건씩 보기</option>
											</select>
                                        </div>
                                    </div>
                                    <!--/ table 버튼 -->

                                    <table class="table list mgt20" id="userList">
                                    </table>
                                </div>
                            </li>
                            <!--/ 사용자 현황 -->
                        </ul>
                        <!--/ 탭메뉴 -->

                        <div class="box-info">
                            <h3 class="hidden">안내</h3>
                            <h4 class="mgt0">운행 유무 표기 안내</h4>
                            <ul>
                                <li>
                                    <strong>주행중</strong> 차량이 현재 운행중으로 엔진종료 후 시작/종료일시, 이동거리가 시스템에 기록됩니다.
                                </li>
                                <li>
                                    <strong>운행대기</strong> 배차 등록/ 승인된 사용자가 있으나 아직 운행을 시작하지 않은 차량입니다.
                                </li>
                                <li>
                                    <strong>주차중</strong> 배차 등록된 사용자가 현재 차량을 주차중입니다.
                                </li>
                                <li>
                                    <strong>미배차</strong> 배차 등록된 사용자가 없는 차량입니다.
                                </li>
                                <li>
                                    <strong>사용중지</strong> 사용하지 않는 차량입니다
                                </li>
                            </ul>

                            <h4>차량 상태 안내</h4>
                            <ul>
                                <li>
                                    <span class="state normal">녹색</span> 차량상태가 정상입니다.
                                </li>
                                <li>
                                    <span class="state error">적색</span> 차량에 이상이 있습니다. 마우스를 롤오버하면 상세내용이 보입니다.
                                </li>
                            </ul>

                            <h4>주차위치 안내</h4>
                            <p>
                                위치확인을 클릭하시면사용자 휴대폰 App을 통해 최종 등록된 주차위치가 아래 지도에 표시됩니다.
                                <br /> (사용자의 GPS 활용 미동의, GPF 오동작, 지하주차장 이동 등 GPS 수신상태로 고르지 못할 경우 위치조회가 정확하지 않을 수 있습니다.)
                            </p>
                        </div>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->
        
<!-- 차량 위치 팝업 -->
<div id="vehicleLocationDialog" title="위치 확인">
    <div class="map clr">
        <div class="left">
        <iframe id="mapFrame" src="about:blank" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>
    <div class="mgt20">
    	<span class="tit" style="color: #333; font-weight: 400; line-height: 40px;">주소</span>
	    <input type="text" id="address" value="" readonly="readonly" style="margin-left: 15px;width:95%;height:40px;background-color:#f8f8f8;padding-left: 10px; border: 1px solid #e3e3e3; line-height: 30px; border-radius: 0; -webkit-appearance: none; -webkit-appearance: none;">
    </div>
    <div class="mgt20">
    	※ 사용자의 GPS 활용 미동의, GPS 오동작, 지하주차장 등과 GPS 수신상태로 고르지 못할 경우 위치가 정확하지 않을 수 있습니다.
    	</br>
		위치가 정확하지 않은 경우 정확한 위치를 직접 지도에서 찾아 표시하거나 입력해주세요.
    </div>
</div>
<!-- 차량 위치 팝업 -->    