<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	var thead2 = [
	              {'내 노선' : '8%'},
	              {'운행목적' : '13%'},
	              {'노선명' : '13%'},
	              {'상세노선' : '53%'},
	              {'운행여부' : '13%'},
	              ];
	var busRouteFavotiresListSetting = {
			thead : thead2
     		,bodyHtml : (function(data){
     			var strHtml = "";
     			if(data.result.length > 0){
     				$('#favoritesEmpty').hide();
     				$('#busRouteFavoritesList').show();
     				$('.func_limitChange').show();
     			}
     			for(i in data.result){
     				var obj = data.result[i];
     				strHtml += '<tr>';
     				strHtml += '    <td>';
    				strHtml += '        <input type="checkbox" id="star_'+i+'" class="star myck" value='+obj.busRouteKey+' checked />';	
     				strHtml += '        <label for="star_'+i+'" class="star"></label>';
     				strHtml += '    </td>';
     				strHtml += '    <td>'+obj.drivingPurposeName+'</td>';
     				strHtml += '    <td>'+obj.busRouteName+'</td>';
     				strHtml += '    <td class="left"><a class="busRouteStatilDetail" data-busroutekey='+obj.busRouteKey+'>'+obj.stationNameGroup+'</a></td>';
     				strHtml += '    <td>';
     				if(obj.busMapping.length > 0){
     					strHtml += '        <div class="states-value state02">운행중</div><br />';
     				}else{
     					strHtml += '        <div class="states-value state07">미운영</div><br />';	
     				}
     				strHtml += '        <button type="button" class="states-value state04 btnBusGps" data-busroutekey='+obj.busRouteKey+'>위치확인</button>';
     				strHtml += '    </td>';
     				strHtml += '</tr>';
     			}
     			return strHtml;
     		})
     		,limit : parseInt($(".func_limitChange").val())
     		,pagePerGroup : 10 //pagingGroup size
     		,url : "/api/1/busRouteStation"
     		,param : {
     			"searchOrder" : "purpose",
     			"favorites" : "favorites"
     		}
     		,http_post_option : {
     			requestMethod : "GET"
     			,header : {key : "${_KEY}"}
     		}
     		,initSearch : true //생성과 동시에 search		
     		,loadingBodyViewer : true //로딩중!! 표시됨
     		,debugMode : true
	}
	//즐겨찾기로 등록해둔
	$('#busRouteFavoritesList').commonList(busRouteFavotiresListSetting);
	
	var busRouteSearchListSetting = {
			thead : thead2
     		,bodyHtml : (function(data){
     			var strHtml = "";
     			for(i in data.result){
     				var obj = data.result[i];
     				strHtml += '<tr>';
     				strHtml += '    <td>';
     				if(obj.favoritesCnt > 0){
     					strHtml += '        <input type="checkbox" id="star_'+i+'" class="star myck" value='+obj.busRouteKey+' checked />';	
     				}else{
     					strHtml += '        <input type="checkbox" id="star_'+i+'" class="star myck" value='+obj.busRouteKey+' />';
     				}
     				
     				strHtml += '        <label for="star_'+i+'" class="star"></label>';
     				strHtml += '    </td>';
     				strHtml += '    <td>'+obj.drivingPurposeName+'</td>';
     				strHtml += '    <td>'+obj.busRouteName+'</td>';
     				strHtml += '    <td class="left"><a class="busRouteStatilDetail" data-busroutekey='+obj.busRouteKey+'>'+obj.stationNameGroup+'</a></td>';
     				strHtml += '    <td>';
     				if(obj.busMapping.length > 0){
     					strHtml += '        <div class="states-value state02">운행중</div><br />';
     				}else{
     					strHtml += '        <div class="states-value state07">미운영</div><br />';	
     				}
     				strHtml += '        <button type="button" class="states-value state04 btnBusGps" data-busroutekey='+obj.busRouteKey+'>위치확인</button>';
     				strHtml += '    </td>';
     				strHtml += '</tr>';
     			}
     			return strHtml;
     		})
     		,limit : parseInt($(".func_limitChange").val())
     		,pagePerGroup : 10 //pagingGroup size
     		,url : "/api/1/busRouteStation"
     		,param : {
     			"searchOrder" : "purpose"
     		}
     		,http_post_option : {
     			requestMethod : "GET"
     			,header : {key : "${_KEY}"}
     		}
     		,initSearch : false //생성과 동시에 search		
     		,loadingBodyViewer : true //로딩중!! 표시됨
     		,debugMode : true
	}
	
	$('#busRouteSearchList').commonList2(busRouteSearchListSetting);
	
	//상단 검색
	$("#top_btn_search").on("click",$.debounce(200,1,function(){
		$('#favoritesEmpty').hide();
		$('#busRouteFavoritesList').hide();
		$('#busRouteSearchList').show();
		$('.func_limitChange').show();
 		var param = {searchType:$("#top_searchType").val() , searchText : $("#top_searchText").val()}
		var limit = parseInt($(".func_limitChange").val());	 
 		$('#busRouteSearchList').commonList2("setParam",param);
		$('#busRouteSearchList').commonList2("setLimit",limit).search();
		
		
 	}));
	
	//상단 키워드 검색	
	$('#top_btn_searchKeyWord').on("click",$.debounce(200,1,function(){
		$('#favoritesEmpty').hide();
		$('#busRouteFavoritesList').hide();
		$('#busRouteSearchList').show();
		$('.func_limitChange').show();
		var param = {searchKeyword : $('#top_searchKeyword')}
		var limit = parseInt($(".func_limitChange").val());	 
 		
 		$('#busRouteSearchList').commonList2("setParam",param);
		$('#busRouteSearchList').commonList2("setLimit",limit).search();
 	}));
	
	$(".func_limitChange").on("change",function(){
		var limit = parseInt($(".func_limitChange").val());	 
		if($('#busRouteSearchList').css('display') != "none"){
			if($('#top_searchKeyword').val() == ""){
				$("#top_btn_search").trigger('click');
			}else{
				$('#top_btn_searchKeyWord').trigger('click');
			}
		}else{
			$('#busRouteFavoritesList').commonList("setLimit",limit).search();
		}
 	});
	
	//별 등록 구현
	//별 삭제 구현 
	//노선정보 들어올때 즐겨찾기 리스트 먼저 뿌려줘야 한다.
	//위치 확인 기능 구현..
	$('body').on('click','.myck',function(){
		
		var busRouteKey = $(this).val();
		//체크를 누른거다
		if($(this).prop('checked')){
			$V4.http_post("/api/1/busFavorites", {"busRouteKey" : busRouteKey}, {
				requestMethod : "POST",
				header : {
					"key" : "${_KEY}"
				},
				success : function(rtv) {
					//
				},
				error : function(t) {
					
				}
			});
		}else{
			$V4.http_post("/api/1/busFavorites/"+busRouteKey, {}, {
				requestMethod : "DELETE",
				header : {
					"key" : "${_KEY}"
				},
				success : function(rtv) {
					//
				},
				error : function(t) {
					
				}
			});
		}
	});
	
	$(document).on('click','.busRouteStatilDetail',function(){
		var busRouteKey = $(this).data('busroutekey');
		
		$V4.move('/drivingInfo/busRouteInfo/detail',{"busRouteKey" : busRouteKey });
	});
	
	$("#mapFrame").attr("src","/map/tripBus.do?busRouteKey=");
	
	$('body').on('click','.btnBusGps',function(){
		var t = 3600*(24*4);
		var busRouteKey = $(this).data('busroutekey');
		$("#mapFrame").get(0).contentWindow.busRouteKey = busRouteKey;
		$("#mapFrame").get(0).contentWindow._o = $("#mapFrame").get(0).contentWindow.createOption();
		$("#mapFrame").get(0).contentWindow.outerReset();
		var load = $("#mapFrame").get(0).contentWindow.getLoadComplate();
		if(load){
			$("#vehicleLocationDialog").dialog("open");
		}
	});
	
	$("#vehicleLocationDialog").dialog({
        autoOpen: false,
        show: {
            duration: 500
        },
        width: '960',
        modal: true
    });
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>버스(셔틀)노선정보</h2>
                    <span>통학, 통근버스 및 학원차량의 노선정보를 확인하실 수 있습니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="driving-page">
                    <div class="bus-route">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                            <div class="item">
                                <span>
									등록 노선 수
									<strong>10</strong>건
								</span>
                            </div>

                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <!-- 상단 검색 -->
                        <div class="top-search mgt20">
                            <div>
                                <select id="top_searchType">
									<option value="">선택</option>
									<option value="route">노선명</option>
									<option value="station">정류장명</option>
								</select>
                                <!-- <span class="search-tit">정류장명</span> -->
                                <input type="text" id="top_searchText" />
	                            <button type="button" class="btn btn03" id="top_btn_search">조회</button>
                            </div>
                            <div class="mgt10">
                                <span class="search-tit">빠른노선찾기</span>
                                <input type="text" name="" style="min-width:540px" id="top_searchKeyword" />
                                <button type="button" class="btn btn03" id="top_btn_searchKeyWord">조회</button>
                            </div>
                        </div>
                        <!--/ 상단 검색 -->

                        <!-- 검색 전 -->
                        <div class="notice icon-spr" id="favoritesEmpty">
                            버스 목록 좌측의 <span>☆</span>를 클릭하여<br />내 노선을 등록해보세요.
                        </div>
                        <!--/ 검색 전 -->

                        <!-- 검색 후 -->
                        <!-- table 버튼 -->
                        <div class="btn-function">
                            <div class="right">
                                <select class="func_limitChange" style="display:none">
									<option value="5" selected>5건씩 보기</option>
									<option value="10">10건씩 보기</option>
									<option value="20">20건씩 보기</option>
									<option value="50">20건씩 보기</option>
								</select>
                            </div>
                        </div>
                        <!--/ table 버튼 -->
						<table class="table list mgt20" id="busRouteFavoritesList" style="display:none;">
                            
                        </table>
                        <table class="table list mgt20" id="busRouteSearchList" style="display:none;">
                            
                        </table>
                        <!--/ 검색 후 -->
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->
        
        
        <!-- 차량 위치 팝업 -->
<div id="vehicleLocationDialog" title="버스 위치 확인">
    <div class="map clr">
        <div class="left">
        <iframe id="mapFrame" src="about:blank" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>
    <!-- <div class="mgt20">
    	<span class="tit" style="color: #333; font-weight: 400; line-height: 40px;">주소</span>
	    <input type="text" id="address" value="" readonly="readonly" style="margin-left: 15px;width:95%;height:40px;background-color:#f8f8f8;padding-left: 10px; border: 1px solid #e3e3e3; line-height: 30px; border-radius: 0; -webkit-appearance: none; -webkit-appearance: none;">
    </div> -->
    <div class="mgt20">
    	※ 사용자의 GPS 활용 미동의, GPS 오동작, 지하주차장 등과 GPS 수신상태로 고르지 못할 경우 위치가 정확하지 않을 수 있습니다.
    	</br>
		위치가 정확하지 않은 경우 정확한 위치를 직접 지도에서 찾아 표시하거나 입력해주세요.
    </div>
</div>
<!-- 차량 위치 팝업 -->   