<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
var _busRouteKey = "${busRouteKey}";
$(function(){
	$("#bus-station-dialog").dialog({
        autoOpen: false,
        show: {
            duration: 500
        },
        width: '543',
        modal: true
    });
    
	$(document).on('click','.camera',function(){
		
		var busStationImgKey = $(this).data('imgkey');
		
		var busStationImg = '${defaultPath}/api/1/'+busStationImgKey+'/${_KEY}/imgDown';
		$('#busStationImg').attr("src",busStationImg);
		
		$("#bus-station-dialog").dialog("open");
		
	});
    
    $V4.http_post("/api/1/busRouteStation", {"busRouteKey" : _busRouteKey}, {
		requestMethod : "GET",
		header : {
			"key" : "${_KEY}"
		},
		success : function(rtv) {
			console.log(rtv);
			
			var obj = rtv.result[0];
			$('#drivingPurposeName').text(obj.drivingPurposeName);
			$('#busRouteName').text(obj.busRouteName);
			$('#busAllRouteImg').html('<img src="${defaultPath}/api/1/'+obj.busRouteImg+'/${_KEY}/imgDown" alt="" />')
			var busStation = obj.busStation;
			var strHtml ="";
			for(var i = 0 ; i < busStation.length ; i++){
				var station = busStation[i];
				//처음
				if(i == 0){
					strHtml += '<li class="start">';
					strHtml += station.stationName;
					strHtml += '    <span class="time">'+station.arrivalTime+'</span>';
					
					strHtml += '    <button type="button" class="btn-img camera" data-imgkey='+getProperty(station,'stationImg')+'>상세보기</button>';
					strHtml += '</li>';
				//마지막
				}else if( i == busStation.length - 1 ){
					strHtml += '<li class="end">';
					strHtml += station.stationName;
					strHtml += '    <span class="time">'+station.arrivalTime+'</span>';
					strHtml += '    <button type="button" class="btn-img camera" data-imgkey='+getProperty(station,'stationImg')+'>상세보기</button>';
					strHtml += '</li>';
				}else{
				//중간
					strHtml += '<li class="arrow">';
					strHtml += station.stationName;
					strHtml += '    <span class="time">'+station.arrivalTime+'</span>';
					strHtml += '    <button type="button" class="btn-img camera" data-imgkey='+getProperty(station,'stationImg')+'>상세보기</button>';
					strHtml += '</li>';
				}
			}
			$('#busStationList').html(strHtml);
		},
		error : function(t) {
			
		}
	});
    
    $('#confirm').on('click',function(){
    	window.history.back(-1)
    });
    
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>버스(셔틀)노선정보</h2>
                    <span>통학, 통근버스 및 학원차량의 노선정보를 확인하실 수 있습니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="driving-page">
                    <div class="box-layout bus-route-detail">
                        <div class="title">
                            <h3 class="tit3" id="drivingPurposeName"></h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
                            <div class="info-txt" id="busRouteName">석림한성@(서산의료원) ↔ 한화토탈 본사</div>

                            <div class="mgt30" id="busAllRouteImg" style="text-align: center;">
                                
                            </div>

                            <div class="route">
                                <ol id="busStationList">
                                    <!-- <li class="start">
                                        석림한성@(서산의료원)
                                        <span class="time">06:08</span>
                                        <button class="btn-img camera" id="bus-station-open">상세보기</button>
                                    </li>
                                    <li class="arrow">
                                        양대4
                                        <span class="time">06:09</span>
                                        <button class="btn-img camera">상세보기</button>
                                    </li>
                                    <li class="dot">
                                        학돌초
                                        <span class="time">06:10</span>
                                        <button class="btn-img camera">상세보기</button>
                                    </li>
                                    <li class="dot">
                                        학돌초
                                        <span class="time">06:12</span>
                                        <button class="btn-img camera">상세보기</button>
                                    </li>
                                    <li class="arrow">
                                        롯데캐슬@
                                        <span class="time">06:20</span>
                                        <button class="btn-img camera">상세보기</button>
                                    </li>
                                    <li class="arrow">
                                        성연3(무정차)
                                        <span class="time">06:25</span>
                                        <button class="btn-img camera">상세보기</button>
                                    </li>
                                    <li class="arrow">
                                        지곡육교(무정차)
                                        <span class="time">06:40</span>
                                        <button class="btn-img camera">상세보기</button>
                                    </li>
                                    <li class="end">
                                        본사
                                        <span class="time">06:50</span>
                                        <button class="btn-img camera">상세보기</button>
                                    </li> -->
                                </ol>
                            </div>
                             <div class="btn-bottom">
						            <button type="button" class="btn btn03" id="confirm">확인</button>
						        </div>
                        </div>
                        
                    </div>
                    <!--/ 콘텐츠 본문 -->
                </div>
            </div>
        </section>
        <!--/ 본문 -->

        <!-- 정류장사진 팝업 -->
    <div id="bus-station-dialog" title="상세 정류장 사진">
        <img id="busStationImg" src="" alt="" style="width: 480px;height: 350px;"/>
    </div>
    <!--/ 정류장사진 팝업 -->