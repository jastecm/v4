<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script>
$(document).ready(function(){
	$('#next').on('click',function(){
		$V4.move("/regist/corp/step3",$('#frm'));
	});
});
</script>
<form id="frm" name="frm">
	<!-- 여기까진 공통 -->
	<input type="hidden" name="accountId" value="${rtv.accountId}"/>
	<input type="hidden" name="accountPw" value="${rtv.accountPw}"/>
	<input type="hidden" name="corpType" value="${rtv.corpType}"/>
	
	<input type="hidden" name="serviceAccepted" value="${rtv.serviceAccepted}"/>
	<input type="hidden" name="privateAccepted" value="${rtv.privateAccepted}"/>
	<input type="hidden" name="locationAccepted" value="${rtv.locationAccepted}"/>
	<input type="hidden" name="marketingAccepted" value="${rtv.marketingAccepted}"/>
	
	<input type="hidden" name="ctyCode" value="${rtv.ctyCode}"/>
	<input type="hidden" name="timezoneOffset" value="${rtv.timezoneOffset}"/>
	
	<input type="hidden" name="unitLength" value="${rtv.unitLength}"/>
	<input type="hidden" name="unitVolume" value="${rtv.unitVolume}"/>
	<input type="hidden" name="unitTemperature" value="${rtv.unitTemperature}"/>
	<input type="hidden" name="unitCurrency" value="${rtv.unitCurrency}"/>
	<input type="hidden" name="unitDate" value="${rtv.unitDate}"/>
	<!-- 여기까진 공통 -->
	
	<!-- 기업 필수 -->
		<!-- 기업명 -->
	<input type="hidden" name="corpName" value="${rtv.corpName}"/>
		<!-- 사업자등록번호 -->
	<input type="hidden" name="corpNum" value="${rtv.corpNum}"/>
		<!-- 대표자명 -->
	<input type="hidden" name="ceoName" value="${rtv.ceoName}"/>

		<!-- 대표자 연락처 -->	
	<input type="hidden" name="corpPhoneCtyCode" value="${rtv.corpPhoneCtyCode}"/>
	<input type="hidden" name="corpPhone" value="${rtv.corpPhone}"/>
	
		<!-- 관리자 정보 -->
	<input type="hidden" name="name" value="${rtv.name}"/>
	<input type="hidden" name="mobilePhoneCtyCode" value="${rtv.mobilePhoneCtyCode}"/>
	<input type="hidden" name="mobilePhone" value="${rtv.mobilePhone}"/>
	<input type="hidden" name="birth" value="${rtv.birth}"/>
	<input type="hidden" name="blood" value="${rtv.blood}"/>
	<input type="hidden" name="bloodRh" value="${rtv.bloodRh}"/>
	<input type="hidden" name="gender" value="${rtv.gender}"/>
		<!-- 대표자이메일 -->
		<input type="hidden" name="ceoId" value="${rtv.ceoId}"/>
		<input type="hidden" name="ceoPw" value="${rtv.ceoPw}"/>
		
	
	<!-- 기업 필수 -->
	
	
	<!-- 기업 선택 -->
		<!-- 업태 -->
	<input type="hidden" name="corpBusieness" value="${rtv.corpBusieness}"/>
		<!-- 종목 -->
	<input type="hidden" name="corpItem" value="${rtv.corpItem}"/>
		<!-- 일반연락처 -->
	<input type="hidden" name="phoneCtyCode" value="${rtv.phoneCtyCode}"/>
	<input type="hidden" name="phone" value="${rtv.phone}"/>
		<!-- 직급 -->	
	<input type="hidden" name="corpPosition" value="${rtv.corpPosition}"/>
	<!-- 기업 선택 -->
	
</form>

 <!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>회원가입</h2>
                    <span></span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="join-page">
                    <!-- 회원가입 STEP -->
                    <div class="join-step">
                        <ol>
                            <li class="step01">
                                <span>STEP01</span>
                                <strong>가입선택</strong>
                            </li>
                            <li class="step02">
                                <span>STEP02</span>
                                <strong>약관동의</strong>
                            </li>
                            <li class="step03">
                                <span>STEP03</span>
                                <strong>지역선택</strong>
                            </li>
                            <li class="step04">
                                <span>STEP04</span>
                                <strong>회원정보입력</strong>
                            </li>
                            <li class="step05 active">
                                <span>STEP05</span>
                                <strong>회원정보확인</strong>
                            </li>
                            <li class="step06">
                                <span>STEP06</span>
                                <strong>등급선택</strong>
                            </li>
                            <li class="step07">
                                <span>STEP07</span>
                                <strong>가입완료</strong>
                            </li>
                        </ol>
                    </div>
                    <!--/ 회원가입 STEP -->

                    <h3 class="tit">회원정보확인</h3>
                    <p class="txt01">
                        <span class="red">스마트 드라이빙!</span> ViewCAR 차량 관리 서비스의 정상적인 이용을 위해 아래 정보의 입력이 필요합니다.
                        <br />개인회원의 회원 가입은 WEB과 Mobile App 을 통해 모두 가입하실 수 있습니다.</p>

                        <!-- 기본정보 -->
                        <h4 class="tit mgt40">기본정보</h4>
                        <table class="table mgt20">
                            <caption>기본정보</caption>
                            <tbody>
                                <tr>
                                    <th scope="row">아이디</th>
                                    <td>
                                        ${rtv.accountId}
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">기업 정보
                                        <span class="necessary">(필수입력)</span>
                                    </th>
                                    <td class="table">
                                        <table>
                                            <caption>기업 정보</caption>
                                            <colgroup>
                                                <col style="width:16.35%" />
                                                <col style="width:34.65%" />
                                                <col style="width:16.35%" />
                                                <col style="width:34.65%" />
                                            </colgroup>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">기업명</th>
                                                    <td>
                                                        ${rtv.corpName}
                                                    </td>
                                                    <th scope="row">사업자등록번호</th>
                                                    <td>
                                                        ${rtv.corpNum}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">대표자명</th>
                                                    <td>
                                                        ${rtv.ceoName}
                                                    </td>
                                                    <th scope="row">대표자 연락처</th>
                                                    <td>
                                                        ${rtv.corpPhoneCtyCode} ${rtv.corpPhone}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">대표자 이메일</th>
                                                    <td colspan="3">
                                                        ${rtv.ceoId}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">부가 정보
                                        <span>(선택입력)</span>
                                    </th>
                                    <td class="table">
                                        <table>
                                            <caption>기업 정보</caption>
                                            <colgroup>
                                                <col style="width:16.35%" />
                                                <col style="width:34.65%" />
                                                <col style="width:16.35%" />
                                                <col style="width:34.65%" />
                                            </colgroup>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">업태</th>
                                                    <td>
                                                        ${rtv.corpBusieness }
                                                    </td>
                                                    <th scope="row">종목</th>
                                                    <td>
                                                        ${rtv.corpItem }
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!--/ 기본정보 -->

                        <!-- 차량관리자 정보 -->
                        <h4 class="tit mgt40">차량관리자 정보</h4>
                        <table class="table mgt20">
                            <caption>차량관리자 정보</caption>
                            <tbody>
                                <tr>
                                    <th scope="row">기본정보
                                        <span class="necessary">(필수입력)</span>
                                    </th>
                                    <td class="table">
                                        <table>
                                            <caption>차량관리자 기본정보</caption>
                                            <colgroup>
                                                <col style="width:16.35%" />
                                                <col />
                                            </colgroup>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">관리자명</th>
                                                    <td>
                                                        ${rtv.name }
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">휴대폰 번호</th>
                                                    <td>
                                                       ${rtv.mobilePhoneCtyCode } ${rtv.mobilePhone }
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">부가정보
                                        <span>(선택입력)</span>
                                    </th>
                                    <td class="table">
                                        <table>
                                            <caption>차량관리자 부가정보</caption>
                                            <colgroup>
                                                <col style="width:16.35%" />
                                                <col style="width:34.65%" />
                                                <col style="width:16.35%" />
                                                <col style="width:34.65%" />
                                            </colgroup>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">일반 연락처</th>
                                                    <td>
                                                        ${rtv.phoneCtyCode } ${rtv.phone }
                                                    </td>
                                                    <th scope="row">직급</th>
                                                    <td>
                                                        ${rtv.corpPosition }
                                                    </td>
                                                </tr>
                                                <tr>
				                                    <th scope="row">생년월일</th>
				                                    <td>
					                                    <c:if test="${not empty rtv.birth}">
															<jsp:useBean id="dateValue" class="java.util.Date"/>
															<jsp:setProperty name="dateValue" property="time" value="${rtv.birth}"/>
															<c:choose>
																<c:when test="${rtv.unitDate eq 'YMD'}">
																	<fmt:formatDate value="${dateValue}" pattern="yyyy/MM/dd"/>	
																</c:when>									
																<c:when test="${rtv.unitDate eq 'MDY'}">
																	<fmt:formatDate value="${dateValue}" pattern="MM/dd/yyyy"/>	
																</c:when>
																<c:when test="${rtv.unitDate eq 'DMY'}">
																	<fmt:formatDate value="${dateValue}" pattern="dd/MM/yyyy"/>	
																</c:when>
																<c:otherwise>
																	<fmt:formatDate value="${dateValue}" pattern="yyyy/MM/dd"/>
																</c:otherwise>
															</c:choose>
														</c:if>
				                                    </td>
				                                    <th scope="row">성별</th>
				                                    <td>
				                                    	<c:if test="${rtv.gender eq 'M'}">
				                                    	남자
				                                    	</c:if>
				                                    	<c:if test="${rtv.gender eq 'F'}">
				                                    	여자
				                                    	</c:if>
				                                    </td>
				                                </tr>
				                                <tr>
				                                    <th scope="row">혈액형</th>
				                                    <td colspan="3">
				                                        ${rtv.blood}형 ${rtv.bloodRh}
				                                    </td>
				                                </tr>
				                               <tr>
				                                    <th scope="row">기준통화</th>
				                                    <td>
				                                    	<c:if test="${rtv.unitCurrency eq 'WON' }">원</c:if>
				                                    	<c:if test="${rtv.unitCurrency eq 'USD' }">달러</c:if>
				                                    </td>
				                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!--/ 차량관리자 정보 -->

                        <!-- 하단 버튼 -->
                        <div class="btn-bottom ">
                            <button class="btn btn02" id="prev">이전</button>
                            <button class="btn btn03" id="next">계속</button>
                        </div>
                        <!--/ 하단 버튼 -->
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->