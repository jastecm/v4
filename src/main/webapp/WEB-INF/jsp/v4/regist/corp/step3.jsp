<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script>
$(document).ready(function(){
	$('#next').on('click',function(){
		if($('#nonSelect').is(':checked')){
			$('#frm input[name="serviceGrade"]').val("0");
		}else{
			//체크 되어 있지 않으면
			var serviceGrade = $('input[name="grade"]:checked').val();
			$('#frm input[name="serviceGrade"]').val(serviceGrade);
		}
		
		var corpType = "${rtv.corpType}";
		var url = "/api/v1/account/";
		
		if(corpType == "2"){
			url += "corp";
		}else if(corpType == "3"){
			url += "rent";
		}else{
			url = "";
		}
		
		url && $V4.http_post("/api/1/account/corp",$('#frm').serializeObject(),{
	    	success : function(rtv){
	    		$V4.move("/regist/step7",$('#frm'));
	    	}
	    });
	});
});
</script>
<form id="frm" name="frm" action="${defaultPath}/regist/step7" method="post">
	<!-- 여기까진 공통 -->
	<input type="hidden" name="accountId" value="${rtv.accountId}"/>
	<input type="hidden" name="accountPw" value="${rtv.accountPw}"/>
	<input type="hidden" name="corpType" value="${rtv.corpType}"/>
	
	<input type="hidden" name="serviceAccepted" value="${rtv.serviceAccepted}"/>
	<input type="hidden" name="privateAccepted" value="${rtv.privateAccepted}"/>
	<input type="hidden" name="locationAccepted" value="${rtv.locationAccepted}"/>
	<input type="hidden" name="marketingAccepted" value="${rtv.marketingAccepted}"/>
	
	<input type="hidden" name="ctyCode" value="${rtv.ctyCode}"/>
	<input type="hidden" name="timezoneOffset" value="${rtv.timezoneOffset}"/>
	<input type="hidden" name="unitLength" value="${rtv.unitLength}"/>
	<input type="hidden" name="unitVolume" value="${rtv.unitVolume}"/>
	<input type="hidden" name="unitTemperature" value="${rtv.unitTemperature}"/>
	<input type="hidden" name="unitCurrency" value="${rtv.unitCurrency}"/>
	<input type="hidden" name="unitDate" value="${rtv.unitDate}"/>
	
	<!-- 여기까진 공통 -->
	
	<!-- 기업 필수 -->
		<!-- 기업명 -->
	<input type="hidden" name="corpName" value="${rtv.corpName}"/>
		<!-- 사업자등록번호 -->
	<input type="hidden" name="corpNum" value="${rtv.corpNum}"/>
		<!-- 대표자명 -->
	<input type="hidden" name="ceoName" value="${rtv.ceoName}"/>

		<!-- 대표자 연락처 -->	
	<input type="hidden" name="corpPhoneCtyCode" value="${rtv.corpPhoneCtyCode}"/>
	<input type="hidden" name="corpPhone" value="${rtv.corpPhone}"/>
	
		<!-- 관리자 정보 -->
	<input type="hidden" name="name" value="${rtv.name}"/>
	<input type="hidden" name="mobilePhoneCtyCode" value="${rtv.mobilePhoneCtyCode}"/>
	<input type="hidden" name="mobilePhone" value="${rtv.mobilePhone}"/>
	<input type="hidden" name="birth" value="${rtv.birth}"/>
	<input type="hidden" name="blood" value="${rtv.blood}"/>
	<input type="hidden" name="bloodRh" value="${rtv.bloodRh}"/>
	<input type="hidden" name="gender" value="${rtv.gender}"/>
		<!-- 대표자이메일 -->
		<input type="hidden" name="ceoId" value="${rtv.ceoId}"/>
		<input type="hidden" name="ceoPw" value="${rtv.ceoPw}"/>
		
	
	<!-- 기업 필수 -->
	
	
	<!-- 기업 선택 -->
		<!-- 업태 -->
	<input type="hidden" name="corpBusieness" value="${rtv.corpBusieness}"/>
		<!-- 종목 -->
	<input type="hidden" name="corpItem" value="${rtv.corpItem}"/>
		<!-- 일반연락처 -->
	<input type="hidden" name="phoneCtyCode" value="${rtv.phoneCtyCode}"/>
	<input type="hidden" name="phone" value="${rtv.phone}"/>
		<!-- 직급 -->	
	<input type="hidden" name="corpPosition" value="${rtv.corpPosition}"/>
	<!-- 기업 선택 -->
	
	<input type="hidden" name="serviceGrade" value=""/>
</form>
 <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>회원가입</h2>
                    <span></span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="join-page">
                   <!-- 회원가입 STEP -->
                    <div class="join-step">
                        <ol>
                            <li class="step01">
                                <span>STEP01</span>
                                <strong>가입선택</strong>
                            </li>
                            <li class="step02">
                                <span>STEP02</span>
                                <strong>약관동의</strong>
                            </li>
                            <li class="step03">
                                <span>STEP03</span>
                                <strong>지역선택</strong>
                            </li>
                            <li class="step04">
                                <span>STEP04</span>
                                <strong>회원정보입력</strong>
                            </li>
                            <li class="step05">
                                <span>STEP05</span>
                                <strong>회원정보확인</strong>
                            </li>
                            <li class="step06 active">
                                <span>STEP06</span>
                                <strong>등급선택</strong>
                            </li>
                            <li class="step07">
                                <span>STEP07</span>
                                <strong>가입완료</strong>
                            </li>
                        </ol>
                    </div>
                    <!--/ 회원가입 STEP -->

                    <h3 class="tit">등급선택</h3>
                    <p class="txt01">ViewCAR는 차별적이고 획신적인 서비스 제공을 위해 노력하겠습니다.
                        <br /> 필요하신 용도에 따라 서비스 등급을 선택해주세요. 궁금하신 사항은 언제든 고객센터
                        <strong>1599-8439</strong>로 문의주세요
                    </p>

                        <div class="select-grade">
                            <ul>
                                <li class="grade01 icon-spr">
                                    <div class="inner">
                                        <strong>BASIC 등급</strong>
                                        <ul>
                                            <li>차량∙사용자 운행정보</li>
                                            <li>차량 위치 </li>
                                            <li>차량주요 알림</li>
                                            <li>사고차량 조회</li>
                                            <li>차량 실시간 자가 진단</li>
                                            <li>차량고장 및 소모품 현황 실시간 확인</li>
                                            <li>차량경비 관리</li>
                                        </ul>
                                        <div class="select">
                                            <input type="radio" id="grade01" name="grade" checked="checked" value="1"/>
                                            <label for="grade01">1 차량당 3,300원/월
												<span>(단말 통신요금 및 VAT 포함)</span>
											</label>
                                        </div>
                                    </div>
                                </li>
                                <li class="grade02 icon-spr">
                                    <div class="inner">
                                        <strong>Standard 등급</strong>
                                        <ul>
                                            <li>BASIC 등급 제공 기능 + </li>
                                            <li>국세청 운행일지 완벽 대응</li>
                                            <li>별점∙과태료 관리</li>
                                            <li>차량사고 보고서</li>
                                            <li>회사 결재 관리</li>
                                            <li>차량 사용/관리 월간 리포트</li>
                                        </ul>
                                        <div class="select">
                                            <input type="radio" id="grade02" name="grade" value="2" />
                                            <label for="grade02">1 차량당 5,500원/월
												<span>(단말 통신요금 및 VAT 포함)</span>
											</label>
                                        </div>
                                    </div>
                                </li>
                                <li class="grade03 icon-spr">
                                    <div class="inner">
                                        <strong>Premium 등급</strong>
                                        <ul>
                                            <li>Standard 등급 제공 기능 + </li>
                                            <li>차량배차 /미배차 운행 관리</li>
                                            <li>운행스케쥴 관리</li>
                                            <li>차량정비 현황∙조회∙등록</li>
                                            <li>차량보고서 조회 등록</li>
                                        </ul>
                                        <p>
                                            ViewCAR가 제공하는 모든 기능을 제한없이
                                            <br /> 사용하실 수 있습니다.
                                        </p>
                                        <div class="select">
                                            <input type="radio" id="grade03" name="grade" value="3" />
                                            <label for="grade03">1 차량당 9,900원/월
												<span>(단말 통신요금 및 VAT 포함)</span>
											</label>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <div class="non-select">
                            <input type="checkbox" name="" id="nonSelect" /> 나중에 회원정보 설정 > 단말 타입 / 이용등급 선택에서 변경하겠습니다.
                        </div>

                        <!-- 하단 버튼 -->
                        <div class="btn-bottom ">
                            <button class="btn btn02" id="prev">이전</button>
                            <button class="btn btn03" id="next">계속</button>
                        </div>
                        <!--/ 하단 버튼 -->
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>