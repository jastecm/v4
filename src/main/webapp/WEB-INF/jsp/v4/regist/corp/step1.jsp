<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script>
$(document).ready(function(){
	$('#next').on('click',function(){
		/* ===필수=== */
		var corpName = $('#corpName').val();
		if( $V4.required( corpName , "기업명은 필수입니다." , $('#corpName')) ) return false;
		
		/* ===필수=== */
		var corpNum = $('#corpNum1').val() + $('#corpNum2').val() + $('#corpNum3').val();
		
		var regExp = /[0-9]{10}/;
		if(!regExp.test(corpNum) && (alert("사업자등록번호가 잘못되었습니다.")||1) && $('#corpNum1').focus()) return false;
				
		if( $V4.required($('#corpNum1').val() , "사업자 등록번호는 필수입니다." , $('#corpNum1')) ) return false;
		if( $V4.required($('#corpNum2').val() , "사업자 등록번호는 필수입니다." , $('#corpNum2')) ) return false;
		if( $V4.required($('#corpNum3').val() , "사업자 등록번호는 필수입니다." , $('#corpNum3')) ) return false;
		
		/* ===필수=== */
		var ceoName = $('#ceoName').val();
		if( $V4.required(ceoName , "대표자명은 필수입니다." , $('#ceoName')) ) return false;

		/* ===필수=== */		
		var corpPhoneCtyCode = $('#corpPhoneCtyCode').val();
		var corpPhone = $('#corpPhone').val();
		
		if( $V4.required(corpPhone , "대표자 연락처는 필수입니다." , $('#corpPhone')) ) return false;
		regExp = /^0[0-9]{8,10}$/;
		if(!regExp.test(corpPhone) && (alert("대표자 연락처 형식이 잘못되었습니다.")||1) && $('#corpPhone').focus()) return false;
		/* ===필수=== */
		
		
		var ceoIdHead = $('#ceoIdHead').val();
		var ceoIdTail = $('#ceoIdTail').val();
		
		if( $V4.required(ceoIdHead , "대표자 이메일은 필수입니다." , $('#ceoIdHead')) ) return false;
		if( $V4.required(ceoIdTail , "대표자 이메일은 필수입니다." , $('#ceoIdTail')) ) return false;
		
		var ceoId = ceoIdHead + "@" + ceoIdTail;
		
		regExp = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}$/;
		if(!regExp.test(ceoId) && (alert("대표자 이메일 형식이 잘못되었습니다.")||1) && $('#ceoIdHead').focus()) return false;
		
		var ceoPw = $('#ceoPw').val();
		
		if( $V4.required(ceoPw , "대표자 비밀번호는 필수입니다." , $('#ceoPw')) ) return false;
		
		//체크가 되어 있지 않으면
		if(!$(".ceoEq").is(':checked')){
			if(!$('#pwResult').hasClass('safe')){
				alert("대표자 비밀번호 형식이 잘못되었습니다.");
				$('#ceoPw').focus()
				return;
			}
		}
		
		
		var regExpAlphabet = new RegExp("(?=.*[a-zA-Z])");
		var regExpNumber = new RegExp("(?=.*[0-9])");
		var regExpSpecialChar = new RegExp("(?=.*[!@#$%^\\&*\\(\\)\\_\\+\\|\\-\\=\\\\\\[\\];'?\\.\\,])");
		var regExpLength = new RegExp("^.{8,20}$");
		
		if(!(regExpAlphabet.test(ceoPw) && regExpNumber.test(ceoPw) && regExpSpecialChar.test(ceoPw) && regExpLength.test(ceoPw)) && (alert("대표자 비밀번호 형식이 잘못되었습니다.")||1)) return false;
		/* ===필수=== */
		
		var corpBusieness = $('#corpBusieness').val()
		var corpItem = $('#corpItem').val()
		
		
		
		/* ===필수=== */
		var name = $('#name').val();
		if( $V4.required(name , "이름은 필수입니다." , $('#name')) ) return false;
		
		var mobilePhoneCtyCode = $('#mobilePhoneCtyCode').val()
		var mobilePhone = $('#mobilePhone').val();
		if( $V4.required(mobilePhone , "휴대폰 번호는 필수입니다." , $('#mobilePhone')) ) return false;
		regExp = /^01[0-9]{8,9}$/;
		if(!regExp.test(mobilePhone) && (alert("휴대폰 번호 형식이 잘못되었습니다.")||1) && $('#mobilePhone').focus()) return false;
		/* ===필수=== */
		
		var phoneCtyCode = $('#phoneCtyCode').val()
		var phone = $('#phone').val()
		regExp = /^0[0-9]{8,10}$|^$/;
		if(!regExp.test(phone) && (alert("전화번호 형식이 잘못되었습니다.")||1) && $('#phone').focus()) return false;
		
		var corpPosition = $('#corpPosition').val();
		var birth = $('#birth').val();
		var blood = $('#blood').val();
		var bloodRh = $('#bloodRh').val();
		var gender = $('input[name="sex"]:checked').val();
		var unitCurrency = $('#unitCurrency').val();
		
		regExp = /^[1-2]{1}[0-9]{3}[0-1]{1}[0-9]{1}[0-3]{1}[0-9]{1}$|^$/;
		
		//birth chk
		if(!regExp.test(birth) && (alert("생년월일 형식이 잘못되엇습니다.")||1)) return false;
		birth = birth?calcDate(new String(birth),"",0,new Date).getTime():null;
		
		$('#frm input[name="birth"]').val(birth);		
		$('#frm input[name="blood"]').val(blood);
		$('#frm input[name="bloodRh"]').val(bloodRh);
		$('#frm input[name="gender"]').val(gender);
		$('#frm input[name="unitCurrency"]').val(unitCurrency);
		
		
		$('#frm input[name="corpName"]').val(corpName);
		$('#frm input[name="corpNum"]').val(corpNum);
		$('#frm input[name="ceoName"]').val(ceoName);
		
		$('#frm input[name="corpPhoneCtyCode"]').val(corpPhoneCtyCode);
		$('#frm input[name="corpPhone"]').val(corpPhone);
		
		$('#frm input[name="name"]').val(name);
		$('#frm input[name="mobilePhoneCtyCode"]').val(mobilePhoneCtyCode);
		$('#frm input[name="mobilePhone"]').val(mobilePhone);
		
		$('#frm input[name="ceoId"]').val(ceoId);
		$('#frm input[name="ceoPw"]').val(ceoPw);
		
		$('#frm input[name="corpBusieness"]').val(corpBusieness);
		$('#frm input[name="corpItem"]').val(corpItem);
		$('#frm input[name="phoneCtyCode"]').val(phoneCtyCode);
		$('#frm input[name="phone"]').val(phone);
		$('#frm input[name="corpPosition"]').val(corpPosition);
		
		
		$V4.http_post("/api/v1/account/conflictChk",{accountId:ceoId},{
			requestMethod : "GET"
	    	,success : function(rtv){
	    		$V4.move("/regist/corp/step2",$('#frm'));
	    	},
	    	error : function(e){
	    		if( e.status == 409){
	    			alert("대표자 이메일이 중복됩니다.");
	    		}else{
	    			e.responseJSON?alert(e.status+"\n"+e.responseJSON.result):"";
	    		}
	    		
	    	}
	    });
	});
	
	
	
	$('#ceoIdTailChange').on('change',function(){
		$('#ceoIdTail').val($(this).val());
	});
	
	$(".ceoEq").on('change',function(){
		if($(this).is(":checked")){
			var name = $('#name').val();
			var mobilePhoneCtyCode = $('#mobilePhoneCtyCode').val();
			var mobilePhone = $('#mobilePhone').val();
			var id = "${rtv.accountId}".split("@");
			
			$("#ceoName").val(name).prop("readonly",true);
			$("#corpPhoneCtyCode").val(mobilePhoneCtyCode).prop("readonly",true);
			$("#corpPhone").val(mobilePhone).prop("readonly",true);
			$("#ceoIdHead").val(id[0]).prop("readonly",true);
			$("#ceoIdTail").val(id[1]).prop("readonly",true);
			$("#ceoPw").val("asd123!@#");
			$("#tr_ceoPw").hide();
			$("#ceoIdTailChange").hide();
			
		}else{
			$("#ceoName").val("").prop("readonly",false);
			$("#corpPhoneCtyCode").val($("#corpPhoneCtyCode option:first").val()).prop("readonly",false);
			$("#corpPhone").val("").prop("readonly",false);
			$("#ceoIdHead").val("").prop("readonly",false);
			$("#ceoIdTail").val("").prop("readonly",false);
			$("#ceoPw").val("");
			$("#tr_ceoPw").val("").show();
			$("#ceoIdTailChange").show();
		}
	});
	
	$("#name,#mobilePhoneCtyCode,#mobilePhone").on("change",function(){
		if($(".ceoEq").is(":checked")){
			var name = $('#name').val();
			var mobilePhoneCtyCode = $('#mobilePhoneCtyCode').val();
			var mobilePhone = $('#mobilePhone').val();
			
			$("#ceoName").val(name);
			$("#corpPhoneCtyCode").val(mobilePhoneCtyCode);
			$("#corpPhone").val(mobilePhone);
		}	
	});
	
	$("#ceoPw").on("keyup",function(e){
		var v = $(this).val();
		var chk = 0;		
		if(v){
			var regExpAlphabet = new RegExp("(?=.*[a-zA-Z])");
			var regExpNumber = new RegExp("(?=.*[0-9])");
			var regExpSpecialChar = new RegExp("(?=.*[!@#$%^\\&*\\(\\)\\_\\+\\|\\-\\=\\\\\\[\\];'?\\.\\,])");
			regExpAlphabet.test(v)&&chk++;
			regExpNumber.test(v)&&chk++;
			regExpSpecialChar.test(v)&&chk++;
			
		}
		var regExpLength = new RegExp("^.{8,20}$");
		if(chk == 3 && regExpLength.test(v)){
			$("#pwResult").removeClass("warning").addClass("safe").css('color','#00CC00').text("사용가능");
			
		}else{
			if(chk==0) chk = 1;
			$("#pwResult").removeClass("safe").addClass("warning").css('color','#e60012').text("사용불가");
			
		}
		
		$(".password-check").find("div").removeClass("step01").removeClass("step02").removeClass("step03").addClass("step0"+chk);
		
		
		
	});
	
});
</script>
<form id="frm" name="frm">
	<!-- 여기까진 공통 -->
	<input type="hidden" name="accountId" value="${rtv.accountId}"/>
	<input type="hidden" name="accountPw" value="${rtv.accountPw}"/>
	<input type="hidden" name="corpType" value="${rtv.corpType}"/>
	
	<input type="hidden" name="serviceAccepted" value="${rtv.serviceAccepted}"/>
	<input type="hidden" name="privateAccepted" value="${rtv.privateAccepted}"/>
	<input type="hidden" name="locationAccepted" value="${rtv.locationAccepted}"/>
	<input type="hidden" name="marketingAccepted" value="${rtv.marketingAccepted}"/>
	
	<input type="hidden" name="ctyCode" value="${rtv.ctyCode}"/>
	<input type="hidden" name="timezoneOffset" value="${rtv.timezoneOffset}"/>
	
	<input type="hidden" name="unitLength" value="${rtv.unitLength}"/>
	<input type="hidden" name="unitVolume" value="${rtv.unitVolume}"/>
	<input type="hidden" name="unitTemperature" value="${rtv.unitTemperature}"/>
	<input type="hidden" name="unitDate" value="${rtv.unitDate}"/>
	
	<!-- 여기까진 공통 -->
	
	<!-- 기업 필수 -->
		<!-- 기업명 -->
	<input type="hidden" name="corpName" value=""/>
		<!-- 사업자등록번호 -->
	<input type="hidden" name="corpNum" value=""/>
		<!-- 대표자명 -->
	<input type="hidden" name="ceoName" value=""/>

		<!-- 대표자 연락처 -->	
	<input type="hidden" name="corpPhoneCtyCode" value=""/>
	<input type="hidden" name="corpPhone" value=""/>
	
		<!-- 관리자 정보 -->
	<input type="hidden" name="name" value=""/>
	<input type="hidden" name="mobilePhoneCtyCode" value=""/>
	<input type="hidden" name="mobilePhone" value=""/>
	<input type="hidden" name="birth" value=""/>
	<input type="hidden" name="blood" value=""/>
	<input type="hidden" name="bloodRh" value=""/>
	<input type="hidden" name="gender" value=""/>
	<input type="hidden" name="unitCurrency" value=""/>
		<!-- 대표자이메일 -->
		<input type="hidden" name="ceoId" value=""/>
		<input type="hidden" name="ceoPw" value=""/>
		
	
	<!-- 기업 필수 -->
	
	
	<!-- 기업 선택 -->
		<!-- 업태 -->
	<input type="hidden" name="corpBusieness" value=""/>
		<!-- 종목 -->
	<input type="hidden" name="corpItem" value=""/>
		<!-- 일반연락처 -->
	<input type="hidden" name="phoneCtyCode" value=""/>
	<input type="hidden" name="phone" value=""/>
		<!-- 직급 -->	
	<input type="hidden" name="corpPosition" value=""/>
	<!-- 기업 선택 -->
	
</form>
 <!-- 본문 -->
  <section id="container">
      <div id="sub-container">
          <!-- 상단 타이틀 -->
          <div class="page-header">
              <h2>회원가입</h2>
              <span></span>
          </div>
          <!--/ 상단 타이틀 -->

          <!-- 콘텐츠 본문 -->
          <div id="contents-page" class="join-page">
              <!-- 회원가입 STEP -->
              <div class="join-step">
                  <ol>
                      <li class="step01">
                          <span>STEP01</span>
                          <strong>가입선택</strong>
                      </li>
                      <li class="step02">
                          <span>STEP02</span>
                          <strong>약관동의</strong>
                      </li>
                      <li class="step03">
                          <span>STEP03</span>
                          <strong>지역선택</strong>
                      </li>
                      <li class="step04 active">
                          <span>STEP04</span>
                          <strong>회원정보입력</strong>
                      </li>
                      <li class="step05">
                          <span>STEP05</span>
                          <strong>회원정보확인</strong>
                      </li>
                      <li class="step06">
                          <span>STEP06</span>
                          <strong>등급선택</strong>
                      </li>
                      <li class="step07">
                          <span>STEP07</span>
                          <strong>가입완료</strong>
                      </li>
                  </ol>
              </div>
              <!--/ 회원가입 STEP -->

              <h3 class="tit">회원정보입력</h3>
              <p class="txt01">
                  <span class="red">스마트 드라이빙!</span> ViewCAR 차량 관리 서비스의 정상적인 이용을 위해 아래 정보의 입력이 필요합니다.
                  <br />개인회원의 회원 가입은 WEB과 Mobile App 을 통해 모두 가입하실 수 있습니다.</p>

				  <!-- 차량관리자 정보 -->
                  <h4 class="tit mgt40 help">차량관리자 정보
                  		<div class="help-layer">
							<b>관리자</b> : 가입자가 기본 차량 관리자로 배정됩니다. 추후 사용자 초대후 관리자 권한을 이전할수 있습니다.
						</div>
                  </h4>
                  <table class="table mgt20">
                      <caption>차량관리자 정보</caption>
                      <tbody>
                          <tr>
                              <th scope="row">기본정보
                                  <span class="necessary">(필수입력)</span>
                              </th>
                              <td class="table">
                                  <table>
                                      <caption>차량관리자 기본정보</caption>
                                      <colgroup>
                                          <col style="width:16.35%" />
                                          <col />
                                      </colgroup>
                                      <tbody>
                                      	  <tr>
                                             	<th scope="row">아이디</th>
                                             	<td>
                                					${rtv.accountId}                 
                                             	</td>
                                          </tr>
                                          <tr>
                                              <th scope="row">이름</th>
                                              <td>
                                                  <input type="text" name="" id="name" />
                                              </td>
                                          </tr>
                                          <tr>
                                              <th scope="row">휴대폰 번호</th>
                                              <td>
		                                        <div class="division">
		                                            <div class="col-3">
		                                               <select name="" id="mobilePhoneCtyCode">
															<option value="+82">+82</option>
														</select>
		                                            </div>
		                                            <div class="space">&nbsp;</div>
		                                            <div class="col-9">
		                                               <input type="text" name="" class="tel numberOnly" id="mobilePhone" maxlength="11"/>
		                                            </div>
		                                        </div>
		                                    </td>
                                          </tr>
                                      </tbody>
                                  </table>
                              </td>
                          </tr>
                          <tr>
                              <th scope="row">부가정보
                                  <span>(선택입력)</span>
                              </th>
                              <td class="table">
                                  <table>
                                      <caption>차량관리자 부가정보</caption>
                                      <colgroup>
                                          <col style="width:16.35%" />
                                          <col style="width:34.65%" />
                                          <col style="width:16.35%" />
                                          <col style="width:34.65%" />
                                      </colgroup>
                                      <tbody>
                                          <tr>
                                              <th scope="row">일반 연락처</th>
                                              <td>
		                                        <div class="division">
		                                            <div class="col-3">
		                                              <select name="" id="phoneCtyCode">
													<option value="+82">+82</option>
												</select>
		                                            </div>
		                                            <div class="space">&nbsp;</div>
		                                            <div class="col-9">
		                                               <input type="text" name="" class="tel numberOnly" id="phone" maxlength="11" />
		                                            </div>
		                                        </div>
		                                    </td>
                                              
                                              
                                              <th scope="row">직급</th>
                                              <td>
                                                  <input type="text" name="" id="corpPosition" />
                                              </td>
                                          </tr>
                                          <tr>
			                                    <th scope="row">생년월일</th>
			                                    <td>
			                                        <input type="text" id="birth" class="numberOnly" placeholder="ex)20100710"/>
			                                    </td>
			                                    <th scope="row">성별</th>
			                                    <td>
			                                        <input type="radio" id="sex01" name="sex" checked="checked" value="M" />
			                                        <label for="sex01">남자</label> &nbsp;&nbsp;&nbsp;&nbsp;
			                                        <input type="radio" id="sex02" name="sex" value="F" />
			                                        <label for="sex02">여자</label>
			                                    </td>
			                                </tr>
			                                <tr>
			                                    <th scope="row">혈액형</th>
			                                    <td colspan="3">
			                                        <select id="blood" style="width:220px">
														<option value="A">A형</option>
														<option value="B">B형</option>
														<option value="O">O형</option>
														<option value="AB">AB형</option>
													</select>
													<select id="bloodRh" style="width:220px">
														<option value="rh+">rh+</option>
														<option value="rh-">rh-</option>
													</select>
			                                    </td>
			                                </tr>
			                                <tr>
			                                    <th scope="row" class="help">기준통화
			                                    	<div class="layer">
			                                    		<b>기준통화</b> 는 추후 변경 불가능합니다.
			                                    	</div>
			                                    </th>
			                                    <td colspan="3">
			                                        <select id="unitCurrency">
														<option value="WON">원</option>
														<option value="USD">달러</option>
													</select>
			                                    </td>
			                                </tr>
                                      </tbody>
                                  </table>
                              </td>
                          </tr>
                      </tbody>
                  </table>
                  <!--/ 차량관리자 정보 -->


                  <!-- 기본정보 -->
                  <h4 class="tit mgt40">기업 정보</h4>
                  <table class="table mgt20">
                      <caption>기본정보</caption>
                      <tbody>
                          <!-- 
                          <tr>
                              <th scope="row">비밀번호</th>
                              <td>
                                  <input type="password" name="" />
                              </td>
                          </tr> -->
                          <tr>
                              <th scope="row">기본 정보
                                  <span class="necessary">(필수입력)</span>
                              </th>
                              <td class="table">
                                  <table>
                                      <caption>기업 정보</caption>
                                      <colgroup>
                                          <col style="width:16.35%" />
                                          <col style="width:34.65%" />
                                          <col style="width:16.35%" />
                                          <col style="width:34.65%" />
                                      </colgroup>
                                      <tbody>
                                          <tr>
                                              <th scope="row">기업명</th>
                                              <td>
                                                  <input type="text" name="" id="corpName"  />
                                              </td>
                                              <th scope="row">사업자등록번호</th>
                                              <td>
                                                  <input type="text" name="" class="numberOnly" maxlength="3" style="width:50px" id="corpNum1" /> -
                                                  <input type="text" name="" class="numberOnly" maxlength="2" style="width:40px" id="corpNum2" /> -
                                                  <input type="text" name="" class="numberOnly" maxlength="5" style="width:70px" id="corpNum3" />
                                              </td>
                                          </tr>
                                          <tr>
                                              <th scope="row" class="help">대표자명
                                              	<div class="layer">
													<b>대표자 정보</b> : 가입자외에 대표자를 자동으로 회원가입합니다. 가입자와 대표자가 동일할경우 가입하지 않습니다.
												</div>
                                              </th>
                                              <td>
                                                  <input type="text" name="" id="ceoName" />
                                                  </br>
                                                  <input type="checkbox" class="ceoEq" style="margin-top:5px"/> 가입자와 대표자가 동일합니다.
                                              </td>
                                              <th scope="row">대표자 연락처</th>
                                              <td>
		                                        <div class="division">
		                                            <div class="col-3">
		                                               <select name="" id="corpPhoneCtyCode">
														<option value="+82">+82</option>
													</select>
		                                            </div>
		                                            <div class="space">&nbsp;</div>
		                                            <div class="col-9">
		                                               <input type="text" name="" class="tel numberOnly" id="corpPhone" maxlength="11"/>
		                                            </div>
		                                        </div>
		                                    </td>
                                          </tr>
                                          <tr>
                                              <th scope="row">대표자 이메일</th>
                                              <td colspan="3">
		                                         <div class="division">
                                            		<div class="col-5">
                                                		<input type="text" id="ceoIdHead" /> 
                                            		</div>
                                            		<div class="space">&nbsp;@&nbsp;</div>
                                            		<div class="col-4">
                                                		<input type="text" id="ceoIdTail" />
                                            		</div>
                                            		<div class="space">&nbsp;</div>
                                            		<div class="col-3">
                                                		<select name="" onchange="" id="ceoIdTailChange">
															<option value="">이메일 선택</option>
															<option value="hanmail.net">다음</option>
															<option value="empal.com">엠파스</option>
															<option value="netian.com">네띠앙</option>
															<option value="naver.com">네이버</option>
															<option value="nate.com">네이트</option>
															<option value="dreamwiz.com">드림위즈</option>
															<option value="lycos.co.kr">라이코스</option>
															<option value="yahoo.co.kr">야후</option>
															<option value="chol.com">천리안</option>
															<option value="korea.com">코리아닷컴</option>
															<option value="paran.com">파란</option>
															<option value="freechal.com">프리챌</option>
															<option value="hitel.net">하이텔</option>
															<option value="hanmir.com">한미르</option>
															<option value="hotmail.com">핫메일</option>
															<option value="hanmir.com">한미르</option>
															<option value="">직접쓰기</option>
														</select>
                                            		</div>
                                        		</div>
                                              </td>
                                          </tr>
                                          <tr id="tr_ceoPw">
                                              <th scope="row">대표자 비밀번호</th>
                                              <td colspan="3">
                                                  <input type="text" id="ceoPw" />
                                                  <div class="password-check">
		                                            <div class="bar step01"><span>1</span></div>
		                                            <p>비밀번호 안전도 :<span class="warning" id="pwResult">사용불가</span>
		                                            </p>
		                                        </div>
                                              </td>
                                          </tr>
                                      </tbody>
                                  </table>
                              </td>
                          </tr>
                          <tr>
                              <th scope="row">부가 정보
                                  <span>(선택입력)</span>
                              </th>
                              <td class="table">
                                  <table>
                                      <caption>기업 정보</caption>
                                      <colgroup>
                                          <col style="width:16.35%" />
                                          <col style="width:34.65%" />
                                          <col style="width:16.35%" />
                                          <col style="width:34.65%" />
                                      </colgroup>
                                      <tbody>
                                          <tr>
                                              <th scope="row">업태</th>
                                              <td>
                                                  <input type="text" name="" id="corpBusieness" />
                                              </td>
                                              <th scope="row">종목</th>
                                              <td>
                                                  <input type="text" name="" id="corpItem" />
                                              </td>
                                          </tr>
                                      </tbody>
                                  </table>
                              </td>
                          </tr>
                      </tbody>
                  </table>
                  <!--/ 기본정보 -->

                  

                  <!-- 하단 버튼 -->
                  <div class="btn-bottom ">
                      <button class="btn btn02" id="prev">이전</button>
                      <button class="btn btn03" id="next">계속</button>
                  </div>
                  <!--/ 하단 버튼 -->
          </div>
          <!--/ 콘텐츠 본문 -->
      </div>
  </section>
  <!--/ 본문 -->