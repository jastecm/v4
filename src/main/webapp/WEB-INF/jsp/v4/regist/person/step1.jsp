<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script>
$(document).ready(function(){
	$('#addrFindBtn').on('click',function(){
		new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 각 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var fullAddr = ''; // 최종 주소 변수
                var extraAddr = ''; // 조합형 주소 변수

                // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
                if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                    fullAddr = data.roadAddress;

                } else { // 사용자가 지번 주소를 선택했을 경우(J)
                    fullAddr = data.jibunAddress;
                }

                // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
                if(data.userSelectedType === 'R'){
                    //법정동명이 있을 경우 추가한다.
                    if(data.bname !== ''){
                        extraAddr += data.bname;
                    }
                    // 건물명이 있을 경우 추가한다.
                    if(data.buildingName !== ''){
                        extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                    }
                    // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                    fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
                }
                
                document.getElementById('addr').value = fullAddr;

            }
        }).open();
	});
	
	//다음
	$('#next').on('click',function(){
		//기본정보는 필수이다
		
		var name = $('#name').val();
		var mobilePhone = $('#mobilePhone').val();
		
		if( $V4.required(name , "이름은 필수입니다." , $('#name')) ) return false;
		if( $V4.required(mobilePhone , "휴대폰 번호는 필수입니다." , $('#mobilePhone')) ) return false;
		
		regExp = /^01[0-9]{8,9}$/;
		if(!regExp.test(mobilePhone) && (alert("휴대폰 번호 형식이 잘못되었습니다.")||1) && $('#mobilePhone').focus()) return false;
		
		
		var mobilePhoneCtyCode = $('#mobilePhoneCtyCode').val();
		
		$('#frm input[name="name"]').val(name);
		$('#frm input[name="mobilePhoneCtyCode"]').val(mobilePhoneCtyCode);
		$('#frm input[name="mobilePhone"]').val(mobilePhone);
		
		
		var phoneCtyCode = $('#phoneCtyCode').val();
		var phone = $('#phone').val();
		
		regExp = /^0[0-9]{8,10}$|^$/;
		if(!regExp.test(phone) && (alert("전화번호 형식이 잘못되었습니다.")||1) && $('#phone').focus()) return false;
		
		
		var addr = $('#addr').val();
		var birth = $('#birth').val();
		var blood = $('#blood').val();
		var bloodRh = $('#bloodRh').val();
		var gender = $('input[name="sex"]:checked').val();
		var unitCurrency = $('#unitCurrency').val();
		
		regExp = /^[1-2]{1}[0-9]{3}[0-1]{1}[0-9]{1}[0-3]{1}[0-9]{1}$|^$/;
		
		//birth chk
		if(!regExp.test(birth) && (alert("생년월일 형식이 잘못되엇습니다.")||1)) return false;
		birth = birth?calcDate(new String(birth),"",0,new Date).getTime():null;
		
		$('#frm input[name="phoneCtyCode"]').val(phoneCtyCode);
		$('#frm input[name="phone"]').val(phone);
		$('#frm input[name="addr"]').val(addr);
		$('#frm input[name="birth"]').val(birth);		
		$('#frm input[name="blood"]').val(blood);
		$('#frm input[name="bloodRh"]').val(bloodRh);
		$('#frm input[name="gender"]').val(gender);
		$('#frm input[name="unitCurrency"]').val(unitCurrency);
		
		var cmd = "/regist/person/step2";
		$V4.move(cmd,$("#frm"));
	});
	
	//이전
	$('#prev').on('click',function(){
		window.history.back();
	});
});
</script>
<form id="frm" name="frm">
	<input type="hidden" name="accountId" value="${rtv.accountId}"/>
	<input type="hidden" name="accountPw" value="${rtv.accountPw}"/>
	<input type="hidden" name="corpType" value="${rtv.corpType}"/>
	
	<input type="hidden" name="serviceAccepted" value="${rtv.serviceAccepted}"/>
	<input type="hidden" name="privateAccepted" value="${rtv.privateAccepted}"/>
	<input type="hidden" name="locationAccepted" value="${rtv.locationAccepted}"/>
	<input type="hidden" name="marketingAccepted" value="${rtv.marketingAccepted}"/>
	
	<input type="hidden" name="ctyCode" value="${rtv.ctyCode}"/>
	<input type="hidden" name="timezoneOffset" value="${rtv.timezoneOffset}"/>
	
	<input type="hidden" name="unitLength" value="${rtv.unitLength}"/>
	<input type="hidden" name="unitVolume" value="${rtv.unitVolume}"/>
	<input type="hidden" name="unitTemperature" value="${rtv.unitTemperature}"/>
	<input type="hidden" name="unitDate" value="${rtv.unitDate}"/>
	

	<!-- 필수 -->	
	<input type="hidden" name="name" value=""/>
	<input type="hidden" name="mobilePhoneCtyCode" value=""/>
	<input type="hidden" name="mobilePhone" value=""/>
	<!-- 필수 -->
	
	<!-- 선택 -->
	<input type="hidden" name="phoneCtyCode" value=""/>
	<input type="hidden" name="phone" value=""/>
	<input type="hidden" name="addr" value=""/>
	<input type="hidden" name="birth" value=""/>
	<input type="hidden" name="blood" value=""/>
	<input type="hidden" name="bloodRh" value=""/>
	<input type="hidden" name="gender" value=""/>
	<input type="hidden" name="unitCurrency" value=""/>
	<!-- 선택 -->
</form>
 <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>회원가입</h2>
                    <span></span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="join-page">
                     <!-- 회원가입 STEP -->
                    <div class="join-step">
                        <ol>
                            <li class="step01">
                                <span>STEP01</span>
                                <strong>가입선택</strong>
                            </li>
                            <li class="step02">
                                <span>STEP02</span>
                                <strong>약관동의</strong>
                            </li>
                            <li class="step03">
                                <span>STEP03</span>
                                <strong>지역선택</strong>
                            </li>
                            <li class="step04 active">
                                <span>STEP04</span>
                                <strong>회원정보입력</strong>
                            </li>
                            <li class="step05">
                                <span>STEP05</span>
                                <strong>회원정보확인</strong>
                            </li>
                            <li class="step07">
                                <span>STEP06</span>
                                <strong>가입완료</strong>
                            </li>
                        </ol>
                    </div>
                    <!--/ 회원가입 STEP -->

                    <h3 class="tit">회원정보입력</h3>
                    <p class="txt01">
                        <span class="red">스마트 드라이빙!</span> ViewCAR 차량 관리 서비스의 정상적인 이용을 위해 아래 정보의 입력이 필요합니다.
                        <br />개인회원의 회원 가입은 WEB과 Mobile App 을 통해 모두 가입하실 수 있습니다.</p>

                    <!-- 폼 -->
                        <!-- 기본정보 -->
                        <h4 class="tit mgt40">기본정보</h4>
                        <table class="table mgt20">
                            <caption>기본정보</caption>
                            <tbody>
                                <%-- <tr>
                                    <th scope="row">아이디</th>
                                    <td>
                                        <span>${rtv.accountId }</span>
                                        <a class="btn btn04 md">인증완료</a>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">비밀번호</th>
                                    <td>
                                        <input type="password" id="accountPw" value="${rtv.accountPw}" />
                                    </td>
                                </tr> --%>
                                <tr>
                                    <th scope="row">이름</th>
                                    <td>
                                        <input type="text" id="name" maxlength="10" />
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">휴대폰번호</th>
                                    <td>
                                        <div class="division">
                                            <div class="col-3">
                                                <select id="mobilePhoneCtyCode">
                                       				<option value="+82">+82</option>
                                    			</select>
                                            </div>
                                            <div class="space">&nbsp;</div>
                                            <div class="col-9">
                                                <input type="text" id="mobilePhone" class="numberOnly" maxlength="11" />
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!--/ 기본정보 -->

                        <!-- 선택정보입력 -->
                        <h4 class="tit mgt40">선택정보입력</h4>
                        <table class="table mgt20">
                            <caption>선택정보입력</caption>
                            <tbody>
                                <tr>
                                    <td colspan="2" class="txt01">
                                        차량사고 발생시, 긴급SOS를 긴급연락처로 전송할 정보를 입력해주세요.
                                        <br /> 해당정보는 회원정보 수정페이지에서 변경하실 수 있습니다.
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">일반연락처</th>
                                    <td>
                                        <div class="division">
                                            <div class="col-3">
                                                <select id="phoneCtyCode">
                                       				<option value="+82">+82</option>
                                    			</select>
                                            </div>
                                            <div class="space">&nbsp;</div>
                                            <div class="col-9">
                                                <input type="text" id="phone" class="numberOnly" maxlength="11" />
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">주소</th>
                                    <td>
                                        <input type="text" id="addr" style="width:420px;" maxlength="100" /> 
                                        <button class="btn btn04 md" id="addrFindBtn">주소 검색</button>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">생년월일</th>
                                    <td>
                                        <input type="text" id="birth" class="numberOnly" maxlength="8" />
                                        &nbsp;&nbsp; ex) 20100710
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">성별</th>
                                    <td>
                                        <input type="radio" id="sex01" name="sex" checked="checked" value="M" />
                                        <label for="sex01">남자</label> &nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="radio" id="sex02" name="sex" value="F" />
                                        <label for="sex02">여자</label>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">혈액형</th>
                                    <td>
                                        <select id="blood" style="width:220px">
											<option value="A">A형</option>
											<option value="B">B형</option>
											<option value="O">O형</option>
											<option value="AB">AB형</option>
										</select>
										<select id="bloodRh" style="width:220px">
											<option value="rh+">rh+</option>
											<option value="rh-">rh-</option>
										</select>
                                    </td>
                                </tr>
								<tr>
                                    <th scope="row" class="help">기준통화
                                    	<div class="layer">
                                    		<b>기준통화</b> 는 추후 변경 불가능합니다.
                                    	</div>
                                    </th>
                                    <td colspan="3">
                                        <select id="unitCurrency">
											<option value="WON">원</option>
											<option value="USD">달러</option>
										</select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!--/ 선택정보입력 -->

                        <!-- 하단 버튼 -->
                        <div class="btn-bottom ">
                            <button class="btn btn02" id="prev">이전</button>
                            <button class="btn btn03" id="next">계속</button>
                        </div>
                        <!--/ 하단 버튼 -->
                    <!--/ 폼 -->
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>