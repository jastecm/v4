<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script>
$(document).ready(function(){
	
});
</script>
 <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>회원가입</h2>
                    <span></span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="join-page">
					 <!-- 회원가입 STEP -->
                    <div class="join-step">
                        <ol>
                            <li class="step01">
                                <span>STEP01</span>
                                <strong>가입선택</strong>
                            </li>
                            <li class="step02">
                                <span>STEP02</span>
                                <strong>약관동의</strong>
                            </li>
                            <li class="step03">
                                <span>STEP03</span>
                                <strong>지역선택</strong>
                            </li>
                            <li class="step04">
                                <span>STEP04</span>
                                <strong>회원정보입력</strong>
                            </li>
                            <li class="step05">
                                <span>STEP05</span>
                                <strong>회원정보확인</strong>
                            </li>
                            <!-- 개인,파트너 -->
                            <c:if test="${corpType eq '1' || corpType eq '4'}">
                            	<li class="step06 active">
                                	<span>STEP06</span>
                                	<strong>가입완료</strong>
                           		</li>
                            </c:if>
                            <!-- 법인렌트카 -->
                            <c:if test="${corpType eq '2' || corpType eq '3'}">
                            	<li class="step06">
                                	<span>STEP06</span>
                                	<strong>등급선택</strong>
                            	</li>
                            	<li class="step07 active">
                                	<span>STEP07</span>
                                	<strong>가입완료</strong>
                            	</li>
                            </c:if>
                        </ol>
                    </div>
                    <!--/ 회원가입 STEP -->

					<div class="join-complete">
                        <strong>WELCOME</strong>
                        <span>회원가입을 진심으로 감사드립니다.</span> ${name}님께서는 ${accountId}(ID)로 ㈜자스텍엠의 회원으로 가입되셨습니다.
                        <br /> ㈜자스텍엠은 다양한 정보와 함께 보다 나은 서비스를 제공하도록 열심히 노력하겠습니다.
                        <br /> 감사합니다.
                    </div>
                    <!--/ 회원가입 완료 -->

                    <!-- 하단 버튼 -->
                    <div class="btn-bottom">
                        <button type="button" class="btn btn01 btn_main" >메인으로</button>
                        <button type="button" class="btn btn02 btn_login" >로그인</button>
                    </div>
                    <!--/ 하단 버튼 -->

                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>