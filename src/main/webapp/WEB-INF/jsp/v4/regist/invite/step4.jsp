<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script>
$(document).ready(function(){
	$('#next').on('click',function(){
		$V4.http_post("/api/1/account/invite",$('#frm').serializeObject(),{
	    	success : function(rtv){
	    		$V4.move("/regist/step7",$('#frm'));
	    	}
	    });
	});
});
</script>
<form id="frm" name="frm" action="" method="post">
	<input type="hidden" name="accountId" value="${rtv.accountId}"/>
	<input type="hidden" name="corpKey" value="${rtv.corpKey}"/>
	
	<input type="hidden" name="serviceAccepted" value="${rtv.serviceAccepted}"/>
	<input type="hidden" name="privateAccepted" value="${rtv.privateAccepted}"/>
	<input type="hidden" name="locationAccepted" value="${rtv.locationAccepted}"/>
	<input type="hidden" name="marketingAccepted" value="${rtv.marketingAccepted}"/>
	
	<input type="hidden" name="ctyCode" value="${rtv.ctyCode}"/>
	<input type="hidden" name="timezoneOffset" value="${rtv.timezoneOffset}"/>
	<input type="hidden" name="unitLength" value="${rtv.unitLength}"/>
	<input type="hidden" name="unitVolume" value="${rtv.unitVolume}"/>
	<input type="hidden" name="unitTemperature" value="${rtv.unitTemperature}"/>
	<input type="hidden" name="unitDate" value="${rtv.unitDate}"/>
	
	<input type="hidden" name="accountPw" value="${rtv.accountPw}"/>
	
	<input type="hidden" name="name" value="${rtv.name}"/>
	<input type="hidden" name="mobilePhoneCtyCode" value="${rtv.mobilePhoneCtyCode}"/>
	<input type="hidden" name="mobilePhone" value="${rtv.mobilePhone}"/>
	
	<input type="hidden" name="phoneCtyCode" value="${rtv.phoneCtyCode}"/>
	<input type="hidden" name="phone" value="${rtv.phone}"/>
	<input type="hidden" name="birth" value="${rtv.birth}"/>
	<input type="hidden" name="blood" value="${rtv.blood}"/>
	<input type="hidden" name="bloodRh" value="${rtv.bloodRh}"/>
	<input type="hidden" name="gender" value="${rtv.gender}"/>
</form>
 <!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>회원가입</h2>
                    <span></span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="join-page">
                    <!-- 회원가입 STEP -->
                    <div class="join-step">
                        <ol>
                            <li class="step01">
                                <span>STEP01</span>
                                <strong>약관동의</strong>
                            </li>
                            <li class="step02">
                                <span>STEP02</span>
                                <strong>지역선택</strong>
                            </li>
                            <li class="step03">
                                <span>STEP03</span>
                                <strong>회원정보입력</strong>
                            </li>
                            <li class="step04 active">
                                <span>STEP04</span>
                                <strong>회원정보확인</strong>
                            </li>
                            <li class="step05">
                               	<span>STEP05</span>
                               	<strong>가입완료</strong>
                           	</li>
                        </ol>
                    </div>
                    <!--/ 회원가입 STEP -->

                    <h3 class="tit">회원정보입력</h3>
                    <p class="txt01">
                        <span class="red">스마트 드라이빙!</span> ViewCAR 차량 관리 서비스의 정상적인 이용을 위해 아래 정보의 입력이 필요합니다.
                        <br />개인회원의 회원 가입은 WEB과 Mobile App 을 통해 모두 가입하실 수 있습니다.</p>

                        <!-- 기본정보 -->
                        <h4 class="tit mgt40">기본정보</h4>
                        <table class="table mgt20">
                            <caption>기본정보</caption>
                            <tbody>
                                <tr>
                                    <th scope="row">아이디</th>
                                    <td>
                                        ${rtv.accountId}
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">이름</th>
                                    <td>
                                        ${rtv.name}
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">휴대폰번호</th>
                                    <td>
                                        ${rtv.mobilePhoneCtyCode} ${rtv.mobilePhone}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!--/ 기본정보 -->

                        <!-- 선택정보 -->
                        <h4 class="tit mgt40">선택정보</h4>
                        <table class="table mgt20">
                            <caption>선택정보</caption>
                            <colgroup>
                                <col style="width:16.35%" />
                                <col style="width:34.65%" />
                                <col style="width:16.35%" />
                                <col style="width:34.65%" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td colspan="4" class="txt01">
                                        차량사고 발생시, 긴급SOS를 긴급연락처로 전송할 정보를 입력해주세요.
                                        <br /> 해당정보는 회원정보 수정페이지에서 변경하실 수 있습니다.
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">전화번호</th>
                                    <td>
                                        ${rtv.phoneCtyCode} ${rtv.phone}
                                    </td>
                                    <th scope="row">성별</th>
                                    <td>
                                        <c:if test="${rtv.gender eq 'M'}">
                                    	남자
                                    	</c:if>
                                    	<c:if test="${rtv.gender eq 'F'}">
                                    	여자
                                    	</c:if>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">생년월일</th>
                                    <td>
                                       <c:if test="${not empty rtv.birth}">
											<jsp:useBean id="dateValue" class="java.util.Date"/>
											<jsp:setProperty name="dateValue" property="time" value="${rtv.birth}"/>
											<c:choose>
												<c:when test="${rtv.unitDate eq 'YMD'}">
													<fmt:formatDate value="${dateValue}" pattern="yyyy/MM/dd"/>	
												</c:when>									
												<c:when test="${rtv.unitDate eq 'MDY'}">
													<fmt:formatDate value="${dateValue}" pattern="MM/dd/yyyy"/>	
												</c:when>
												<c:when test="${rtv.unitDate eq 'DMY'}">
													<fmt:formatDate value="${dateValue}" pattern="dd/MM/yyyy"/>	
												</c:when>
												<c:otherwise>
													<fmt:formatDate value="${dateValue}" pattern="yyyy/MM/dd"/>
												</c:otherwise>
											</c:choose>
										</c:if>
                                    </td>
                                    <th scope="row">혈액형</th>
                                    <td>
                                        ${rtv.blood}형 ${rtv.bloodRh}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!--/ 선택정보 -->

                        <!-- 하단 버튼 -->
                        <div class="btn-bottom ">
                            <button class="btn btn02" id="prev">이전</button>
                            <button class="btn btn03" id="next">계속</button>
                        </div>
                        <!--/ 하단 버튼 -->
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->