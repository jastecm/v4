<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<style>
table.table .safe {
    margin-left: 30px;
    font-size: 15px;
    color: #00CC00;
}
.join-page .password-check span.safe {
    margin-left: 0 !important;
}
</style>
<script>
$(document).ready(function(){
	$('#prev').on('click',function(){
		window.history.back();
	});
	
	$('#next').on('click',function(){
		var name = $('#name').val();
		var mobilePhone = $('#mobilePhone').val();
		var accountPw = $('#accountPw').val();
		
		if( $V4.required(accountPw , "비밀번호는 필수입니다." , $('#accountPw')) ) return false;
		
		var regExpAlphabet = new RegExp("(?=.*[a-zA-Z])");
		var regExpNumber = new RegExp("(?=.*[0-9])");
		var regExpSpecialChar = new RegExp("(?=.*[!@#$%^\\&*\\(\\)\\_\\+\\|\\-\\=\\\\\\[\\];'?\\.\\,])");
		var regExpLength = new RegExp("^.{8,20}$");
		
		if(!($("#pwResult").hasClass("safe")&&$("#reResult").hasClass("safe"))&&(alert("비밀번호를 확인해주세요.(영어,숫자,특수문자 가 포함된 8~20자)")||1)) return false;
		
		
		if( $V4.required(name , "이름은 필수입니다." , $('#name')) ) return false;
		
		if( $V4.required(mobilePhone , "휴대폰 번호는 필수입니다." , $('#mobilePhone')) ) return false;
		regExp = /^01[0-9]{8,9}$/;
		if(!regExp.test(mobilePhone) && (alert("휴대폰 번호 형식이 잘못되었습니다.")||1) && $('#mobilePhone').focus()) return false;
		
		var mobilePhoneCtyCode = $('#mobilePhoneCtyCode').val();
		
		$('#frm input[name="accountPw"]').val(accountPw);
		$('#frm input[name="name"]').val(name);
		$('#frm input[name="mobilePhoneCtyCode"]').val(mobilePhoneCtyCode);
		$('#frm input[name="mobilePhone"]').val(mobilePhone);
		
		var phoneCtyCode = $('#phoneCtyCode').val();
		var phone = $('#phone').val();
		
		regExp = /^0[0-9]{8,10}$|^$/;
		if(!regExp.test(phone) && (alert("전화번호 형식이 잘못되었습니다.")||1) && $('#phone').focus()) return false;
		
		var birth = $('#birth').val();
		var blood = $('#blood').val();
		var bloodRh = $('#bloodRh').val();
		var gender = $('input[name="sex"]:checked').val();
		
		//birth chk
		if(birth && birth.length != 8 && (alert("생년월일 형식이 잘못되엇습니다.")||1)) return false;
		birth = birth?calcDate(new String(birth),"",0,new Date).getTime():null;
		
		$('#frm input[name="phoneCtyCode"]').val(phoneCtyCode);
		$('#frm input[name="phone"]').val(phone);
		$('#frm input[name="birth"]').val(birth);
		$('#frm input[name="blood"]').val(blood);
		$('#frm input[name="bloodRh"]').val(bloodRh);
		$('#frm input[name="gender"]').val(gender);
		
		
		$V4.move("/inviteRegist/step4",$("#frm"));
	});
	
	$("#accountPw").on("keyup",function(e){
		var v = $(this).val();
		var chk = 0;		
		if(v){
			var regExpAlphabet = new RegExp("(?=.*[a-zA-Z])");
			var regExpNumber = new RegExp("(?=.*[0-9])");
			var regExpSpecialChar = new RegExp("(?=.*[!@#$%^\\&*\\(\\)\\_\\+\\|\\-\\=\\\\\\[\\];'?\\.\\,])");
			regExpAlphabet.test(v)&&chk++;
			regExpNumber.test(v)&&chk++;
			regExpSpecialChar.test(v)&&chk++;
			$("#accountPwRe").val()==v?$("#reResult").removeClass("warning").addClass("safe").text("비밀번호가 일치합니다."):$("#reResult").removeClass("safe").addClass("warning").text("비밀번호가 일치하지 않습니다.");
		}
		var regExpLength = new RegExp("^.{8,20}$");
		if(chk == 3 && regExpLength.test(v)){
			$("#pwResult").removeClass("warning").addClass("safe").text("사용가능");
			
		}else{
			if(chk==0) chk = 1;
			$("#pwResult").removeClass("safe").addClass("warning").text("사용불가");
			
		}
		
		$(".password-check").find("div").removeClass("step01").removeClass("step02").removeClass("step03").addClass("step0"+chk);
		
		
		
	});
	
	$("#accountPwRe").on("keyup",function(e){
		var v = $(this).val();
		v&&$("#accountPw").val()==v?$("#reResult").removeClass("warning").addClass("safe").text("비밀번호가 일치합니다."):$("#reResult").removeClass("safe").addClass("warning").text("비밀번호가 일치하지 않습니다.");
	});
});
</script>
<form id="frm" name="frm" action="" method="post">
	<input type="hidden" name="accountId" value="${rtv.accountId}"/>
	<input type="hidden" name="corpKey" value="${rtv.corpKey}"/>
	
	<input type="hidden" name="serviceAccepted" value="${rtv.serviceAccepted}"/>
	<input type="hidden" name="privateAccepted" value="${rtv.privateAccepted}"/>
	<input type="hidden" name="locationAccepted" value="${rtv.locationAccepted}"/>
	<input type="hidden" name="marketingAccepted" value="${rtv.marketingAccepted}"/>
	
	<input type="hidden" name="ctyCode" value="${rtv.ctyCode}"/>
	<input type="hidden" name="timezoneOffset" value="${rtv.timezoneOffset}"/>
	
	<input type="hidden" name="unitLength" value="${rtv.unitLength}"/>
	<input type="hidden" name="unitVolume" value="${rtv.unitVolume}"/>
	<input type="hidden" name="unitTemperature" value="${rtv.unitTemperature}"/>
	<input type="hidden" name="unitDate" value="${rtv.unitDate}"/>
	
	<input type="hidden" name="accountPw" value=""/>
	
	<input type="hidden" name="name" value=""/>
	<input type="hidden" name="mobilePhoneCtyCode" value=""/>
	<input type="hidden" name="mobilePhone" value=""/>
	
	<input type="hidden" name="phoneCtyCode" value=""/>
	<input type="hidden" name="phone" value=""/>
	<input type="hidden" name="birth" value=""/>
	<input type="hidden" name="blood" value=""/>
	<input type="hidden" name="bloodRh" value=""/>
	<input type="hidden" name="gender" value=""/>
	
</form>
 <!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>회원가입</h2>
                    <span></span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="join-page">
                    <!-- 회원가입 STEP -->
                    <div class="join-step">
                        <ol>
                            <li class="step01">
                                <span>STEP01</span>
                                <strong>약관동의</strong>
                            </li>
                            <li class="step02">
                                <span>STEP02</span>
                                <strong>지역선택</strong>
                            </li>
                            <li class="step03 active">
                                <span>STEP03</span>
                                <strong>회원정보입력</strong>
                            </li>
                            <li class="step04">
                                <span>STEP04</span>
                                <strong>회원정보확인</strong>
                            </li>
                            <li class="step05">
                               	<span>STEP05</span>
                               	<strong>가입완료</strong>
                           	</li>
                        </ol>
                    </div>
                    <!--/ 회원가입 STEP -->

                    <h3 class="tit">회원정보입력</h3>
                    <p class="txt01">
                        <span class="red">스마트 드라이빙!</span> ViewCAR 차량 관리 서비스의 정상적인 이용을 위해 아래 정보의 입력이 필요합니다.
                        <br />개인회원의 회원 가입은 WEB과 Mobile App 을 통해 모두 가입하실 수 있습니다.</p>

                        <!-- 기본정보 -->
                        <h4 class="tit mgt40">기본정보</h4>
                        <table class="table mgt20">
                            <caption>기본정보</caption>
                            <tbody>
                                <tr>
                                    <th scope="row">아이디</th>
                                    <td>
                                        ${rtv.accountId}
                                        <a href="javascript:void(0);" class="btn btn04 md">인증완료</a>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">비밀번호</th>
                                    <td>
                                        <input type="password" name="" id="accountPw" />
                                        <div class="password-check">
                                            <div class="bar step01"><span>1</span></div>
                                            <p>비밀번호 안전도 :<span class="warning" id="pwResult">사용불가</span>
                                            </p>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">비밀번호 확인</th>
                                    <td>
                                        <input type="password" name="" id="accountPwRe" />
                                        <span class="warning" id="reResult">비밀번호가 일치하지 않습니다.</span>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">이름</th>
                                    <td>
                                        <input type="text" name="" id="name" />
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">휴대폰번호</th>
                                    <td>
                                        <div class="division">
                                           <div class="col-3">
                                             <select name="" id="mobilePhoneCtyCode">
											<option value="+82">+82</option>
										</select>
                                           </div>
                                           <div class="space">&nbsp;</div>
                                           <div class="col-9">
                                              <input type="text" name="" class="tel numberOnly" id="mobilePhone" maxlength="11"/>
                                           </div>
                                       </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!--/ 기본정보 -->

                        <!-- 선택정보 -->
                        <h4 class="tit mgt40">선택정보</h4>
                        <table class="table mgt20">
                            <caption>선택정보</caption>
                            <colgroup>
                                <col style="width:16.35%" />
                                <col style="width:34.65%" />
                                <col style="width:16.35%" />
                                <col style="width:34.65%" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td colspan="4" class="txt01">
                                        차량사고 발생시, 긴급SOS를 긴급연락처로 전송할 정보를 입력해주세요.
                                        <br /> 해당정보는 회원정보 수정페이지에서 변경하실 수 있습니다.
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">전화번호</th>
                                   <td>
                                       <div class="division">
                                           <div class="col-3">
                                             <select name="" id="phoneCtyCode">
											<option value="+82">+82</option>
											</select>
                                           </div>
                                           <div class="space">&nbsp;</div>
                                           <div class="col-9">
                                              <input type="text" name="" class="tel numberOnly" id="phone" maxlength="11"/>
                                           </div>
                                       </div>
                                   </td>
                                   
                                    <th scope="row">성별</th>
                                    <td>
                                        <input type="radio" id="sex01" name="sex" checked="checked" value="M" />
                                        <label for="sex01">남자</label> &nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="radio" id="sex02" name="sex" value="F" />
                                        <label for="sex02">여자</label>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">생년월일</th>
                                    <td>
                                        <input type="text" name="" class="numberOnly" id="birth" /> ex) 20180101
                                    </td>
                                    <th scope="row">혈액형</th>
                                    <td>
                                        <select id="blood" style="width:150px">
											<option value="A">A형</option>
											<option value="B">B형</option>
											<option value="O">O형</option>
											<option value="AB">AB형</option>
										</select>
										<select id="bloodRh" style="width:150px">
											<option value="rh+">rh+</option>
											<option value="rh-">rh-</option>
										</select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!--/ 선택정보 -->

                        <!-- 하단 버튼 -->
                        <div class="btn-bottom ">
                            <button class="btn btn02" id="prev">이전</button>
                            <button class="btn btn03" id="next">계속</button>
                        </div>
                        <!--/ 하단 버튼 -->
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->