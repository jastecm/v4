<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script>
	$(document).ready(function(){
		//전체 동의 클릭
		$('#checkall').on('click',function(){
			if($(this).is(":checked")){
				/* 필수 */
				$('#serviceAccepted').prop('checked',true);
				$('#privateAccepted').prop('checked',true);
				$('#locationAccepted').prop('checked',true);
				/* 필수 */
				/* 선택 */
				$('#marketingAccepted').prop('checked',true);
				/* 선택 */
			}else{
				$('#serviceAccepted').prop('checked',false);
				$('#privateAccepted').prop('checked',false);
				$('#locationAccepted').prop('checked',false);
				$('#marketingAccepted').prop('checked',false);
			}
		});
		
		$('#prev').on('click',function(){
			window.history.back();
		});
		
		$('#next').on('click',function(){
			//필수 체크
			if(!$('#serviceAccepted').is(":checked") && (alert('서비스 이용약관 동의는 필수 입니다.')||1) && $('#serviceAccepted').focus()) return false;
			if(!$('#privateAccepted').is(":checked") && (alert('개인정보 취급방침 동의는 필수 입니다.')||1) && $('#privateAccepted').focus()) return false;
			if(!$('#locationAccepted').is(":checked") && (alert('위치정보 수집은 동의는 필수 입니다.')||1) && $('#locationAccepted').focus()) return false;
			
			//마케팅선택 동의
			$('#marketingAccepted').is(":checked")?$('#frm input[name="marketingAccepted"]').val("1"):$('#frm input[name="marketingAccepted"]').val("0");
			
			$V4.move("/inviteRegist/step2",$('#frm'));
		});
		
	});
</script>
<form id="frm" name="frm">
	<input type="hidden" name="accountId" value="${accountId}"/>
	<input type="hidden" name="corpKey" value="${corpKey}"/>
	
	<input type="hidden" name="serviceAccepted" value="1"/>
	<input type="hidden" name="privateAccepted" value="1"/>
	<input type="hidden" name="locationAccepted" value="1"/>
	<input type="hidden" name="marketingAccepted" value=""/>
</form>
 <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>회원가입</h2>
                    <span></span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="join-page">
                    <!-- 회원가입 STEP -->
                    <div class="join-step">
                        <ol>
                            <li class="step02 active">
                                <span>STEP01</span>
                                <strong>약관동의</strong>
                            </li>
                            <li class="step03">
                                <span>STEP02</span>
                                <strong>지역선택</strong>
                            </li>
                            <li class="step04">
                                <span>STEP03</span>
                                <strong>회원정보입력</strong>
                            </li>
                            <li class="step05">
                                <span>STEP04</span>
                                <strong>회원정보확인</strong>
                            </li>
                            <li class="step07">
                               	<span>STEP05</span>
                               	<strong>가입완료</strong>
                           	</li>
                        </ol>
                    </div>
                    <!--/ 회원가입 STEP -->

                    <div class="terms-agree">
                        <strong>서비스 이용약관, 개입정보 취급방침, 위치정보 수집, 마케팅 수신 동의에
							<em>모두 동의</em>합니다.</strong>
                        <div class="checkall">
                            <input type="checkbox" id="checkall" />
                            <label for="checkall">전체동의</label>
                        </div>
                    </div>

                    <div class="terms-list">
                        <!-- 01. 서비스 이용약관 -->
                        <div class="top">
                            <h3>01. 서비스 이용약관
                                <span>(필수)</span>
                            </h3>
                            <div class="check">
                                <input type="checkbox" name="serviceAccepted" id="serviceAccepted" />
                                <label for="serviceAccepted">동의</label>
                            </div>
                        </div>
                        <div class="terms-box">

                            <h4 class="mgt0">제1조 [목적]</h4>
                            <p>이 약관은 ㈜자스텍엠(이하 “회사”라고 함)가 제공하는 "ViewCAR Pro" 어플리케이션 서비스 이용조건 및 절차 등과 관련하여 회사와 회원간의 권리, 의무 등 기타 필요한 사항을 규정함을 목적으로 합니다.</p>

                            <h4>제2조 [약관의 효력 및 변경]</h4>
                            <ol>
                                <li>
                                    <span class="num">①</span> 본 약관은 서비스를 이용하고자 본 약관에 동의한 회원에 대하여 그 효력을 발생하며, 회원이 쉽게 알 수 있도록 http://www.viewcar.net 초기화면 및 어플리케이션에 게시합니다.
                                </li>
                                <li>
                                    <span class="num">②</span> 본 약관에 대한 회원의 동의는 스마트폰 등의 모바일 단말기(이하 “스마트폰 등”이라고 함)의 화면에 게시되거나 기타의 방법으로 고지된 본 약관을 회원이 확인하고, 동의 요구창에 동의하는 등의 방법으로 합니다.
                                </li>
                                <li>
                                    <span class="num">③</span> 회사는 "약관의규제에관한법률", "정보통신망이용촉진및정보보호등에관한법률(이하 "정보통신망법")" 등 관련법을 위배하지 않는 범위에서 이 약관을 개정할 수 있습니다.
                                </li>
                                <li>
                                    <span class="num">④</span> 회사가 약관을 개정할 경우에는 적용일자 및 개정사유를 명시하여 현행약관과 함께 제1항의 방식에 따라 그 개정약관의 적용일자 30일 전부터 적용일자 전일까지 공지합니다. 다만, 회원에게 불리한 약관의 개정의 경우에는 공지 외에 일정기간 서비스 내 전자우편, 전자쪽지, 로그인시 동의창 등의 전자적 수단을 통해 따로 명확히 통지하도록 합니다.
                                </li>
                                <li>
                                    <span class="num">⑤</span> 회사가 전항에 따라 개정약관을 공지 또는 통지하면서 회원에게 30일 기간 내에 의사표시를 하지 않으면 의사표시가 표명된 것으로 본다는 뜻을 명확하게 공지 또는 통지하였음에도 회원이 명시적으로 거부의 의사표시를 하지 아니한 경우 회원이 개정약관에 동의한 것으로 봅니다.
                                </li>
                                <li>
                                    <span class="num">⑥</span> 회원이 개정약관의 적용에 동의하지 않는 경우 회사는 개정 약관의 내용을 적용할 수 없으며, 이 경우 회원은 이용계약을 해지할 수 있습니다. 다만, 기존 약관을 적용할 수 없는 특별한 사정이 있는 경우에는 회사는 이용계약을 해지할 수 있습니다.
                                </li>
                            </ol>

                            <h4>제3조[용어의 정의]</h4>
                            <p>이 약관에서 사용하는 용어의 정의는 다음과 같으며 본 약관에서 정의되지 않은 용어는 관계법령 및 회사에서 정하는 바에 의합니다.</p>
                            <ol>
                                <li>
                                    <span class="num">①</span> ‘ViewCAR Pro 서비스’라 함은 OBD(On Board Diagnostics)단말기와연계된 ViewCAR PRO어플리케이션을이용하여회원차량의데이터를수집하고 ViewCAR PRO서비스운영서버에전송, 분석하여운행정보/ 주차위치/ 차량진단/ 마이카클리닉/ 소모품관리등의정보를제공하거나긴급SOS 등외부통신연결을제공하는서비스를말합니다.
                                </li>
                                <li>
                                    <span class="num">②</span> ‘OBD단말기’ 라함은 차량에서 제공 가능한 다양한 차량진단정보 및 운행정보를 저장하여 회원에게 전달할 수 있는 장치를 의미합니다.
                                </li>
                                <li>
                                    <span class="num">③</span> ‘ViewCAR Pro 어플리케이션’ 라 함은 사용자가 OBD단말기와 통신을 목적으로 하거나, 사용자의 차량운행의 효율성 향상 및 도어개폐, 내차 주치의 등의 서비스 이용을 목적으로 회사에서 개발한 어플리케이션을 의미합니다.
                                </li>
                                <li>
                                    <span class="num">④</span> ‘회원’이라 함은, 회사가 제공하는 OBD단말기를 구입 또는 취득하여 회사에서 배포한 어플리케이션을 휴대폰에 설치하여 OBD와 어플리케이션과 회사의 서비스 운영서버간의 통신에 의해 제공되는 서비스를 이용하는 고객을 의미합니다.
                                </li>
                                <li>
                                    <span class="num">⑤</span> ‘회원정보’라 함은 회원이 서비스 이용 시 필요한 휴대전화번호, 성명, 차량정보 등 회원의 기본적인 정보를 의미합니다.
                                </li>
                                <li>
                                    <span class="num">⑥</span> ‘차량정보’라 함은 OBD단말기를 통하여 수집되는 정보와 회원이 서비스 이용을 위하여 입력하는 차량정보를 의미합니다.
                                </li>
                                <li>
                                    <span class="num">⑦</span> ‘장착점’이라 함은 OBD단말기를 유, 무상으로 판매 또는 대여한 자동차 정비업소를 말합니다.
                                </li>
                                <li>
                                    <span class="num">⑧</span> ‘제휴사’라 함은 ViewCAR Pro 서비스 제공에 직, 간접적으로 참여하는 회사로서 ‘회사’와 정식적인 계약관계에 있는 정비업소, 손해보험사 등을 의미합니다.
                                </li>
                                <li>
                                    <span class="num">⑨</span> ‘사이트’라 함은 회원의 서비스 이용에 도움을 줄 수 있도록 회사가 운영하는 웹사이트(www.viewcar.net)를 말합니다.
                                </li>
                                <li>
                                    <span class="num">⑩</span> ‘페어링’이라 함은 스마트폰과 OBD단말기에 내장된 블루투스 기능을 활용하여 2기기간의 네트워크를 연결해주어 정상적인 서비스 이용을 가능하게 해주는 행위를 말합니다.
                                </li>
                                <li>
                                    <span class="num">⑪</span> ‘제품번호’라 함은 OBD단말기 장치에 저장된 제품번호로써, 비정상적인 OBD단말기 접근을 방지하기 위한 일련번호를 의미합니다.
                                </li>
                                <li>
                                    <span class="num">⑫</span> ‘장착점코드’라 함은 ViewCAR Pro 어플리케이션을 통해 정비이력, 내차 주치의 서비스 등을 이용할 수 있도록 하는 장착점의 고유번호를 의미하는 것으로 회원은 정착점코드 입력을 통해 장착점에 회원정보 및 차량정보, 차량 정비이력, 최근 운행연비, 주행 거리 등의 제공을 동의한 것으로 간주합니다.
                                </li>
                                <li>
                                    <span class="num">⑬</span> ‘VID비밀번호’라 함은 OBD단말기 장치와 ViewCAR Pro어플리케이션간 보안을 위해 사용자가 지정한 비밀번호로써, 비정상적인 OBD 접근을 방지하기 위한 비밀번호를 말합니다.
                                </li>
                                <li>
                                    <span class="num">⑭</span> ‘에코드라이브 지수’라 함은, 급가속, 급감속, 운행거리, 운행시간, 야간운전 등을 토대로 사용자의 운행습관이 얼마나 안전한지를 표준지수로 표현한 것을 말합니다.
                                </li>
                            </ol>

                            <h4>제4조 [서비스 제공 및 변경]</h4>
                            <ol>
                                <li>
                                    <span class="num">①</span>서비스는 연중무휴, 1일 24시간 제공함을 원칙으로 합니다.
                                </li>
                                <li>
                                    <span class="num">②</span>서비스는 ViewCAR Pro 어플리케이션이 정상적으로 설치될 수 있는 스마트폰 등을 이용하는 회원에 한하여 이용할 수 있으며, 제공되는 OBD의 종류 및 스마트폰 등의 운영체계, 성능, 회원차량의 종류 등에 따라 회원에 대한 서비스 제공 및 범위를 제한할 수 있습니다.
                                </li>
                                <li>
                                    <span class="num">③</span>설치된 ViewCAR Pro 어플케이션의 관리 부주의, 휴대기기의 작동 오류, 범죄행위에 이용 또는 부정한 사용 등으로 인한 손해에 대해서는 회사는 책임을 지지 않습니다.
                                </li>
                                <li>
                                    <span class="num">④</span>회사는 트래픽 폭주, 포화 및 기타 사유로 서비스 사용을 위해 접속하는 회원에게 접속방식의 변경이나 재접속을 요구할 수 있습니다.
                                </li>
                                <li>
                                    <span class="num">⑤</span> 회사는 아래 각 호에 명시된 서비스의 종류는 회원과 체결하는 약정에 따라 차이가 있을 수 있습니다. 또한, 운행중인 모든 차량을 지원하지는 못하고, 기능지원이 가능한 차량은 공지사항 등을 통해 별도 공지됩니다.
                                    <ol>
                                        <li>
                                            <span class="num">가.</span>운행 정보
                                        </li>
                                        <li>
                                            <span class="num">나.</span>통합랭킹
                                        </li>
                                        <li>
                                            <span class="num">다.</span>(기업회원인경우:배차관리)
                                        </li>
                                        <li>
                                            <span class="num">라.</span>소모품관리
                                        </li>
                                        <li>
                                            <span class="num">마.</span>차량진단
                                        </li>
                                        <li>
                                            <span class="num">바.</span>차량위치
                                        </li>
                                        <li>
                                            <span class="num">사.</span>긴급SOS (구현예정)
                                        </li>
                                        <li>
                                            <span class="num">아.</span>설정
                                        </li>
                                        <li>
                                            <span class="num">자.</span>업데이트
                                        </li>
                                        <li>
                                            <span class="num">차.</span>기타 회사가 추가 개발하거나 다른 회사와의 제휴계약을 통해 회원에게 제공하는 일체의 서비스
                                        </li>
                                    </ol>
                                </li>
                                <li>
                                    <span class="num">⑥</span>회사는 원활한 서비스 제공 및 회원의 편의를 위하여 회원의 차량정보와 운행기록, 정비이력 등을 장착점 또는 제휴사에 제공할 수 있습니다. 다만, 개인정보가 포함된 경우에는 정보통신망이용촉진등에관한법률, 정보보호법 등에 정해진 절차에 따라 제휴사에 제공할 수 있습니다.
                                </li>
                                <li>
                                    <span class="num">⑦</span>회사는 서비스의 내용 및 방법 기타 일체의 사항을 회사의 상황에 따라 변경할 수 있습니다. 이 경우는 회사가 변경된 서비스의 제공일 이전에 변경된 서비스의 내용 및 제공 일자를 명시하는 등 적절한 방법으로 공지합니다.
                                </li>
                                <li>
                                    <span class="num">⑧</span>회사는 회원이 스마트폰 등에 내장된 소프트웨어나 OBD단말기를 임의로 조작하거나 OBD단말기로부터 출력되는 정보를 회사가 지정하지 않은 단말기에 접속하여 데이터를 유출 또는 기록하는 경우 서비스 제공을 제한할 수 있으며, 이로 인하여 회원에게 발생한 모든 손해에 대하여 책임이 없습니다.
                                </li>
                                <li>
                                    <span class="num">⑨</span> 회사는 회원에게 보다 전문적이고 다양한 서비스를 제공하기 위해 외부전문 사업자와 제휴협정을 맺고 공동으로 서비스를 제공할 수 있습니다.
                                    <ol>
                                        <li>
                                            <span class="num">가.</span>보험서비스 등 전문적인 지식과 경험이 요구되는 경우 외부전문사업자와 공동으로 서비스를 제공하며 해당전문사업의 상호는 서비스내의 해당 서비스페이지에 명시합니다.
                                        </li>
                                        <li>
                                            <span class="num">나.</span>이와 같은 외부전문사업자와의 공동서비스를 제공함에 있어 회원의 성명, 연락처 등 공동서비스를 위해 필요한 최소한의 정보를 공유할 수 있으며, 공유되는 정보는 엄격히 보호 관리됩니다.
                                        </li>
                                    </ol>
                                </li>
                            </ol>
                            <h4>제5조 [서비스 이용 거절과 승낙의 제한]</h4>
                            <ol>
                                <li>
                                    <span class="num">①</span>회사는 다음의 각호에 해당하는 서비스의 신청에 대하여 승낙을 거절할 수 있습니다.
                                    <ol>
                                        <li>
                                            <span class="num">가.</span>기술상 서비스 제공이 불가한 경우
                                        </li>
                                        <li>
                                            <span class="num">나.</span>회사가 제공하지 않은 OBD단말기 상의 문제에 대하여 회사에 서비스 제공을 요청하는 경우
                                        </li>
                                        <li>
                                            <span class="num">다.</span>회원이 본인의 정보가 아닌 타인 명의 또는 허위 정보를 입력하여 신청한 경우
                                        </li>
                                        <li>
                                            <span class="num">라.</span>악성프로그램이나 시스템 취약점을 악용하는 등 부정한 방법을 서비스에 사용한 경우
                                        </li>
                                        <li>
                                            <span class="num">마.</span>사회의 안녕질서 또는 미풍양속을 저해하거나 저해할 목적으로 서비스를 신청한 경우
                                        </li>
                                        <li>
                                            <span class="num">바.</span>기타 회사가 별도로 정한 이용신청 기준 등에 부합되지 않는 경우
                                        </li>
                                    </ol>
                                </li>
                                <li>
                                    <span class="num">②</span>회사는 다음 각호의 사유가 있는 경우 이용신청에 대한 승낙을 유보할 수 있습니다.
                                    <ol>
                                        <li>
                                            <span class="num">가.</span>회사에 의하여 이용계약이 해지된 날로부터 30일 이내에 재이용을 신청한 경우
                                        </li>
                                        <li>
                                            <span class="num">나.</span>서비스 설비에 여유가 없거나 기술상 서비스 제공에 지장이 있는 경우
                                        </li>
                                        <li>
                                            <span class="num">다.</span>기타 본 약관에 위배되거나 위법 또는 부당한 이용신청임이 확인된 경우
                                        </li>
                                        <li>
                                            <span class="num">라.</span>회사가 합리적인 판단에 의하여 필요하다고 인정하는 경우
                                        </li>
                                    </ol>
                                </li>
                            </ol>
                            <h4>제6조 [서비스의 중단]</h4>
                            <ol>
                                <li>
                                    <span class="num">①</span>다음 각호의 경우 회사는 서비스 제공을 일부 또는 전부 중단할 수 있습니다.
                                    <ol>
                                        <li>
                                            <span class="num">가.</span>회사가 긴급한 시스템 점검, 증설, 교체, 시설의 보수 또는 공사를 하기 위하여 부득이한 경우
                                        </li>
                                        <li>
                                            <span class="num">나.</span>새로운 서비스 시스템으로 교체 등을 위하여 회사가 필요하다고 판단되는 경우
                                        </li>
                                        <li>
                                            <span class="num">다.</span>시스템 또는 기타 서비스 및 네트워크 장애, 서비스 이용의 폭주 등으로 정상적인 서비스 제공이 불가능할 경우
                                        </li>
                                        <li>
                                            <span class="num">라.</span>회사의 경영상 사정으로 인하여 본 서비스의 종료를 결정한 경우
                                        </li>
                                        <li>
                                            <span class="num">마.</span>천재지변, 국가비상사태, 정전 등 회사가 통제할 수 없는 불가항력적 사유로 인한 경우
                                        </li>
                                    </ol>
                                    <li>
                                        <span class="num">②</span>제 1항에 의한 서비스 중단의 경우 회사는 공지사항이나 전자우편 등의 방법으로 중단 사실을 회원에게 사전 통지합니다. 다만, 회사가 미리 예측할 수 없는 사정에 의한 서비스 중단의 경우에는 사전통지 없이 서비스를 중단할 수 있습니다.
                                    </li>
                                    <li>
                                        <span class="num">③</span>제 1항의 서비스 중단으로 인하여 발생된 회원의 손해에 대해서는 회사는 고의 또는 중대한 과실이 없는 한 책임을 부담하지 않습니다.
                                    </li>
                            </ol>
                            <h4>제7조 [이용 제한 및 해지]</h4>
                            <ol>
                                <li>
                                    <span class="num">①</span>회원은 언제든지 본 서비스 또는 회사가 정한 방법으로 본 서비스 이용계약에 대한 해지신청을 할 수 있고, 회사는 이를 지체 없이 처리합니다.
                                </li>
                                <li>
                                    <span class="num">②</span>회사는 다음 각 항에 해당하는 결제 승인신청에 대해서는 승인을 하지 않거나 취소할 수 있으며, 회사는 필요하다고 인정되는 경우 다음 각호에 해당하는 회원의 서비스 이용을 제한하거나 이용계약을 해지할 수 있습니다. 또한 동 위반행위로 인해 발생하는 모든 책임은 해당 회원에게 있습니다.
                                    <ol>
                                        <li>
                                            <span class="num">가.</span>기존에 결제한 요금을 납부하지 않거나, 납부자 확인이 불가능한 경우
                                        </li>
                                        <li>
                                            <span class="num">나.</span>만 20세 미만의 미성년자가 법정대리인 동의 없이 결제한 경우
                                        </li>
                                        <li>
                                            <span class="num">다.</span>회원의 귀책사유로 결제승인이 불가능하다고 판단되는 경우
                                        </li>
                                        <li>
                                            <span class="num">라.</span>타인의 신용정보, 결제정보를 도용하거나 부정한 행위로써 거래하는 경우
                                        </li>
                                        <li>
                                            <span class="num">마.</span>결제된 서비스에 대한 권리를 회사의 승인 없이 제3자에게 양도, 매매하는 경우
                                        </li>
                                        <li>
                                            <span class="num">바.</span>결제수단을 악용하여 불법적인 행위를 하는 경우
                                        </li>
                                        <li>
                                            <span class="num">사.</span>해킹 등으로 회사의 시스템에 피해를 주는 행위를 하는 경우
                                        </li>
                                        <li>
                                            <span class="num">아.</span>서비스 가입 시 등록한 개인정보의 전부 또는 일부가 허위임이 밝혀진 경우
                                        </li>
                                        <li>
                                            <span class="num">자.</span>타인의 차량에 설치되어 관리되던 OBD 단말기를 사용하는 경우
                                        </li>
                                        <li>
                                            <span class="num">차.</span>회사가 지정하지 않은 단말기에 접속하여 데이터를 유출 또는 기록하는 경우
                                        </li>
                                        <li>
                                            <span class="num">카.</span>이용약관 및 관련 법령에 위배되는 행위를 하는 경우
                                        </li>
                                    </ol>
                                </li>

                                <li>
                                    <span class="num">③</span>회원이 이용요금이 연체된 경우 회사는 동 회원의 서비스 이용을 일부 또는 전부를 제한하거나 이용계약을 해지할 수 있습니다.
                                </li>
                                <li>
                                    <span class="num">④</span>회사는 회원이 ViewCAR Pro 서비스를 1년이상 이용하지 않은 경우 이용계약을 해지할 수 있습니다.
                                </li>
                                <li>
                                    <span class="num">⑤</span>본 조에 따라 계약이 해지된 경우 회원은 손해에 대한 배상 또는 손실에 대한 배상을 회사에 청구할 수 없습니다.
                                </li>
                            </ol>

                            <h4>제8조 [이용자에 대한 통지]</h4>
                            <ol>
                                <li>
                                    <span class="num">①</span>회사가 특정 이용자에 대한 통지를 하는 경우 가입신청 시 이용자가 가입한 연락수단을 활용할 수 있습니다.
                                </li>
                                <li>
                                    <span class="num">②</span>회사가 불특정 다수 이용자에 대한 통지를 하는 경우 7일 이상 서비스에 게시함으로써 개별통지에 갈음할 수 있습니다.
                                </li>
                            </ol>

                            <h4>제9조 [서비스 이용요금]</h4>
                            <ol>
                                <li>
                                    <span class="num">①</span>FMS(Fleet Management Service) 개인/법인이용회원
                                    <ol>
                                        <li>
                                            <span class="num">가.</span>iOS 운영 디바이스 :
                                            <table>
                                                <thead>
                                                    <tr>
                                                        <th scope="col">구 분</th>
                                                        <th scope="col">금 액 (USD)</th>
                                                        <th scope="col">비 고</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">1개월 이용권</th>
                                                        <td>4.99</td>
                                                        <td>von-S31 단말기를 이용한 ViewCAR Pro서비스의 1개월 이용권입니다.</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">1년 이용권 (20%할인) </th>
                                                        <td>47.99</td>
                                                        <td>von-S31 단말기를 이용한 ViewCAR Pro서비스의 1년 이용권입니다.</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">3개월 이용권 (5% 할인)</th>
                                                        <td>13.99</td>
                                                        <td>von-S31 단말기를 이용한 ViewCAR Pro서비스의 3개월 이용권입니다.</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">월 정기 이용권</th>
                                                        <td>4.99</td>
                                                        <td>von-S31 단말기를 이용한 ViewCAR Pro서비스의 월 정기 이용권입니다.</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <ol>
                                                <li>
                                                    <span class="num">i.</span> 서비스 구독 비용은 iTunes 계정에서 구매 확인에서 결제됩니다.
                                                </li>
                                                <li>
                                                    <span class="num">ii.</span> ViewCAR Pro” 서비스는 auto-renew 해제를 구독 기간 끝나기 24시간 전에 하지 않는 한 auto-renew가 됩니다.
                                                </li>
                                                <li>
                                                    <span class="num">iii.</span> 재구독에 대한 결제는 계정에서 현 서비스의 구독 기간이 만료되기 24시간 전 청구됩니다. 그리고 결제된 금액은 재구독으로 반영됩니다.
                                                </li>
                                                <li>
                                                    <span class="num">iv.</span> 구독은 사용자에 의해 관리가 되고 자동 재구독은 결제 후 계정 설정에서 해제할 수 있습니다.
                                                </li>
                                                <li>
                                                    <span class="num">v.</span> 무료 체험 기간 중 제공된 서비스는 사용자가 구독을 결제하였을 시에는 몰수됩니다.
                                                </li>
                                            </ol>
                                        </li>

                                        <li>
                                            <span class="num">나.</span>안드로이드 운영 디바이스 :
                                            <table>
                                                <thead>
                                                    <tr>
                                                        <th scope="col">구 분</th>
                                                        <th scope="col">금 액 (KRW)</th>
                                                        <th scope="col">비 고</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">1개월 이용권</th>
                                                        <td>5,000</td>
                                                        <td>von-S31 단말기를 이용한 ViewCAR Pro서비스의 1개월 이용권입니다.</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">1년 이용권 (20%할인) </th>
                                                        <td>48,000</td>
                                                        <td>von-S31 단말기를 이용한 ViewCAR Pro서비스의 1년 이용권입니다.</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">3개월 이용권 (5% 할인)</th>
                                                        <td>14,250</td>
                                                        <td>von-S31 단말기를 이용한 ViewCAR Pro서비스의 3개월 이용권입니다.</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">월 정기 이용권</th>
                                                        <td>5,000</td>
                                                        <td>von-S31 단말기를 이용한 ViewCAR Pro서비스의 월 정기 이용권입니다.</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <ol>
                                                <li>
                                                    <span class="num">i.</span>서비스 구독 비용은 Google Play store 계정에서 구매 확인에서 결제됩니다.
                                                </li>
                                                <li>
                                                    <span class="num">ii.</span>ViewCAR Pro” 서비스는 auto-renew 해제를 구독 기간 끝나기 24시간 전에 하지 않는 한 auto-renew가 됩니다.
                                                </li>
                                                <li>
                                                    <span class="num">iii.</span>재구독에 대한 결제는 계정에서 현 서비스의 구독 기간이 만료되기 24시간 전 청구됩니다. 그리고 결제된 금액은 재구독으로 반영됩니다.
                                                </li>
                                                <li>
                                                    <span class="num">iv.</span>구독은 사용자에 의해 관리가 되고 자동 재구독은 결제 후 계정 설정에서 해제할 수 있습니다.
                                                </li>
                                                <li>
                                                    <span class="num">v.</span>무료 체험 기간 중 제공된 서비스는 사용자가 구독을 결제하였을 시에는 몰수됩니다.
                                                </li>
                                            </ol>
                                        </li>
                                    </ol>
                                </li>
                                <li>
                                    <span class="num">②</span>보험용서비스개인이용회원
                                    <ol>
                                        <li>
                                            <span class="num">가.</span>iOS 운영 디바이스 :
                                            <table>
                                                <thead>
                                                    <tr>
                                                        <th scope="col">구 분</th>
                                                        <th scope="col">금 액 (USD)</th>
                                                        <th scope="col">비 고</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">1개월 이용권</th>
                                                        <td>1.99</td>
                                                        <td>von-S31 단말기를 이용한 ViewCAR Pro서비스의 1개월 이용권입니다.</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">1년 이용권 (20%할인) </th>
                                                        <td>19.10 </td>
                                                        <td>von-S31 단말기를 이용한 ViewCAR Pro서비스의 1년 이용권입니다.</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">3개월 이용권 (5% 할인)</th>
                                                        <td>5.57</td>
                                                        <td>von-S31 단말기를 이용한 ViewCAR Pro서비스의 3개월 이용권입니다.</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">월 정기 이용권</th>
                                                        <td>1.99</td>
                                                        <td>von-S31 단말기를 이용한 ViewCAR Pro서비스의 월 정기 이용권입니다.</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <ol>
                                                <li>
                                                    <span class="num">i.</span>서비스 구독 비용은 iTunes 계정에서 구매 확인에서 결제됩니다.
                                                </li>
                                                <li>
                                                    <span class="num">ii.</span>ViewCAR Pro” 서비스는 auto-renew 해제를 구독 기간 끝나기 24시간 전에 하지 않는 한 auto-renew가 됩니다.
                                                </li>
                                                <li>
                                                    <span class="num">iii.</span>재구독에 대한 결제는 계정에서 현 서비스의 구독 기간이 만료되기 24시간 전 청구됩니다. 그리고 결제된 금액은 재구독으로 반영됩니다.
                                                </li>
                                                <li>
                                                    <span class="num">iv.</span>구독은 사용자에 의해 관리가 되고 자동 재구독은 결제 후 계정 설정에서 해제할 수 있습니다.
                                                </li>
                                                <li>
                                                    <span class="num">v.</span>무료 체험 기간 중 제공된 서비스는 사용자가 구독을 결제하였을 시에는 몰수됩니다.
                                                </li>
                                            </ol>
                                        </li>

                                        <li>
                                            <span class="num">나.</span>안드로이드 운영 디바이스 :
                                            <table>
                                                <thead>
                                                    <tr>
                                                        <th scope="col">구 분</th>
                                                        <th scope="col">금 액 (KRW)</th>
                                                        <th scope="col">비 고</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">1개월 이용권</th>
                                                        <td>2,000</td>
                                                        <td>von-S31 단말기를 이용한 ViewCAR Pro서비스의 1개월 이용권입니다.</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">1년 이용권 (20%할인) </th>
                                                        <td>19,200</td>
                                                        <td>von-S31 단말기를 이용한 ViewCAR Pro서비스의 1년 이용권입니다.</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">3개월 이용권 (5% 할인)</th>
                                                        <td>5,700</td>
                                                        <td>von-S31 단말기를 이용한 ViewCAR Pro서비스의 3개월 이용권입니다.</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">월 정기 이용권</th>
                                                        <td>2,000</td>
                                                        <td>von-S31 단말기를 이용한 ViewCAR Pro서비스의 월 정기 이용권입니다.</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <ol>
                                                <li>
                                                    <span class="num">i.</span>서비스 구독 비용은 Google Play store 계정에서 구매 확인에서 결제됩니다.
                                                </li>
                                                <li>
                                                    <span class="num">ii.</span>ViewCAR Pro” 서비스는 auto-renew 해제를 구독 기간 끝나기 24시간 전에 하지 않는 한 auto-renew가 됩니다.
                                                </li>
                                                <li>
                                                    <span class="num">iii.</span>재구독에 대한 결제는 계정에서 현 서비스의 구독 기간이 만료되기 24시간 전 청구됩니다. 그리고 결제된 금액은 재구독으로 반영됩니다.
                                                </li>
                                                <li>
                                                    <span class="num">iv.</span>구독은 사용자에 의해 관리가 되고 자동 재구독은 결제 후 계정 설정에서 해제할 수 있습니다.
                                                </li>
                                                <li>
                                                    <span class="num">v.</span>무료 체험 기간 중 제공된 서비스는 사용자가 구독을 결제하였을 시에는 몰수됩니다.
                                                </li>
                                            </ol>
                                        </li>
                                    </ol>
                                </li>

                                <li>
                                    <span class="num">③</span>장착점 이용회원 :제3자 대납 또는 별도 장착점을 통해 이용권구매를 할수있습니다.
                                </li>
                                <li>
                                    <span class="num">④</span>제휴사 이용회원 :제휴사별 계약에 따른 별도 요금 적용
                                </li>
                            </ol>

                            <h4>제10조 [정보의 제공 및 광고의 게재]</h4>
                            <ol>
                                <li>
                                    <span class="num">①</span>회사는 회원이 서비스 이용 중 필요하다고 인정되는 다양한 정보를 서비스 내 공지사항, 서비스 화면, 서비스 내 메시지, 전자우편 등의 방법으로 회원에게 제공할 수 있습니다. 다만, 회원은 관련법규에 따라 회사가 발송하는 SMS나 이메일에 대해 언제든지 수신 거절을 할 수 있습니다.</li>
                                <li>
                                    <span class="num">②</span>회사는 서비스의 운영과 관련하여 서비스 화면, 서비스 내 메시지, 홈페이지 등에 광고를 게재할 수 있습니다.</li>
                            </ol>

                            <h4>제11조 [손해배상 및 면책조항]</h4>
                            <ol>
                                <li>
                                    <span class="num">①</span>회사는 다음 각호의 사유로 인하여 발생한 손해에 대해서는 그 책임을 지지 않습니다.
                                    <ol>
                                        <li>
                                            <span class="num">가.</span>서비스 설비의 보수, 공사, 업그레이드 등을 위하여 서비스의 점검이 불가피한 경우
                                        </li>
                                        <li>
                                            <span class="num">나.</span>국가의 비상사태, 천재지변 또는 이에 준하는 불가항력으로 인해 서비스를 제공할 수 없는 경우
                                        </li>
                                        <li>
                                            <span class="num">다.</span>회원 또는 제3자의 귀책사유로 인하여 데이터 유실 등의 서비스 이용장애가 발생한 경우
                                        </li>
                                        <li>
                                            <span class="num">라.</span>서비스의 관리영역을 벗어난 장애로 서비스 이용이 불가능한 경우
                                        </li>
                                        <li>
                                            <span class="num">마.</span>기타 회사의 귀책사유가 없는 통신서비스 등의 장애로 인한 경우
                                        </li>
                                        <li>
                                            <span class="num">바.</span>설치된 ViewCAR Pro 어플케이션의 관리 부주의, 휴대기기의 작동 오류, 범죄행위에 이용 또는 부정한 사용 등으로 인한 손해가 발생한 경우
                                        </li>
                                        <li>
                                            <span class="num">사.</span>긴급 SOS 서비스 기능은 위급상황에 대한 보조적 통지수단으로 문자발송이 실패한 경우
                                        </li>
                                    </ol>
                                </li>
                                <li>
                                    <span class="num">②</span>회사는 고의 또는 중대한 과실이 없는 한 회원에게 서비스 미 제공으로 인한 책임을 지지 않습니다. 또한, 회원이 서비스 제공으로부터 기대되는 이익을 얻지 못한 것에 대하여는 어떠한 책임도 지지 않습니다.</li>
                            </ol>

                            <h4>제12조 [금지행위]</h4>
                            <ol>
                                <li>
                                    <span class="num">①</span>회원은 다음 각호의 행위를 하여서는 안됩니다.
                                    <ol>
                                        <li>
                                            <span class="num">가.</span>서비스를 이용하여 얻는 정보를 회사의 사전 승낙 없이 복제 유통시키거나 상업적으로 이용하는 행위
                                        </li>
                                        <li>
                                            <span class="num">나.</span>회사의 저작권 등의 권리를 침해하는 행위
                                        </li>
                                        <li>
                                            <span class="num">다.</span>서비스를 회사 보안정책에 반하여 이용하는 행위
                                        </li>
                                        <li>
                                            <span class="num">라.</span>서비스와 관련된 설비의 안정적 운영을 방해할 수 있는 정보를 전송하는 행위를 하거나, 기타 서비스의 운영을 고의적으로 방해하는 행위
                                        </li>
                                    </ol>
                                </li>
                                <li>
                                    <span class="num">②</span>회원은 관계법령, 본 약관의 규정, 이용안내 및 서비스의 공지사항, 회사가 통지한 사항 등을 준수하여야 하며, 기타 회사의 업무에 방해되는 행위를 해서는 안됩니다.
                                </li>
                                <li>
                                    <span class="num">③</span>회원의 본 조 위반행위가 있을 경우 회사는 회원의 현재 또는 장래의 모든 또는 일부 서비스의 이용을 제한하거나 거절할 수 있으며, 서비스 계약을 해지할 수 있습니다. 또한, 회원이 이로 인하여 회사에 손해를 끼쳤을 경우 회사에 대하여 손해배상의무를 집니다.
                                </li>
                            </ol>
                            <h4>제13조 [기타]</h4>
                            <ol>
                                <li>
                                    <span class="num">①</span>본 약관에 명기되지 아니한 사항은 회사가 정한 정책에 따릅니다.
                                </li>
                                <li>
                                    <span class="num">②</span>회원의 개인정보보호에 관한 사항은 회사의 개인정보보호정책 및 개인정보취급방침에 따릅니다.
                                </li>
                                <li>
                                    <span class="num">③</span>기타 회사가 정하는 정책 및 이용약관에 명기되지 않은 사항은 전기통신사업법, 정보통신망이용촉진에관한법률 및 기타 관련 법령의 규정에 따릅니다.
                                </li>
                            </ol>

                            <h4>제14조 [분쟁의 해결]</h4>
                            <ol>
                                <li>
                                    <span class="num">①</span>서비스 이용과 관련하여 회사와 회원 사이에 분쟁이 발생될 경우, 양 당사자는 분쟁의 해결을 위해 성실히 협의합니다.
                                </li>
                                <li>
                                    <span class="num">②</span>본 조 1항의 합의에도 분쟁이 해결되지 아니할 경우 민사소송법상의 관할법원에 소를 제기할 수 있습니다 .
                                </li>
                            </ol>

                            <h4>부칙</h4>
                            <ol>
                                <li>
                                    <span class="num">①</span>공고일자 : 2017년 10월 1일
                                </li>
                                <li>
                                    <span class="num">②</span>시 행 일 : 2017년 11월 1일
                                </li>
                            </ol>
                        </div>
                        <!--/ 01. 서비스 이용약관 -->

                        <!-- 02. 개인정보 취급방침 -->
                        <div class="top">
                            <h3>02. 개인정보 취급방침
                                <span>(필수)</span>
                            </h3>
                            <div class="check">
                                <input type="checkbox" name="privateAccepted" id="privateAccepted" />
                                <label for="privateAccepted">동의</label>
                            </div>
                        </div>
                        <div class="terms-box">
                            <p>
                                ㈜자스텍엠은 회원가입, 서비스 제공을 위해 ‘정보통신망이용촉진및정보보호등에관한법률’에 따라 아래와 같이 개인정보를 수집할 수 있습니다.
                                <br /> 개인정보 파기절차 및 방법, 개인정보의 기술적, 관리적 보호개인정보 관리책임부서 및 연락처 등에 관한 사항은 아래 ‘개인정보취급방침’에 따릅니다.
                            </p>
                            <h4>1. 개인정보의 수집 및 이용목적</h4>
                            <ol>
                                <li>
                                    <span class="num">①</span>서비스 가입/ 변경 /해지 처리, 본인확인 및 가입의사 확인, 고지사항의 전달, 서비스 제공관련 안내, 개인식별, 불량이용자의 부정 이용방지와 비인가 사용방지, 이용관련 문의/ 불만처리, 서비스의 제공
                                </li>
                                <li>
                                    <span class="num">②</span>이용자의 차량진단과 운행에 도움이 되는 서비스(연비, 차량정비이력, 운행정보, 주차위치 확인 등)의 제공, 긴급사태 발생 통지
                                </li>
                                <li>
                                    <span class="num">③</span>신규 서비스 개발 및 서비스 품질/기능 개선, 이벤트 등 광고성 정보 전달, 인구통계학적 특성에 따른 서비스 제공 및 광고 게재, 접속 빈도 파악 또는 회원의 서비스 이용에 대한 통계
                                </li>
                                <li>
                                    <span class="num">④</span>이용자 차량에 대한 주치의 서비스 운영을 위한 이용자가 OBD 단말기 부착 후 코드입력을 통해 지정 장착점(정비업소)에게 필요한 정보조회/입력 수단 제공 및 활용
                                </li>
                                <li>
                                    <span class="num">⑤</span>기타 별도 고지된 제휴사에게 제휴서비스 제공 등 계약의 이행에 필요한 업무의 제공
                                </li>
                                <li>
                                    <span class="num">⑥</span>회사는 회원가입, 상담, 서비스 신청/ 이용 등을 위해 아래와 같은 개인정보를 수집하고 있습니다.
                                    <ol>
                                        <li>
                                            <span class="num">가.</span>성명, 휴대전화(휴대용 단말기)번호, 이메일계정, 주소, 차량ID, 운행기록장치(OBD, On Board Diagnostics) 정보 등 서비스 이용정보
                                        </li>
                                        <li>
                                            <span class="num">나.</span>이용자 상태 및 차량 상태?운행정보 : 제휴사 서비스 이용기록, 스마트폰 위치정보
                                        </li>
                                        <li>
                                            <span class="num">다.</span>서비스 이용기록, 접속 로그, 쿠키, 결제기록
                                        </li>
                                        <li>
                                            <span class="num">라.</span>위 개인정보는 ViewCAR Pro 어플리케이션 서비스 프로그램을 실행 또는 사용함으로써 자동으로 수집될 수 있습니다.
                                        </li>
                                    </ol>
                                </li>
                            </ol>

                            <h4>2. 개인정보의 취급위탁</h4>
                            <p>회사는 서비스 향상을 위해 아래와 같이 개인정보를 위탁하고 있으며, 관계 법령에 따라 위탁계약 시 개인정보가 안전하게 관리될 수 있도록 필요한 사항을 규정하고 있습니다.</p>
                            <p>수탁업체 : ㈜에넥스텔레콤
                                <br /> 위탁업무 내용 : 문자서비스 전송 시스템 운영
                                <br /> 개인정보의 보유 및 이용기간 : 회원탈퇴 시 혹은 위탁계약 종료 시까지</p>

                            <h4>3. 개인정보의 보유 및 이용기간</h4>
                            <p>원칙적으로, 개인정보 수집 및 이용목적이 달성된 후에는 해당 정보를 지체 없이 파기합니다. 단, 관계법령의 규정에 의하여 보존할 필요가 있는 경우 회사는 아래와 같이 관계법령에서 정한 일정한 기간 동안 회원정보를 보관합니다.
                            </p>
                            <ol>
                                <li>
                                    <span class="num">①</span>보존 항목 : 성명, 휴대전화(휴대용 단말기)번호, 이메일계정 , 주소, 차량ID, 운행기록장치(OBD, On Board Diagnostics), 결제기록
                                </li>
                                <li>
                                    <span class="num">②</span>결제기록보존 근거 : 국세청 고시 제2001-4호에 의거 전자세금계산서 사용자에 한함
                                </li>
                                <li>
                                    <span class="num">③</span>보존 기간 : 5년
                                </li>
                                <li>
                                    <span class="num">④</span>계약 또는 청약철회 등에 관한 기록 : 5년 (전자상거래등에서의 소비자보호에 관한 법률)
                                </li>
                                <li>
                                    <span class="num">⑤</span>대금결제 및 재화 등의 공급에 관한 기록 : 5년 (전자상거래등에서의 소비자보호에 관한 법률)
                                </li>
                                <li>
                                    <span class="num">⑥</span>신용정보의 수집/처리 및 이용 등에 관한 기록 : 3년 (신용정보의 이용 및 보호에 관한 법률)
                                </li>
                            </ol>

                            <h4>4. 개인정보의 파기절차 및 방법</h4>
                            <p>회사는 원칙적으로 개인정보 수집 및 이용목적이 달성된 후에는 해당 정보를 지체없이 파기합니다. 파기절차 및 방법은 다음과 같습니다.</p>
                            <p>
                                파기 절차 :
                                <br /> 회원님이 회원가입 등을 위해 입력하신 정보는 목적이 달성된 후 별도의 DB로 옮겨져(종이의 경우 별도의 서류함) 내부 방침 및 기타 관련 법령에 의한 정보보호 사유에 따라(보유 및 이용기간 참조) 일정 기간 저장된 후 파기됩니다.
                                <br /> 별도 DB로 옮겨진 개인정보는 법률에 의한 경우가 아니고서는 보유되는 이외의 다른 목적으로 이용되지 않습니다.
                            </p>
                            <p>
                                파기 방법 :
                                <br /> 전자적 파일형태로 저장된 개인정보는 기록을 재생할 수 없는 기술적 방법을 사용하여 삭제합니다.
                            </p>

                            <h4>5. 개인정보 제공</h4>
                            <p>
                                회사는 이용자의 개인정보를 원칙적으로 외부에 제공하지 않습니다. 다만, 아래의 경우에는 예외로 합니다.
                                <br /> 이용자들이 사전에 동의한 경우
                                <br /> 법령의 규정에 의거하거나, 수사 목적으로 법령에 정해진 절차와 방법에 따라 수사기관의 요구가 있는 경우
                            </p>

                            <h4>6. 수집한 개인정보의 위탁</h4>
                            <p>
                                회사는 고객님의 동의 없이 고객님의 정보를 외부 업체에 위탁하지 않습니다. 향후 그러한 필요가 생길 경우, 위탁 대상자와 위탁 업무 내용에 대해 고객님에게 통지하고 필요한 경우 사전 동의를 받도록 하겠습니다.
                            </p>

                            <h4>7. 이용자 및 법정대리인의 권리와 그 행사방법</h4>
                            <p>
                                이용자 및 법정 대리인은 언제든지 등록되어 있는 자신 혹은 당해 만 14세 미만 아동의 개인정보를 조회하거나 수정할 수 있으며 가입 해지를 요청할 수도 있습니다.
                                <br /> 이용자 혹은 만 14세 미만 아동의 개인정보 조회수정을 위해서는 ‘개인정보변경’(또는 ‘회원정보수정’ 등)을 가입 해지(동의철회)를 위해서는 ‘회원탈퇴’를 클릭하여 본인 확인 절차를 거치신 후 직접 열람, 정정 또는 탈퇴가 가능합니다. 혹은 개인정보관리책임자에게 서면, 전화 또는 이메일로 연락하시면 지체 없이 조치하겠습니다.
                                <br /> 귀하가 개인정보의 오류에 대한 정정을 요청하신 경우에는 정정을 완료하기 전까지 당해 개인정보를 이용 또는 제공하지 않습니다.
                                <br /> 또한 잘못된 개인정보를 제3자에게 이미 제공한 경우에는 정정 처리결과를 제3자에게 지체 없이 통지하여 정정이 이루어지도록 하겠습니다.
                                <br /> ViewCAR Pro는 이용자 혹은 법정 대리인의 요청에 의해 해지 또는 삭제된 개인정보는 ViewCAR Pro가 수집하는 개인정보의 보유 및 이용기간에 명시된 바에 따라 처리하고 그 외의 용도로 열람 또는 이용할 수 없도록 처리 하고 있습니다.
                            </p>

                            <h4>8. 개인정보에 관한 민원서비스</h4>
                            <p>회사는 고객의 개인정보를 보호하고 개인정보와 관련한 불만을 처리하기 위하여 아래와 같이 관련 부서 및 개인정보관리책임자를 지정하고 있습니다.</p>
                            <ol>
                                <li>
                                    <span class="num">①</span>고객서비스담당 부서 : (주)자스텍엠
                                </li>
                                <li>
                                    <span class="num">②</span>개인정보관리책임자 성명 : 정종웅 주임연구원
                                </li>
                                <li>
                                    <span class="num">③</span>전화번호 : (02)452-9610
                                </li>
                                <li>
                                    <span class="num">④</span>이메일 : triplog@jastecm.com
                                </li>
                            </ol>

                            <p>
                                귀하께서는 회사의 서비스를 이용하시며 발생하는 모든 개인정보보호 관련 민원을 개인정보관리책임자 혹은 담당부서로 신고하실 수 있습니다.
                                <br /> 회사는 이용자들의 신고사항에 대해 신속하게 충분한 답변을 드릴 것입니다.
                                <br /> 기타 개인정보침해에 대한 신고나 상담이 필요하신 경우에는 아래 기관에 문의하시기 바랍니다.
                            </p>

                            <ol>
                                <li>
                                    <span class="num">①</span>개인분쟁조정위원회 (www.1336.or.kr/1336)
                                </li>
                                <li>
                                    <span class="num">②</span>정보보호마크인증위원회 (www.eprivacy.or.kr/02-580-0533~4)
                                </li>
                                <li>
                                    <span class="num">③</span>대검찰청 인터넷범죄수사센터 (http://icic.spo.go.kr/02-3480-3600)
                                </li>
                                <li>
                                    <span class="num">④</span>경찰청 사이버테러대응센터 (www.ctrc.go.kr/02-392-0330)
                                </li>
                            </ol>

                            <h4>9. 회원탈퇴 시 개인정보의 보유기간 및 이용기간</h4>
                            <p>
                                “ViewCAR Pro”는 이용자가 당사에서 제시하고 있는 절차와 방법을 통해 가입을 신청한 시점부터 탈퇴절차가 완료될 때까지 개인정보를 보유하고 이용하는 것을 원칙으로 합니다.
                                <br /> 회원의 탈퇴 신청을 처리할 때 소요되는 시간은 90일이며 해당 기간 동안에는 회원이었던 분들의 정보는 보유하고 있으나 사용 하실 수 없으며, 만약 90일 이내에 저희 서비스의 고객정보를 필요로 하신다면 해지 철회를 해드릴 수 있습니다.
                                <br /> 90일 후에는 ‘ViewCAR Pro’에 해당 성명, 휴대전화(휴대용 단말기)번호로 회원가입이 가능하며, 회원가입 시 기존 등록된 정보가 표기되며, 회원정보를 수정한 후 재 가입이 가능하도록 처리합니다.
                                <br /> 탈퇴 후 90일 이후에도 회원정보를 보유하는 이유는 국세청 고시에 의한 정보 보호와 관련된 법률상 5년동안 데이터를 보관해야 하며, 해당 사업장 또는 개인의 정보로 세금계산서를 발행한 경우 발행된 세금계산서의 보유 자체가 문제가 될 소지가 있으므로 정보 자체를 삭제 처리 하지 않음을 유념해 주시기 바랍니다.
                            </p>

                            <h4>10. 고지의 의무</h4>
                            <p>
                                현 “ViewCAR Pro”의 개인정보보호정책은 2017년 10월 1일에 제정되었으며, 정부의 정책 또는 보안기술의 변경에 따라 내용의 추가/삭제 및 수정이 있을 시에는 개정 최소 7일 전부터 홈페이지의 ‘공지사항’을 통하여 고지할 것입니다.
                            </p>

                            <p>개인정보보호정책 시행 일자 : 2017년 11월 1일</p>
                        </div>
                        <!--/ 02. 개인정보 취급방침 -->

                        <!-- 03. 위치정보 수립 -->
                        <div class="top">
                            <h3>03. 위치정보 수립
                                <span>(필수)</span>
                            </h3>
                            <div class="check">
                                <input type="checkbox" name="locationAccepted" id="locationAccepted" />
                                <label for="locationAccepted">동의</label>
                            </div>
                        </div>
                        <div class="terms-box">
                            <strong>제1장 총 칙</strong>
                            <h4>제 1 조 (목적) </h4>
                            <p>본 약관은 회원(ViewCAR Pro 위치기반 서비스 약관에 동의한 자를 말합니다. 이하 “회원”이라고 합니다.)이 주식회사 자스텍엠(이하 “회사”라고 합니다.)가 제공하는 스마트 드라이빙 스코어링 콘텐츠 서비스(이하 “서비스”라고 합니다)를 이용함에 있어 회사와 회원의 권리 의무 및 책임사항을 규정함을 목적으로 합니다.</p>

                            <h4>제 2 조 (이용약관의 효력 및 변경)</h4>
                            <ol>
                                <li>
                                    <span class="num">①</span>본 약관은 서비스를 신청한 고객 또는 개인위치정보주체가 본 약관에 동의하고 회사가 정한소정의 절차에 따라 서비스의 이용자로 등록함으로써 효력이 발생합니다.
                                </li>
                                <li>
                                    <span class="num">②</span>회원이 온라인에서 본 약관의 "동의하기" 버튼을 클릭하였을 경우 본 약관의 내용을 모두 읽고 이를 충분히 이해하였으며, 그 적용에 동의한 것으로 봅니다.
                                </li>
                                <li>
                                    <span class="num">③</span>회사는 위치정보의 보호 및 이용 등에 관한 법률, 콘텐츠산업 진흥법, 전자상거래 등에서의 소비자보호에 관한 법률, 소비자기본법 약관의 규제에 관한 법률 등 관련법령을 위배하지 않는 범위에서 본 약관을 개정할 수 있습니다.
                                </li>
                                <li>
                                    <span class="num">④</span>회사가 약관을 개정할 경우에는 기존약관과 개정약관 및 개정약관의 적용일자와 개정사유를 명시하여 현행약관과 함께 그 적용일자 10일 전부터 적용일 이후 상당한 기간 동안 공지만을 하고, 개정 내용이 회원에게 불리한 경우에는 그 적용일자 30일 전부터 적용일 이후 상당한 기간 동안 각각 이를 서비스 홈페이지에 게시하거나 회원에게 전자적 형태(전자우편,
                                    SMS 등)로 약관 개정 사실을 발송하여 고지합니다.
                                </li>
                                <li>
                                    <span class="num">⑤</span>회사가 전항에 따라 회원에게 통지하면서 공지 또는 공지 고지일로부터 개정약관 시행일 7일후까지 거부의사를 표시하지 아니하면 이용약관에 승인한 것으로 봅니다. 회원이 개정약관에 동의하지 않을 경우 회원은 이용계약을 해지할 수 있습니다.
                                </li>
                            </ol>

                            <h4>제 3 조 (관계법령의 적용)</h4>
                            <p>본 약관은 신의성실의 원칙에 따라 공정하게 적용하며, 본 약관에 명시되지 아니한 사항에 대하여는 관계법령 또는 상관례에 따릅니다.</p>

                            <h4>제 4 조 (서비스의 내용) 회사가 제공하는 서비스는 아래와 같습니다.</h4>
                            <p>
                                서비스 명 서비스 내용
                                <br /> 스마트 드라이빙 뷰카 서비스
                            </p>
                            <ol>
                                <li>
                                    <span class="num">①</span>ViewCAR Pro 스마트폰 어플리케이션 서비스
                                </li>
                                <li>
                                    <span class="num">②</span>차량운전 위험도 분석 및 지도
                                </li>
                                <li>
                                    <span class="num">③</span>차량진단
                                </li>
                                <li>
                                    <span class="num">④</span>E-CALL(사고여부 판단 및 긴급구난 전송)
                                </li>
                                <li>
                                    <span class="num">⑤</span>VDAS(Vehicle Driving Analysys System and Map
                                </li>
                                <li>
                                    <span class="num">⑥</span>기타관련서비스
                                </li>
                            </ol>

                            <h4>제 5 조 (서비스 이용요금) </h4>
                            <ol>
                                <li>
                                    <span class="num">①</span>회사가 제공하는 서비스는 기본적으로 무료입니다. 단, 별도의 유료 서비스의 경우 해당 서비스에 명시된 요금을 지불하여야 사용 가능합니다.
                                </li>
                                <li>
                                    <span class="num">②</span>회사는 유료 서비스 이용요금을 회사와 계약한 전자지불업체에서 정한 방법에 의하거나 회사가 정한 청구서에 합산하여 청구할 수 있습니다.
                                </li>
                                <li>
                                    <span class="num">③</span>유료서비스 이용을 통하여 결제된 대금에 대한 취소 및 환불은 회사의 결제 이용약관 등 관계법에 따릅니다.
                                </li>
                                <li>
                                    <span class="num">④</span>회원의 개인정보도용 및 결제사기로 인한 환불요청 또는 결제자의 개인정보 요구는 법률이 정한 경우 외에는 거절될 수 있습니다.
                                </li>
                                <li>
                                    <span class="num">⑤</span>무선 서비스 이용 시 발생하는 데이터 통신료는 별도이며 가입한 각 이동통신사의 정책에 따릅니다.
                                </li>
                                <li>
                                    <span class="num">⑥</span>MMS 등으로 게시물을 등록할 경우 발생하는 요금은 이동통신사의 정책에 따릅니다.
                                </li>
                            </ol>

                            <h4>제 6 조 (서비스내용변경 통지 등)</h4>
                            <ol>
                                <li>
                                    <span class="num">①</span>회사가 서비스 내용을 변경하거나 종료하는 경우 회사는 회원의 등록된 전자우편 주소로이메일을 통하여 서비스 내용의 변경 사항 또는 종료를 통지할 수 있습니다.
                                </li>
                                <li>
                                    <span class="num">②</span>①항의 경우 불특정 다수인을 상대로 통지를 함에 있어서는 웹사이트 등 기타 회사의 공지사항을 통하여 회원들에게 통지할 수 있습니다.
                                </li>
                            </ol>

                            <h4>제 7 조 (서비스이용의 제한 및 중지)</h4>
                            <ol>
                                <li>
                                    <span class="num">①</span>회사는 아래 각 호의 1에 해당하는 사유가 발생한 경우에는 회원의 서비스 이용을 제한하거나 중지시킬 수 있습니다.
                                    <ol>
                                        <li>
                                            <span class="num">가.</span>회원이 회사 서비스의 운영을 고의 또는 중과실로 방해하는 경우
                                        </li>
                                        <li>
                                            <span class="num">나.</span>서비스용 설비 점검, 보수 또는 공사로 인하여 부득이한 경우
                                        </li>
                                        <li>
                                            <span class="num">다.</span>전기통신사업법에 규정된 기간통신사업자가 전기통신 서비스를 중지했을 경우
                                        </li>
                                        <li>
                                            <span class="num">라.</span>국가비상사태, 서비스 설비의 장애 또는 서비스 이용의 폭주 등으로 서비스 이용에 지장이 있는 때
                                        </li>
                                        <li>
                                            <span class="num">마.</span>기타 중대한 사유로 인하여 회사가 서비스 제공을 지속하는 것이 부적당하다고 인정하는 경우
                                        </li>
                                    </ol>
                                </li>
                                <li>
                                    <span class="num">②</span>회사는 전항의 규정에 의하여 서비스의 이용을 제한하거나 중지한 때에는 그 사유 및 제한기간 등을 회원에게 알려야 합니다.
                                </li>
                            </ol>

                            <h4>제 8 조 (개인위치정보의 이용 또는 제공)</h4>
                            <ol>
                                <li>
                                    <span class="num">①</span>회사는 개인위치정보를 이용하여 서비스를 제공하고자 하는 경우에는 미리 이용약관에 명시한 후 개인위치정보주체의 동의를 얻어야 합니다.
                                </li>
                                <li>
                                    <span class="num">②</span>회원 및 법정대리인의 권리와 그 행사방법은 제소 당시의 이용자의 주소에 의하며, 주소가없는 경우에는 거소를 관할하는 지방법원의 전속관할로 합니다.
                                    <br />다만, 제소 당시 이용자의 주소 또는 거소가 분명하지 않거나 외국 거주자의 경우에는 민사소송법상의 관할법원에 제기합니다.
                                </li>
                                <li>
                                    <span class="num">③</span>회사는 타사업자 또는 이용 고객과의 요금정산 및 민원처리를 위해 위치정보 이용 제공 사실 확인 자료를 자동 기록 보존하며, 해당 자료는 1년간 보관합니다.
                                </li>
                                <li>
                                    <span class="num">④</span>회사는 개인위치정보를 회원이 지정하는 제3자에게 제공하는 경우에는 개인위치정보를 수집한 당해 통신 단말장치로 매회 회원에게 제공받는 자, 제공일시 및 제공목적을 즉시 통보합니다.
                                    <br /> 단, 아래 각 호의 1에 해당하는 경우에는 회원이 미리 특정하여 지정한 통신 단말장치 또는 전자우편주소로 통보합니다.
                                    <ol>
                                        <li>
                                            <span class="num">가.</span>개인위치정보를 수집한 당해 통신단말장치가 문자, 음성 또는 영상의 수신기능을 갖추지 아니한 경우
                                        </li>
                                        <li>
                                            <span class="num">나.</span>회원이 온라인 게시 등의 방법으로 통보할 것을 미리 요청한 경우
                                        </li>
                                    </ol>
                                </li>
                                <li>
                                    <span class="num">⑤</span>본 서비스는 고객의 개인정보를 타인 또는 타기업, 기관에 공유하거나 제공하지 않습니다. 다만, 고객의 사전 동의가 있거나, 관련 법령(통신비밀보호법, 전기통신사업법, 국세기본법 등)의 특별한 규정이 있는 경우, 법령에 정해진 규정과 절차에 따라 제공하는 경우는 예외로 합니다.
                                </li>
                            </ol>

                            <h4>제 9 조 (개인위치정보주체의 권리)</h4>
                            <ol>
                                <li>
                                    <span class="num">①</span>회원은 회사에 대하여 언제든지 개인위치정보를 이용한 위치기반서비스 제공 및 개인위치정보의 제3자 제공에 대한 동의의 전부 또는 일부를 철회할 수 있습니다. 이 경우 회사는 수집한 개인위치정보 및 위치정보 이용, 제공사실 확인자료를 파기합니다.
                                </li>
                                <li>
                                    <span class="num">②</span>회원은 회사에 대하여 언제든지 개인위치정보의 수집, 이용 또는 제공의 일시적인 중지를 요구할 수 있으며, 회사는 이를 거절할 수 없고 이를 위한 기술적 수단을 갖추고 있습니다.
                                </li>
                                <li>
                                    <span class="num">③</span>회원은 회사에 대하여 아래 각 호의 자료에 대한 열람 또는 고지를 요구할 수 있고, 당해자료에 오류가 있는 경우에는 그 정정을 요구할 수 있습니다. 이 경우 회사는 정당한 사유 없이 회원의 요구를 거절할 수 없습니다.
                                    <ol>
                                        <li>
                                            <span class="num">가.</span>본인에 대한 위치정보 수집, 이용, 제공사실 확인자료
                                        </li>
                                        <li>
                                            <span class="num">나.</span>본인의 개인위치정보가 위치정보의 보호 및 이용 등에 관한 법률 또는 다른 법률 규정에 의하여 제3자에게 제공된 이유 및 내용
                                        </li>
                                    </ol>
                                </li>
                                <li>
                                    <span class="num">④</span>회원은 제1항 내지 제3항의 권리행사를 위해 회사의 소정의 절차를 통해 요구할 수 있습니다.
                                </li>
                            </ol>

                            <h4>제 10 조 (법정대리인의 권리) </h4>
                            <ol>
                                <li>
                                    <span class="num">①</span>회사는 14세 미만의 회원에 대해서는 개인위치정보를 이용한 위치기반서비스 제공 및 개인위치정보의 제3자 제공에 대한 동의를 당해 회원과 당해 회원의 법정대리인으로부터 동의를 받아야 합니다. 이 경우 법정대리인은 제9조에 의한 회원의 권리를 모두 가집니다.
                                </li>
                                <li>
                                    <span class="num">②</span>회사는 14세 미만의 아동의 개인위치정보 또는 위치정보 이용 제공사실 확인자료를 이용약관에 명시 또는 고지한 범위를 넘어 이용하거나 제3자에게 제공하고자 하는 경우에는 14세미만의 아동과 그 법정대리인의 동의를 받아야 합니다. 단, 아래의 경우는 제외합니다.
                                    <ol>
                                        <li>
                                            <span class="num">가.</span>위치정보 및 위치기반서비스 제공에 따른 요금정산을 위하여 위치정보 이용, 제공사실확인 자료가 필요한 경우
                                        </li>
                                        <li>
                                            <span class="num">나.</span>통계작성, 학술연구 또는 시장조사를 위하여 특정 개인을 알아볼 수 없는 형태로 가공하여 제공하는 경우
                                        </li>
                                    </ol>
                                </li>
                            </ol>

                            <h4>제 11 조 (8세 이하의 아동 등의 보호의무자의 권리) </h4>
                            <ol>
                                <li>
                                    <span class="num">①</span>회사는 아래의 경우에 해당하는 자(이하 “8세 이하의 아동”등이라 한다)의 보호의무자가 8세이하의 아동 등의 생명 또는 신체보호를 위하여 개인위치정보의 이용 또는 제공에 동의하는 경우에는 본인의 동의가 있는 것으로 봅니다.
                                    <ol>
                                        <li>
                                            <span class="num">가.</span>8세 이하의 아동
                                        </li>
                                        <li>
                                            <span class="num">나.</span>금치산자
                                        </li>
                                        <li>
                                            <span class="num">다.</span>장애인복지법제2조제2항제2호의 규정에 의한 정신적 장애를 가진 자로서 장애인고용 촉진 및 직업 재활법 제2조제2호의 규정에 의한 중증장애인에 해당하는 자(장애인복지법 제29조의 규정에 의하여 장애인등록을 한 자에 한한다)
                                        </li>
                                    </ol>
                                </li>
                                <li>
                                    <span class="num">②</span>8세 이하의 아동 등의 생명 또는 신체의 보호를 위하여 개인위치정보의 이용 또는 제공에 동의를 하고자 하는 보호의무자는 서면동의서에 보호의무자임을 증명하는 서면을 첨부하여 회사에 제출하여야 합니다.
                                </li>
                                <li>
                                    <span class="num">③</span>보호의무자는 8세 이하의 아동 등의 개인위치정보 이용 또는 제공에 동의하는 경우 개인위치정보주체 권리의 전부를 행사할 수 있습니다.
                                </li>
                            </ol>

                            <h4>제 12 조 (위치정보관리책임자의 지정)</h4>
                            <ol>
                                <li>
                                    <span class="num">①</span>회사는 위치정보를 적절히 관리보호하고 개인위치정보주체의 불만을 원활히 처리할 수 있도록 실질적인 책임을 질 수 있는 지위에 있는 자를 위치정보관리책임자로 지정해 운영합니다.
                                </li>
                                <li>
                                    <span class="num">②</span>위치정보관리책임자는 위치기반서비스를 제공하는 부서의 부서장으로서 구체적인 사항은 본 약관의 부칙에 따릅니다.
                                </li>
                            </ol>

                            <h4>제 13 조 (손해배상) </h4>
                            <ol>
                                <li>
                                    <span class="num">①</span>회사가 위치정보의 보호 및 이용 등에 관한 법률 제15조 내지 제26조의 규정을 위반한 행위로 회원에게 손해가 발생한 경우 회원은 회사에 대하여 손해배상 청구를 할 수 있습니다. 이 경우 회사는 고의, 과실이 없음을 입증하지 못하는 경우 책임을 면할 수 없습니다.
                                </li>
                                <li>
                                    <span class="num">②</span>회원이 본 약관의 규정을 위반하여 회사에 손해가 발생한 경우 회사는 회원에 대하여 손해배상을 청구할 수 있습니다. 이 경우 회원은 고의, 과실이 없음을 입증하지 못하는 경우 책임을 면할 수 없습니다.
                                </li>
                            </ol>

                            <h4>제 14 조 (면책) </h4>
                            <ol>
                                <li>
                                    <span class="num">①</span>회사는 다음 각 호의 경우로 서비스를 제공할 수 없는 경우 이로 인하여 회원에게 발생한 손해에 대해서는 책임을 부담하지 않습니다.
                                    <ol>
                                        <li>
                                            <span class="num">가.</span>천재지변 또는 이에 준하는 불가항력의 상태가 있는 경우
                                        </li>
                                        <li>
                                            <span class="num">나.</span>서비스 제공을 위하여 회사와 서비스 제휴계약을 체결한 제3자의 고의적인 서비스 방해가 있는 경우
                                        </li>
                                        <li>
                                            <span class="num">다.</span>회원의 귀책사유로 서비스 이용에 장애가 있는 경우
                                        </li>
                                        <li>
                                            <span class="num">라.</span>제1호 내지 제3호를 제외한 기타 회사의 고의 과실이 없는 사유로 인한 경우
                                        </li>
                                    </ol>
                                </li>
                                <li>
                                    <span class="num">②</span>회사는 서비스 및 서비스에 게재된 정보, 자료, 사실의 신뢰도, 정확성 등에 대해서는 보증을 하지 않으며 이로 인해 발생한 회원의 손해에 대하여는 책임을 부담하지 아니합니다.
                                </li>
                            </ol>

                            <h4>제 15 조 (규정의 준용) </h4>
                            <ol>
                                <li>
                                    <span class="num">①</span>본 약관은 대한민국법령에 의하여 규정되고 이행됩니다.
                                </li>
                                <li>
                                    <span class="num">②</span>본 약관에 규정되지 않은 사항에 대해서는 관련법령 및 상관습에 의합니다.
                                </li>
                            </ol>

                            <h4>제 16 조 (분쟁의 조정 및 기타) </h4>
                            <ol>
                                <li>
                                    <span class="num">①</span>회사는 위치정보와 관련된 분쟁에 대해 당사자간 협의가 이루어지지 아니하거나 협의를할 수 없는 경우에는 위치정보의 보호 및 이용 등에 관한 법률 제28조의 규정에 의한 방송통신위원회에 재정을 신청할 수 있습니다.
                                </li>
                                <li>
                                    <span class="num">②</span>회사 또는 고객은 위치정보와 관련된 분쟁에 대해 당사자간 협의가 이루어지지 아니하거나 협의를 할 수 없는 경우에는 개인정보보호법 제43조의 규정에 의한 개인정보 분쟁조정위원회에 조정을 신청할 수 있습니다.
                                </li>
                            </ol>

                            <h4>제 17 조 (회사의 연락처)</h4>
                            <p>회사의 상호 및 주소 등은 다음과 같습니다.</p>
                            <ol>
                                <li>
                                    <span class="num">①</span>상 호 : (주)자스텍엠
                                </li>
                                <li>
                                    <span class="num">②</span>대 표 자 : 백 용 범
                                </li>
                                <li>
                                    <span class="num">③</span>주소 : (13487) 경기도 성남시 분당구 판교로 C동 402-1호 (삼평동, 판교디지털센터)
                                </li>
                                <li>
                                    <span class="num">④</span>대표전화 : 1599-8439
                                </li>
                            </ol>

                            <h4>부 칙</h4>
                            <p>
                                제1조 (시행일) 이 약관은 2017년11월 1일부터 시행한다.
                                <br /> 제2조 위치정보관리책임자는 2017년11월 1일을 기준으로 다음과 같이 지정합니다.
                            </p>
                            <ol>
                                <li>
                                    <span class="num">①</span>소 속 : 서비스운영본부
                                </li>
                                <li>
                                    <span class="num">②</span>관리책임자 : 장승부
                                </li>
                                <li>
                                    <span class="num">③</span>연락처 : 031-8060-0321
                                </li>
                            </ol>
                        </div>
                        <!--/ 03. 위치정보 수립-->

                        <!-- 04. 마케팅선택 동의 -->
                        <div class="top">
                            <h3>04. 마케팅선택 동의
                                <span>(선택)</span>
                            </h3>
                            <div class="check">
                                <input type="checkbox" id="marketingAccepted" />
                                <label for="marketingAccepted">동의</label>
                            </div>
                        </div>
                        <!--/ 04. 마케팅선택 동의 -->
                    </div>

                    <!-- 하단 버튼 -->
                    <div class="btn-bottom">
                        <button class="btn btn02" id="prev">이전</button>
                        <button class="btn btn03" id="next">확인</button>
                    </div>
                    <!--/ 하단 버튼 -->
                </div>
            </div>
            <!--/ 콘텐츠 본문 -->
        </section>