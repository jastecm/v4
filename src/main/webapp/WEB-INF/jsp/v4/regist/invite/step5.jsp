<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script>
$(document).ready(function(){
	$("#main").on("click",function(){$V4.move("/main");});
	
});
</script>
 <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>회원가입</h2>
                    <span></span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="join-page">
					 <!-- 회원가입 STEP -->
                    <div class="join-step">
                        <ol>
                            <li class="step01">
                                <span>STEP01</span>
                                <strong>약관동의</strong>
                            </li>
                            <li class="step02">
                                <span>STEP02</span>
                                <strong>지역선택</strong>
                            </li>
                            <li class="step03">
                                <span>STEP03</span>
                                <strong>회원정보입력</strong>
                            </li>
                            <li class="step04">
                                <span>STEP04</span>
                                <strong>회원정보확인</strong>
                            </li>
                            <li class="step05 active">
                               	<span>STEP05</span>
                               	<strong>가입완료</strong>
                           	</li>
                        </ol>
                    </div>
                    <!--/ 회원가입 STEP -->

					<!-- 제공받은 자료에 완료 페이지에 대한 정의가 되어있지 않아 임의로 작업해서 넣었습니다. -->
                    <div class="join-complete">
                        <strong>WELCOME</strong>
                        <span>회원가입을 진심으로 감사드립니다.</span> 홍길동님께서는 xxxx(ID)로 ㈜자스텍엠의 회원으로 가입되셨습니다.
                        <br /> ㈜자스텍엠은 다양한 정보와 함께 보다 나은 서비스를 제공하도록 열심히 노력하겠습니다.
                        <br /> 감사합니다.
                    </div>
                    <!--/ 회원가입 완료 -->

                    <!-- 하단 버튼 -->
                    <div class="btn-bottom">
                        <button class="btn btn01" id="main">메인으로</button>
                        <button class="btn btn02">로그인</button>
                    </div>
                    <!--/ 하단 버튼 -->

                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>