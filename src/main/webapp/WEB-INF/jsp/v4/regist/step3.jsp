<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script>
$(document).ready(function(){
	
	$('#prev').on('click',function(){
		window.history.back();
	});
	
	$('#next').on('click',function(){
		
		var ctyCode = $('select[name="ctyCode"]').val();
		var timezoneOffset = $('select[name="timezoneOffset"]').val();
		
		var unitLength = $('select[name="unitLength"]').val();
		var unitVolume = $('select[name="unitVolume"]').val();
		var unitTemperature = $('select[name="unitTemperature"]').val();
		var unitCurrency = $('select[name="unitCurrency"]').val();
		var unitDate = $('select[name="unitDate"]').val();
		
		$('input[name="ctyCode"]').val(ctyCode);
		$('input[name="timezoneOffset"]').val(timezoneOffset);
		
		$('input[name="unitLength"]').val(unitLength);
		$('input[name="unitVolume"]').val(unitVolume);
		$('input[name="unitTemperature"]').val(unitTemperature);
		$('input[name="unitDate"]').val(unitDate);
		
		//1 개인 2 법인/단체 3 렌트 4파트너
		//개인 /regist/person/step1
		//법인,렌트 /regist/corp/step1
		//파트너 /regist/partner/step1
		
		var corpType = $("#frm input[name=corpType]").val();
		var cmd = "/main";
		
		if(corpType == "1")
			cmd = "/regist/person/step1";
		else if(corpType == "2" || corpType == "3")
			cmd = "/regist/corp/step1";
		else if(corpType == "4")
			cmd = "/regist/partner/step1";
		
		$V4.move(cmd,$("#frm"));
		
	});
	
});

</script>
<!-- corptype에 따라 이제 달라진다. -->
<form id="frm" name="frm" action="" method="post">
	<input type="hidden" name="accountId" value="${rtv.accountId}"/>
	<input type="hidden" name="accountPw" value="${rtv.accountPw}"/>
	<input type="hidden" name="corpType" value="${rtv.corpType}"/>
	
	<input type="hidden" name="serviceAccepted" value="${rtv.serviceAccepted}"/>
	<input type="hidden" name="privateAccepted" value="${rtv.privateAccepted}"/>
	<input type="hidden" name="locationAccepted" value="${rtv.locationAccepted}"/>
	<input type="hidden" name="marketingAccepted" value="${rtv.marketingAccepted}"/>
	
	<input type="hidden" name="ctyCode" value=""/>
	<input type="hidden" name="timezoneOffset" value=""/>
	
	<input type="hidden" name="unitLength" value=""/>
	<input type="hidden" name="unitVolume" value=""/>
	<input type="hidden" name="unitTemperature" value=""/>
	<input type="hidden" name="unitDate" value=""/>
	
</form>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>회원가입</h2>
                    <span></span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="join-page">
                    <!-- 회원가입 STEP -->
                    <div class="join-step">
                        <ol>
                            <li class="step01">
                                <span>STEP01</span>
                                <strong>가입선택</strong>
                            </li>
                            <li class="step02">
                                <span>STEP02</span>
                                <strong>약관동의</strong>
                            </li>
                            <li class="step03 active">
                                <span>STEP03</span>
                                <strong>지역선택</strong>
                            </li>
                            <li class="step04">
                                <span>STEP04</span>
                                <strong>회원정보입력</strong>
                            </li>
                            <li class="step05">
                                <span>STEP05</span>
                                <strong>회원정보확인</strong>
                            </li>
                            <!-- 개인,파트너 -->
                            <c:if test="${rtv.corpType eq '1' || rtv.corpType eq '4'}">
                            	<li class="step06">
                                	<span>STEP06</span>
                                	<strong>가입완료</strong>
                           		</li>
                            </c:if>
                            <!-- 법인렌트카 -->
                            <c:if test="${rtv.corpType eq '2' || rtv.corpType eq '3'}">
                            	<li class="step06">
                                	<span>STEP06</span>
                                	<strong>등급선택</strong>
                            	</li>
                            	<li class="step07">
                                	<span>STEP07</span>
                                	<strong>가입완료</strong>
                            	</li>
                            </c:if>
                        </ol>
                    </div>
                    <!--/ 회원가입 STEP -->

                    <h3 class="tit">지역선택</h3>
                    <!-- 폼 -->
                        <table class="table mgt30">
                            <caption>지역선택</caption>
                            <tbody>
                                <tr>
                                    <th scope="row">서비스 언어 및 지역</th>
                                    <td>
                                        <select name="ctyCode" style="width:500px">
											<option value="82">+82 대한민국 (Republic of Korea)</option>
											<option value="1">+1 미국 (US)</option>
										</select>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">표준시간대 선택</th>
                                    <td>
                                        <select name="timezoneOffset" style="width:500px">
											<option value="540">(UTC + 09:00) 서울</option>
											<option value="-360">(UTC - 06:00) 중부표준시(미국)</option>
										</select>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">거리단위 선택</th>
                                    <td>
                                        <select name="unitLength" style="width:500px">
											<option value="km">키로미터</option>
											<option value="mi">마일</option>
										</select>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">부피단위 선택</th>
                                    <td>
                                        <select name="unitVolume" style="width:500px">
											<option value="l">리터</option>
											<option value="gal">갤런</option>
										</select>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">온도단위 선택</th>
                                    <td>
                                        <select name="unitTemperature" style="width:500px">
											<option value="C">섭씨</option>
											<option value="F">화씨</option>
										</select>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">날짜표기방식 선택</th>
                                    <td>
                                        <select name="unitDate" style="width:500px">
											<option value="YMD">년/월/일</option>
											<option value="MDY">월/일/년</option>
											<option value="DMY">일/월/년</option>
										</select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <!-- 하단 버튼 -->
                        <div class="btn-bottom ">
                            <button class="btn btn02" id="prev">이전</button>
                            <button class="btn btn03" id="next">계속</button>
                        </div>
                        <!--/ 하단 버튼 -->
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->