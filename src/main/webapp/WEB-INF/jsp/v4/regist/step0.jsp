<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<style>
table.table .safe {
    margin-left: 30px;
    font-size: 15px;
    color: #00CC00;
}
.join-page .password-check span.safe {
    margin-left: 0 !important;
}
</style>



<script type="text/javascript">
$(function(){
	
	$("#pw").on("keyup",function(e){
		var v = $(this).val();
		var chk = 0;		
		if(v){
			var regExpAlphabet = new RegExp("(?=.*[a-zA-Z])");
			var regExpNumber = new RegExp("(?=.*[0-9])");
			var regExpSpecialChar = new RegExp("(?=.*[!@#$%^\\&*\\(\\)\\_\\+\\|\\-\\=\\\\\\[\\];'?\\.\\,])");
			regExpAlphabet.test(v)&&chk++;
			regExpNumber.test(v)&&chk++;
			regExpSpecialChar.test(v)&&chk++;
			$("#re").val()==v?$("#reResult").removeClass("warning").addClass("safe").text("비밀번호가 일치합니다."):$("#reResult").removeClass("safe").addClass("warning").text("비밀번호가 일치하지 않습니다.");
		}
		var regExpLength = new RegExp("^.{8,20}$");
		if(chk == 3 && regExpLength.test(v)){
			$("#pwResult").removeClass("warning").addClass("safe").text("사용가능");
			
		}else{
			if(chk==0) chk = 1;
			$("#pwResult").removeClass("safe").addClass("warning").text("사용불가");
			
		}
		
		$(".password-check").find("div").removeClass("step01").removeClass("step02").removeClass("step03").addClass("step0"+chk);
		
		
		
	});
	
	$("#re").on("keyup",function(e){
		var v = $(this).val();
		v&&$("#pw").val()==v?$("#reResult").removeClass("warning").addClass("safe").text("비밀번호가 일치합니다."):$("#reResult").removeClass("safe").addClass("warning").text("비밀번호가 일치하지 않습니다.");
	});

	$(".btn02").on("click",function(){
		$V4.move("/main");
	});
	
	$(".btn01").on("click",function(){
		
		var p = {}
		var o = $("#frm").serializeObject();
		
		if(!o.email1&&(alert("아이디를 입력해주세요.")||1)&&$("#frm").find("input[name=email1]").focus()) return false;
		if(!o.email2&&(alert("아이디를 입력해주세요.")||1)&&$("#frm").find("input[name=email2]").focus()) return false;
		if(!($("#pwResult").hasClass("safe")&&$("#reResult").hasClass("safe"))&&(alert("비밀번호를 확인해주세요.(영어,숫자,특수문자 가 포함된 8~20자)")||1)&&$("#frm").find("input[name=pw]").focus()) return false;
		
		console.log("ok");
		p.accountId = o.email1+"@"+o.email2;
		p.accountPw = o.pw;
		
		var url = "/api/v1/regist";
		$V4.http_post(url,p,{
			success : function(rtv){
				alert("초대메일이 발송되었습니다.");
			}
		});
		
	});
});
</script>
<section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>회원가입</h2>
                    <span></span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="join-page">
                    <h3 class="tit">회원 가입 메일 전송</h3>
                    <p class="txt01">사용하시고자 하는 메일과 비밀번호를 입력해주시면 입력하신 메일주소로 가입 인증메일이 전송됩니다.
                        <br /> 메일을 열람하시어 ViewCAR 가입절차를 따라주세요.</p>

                    <!-- 폼 -->
                    <form method="post" name="form" id="frm">
                        <table class="table mgt30">
                            <colgroup>
                                <col style="width:180px" />
                                <col />
                            </colgroup>
                            <caption>회원 가입 메일 전송</caption>
                            <tbody>
                                <tr>
                                    <th scope="row">아이디</th>
                                    <td>
                                        <div class="division">
                                            <div class="col-5">
                                                <input type="text" name="email1" />
                                            </div>
                                            <div class="space">&nbsp;@&nbsp;</div>
                                            <div class="col-4">
                                                <input type="text" name="email2" />
                                            </div>
                                            <div class="space">&nbsp;</div>
                                            <div class="col-3">
                                                <select name="" onchange="this.form.email2.value=this.options[this.selectedIndex].value">
													<option value="">이메일 선택</option>
													<option value="hanmail.net">다음</option>
													<option value="empal.com">엠파스</option>
													<option value="netian.com">네띠앙</option>
													<option value="naver.com">네이버</option>
													<option value="nate.com">네이트</option>
													<option value="dreamwiz.com">드림위즈</option>
													<option value="lycos.co.kr">라이코스</option>
													<option value="yahoo.co.kr">야후</option>
													<option value="chol.com">천리안</option>
													<option value="korea.com">코리아닷컴</option>
													<option value="paran.com">파란</option>
													<option value="freechal.com">프리챌</option>
													<option value="hitel.net">하이텔</option>
													<option value="hanmir.com">한미르</option>
													<option value="hotmail.com">핫메일</option>
													<option value="hanmir.com">한미르</option>
													<option value="">직접쓰기</option>
												</select>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">비밀번호</th>
                                    <td>
                                        <input type="password" name="pw" id="pw"/>
                                        <div class="password-check">
                                            <div class="bar step01"><span>1</span></div>
                                            <p>비밀번호 안전도 :<span class="warning" id="pwResult">사용불가</span>
                                            </p>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">비밀번호 확인</th>
                                    <td>
                                        <input type="password" id="re"/>
                                        <div class="warning" id="reResult">비밀번호가 일치하지 않습니다.</div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <!-- 하단 버튼 -->
                        <div class="btn-bottom ">
                            <button class="btn btn01" onClick="return false;">가입메일 전송</button>
                            <button class="btn btn02" onClick="return false;">가입 취소</button>
                        </div>
                        <!--/ 하단 버튼 -->
                    </form>
                    <!--/ 폼 -->
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>