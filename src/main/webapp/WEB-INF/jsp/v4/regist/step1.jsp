<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script>
$(document).ready(function(){
	
	//회원 선택 이벤트
	$('#corpType a').on('click',function(){

		var corpType = $(this).data('corptype');
		$('#frm input[name="corpType"]').val(corpType);
		
		$V4.move("/regist/step2",$('#frm'));
		
	});
	
});

</script>
<form id="frm" name="frm">
	<input type="hidden" name="accountId" value="${accountId}"/>
	<input type="hidden" name="accountPw" value="${accountPw}"/>
	<input type="hidden" name="corpType" value=""/>
</form>


 <!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>회원가입</h2>
                    <span></span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="join-page">
					 <!-- 회원가입 STEP -->
                    <div class="join-step">
                        <ol>
                            <li class="step01 active">
                                <span>STEP01</span>
                                <strong>가입선택</strong>
                            </li>
                            <li class="step02">
                                <span>STEP02</span>
                                <strong>약관동의</strong>
                            </li>
                            <li class="step03">
                                <span>STEP03</span>
                                <strong>지역선택</strong>
                            </li>
                            <li class="step04">
                                <span>STEP04</span>
                                <strong>회원정보입력</strong>
                            </li>
                            <li class="step05">
                                <span>STEP05</span>
                                <strong>회원정보확인</strong>
                            </li>
                            <li class="step06">
                                <span>STEP06</span>
                                <strong>등급선택</strong>
                            </li>
                            <li class="step07">
                                <span>STEP07</span>
                                <strong>가입완료</strong>
                            </li>
                        </ol>
                    </div>
                    <!--/ 회원가입 STEP -->
					
                    <h3 class="tit">회원선택</h3>
					<p class="txt01">가입하고자 하시는 회원 유형을 선택해주세요.<br />서비스 스토어를 이용하시는 단체/사업자회원은 제휴/파트너 회원 가입을 선택해주세요</p>
					
					<div class="join-type-select" id="corpType">
                        <ul>
                            <li class="type01">
                                <a href="#none" data-corptype="1">
                                    <span>차량을 개인적으로
										<br />이용하시는 일반인 / 가족</span>
                                    <strong>개인 회원가입</strong>
                                </a>
                            </li>
                            <li class="type02">
                                <a href="#none" data-corptype="2">
                                    <span>여러대의 차량을 관리하는
										<br />단체 / 사업자</span>
                                    <strong>단체/사업자 회원가입</strong>
                                </a>
                            </li>
                            <li class="type03">
                                <a href="#none" data-corptype="3">
                                    <span>차량을 영리목적으로
										<br />사용하시는 사업자</span>
                                    <strong>렌터카 회원가입</strong>
                                </a>
                            </li>
                            <li class="type04">
                                <a href="#none" data-corptype="4">
                                    <span>보험/정비/딜러/세차 등
										<br />자동차 기반 사업자</span>
                                    <strong>제휴/파트너 회원가입</strong>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->