<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	$("#search-date1").datepicker({
        dateformat: 'yy-mm-dd'
    });
    $("#search-date2").datepicker({
        dateformat: 'yy-mm-dd'
    });
    $("#search-date3").datepicker({
        dateformat: 'yy-mm-dd'
    });
    $("#search-month-date").datepicker({
        dateformat: 'yy-mm-dd'
    });

    // 탭메뉴
    $(".tab-menu>li>a").click(function() {
        $(this).parent().addClass("on").siblings().removeClass("on");
        return false;
    });
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>리포트</h2>
                    <span>등록차량의 운행기록 빅데이터를 기간별로 분석하여 차량의 합리적인  관리를 지원합니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="report-page">
                    <div class="report-list">
                        <!-- 탭메뉴 -->
                        <ul class="page-tab tab-menu col-4">
                            <!-- 개인별 -->
                            <li class="list01 on">
                                <a href="#">개인별</a>
                                <div class="tab-list">
                                    <div class="search-wrap">
                                        <input type="text" id="search-date1" />
                                        <input type="text" placeholder="사용자" class="user" />
                                        <button class="btn btn01 search">검색</button>
                                        <div class="btn-wrap">
                                            <button class="btn btn03">조회</button>
                                            <button class="btn btn02">리포트인쇄</button>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <!--/ 개인별 -->

                            <!-- 부서별 -->
                            <li class="list02">
                                <a href="#">부서별</a>
                                <div class="tab-list">
                                    <div class="search-wrap">
                                        <input type="text" id="search-date2" />
                                        <input type="text" placeholder="부서" class="user" />
                                        <button class="btn btn01 search">검색</button>
                                        <div class="btn-wrap">
                                            <button class="btn btn03">조회</button>
                                            <button class="btn btn02">리포트인쇄</button>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <!--/ 부서별 -->

                            <!-- 회사 전체 -->
                            <li class="list03">
                                <a href="#">회사 전체</a>
                                <div class="tab-list">
                                    <div class="search-wrap">
                                        <input type="text" id="search-month-date" placeholder="기준월 선택" />
                                        <input type="text" id="search-date3" />
                                        <div class="btn-wrap">
                                            <button class="btn btn03">조회</button>
                                            <button class="btn btn02">리포트인쇄</button>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <!--/ 회사 전체 -->
                        </ul>
                    </div>
                    <!--/ 콘텐츠 본문 -->
                </div>
        </section>
        <!--/ 본문 -->