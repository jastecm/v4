<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>국세청 운행일지</h2>
                    <span>별도의 작성 없이 운행일지를 간편하게 다운로드 할 수 있습니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="report-page">
                    <div class="NTS-report">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                            <div class="item">
                                <span>
									등록차량 수
									<strong>10</strong>대
								</span>
                                <span>
									업무사용비율
									<strong>100</strong>%
								</span>
                            </div>

                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <!-- 상단 검색 -->
                        <div class="top-search mgt20">
                            <select name="">
								<option value="">2018</option>
							</select>
                            <select name="">
									<option value="">1분기</option>
								</select>
                            <input type="text" name="" placeholder="차량 검색" style="min-width:200px" />
                            <button class="btn btn03">조회</button>
                        </div>
                        <!--/ 상단 검색 -->


                        <!-- table 버튼 -->
                        <div class="btn-function">
                            <div class="left">
                                <span class="txt">
									<strong>선택 항목&nbsp;&nbsp;<span class="red">1대</span></strong>
                                </span>
                            </div>
                            <div class="right">
                                <button class="excel">국세청 제출용</button>
                                <button class="excel">통합 운행내역</button>
                                <select name="">
									<option value="">5건씩 보기</option>
								</select>
                            </div>
                        </div>
                        <!--/ table 버튼 -->

                        <div class="scroll-table mgt20">
                            <table class="table list">
                                <caption>등록된 서비스 리스트</caption>
                                <thead>
                                    <tr>
                                        <th scope="col"><input type="checkbox" /></th>
                                        <th scope="col">차량정보</th>
                                        <th scope="col">총 주행거리</th>
                                        <th scope="col">업무용/출퇴근 사용거리</th>
                                        <th scope="col">비업무용 사용거리</th>
                                        <th scope="col">업무사용비율</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>
                                            <div class="car-img">
                                                <span class="img"><img src="./img/vehicle/S103.png" alt="" /></span>
                                                <span class="num">쏘나타 63호 5703</span>
                                            </div>
                                        </td>
                                        <td>17km</td>
                                        <td>17km</td>
                                        <td>0km</td>
                                        <td>100%</td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>
                                            <div class="car-img">
                                                <span class="img"><img src="./img/vehicle/S103.png" alt="" /></span>
                                                <span class="num">쏘나타 63호 5703</span>
                                            </div>
                                        </td>
                                        <td>17km</td>
                                        <td>17km</td>
                                        <td>0km</td>
                                        <td>100%</td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>
                                            <div class="car-img">
                                                <span class="img"><img src="./img/vehicle/S103.png" alt="" /></span>
                                                <span class="num">쏘나타 63호 5703</span>
                                            </div>
                                        </td>
                                        <td>17km</td>
                                        <td>17km</td>
                                        <td>0km</td>
                                        <td>100%</td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>
                                            <div class="car-img">
                                                <span class="img"><img src="./img/vehicle/S103.png" alt="" /></span>
                                                <span class="num">쏘나타 63호 5703</span>
                                            </div>
                                        </td>
                                        <td>17km</td>
                                        <td>17km</td>
                                        <td>0km</td>
                                        <td>100%</td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>
                                            <div class="car-img">
                                                <span class="img"><img src="./img/vehicle/S103.png" alt="" /></span>
                                                <span class="num">쏘나타 63호 5703</span>
                                            </div>
                                        </td>
                                        <td>17km</td>
                                        <td>17km</td>
                                        <td>0km</td>
                                        <td>100%</td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>
                                            <div class="car-img">
                                                <span class="img"><img src="./img/vehicle/S103.png" alt="" /></span>
                                                <span class="num">쏘나타 63호 5703</span>
                                            </div>
                                        </td>
                                        <td>17km</td>
                                        <td>17km</td>
                                        <td>0km</td>
                                        <td>100%</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->