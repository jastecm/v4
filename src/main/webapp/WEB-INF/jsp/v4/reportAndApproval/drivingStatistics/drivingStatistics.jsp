<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script src="${pageContext.request.contextPath}/common/new/js/iscroll.js"></script>
<script type="text/javascript">
$(function(){
});
function loaded() {
    var iscroll = new iScroll("table-iscroll", {
        vScroll: false
    });
}

document.addEventListener('DOMContentLoaded', loaded, false);

</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>운행통계</h2>
                    <span>등록차량의 운행기록을 월기준으로 요약해서 관리합니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="report-page">
                    <div class="driving-stats">
                        <h3 class="tit2 mgt30">월별 운행기록</h3>

                        <!-- 상단 검색 -->
                        <div class="top-search mgt20">
                            <span class="search-tit">조회월</span>
                            <select name="">
								<option value="">2018년 03월</option>
							</select>
                            <button class="btn btn03">조회</button>
                        </div>
                        <!--/ 상단 검색 -->
                        <!-- table 버튼 -->
                        <div class="btn-function">
                            <div class="left">
                                <input type="checkbox" /> 운행시간&nbsp;&nbsp;&nbsp;
                                <input type="checkbox" /> 운행거리&nbsp;&nbsp;&nbsp;
                                <input type="checkbox" /> 과속건수
                                <button class="btn btn02 md">저장</button>
                                <span class="txt">(최근 6개월 이내의 운행기록 내역 조회가 가능합니다.)</span>
                            </div>
                            <div class="right">
                                <button class="excel">엑셀 다운로드</button>
                            </div>
                        </div>

                        <!--/ table 버튼 -->

                        <div id="table-iscroll">
                            <table class="table list border mgt20">
                                <caption>등록된 서비스 리스트</caption>
                                <thead>
                                    <tr>
                                        <th scope="col">순번</th>
                                        <th scope="col">차량번호</th>
                                        <th scope="col">운전자명</th>
                                        <th scope="col">구분</th>
                                        <th scope="col">합계</th>
                                        <th scope="col">1</th>
                                        <th scope="col">2</th>
                                        <th scope="col">3</th>
                                        <th scope="col">4</th>
                                        <th scope="col">5</th>
                                        <th scope="col">6</th>
                                        <th scope="col">7</th>
                                        <th scope="col">8</th>
                                        <th scope="col">9</th>
                                        <th scope="col">10</th>
                                        <th scope="col">11</th>
                                        <th scope="col">12</th>
                                        <th scope="col">13</th>
                                        <th scope="col">14</th>
                                        <th scope="col">15</th>
                                        <th scope="col">16</th>
                                        <th scope="col">17</th>
                                        <th scope="col">18</th>
                                        <th scope="col">19</th>
                                        <th scope="col">20</th>
                                        <th scope="col">21</th>
                                        <th scope="col">22</th>
                                        <th scope="col">23</th>
                                        <th scope="col">24</th>
                                        <th scope="col">25</th>
                                        <th scope="col">26</th>
                                        <th scope="col">27</th>
                                        <th scope="col">28</th>
                                        <th scope="col">29</th>
                                        <th scope="col">30</th>
                                        <th scope="col">31</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- 반복 -->
                                    <tr>
                                        <td rowspan="3">1</td>
                                        <td rowspan="3">33호2212</td>
                                        <td rowspan="3" class="bd-rt">차량반납</td>
                                        <td>운행시간(분)</td>
                                        <td>876</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>56</td>
                                        <td>257</td>
                                        <td>185</td>
                                        <td>116</td>
                                        <td>18</td>
                                        <td></td>
                                        <td>175</td>
                                        <td>68</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>운행거리(KM)</td>
                                        <td>374.3</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>94.5</td>
                                        <td>36.4</td>
                                        <td>80.7</td>
                                        <td>128.2</td>
                                        <td>120.2</td>
                                        <td>0</td>
                                        <td>79.8</td>
                                        <td>26.0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td>과속횟수</td>
                                        <td>0</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td></td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <!--/ 반복 -->
                                    <tr>
                                        <td rowspan="3">2</td>
                                        <td rowspan="3">33호2212</td>
                                        <td rowspan="3" class="bd-rt">차량반납</td>
                                        <td>운행시간(분)</td>
                                        <td>876</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>56</td>
                                        <td>257</td>
                                        <td>185</td>
                                        <td>116</td>
                                        <td>18</td>
                                        <td></td>
                                        <td>175</td>
                                        <td>68</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>운행거리(KM)</td>
                                        <td>374.3</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>94.5</td>
                                        <td>36.4</td>
                                        <td>80.7</td>
                                        <td>128.2</td>
                                        <td>120.2</td>
                                        <td>0</td>
                                        <td>79.8</td>
                                        <td>26.0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr>
                                        <td>과속횟수</td>
                                        <td>0</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td></td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <!-- 페이징 -->
                        <div id="paging">
                            <a href="" class="first icon-spr">제일 처음으로</a>
                            <a href="" class="prev icon-spr">이전으로</a>
                            <span class="current">1</span>
                            <a href="" class="num">2</a>
                            <a href="" class="num">3</a>
                            <a href="" class="num">4</a>
                            <a href="" class="num">5</a>
                            <a href="" class="next icon-spr">다음으로</a>
                            <a href="" class="last icon-spr">제일 마지막으로</a>
                        </div>
                        <!--/ 페이징 -->
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->