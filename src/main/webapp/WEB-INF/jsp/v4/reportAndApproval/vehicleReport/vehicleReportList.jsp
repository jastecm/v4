<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	 $("#start-date").datepicker({
         dateformat: 'yy-mm-dd'
     });

     $("#end-date").datepicker({
         dateformat: 'yy-mm-dd'
     });
     
     $(document).on('click','.reportDetailRegBtn',function(){
    	$V4.move('/reportApproval/vehicleReport/reg');
     });
});
</script>
 <!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>차량보고서 조회ㆍ제출</h2>
                    <span>사용차량의 운행내역ㆍ경비보고 또는 반납을 위한 보고서를 제출합니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="report-page">
                    <div class="">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                            <div class="item">
                                <span>
									총 보고서
									<strong>10</strong>건
								</span>
                                <span>
									제출
									<strong>8</strong>건
								</span>
                                <span>
									미제출
									<strong>2</strong>건
								</span>
                            </div>

                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <!-- 상단 검색 -->
                        <div class="top-search mgt20">
                            <select name="">
								<option value="">전체</option>
							</select>
                            <input type="text" name="" />
                            <button class="btn btn03">조회</button>
                        </div>
                        <!--/ 상단 검색 -->

                        <!-- table 버튼 -->
                        <div class="btn-function">
                            <div class="left">
                                <select name="">
									<option value="">최근 운행순</option>
								</select>

                                <div class="date">
                                    <button class="active">전체</button>
                                    <button>오늘</button>
                                    <button>1주일</button>
                                    <button>1개월</button>
                                    <button>3개월</button>
                                    <button>6개월</button>
                                    <div class="direct">
                                        <input type="text" id="start-date" /> &nbsp;~&nbsp;
                                        <input type="text" id="end-date" />
                                    </div>
                                    <!-- 기획서에는 직접입력이 셀렉트박스로 되어 있는데, 사용자가 어떻게 날짜를 입력 해야 하는지 모르겠어서 임의로 변경했습니다.-->
                                </div>
                            </div>
                            <div class="right">
                                <select name="">
									<option value="">5건씩 보기</option>
								</select>
                            </div>
                        </div>
                        <!--/ table 버튼 -->

                        <table class="table list mgt20">
                            <caption>등록된 서비스 리스트</caption>
                            <colgroup>
                                <col style="width:20%" />
                                <col style="width:20%" />
                                <col />
                                <col style="width:15%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="col">차량정보</th>
                                    <th scope="col">최근 사용자</th>
                                    <th scope="col">운행정보</th>
                                    <th scope="col">
                                        <select name="" class="arrow">
											<option value="">보고서</option>
										</select>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="car-img">
                                            <span class="img"><img src="./img/vehicle/S103.png" alt="" /></span><br />
                                            <span class="num">쏘나타 63호 5703</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="user-img">
                                            <span class="img"><img src="./img/common/user-default.png"alt="" /></span><br />
                                            <span class="name">maumgolf / 카카오VX</span>
                                        </div>
                                    </td>
                                    <td class="left">
                                        <div class="driving-info">
                                            <ul>
                                                <li>
                                                    <strong class="title">시작/종료</strong>
                                                    <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                </li>
                                                <li>
                                                    <strong class="title">업무명</strong>
                                                    <span>업무용</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                    <td>
                                        <strong class="warning">미제출</strong>&nbsp;&nbsp;
                                        <button class="btn-img change reportDetailRegBtn">보고서</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <!-- 페이징 -->
                        <div id="paging">
                            <a href="" class="first icon-spr">제일 처음으로</a>
                            <a href="" class="prev icon-spr">이전으로</a>
                            <span class="current">1</span>
                            <a href="" class="num">2</a>
                            <a href="" class="num">3</a>
                            <a href="" class="num">4</a>
                            <a href="" class="num">5</a>
                            <a href="" class="next icon-spr">다음으로</a>
                            <a href="" class="last icon-spr">제일 마지막으로</a>
                        </div>
                        <!--/ 페이징 -->

                        <!-- 안내 -->
                        <div class="box-info">
                            <h3 class="hidden">안내</h3>
                            <h4 class="mgt0">주차위치 안내</h4>
                            <ul>
                                <li>위치확인을 클릭하시면 사용자 휴대폰 App을 통해 최종 등록된 주차위치가 아래 지도에 표시됩니다.<br /> (사용자의 GPS 활용 미동의, GPS 오동작, 지하주차장 이동 등 GPS 수신상태로 고르지 못할 경우 위치조회가 정확하지 않을 수 있습니다.)</li>
                            </ul>
                            <h4>보고서 제출</h4>
                            <ul>
                                <li>표 우측에 있는 보고서 버튼을 누르면 보고서를 작성하거나 조회할 수 있습니다.</li>
                                <li>이미 제출 된 보고서는 수정할 수 없습니다.</li>
                            </ul>
                        </div>
                        <!--/ 안내 -->
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->