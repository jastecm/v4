<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	// 운행기록 현황
    function detail(id) {
        var obj = document.getElementById("details" + id);
        if (obj) {
            obj.style.display = obj.style.display == "none" ? "table-row" : "none";
        }
    }
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>차량보고서 조회ㆍ제출</h2>
                    <span>사용차량의 운행내역ㆍ경비보고 또는 반납을 위한 보고서를 제출합니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="report-page">
                    <div class="box-layout vehicle-report-detail">
                        <div class="title">
                            <h3 class="tit3">차량사용 보고서 제출</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
                            <div class="info-txt">
                                사용차량의 운행내역ㆍ경비보고 또는 반납을 위한 보고서를 제출합니다.
                            </div>

                            <div class="user-info2">
                                <div class="img">
                                    <img src="./img/common/user-img3.png" alt="" />
                                </div>
                                <div class="info">
                                    <table class="table mgt20">
                                        <caption>등록차량 정보</caption>
                                        <colgroup>
                                            <col style="width:21.5%" />
                                            <col style="width:28.5%" />
                                            <col style="width:21.5%" />
                                            <col style="width:28.5%" />
                                        </colgroup>
                                        <tbody>
                                            <tr>
                                                <th scope="row">사용개시</th>
                                                <td>2018/05/21 16:07</td>
                                                <th scope="row">사용종료</th>
                                                <td>2018/05/21 16:07</td>
                                            </tr>
                                        </tbody>

                                        <tbody>
                                            <tr>
                                                <th scope="row">소속/지점</th>
                                                <td>카카오VS</td>
                                                <th scope="row">전화번호</th>
                                                <td>010-1111-2222</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">부서/이름</th>
                                                <td>팀원/홍길동</td>
                                                <th scope="row">휴대폰</th>
                                                <td>010-1111-2222</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">&nbsp;</th>
                                                <td>&nbsp;</td>
                                                <th scope="row">이메일</th>
                                                <td>sfasd@gmial.com</td>
                                            </tr>
                                        </tbody>

                                        <tbody>
                                            <tr>
                                                <th scope="row">업무명</th>
                                                <td colspan="3">업무명</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">활용계획</th>
                                                <td colspan="3">업무용</td>
                                            </tr>
                                        </tbody>
									</table>
								</div>
							</div>
							
							<div class="user-info2">
								<div class="img">
									<img src="./img/vehicle/L47.png" width="105" alt="" />
								</div>
								<div class="info">
									<h4 class="tit2">사용 차량정보</h4>
									<table class="table mgt20">
										<caption>사용 차량정보</caption>
										<tbody>
											<tr>
												<th scope="row">차량선택</th>
												<td>스타렉스/82도 5913</td>
											</tr>
											<tr>
												<th scope="row">ViewCar 모델명/일련번호</th>
												<td>von-S31/A000356565</td>
											</tr>
										</tbody>
									</table>
									
									<table class="table">
										<caption>사용 차량정보</caption>
										<colgroup>
											<col style="width:21.5%" />
											<col style="width:28.5%" />
											<col style="width:21.5%" />
											<col style="width:28.5%" />
										</colgroup>
										<tbody>
											<tr>
												<th scope="row">차량번호</th>
												<td>17우8749</td>
												<th scope="row">제조사/차종</th>
												<td>르노삼성/SM5 노바 LE</td>
											</tr>
											<tr>
												<th scope="row">배기량</th>
												<td>1,998 cc</td>
												<th scope="row">모델명/년식</th>
												<td>SM5/2018</td>
											</tr>
											<tr>
												<th scope="row">기어방식</th>
												<td>자동</td>
												<th scope="row">유종</th>
												<td>가솔린</td>
											</tr>
											<tr>
												<th scope="row">총주행거리</th>
												<td>949.2 km</td>
												<th scope="row">색상</th>
												<td>검정</td>
											</tr>
										</tbody>
									</table>
								</div>
                            </div>

                            <h4 class="tit mgt30">운행기록 현황</h4>
                            <p class="mgt10">해당 배차시간동안 주행한 내역입니다.</p>
                            <table class="table list mgt20">
                                <caption>운행기록 현황 리스트</caption>
                                <colgroup>
                                    <col style="width:25%" />
                                    <col />
                                    <col style="width:12%" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th scope="col">차량 및 사용자</th>
                                        <th scope="col">운행정보</th>
                                        <th scope="col">
                                            <select name="" class="arrow">
												<option value="">경고</option>
												<option value="">지역</option>
												<option value="">시간</option>
											</select>
                                        </th>
                                    </tr>
                                </thead>
                                <!-- tbody가 반복 -->
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="car-img">
                                                <span class="img"><img src="./img/vehicle/S103.png" alt="" /></span><br />
                                                <span class="num">쏘나타 63호 5703</span>
                                            </div>
                                        </td>
                                        <td class="left">
                                            <div class="driving-info with-btn">
                                                <ul>
                                                    <li>
                                                        <strong class="title w40">출발</strong>
                                                        <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                    </li>
                                                    <li>
                                                        <strong class="title w40">도착</strong>
                                                        <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                    </li>
                                                </ul>
                                                <button class="btn-img location">위치</button>
                                            </div>
                                        </td>
                                        <td>
                                            이수지역<br />
                                            <button class="states-value state04 btn-toggle" onClick="javascript:detail(1)">상세기록</button>
                                        </td>
                                    </tr>
                                    <tr id="details1" class="detail" style="display:none">
                                        <td>
                                            <div class="user-img">
                                                <span class="img"><img src="./img/common/user-default.png"alt="" /></span><br />
                                                <span class="name">maumgolf / 카카오VX</span>
                                            </div>
                                        </td>
                                        <td colspan="3" class="left">
                                            <div class="car-info">
                                                <ul class="grade">
                                                    <li><span>안전점수</span>E(0.0점)</li>
                                                    <li><span>연비점수</span>A(89.6점)</li>
                                                    <li><span>에코점수</span>E(0.0점)</li>
                                                </ul>

                                                <ul class="spec">
                                                    <li><span>주행시간</span>0시간 10분 01초</li>
                                                    <li><span>평균속도</span>0 km/h</li>
                                                    <li><span>냉각수</span>0 ℃</li>
                                                    <li><span>주행거리</span>5.4 km</li>
                                                    <li><span>최고속도</span>0 km/h</li>
                                                    <li><span>배터리</span>0.0 v</li>
                                                    <li><span>연비</span>11.4 km/ℓ</li>
                                                    <li><span>연료</span>0.5 ℓ</li>
                                                    <li><span>발전기</span>0.0 v</li>
                                                    <li><span>co2배출량</span>0 ㎎</li>
                                                    <li><span>퓨얼컷</span>0 초</li>
                                                    <li><span>공회전</span>0 초</li>
                                                    <li><span>웜업시간</span>0 초</li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                                <!--/ tbody가 반복 -->

                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="car-img">
                                                <span class="img"><img src="./img/vehicle/S103.png" alt="" /></span><br />
                                                <span class="num">쏘나타 63호 5703</span>
                                            </div>
                                        </td>
                                        <td class="left">
                                            <div class="driving-info with-btn">
                                                <ul>
                                                    <li>
                                                        <strong class="title w40">출발</strong>
                                                        <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                    </li>
                                                    <li>
                                                        <strong class="title w40">도착</strong>
                                                        <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                    </li>
                                                </ul>
                                                <button class="btn-img location">위치</button>
                                            </div>
                                        </td>
                                        <td>
                                            이수지역<br />
                                            <button class="states-value state04 btn-toggle" onClick="javascript:detail(2)">상세기록</button>
                                        </td>
                                    </tr>
                                    <tr id="details2" class="detail" style="display:none">
                                        <td>
                                            <div class="user-img">
                                                <span class="img"><img src="./img/common/user-default.png"alt="" /></span><br />
                                                <span class="name">maumgolf / 카카오VX</span>
                                            </div>
                                        </td>
                                        <td colspan="3" class="left">
                                            <div class="car-info">
                                                <ul class="grade">
                                                    <li><span>안전점수</span>E(0.0점)</li>
                                                    <li><span>연비점수</span>A(89.6점)</li>
                                                    <li><span>에코점수</span>E(0.0점)</li>
                                                </ul>

                                                <ul class="spec">
                                                    <li><span>주행시간</span>0시간 10분 01초</li>
                                                    <li><span>평균속도</span>0 km/h</li>
                                                    <li><span>냉각수</span>0 ℃</li>
                                                    <li><span>주행거리</span>5.4 km</li>
                                                    <li><span>최고속도</span>0 km/h</li>
                                                    <li><span>배터리</span>0.0 v</li>
                                                    <li><span>연비</span>11.4 km/ℓ</li>
                                                    <li><span>연료</span>0.5 ℓ</li>
                                                    <li><span>발전기</span>0.0 v</li>
                                                    <li><span>co2배출량</span>0 ㎎</li>
                                                    <li><span>퓨얼컷</span>0 초</li>
                                                    <li><span>공회전</span>0 초</li>
                                                    <li><span>웜업시간</span>0 초</li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <!-- 페이징 -->
                            <div id="paging">
                                <a href="" class="first icon-spr">제일 처음으로</a>
                                <a href="" class="prev icon-spr">이전으로</a>
                                <span class="current">1</span>
                                <a href="" class="num">2</a>
                                <a href="" class="num">3</a>
                                <a href="" class="num">4</a>
                                <a href="" class="num">5</a>
                                <a href="" class="next icon-spr">다음으로</a>
                                <a href="" class="last icon-spr">제일 마지막으로</a>
                            </div>
                            <!--/ 페이징 -->

                            <h4 class="tit mgt30">차량경비 현황</h4>
                            <p class="mgt10">해당 운행기록중 발생한 경비내역입니다.</p>
                            <table class="table list mgt20">
                                <caption>운행기록 현황 리스트</caption>
                                <colgroup>
                                    <col style="width:20%" />
                                    <col style="width:10%" />
                                    <col style="width:10%" />
                                    <col style="width:15%" />
                                    <col style="width:12%" />
                                    <col />
                                    <col style="width:10%" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th scope="col">차량정보</th>
                                        <th scope="col">사용자</th>
                                        <th scope="col">일시</th>
                                        <th scope="col">항목</th>
                                        <th scope="col">합계금액</th>
                                        <th scope="col">메모</th>
                                        <th scope="col">상세</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="car-img">
                                                <span class="img"><img src="./img/vehicle/S103.png" alt="" /></span><br />
                                                <span class="num">쏘나타 63호 5703</span>
                                            </div>
                                        </td>
                                        <td>플랫폼팀/<br />강기동</td>
                                        <td>2018.05.20</td>
                                        <td>유류비외 3건</td>
                                        <td>78,000원</td>
                                        <td class="left">메모 노출</td>
                                        <td><button class="btn-img change">상세보기</button></td>
                                    </tr>
                                </tbody>
                            </table>

                            <!-- 안내 -->
                            <div class="box-info">
                                <h3 class="hidden">안내</h3>
                                <h4 class="mgt0">출발/도착지 미기록</h4>
                                <ul>
                                    <li>GPS 미동작, 지하주차장 이동 등 GPS 수신상태가 고르지 못할 경우 출발지 및 도착지가 기록되지 않을 수 있습니다.</li>
                                </ul>
                            </div>
                            <!--/ 안내 -->

                            <h4 class="tit mgt30">차량반납 주차 위치</h4>
                            <div class="parking-location">
                                <p>전라남도 영암군 삼호읍 대불산당4로 308</p>
                                <div class="btn-function"><button class="location">차량반납 주차위치 확인</button></div>
                            </div>

                            <!-- 하단 버튼 -->
                            <div class="btn-bottom">
                                <button class="btn btn02">취소</button>
                                <button class="btn btn03">보고서 제출</button>
                            </div>
                            <!--/ 하단 버튼 -->
                        </div>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->