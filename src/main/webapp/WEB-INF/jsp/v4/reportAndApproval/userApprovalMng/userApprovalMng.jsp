<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	$("#start-date1").datepicker({
        dateformat: 'yy-mm-dd'
    });
    $("#end-date1").datepicker({
        dateformat: 'yy-mm-dd'
    });
    $("#start-date2").datepicker({
        dateformat: 'yy-mm-dd'
    });
    $("#end-date2").datepicker({
        dateformat: 'yy-mm-dd'
    });
    $("#start-date3").datepicker({
        dateformat: 'yy-mm-dd'
    });
    $("#end-date3").datepicker({
        dateformat: 'yy-mm-dd'
    });
    $("#start-date4").datepicker({
        dateformat: 'yy-mm-dd'
    });
    $("#end-date4").datepicker({
        dateformat: 'yy-mm-dd'
    });

    // 탭메뉴
    $(".tab-menu>li>a").click(function() {
        $(this).parent().addClass("on").siblings().removeClass("on");
        return false;
    });
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>결재 관리</h2>
                    <span>차량배차, 정비, 경비, 사고보고서까지 기업 차량 관리에 필요한 모든 결재를 관리합니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="report-page">
                    <div class="sign-management2">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                            <div class="item">
                                <span>
									배차 결재 대기
									<strong>10</strong>건
								</span>
                                <span>
									정비 결재 대기
									<strong>8</strong>건
								</span>
                                <span>
									정비 결제 대기
									<strong>2</strong>건
								</span>
                                <span>
									사고보고서 결재대기
									<strong>2</strong>건
								</span>
                            </div>

                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <!-- 탭메뉴 -->
                        <ul class="page-tab tab-menu col-4">
                            <!-- 차량 배차 -->
                            <li class="list01 on">
                                <a href="#">차량 배차</a>
                                <div class="tab-list">
                                    <!-- 상단 검색 -->
                                    <div class="top-search mgt20">
                                        <select name="">
												<option value="">전체</option>
											</select>
                                        <input type="text" name="" />
                                        <button class="btn btn03">조회</button>
                                    </div>
                                    <!--/ 상단 검색 -->

                                    <!-- table 버튼 -->
                                    <div class="btn-function row-2">
                                        <div class="left">
                                            <select name="">
												<option value="">최근 운행순</option>
											</select>

                                            <div class="date">
                                                <button class="active">전체</button>
                                                <button>오늘</button>
                                                <button>1주일</button>
                                                <button>1개월</button>
                                                <button>3개월</button>
                                                <button>6개월</button>
                                                <div class="direct">
                                                    <input type="text" id="start-date1" /> &nbsp;~&nbsp;
                                                    <input type="text" id="end-date1" />
                                                </div>
                                                <!-- 기획서에는 직접입력이 셀렉트박스로 되어 있는데, 사용자가 어떻게 날짜를 입력 해야 하는지 모르겠어서 임의로 변경했습니다.-->
                                            </div>
                                        </div>
                                        <div class="right">
                                            <select name="">
												<option value="">5건씩 보기</option>
											</select>
                                        </div>
                                    </div>
                                    <!--/ table 버튼 -->

                                    <!-- 체크된 항목 노출 -->
                                    <div class="checked-item">
                                        <span>1차합의 선택승인</span>
                                        <span>1차승인 선택승인</span>
                                        <span>선택반려</span>
                                    </div>
                                    <!--/ 체크된 항목 노출 -->

                                    <div class="scroll-table mgt20">
                                        <table class="table list">
                                            <caption>차량배차 리스트</caption>
                                            <thead>
                                                <tr>
                                                    <th scope="col">신청일</th>
                                                    <th scope="col">사용자</th>
                                                    <th scope="col">차량정보</th>
                                                    <th scope="col">사용(예정)일시/업무명</th>
                                                    <th scope="col">상세</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <!-- 반복 -->
                                                <tr>
                                                    <td>2018/01/16 01:02</td>
                                                    <td class="left">
                                                        <div class="driving-info">
                                                            <ul>
                                                                <li>
                                                                    <strong class="title w40">사용</strong>
                                                                    <span>복합소재연구팀/주임/강희우</span>
                                                                </li>
                                                                <li>
                                                                    <strong class="title w40">신청</strong>
                                                                    <span>복합소재연구팀/주임/강희우</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td>아반떼<br />55호 3090</td>
                                                    <td class="left">
                                                        <div class="driving-info">
                                                            <ul>
                                                                <li>
                                                                    <strong class="title">시작/종료</strong>
                                                                    <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                                </li>
                                                                <li>
                                                                    <strong class="title">업무명</strong>
                                                                    <span>SP2 MIC조색 관련</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td><button class="btn-img repair">상세</button></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="6" class="process">
                                                        <ul>
                                                            <li>
                                                                <strong>1차 합의</strong>
                                                                <div>2018/05/21 07:54<br />인사지원팀/이성윤</div>
                                                            </li>
                                                            <li>
                                                                <input type="checkbox" />
                                                                <strong>2차통보</strong>
                                                                <div><button class="btn btn02 sm">승인하기</button> </div>
                                                            </li>
                                                            <li>
                                                                <strong>반려</strong>
                                                                <div><button class="btn btn05 sm">반려하기</button></div>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                                <!--/ 반복 -->
                                                <tr>
                                                    <td>2018/01/16 01:02</td>
                                                    <td class="left">
                                                        <div class="driving-info">
                                                            <ul>
                                                                <li>
                                                                    <strong class="title w40">사용</strong>
                                                                    <span>복합소재연구팀/주임/강희우</span>
                                                                </li>
                                                                <li>
                                                                    <strong class="title w40">신청</strong>
                                                                    <span>복합소재연구팀/주임/강희우</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td>아반떼<br />55호 3090</td>
                                                    <td class="left">
                                                        <div class="driving-info">
                                                            <ul>
                                                                <li>
                                                                    <strong class="title">시작/종료</strong>
                                                                    <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                                </li>
                                                                <li>
                                                                    <strong class="title">업무명</strong>
                                                                    <span>SP2 MIC조색 관련</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td><button class="btn-img repair">상세</button></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="5" class="process">
                                                        <ul>
                                                            <li>
                                                                <strong>1차 합의</strong>
                                                                <div>2018/05/21 07:54<br />인사지원팀/이성윤</div>
                                                            </li>
                                                            <li>
                                                                <input type="checkbox" />
                                                                <strong>2차통보</strong>
                                                                <div><button class="btn btn02 sm">승인하기</button> </div>
                                                            </li>
                                                            <li>
                                                                <strong>반려</strong>
                                                                <div><button class="btn btn05 sm">반려하기</button></div>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2018/01/16 01:02</td>
                                                    <td class="left">
                                                        <div class="driving-info">
                                                            <ul>
                                                                <li>
                                                                    <strong class="title w40">사용</strong>
                                                                    <span>복합소재연구팀/주임/강희우</span>
                                                                </li>
                                                                <li>
                                                                    <strong class="title w40">신청</strong>
                                                                    <span>복합소재연구팀/주임/강희우</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td>아반떼<br />55호 3090</td>
                                                    <td class="left">
                                                        <div class="driving-info">
                                                            <ul>
                                                                <li>
                                                                    <strong class="title">시작/종료</strong>
                                                                    <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                                </li>
                                                                <li>
                                                                    <strong class="title">업무명</strong>
                                                                    <span>SP2 MIC조색 관련</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td><button class="btn-img repair">상세</button></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="5" class="process">
                                                        <ul>
                                                            <li>
                                                                <strong>1차 합의</strong>
                                                                <div>2018/05/21 07:54<br />인사지원팀/이성윤</div>
                                                            </li>
                                                            <li>
                                                                <input type="checkbox" />
                                                                <strong>2차통보</strong>
                                                                <div><button class="btn btn02 sm">승인하기</button> </div>
                                                            </li>
                                                            <li>
                                                                <strong>반려</strong>
                                                                <div><button class="btn btn05 sm">반려하기</button></div>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2018/01/16 01:02</td>
                                                    <td class="left">
                                                        <div class="driving-info">
                                                            <ul>
                                                                <li>
                                                                    <strong class="title w40">사용</strong>
                                                                    <span>복합소재연구팀/주임/강희우</span>
                                                                </li>
                                                                <li>
                                                                    <strong class="title w40">신청</strong>
                                                                    <span>복합소재연구팀/주임/강희우</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td>아반떼<br />55호 3090</td>
                                                    <td class="left">
                                                        <div class="driving-info">
                                                            <ul>
                                                                <li>
                                                                    <strong class="title">시작/종료</strong>
                                                                    <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                                </li>
                                                                <li>
                                                                    <strong class="title">업무명</strong>
                                                                    <span>SP2 MIC조색 관련</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td><button class="btn-img repair">상세</button></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="5" class="process">
                                                        <ul>
                                                            <li>
                                                                <strong>1차 합의</strong>
                                                                <div>2018/05/21 07:54<br />인사지원팀/이성윤</div>
                                                            </li>
                                                            <li>
                                                                <input type="checkbox" />
                                                                <strong>2차통보</strong>
                                                                <div><button class="btn btn02 sm">승인하기</button> </div>
                                                            </li>
                                                            <li>
                                                                <strong>반려</strong>
                                                                <div><button class="btn btn05 sm">반려하기</button></div>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2018/01/16 01:02</td>
                                                    <td class="left">
                                                        <div class="driving-info">
                                                            <ul>
                                                                <li>
                                                                    <strong class="title w40">사용</strong>
                                                                    <span>복합소재연구팀/주임/강희우</span>
                                                                </li>
                                                                <li>
                                                                    <strong class="title w40">신청</strong>
                                                                    <span>복합소재연구팀/주임/강희우</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td>아반떼<br />55호 3090</td>
                                                    <td class="left">
                                                        <div class="driving-info">
                                                            <ul>
                                                                <li>
                                                                    <strong class="title">시작/종료</strong>
                                                                    <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                                </li>
                                                                <li>
                                                                    <strong class="title">업무명</strong>
                                                                    <span>SP2 MIC조색 관련</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td><button class="btn-img repair">상세</button></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="5" class="process">
                                                        <ul>
                                                            <li>
                                                                <strong>1차 합의</strong>
                                                                <div>2018/05/21 07:54<br />인사지원팀/이성윤</div>
                                                            </li>
                                                            <li>
                                                                <input type="checkbox" />
                                                                <strong>2차통보</strong>
                                                                <div><button class="btn btn02 sm">승인하기</button> </div>
                                                            </li>
                                                            <li>
                                                                <strong>반려</strong>
                                                                <div><button class="btn btn05 sm">반려하기</button></div>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2018/01/16 01:02</td>
                                                    <td class="left">
                                                        <div class="driving-info">
                                                            <ul>
                                                                <li>
                                                                    <strong class="title w40">사용</strong>
                                                                    <span>복합소재연구팀/주임/강희우</span>
                                                                </li>
                                                                <li>
                                                                    <strong class="title w40">신청</strong>
                                                                    <span>복합소재연구팀/주임/강희우</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td>아반떼<br />55호 3090</td>
                                                    <td class="left">
                                                        <div class="driving-info">
                                                            <ul>
                                                                <li>
                                                                    <strong class="title">시작/종료</strong>
                                                                    <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                                </li>
                                                                <li>
                                                                    <strong class="title">업무명</strong>
                                                                    <span>SP2 MIC조색 관련</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td><button class="btn-img repair">상세</button></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="5" class="process">
                                                        <ul>
                                                            <li>
                                                                <strong>1차 합의</strong>
                                                                <div>2018/05/21 07:54<br />인사지원팀/이성윤</div>
                                                            </li>
                                                            <li>
                                                                <input type="checkbox" />
                                                                <strong>2차통보</strong>
                                                                <div><button class="btn btn02 sm">승인하기</button> </div>
                                                            </li>
                                                            <li>
                                                                <strong>반려</strong>
                                                                <div><button class="btn btn05 sm">반려하기</button></div>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </li>
                            <!--/ 차량 배차 -->

                            <!-- 차량 정비 -->
                            <li class="list02">
                                <a href="#">차량 정비</a>
                                <div class="tab-list">
                                    <!-- 상단 검색 -->
                                    <div class="top-search mgt20">
                                        <select name="">
													<option value="">전체</option>
												</select>
                                        <input type="text" name="" />
                                        <button class="btn btn03">조회</button>
                                    </div>
                                    <!--/ 상단 검색 -->

                                    <!-- table 버튼 -->
                                    <div class="btn-function row-2">
                                        <div class="left">
                                            <select name="">
												<option value="">최근 운행순</option>
											</select>

                                            <div class="date">
                                                <button class="active">전체</button>
                                                <button>오늘</button>
                                                <button>1주일</button>
                                                <button>1개월</button>
                                                <button>3개월</button>
                                                <button>6개월</button>
                                                <div class="direct">
                                                    <input type="text" id="start-date2" /> &nbsp;~&nbsp;
                                                    <input type="text" id="end-date2" />
                                                </div>
                                                <!-- 기획서에는 직접입력이 셀렉트박스로 되어 있는데, 사용자가 어떻게 날짜를 입력 해야 하는지 모르겠어서 임의로 변경했습니다.-->
                                            </div>
                                        </div>
                                        <div class="right">
                                            <select name="">
												<option value="">5건씩 보기</option>
											</select>
                                        </div>
                                    </div>
                                    <!--/ table 버튼 -->

                                    <!-- 체크된 항목 노출 -->
                                    <div class="checked-item">
                                        <span>1차합의 선택승인</span>
                                        <span>1차승인 선택승인</span>
                                        <span>선택반려</span>
                                    </div>
                                    <!--/ 체크된 항목 노출 -->

                                    <div class="scroll-table mgt20">
                                        <table class="table list">
                                            <caption>차량배차 리스트</caption>
                                            <thead>
                                                <tr>
                                                    <th scope="col">신청일</th>
                                                    <th scope="col">사용자</th>
                                                    <th scope="col">차량번호</th>
                                                    <th scope="col">정비요청일</th>
                                                    <th scope="col">고장내역</th>
                                                    <th scope="col">상세</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <!-- 반복 -->
                                                <tr>
                                                    <td>2018/01/16 01:02</td>
                                                    <td class="left">
                                                        <div class="driving-info">
                                                            <ul>
                                                                <li>
                                                                    <strong class="title w40">사용</strong>
                                                                    <span>복합소재연구팀/주임/강희우</span>
                                                                </li>
                                                                <li>
                                                                    <strong class="title w40">신청</strong>
                                                                    <span>복합소재연구팀/주임/강희우</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td>아반떼<br />55호 3090</td>
                                                    <td>2018/01/16</td>
                                                    <td class="left">1호 소방차 앞 1명 타이어 노후에 딸느 교체 요청 사내 모터풀에서 교체가 불가하여 외주에서 교체하고 함</td>
                                                    <td><button class="btn-img repair">상세</button></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="5" class="process">
                                                        <ul>
                                                            <li>
                                                                <strong>1차 합의</strong>
                                                                <div>2018/05/21 07:54<br />인사지원팀/이성윤</div>
                                                            </li>
                                                            <li>
                                                                <input type="checkbox" />
                                                                <strong>2차통보</strong>
                                                                <div><button class="btn btn02 sm">승인하기</button> </div>
                                                            </li>
                                                            <li>
                                                                <strong>반려</strong>
                                                                <div><button class="btn btn05 sm">반려하기</button></div>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                                <!--/ 반복 -->
                                                <tr>
                                                    <td>2018/01/16 01:02</td>
                                                    <td class="left">
                                                        <div class="driving-info">
                                                            <ul>
                                                                <li>
                                                                    <strong class="title w40">사용</strong>
                                                                    <span>복합소재연구팀/주임/강희우</span>
                                                                </li>
                                                                <li>
                                                                    <strong class="title w40">신청</strong>
                                                                    <span>복합소재연구팀/주임/강희우</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td>아반떼<br />55호 3090</td>
                                                    <td>2018/01/16</td>
                                                    <td class="left">1호 소방차 앞 1명 타이어 노후에 딸느 교체 요청 사내 모터풀에서 교체가 불가하여 외주에서 교체하고 함</td>
                                                    <td><button class="btn-img repair">상세</button></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="5" class="process">
                                                        <ul>
                                                            <li>
                                                                <strong>1차 합의</strong>
                                                                <div>2018/05/21 07:54<br />인사지원팀/이성윤</div>
                                                            </li>
                                                            <li>
                                                                <input type="checkbox" />
                                                                <strong>2차통보</strong>
                                                                <div><button class="btn btn02 sm">승인하기</button> </div>
                                                            </li>
                                                            <li>
                                                                <strong>반려</strong>
                                                                <div><button class="btn btn05 sm">반려하기</button></div>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2018/01/16 01:02</td>
                                                    <td class="left">
                                                        <div class="driving-info">
                                                            <ul>
                                                                <li>
                                                                    <strong class="title w40">사용</strong>
                                                                    <span>복합소재연구팀/주임/강희우</span>
                                                                </li>
                                                                <li>
                                                                    <strong class="title w40">신청</strong>
                                                                    <span>복합소재연구팀/주임/강희우</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td>아반떼<br />55호 3090</td>
                                                    <td>2018/01/16</td>
                                                    <td class="left">1호 소방차 앞 1명 타이어 노후에 딸느 교체 요청 사내 모터풀에서 교체가 불가하여 외주에서 교체하고 함</td>
                                                    <td><button class="btn-img repair">상세</button></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="5" class="process">
                                                        <ul>
                                                            <li>
                                                                <strong>1차 합의</strong>
                                                                <div>2018/05/21 07:54<br />인사지원팀/이성윤</div>
                                                            </li>
                                                            <li>
                                                                <input type="checkbox" />
                                                                <strong>2차통보</strong>
                                                                <div><button class="btn btn02 sm">승인하기</button> </div>
                                                            </li>
                                                            <li>
                                                                <strong>반려</strong>
                                                                <div><button class="btn btn05 sm">반려하기</button></div>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2018/01/16 01:02</td>
                                                    <td class="left">
                                                        <div class="driving-info">
                                                            <ul>
                                                                <li>
                                                                    <strong class="title w40">사용</strong>
                                                                    <span>복합소재연구팀/주임/강희우</span>
                                                                </li>
                                                                <li>
                                                                    <strong class="title w40">신청</strong>
                                                                    <span>복합소재연구팀/주임/강희우</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td>아반떼<br />55호 3090</td>
                                                    <td>2018/01/16</td>
                                                    <td class="left">1호 소방차 앞 1명 타이어 노후에 딸느 교체 요청 사내 모터풀에서 교체가 불가하여 외주에서 교체하고 함</td>
                                                    <td><button class="btn-img repair">상세</button></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="5" class="process">
                                                        <ul>
                                                            <li>
                                                                <strong>1차 합의</strong>
                                                                <div>2018/05/21 07:54<br />인사지원팀/이성윤</div>
                                                            </li>
                                                            <li>
                                                                <input type="checkbox" />
                                                                <strong>2차통보</strong>
                                                                <div><button class="btn btn02 sm">승인하기</button> </div>
                                                            </li>
                                                            <li>
                                                                <strong>반려</strong>
                                                                <div><button class="btn btn05 sm">반려하기</button></div>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2018/01/16 01:02</td>
                                                    <td class="left">
                                                        <div class="driving-info">
                                                            <ul>
                                                                <li>
                                                                    <strong class="title w40">사용</strong>
                                                                    <span>복합소재연구팀/주임/강희우</span>
                                                                </li>
                                                                <li>
                                                                    <strong class="title w40">신청</strong>
                                                                    <span>복합소재연구팀/주임/강희우</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td>아반떼<br />55호 3090</td>
                                                    <td>2018/01/16</td>
                                                    <td class="left">1호 소방차 앞 1명 타이어 노후에 딸느 교체 요청 사내 모터풀에서 교체가 불가하여 외주에서 교체하고 함</td>
                                                    <td><button class="btn-img repair">상세</button></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="5" class="process">
                                                        <ul>
                                                            <li>
                                                                <strong>1차 합의</strong>
                                                                <div>2018/05/21 07:54<br />인사지원팀/이성윤</div>
                                                            </li>
                                                            <li>
                                                                <input type="checkbox" />
                                                                <strong>2차통보</strong>
                                                                <div><button class="btn btn02 sm">승인하기</button> </div>
                                                            </li>
                                                            <li>
                                                                <strong>반려</strong>
                                                                <div><button class="btn btn05 sm">반려하기</button></div>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2018/01/16 01:02</td>
                                                    <td class="left">
                                                        <div class="driving-info">
                                                            <ul>
                                                                <li>
                                                                    <strong class="title w40">사용</strong>
                                                                    <span>복합소재연구팀/주임/강희우</span>
                                                                </li>
                                                                <li>
                                                                    <strong class="title w40">신청</strong>
                                                                    <span>복합소재연구팀/주임/강희우</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td>아반떼<br />55호 3090</td>
                                                    <td>2018/01/16</td>
                                                    <td class="left">1호 소방차 앞 1명 타이어 노후에 딸느 교체 요청 사내 모터풀에서 교체가 불가하여 외주에서 교체하고 함</td>
                                                    <td><button class="btn-img repair">상세</button></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="5" class="process">
                                                        <ul>
                                                            <li>
                                                                <strong>1차 합의</strong>
                                                                <div>2018/05/21 07:54<br />인사지원팀/이성윤</div>
                                                            </li>
                                                            <li>
                                                                <input type="checkbox" />
                                                                <strong>2차통보</strong>
                                                                <div><button class="btn btn02 sm">승인하기</button> </div>
                                                            </li>
                                                            <li>
                                                                <strong>반려</strong>
                                                                <div><button class="btn btn05 sm">반려하기</button></div>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </li>
                            <!--/ 차량 정비 -->

                            <!-- 차량 경비 -->
                            <li class="list03">
                                <a href="#">차량 경비</a>
                                <div class="tab-list">
                                    <!-- 상단 검색 -->
                                    <div class="top-search mgt20">
                                        <select name="">
											<option value="">전체</option>
										</select>
                                        <input type="text" name="" />
                                        <button class="btn btn03">조회</button>
                                    </div>
                                    <!--/ 상단 검색 -->

                                    <!-- table 버튼 -->
                                    <div class="btn-function row-2">
                                        <div class="left">
                                            <select name="">
												<option value="">최근 운행순</option>
											</select>

                                            <div class="date">
                                                <button class="active">전체</button>
                                                <button>오늘</button>
                                                <button>1주일</button>
                                                <button>1개월</button>
                                                <button>3개월</button>
                                                <button>6개월</button>
                                                <div class="direct">
                                                    <input type="text" id="start-date3" /> &nbsp;~&nbsp;
                                                    <input type="text" id="end-date3" />
                                                </div>
                                                <!-- 기획서에는 직접입력이 셀렉트박스로 되어 있는데, 사용자가 어떻게 날짜를 입력 해야 하는지 모르겠어서 임의로 변경했습니다.-->
                                            </div>
                                        </div>
                                        <div class="right">
                                            <select name="">
												<option value="">5건씩 보기</option>
											</select>
                                        </div>
                                    </div>
                                    <!--/ table 버튼 -->

                                    <!-- 체크된 항목 노출 -->
                                    <div class="checked-item">
                                        <span>1차합의 선택승인</span>
                                        <span>1차승인 선택승인</span>
                                        <span>선택반려</span>
                                    </div>
                                    <!--/ 체크된 항목 노출 -->

                                    <div class="scroll-table mgt20">
                                        <table class="table list">
                                            <caption>차량배차 리스트</caption>
                                            <thead>
                                                <tr>
                                                    <th scope="col">신청일</th>
                                                    <th scope="col">사용자</th>
                                                    <th scope="col">차량번호</th>
                                                    <th scope="col">사용일시</th>
                                                    <th scope="col">항목</th>
                                                    <th scope="col">합계금액</th>
                                                    <th scope="col">상세</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <!-- 반복 -->
                                                <tr>
                                                    <td>2018/01/16 01:02</td>
                                                    <td class="left">
                                                        <div class="driving-info">
                                                            <ul>
                                                                <li>
                                                                    <strong class="title w40">사용</strong>
                                                                    <span>복합소재연구팀/주임/강희우</span>
                                                                </li>
                                                                <li>
                                                                    <strong class="title w40">신청</strong>
                                                                    <span>복합소재연구팀/주임/강희우</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td>아반떼<br />55호 3090</td>
                                                    <td>2018/01/16</td>
                                                    <td class="left">외부 미팅</td>
                                                    <td>100,000원</td>
                                                    <td><button class="btn-img repair">상세</button></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="5" class="process">
                                                        <ul>
                                                            <li>
                                                                <strong>1차 합의</strong>
                                                                <div>2018/05/21 07:54<br />인사지원팀/이성윤</div>
                                                            </li>
                                                            <li>
                                                                <input type="checkbox" />
                                                                <strong>2차통보</strong>
                                                                <div><button class="btn btn02 sm">승인하기</button> </div>
                                                            </li>
                                                            <li>
                                                                <strong>반려</strong>
                                                                <div><button class="btn btn05 sm">반려하기</button></div>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                                <!--/ 반복 -->
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </li>
                            <!--/ 차량 경비 -->

                            <!-- 사고보고서 -->
                            <li class="list04">
                                <a href="#">사고보고서</a>
                                <div class="tab-list">
                                    <!-- 상단 검색 -->
                                    <div class="top-search mgt20">
                                        <select name="">
												<option value="">전체</option>
											</select>
                                        <input type="text" name="" />
                                        <button class="btn btn03">조회</button>
                                    </div>
                                    <!--/ 상단 검색 -->

                                    <!-- table 버튼 -->
                                    <div class="btn-function row-2">
                                        <div class="left">
                                            <select name="">
													<option value="">최근 운행순</option>
												</select>

                                            <div class="date">
                                                <button class="active">전체</button>
                                                <button>오늘</button>
                                                <button>1주일</button>
                                                <button>1개월</button>
                                                <button>3개월</button>
                                                <button>6개월</button>
                                                <div class="direct">
                                                    <input type="text" id="start-date4" /> &nbsp;~&nbsp;
                                                    <input type="text" id="end-date4" />
                                                </div>
                                                <!-- 기획서에는 직접입력이 셀렉트박스로 되어 있는데, 사용자가 어떻게 날짜를 입력 해야 하는지 모르겠어서 임의로 변경했습니다.-->
                                            </div>
                                        </div>
                                        <div class="right">
                                            <select name="">
													<option value="">5건씩 보기</option>
												</select>
                                        </div>
                                    </div>
                                    <!--/ table 버튼 -->

                                    <!-- 체크된 항목 노출 -->
                                    <div class="checked-item">
                                        <span>1차합의 선택승인</span>
                                        <span>1차승인 선택승인</span>
                                        <span>선택반려</span>
                                    </div>
                                    <!--/ 체크된 항목 노출 -->

                                    <div class="scroll-table mgt20">
                                        <table class="table list">
                                            <caption>차량배차 리스트</caption>
                                            <thead>
                                                <tr>
                                                    <th scope="col">신청일</th>
                                                    <th scope="col">사고일</th>
                                                    <th scope="col">사용자</th>
                                                    <th scope="col">차량정보</th>
                                                    <th scope="col">처리결과</th>
                                                    <th scope="col">상세</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <!-- 반복 -->
                                                <tr>
                                                    <td>2018/01/16 01:02</td>
                                                    <td>2018/01/16 01:02</td>
                                                    <td class="left">
                                                        <div class="driving-info">
                                                            <ul>
                                                                <li>
                                                                    <strong class="title w40">사용</strong>
                                                                    <span>복합소재연구팀/주임/강희우</span>
                                                                </li>
                                                                <li>
                                                                    <strong class="title w40">신청</strong>
                                                                    <span>복합소재연구팀/주임/강희우</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td>아반떼<br />55호 3090</td>
                                                    <td class="left">
                                                        <div class="driving-info">
                                                            <ul>
                                                                <li>
                                                                    <strong class="title">시작/종료</strong>
                                                                    <span>2018/05/19 05:58 ~ 2018/05/19 06:25</span>
                                                                </li>
                                                                <li>
                                                                    <strong class="title">업무명</strong>
                                                                    <span>SP2 MIC조색 관련</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <td><button class="btn-img repair">상세</button></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="5" class="process col-4">
                                                        <ul>
                                                            <li>
                                                                <strong>1차 합의</strong>
                                                                <div>2018/05/21 07:54<br />인사지원팀/이성윤</div>
                                                            </li>
                                                            <li>
                                                                <strong>2차승인</strong>
                                                                <div>연구팀/임재곤</div>
                                                            </li>
                                                            <li>
                                                                <strong>3차승인</strong>
                                                                <div>대표이사 홍길동</div>
                                                            </li>
                                                            <li>
                                                                <strong>반려</strong>
                                                                <div>&nbsp;</div>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                                <!--/ 반복 -->
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </li>
                            <!--/ 사고보고서 -->
                        </ul>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->