<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">

var _accidentKey = "${accidentKey}";
$(function(){

	
	$V4.http_post("/api/1/insurance", {}, {
		requestMethod : "GET",
		header : {
			"key" : "${_KEY}"
		},
		success : function(rtv) {
			var insurance = rtv.result;
			var strHtml = "";
			for(var i = 0 ; i < insurance.length; i++){
				strHtml += "<option value="+insurance[i].seq+">"+insurance[i].insureNm+"</option>";
			}
			$('#insuranceCompany').html(strHtml);
		},
		error : function(t) {
			console.log(t);
		}
	});
	
	
	$('#vehiclePopBtn').data({
		type : "vehicle",
		http_post_option : {
			header : {
				key : "${_KEY}"
			}
		},
		selType : "radio",
		action : {
			ok : function(arrSelect, inputObj, valueObj, _this) {
				var info = base64ToJsonObj($(arrSelect).data("info"));
				$(inputObj).val(info.plateNum);
				$(valueObj).val(info.vehicleKey);
				
				var strHtml = "";
				strHtml += '<tr>';
				strHtml += '	<td>';
                strHtml += '		<div class="car-img">';
				strHtml += '    	<span class="img"><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" alt="" /></span><br />';
				strHtml += '    	<span class="num">'+getProperty(info,'vehicleModel.modelMaster')+' '+getProperty(info,'plateNum')+'</span>';
				strHtml += '		</div>';    
				strHtml += '	</td>';
				strHtml += '	<td>';
				
				if(info.groups){
					for(i in info.groups){
						if(i!=0) strHtml += "</br>";
						strHtml += (info.groups[i].parentGroupNm)?(convertNullString(info.groups[i].parentGroupNm)):"";
						strHtml += (info.groups[i].groupNm)?("/"+convertNullString(info.groups[i].groupNm)):"";
					}
					
				}
				strHtml += '	</td>';
				
				strHtml += '	<td>'+simpleUserObjectNameViewer(info.corp.manager.vehicleManager,"/"," "," ")+'</td>';
				strHtml += '</tr>';
            
				
				$('#vehicleInfo').html(strHtml)
				
			}
		},
		targetInput : $('#plateNum'),
		targetInputValue : $('#vehicleKey')
	}).commonPop();
	
	$('#userPopBtn').data({
		type : "user",
		http_post_option : {
			header : {
				key : "${_KEY}"
			}
		},
		selType : "radio",
		action : {
			ok : function(arrSelect, inputObj, valueObj, _this) {
				var info = base64ToJsonObj($(arrSelect).data("info"));
				$(inputObj).val(info.name);
				$(valueObj).val(info.accountKey);
			}
		},
		targetInput : $('#name'),
		targetInputValue : $('#accountKey')
	}).commonPop();
	
	$('#file').on('click',function(e){
		var length = $('#fileList li').length;
		if(length >= 3){
			alert('3개 이상 파일첨부가 되지 않습니다.');
			e.preventDefault();
			return;
		}
	});
	$('#file').on('change',function(){
    	var formData = new FormData();
    	formData.append("uploadType","accident");
    	
    	for(var i = 0 ; i < $("#file")[0].files.length;i++){
    		formData.append("img"+i, $("#file")[0].files[i]);	
    	}
		
		$V4.fileUpload(formData,{
			header : {"key" : "${_KEY}"},
			success : function(data){
				var strHtml = "";
				strHtml += '<li data-key="'+data.result.img0+'" style="float: left; width: 33%; text-align: center;">';
				strHtml += '	<span>'+data.result.fileName+'</span>';
				strHtml += '	<button class="delete-x btn_reset_action imgDelete">삭제</button>';
				strHtml += '</li>';
				$('#fileList').append(strHtml);
			},error : function(t){
				alert('error');
			}
		});
    });
	
	$(document).on('click','.imgDelete',function(){
		$(this).parent().remove();
	});
	
	$('#accidentCancel').on('click',function(){
		window.history.back(-1);
	});
	
	$('#accidentReg').on('click',function(){
		if( $V4.requiredMsg($('#accountKey').val() , "사용자 선택은 필수입니다.") ) return false;
		if( $V4.requiredMsg($('#accidentDate').val() , "사고일시 선택은 필수입니다.") ) return false;
		if( $V4.requiredMsg($('#vehicleKey').val() , "차량선택은 필수입니다.") ) return false;
		
		
		var accountKey = $('#accountKey').val();
		var date = $('#accidentDate').val();
		var hour = $('#accidentHour').val();
		var minute = $('#accidentMinute').val();
		var accidentDate = date + ' ' + hour + ':' + minute;
		var vehicleKey = $('#vehicleKey').val();
		
		
		var location = $('#location').val();
		var outline = $('#outline').val();
		
		var opponentPlateNum = $('#opponentPlateNum').val();
		var opponentName = $('#opponentName').val();
		var opponentPhone = $('#opponentPhone').val();
		var insuranceCompany = $('#insuranceCompany').val();
		var insuranceReceiptNumber = $('#insuranceReceiptNumber').val();
		
		
		var sendData = {
			"accountKey" : accountKey,
			"accidentDate" : accidentDate,
			"vehicleKey" : vehicleKey,
			"location" : location,
			"outline" : outline,
			"opponentPlateNum" : opponentPlateNum,
			"opponentName" : opponentName,
			"opponentPhone" : opponentPhone,
			"insuranceCompany" : insuranceCompany,
			"insuranceReceiptNumber" : insuranceReceiptNumber
		}
		
		//이미지 파일
		var fileList = $('#fileList li');
		if(fileList.length != 0){
			for(var i = 0 ; i < fileList.length;i++){
				sendData["accidentImg"+(i+1)] = $(fileList[i]).data('key'); 
			}	
		}
		
		$V4.http_post("/api/1/accident", sendData, {
			requestMethod : "POST",
			header : {
				"key" : "${_KEY}"
			},
			success : function(rtv) {
				$V4.move('/reportApproval/vehicleAccident');
			},
			error : function(t) {
				console.log(t);
			}
		});
		
	});
	
	
});
</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>차량 사고 보고서</h2>
                    <span>회사 차량 운행 중 발생한 사고를 보고하거나, 조회합니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="report-page">
                    <div class="box-layout register-accident-report">
                        <div class="title">
                            <h3 class="tit3">사고 보고서 작성</h3>
                            <div class="btn-closed">
                                <a href="" class="icon-spr">닫기</a>
                            </div>
                        </div>
                        <div class="contents-wrap">
                            <div class="info-txt">
                                차량 정보를 정확히 입력해주세요.
                            </div>
                            <table class="table mgt20">
                                <caption>운전자 및 사고일시</caption>
                                <colgroup>
                                    <col style="width:21.5%" />
                                    <col />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <th scope="row">운전자</th>
                                        <td>
                                            <div class="division">
                                                <div class="col-3">
                                                    <input type="text" id="name" readonly="readonly" />
                                                    <input type="hidden" id="accountKey"/>
                                                </div>
                                                <div class="space">&nbsp;</div>
                                                <div class="col-9">
                                                    <button type="button" class="btn btn04 md" id="userPopBtn">검색</button>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">사고일시</th>
                                        <td>
                                            <input type="text" id="accidentDate" class="datePicker" style="width:200px" />
                                            <select id="accidentHour" style="width:50px">&nbsp;&nbsp;
												<option value="">00</option>
												<option value="00">00</option>
												<option value="01">01</option>
												<option value="02">02</option>
												<option value="03">03</option>
												<option value="04">04</option>
												<option value="05">05</option>
												<option value="06">06</option>
												<option value="07">07</option>
												<option value="08">08</option>
												<option value="09">09</option>
												<option value="10">10</option>
												<option value="11">11</option>
												<option value="12">12</option>
												<option value="13">13</option>
												<option value="14">14</option>
												<option value="15">15</option>
												<option value="16">16</option>
												<option value="17">17</option>
												<option value="18">18</option>
												<option value="19">19</option>
												<option value="20">20</option>
												<option value="21">21</option>
												<option value="22">22</option>
												<option value="23">23</option>
											</select> 시&nbsp;&nbsp;
                                            <select id="accidentMinute" style="width:50px">
												<option value="00">00</option>
												<option value="01">01</option>
												<option value="02">02</option>
												<option value="03">03</option>
												<option value="04">04</option>
												<option value="05">05</option>
												<option value="06">06</option>
												<option value="07">07</option>
												<option value="08">08</option>
												<option value="09">09</option>
												<option value="10">10</option>
												<option value="11">11</option>
												<option value="12">12</option>
												<option value="13">13</option>
												<option value="14">14</option>
												<option value="15">15</option>
												<option value="16">16</option>
												<option value="17">17</option>
												<option value="18">18</option>
												<option value="19">19</option>
												<option value="20">20</option>
												<option value="21">21</option>
												<option value="22">22</option>
												<option value="23">23</option>
												<option value="24">24</option>
												<option value="25">25</option>
												<option value="26">26</option>
												<option value="27">27</option>
												<option value="28">28</option>
												<option value="29">29</option>
												<option value="30">30</option>
												<option value="31">31</option>
												<option value="32">32</option>
												<option value="33">33</option>
												<option value="34">34</option>
												<option value="35">35</option>
												<option value="36">36</option>
												<option value="37">37</option>
												<option value="38">38</option>
												<option value="39">39</option>
												<option value="40">40</option>
												<option value="41">41</option>
												<option value="42">42</option>
												<option value="43">43</option>
												<option value="44">44</option>
												<option value="45">45</option>
												<option value="46">46</option>
												<option value="47">47</option>
												<option value="48">48</option>
												<option value="49">49</option>
												<option value="50">50</option>
												<option value="51">51</option>
												<option value="52">52</option>
												<option value="53">53</option>
												<option value="54">54</option>
												<option value="55">55</option>
												<option value="56">56</option>
												<option value="57">57</option>
												<option value="58">58</option>
												<option value="59">59</option>
											</select> 분
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <h3 class="tit2 mgt40">사고차량 선택</h3>
                            <div class="mgt10">
                                <input type="text" readonly="readonly" id="plateNum" />
                                <input type="hidden" id="vehicleKey" />
                                <button type="button" class="btn btn04 md" id="vehiclePopBtn">검색</button>
                            </div>

                            <table class="table list mgt20">
                                <caption>등록된 서비스 리스트</caption>
                                <colgroup>
                                    <col style="width:33%" />
                                    <col style="width:34%" />
                                    <col style="width:33%" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th scope="col">차량정보</th>
                                        <th scope="col">차량소속</th>
                                        <th scope="col">차량관리자</th>
                                    </tr>
                                </thead>
                                <tbody id="vehicleInfo">
                                    
                                </tbody>
                            </table>

                            <%-- <table class="table list mgt20">
                                <caption>직접입력</caption>
                                <colgroup>
                                    <col style="width:21.5%" />
                                    <col />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <th scope="row">직접입력</th>
                                        <td><textarea rows="5" class="w100" placeholder="입력"></textarea></td>
                                    </tr>
                                </tbody>
                            </table> --%>

                            <h3 class="tit2 mgt40">상세 사고 정보</h3>
                            <table class="table list mgt20">
                                <caption>상세 사고 정보 등록</caption>
                                <colgroup>
                                    <col style="width:21.5%" />
                                    <col />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <th scope="row">상세장소</th>
                                        <td><textarea id="location" rows="5" class="w100" placeholder="상세히 기입해주세요."></textarea></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">사고개요</th>
                                        <td><textarea id="outline" rows="5" class="w100" placeholder="상세히 기입해주세요."></textarea></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">사진첨부</th>
                                        <td class="add-file">
                                            <input type="file" id="file" />
                                            <label for="file">사진 첨부 (최대 3장까지 첨부가능, JPG, JPEG, PNG, GIF 파일만 업로드 가능, 최대 10Mbyte만 업로드 가능)</label>
                                            <div class="file_names">
												<ul id="fileList" style="height: 24px;border: 1px solid #e3e3e3;background-color: #f8f8f8;">
													
												</ul>
											</div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <h3 class="tit2 mgt40">사고관계 인적사항</h3>
                            <table class="table mgt20">
                                <caption>사고관계 인적사항 등록</caption>
                                <colgroup>
                                    <col style="width:21.5%" />
                                    <col style="width:28.5%" />
                                    <col style="width:21.5%" />
                                    <col style="width:28.5%" />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <th scope="row">차량번호</th>
                                        <td><input type="text" id="opponentPlateNum" /></td>
                                        <th scope="row">&nbsp;</th>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">운전자 이름</th>
                                        <td><input type="text" id="opponentName" /></td>
                                        <th scope="row">연락처</th>
                                        <td><input type="text" id="opponentPhone" /></td>
                                    </tr>
                                </tbody>
                            </table>

                            <h3 class="tit2 mgt40">보험 접수번호</h3>
                            <table class="table mgt20">
                                <caption>보험 접수번호 등록</caption>
                                <colgroup>
                                    <col style="width:21.5%" />
                                    <col style="width:28.5%" />
                                    <col style="width:21.5%" />
                                    <col style="width:28.5%" />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <th scope="row">보험사</th>
                                        <td>
                                            <select id="insuranceCompany">
												
											</select>
                                        </td>
                                        <th scope="row">접수번호</th>
                                        <td><input type="text" id="insuranceReceiptNumber" /></td>
                                    </tr>
                                </tbody>
                            </table>

                            <!-- 하단 버튼 -->
                            <div class="btn-bottom">
                                <button type="button" class="btn btn02" id="accidentCancel">취소</button>
                                <button type="button" class="btn btn03" id="accidentReg">제출</button>
                            </div>
                            <!--/ 하단 버튼 -->
                        </div>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->