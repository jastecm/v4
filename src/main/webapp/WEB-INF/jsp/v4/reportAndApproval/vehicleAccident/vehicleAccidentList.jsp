<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
var updateAccidentKey;
$(function(){
    $("#processing-result-dialog").dialog({
        autoOpen: false,
        show: {
            duration: 500
        },
        width: '960',
        modal: true
    });

    $("#processing-result-open").on("click", function() {
        $("#processing-result-dialog").dialog("open");
    });
    
    $('#insuraceResultCancelBtn').on('click',function(){
    	$("#processing-result-dialog").dialog("close");
    });
    
    $('#vehicleAccidentRegBtn').on('click',function(){
    	$V4.move('/reportApproval/vehicleAccident/reg');
    });
    
    var thead = [
	                 {"상태":"18%"},
	                 {"사고일시":"18%"},
	                 {"운전자":"18%"},
	                 {"차량정보":"18%"},
	                 {"처리결과":"18%"},
	                 {"보고확인":"10%"}
                 ];
    
    var accidentSetting = {
    		thead : thead
    		,bodyHtml : (function(data){
    			var strHtml = "";
    			$('#totalCnt').text(data.totalCnt);
    			for(i in data.result){
    				var obj = data.result[i];
    				strHtml += '<tr>';
    				strHtml += '    <td>';
    				var state = getProperty(obj,'approval.state');
					var maxStep = getProperty(obj,'approval.maxStep');
					
    				if(state != maxStep && state == "0"){
    					strHtml += '        <div class="states-value state07">사고보고 대기</div>';
    					strHtml += '<br /> '+convertDateUint(new Date(obj.regDate),_unitDate,_timezoneOffset,true);
    					strHtml += '<br />'+simpleUserObjectNameViewer(obj.account,"/"," "," ");
    				}else if(state != maxStep && state == "1"){
    					strHtml += '        <div class="states-value state04">사고보고 1차합의</div>';
    					strHtml += '<br /> '+convertDateUint(new Date(getProperty(obj,'approval.step1Date')),_unitDate,_timezoneOffset,true);
    					strHtml += '<br />'+simpleUserObjectNameViewer(obj.approval.step1,"/"," "," ");
    				}else if(state != maxStep && state == "2"){
    					strHtml += '        <div class="states-value state02">사고보고 2차합의</div>';
    					strHtml += '<br /> '+convertDateUint(new Date(getProperty(obj,'approval.step2Date')),_unitDate,_timezoneOffset,true);
    					strHtml += '<br />'+simpleUserObjectNameViewer(obj.approval.step2,"/"," "," ");
    				}else if(state != maxStep && state == "3"){
    					strHtml += '        <div class="states-value state02">사고보고 3차합의</div>';
    					strHtml += '<br /> '+convertDateUint(new Date(getProperty(obj,'approval.step3Date')),_unitDate,_timezoneOffset,true);
    					strHtml += '<br />'+simpleUserObjectNameViewer(obj.approval.step3,"/"," "," ");
    				}else if(state == "-1"){
    					strHtml += '        <div class="states-value state08">사고보고서 반려</div>';
    					strHtml += '<br /> '+convertDateUint(new Date(getProperty(obj,'approval.reserve1Date')),_unitDate,_timezoneOffset,true);
    					strHtml += '<br />'+simpleUserObjectNameViewer(obj.approval.reserve1,"/"," "," ");
    				}else if(state != maxStep && state == "4"){
    					strHtml += '        <div class="states-value state04">처리결과 1차승인</div>';
    					strHtml += '<br /> '+convertDateUint(new Date(getProperty(obj,'approval.step4Date')),_unitDate,_timezoneOffset,true);
    					strHtml += '<br />'+simpleUserObjectNameViewer(obj.approval.step4,"/"," "," ");
    				}else if(state != maxStep && state == "5"){
    					strHtml += '        <div class="states-value state02">처리결과 2차승인</div>';
    					strHtml += '<br /> '+convertDateUint(new Date(getProperty(obj,'approval.step5Date')),_unitDate,_timezoneOffset,true);
    					strHtml += '<br />'+simpleUserObjectNameViewer(obj.approval.step5,"/"," "," ");
    				}else if(state != maxStep && state == "6"){
    					strHtml += '        <div class="states-value state02">처리결과 3차승인</div>';
    					strHtml += '<br /> '+convertDateUint(new Date(getProperty(obj,'approval.step6Date')),_unitDate,_timezoneOffset,true);
    					strHtml += '<br />'+simpleUserObjectNameViewer(obj.approval.step6,"/"," "," ");
    				}else if(state == "-2"){
    					strHtml += '        <div class="states-value state08">처리결과 반려</div>';
    					strHtml += '<br /> '+convertDateUint(new Date(getProperty(obj,'approval.reserve2Date')),_unitDate,_timezoneOffset,true);
    					strHtml += '<br />'+simpleUserObjectNameViewer(obj.approval.reserve2,"/"," "," ");
    				}
    				
    				strHtml += '    </td>';
    				strHtml += '    <td>'+convertDateUint(new Date(obj.accidentDate),_unitDate,_timezoneOffset,true)+'</td>';
    				strHtml += '    <td>'+simpleUserObjectNameViewer(obj.account,"/"," "," ")+'</td>';
    				strHtml += '    <td>';
    				strHtml += '        <div class="car-img">';
    				strHtml += '            <span class="img"><img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" alt="" /></span><br />';
    				strHtml += '            <span class="num">'+getProperty(obj,'vehicle.vehicleModel.modelMaster')+'</br>'+getProperty(obj,'vehicle.plateNum')+'</span>';
    				strHtml += '        </div>';
    				strHtml += '    </td>';
    			    				
    				if(obj.insuranceResult == null || obj.insuranceResult == ""){
						if(parseInt(state) == 0 || parseInt(state) == 1){
							strHtml += '    <td><span  onclick="javascript:message();" class="important">미입력</span></td>';
						}else{
							strHtml += '    <td><span class="important resultInput" data-accidentkey="'+obj.accidentKey+'" data-accidentdata="'+obj.accidentDate+'" data-account="'+jsonObjToBase64(obj.account)+'">미입력</span></td>';
						}	
					}else{
						if(parseInt(state) >= 2){
							strHtml += '	<td>승인완료</td>';
						}else{
							strHtml += '	<td>입력(미승인)</td>';	
						}
					}
    				strHtml += '    <td>';
    				
    				if(getProperty(obj,'insuranceResult') != null){
    					strHtml += '<button type="button" class="btn-img change resultDetail" data-accidentkey="'+obj.accidentKey+'">보고확인</button>';	
    				}
    				strHtml += '    </td>';
    				
    				strHtml += '</tr>';
    			}
    			return strHtml;
    		})
    		,limit : parseInt($(".func_limitChange").val())
    		,pagePerGroup : 10 //pagingGroup size
    		,url : "/api/1/accident"
    		,param : {'searchOrder' : $(".func_order").val()}
    		,http_post_option : {
    			requestMethod : "GET"
    			,header : {key : "${_KEY}"}
    		}
    		,initSearch : true //생성과 동시에 search		
    		,loadingBodyViewer : true //로딩중!! 표시됨
    		,debugMode : true
    }
    $('#accidentList').commonList(accidentSetting);
    
    
    //처리결과 팝업 오픈
    $(document).on('click','.resultInput',function(){
    	var accidentKey = $(this).data('accidentkey');
    	var accidentDate = $(this).data('accidentdata');
    	var account = base64ToJsonObj($(this).data('account'));
    	
    	updateAccidentKey = accidentKey;
    	
    	$('#popName').text(simpleUserObjectNameViewer(account,"/"," "," "));
    	$('#popDate').text(convertDateUint(new Date(accidentDate),_unitDate,_timezoneOffset,true));
    	
    	$("#processing-result-dialog").dialog("open");
    });
    $(document).on('click','.resultDetail',function(){
    	var accidentKey = $(this).data('accidentkey');
    	
    	$V4.move('/reportApproval/vehicleAccident/detail',{"accidentKey" : accidentKey});
    });
    
    
    //처리결과 등록
    $('#insuraceResultRegBtn').on('click',function(){
    	if( $V4.requiredMsg($('#insuranceResult').val() , "처리 결과는 필수입니다.") ) return false;
    	if( $V4.requiredMsg($('#cost').val() , "비용은 필수입니다.") ) return false;
    	
    	var insuranceResult = $('#insuranceResult').val();
    	var repairDesc = $('#repairDesc').val();
    	var cost = $('#cost').val(); 
    	var costType = $('#processing-result-dialog input[name="costType"]:checked').val();
    	var sendData = {
    			"accidentKey" : updateAccidentKey,
    			"insuranceResult" : insuranceResult,
    			"repairDesc" : repairDesc,
    			"cost" : cost,
    			"costType" : costType
    	}
    	
    	$V4.http_post("/api/1/accidentResult", sendData, {
    		requestMethod : "PUT",
    		header : {
    			"key" : "${_KEY}"
    		},
    		success : function(rtv) {
    			$("#processing-result-dialog").dialog("close");
    			$("#top_btn_search").trigger("click");
    		},
    		error : function(t) {
    			console.log(t);
    		}
    	});
    });
    
    
	$("#top_btn_search").on("click",$.debounce(200,1,function(){
		
		var param = {searchType:$("#top_searchType").val() , searchText : $("#top_searchText").val()}
		
		
		var limit = parseInt($(".func_limitChange").val());	 
		param.searchOrder = $(".func_order").val();
		
		$(".searchStart").val()&&(param.searchStartDate = $(".searchStart").val());
		$(".searchEnd").val()&&(param.searchEndDate = $(".searchEnd").val());
		
		$('#accidentList').commonList("setParam",param);
		$('#accidentList').commonList("setLimit",limit).search();
		
	}));
	
    $(".func_order").on("change",function(){
		$("#top_btn_search").trigger("click");
	});
	
	$(".func_limitChange").on("change",function(){
		$("#top_btn_search").trigger("click");
	});
	
    $(".searchStart,.searchEnd").on("change",function(){
		$("#top_btn_search").trigger("click");
	});
    
    $("div.date button").on("click",function(){
		$(this).closest("div").find("button").removeClass("active");
		$(this).addClass("active");
		var d = $(this).data("day");
		
		if(d == ""){
			$(".datePicker").val("");
			$(".searchStart, .searchEnd").val("");
			
			$("#top_btn_search").trigger("click");
		}else{
			
			var rtvStartDate = calcDate(new Date() , 'd' , 0 , new Date());
			var rtvEndDate = calcDate(new Date() , 'd' , 0 , new Date());
			
			if(d == "-0D"){
			}else if(d == "-7D") rtvStartDate = calcDate(new Date() , 'd' , -7 , new Date());
			else if(d == "-1M") rtvStartDate = calcDate(new Date() , 'm' , -1 , new Date());
			else if(d == "-3M") rtvStartDate = calcDate(new Date() , 'm' , -3 , new Date());
			else if(d == "-6M") rtvStartDate = calcDate(new Date() , 'm' , -6 , new Date());
			
			$(".datePicker").eq(0).datepicker('setDate', rtvStartDate);
			$(".datePicker").eq(1).datepicker('setDate', rtvEndDate);
			
			var v = $(".datePicker").eq(0).datepicker( "getDate" ).getTime();
    		var t = $(".datePicker").eq(0).data("target");
    		$(t).val(v);
    		
    		var v = $(".datePicker").eq(1).datepicker( "getDate" ).getTime();
    		var t = $(".datePicker").eq(1).data("target");
    		$(t).val(v);
    		
    		$(t).trigger("change");
		}
		
	});
    
    $("#top_searchText").on("keyup",function(e){
		if(e.keyCode == 13) {
			$("#top_btn_search").trigger("click");
		}
	});
    
});
function message(){
	alert("‘(1차)사고보고’ 2차승인 후, 처리결과를 입력하실 수 있습니다");
}

</script>
<!-- 본문 -->
        <section id="container">
            <div id="sub-container">
                <!-- 상단 타이틀 -->
                <div class="page-header">
                    <h2>차량 사고 보고서</h2>
                    <span>회사 차량 운행 중 발생한 사고를 보고하거나, 조회합니다.</span>
                </div>
                <!--/ 상단 타이틀 -->

                <!-- 콘텐츠 본문 -->
                <div id="contents-page" class="report-page">
                    <div class="accident-report">
                        <!-- 상단 상태값 -->
                        <div class="top-state clr">
                            <div class="item">
                                <span>
									사고 보고서 작성 수
									<strong id="totalCnt">0</strong>부
								</span>
                                <span>
                            </div>

                            <div class="time">
                                <span>2018/05/27</span>
                                <span>19:53</span>
                                <span>현재</span>
                                <button class="btn btn04">새로고침</button>
                            </div>
                        </div>
                        <!--/ 상단 상태값 -->

                        <!-- 사고보고서작성 -->
                        <h3 class="tit2 mgt30">사고 보고서 작성</h3>
                        <div class="box-register">
							결재는 ‘사고보고’와 ’처리결과’ 2단계로 나눠집니다. 사고보고 승인후, 처리결과 보고를 꼭 진행해주세요.<br /><br />
                        	<div class="txt">
								<strong>1차 : 사고보고 결재</strong>
								<p>
									1차합의: 차량 관리자 (ex.인사부서 또는 총무부서 소속 관리자)<br />
									2차 승인 : 차량 담당자(차량소속 부서장)<br />
									3차 승인 : 최종 결재자 (회사 대표자 또는 임원)
								</p>
							</div>
							
                            <div class="txt">
								<strong>2차 : 처리결과 결재</strong>
								<p>
									1차합의: 차량 관리자 (ex.인사부서 또는 총무부서 소속 관리자) <br />
									 2차 승인 : 차량 담당자(차량소속 부서장)<br />
									 3차 승인 : 최종 결재자 (회사 대표자 또는 임원)
								</p>
							</div>
							
                            ※ 결재단계는 서비스 관리자 설정을 통해 단계 축소 및 지정하실 수 있습니다.

                            <div class="right">
                                <button id="vehicleAccidentRegBtn" type="button" class="btn btn01">사고보고서 작성</button>
                            </div>
                        </div>
                        <!--/ 사고보고서작성 -->

                        <!-- 상단 검색 -->
                        <h3 class="tit2 mgt30">신청 목록</h3>
                        <div class="top-search mgt20">
                            <select id="top_searchType">
								<option value="">전체</option>
								<option value="plateNum">차량번호</option>
							</select>
                            <input type="text" id="top_searchText"/>
                            <button type="button" class="btn btn03" id="top_btn_search">조회</button>
                        </div>
                        <!--/ 상단 검색 -->

                        <!-- table 버튼 -->
                        <div class="btn-function row-2">
                            <div class="left">
                                <select class="func_order">
									<option value="regDate">최근 등록순</option>
								</select>
                                <div class="date">
                                   <button type="button" class="active" data-day="">전체</button>
                                   <button type="button" data-day="-0D">오늘</button>
                                   <button type="button" data-day="-7D">1주일</button>
                                   <button type="button" data-day="-1M">1개월</button>
                                   <button type="button" data-day="-3M">3개월</button>
                                   <button type="button" data-day="-6M">6개월</button>
                                   <div class="direct">
                                       <input type="text" class="datePicker" data-target="#searchStart"/> &nbsp;~&nbsp;
                                       <input type="hidden" id="searchStart" class="searchStart"/>
                                       <input type="text" class="datePicker" data-target="#searchEnd"/>
                                       <input type="hidden" id="searchEnd" class="searchEnd"/>
                                   </div>
                               </div>
                            </div>
                            <div class="right">
                                <!-- <button type="button" class="img-btn delete" id="penaltyDelBtn">삭제</button> -->
                                <select class="func_limitChange">
									<option value="5" selected>5건씩 보기</option>
									<option value="10">10건씩 보기</option>
									<option value="20">20건씩 보기</option>
									<option value="50">20건씩 보기</option>
								</select>
                            </div>
                        </div>
                        <!--/ table 버튼 -->

                        <table class="table list mgt20" id="accidentList">
                        </table>
                    </div>
                </div>
                <!--/ 콘텐츠 본문 -->
            </div>
        </section>
        <!--/ 본문 -->
<!-- 정비내역 대량등록 팝업 -->
    <div id="processing-result-dialog" title="처리결과 보고">
        <table class="table mgt20">
            <caption>처리결과 보고</caption>
            <tbody>
                <tr>
                    <th scope="row">운전자</th>
                    <td id="popName"></td>
                </tr>
                <tr>
                    <th scope="row">사고일시</th>
                    <td id="popDate"></td>
                </tr>
            </tbody>
        </table>

        <h3 class="tit mgt30">보험 처리결과</h3>
        <table class="table mgt20">
            <caption>보험 처리결과 입력</caption>
            <tbody>
                <tr>
                    <th scope="row">처리결과</th>
                    <td><textarea id="insuranceResult" rows="5" class="w100" placeholder="상세히 기입해주세요."></textarea></td>
                </tr>
                <tr>
                    <th scope="row">수리내역</th>
                    <td><textarea id="repairDesc" rows="5" class="w100" placeholder="상세히 기입해주세요."></textarea></td>
                </tr>
                <tr>
                    <th scope="row">비용</th>
                    <td><input type="text" id="cost" style="width:200px" class="numberOnly" /> 원&nbsp;&nbsp;&nbsp;
                    <input type="radio" checked="checked" name="costType" value="1" /> 회사부담&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="costType" value="2" /> 개인부담</td>
                </tr>
            </tbody>
        </table>

        <!-- 하단 버튼 -->
        <div class="btn-bottom">
            <button type="button" class="btn btn02" id="insuraceResultCancelBtn">취소</button>
            <button type="button" class="btn btn03" id="insuraceResultRegBtn">제출</button>
        </div>
        <!--/ 하단 버튼 -->
    </div>
    <!--/ 정비내역 대량등록 팝업 -->