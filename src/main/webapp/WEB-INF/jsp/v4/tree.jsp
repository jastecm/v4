<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@include file="/WEB-INF/jsp/common/common.jsp"%>




<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta name="description" content="VIEWCAR - For Your Smart Driving">
<meta property="og:title" content="VIEWCAR">
<meta property="og:description"
	content="VIEWCAR - For Your Smart Driving">
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<link rel="stylesheet" type="text/css"
	href="https://static.jstree.com/latest/assets/dist/themes/default/style.min.css" />
<script
	src="${pageContext.request.contextPath}/common/js/jquery-1.11.2.min.js"></script>
<script src="https://static.jstree.com/latest/assets/dist/jstree.min.js"></script>

<script type="text/javascript">
	$(function() {
		
		$.ajax({
	        url: "${defaultPath}/getTree",
	        contentType : 'application/json;charset=utf-8',
	        dataType: 'json',
	        method: 'POST',
	        success : function(res){
	            var tree = unflatten(res.result);
	            console.log(tree);
	            $('#container').jstree({
	    			'core' : {
	    				'data' : tree
	    			}
	    		});
	        }
	    }); 
		
		/* $('#container').jstree({
			'core' : {
				'data' : [ {
					"id" : 1,
					"parentid" : 0,
					"text" : "a",
					"children" : [ {
						"id" : 2,
						"parentid" : 1,
						"text" : "f",
						"children" : [ {
							"id" : 4,
							"parentid" : 2,
							"text" : "b",
							"children" : [ {
								"id" : 7,
								"parentid" : 4,
								"text" : "g",
								"children" : []
							} ]
						} ]
					}, {
						"id" : 3,
						"parentid" : 1,
						"text" : "c",
						"children" : []
					}, {
						"id" : 8,
						"parentid" : 1,
						"text" : "h",
						"children" : []
					} ]
				}, {
					"id" : 5,
					"parentid" : 0,
					"text" : "d",
					"children" : []
				}, {
					"id" : 6,
					"parentid" : 0,
					"text" : "e",
					"children" : []
				} ]
			}
		}); */
	});
	
	function unflatten(arr) {
		
		  for(var j = 0 ; j < arr.length; j++){
			  arr[j].text = arr[j].groupName;
		  }
		
	      var tree = [],
	          mappedArr = {},
	          arrElem,
	          mappedElem;
	      
	      for(var i = 0 ; i < arr.length; i++) {
	        arrElem = arr[i];
	        mappedArr[arrElem.id] = arrElem;
	        mappedArr[arrElem.id]['children'] = [];
	      }
	      
	      for (var id in mappedArr) {
	        if (mappedArr.hasOwnProperty(id)) {
	          mappedElem = mappedArr[id];
	          if (mappedElem.parentid) {
	            mappedArr[mappedElem['parentid']]['children'].push(mappedElem);
	          }
	          else {
	            tree.push(mappedElem);
	          }
	        }
	      }
	      return tree;
	    }
</script>
</head>
<body>
	<h1>트리냥?</h1>
	<div id="container"></div>
</body>
</html>