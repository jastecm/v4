<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 

<fmt:setLocale value="${_LANG}" scope="request" />
<fmt:setBundle basename="properties.messages.rest.message" />

<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<c:if test="${VDAS.rule eq '0' or VDAS.rule eq '1' }">
<script type="text/javascript">
var lazy;
var lazyExpend;
var lazyDtc;
var tab;
$(document).ready(function(){
	getCnts();
	initMenuSel("M3002");

	$(".tabItem").on("click",function(){
		$(".tabItem").removeClass("on");
		$(this).addClass("on");
		
		$(".table1").hide();
		tab = $(this).data("tab");
		$("."+tab).show();

		if(tab == 'clinic') $.lazyLoader.search(lazy);
		else $.lazyLoader.search(lazyExpend);
	});
	
	$(".btn_search").on("click",function(){
		$.lazyLoader.search(lazy);
		$.lazyLoader.search(lazyExpend);
	});
	
	
	$("#searchText").on("keyup",function(e){
		if(e.keyCode == 13) $(".btn_search").trigger("click");
	});

	if(!lazy){
		lazy = $("#contentsTable1").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
				searchType : $("#searchType")
				,searchText : $("#searchText")
				,startDate : $("#startDate")
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/com/getVehicleList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.rtvList;

				console.log(data);
				var strHtml = '';
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += '<tr>';
					strHtml += '<th>';
					strHtml += '<strong class="car_img">';
					strHtml += '<img src="${pathCommon}/images/vehicle/'+vo.imgS+'.png" alt="" />';
					strHtml += '</strong>';
					strHtml += '<a href="#none" class="a_vehicle" data-key="'+vo.vehicleKey+'">';
					strHtml += '<span>'+vo.modelMaster+'</span>';
					strHtml += vo.plateNum;
					strHtml += '</a>';
					strHtml += '</th>';
					strHtml += '<th>';
					strHtml += '<span class="t5">';
					strHtml += ''+vo.year+' <fmt:message key='vehicleClinic.year'/><br>'+number_format((vo.totDistance/1000).toFixed(1))+' km';
					strHtml += '</span>';
					strHtml += '</th>';
					strHtml += '<td>';
					strHtml += '<span class="t5">';
					strHtml += ''+vo.corpName;
					strHtml += '</span>';
					strHtml += '</td>';
					strHtml += '<th>';
					
					$VDAS.http_post("/clinic/getDtcList.do",{dtcKey : vo.dtcKey},{
						sync : true
						,success : function(rDtc){
							strHtml += '<p><span style="font-weight:600;"><fmt:message key='vehicleClinic.dtcCode'/> </span>'+rDtc.result[0].dtcCode+'</p>';
							var descript = '';
							if(_lang == 'ko') descript = (rDtc.result[0].descript?rDtc.result[0].descript:'');
							else  descript = (rDtc.result[0].descript?rDtc.result[0].descriptEn:'');
							strHtml += '<p><a href="#none" class="a_detail" data-key="'+vo.dtcKey+'"><span style="font-weight:600;"><fmt:message key='vehicleClinic.dtcDescript'/> </span><b class="t6">'+descript+'</b></a></p>';
						}
					});
					
					strHtml += '</th>';
					strHtml += '<td>';
					strHtml += '<a href="#none" class="arr_detail" data-key="'+vo.vehicleKey+'">';
					strHtml += '<img src="${pathCommon}/images/btn7.jpg" alt="" />';
					strHtml += '</a>';
					strHtml += '</td>';
					strHtml += '</tr>';
				}
				return strHtml;
			}
		});
	}
	if(!lazyExpend){
		lazyExpend = $("#contentsTable2").lazyLoader({
			searchFrmObj : $("#searchFrm2")
			,searchFrmVal : {
				searchType : $("#searchType")
				,searchText : $("#searchText")
				,startDate : $("#startDate")
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/com/getVehicleList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.rtvList;
				
				console.log(data);
				var strHtml = '';
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += '<tr>';
					strHtml += '<th>';
					strHtml += '<strong class="car_img">';
					strHtml += '<img src="${pathCommon}/images/vehicle/'+vo.imgS+'.png" alt="" />';
					strHtml += '</strong>';
					strHtml += '<a href="#none" class="a_vehicle" data-key="'+vo.vehicleKey+'">';
					strHtml += '<span>'+vo.modelMaster+'</span>';
					strHtml += ''+vo.plateNum;
					strHtml += '</a>';
					strHtml += '</th>';
					strHtml += '<th>';
					strHtml += '<span class="t5">';
					strHtml += ''+vo.year+' <fmt:message key='vehicleClinic.year2'/><br>'+number_format((vo.totDistance/1000).toFixed(1))+' km';
					strHtml += '</span>';
					strHtml += '</th>';
					strHtml += '<td>';
					strHtml += '<span class="t5">';
					strHtml += ''+vo.corpName;
					strHtml += '</span>';
					strHtml += '</td>';
					strHtml += '<td>';
					strHtml += '<span class="t5">';
					if(vo.warningItemCnt == 0) strHtml += vo.warningItemNm
					else strHtml += vo.warningItemNm +' <fmt:message key='vehicleClinic.except'/>' + vo.warningItemCnt+'<fmt:message key='vehicleClinic.count'/>'; 
					strHtml += '</span>';
					strHtml += '</td>';
					strHtml += '<td>';
					strHtml += '<a href="#none" class="a_detailClinic" data-key="'+vo.vehicleKey+'"><img src="${pathCommon}/images/btn7.jpg" alt="" /></a>';
					strHtml += '</td>';
					strHtml += '</tr>';
				}
				/*
				//	점검필요
				strHtml += '<div class="statu_2">';
				strHtml += '<strong><b></b>엔진오일외 3건</strong>';
				strHtml += '<span></span>';
				//	정상
				strHtml += '<div class="statu_1">';
				strHtml += '<span></span>';
				strHtml += '</div>';
				*/
				return strHtml;
			}
		});
	}

	if(!lazyDtc){
		lazyDtc = $("#contentsTableDtc").lazyLoader({
			searchFrmObj : $("#searchDtcFrm")
			,searchFrmVal : {}
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/clinic/getDtcList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.result;
				
				console.log(data);
				var strHtml = '';
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += '<tr>';
					strHtml += '<td>';
					strHtml += ''+convertNullString(vo.regDate);
					strHtml += '</td>';
					strHtml += '<td>';
					strHtml += ''+convertNullString(vo.dtcCode);
					strHtml += '</td>';
					strHtml += '<td>';
					if(_lang == 'ko'){
						strHtml += ''+convertNullString(vo.typeName);
						strHtml += '</td>';
						strHtml += '<td>';
						strHtml += ''+convertNullString(vo.descript);
					} 
					else {
						strHtml += ''+convertNullString(vo.typeNameEn);
						strHtml += '</td>';
						strHtml += '<td>';
						strHtml += ''+convertNullString(vo.descriptEn);
					}
					strHtml += '</td>';
					strHtml += '</tr>';
				}
				return strHtml;
			}
		});
	}
	
	$("body").on("click",".a_detail",function(){
		$("#searchDtcFrm input[name=dtcKey]").val($(this).data("key"));
		$("#searchDtcFrm input[name=vehicleKey]").val("");
		$(".breakdown").show();
		$.lazyLoader.search(lazyDtc);
		
		$(".pop7").fadeIn();
		$(".black").fadeIn();
	});
	
	$("body").on("click",".arr_detail",function(){
		$("#searchDtcFrm input[name=dtcKey]").val("");
		$("#searchDtcFrm input[name=vehicleKey]").val($(this).data("key"));
		$(".breakdown").show();
		$.lazyLoader.search(lazyDtc);
		
		$(".pop7").fadeIn();
		$(".black").fadeIn();
	});
	
	
	$("body").on("click",".tab_select_btn1 a",function(){
		$(".tab_select_btn1 a").removeClass("on");
		$(this).addClass("on");
	});
	
	$("body").on("click",".btn_ok",function(){
		$(".pop7").fadeOut();
		$(".black").fadeOut();
	});
	
	$("body").on("click",".btn_accept",function(){
		$(".pop8").fadeOut();
		$(".black").fadeOut();
	});
	
	$("body").on("click",".a_vehicle",function(){
		var vehicleKey = $(this).data("key");

		$VDAS.instance_post("/drivingMng/searchVehicleDetail.do",{vehicleKey:vehicleKey});
	});

	$("body").on("click",".a_detailClinic",function(){
		$("#vehicleKey").val($(this).data("key"));
		clinicDetail();
	});

	$("body").on("click",".btn_addItem",function(){
		var name = $(this).parent("li").find(".name").val();
		var val = $(this).parent("li").find(".val").val();
		var max = $(this).parent("li").find(".max").val();
		
		$VDAS.http_post("/clinic/updateItems.do",{type:"item",sub:"add",name:name,val:val,max:max,vehicleKey:$("#vehicleKey").val()},{
			success : function(){
				clinicDetail();
			}
		});
	});

	$("body").on("click",".btn_apply",function(){
		var itemKey = $(this).data("key");
		var name = $(this).parent("li").find(".name").val();
		var val = $(this).parent("li").find(".val").val();
		var max = $(this).parent("li").find(".max").val();
		
		$VDAS.http_post("/clinic/updateItems.do",{type:"item",sub:"replace",itemKey:itemKey,name:name,val:val,max:max,vehicleKey:$("#vehicleKey").val()},{
			success : function(){
				getCnts();
				clinicDetail();
				$.lazyLoader.search(lazyExpend);
			}
		});
	});
	
	$("body").on("click",".btn_delete",function(){
		var itemKey = $(this).data("key");
		var name = $(this).parent("li").find(".name").val();
		var val = $(this).parent("li").find(".val").val();
		var max = $(this).parent("li").find(".max").val();
		
		$VDAS.http_post("/clinic/updateItems.do",{type:"item",sub:"delete",itemKey:itemKey,vehicleKey:$("#vehicleKey").val()},{
			success : function(){
				clinicDetail();
				$.lazyLoader.search(lazyExpend);
			}
		});
	});
	
	$("body").on("click",".btn_add",function(){
		strHtml = "";
		
		strHtml += '<li>';
		strHtml += '<span class="s1">';
		strHtml += '<input type="text" class="s4 name" style="width:115px" value="" />';
		strHtml += '</span><span class="s2">';
		strHtml += '<strong class="c1 graph" style="width:0%"></strong>';
		strHtml += '</span><span class="s3">';
		strHtml += '<fmt:message key='vehicleClinic.remainingDistance'/>';
		strHtml += '</span><input type="text" class="s4 val" value="" /><span class="s5">';
		strHtml += 'Km';
		strHtml += '</span><span class="s6">';
		strHtml += '/ <b><fmt:message key='vehicleClinic.exchangeCycle'/></b>';
		strHtml += '</span><input type="text" class="s4 max" value=""/><span class="s5">';
		strHtml += 'Km';
		strHtml += '</span><a href="#none" class="btn_addItem"><fmt:message key='vehicleClinic.apply'/></a>';
		strHtml += '</li>';
		
		$(".pop8_list").append(strHtml);
	});
});


function clinicDetail(){
	var vehicleKey = $("#vehicleKey").val();
	$(".black").fadeIn();
	$(".pop8").fadeIn();
	$VDAS.http_post("/clinic/getItems.do",{vehicleKey:vehicleKey},{
		success : function(r){
			var data = r.result;
			var dataCnt = data.length;
			strHtml = "";			
			$(".pop8_list").empty();			
			for(var i = dataCnt-1 ; i >= 0 ; i--){
				var vo = data[i];
				strHtml += '<li>';
				strHtml += '<span class="s1">';
				strHtml += '<input type="text" class="s4 name" style="width:115px" value="'+vo.name+'" />';
				strHtml += '</span><span class="s2">';
				if((vo.val*100)/vo.max >= 100)
					strHtml += '<strong class="c2 graph" style="width:100%"></strong>';
				else if((vo.val*100)/vo.max<10)
					strHtml += '<strong class="c1 graph" style="width:'+(vo.val*100)/vo.max+'%"></strong>';
				else
					strHtml += '<strong class="c2 graph" style="width:'+(vo.val*100)/vo.max+'%"></strong>';
				strHtml += '</span><span class="s3">';
				strHtml += '<fmt:message key='vehicleClinic.remainingDistance2'/>';
				if(vo.val>vo.max)
					strHtml += '</span><input type="text" class="s4 val" value="'+vo.max+'" /><span class="s5">';
				else
					strHtml += '</span><input type="text" class="s4 val" value="'+vo.val+'" /><span class="s5">';
				strHtml += 'Km';
				strHtml += '</span><span class="s6">';
				strHtml += '/ <b><fmt:message key='vehicleClinic.exchangeCycle2'/></b>';
				strHtml += '</span><input type="text" class="s4 max" value="'+vo.max+'"/><span class="s5">';
				strHtml += 'Km';
				strHtml += '</span><a href="#none" class="btn_apply" data-key="'+vo.itemKey+'"><fmt:message key='vehicleClinic.apply2'/></a>';
				strHtml += '</span><a href="#none" class="btn_delete" data-key="'+vo.itemKey+'"><fmt:message key='vehicleClinic.del'/></a>';
				strHtml += '</li>';
			}
			
			$(".pop8_list").append(strHtml);
		}
	});
}

function getCnts(){
	$VDAS.http_post("/clinic/getDtcWarningCnt.do",{},{
		success : function(r){
			var data = r.result;
			console.log(data);
			$("#titleCnt1").text(data.total+"<fmt:message key="vehicleClinic.vehicleCount1"/>");
			$("#titleCnt2").text(data.dtc+"<fmt:message key="vehicleClinic.vehicleCount2"/>");
			$("#titleCnt3").text(data.warning+"<fmt:message key="vehicleClinic.vehicleCount3"/>");
		}
	});
}
</script>
<input type="hidden" id="vehicleKey" value="" />
<form id="searchFrm" style="display:none">
	<input type="hidden" name="startDate" value="" />
	<input type="hidden" name="endDate" value="" />
	<input type="hidden" name="searchPeriod" value="" />
	<input type="hidden" name="searchType" value="" />
	<input type="hidden" name="searchText" value="" />
	<input type="hidden" name="searchDtc" value="1" />
</form>
<form id="searchFrm2" style="display:none">
	<input type="hidden" name="startDate" value="" />
	<input type="hidden" name="endDate" value="" />
	<input type="hidden" name="searchPeriod" value="" />
	<input type="hidden" name="searchType" value="" />
	<input type="hidden" name="searchText" value="" />
	<input type="hidden" name="searchWarningItem" value="1" />
</form>
<form id="searchDtcFrm" style="display:none;">
	<input type="hidden" name="dtcKey" value="" />
	<input type="hidden" name="vehicleKey" value="" />
</form>
<div class="right_layout">
	<h2 class="tit_sub">
		<fmt:message key='vehicleClinic.txt1'/>
		<p><fmt:message key='vehicleClinic.txt1_1'/></p>
	</h2>
	<div class="sub_top1">
		<span><fmt:message key='vehicleClinic.titleCnt1'/><b id="titleCnt1"></b></span>
		<span><fmt:message key='vehicleClinic.titleCnt2'/><b id="titleCnt2"></b></span>
		<span><fmt:message key='vehicleClinic.titleCnt3'/><b id="titleCnt3"></b></span>
		<strong id="nowDate"></strong>
	</div>
	<h3 class="h3tit"><fmt:message key='vehicleClinic.txt2'/></h3>
	<div class="search_box1">
		<form id="frmSearch" method="post" onSubmit="return false;">
			<div class="search_box1_1">
				<div class="select_type1">
					<div class="select_type1_1">
						<select class="sel" id="searchType">
							<option value=""><fmt:message key='vehicleClinic.select'/></option>
							<option value="plateNum"><fmt:message key='vehicleClinic.plateNum'/></option>
							<option value="modelMaster"><fmt:message key='vehicleClinic.modelMaster'/></option>
						</select>
					</div>
				</div><input type="text" id="searchText" class="search_input"/>
			</div><a href="#none" class="btn_search"><fmt:message key='vehicleClinic.search'/></a>
		</form>
	</div>
	<div class="tab_layout">
		<div class="pageTab">
			<ul class="tab_nav">
				<li class="tabItem on" data-tab='clinic'><a href="#none" ><fmt:message key='vehicleClinic.txt3'/></a></li>
				<li class='tabItem' data-tab='expend'><a href="#none" ><fmt:message key='vehicleClinic.txt4'/></a></li>
			</ul>
		</div>
		<div style="margin-bottom: 10px;"></div>
		<table class="table1 clinic" id="contentsTable1">
			<colgroup>
				<col style="width:195px;" />
				<col style="width:95px;" />
				<col style="width:115px;" />
				<col />
				<col style="width:153px;" />
			</colgroup>
			<thead>
				<tr>
					<th><fmt:message key='vehicleClinic.vehicleInfo'/></th>
					<th><fmt:message key='vehicleClinic.yearAndDistance'/></th>
					<th><fmt:message key='vehicleClinic.corpGroup'/></th>
					<th><fmt:message key='vehicleClinic.breakdownDesc'/></th>
					<th><fmt:message key='vehicleClinic.detailView'/></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		<table class="table1 expend" id="contentsTable2" style="display:none">
			<colgroup>
				<col style="width:195px;" />
				<col style="width:95px;" />
				<col style="width:115px;" />
				<col />
				<col style="width:100px;" />
			</colgroup>
			<thead>
				<tr>
					<th><fmt:message key='vehicleClinic.vehicleInfo2'/></th>
					<th><fmt:message key='vehicleClinic.yearAndDistance2'/></th>
					<th><fmt:message key='vehicleClinic.corpGroup2'/></th>
					<th><fmt:message key='vehicleClinic.expendables'/></th>
					<th><fmt:message key='vehicleClinic.detailView2'/></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	<div class="txt1 mab16">
		<h1><fmt:message key='vehicleClinic.txt5_1'/></h1>
		<p><img src="${pathCommon}/images/btn7.jpg" alt="" /> <fmt:message key='vehicleClinic.txt5_2'/></p><br>
		<h1><fmt:message key='vehicleClinic.txt5_3'/></h1>
		<p><fmt:message key='vehicleClinic.txt5_4'/></p>
	</div>
</div>
<div class="black"></div>
<div class="pop7" style="display:none;">
	<div class="pop6_area">
		<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt=""></a>
		<h1><fmt:message key='vehicleClinic.TroubleHstr'/></h1>
		<table class="table1 breakdown" id="contentsTableDtc">
			<colgroup>
				<col style="width:130px;"/>
				<col style="width:60px;"/>
				<col style="width:90px;"/>
				<col />
			</colgroup>
			<thead>
				<tr>
					<th><fmt:message key='vehicleClinic.dtcTime'/></th>
					<th><fmt:message key='vehicleClinic.dtcCode'/></th>
					<th><fmt:message key='vehicleClinic.dtcPart'/></th>
					<th><fmt:message key='vehicleClinic.dtcContent'/></th>
				</tr>
			</thead>
			<tbody>	
			</tbody>
		</table>
		<br><br>
		<div class="btn_type1"><a href="#none" class="btn_ok"><fmt:message key='vehicleClinic.ok'/></a></div>
	</div>
</div>
<div class="pop8" style="display:none;">
	<div class="pop_layout">
		<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt="" /></a>
		<h1><fmt:message key='vehicleClinic.txt6'/></h1>
		<h2><fmt:message key='vehicleClinic.txt6_1'/></h2>
		<div class="pop_scroll">
			<ul class="pop8_list">
			</ul>
			</div>
			<div class="btn_type1"><a href="#none" class="btn_accept"><fmt:message key='vehicleClinic.ok1'/></a><a href="#none" class="btn_add"><fmt:message key='vehicleClinic.add'/></a></div>
		</div>
	</div>
</div>
</c:if>
<c:if test="${VDAS.rule ne '0' and VDAS.rule ne '1' }">
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("M3002");
});
</script>
<div class="right_layout">
	<img src="${pathCommon}/images/${_LANG}/authError.png" alt="" style="width:100%">
</div>
</c:if>