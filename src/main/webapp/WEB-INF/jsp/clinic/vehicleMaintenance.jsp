<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<fmt:setLocale value="${_LANG}" scope="request" />
<fmt:setBundle basename="properties.messages.rest.message" />
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<c:if test="${VDAS.rule eq '0' or VDAS.rule eq '1' }">
<script type="text/javascript">
var lazy;
var lazyUser;
$(document).ready(function(){
	initMenuSel("M3001");
	getCnts();
	
	$(".btn_search").on("click",function(){
		$.lazyLoader.search(lazy);
	});
	
	$("#searchText").on("keyup",function(e){
		if(e.keyCode == 13) $(".btn_search").trigger("click");
	});
	
	if(!lazy)
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
				searchType : $("#searchType")
				,searchText : $("#searchText")
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/clinic/getMainteanceList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.result;
				console.log(data);
				
				var strHtml = '';
				var contents;
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					if(vo.maintenanceContents != null)
						contents = vo.maintenanceContents.split(",",-1);
					else
						contents = "";
					strHtml += '<tr>';
					strHtml += '<th class="car_list2">';
					strHtml += '<a href="#none" class="a_vehicleDetail" data-key="'+vo.vehicleKey+'">';
					strHtml += '<p>'+vo.modelMaster+'</p>';
					strHtml += ''+vo.plateNum;
					strHtml += '</a>';
					strHtml += '</th>';
					strHtml += '<th>'+vo.clinicDate+', '+LPAD(vo.clinicH,'0',2)+':'+LPAD(vo.clinicM,'0',2)+'</th>';
					strHtml += '<th>'+contents[0];
					if(contents.length > 1)
						strHtml +=' <fmt:message key='vehicleMaintenance.except'/> '+(contents.length - 1)+'<fmt:message key='vehicleMaintenance.count'/></th>';
					strHtml += '<td>'+number_format(vo.partsTotalPay)+' <fmt:message key='vehicleMaintenance.won'/></td>';
					strHtml += '<td>'+number_format(vo.totalWage)+' <fmt:message key='vehicleMaintenance.won1'/></td>';
					if(vo.partsQuality == '0')
						strHtml += '<td>A</td>';
					else if(vo.partsQuality == '1')
						strHtml += '<td>B</td>';
					else if(vo.partsQuality == '2')
						strHtml += '<td>C</td>';
					strHtml += '<td>'+vo.clinicNm+'</td>';
					strHtml += '<td>';
					strHtml += '<a href="#" class="a_detail" data-id="'+vo.id+'"><img src="${pathCommon}/images/btn7.jpg" alt="" /></a>';
					strHtml += '</td>'; 
					strHtml += '</tr>';
				}
				
				return strHtml;
			}
		});
	
	$("body").on("click",".a_reg",function(){
		$VDAS.instance_post("/clinic/vehicleMaintenanceRegister.do",{});
	});
	
	$("body").on("click",".a_detail",function(){
		var id = $(this).data("id");
		$VDAS.instance_post("/clinic/vehicleMaintenanceDetail.do",{id:id});
	});
	
	$("body").on("click",".a_vehicleDetail",function(){
		var vehicleKey = $(this).data("key");
		$VDAS.instance_post("/drivingMng/searchVehicleDetail.do",{vehicleKey:vehicleKey});
	});
});

function getCnts(){
	$VDAS.http_post("/clinic/getCnts.do",{},{
		success : function(r){
			var cnt = r.result;
			$("#clinicCnt").text(""+cnt.clinicCnt+"<fmt:message key='vehicleMaintenance.count'/>");
			$("#maintenanceCnt").text(""+cnt.maintenanceCnt+"<fmt:message key='vehicleMaintenance.count'/>");
		}
	});
}

</script>
<form id="searchFrm" style="display:none">
	<input type="hidden" name="searchType" value="" />
	<input type="hidden" name="searchText" value="" />
</form>
<div class="right_layout">
	<h2 class="tit_sub">
		<fmt:message key='vehicleMaintenance.txt1'/>
		<p><fmt:message key='vehicleMaintenance.txt1_1'/></p>
	</h2>
	<div class="sub_top1">
		<span><fmt:message key='vehicleMaintenance.clinicCnt'/><b id="clinicCnt"></b></span><span><fmt:message key='vehicleMaintenance.maintenanceCnt'/><b id="maintenanceCnt"></b></span>
		<strong id="nowDate"></strong>
	</div>
	<h3 class="h3tit"><fmt:message key='vehicleMaintenance.txt2'/></h3>
	<div class="box1">
		<fmt:message key='vehicleMaintenance.txt3'/>
		<p><fmt:message key='vehicleMaintenance.txt3_1'/></p>
		<a href="#" class="a_reg"><fmt:message key='vehicleMaintenance.txt4'/></a>
	</div>
	<h3 class="h3tit"><fmt:message key='vehicleMaintenance.txt5'/></h3>
	<div class="search_box1">
		<form id="frmSearch" method="post" onSubmit="return false;">
			<div class="search_box1_1">
				<div class="select_type1">
					<div class="select_type1_1">
						<select class="sel" id="searchType">
							<option value=""><fmt:message key='vehicleMaintenance.select'/></option>
							<option value="plateNum"><fmt:message key='vehicleMaintenance.plateNum'/></option>
							<option value="modelMaster"><fmt:message key='vehicleMaintenance.modelMaster'/></option>
						</select>
					</div>
				</div><input type="text" id="searchText" class="search_input" />
			</div><a href="#none" class="btn_search"><fmt:message key='vehicleMaintenance.search'/></a>
		</form>
	</div>
	<table class="table1" id="contentsTable">
		<colgroup>
			<col style="width:138px;" />
			<col style="width:130px;" />
			<col />
			<col style="width:92px;" />
			<col style="width:92px;" />
			<col style="width:60px;" />
			<col style="width:96px;" />
			<col style="width:50px;" />
		</colgroup>
		<thead>
			<tr>
				<th><fmt:message key='vehicleMaintenance.vehicleInfo'/></th>
				<th><fmt:message key='vehicleMaintenance.maintenanceDate'/></th>
				<th><fmt:message key='vehicleMaintenance.maintenanceItem'/></th>
				<th><fmt:message key='vehicleMaintenance.partsAmount'/></th>
				<th><fmt:message key='vehicleMaintenance.repairAmount'/></th>
				<th><fmt:message key='vehicleMaintenance.rank'/></th>
				<th><fmt:message key='vehicleMaintenance.maintenanceShop'/></th>
				<th><fmt:message key='vehicleMaintenance.detailDescript'/></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
	<div class="txt1">
		<h1><fmt:message key='vehicleMaintenance.txt6'/></h1>
		<p><img src="${pathCommon}/images/btn7.jpg" alt="" /> <fmt:message key='vehicleMaintenance.txt6_1'/></p>
	</div>
</div>
</c:if>
<c:if test="${VDAS.rule ne '0' and VDAS.rule ne '1' }">
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("M3001");
});
</script>
<div class="right_layout">
	<img src="${pathCommon}/images/${_LANG}/authError.png" alt="" style="width:100%">
</div>
</c:if>