<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 

<fmt:setLocale value="${_LANG}" scope="request" />
<fmt:setBundle basename="properties.messages.rest.message" />

<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/index.js"></script>

<script type="text/javascript">
var lazy;
var lazyRoute;
var lazySupplies;
$(document).ready(function(){
	initMenuSel("M3001");
	
	getVehicleInfo();
	getMaintenanceItem();
	
	$("#drivingDistance").val(parseInt($("#drivingDistance").val()));
	
	$("body").on("click",".bt_add",function(){
		strHtml = '';
		$(".selectItem").removeClass("add");
		
		strHtml += '<div class="repair_box2_2_3">';
		strHtml += '<div class="maintenancelList">';
		strHtml += '<span class="tit"><fmt:message key='vehicleMaintenanceDetail.maintenanceItem'/></span>';
		strHtml += '<div class="select_type7">';
		strHtml += '<select class="select form selectItem add" style="width:131px;height:27px;">';
		strHtml += '<option value=""><fmt:message key='vehicleMaintenanceDetail.select'/></option>';
		strHtml += '</select>';
		strHtml += '</div><input type="text" class="input1 itemDetail" style="width:338px;" /><a href="#none" class="btn_del"><img src="${pathCommon}/images/${_LANG}/btn_delete.jpg" alt="" /></a>';
		strHtml += '<div class="r3" style="width:510px; margin-left:80px;">';
		strHtml += '<span class="r3_1"><fmt:message key='vehicleMaintenanceDetail.partsRank'/></span>';
		strHtml += '<div class="select_type5">';
		strHtml += '<select class="sel partRank" style="width:49px; height:25px;">';
		strHtml += '<option value="0"><fmt:message key='vehicleMaintenanceDetail.partRank1'/></option>';
		strHtml += '<option value="1"><fmt:message key='vehicleMaintenanceDetail.partRank2'/></option>';
		strHtml += '<option value="2"><fmt:message key='vehicleMaintenanceDetail.partRank3'/></option>';
		strHtml += '</select>';
		strHtml += '</div><span class="r3_2"><fmt:message key='vehicleMaintenanceDetail.count'/></span><input type="text" class="numberonly partCount" style="width:15px"/>';
		strHtml += '<span class="r3_3"><fmt:message key='vehicleMaintenanceDetail.partsAmount'/></span><input type="text" class="input2 partCost" /><span class="r3_4"><fmt:message key='vehicleMaintenanceDetail.won'/></span><span class="r3_5"><fmt:message key='vehicleMaintenanceDetail.repair'/></span><input type="text" class="input2 wagesCost" /><span class="r3_4"><fmt:message key='vehicleMaintenanceDetail.won1'/></span>';
		strHtml += '</div>';
		strHtml += '</div>';
		
		$("#list").append(strHtml);

		var vehicleKey = $("#vehicleKey").val();
		$VDAS.ajaxCreateSelect("/vehicle/getVehicleItemsCode.do",{vehicleKey:vehicleKey},$(".add"),false,"<fmt:message key="vehicleMaintenanceDetail.select2"/>","","${_LANG}",false,null);	
	});
	
	$("body").on("click",".rdo_sel",function(){
		$("#vehicleKey").val($(this).val());
	});
	
	
	$("body").on("change",".partCost",function(){
		TotalPartCost();
	    TotalCost();
	});
	
	$("body").on("change",".wagesCost",function(){
		TotalWagesCost();
	    TotalCost();
	});
	
	$("body").on("click",".btn_cancel",function(){
		$(".pop1").fadeOut();
	});

	$("body").on("click",".btn_pop",function(){
		$(".pop1").fadeIn();
		$(".black").fadeIn();
		$.lazyLoader.search(lazy);
	});
	
	$("body").on("click",".btn_del",function(){
		$(this).parent().parent().remove();
		
		TotalPartCost();
		TotalWagesCost();
	    TotalCost();
	});
	
	$(".btn_car_detail").on("click",function(){
		var key = $("#vehicleKey").val();
		$VDAS.instance_post("/drivingMng/searchVehicleDetail.do",{vehicleKey:key});
	});
	
	$("body").on("click",".btn_reg",function(){
		var vaildChk = true;

		$("#editFrm").find("input").val("");

		$("#editFrm input[name=maintenanceId]").val($("#maintenanceId").val());
		$("#editFrm input[name=vehicleKey]").val($("#vehicleKey").val());
		$("#editFrm input[name=clinicNm]").val($("#clinicNm").val());
		$("#editFrm input[name=drivingDistance]").val($("#drivingDistance").val()*1000);
		$("#editFrm input[name=selectDate]").val(replaceAll($("#selectDate").val(),"/",""));
		$("#editFrm input[name=selectH]").val($("#selectH").val());
		$("#editFrm input[name=selectM]").val($("#selectM").val());
		$("#editFrm input[name=descript]").val($("#descript").text());

		$(".maintenancelList").each(function(index){
			if(($(".selectItem:eq("+index+")").val() == "" || $(".selectItem:eq("+index+")").val() == null)
					&& ($(".itemDetail:eq("+index+")").val() == '' || $(".itemDetail:eq("+index+")").val() == null)){
	    		alert("<fmt:message key="vehicleMaintenanceDetail.txt1"/>");
	    		vaildChk = false;
				return false;
			}
			
			var inputObj = $("#editFrm input[name=selectItem]");
			inputObj.val(inputObj.val() + $(".selectItem:eq("+index+")").val());
            if(index != $(".selectItem").length - 1) inputObj.val(inputObj.val()+",");

            inputObj = $("#editFrm input[name=itemDetail]");
            if(($(".selectItem:eq("+index+")").val() != null && $(".selectItem:eq("+index+")").val() != '')
            		&& ($(".itemDetail:eq("+index+")").val() == null || $(".itemDetail:eq("+index+")").val() == ''))
				inputObj.val(inputObj.val() + $(".selectItem:eq("+index+") option:selected").text());
            else inputObj.val(inputObj.val() + $(".itemDetail:eq("+index+")").val());
            if(index != $(".itemDetail").length - 1) inputObj.val(inputObj.val()+",");

            inputObj = $("#editFrm input[name=partRank]");
			inputObj.val(inputObj.val() + $(".partRank:eq("+index+")").val());
            if(index != $(".partRank").length - 1) inputObj.val(inputObj.val()+",");
            
			inputObj = $("#editFrm input[name=partsType]");
			if($(".selectItem:eq("+index+")").val() == null || $(".selectItem:eq("+index+")").val() == "")
				inputObj.val(inputObj.val() + "0");
			else
				inputObj.val(inputObj.val() + "1");
            if(index != $(".partsType").length - 1) inputObj.val(inputObj.val()+",");

            inputObj = $("#editFrm input[name=partCount]");
			if($(".partCount:eq("+index+")").val() != null && $(".partCount:eq("+index+")").val() != '')
				inputObj.val(inputObj.val() + $(".partCount:eq("+index+")").val());
			else inputObj.val(inputObj.val() + "0");
            if(index != $(".partCount").length - 1) inputObj.val(inputObj.val()+",");

            inputObj = $("#editFrm input[name=partCost]");
			if($(".partCost:eq("+index+")").val() != null && $(".partCost:eq("+index+")").val() != '')
				inputObj.val(inputObj.val() + $(".partCost:eq("+index+")").val());
			else inputObj.val(inputObj.val() + "0");
            if(index != $(".partCost").length - 1) inputObj.val(inputObj.val()+",");
            
			inputObj = $("#editFrm input[name=wagesCost]");
			if($(".wagesCost:eq("+index+")").val() != null && $(".wagesCost:eq("+index+")").val() != '')
				inputObj.val(inputObj.val() + $(".wagesCost:eq("+index+")").val());
			else inputObj.val(inputObj.val() + "0");
            if(index != $(".wagesCost").length - 1) inputObj.val(inputObj.val()+",");
    	});
    	console.log($("#editFrm").serialize());
   	
    	if(vaildChk){
        	$VDAS.http_post("/clinic/updateMaintenance.do",$("#editFrm").serialize(),{
    			success:function(r){
    				$VDAS.instance_post("/clinic/vehicleMaintenance.do",{});
    			},
    			error:function(r){
    			}
    		});
    	}
	});
});

function getMaintenanceItem(){
	var maintenanceId = $("#maintenanceId").val();
	var vehicleKey = $("#vehicleKey").val();
	
	$VDAS.http_post("/clinic/getMaintenanceItem.do",{maintenanceId:maintenanceId},{
		success : function(r){
			var data = r.result;
			console.log(data);
			var firstIdx = data[0];
			var rank = firstIdx.partsQuality;
			
			$(".itemDetail").val(firstIdx.maintenanceContents);
			$(".partCount").val(firstIdx.partsCnt);
			$(".partCost").val(firstIdx.partsPay);
			$(".wagesCost").val(firstIdx.wage);

			$VDAS.ajaxCreateSelect("/vehicle/getVehicleItemsCode.do",{vehicleKey:vehicleKey},$(".selectItem"),false,"<fmt:message key="vehicleMaintenanceDetail.select3"/>",firstIdx.itemIdx,"${_LANG}",false,null);	
			var strHtml;
			
			for(var i = 1;i < data.length;i++){
				var vo = data[i];
				strHtml = '';
				var quality = vo.partsQuality;
				$(".selectItem").removeClass("add");
				
				strHtml += '<div class="repair_box2_2_3">';
				strHtml += '<div class="maintenancelList">';
				strHtml += '<span class="tit"><fmt:message key='vehicleMaintenanceDetail.maintenanceItem2'/></span>';
				strHtml += '<div class="select_type7">';
				strHtml += '<select class="select form selectItem add" style="width:131px;height:27px;">';
				strHtml += '<option value=""><fmt:message key='vehicleMaintenanceDetail.select4'/></option>';
				strHtml += '</select>';
				strHtml += '</div><input type="text" class="input1 itemDetail" style="width:338px;" value="'+vo.maintenanceContents+'" /><a href="#none" class="btn_del"><img src="${pathCommon}/images/${_LANG}/btn_delete.jpg" alt="" /></a>';
				strHtml += '<div class="r3" style="width:510px; margin-left:80px;">';
				strHtml += '<span class="r3_1"><fmt:message key='vehicleMaintenanceDetail.partsRank2'/></span>';
				strHtml += '<div class="select_type5">';
				strHtml += '<select class="sel partRank" style="width:49px; height:25px;">';
				strHtml += '<option value="0"';
				if(quality == '0')
					strHtml += 'selected';
				strHtml += '>A</option>';
				strHtml += '<option value="1"';
				if(quality == '1')
					strHtml += 'selected';
				strHtml += '>B</option>';
				strHtml += '<option value="2"';
				if(quality == '2')
					strHtml += 'selected';
				strHtml += '>C</option>';
				strHtml += '</select>';
				strHtml += '</div><span class="r3_2"><fmt:message key='vehicleMaintenanceDetail.count2'/></span><input type="text" class="numberonly partCount" style="width:15px" value="'+vo.partsCnt+'"/>';
				strHtml += '<span class="r3_3"><fmt:message key='vehicleMaintenanceDetail.partsAmount2'/></span><input type="text" class="input2 partCost" value="'+vo.partsPay+'"/><span class="r3_4"><fmt:message key='vehicleMaintenanceDetail.won2'/></span><span class="r3_5"><fmt:message key='vehicleMaintenanceDetail.repair'/></span><input type="text" class="input2 wagesCost" value="'+vo.wage+'"/><span class="r3_4"><fmt:message key='vehicleMaintenanceDetail.won4'/></span>';
				strHtml += '</div>';
				strHtml += '</div>';
				
				$("#list").append(strHtml);
				$VDAS.ajaxCreateSelect("/vehicle/getVehicleItemsCode.do",{vehicleKey:vehicleKey},$(".add"),false,"<fmt:message key='vehicleMaintenanceDetail.select'/>",vo.itemIdx,"${_LANG}",false,null);	
			}
			TotalWagesCost();
			TotalPartCost();
			TotalCost();
		}
	});
}

function getVehicleInfo(){
	var vehicleKey = $("#vehicleKey").val();

	$VDAS.http_post("/vehicle/getVehicleInfo.do",{vehicleKey:vehicleKey},{
		success : function(r){
			var obj = r.result;

			console.log(obj);
			$("#modelNm").text(""+convertNullString(obj.deviceSeries)+"/"+convertNullString(obj.deviceSn));
			$("#vehicleImg").attr("src",'${pathCommon}/images/vehicle/'+obj.imgL+'.png');
			$("#plateNum").text(""+obj.plateNum);
			$("#corpNm").text(""+obj.corpName);
			$("#volume").text(""+number_format(obj.volume)+" cc");
			$("#transmission").text(""+obj.transmission);
			$("#totDistance").text(""+number_format((obj.totDistance/1000).toFixed(1))+" km");
			$("#manufacture").text(""+obj.manufacture);
			$("#modelMaster").text(""+obj.modelMaster+"/"+obj.year);
			$("#fuelType").text(""+obj.fuelType);
			$("#color").text(convertNullString(obj.color));
			//$(".car_detail_left").append('<img src="${pathCommon}/images/vehicle/'+obj.imgL+'.png" alt="" />');
		}
	});
}

function TotalPartCost(){
	var totalPartCost = 0;
	$(".partCost").each(function(index) {
		totalPartCost += $(".partCost:eq("+index+")").val()?parseInt($(".partCost:eq("+index+")").val()):0;
	});
	$("#totalPartCost").val(totalPartCost);
}

function TotalWagesCost(){
	var totalWagesCost = 0;
    $(".wagesCost").each(function(index) {
    	totalWagesCost  += $(".wagesCost:eq("+index+")").val()?parseInt($(".wagesCost:eq("+index+")").val()):0;
    });
    $("#totalWagesCost").val(totalWagesCost);
}

function TotalCost(){
	var partCost = $("#totalPartCost").val()?parseInt($("#totalPartCost").val()):0;
	var wagesCost = $("#totalWagesCost").val()?parseInt($("#totalWagesCost").val()):0;
	var cost = partCost + wagesCost;

	$("#totalCost").val(cost);
}

</script>
<input type="hidden" id="vehicleKey" value="${reqVo.vehicleKey}" />
<input type="hidden" id="maintenanceId" value="${reqVo.id}" />
<form id="editFrm" style="display:none">
	<input type="hidden" name="maintenanceId" value="" />
	<input type="hidden" name="vehicleKey" value="" />
	<input type="hidden" name="clinicNm" value="" />
	<input type="hidden" name="drivingDistance" value="" />
	<input type="hidden" name="selectDate" value="" />
	<input type="hidden" name="selectH" value="" />
	<input type="hidden" name="selectM" value="" />
	<input type="hidden" name="descript" value="" />
	
	<input type="hidden" name="selectItem" value="" />
	<input type="hidden" name="partsType" value="" />
	<input type="hidden" name="itemDetail" value="" />
	<input type="hidden" name="partRank" value="" />
	<input type="hidden" name="partCount" value="" />
	<input type="hidden" name="partCost" value="" />
	<input type="hidden" name="wagesCost" value="" />
</form>
<div class="right_layout">
	<div class="detail_view">
		<a href="javascript:window.history.back();" class="btn_close"><img src="${pathCommon}/images/close.jpg" alt="" /></a>
		<h2 class="h2tit">
			<fmt:message key='vehicleMaintenanceDetail.txt2'/>
		</h2>
		<div class="step_box sub_top1" style="text-align:left; padding:7px 12px 6px 21px;">
			<span><fmt:message key='vehicleMaintenanceDetail.txt3'/></span>
		</div>
		<div class="repair_layout">
			<div class="repair_left">
				<!-- img size : 가로 308*212-->
				<img src="${pathCommon}/images/${_LANG}/car_img6.jpg" alt="" id="vehicleImg"/>
			</div>
			<ul class="repair_right">
				<li class="r1">
					<div>
						<span><fmt:message key='vehicleMaintenanceDetail.vehicleSelect'/></span><i id="plateNum"></i><a href="#" class="btn_car_detail"><fmt:message key='vehicleMaintenanceDetail.vehicleSelectDetail'/></a>
					</div>
					<div>
						<span><fmt:message key='vehicleMaintenanceDetail.modelNm'/></span><i id="modelNm"></i>
					</div>
				</li>
				<li class="r2">
					<div>
						<p><span><fmt:message key='vehicleMaintenanceDetail.corpNm'/></span><i id="corpNm"></i></p>
						<p><span><fmt:message key='vehicleMaintenanceDetail.volume'/></span><i id="volume"></i></p>
						<p><span><fmt:message key='vehicleMaintenanceDetail.transmission'/></span><i id="transmission"></i></p>
						<p><span><fmt:message key='vehicleMaintenanceDetail.totDistance'/></span><i id="totDistance"></i></p>
					</div>
					<div>
						<p><span><fmt:message key='vehicleMaintenanceDetail.manufacture'/></span><i id="manufacture"></i></p>
						<p><span><fmt:message key='vehicleMaintenanceDetail.modelMaster'/></span><i id="modelMaster"></i></p>
						<p><span><fmt:message key='vehicleMaintenanceDetail.fuelType'/></span><i id="fuelType"></i></p>
						<p><span><fmt:message key='vehicleMaintenanceDetail.color'/></span><i id="color"></i></p>
					</div>
				</li>
			</ul>
		</div>
		<div><img src="${pathCommon}/images/${_LANG}/h2_5.png" alt="차량정비 내용 입력" /></div>
		<div class="repair_layout2">
			<div class="repair_box1">
				<div class="tit"><fmt:message key='vehicleMaintenanceDetail.maintenanceContent'/></div>
				<div class="repair_box2">
					<div class="repair_box2_1">
						<span class="tit"><fmt:message key='vehicleMaintenanceDetail.repairShop'/></span><input type="text" id="clinicNm" value="${reqVo.clinicNm }"/>
						<span class="tit" style="margin-left:20px"><fmt:message key='vehicleMaintenanceDetail.drivingDistance'/></span><input type="text" class="numberOnly" id="drivingDistance" value="${reqVo.distance/1000 }" />
					</div>
					<div class="car_info5 repair_box2_1">
						<span class="tit"><fmt:message key='vehicleMaintenanceDetail.maintenanceDate'/></span>
						<div class="cal_layout">
							<div class="cal_area">
								<div>
									<input type="text" id="selectDate" class="date datePicker" style="width:65px; height:25px; border:1px solid #c0c0c0;" value="${reqVo.clinicDate }"/>
									<div class="select_type5">
										<select class="sel" id="selectH">
											<c:forEach begin="0" end="23" step="1" var="i">
												<option value="${i}" <c:if test='${i eq reqVo.clinicH}'>selected</c:if> >${i}</option>
											</c:forEach>
										</select>
									</div><span><fmt:message key='vehicleMaintenanceDetail.hour'/></span>
									<div class="select_type5">
										<select class="sel" id="selectM">
											<c:forEach begin="0" end="59" step="1" var="i">
												<option value="${i}" <c:if test='${i eq reqVo.clinicM}'>selected</c:if> >${i}</option>
											</c:forEach>
										</select>
									</div><span><fmt:message key='vehicleMaintenanceDetail.minute'/></span>
								</div>
								<p><fmt:message key='vehicleMaintenanceDetail.txt4'/></p>
							</div>
						</div>
					</div>
					<div class="car_info4 repair_box2_1">
						<span class="tit"><fmt:message key='vehicleMaintenanceDetail.carInfo'/></span><div class="car_info4_1">
							<div class="car_info4_3">
								<textarea id="descript">${reqVo.clinicContents }</textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="repair_layout2 repair_layout2_2">
				<div class="repair_box1">
					<div class="tit"><fmt:message key='vehicleMaintenanceDetail.detailDesc'/></div>
					<div class="repair_box2">
						<div class="repair_box2_1 repair_box2_2" id="list">
							<div class="maintenancelList">
								<span class="tit"><fmt:message key='vehicleMaintenanceDetail.maintenanceItem3'/></span>
								<div class="select_type7">
									<select class="select form selectItem" style="width:131px;height:27px;">
										<option value=""><fmt:message key='vehicleMaintenanceDetail.select5'/></option>
									</select>
								</div><input type="text" class="input1 itemDetail" style="width:338px;" /><a href="#none" class="bt_add"><img src="${pathCommon}/images/${_LANG}/btn_add.jpg" alt="" /></a>
								<div class="r3" style="width:510px; margin-left:80px;">
									<span class="r3_1"><fmt:message key='vehicleMaintenanceDetail.partsRank3'/></span>
									<div class="select_type5">
										<select class="sel partRank">
											<option value="0" <c:if test="${rank eq '0'}">selected</c:if>><fmt:message key='vehicleMaintenanceDetail.partRank1_1'/></option>
											<option value="1" <c:if test="${rank eq '1'}">selected</c:if>><fmt:message key='vehicleMaintenanceDetail.partRank2_1'/></option>
											<option value="2" <c:if test="${rank eq '2'}">selected</c:if>><fmt:message key='vehicleMaintenanceDetail.partRank3_1'/></option>
										</select>
									</div><span class="r3_2"><fmt:message key='vehicleMaintenanceDetail.partCount'/></span><input type="text" class="numberonly partCount numberOnly" style="width:15px"/>
									<span class="r3_3"><fmt:message key='vehicleMaintenanceDetail.partsAmount3'/></span>
									<input type="text" class="input2 partCost numberOnly" />
									<span class="r3_4"><fmt:message key='vehicleMaintenanceDetail.won5'/></span>
									<span class="r3_5"><fmt:message key='vehicleMaintenanceDetail.repair2'/></span>
									<input type="text" class="input2 wagesCost" />
									<span class="r3_4"><fmt:message key='vehicleMaintenanceDetail.won6'/></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> 
		<div class="repair_layout3">
			<div class="d1">
				<span class="t1"><fmt:message key='vehicleMaintenanceDetail.totalCost'/></span><input type="text" id="totalCost" readonly/><strong><fmt:message key='vehicleMaintenanceDetail.won7'/></strong>
			</div>
			<div class="d2">
				<span class="t1"><fmt:message key='vehicleMaintenanceDetail.totalPartCost'/></span>
				<input type="text" id="totalPartCost" readonly/>
				<span class="t2">+</span>
				<input type="text" id="totalWagesCost" readonly/>
				<strong><fmt:message key='vehicleMaintenanceDetail.won8'/></strong>
				<p><fmt:message key='vehicleMaintenanceDetail.txt5'/></p>
			</div>
		</div>
		<div class="btn_area2"><a href="javascript:window.history.back();" class="btn_cancel"><fmt:message key='vehicleMaintenanceDetail.cancel'/></a>
		<a href="#none" class="btn_reg"><fmt:message key='vehicleMaintenanceDetail.regComplete'/></a></div>
	</div>
</div>