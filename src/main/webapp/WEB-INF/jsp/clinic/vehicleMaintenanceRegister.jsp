<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<fmt:setLocale value="${_LANG}" scope="request" />
<fmt:setBundle basename="properties.messages.rest.message" />
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/index.js"></script>
<c:if test="${VDAS.rule eq '0' or VDAS.rule eq '1' }">
<script type="text/javascript">
var lazy;
var lazyRoute;
var lazySupplies;
$(document).ready(function(){
	initMenuSel("M3001");
	
	$("body").on("keyup", "#searchText",function(e){
		if(e.keyCode == 13) $(".btn_ajaxPop").trigger("click");
	});
	
	$("body").on("click",".btn_ajaxPop",function(){
		$.lazyLoader.search(lazy);
	});
	
	if(!lazy){
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
				searchText : $("#searchText")
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/com/getVehicleList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var strHtml = '';
				var data = r.rtvList;
				console.log(data);
				
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += '';
					strHtml += '<tr>';
					strHtml += '<td>';
					strHtml += '<input type="radio" name="rdo_sel" class="rdo_sel" value="'+vo.vehicleKey+'">';
					strHtml += '</td>';
					strHtml += '<td>';
					strHtml += '<p>'+vo.manufacture+'</p>';
					strHtml += '</td>';
					strHtml += '<th class="car_list">';
					strHtml += '<strong class="car_img"><img src="${pathCommon}/images/vehicle/'+vo.imgS+'.png" alt=""></strong>';
					strHtml += '<a href="#none">';
					strHtml += '<span>'+vo.modelMaster+'</span>'+vo.plateNum+'</a>';
					strHtml += '</th>';
					strHtml += '<td>';
					strHtml += '<p>'+vo.volume+' cc</p>';
					strHtml += '</td>';
					strHtml += '<td>';
					if(vo.vehicleBizType == '1')
						strHtml += '<p><fmt:message key='vehicleMaintenanceRegister.vehicleBizType1'/></p>';
					else if(vo.vehicleBizType == '2')
						strHtml += '<p><fmt:message key='vehicleMaintenanceRegister.vehicleBizType2'/></p>';
					else if(vo.vehicleBizType == '3')
						strHtml += '<p><fmt:message key='vehicleMaintenanceRegister.vehicleBizType3'/></p>';
					else if(vo.vehicleBizType == '4')
						strHtml += '<p><fmt:message key='vehicleMaintenanceRegister.vehicleBizType4'/></p>';
					strHtml += '</td>';
					strHtml += '<th>';
					strHtml += '<p>'+vo.year+'</p>';
					strHtml += '<p>'+number_format((vo.totDistance/1000).toFixed(1))+' km</p>';
					strHtml += '</th>';
					strHtml += '<th>';
					strHtml += '<p></p>';
					strHtml += '<p>'+vo.fuelType+'</p>';
					strHtml += '</th></tr>';
				}
				
				return strHtml;
			}
		});
	}
	
	$("body").on("click",".bt_add",function(){
		strHtml = '';
		$(".selectItem").removeClass("add");
		
		strHtml += '<div class="repair_box2_2_3">';
		strHtml += '<div class="maintenancelList">';
		strHtml += '<span class="tit"><fmt:message key='vehicleMaintenanceRegister.maintenanceItem'/></span>';
		strHtml += '<div class="select_type7">';
		strHtml += '<select class="select form selectItem add" style="width:131px;height:27px;">';
		strHtml += '<option value=""><fmt:message key='vehicleMaintenanceRegister.select'/></option>';
		strHtml += '</select>';
		strHtml += '</div><input type="text" class="input1 itemDetail" style="width:338px;" /><a href="#none" class="btn_del"><img src="${pathCommon}/images/${_LANG}/btn_delete.jpg" alt="" /></a>';
		strHtml += '<div class="r3" style="width:510px; margin-left:80px;">';
		strHtml += '<span class="r3_1"><fmt:message key='vehicleMaintenanceRegister.partsRank'/></span>';
		strHtml += '<div class="select_type5">';
		strHtml += '<select class="sel partRank" style="width:49px; height:25px;">';
		strHtml += '<option value="0">A</option>';
		strHtml += '<option value="1">B</option>';
		strHtml += '<option value="2">C</option>';
		strHtml += '</select>';
		strHtml += '</div><span class="r3_2"><fmt:message key='vehicleMaintenanceRegister.count'/></span><input type="text" class="numberonly partCount" style="width:15px"/>';
		strHtml += '<span class="r3_3"><fmt:message key='vehicleMaintenanceRegister.partsAmount'/></span><input type="text" class="input2 partCost" /><span class="r3_4"><fmt:message key='vehicleMaintenanceRegister.won'/></span><span class="r3_5"><fmt:message key='vehicleMaintenanceRegister.repair'/></span><input type="text" class="input2 wagesCost" /><span class="r3_4"><fmt:message key='vehicleMaintenanceRegister.won1'/></span>';
		strHtml += '</div>';
		strHtml += '</div>';
		
		$("#list").append(strHtml);

		var vehicleKey = $("#vehicleKey").val();
		$VDAS.ajaxCreateSelect("/vehicle/getVehicleItemsCode.do",{vehicleKey:vehicleKey},$(".add"),false,"<fmt:message key="vehicleMaintenanceRegister.select2"/>","","${_LANG}",false,null);	
	});
	
	$("body").on("click",".rdo_sel",function(){
		$("#vehicleKey").val($(this).val());
	});
	
	$("body").on("click",".btn_select",function(){
		if($("input[name=rdo_sel]").is(":checked")){
			var vehicleKey = $("#vehicleKey").val();
			
			$VDAS.http_post("/vehicle/getVehicleInfo.do",{vehicleKey:vehicleKey},{
				success : function(r){
					var obj = r.result;

					console.log(obj);
					$("#modelNm").text(""+obj.deviceSeries+"/"+obj.deviceSn);
					$("#vehicleImg").attr("src",'${pathCommon}/images/vehicle/'+obj.imgL+'.png');
					$("#vehicleNm").val(""+obj.plateNum);
					$("#corpNm").text(""+obj.corpName);
					$("#volume").text(""+number_format(obj.volume)+" cc");
					$("#transmission").text(""+obj.transmission);
					$("#totDistance").text(""+number_format((obj.totDistance/1000).toFixed(1))+" km");
					$("#manufacture").text(""+obj.manufacture);
					$("#modelMaster").text(""+obj.modelMaster+"/"+obj.year);
					$("#fuelType").text(""+obj.fuelType);
					$("#color").text(convertNullString(obj.color));
					//$(".car_detail_left").append('<img src="${pathCommon}/images/vehicle/'+obj.imgL+'.png" alt="" />');
				}
			});
			$(".pop1").fadeOut();
			$(".black").fadeOut();
			
			$VDAS.ajaxCreateSelect("/vehicle/getVehicleItemsCode.do",{vehicleKey:vehicleKey},$(".selectItem"),false,"<fmt:message key="vehicleMaintenanceRegister.select3"/>","","${_LANG}",false,null);			
		}
		else
			alert("<fmt:message key="vehicleMaintenanceRegister.txt1"/>");
	});
	
	$("body").on("change",".partCost",function(){
		TotalPartCost();
	    TotalCost();
	});
	
	$("body").on("change",".wagesCost",function(){
		TotalWagesCost();
	    TotalCost();
	});
	
	$("body").on("click",".btn_cancel",function(){
		$(".pop1").fadeOut();
	});

	$("body").on("click",".btn_pop",function(){
		$(".pop1").fadeIn();
		$(".black").fadeIn();
		$.lazyLoader.search(lazy);
	});
	
	$("body").on("click",".btn_del",function(){
		$(this).parent().parent().remove();
		
		TotalPartCost();
		TotalWagesCost();
	    TotalCost();
	});
	
	$(".btn_car_detail").on("click",function(){
		var key = $("#vehicleKey").val();
		if(key.trim().length == 0) alert("<fmt:message key="vehicleMaintenanceRegister.txt2"/>");
		else $VDAS.instance_post("/drivingMng/searchVehicleDetail.do",{vehicleKey:key});
	});
	
	$("body").on("click",".btn_reg",function(){
		if($("#vehicleKey").val() != null && $("#vehicleKey").val() != ""){
			var vaildChk = true;

			$("#editFrm").find("input").val("");
			
			$("#registFrm input[name=vehicleKey]").val($("#vehicleKey").val());
			$("#registFrm input[name=clinicNm]").val($("#clinicNm").val());
			$("#registFrm input[name=drivingDistance]").val($("#drivingDistance").val()*1000);
			$("#registFrm input[name=selectDate]").val(replaceAll($("#selectDate").val(),"/",""));
			$("#registFrm input[name=selectH]").val($("#selectH").val());
			$("#registFrm input[name=selectM]").val($("#selectM").val());
			$("#registFrm input[name=descript]").val($("#descript").val());
			
			$(".maintenancelList").each(function(index){
				if(($(".selectItem:eq("+index+")").val() == "" || $(".selectItem:eq("+index+")").val() == null)
						&& ($(".itemDetail:eq("+index+")").val() == '' || $(".itemDetail:eq("+index+")").val() == null)){
		    		alert("<fmt:message key="vehicleMaintenanceRegister.txt3"/>");
		    		vaildChk = false;
					return false;
				}
				
				var inputObj = $("#registFrm input[name=selectItem]");
				inputObj.val(inputObj.val() + $(".selectItem:eq("+index+")").val());
	            if(index != $(".selectItem").length - 1) inputObj.val(inputObj.val()+",");
	
	            inputObj = $("#registFrm input[name=itemDetail]");
	            if(($(".selectItem:eq("+index+")").val() != null && $(".selectItem:eq("+index+")").val() != '')
	            		&& ($(".itemDetail:eq("+index+")").val() == null || $(".itemDetail:eq("+index+")").val() == ''))
					inputObj.val(inputObj.val() + $(".selectItem:eq("+index+") option:selected").text());
	            else inputObj.val(inputObj.val() + $(".itemDetail:eq("+index+")").val());
	            if(index != $(".itemDetail").length - 1) inputObj.val(inputObj.val()+",");
	
	            inputObj = $("#registFrm input[name=partRank]");
				inputObj.val(inputObj.val() + $(".partRank:eq("+index+")").val());
	            if(index != $(".partRank").length - 1) inputObj.val(inputObj.val()+",");
	            
				inputObj = $("#registFrm input[name=partsType]");
				if($(".selectItem:eq("+index+")").val() == null || $(".selectItem:eq("+index+")").val() == "")
					inputObj.val(inputObj.val() + "0");
				else
					inputObj.val(inputObj.val() + "1");
	            if(index != $(".partsType").length - 1) inputObj.val(inputObj.val()+",");
	
	            inputObj = $("#registFrm input[name=partCount]");
				if($(".partCount:eq("+index+")").val() != null && $(".partCount:eq("+index+")").val() != '')
					inputObj.val(inputObj.val() + $(".partCount:eq("+index+")").val());
				else inputObj.val(inputObj.val() + "0");
	            if(index != $(".partCount").length - 1) inputObj.val(inputObj.val()+",");
	
	            inputObj = $("#registFrm input[name=partCost]");
				if($(".partCost:eq("+index+")").val() != null && $(".partCost:eq("+index+")").val() != '')
					inputObj.val(inputObj.val() + $(".partCost:eq("+index+")").val());
				else inputObj.val(inputObj.val() + "0");
	            if(index != $(".partCost").length - 1) inputObj.val(inputObj.val()+",");
	            
				inputObj = $("#registFrm input[name=wagesCost]");
				if($(".wagesCost:eq("+index+")").val() != null && $(".wagesCost:eq("+index+")").val() != '')
					inputObj.val(inputObj.val() + $(".wagesCost:eq("+index+")").val());
				else inputObj.val(inputObj.val() + "0");
	            if(index != $(".wagesCost").length - 1) inputObj.val(inputObj.val()+",");
	    	});
	    	console.log($("#registFrm").serialize());
    	
	    	if(vaildChk){
	        	$VDAS.http_post("/clinic/registMaintenance.do",$("#registFrm").serialize(),{
	    			success:function(r){
	    				$VDAS.instance_post("/clinic/vehicleMaintenance.do",{});
	    			},
	    			error:function(r){
	    			}
	    		});
	    	}
    	}
    	else
    		alert("<fmt:message key="vehicleMaintenanceRegister.txt4"/>");
	});
});

function TotalPartCost(){
	var totalPartCost = 0;
	$(".partCost").each(function(index) {
		totalPartCost += $(".partCost:eq("+index+")").val()?parseInt($(".partCost:eq("+index+")").val()):0;
	});
	$("#totalPartCost").val(totalPartCost);
}

function TotalWagesCost(){
	var totalWagesCost = 0;
    $(".wagesCost").each(function(index) {
    	totalWagesCost  += $(".wagesCost:eq("+index+")").val()?parseInt($(".wagesCost:eq("+index+")").val()):0;
    });
    $("#totalWagesCost").val(totalWagesCost);
}

function TotalCost(){
	var partCost = $("#totalPartCost").val()?parseInt($("#totalPartCost").val()):0;
	var wagesCost = $("#totalWagesCost").val()?parseInt($("#totalWagesCost").val()):0;
	var cost = partCost + wagesCost;

	$("#totalCost").val(cost);
}

</script>
<input type="hidden" id="vehicleKey" value="" />
<form id="searchFrm" style="display:none">
	<input type="hidden" name="searchText" value="" />
	<input type="hidden" name="searchType" value="plateNum" />
	<input type="hidden" name="searchUsed" value="1" />
</form>
<form id="registFrm" style="display:none">
	<input type="hidden" name="vehicleKey" value="" />
	<input type="hidden" name="clinicNm" value="" />
	<input type="hidden" name="drivingDistance" value="" />
	<input type="hidden" name="selectDate" value="" />
	<input type="hidden" name="selectH" value="" />
	<input type="hidden" name="selectM" value="" />
	<input type="hidden" name="descript" value="" />
	
	<input type="hidden" name="selectItem" value="" />
	<input type="hidden" name="partsType" value="" />
	<input type="hidden" name="itemDetail" value="" />
	<input type="hidden" name="partRank" value="" />
	<input type="hidden" name="partCount" value="" />
	<input type="hidden" name="partCost" value="" />
	<input type="hidden" name="wagesCost" value="" />
</form>
<div class="right_layout">
	<div class="detail_view">
		<a href="javascript:window.history.back();" class="btn_close"><img src="${pathCommon}/images/close.jpg" alt="" /></a>
		<h2 class="h2tit">
			<fmt:message key='vehicleMaintenanceRegister.txt5'/>
		</h2>
		<div class="step_box sub_top1" style="text-align:left; padding:7px 12px 6px 21px;">
			<span><fmt:message key='vehicleMaintenanceRegister.txt5_1'/></span>
		</div>
		<div class="repair_layout">
			<div class="repair_left">
				<!-- img size : 가로 308*212-->
				<img src="${pathCommon}/images/${_LANG}/car_img6.jpg" alt="" id="vehicleImg"/>
			</div>
			<ul class="repair_right">
				<li class="r1">
					<div>
						<span><fmt:message key='vehicleMaintenanceRegister.vehicleSelect'/></span>
						<input type="text" placeholder="<fmt:message key='vehicleMaintenanceRegister.plateNum'/>" id="vehicleNm" />
						<a href="#" class="btn_pop">
							<img src="${pathCommon}/images/btn_search.jpg" alt="" />
						</a>
						<a href="#none" class="btn_car_detail"><fmt:message key='vehicleMaintenanceRegister.vehicleSelectDetail'/></a>
					</div>
					<div>
						<span><fmt:message key='vehicleMaintenanceRegister.modelNm'/></span><i id="modelNm"></i>
					</div>
				</li>
				<li class="r2">
					<div>
						<p><span><fmt:message key='vehicleMaintenanceRegister.corpNm'/></span><i id="corpNm"></i></p>
						<p><span><fmt:message key='vehicleMaintenanceRegister.volume'/></span><i id="volume"></i></p>
						<p><span><fmt:message key='vehicleMaintenanceRegister.transmission'/></span><i id="transmission"></i></p>
						<p><span><fmt:message key='vehicleMaintenanceRegister.totDistance'/></span><i id="totDistance"></i></p>
					</div>
					<div>
						<p><span><fmt:message key='vehicleMaintenanceRegister.manufacture'/></span><i id="manufacture"></i></p>
						<p><span><fmt:message key='vehicleMaintenanceRegister.modelMaster'/></span><i id="modelMaster"></i></p>
						<p><span><fmt:message key='vehicleMaintenanceRegister.fuelType'/></span><i id="fuelType"></i></p>
						<p><span><fmt:message key='vehicleMaintenanceRegister.color'/></span><i id="color"></i></p>
					</div>
				</li>
			</ul>
		</div>
		<div><img src="${pathCommon}/images/${_LANG}/h2_5.png" alt="차량정비 내용 입력" /></div>
		<div class="repair_layout2">
			<div class="repair_box1">
				<div class="tit"><fmt:message key='vehicleMaintenanceRegister.maintenanceContent'/></div>
				<div class="repair_box2">
					<div class="repair_box2_1">
						<span class="tit"><fmt:message key='vehicleMaintenanceRegister.repairShop'/></span><input type="text" id="clinicNm" />
						<span class="tit" style="margin-left:20px"><fmt:message key='vehicleMaintenanceRegister.drivingDistance'/></span><input type="text" class="numberOnly" id="drivingDistance" />
					</div>
					<div class="car_info5 repair_box2_1">
						<span class="tit"><fmt:message key='vehicleMaintenanceRegister.maintenanceDate'/></span>
						<div class="cal_layout">
							<div class="cal_area">
								<div>
									<input type="text" id="selectDate" class="date datePicker" style="width:65px; height:25px; border:1px solid #c0c0c0;"/>
									<div class="select_type5">
										<select class="sel" id="selectH">
											<c:forEach begin="0" end="23" step="1" var="i">
												<option value="${i}">${i}</option>
											</c:forEach>				
										</select>
									</div><span><fmt:message key='vehicleMaintenanceRegister.hour'/></span>
									<div class="select_type5">
										<select class="sel" id="selectM">
											<c:forEach begin="0" end="59" step="1" var="i">
												<option value="${i}">${i}</option>
											</c:forEach>
										</select>
									</div><span><fmt:message key='vehicleMaintenanceRegister.minute'/></span>
								</div>
								<p><fmt:message key='vehicleMaintenanceRegister.txt6'/></p>
							</div>
						</div>
					</div>
					<div class="car_info4 repair_box2_1">
						<span class="tit"><fmt:message key='vehicleMaintenanceRegister.carInfo'/></span><div class="car_info4_1">
							<div class="car_info4_3">
								<textarea id="descript"></textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="repair_layout2 repair_layout2_2">
				<div class="repair_box1">
					<div class="tit"><fmt:message key='vehicleMaintenanceRegister.detailDesc'/></div>
					<div class="repair_box2">
						<div class="repair_box2_1 repair_box2_2" id="list">
							<div class="maintenancelList">
								<span class="tit"><fmt:message key='vehicleMaintenanceRegister.maintenanceItem2'/></span>
								<div class="select_type7">
									<select class="select form selectItem" style="width:131px;height:27px;">
										<option value=""><fmt:message key='vehicleMaintenanceRegister.select4'/></option>
									</select>
								</div><input type="text" class="input1 itemDetail" style="width:338px;" /><a href="#none" class="bt_add"><img src="${pathCommon}/images/${_LANG}/btn_add.jpg" alt="" /></a>
								<div class="r3" style="width:510px; margin-left:80px;">
									<span class="r3_1"><fmt:message key='vehicleMaintenanceRegister.partsRank2'/></span>
									<div class="select_type5">
										<select class="sel partRank">
											<option value="0"><fmt:message key='vehicleMaintenanceRegister.partRank1'/></option>
											<option value="1"><fmt:message key='vehicleMaintenanceRegister.partRank2'/></option>
											<option value="2"><fmt:message key='vehicleMaintenanceRegister.partRank3'/></option>
										</select>
									</div><span class="r3_2"><fmt:message key='vehicleMaintenanceRegister.partCount'/></span><input type="text" class="numberonly partCount numberOnly" style="width:15px"/>
									<span class="r3_3"><fmt:message key='vehicleMaintenanceRegister.partsAmount2'/></span>
									<input type="text" class="input2 partCost numberOnly" />
									<span class="r3_4"><fmt:message key='vehicleMaintenanceRegister.won2'/></span>
									<span class="r3_5"><fmt:message key='vehicleMaintenanceRegister.repair2'/></span>
									<input type="text" class="input2 wagesCost" />
									<span class="r3_4"><fmt:message key='vehicleMaintenanceRegister.won3'/></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> 
		<div class="repair_layout3">
			<div class="d1">
				<span class="t1"><fmt:message key='vehicleMaintenanceRegister.totalCost'/></span><input type="text" id="totalCost" readonly/><strong><fmt:message key='vehicleMaintenanceRegister.won4'/></strong>
			</div>
			<div class="d2">
				<span class="t1"><fmt:message key='vehicleMaintenanceRegister.totalPartCost'/></span><input type="text" id="totalPartCost" readonly/><span class="t2">+</span><input type="text" id="totalWagesCost" readonly/><strong><fmt:message key='vehicleMaintenanceRegister.won5'/></strong>
				<p><fmt:message key='vehicleMaintenanceRegister.txt7'/></p>
			</div>
		</div>
		<div class="btn_area2"><a href="javascript:window.history.back();" class="btn_cancel"><fmt:message key='vehicleMaintenanceRegister.cancel'/></a><a href="#none" class="btn_reg"><fmt:message key='vehicleMaintenanceRegister.regComplete'/></a></div>
	</div>
</div>
<div class="black"></div>
<div class="pop1" style="display:none;">
	<div class="pop_layout">
		<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt="" /></a>
		<h1><fmt:message key='vehicleMaintenanceRegister.vehicleSelect2'/></h1>
		<h2 class="title3 pat42">
			<p><fmt:message key='vehicleMaintenanceRegister.txt8'/></p>
		</h2>
		<div class="car_choice_search_box mab12">
			<input type="text" placeholder="<fmt:message key='vehicleMaintenanceRegister.input'/>" id="searchText"/><a href="#none" class="btn_ajaxPop"><img src="${pathCommon}/images/btn_search3.jpg" alt=""></a>
		</div>
		<table class="table1" id="contentsTable">
			<colgroup>
				<col style="width:60px;" />
				<col style="width:70px;" />
				<col style="width:230px;" />
				<col style="width:70px;" />
				<col style="width:100px;" />
				<col style="width:112px;" />
				<col style="width:118px;" />
			</colgroup>
			<thead>
				<tr>
					<th></th>
					<th><fmt:message key='vehicleMaintenanceRegister.manufacture'/></th>
					<th><fmt:message key='vehicleMaintenanceRegister.vehicleInfo'/></th>
					<th><fmt:message key='vehicleMaintenanceRegister.volumn'/></th>
					<th><fmt:message key='vehicleMaintenanceRegister.vehicleBizType'/></th>
					<th><fmt:message key='vehicleMaintenanceRegister.yearAndDistance'/></th>
					<th><fmt:message key='vehicleMaintenanceRegister.colorAndFuelType'/></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		<br><br>
		<div class="btn_type1"><a href="#none" class="btn_cancel"><fmt:message key='vehicleMaintenanceRegister.cancel2'/></a><a href="#none" class="btn_select"><fmt:message key='vehicleMaintenanceRegister.apply'/></a></div>
	</div>
</div>
</c:if>
<c:if test="${VDAS.rule ne '0' and VDAS.rule ne '1' }">
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("M3002");
});
</script>
<div class="right_layout">
	<img src="${pathCommon}/images/${_LANG}/authError.png" alt="" style="width:100%">
</div>
</c:if>