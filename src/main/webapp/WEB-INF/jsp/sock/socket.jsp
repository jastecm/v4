<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>

<html>

<head>

<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery-1.10.2.min.js" charset="utf-8" ></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/sockjs.min.js" ></script>


<script type="text/javascript">

//var sock = new SockJS('${pageContext.request.contextPath}/echo',[],{sessionId : function(){return "asdf";}});
var sock = new SockJS('${pageContext.request.contextPath}/echo',[],{sessionId : 16});

sock.onopen = function() {

    $('#console').append('websocket opened' + '<br>');

};

sock.onmessage = function(message) {

    $('#console').append('receive message : ' + message.data + '<br>');

};

sock.onclose = function(event) {

    $('#console').append('websocket closed : ' + event);

};

function messageSend() {

    sock.send($('#message').val());

}

</script>

</head>

<body>


<input type="text" id="message" />

<input type="button" value="전송" onclick="messageSend();" />

<div id="console" />


</body>

</html>

