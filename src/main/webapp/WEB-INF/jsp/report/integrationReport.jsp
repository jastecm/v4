<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 

<fmt:setLocale value="${_LANG}" scope="request" />
<fmt:setBundle basename="properties.messages.rest.message" />

<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/index.js"></script>
<c:if test="${VDAS.rule eq '0' or VDAS.rule eq '1' }">
<script type="text/javascript">
var lazyVehicle;
var printPop;

var rData;
$(document).ready(function(){
	initMenuSel("M4001");
	
	var Now = new Date();
	var NowY = Now.getFullYear();
	var NowM = LPAD(''+(Now.getMonth()+1),'0',2);
	$("#searchMonth").val(NowY+"-"+NowM);
	
	$(".btn_search").on("click",function(){
		var date = replaceAll($("#searchMonth").val(),"-","");
		var vehicleKey = $("#vehicleKey").val();
		var group = $("#corpGroup").val();
		SuddenSummaryInit();
		$.ajax({
			url:"/vdasPro/report/getDrivingSummary.do"
			,dataType : 'json'
			,contentType: "application/x-www-form-urlencoded; charset=UTF-8"
			,type:"POST"
			,data:{yyyymm:date, vehicleKey:vehicleKey, groupKey:group}
			,cache:!1
			,success:function(t){
				var a = t.rtvList
				rData = a;
				$VDAS.http_post("/vehicle/getVehicleCnt.do",{},{
					success : function(r){
						var data = r.result;
						$(".vehicleCnt").text(data.total+"<fmt:message key="integrationReport.vehicleCnt"/>");
					}
				});
				$VDAS.http_post("/account/getAccountCnt.do",{},{
					success : function(r){
						var data = r.result;
						$(".userCnt").text(data.total+"<fmt:message key="integrationReport.userCnt"/>");
					}
				});
				
				draw(a);
				
			}		
			,error:function(t){
				$(".contents").children().not(".noneHide").show();
				$(".white").fadeOut();
			}
			,beforeSend: function() {
				$(".contents").children().not(".noneHide").hide();
				$(".white").fadeIn();
			}
		}).done(function(){
			$(".contents").children().not(".noneHide").show();
			$(".white").fadeOut();
		});
		
		
	});
	
	/*
	$(".btn_search").on("click",function(){
		alert("todo! - SEARCH");
		//$.lazyLoader.search(lazy);
	});
    */
	//https://canvasjs.com/docs/charts/basics-of-creating-html5-chart/
	$VDAS.ajaxCreateSelect("/com/getGroupCode.do",{},$("#corpGroup"),false,"<fmt:message key="integrationReport.allCorpGroup"/>","${rtv.groupKey}","${_LANG}",null,null);

	$(".contents").children().not(".noneHide").hide(); // remove
	
	$("body").on("click","input[name=searchVehicle]",function(e){
		$("#searchBox a").trigger("click");
	});
	
	$("body").on("keyup","#searchTextVehicle",function(e){
		if(e.keyCode == 13) $("#btn_searchVehicle").trigger("click");
	});
	
	$("body").on("click","#btn_searchVehicle",function(){
		$.lazyLoader.search(lazyVehicle);
	});
	
	$("body").on("click","#btn_ajaxPop,.btn_ajaxPop",function(){
		$(".black").fadeIn();
		$(".vehicle").fadeIn();
		$.lazyLoader.search(lazyVehicle);
	});
	
	$("#corpGroup").on("change",function(){
		var corpGroup = $(this).val();
		$("input[name=searchVehicle]").val("");
		$("#vehicleKey").val("");
		$("#vehicleFrm input[name=searchGroup]").val(corpGroup);
	});

	$("body").on("click",".btn_ok",function(){
		$("input[name=rdo_sel]").each(function(){
			if($(this).is(":checked")){
				var name = $(this).data("name");
				var key = $(this).data("key");
				$("input[name=searchVehicle]").val(name);
				$("#vehicleKey").val(key);
				return false;
			}
		})
		$(".pop7").fadeOut();
		$(".black").fadeOut();
	});
	
	if(!lazyVehicle){
		lazyVehicle = $("#contentsTableVehicle").lazyLoader({
			searchFrmObj : $("#vehicleFrm")
			,searchFrmVal : {
				searchText : $("#searchTextVehicle")
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/com/getVehicleList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var strHtml = '';
				var data = r.rtvList;
				
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += '';
					strHtml += '<tr>';
					strHtml += '<td>';
					strHtml += '<input type="radio" name="rdo_sel" class="rdo_sel" data-name="'+vo.plateNum+'" data-key="'+vo.vehicleKey+'">';
					strHtml += '</td>';
					strHtml += '<td>';
					strHtml += '<p>'+vo.manufacture+'</p>';
					strHtml += '</td>';
					strHtml += '<th class="car_list">';
					strHtml += '<strong class="car_img"><img src="${pathCommon}/images/vehicle/'+vo.imgS+'.png" alt=""></strong>';
					strHtml += '<a href="#none">';
					strHtml += '<span>'+vo.modelMaster+'</span>'+vo.plateNum+'</a>';
					strHtml += '</th>';
					strHtml += '<td>';
					strHtml += '<p>'+vo.volume+' cc</p>';
					strHtml += '</td>';
					strHtml += '<td>';
					if(vo.vehicleBizType == '1')
						strHtml += '<p><fmt:message key='integrationReport.vehicleBizType1'/></p>';
					else if(vo.vehicleBizType == '2')
						strHtml += '<p><fmt:message key='integrationReport.vehicleBizType2'/></p>';
					else if(vo.vehicleBizType == '3')
						strHtml += '<p><fmt:message key='integrationReport.vehicleBizType3'/></p>';
					else if(vo.vehicleBizType == '4')
						strHtml += '<p><fmt:message key='integrationReport.vehicleBizType4'/></p>';
					strHtml += '</td>';
					strHtml += '<th>';
					strHtml += '<p>'+vo.year+'</p>';
					strHtml += '<p>'+number_format((vo.totDistance/1000).toFixed(1))+' km</p>';
					strHtml += '</th>';
					strHtml += '<th>';
					strHtml += '<p></p>';
					strHtml += '<p>'+vo.fuelType+'</p>';
					strHtml += '</th></tr>';
				}
				
				return strHtml;
			}
		});
	}
	
	$(".btn_black").on("click",function(){
		if(rData) {
			$("body").append("<form id='popFrm'></form>");

			var strHtml = '<input type="text" name="rData" value='+replaceAll(JSON.stringify(rData)," ","&nbsp;")+' >';
			$("#popFrm").append(strHtml);
			$("#popFrm").attr("action","/vdasPro/report/integrationReportPrint.do");
			$("#popFrm").attr("method","POST");
			$("#popFrm").attr("target","popForm");
			$("#popFrm").attr("accept-charset","UTF-8");
			 
			var p = window.open("" ,"popForm","");
			$("#popFrm").submit();
			
		}else{
			alert("<fmt:message key="integrationReport.txt1"/>");
		}
	});
});


function draw(a){
	chart1Area(a.getInfoSummaryTotalMonth, a.getSuddenSummaryTotalAll, a.getScoreSummaryGroupAll);
	chart2Area(a.getBizTripSummaryGroupAll, a.getBizTripSummaryGroupPrev, a.getTotalVehicleCnt, a.getTotalVehicleNoneTripCnt, a.getTotalVehicleNoneTripPrevCnt, a.getTotalGroupNoneTripCnt);
	chart4Area(a.getBizTripSummaryTotalMonth);
	chart5Area(a.getBizTripSummaryTotalHours, a.getBizTripSummaryGroupAll, a.getBizTripSummaryTotalQuater);
	chart6Area(a.getExpenseSummaryTotalMonth, a.getExpenseSummaryTotalPrev, a.getExpenseSummaryGroupAll, a.getExpenseSummaryGroupPrev);
}

function SuddenSummaryInit(){
	$(".rapidStart").text("<fmt:message key="integrationReport.zeroCount1"/>");
	$(".rapidStop").text("<fmt:message key="integrationReport.zeroCount2"/>");
	$(".rapidAccel").text("<fmt:message key="integrationReport.zeroCount3"/>");
	$(".rapidDeaccel").text("<fmt:message key="integrationReport.zeroCount4"/>");
	$(".rapidTurn").text("<fmt:message key="integrationReport.zeroCount5"/>");
	$(".rapidUtern").text("<fmt:message key="integrationReport.zeroCount6"/>");
	$(".overSpeed").text("<fmt:message key="integrationReport.zeroCount7"/>");
	$(".overSpeedLong").text("<fmt:message key="integrationReport.zeroCount8"/>");
}

function suddenSummaryCall(a){
	$(".rapidStart").text(a.rapidStart+"<fmt:message key="integrationReport.count1"/>");
	$(".rapidStop").text(a.rapidStop+"<fmt:message key="integrationReport.count2"/>");
	$(".rapidAccel").text(a.rapidAccel+"<fmt:message key="integrationReport.count3"/>");
	$(".rapidDeaccel").text(a.rapidDeaccel+"<fmt:message key="integrationReport.count4"/>");
	$(".rapidTurn").text(a.rapidTurn+"<fmt:message key="integrationReport.count5"/>");
	$(".rapidUtern").text(a.rapidUtern+"<fmt:message key="integrationReport.count6"/>");
	$(".overSpeed").text(a.overSpeed+"<fmt:message key="integrationReport.count7"/>");
	$(".overSpeedLong").text(a.overSpeedLong+"<fmt:message key="integrationReport.count8"/>");
}

function chart1Area(infoSummary, suddenSummary, scoreSummary){
	var drivingTime;
	var score4;
	var safeScore;
	var ecoScore;
	var fuelRank;
	var safeRank;
	var ecoRank;
	var totalRank;
	var month=[0,0,0,0,0,0,0,0,0,0,0,0];
	var totalDistance = 0;
	var totalFco = 0;
	var totalFuelEff = 0;
	var count = 0;
	var selFlag=[null,null,null,null,null,null,null,null,null,null,null,null];
	$("#myChart01").html('<div class="level"><span id="totalRank"></span> <i class="icon totalIcon"></i></div>');
	$("#fuelRank").text("");
	$("#safeRank").text("");
	$("#ecoRank").text("");
	$("#distance").text(" km");
	$("#drivingTime").text("<fmt:message key="integrationReport.timeText"/>");
	$("#fco").text(" ℓ");
	$("#oilPrice").text(" <fmt:message key="integrationReport.won"/>");
	
	for(var i=0;i<infoSummary.length;i++){
		month[i] = infoSummary[i].fuelEff?parseFloat(infoSummary[i].fuelEff):null;
		totalDistance += infoSummary[i].distance?parseInt(infoSummary[i].distance):0;
		totalFco += infoSummary[i].fco?parseInt(infoSummary[i].fco):0;
		totalFuelEff += month[i];
		if(infoSummary[i].officialFuelEff != null) count++;
		if(infoSummary[i].selFlag == "1"){
			selFlag[i] = "red";
			drivingTime = infoSummary[i].drivingTime?infoSummary[i].drivingTime:0;
			score4 = infoSummary[i].score4?infoSummary[i].score4:0;
			safeScore = infoSummary[i].safeScore?infoSummary[i].safeScore:0;
			ecoScore = infoSummary[i].ecoScore?infoSummary[i].ecoScore:0;
			
			fuelRank = calRank(score4);
			safeRank = calRank(safeScore);
			ecoRank = calRank(ecoScore);
			totalRank = calRank((score4+safeScore+ecoScore)/3);
			
		    // getInfoSummaryTotalMonth
			$("#myChart01").drawDoughnutChart([
		 	    { title: "<fmt:message key="integrationReport.fuelEfficiency"/>", value : score4,  color: "#a150cb" },
		  	    { title: "<fmt:message key="integrationReport.safeScore"/>", value : safeScore,  color: "#ea2b7e" },
		  	    { title: "<fmt:message key="integrationReport.ecoScroe"/>", value:  ecoScore,   color: "#68b2c2" }
		    ]);

			$("#totalRank").text(""+totalRank);
			if(true)
				$(".totalIcon").addClass("up");
			else
				$(".totalIcon").addClass("down");
			$("#fuelRank").text(""+fuelRank);
			$("#safeRank").text(""+safeRank);
			$("#ecoRank").text(""+ecoRank);
			$("#distance").text(""+number_format(parseFloat(infoSummary[i].distance?(infoSummary[i].distance/1000):0).toFixed(1))+" km");
			$("#drivingTime").text(""+parseInt(drivingTime/3600)+'<fmt:message key='integrationReport.time'/> '+LPAD(''+parseInt((drivingTime%3600)/60),'0',2)+'<fmt:message key='integrationReport.minute'/> '+LPAD(''+parseInt((drivingTime%3600)%60),'0',2)+'<fmt:message key='integrationReport.second'/>');
			$("#fco").text(""+number_format(parseFloat(infoSummary[i].fco?(infoSummary[i].fco/1000):0).toFixed(2))+" ℓ");
			$("#oilPrice").text(""+number_format(infoSummary[i].oilPrice?infoSummary[i].oilPrice:0)+" <fmt:message key="integrationReport.won2"/>");
			
			$("#avgFco").text(""+parseFloat(infoSummary[i].fuelEff?infoSummary[i].fuelEff:0).toFixed(2)+" km/L");
			$("#comFco").text(""+parseFloat(infoSummary[i].officialFuelEff?infoSummary[i].officialFuelEff:0).toFixed(2)+" km/L");
		}
	}
	
	if(suddenSummary!=null)
		suddenSummaryCall(suddenSummary);

	

	var tableHtml = "";
	var groupCnt = scoreSummary.length;
	for(var i=0;i<scoreSummary.length;i++){
		if(scoreSummary[i].corpGroupNm == "<fmt:message key="integrationReport.emptyGroup"/>") groupCnt = groupCnt - 1;
		tableHtml += "<tr>";
		tableHtml += "<td class='first'>"+scoreSummary[i].corpGroupNm+"</td>";
		tableHtml += "<td>";
		tableHtml += "<div class='bslist'>";
		tableHtml += "<span><fmt:message key="integrationReport.fuelEfficiency2"/> <b>"+calRank(scoreSummary[i].ecoScore?scoreSummary[i].ecoScore:0)+"</b></span>";
		tableHtml += "<span><fmt:message key="integrationReport.safeScore2"/> <b>"+calRank(scoreSummary[i].safeScore?scoreSummary[i].safeScore:0)+"</b></span>";
		tableHtml += "<span><fmt:message key="integrationReport.ecoScroe2"/> <b>"+calRank(scoreSummary[i].fuelScore?scoreSummary[i].fuelScore:0)+"</b></span>";
		tableHtml += "</div>";
		tableHtml += "</td>";
		tableHtml += "</tr>";
	}
	$(".groupScoreTable tbody").empty();
	$(".groupScoreTable tbody").append(tableHtml);

	$(".groupCnt").text(groupCnt+"<fmt:message key="integrationReport.groupCnt"/>");
	$(".userCnt").text("<fmt:message key="integrationReport.userCnt"/>");
	$(".vehicleCnt").text("<fmt:message key="integrationReport.vehicleCnt"/>");

	monthOne = month.slice(0);
	selFlagOne = selFlag.slice(0);
	chartOne(month,selFlag);
}

function chartOne(month,selFlag){
	// getInfoSummaryTotalMonth
	var chart1 = new CanvasJS.Chart("chart1", {
		width:806,
		height:157,
		axisY: {
			labelFormatter:function(e){
				return e.value+"km/L";				
			}
			,labelFontSize: 12
			,labelFontWeight : "lighter"
			,labelFontColor: "black"
			//,interlacedColor: "#F0F8FF" 
			,lineThickness: 0
			,tickLength: 0
			,gridThickness: 1
		},
		axisX: {
			interval: 1
			,labelFormatter:function(e){				
				return (e.value+1)+"<fmt:message key="integrationReport.month"/>";		
				
			}
			,labelFontSize: 12
			,labelFontWeight : "lighter"
			,labelFontColor: "black"
			,lineThickness: 1
			,tickLength: 5
		},
		
		toolTip : {
			fontStyle : "normal"
			,fontSize : 14
			
			,fontWeight : "normal"
			,fontFamily : "sans-serif"
		},
		data: [{
			type: "line"
			,markerSize: 12
			,lineColor : "#cccdc9"
			,color : "#cccdc9"
			,toolTipContent: "{y}km/L"
			,dataPoints: [
			  { y: month[0] ,color: selFlag[0]},
			  { y: month[1] ,color: selFlag[1]},
			  { y: month[2] ,color: selFlag[2]},
			  { y: month[3] ,color: selFlag[3]},
			  { y: month[4] ,color: selFlag[4]},
			  { y: month[5] ,color: selFlag[5]},
			  { y: month[6] ,color: selFlag[6]},
			  { y: month[7] ,color: selFlag[7]},
			  { y: month[8] ,color: selFlag[8]},
			  { y: month[9] ,color: selFlag[9]},
			  { y: month[10] ,color: selFlag[10]},
			  { y: month[11] ,color: selFlag[11]},
			]
		}]
	});
	chart1.render();
}

function chart2Area(bizTripSummary, bizTripSummaryPrev, vehicleCnt, vehicleNoneCnt, vehicleNonePrevCnt, groupNoneCnt){
	var biz = new Array(bizTripSummary.length);
	var noneBiz = new Array(bizTripSummary.length);
	var groupNm = new Array(bizTripSummary.length);
	var jsonBizArray = new Array();
	var jsonNoneBizArray = new Array();
	var totalBiz = 0;
	var totalNoneBiz = 0;
	var jsonBizTemp;
	var jsonNoneBizTemp;
	
	for(var i = 0;i < bizTripSummary.length;i++){
		jsonBizTemp = new Object();
		jsonNoneBizTemp = new Object();
		biz[i] = bizTripSummary[i].biz?bizTripSummary[i].biz:0;
		noneBiz[i] = bizTripSummary[i].noneBiz?bizTripSummary[i].noneBiz:0;
		groupNm[i] = bizTripSummary[i].corpGroupNm?bizTripSummary[i].corpGroupNm:"";
		totalBiz += biz[i];
		totalNoneBiz += noneBiz[i];
		
		jsonBizTemp.y = biz[i];
		jsonBizTemp.label = groupNm[i];
		jsonNoneBizTemp.y = noneBiz[i];
		jsonNoneBizTemp.label = groupNm[i];
		jsonBizArray.push(jsonBizTemp);
		jsonNoneBizArray.push(jsonNoneBizTemp);
	}
	jsonBizTemp = new Object();
	jsonNoneBizTemp = new Object();
	jsonBizTemp.y = totalBiz;
	jsonBizTemp.label = "<fmt:message key="integrationReport.all"/>";
	jsonNoneBizTemp.y = totalNoneBiz;
	jsonNoneBizTemp.label = "<fmt:message key="integrationReport.all2"/>";
	jsonBizArray.push(jsonBizTemp);
	jsonNoneBizArray.push(jsonNoneBizTemp);

	jsonBizArrayTwo = jsonBizArray.slice(0);
	jsonNoneBizArrayTwo = jsonNoneBizArray.slice(0);
	chartTwo(jsonBizArray, jsonNoneBizArray);
	
	$(".bizTime").text("<fmt:message key="integrationReport.bizTime"/> "+number_format(Math.round(totalBiz/3600))+"<fmt:message key="integrationReport.time2"/>)");
	$(".noneBizTime").text("<fmt:message key="integrationReport.noneBizTime"/> "+number_format(Math.round(totalNoneBiz/3600))+"<fmt:message key="integrationReport.time3"/>)");
	var now = ((vehicleCnt - vehicleNoneCnt)/vehicleCnt)*100;
	var prev = ((vehicleCnt - vehicleNonePrevCnt)/vehicleCnt)*100;
	$(".tripRate").text(""+now.toFixed(1)+"%");
	if(now >= prev)
		$(".comparePrevRate").text(""+(now - prev).toFixed(1)+"% <fmt:message key="integrationReport.comparePrevRateUp"/>");
	else
		$(".comparePrevRate").text(""+(prev - now).toFixed(1)+"% <fmt:message key="integrationReport.comparePrevRateDown"/>");
	$(".totalVehicleCnt").text(""+vehicleCnt+"<fmt:message key="integrationReport.totalVehicleCnt"/>");
	$(".noneTripCnt").text(""+vehicleNoneCnt+"<fmt:message key="integrationReport.noneTripCnt"/>");
	$(".noneGroupTripCnt").text(""+groupNoneCnt+"<fmt:message key="integrationReport.noneGroupTripCnt"/>");
}

function chartTwo(jsonBizArray, jsonNoneBizArray){
	// getBizTripSummaryGroupAll & getBizTripSummaryGroupPrev & getTotalVehicleCnt & getTotalVehicleNoneTripCnt & getTotalGroupNoneTripCnt
	var chart2 = new CanvasJS.Chart("chart2", {
		width:806,
		axisX: {
			interval: 1
			,labelFontSize: 12
			,labelFontWeight : "normal"
			,labelFontColor: "black"
			//,interlacedColor: "#F0F8FF" 
			,lineThickness: 0
			,tickLength: 0
			,gridThickness: 0
		},
		
		axisY: {
			interval: 10
			,labelFormatter:function(e){				
				return (e.value)+"%";		
			}
			,labelFontSize: 12
			,labelFontWeight : "lighter"
			,labelFontColor: "black"
			,lineThickness: 0
			,gridThickness: 1
			,tickLength: 5
			,minimum : 0
			,maximum : 100
		},
		
		toolTip : {
			fontStyle : "normal"
			,fontSize : 14
			,fontWeight : "normal"
			,fontFamily : "sans-serif"
	        ,contentFormatter: function(e){
	          var str = "";
	          for (var i = 0; i < e.entries.length; i++){
	            var  temp = secondToTime(e.entries[i].dataPoint.y,"${_LANG}"); 
	            str = str.concat(temp);
	          }
	          return (str);
	        }
		},
		 dataPointWidth: 20,
		data: [{
			type: "stackedBar100"
			//,indexLabelPlacement : "outside"
			//,indexLabelFontColor : "#50fe6b"
			,color : "#92cc70"
			,dataPoints: []
		},{
			type: "stackedBar100"
			,color : "#e81626"
			//,indexLabelPlacement : "outside"
			//,indexLabelFontColor : "#e81626"
			,dataPoints: []
		}]
	});
	for(var i = 0;i < jsonBizArray.length;i++){
		chart2.options.data[0].dataPoints.push(jsonBizArray[i]);
		chart2.options.data[1].dataPoints.push(jsonNoneBizArray[i]);
	}
	chart2.render();
	
	//var h = $("#chart2").find(".canvasjs-chart-canvas").css("height");
	h = chart2.height+"px";
	$("#chart2").css("height",h);
}

function chart4Area(bizTripSummary){
	var biz = new Array(bizTripSummary.length);
	var commute = new Array(bizTripSummary.length);
	var noneBiz = new Array(bizTripSummary.length);
	var jsonBizArray = new Array();
	var jsonCommuteArray = new Array();
	var jsonNoneBizArray = new Array();
	var jsonBizTemp;
	var jsonCommuteTemp;
	var jsonNoneBizTemp;
	var maximum = 0;
	
	for(var i = 0;i < bizTripSummary.length;i++){
		jsonBizTemp = new Object();
		jsonCommuteTemp = new Object();
		jsonNoneBizTemp = new Object();
		biz[i] = bizTripSummary[i].bizCnt1?bizTripSummary[i].bizCnt1:0;
		commute[i] = bizTripSummary[i].bizCnt2?bizTripSummary[i].bizCnt2:0;
		noneBiz[i] = bizTripSummary[i].bizCnt3?bizTripSummary[i].bizCnt3:0;
		if(maximum < biz[i]) maximum = biz[i];
		else if(maximum < commute[i]) maximum = commute[i];
		else if(maximum < noneBiz[i]) maximum = noneBiz[i];
		
		jsonBizTemp.y = biz[i];
		jsonCommuteTemp.y = commute[i];
		jsonNoneBizTemp.y = noneBiz[i];
		if(bizTripSummary[i].selFlag == "1")
			jsonBizTemp.indexLabel = "now";
		jsonBizArray.push(jsonBizTemp);
		jsonCommuteArray.push(jsonCommuteTemp);
		jsonNoneBizArray.push(jsonNoneBizTemp);
	}
	var interval = parseInt(maximum / 5);
	
	jsonBizArrayFour = jsonBizArray.slice(0);
	jsonCommuteArrayFour = jsonCommuteArray.slice(0);
	jsonNoneBizArrayFour = jsonNoneBizArray.slice(0);
	intervalFour = interval;
	chartFour(jsonBizArray, jsonCommuteArray, jsonNoneBizArray, interval);
}

function chartFour(jsonBizArray, jsonCommuteArray, jsonNoneBizArray, interval){
	var chart4 = new CanvasJS.Chart("chart4", {
		width:806,
		height:200,
		axisY: {
			labelFormatter:function(e){				
				return e.value+"<fmt:message key="integrationReport.ea"/>";				
			}
			,labelFontSize: 12
			,labelFontWeight : "lighter"
			,labelFontColor: "black"
			,lineThickness: 0
			,tickLength: 0
			,gridThickness: 1
			,interval: interval
		},
		axisX: {
			interval: 1
			,labelFormatter:function(e){				
				return (e.value+1)+"<fmt:message key="integrationReport.month2"/>";
				
			}
			,labelFontSize: 12
			,labelFontWeight : "lighter"
			,labelFontColor: "black"
			,lineThickness: 1
			,tickLength: 5
		},
		toolTip : {
			fontStyle : "normal"
			,fontSize : 14
			,fontWeight : "normal"
			,fontFamily : "sans-serif"
		},
		legend:{
			fontStyle : "normal"
			,fontSize : 12
			,fontWeight : "normal"
			,fontFamily : "sans-serif"
	       	,horizontalAlign: "right" // left, center ,right 
	        ,verticalAlign: "top" // top, center, bottom
	      },
	      dataPointWidth: 15,
		data: [{
			color : "#ffc71c"
			,legendText: "<fmt:message key="integrationReport.drivingType1"/>"
			,showInLegend: true
			,toolTipContent: "<fmt:message key="integrationReport.drivingType11"/> {y}<fmt:message key="integrationReport.drivingType1_2"/>"
			,dataPoints: []
		},{
			color : "#ca58ff"
			,legendText: "<fmt:message key="integrationReport.drivingType2"/>"
			,showInLegend: true
			,toolTipContent: "<fmt:message key="integrationReport.drivingType21"/> {y}<fmt:message key="integrationReport.drivingType2_2"/>"
			,dataPoints: []
		},{
			color : "#ed1c24"
			,legendText: "<fmt:message key="integrationReport.drivingType3"/>"
			,showInLegend: true
			,toolTipContent: "<fmt:message key="integrationReport.drivingType31"/> {y}<fmt:message key="integrationReport.drivingType3_2"/>"
			,dataPoints: []
		}]
	});
	
	for(var i = 0;i < jsonBizArray.length;i++){
		chart4.options.data[0].dataPoints.push(jsonBizArray[i]);
		chart4.options.data[1].dataPoints.push(jsonCommuteArray[i]);
		chart4.options.data[2].dataPoints.push(jsonNoneBizArray[i]);
	}
	chart4.render();
}

function chart5Area(bizTripSummary, bizTripSummaryGroup, bizTripSummaryQuater){
	var biz = new Array(bizTripSummary.length);
	var commute = new Array(bizTripSummary.length);
	var noneBiz = new Array(bizTripSummary.length);
	var jsonBizArray = new Array();
	var jsonCommuteArray = new Array();
	var jsonNoneBizArray = new Array();
	var jsonBizTemp;
	var jsonCommuteTemp;
	var jsonNoneBizTemp;
	var maximum = 0;
	
	for(var i = 0;i < bizTripSummary.length;i++){
		jsonBizTemp = new Object();
		jsonCommuteTemp = new Object();
		jsonNoneBizTemp = new Object();
		biz[i] = bizTripSummary[i].bizCnt1?bizTripSummary[i].bizCnt1:0;
		commute[i] = bizTripSummary[i].bizCnt2?bizTripSummary[i].bizCnt2:0;
		noneBiz[i] = bizTripSummary[i].bizCnt3?bizTripSummary[i].bizCnt3:0;
		if(maximum < biz[i]) maximum = biz[i];
		else if(maximum < commute[i]) maximum = commute[i];
		else if(maximum < noneBiz[i]) maximum = noneBiz[i];
		
		jsonBizTemp.y = biz[i];
		jsonCommuteTemp.y = commute[i];
		jsonNoneBizTemp.y = noneBiz[i];
		if(bizTripSummary[i].selFlag == "1")
			jsonBizTemp.indexLabel = "now";
		jsonBizArray.push(jsonBizTemp);
		jsonCommuteArray.push(jsonCommuteTemp);
		jsonNoneBizArray.push(jsonNoneBizTemp);
	}
	
	var tableHtml = '';
	var drivingTime = 0;
	var bizDistanceTot = 0;
	var bizTime = 0;
	var bizCnt = [0,0,0];
	for(var i = 0;i < bizTripSummaryGroup.length;i++){
		var vo = bizTripSummaryGroup[i];

		var biz1Distance = vo.biz1Distance?(vo.biz1Distance/1000).toFixed(2):0;
		var biz2Distance = vo.biz2Distance?(vo.biz2Distance/1000).toFixed(2):0;
		var biz3Distance = vo.biz3Distance?(vo.biz3Distance/1000).toFixed(2):0;
		var bizDistance = (parseFloat(biz1Distance)+parseFloat(biz2Distance)+parseFloat(biz3Distance)).toFixed(2);
		bizDistanceTot += vo.biz1Distance?vo.biz1Distance:0+vo.biz2Distance?vo.biz2Distance:0;
		
		var biz1Fco = vo.biz1Fco?Math.round(parseInt(vo.biz1Fco)/10)/100:0;
		var biz2Fco = vo.biz2Fco?Math.round(parseInt(vo.biz2Fco)/10)/100:0;
		var biz3Fco = vo.biz3Fco?Math.round(parseInt(vo.biz3Fco)/10)/100:0;
		
		var bizFco = biz1Fco+biz2Fco+biz3Fco;
		
		var biz1Price = vo.biz1Price?parseInt(vo.biz1Price):0;
		var biz2Price = vo.biz2Price?parseInt(vo.biz2Price):0;
		var biz3Price = vo.biz3Price?parseInt(vo.biz3Price):0;
		var bizPrice = biz1Price+biz2Price+biz3Price;
		drivingTime += vo.drivingTime?parseInt(vo.drivingTime):0;
		bizTime += vo.biz?parseInt(vo.biz):0;
		bizCnt[0] += vo.bizCnt1?vo.bizCnt1:0;
		bizCnt[1] += vo.bizCnt2?vo.bizCnt2:0;
		bizCnt[2] += vo.bizCnt3?vo.bizCnt3:0;
		
		tableHtml += '<tr>';
		tableHtml += '<td rowspan="4" class="row_tit">';
		tableHtml += '<b>'+vo.corpGroupNm+'</b><br />';
		tableHtml += '(<fmt:message key='integrationReport.txt2'/> '+((vo.biz?vo.biz:0)/(vo.drivingTime?vo.drivingTime:1)*100).toFixed(1)+'%)';
		tableHtml += '</td>';
		tableHtml += '<td class="first bg_f7">';
		tableHtml += '<b><fmt:message key='integrationReport.totSum'/></b>';
		tableHtml += '</td>';
		tableHtml += '<td class="bg_f7">';
		tableHtml += '<div class="bslist">';
		tableHtml += '<span class="wid30"><b>'+number_format(bizDistance)+' km</b></span>';
		tableHtml += '<span class="wid30"><b>'+number_format(bizFco)+' ℓ</b></span>';
		tableHtml += '<span class="wid30"><b>'+number_format(bizPrice)+' <fmt:message key='integrationReport.won3'/></b></span>';
		tableHtml += '</div>';
		tableHtml += '</td>';
		tableHtml += '</tr>';
		tableHtml += '<tr>';
		tableHtml += '<td class="first">';
		tableHtml += '<fmt:message key='integrationReport.drivingType_1'/>';	
		tableHtml += '</td>';
		tableHtml += '<td>';
		tableHtml += '<div class="bslist">';
		tableHtml += '<span class="wid30"><b>'+number_format(biz1Distance)+' km</b></span>';
		tableHtml += '<span class="wid30"><b>'+number_format(biz1Fco)+' ℓ</b></span>';
		tableHtml += '<span class="wid30"><b>'+number_format(biz1Price)+' <fmt:message key='integrationReport.won4'/></b></span>';
		tableHtml += '</div>';
		tableHtml += '</td>';
		tableHtml += '</tr>';
		tableHtml += '<tr>';
		tableHtml += '<td class="first">';
		tableHtml += '<fmt:message key='integrationReport.drivingType_2'/>';
		tableHtml += '</td>';
		tableHtml += '<td>';
		tableHtml += '<div class="bslist">';
		tableHtml += '<span class="wid30"><b>'+number_format(biz2Distance)+' km</b></span>';
		tableHtml += '<span class="wid30"><b>'+number_format(biz2Fco)+' ℓ</b></span>';
		tableHtml += '<span class="wid30"><b>'+number_format(biz2Price)+' <fmt:message key='integrationReport.won5'/></b></span>';
		tableHtml += '</div>';
		tableHtml += '</td>';
		tableHtml += '</tr>';
		tableHtml += '<tr>';
		tableHtml += '<td class="first">';
		tableHtml += '<fmt:message key='integrationReport.drivingType_3'/>';
		tableHtml += '</td>';
		tableHtml += '<td>';
		tableHtml += '<div class="bslist">';
		tableHtml += '<span class="wid30"><b>'+number_format(biz3Distance)+' km</b></span>';
		tableHtml += '<span class="wid30"><b>'+number_format(biz3Fco)+' ℓ</b></span>';
		tableHtml += '<span class="wid30"><b>'+number_format(biz3Price)+' <fmt:message key='integrationReport.won6'/></b></span>';
		tableHtml += '</div>';
		tableHtml += '</td>';
		tableHtml += '</tr>';
	}
	$(".groupTripSummaryTable tbody").empty();
	$(".groupTripSummaryTable tbody").append(tableHtml);
	
	var rate = bizTime / drivingTime * 100;
	var quaterRate = bizTripSummaryQuater.biz / bizTripSummaryQuater.drivingTime * 100;
	$(".useWorkRate").text(""+rate.toFixed(1)+"%");
	$(".quater").text(""+bizTripSummaryQuater.quarter+"<fmt:message key="integrationReport.quater"/>");
	$(".corpTax").text(""+(((bizDistanceTot/1000)  /  (bizTripSummaryQuater.biz/1000) * 100) * (rate / 100)).toFixed(1)+"%");
	
	$(".bizCnt1").text(""+bizCnt[0]+"<fmt:message key="integrationReport.count9"/>");
	$(".bizCnt2").text(""+bizCnt[1]+"<fmt:message key="integrationReport.count10"/>");
	$(".bizCnt3").text(""+bizCnt[2]+"<fmt:message key="integrationReport.count11"/>");
	$(".bizRate").text(""+rate.toFixed(1)+"%");
	
	var interval = parseInt(maximum / 5);
	jsonBizArrayFive = jsonBizArray.slice(0);
	jsonCommuteArrayFive = jsonCommuteArray.slice(0);
	jsonNoneBizArrayFive = jsonNoneBizArray.slice(0);
	intervalFive = interval;
	chartFive(jsonBizArray, jsonCommuteArray, jsonNoneBizArray, interval);
}

function chartFive(jsonBizArray, jsonCommuteArray, jsonNoneBizArray, interval){
	var chart5 = new CanvasJS.Chart("chart5", {
		width:806,
		height:200,
		axisY: {
			labelFormatter:function(e){				
				return e.value+"<fmt:message key="integrationReport.ea"/>";				
			}
			,labelFontSize: 12
			,labelFontWeight : "lighter"
			,labelFontColor: "black"
			,lineThickness: 0
			,tickLength: 0
			,gridThickness: 1
			,interval: interval
		},
		axisX: {
			interval: 2
			,labelFormatter:function(e){				
				return (e.value)+"<fmt:message key="integrationReport.hour"/>";		
				
			}
			,labelFontSize: 12
			,labelFontWeight : "lighter"
			,labelFontColor: "black"
			,lineThickness: 1
			,tickLength: 5
		},
		toolTip : {
			fontStyle : "normal"
			,fontSize : 14
			,fontWeight : "normal"
			,fontFamily : "sans-serif"
		},
		legend:{
			fontStyle : "normal"
			,fontSize : 12
			,fontWeight : "normal"
			,fontFamily : "sans-serif"
	       	,horizontalAlign: "right" // left, center ,right 
	        ,verticalAlign: "top" // top, center, bottom
	      },
	      
	      dataPointWidth: 17,
		data: [{
			type: "stackedColumn"
			,color : "#ffc71c"
			,legendText: "<fmt:message key="integrationReport.drivingType1_1"/>"
			,showInLegend: true 
			,toolTipContent: "<fmt:message key="integrationReport.drivingType1_1_1"/> {y}<fmt:message key="integrationReport.drivingType1_2_1"/>"
			,dataPoints: []
		},{
			type: "stackedColumn"
			,color : "#ca58ff"
			,legendText: "<fmt:message key="integrationReport.drivingType2_1"/>"
			,showInLegend: true
			,toolTipContent: "<fmt:message key="integrationReport.drivingType2_1_1"/> {y}<fmt:message key="integrationReport.drivingType2_2_1"/>"
			,dataPoints: []
		},{
			type: "stackedColumn"
			,color : "#ed1c24"			
			,legendText: "<fmt:message key="integrationReport.drivingType3_1"/>"
			,showInLegend: true
			,toolTipContent: "<fmt:message key="integrationReport.drivingType3_1_1"/> {y}<fmt:message key="integrationReport.drivingType3_2_1"/>"
			,dataPoints: []
		}]
	});
	
	for(var i = 0;i < jsonBizArray.length;i++){
		chart5.options.data[0].dataPoints.push(jsonBizArray[i]);
		chart5.options.data[1].dataPoints.push(jsonCommuteArray[i]);
		chart5.options.data[2].dataPoints.push(jsonNoneBizArray[i]);
	}
	chart5.render();
}

function chart6Area(expenseSummary, expenseSummaryPrev, expenseSummaryGroup, expenseSummaryGroupPrev){
	var price = new Array(expenseSummary.length);
	var jsonPriceArray = new Array();
	var priceTemp;
	var maximum = 0;
	var currentPrice=[0,0,0,0,0,0,0,0];
	var totalExpense = 0;
	
	for(var i = 0;i < expenseSummary.length;i++){
		priceTemp = new Object();
		price[i] = expenseSummary[i].priceAll?expenseSummary[i].priceAll:null;
		if(maximum < price[i]) maximum = price[i];
		
		priceTemp.y = price[i];
		if(expenseSummary[i].selFlag == "1"){
			priceTemp.color = "red";
			currentPrice[0] = expenseSummary[i].price1?expenseSummary[i].price1:0;	// 주유
			currentPrice[1] = expenseSummary[i].price2?expenseSummary[i].price2:0;	// 정비
			currentPrice[2] = expenseSummary[i].price3?expenseSummary[i].price3:0;	// 주차
			currentPrice[3] = expenseSummary[i].price4?expenseSummary[i].price4:0;	// 통행료
			currentPrice[4] = expenseSummary[i].price5?expenseSummary[i].price5:0;	// 렌탈료
			currentPrice[5] = expenseSummary[i].price6?expenseSummary[i].price6:0;	// 세차
			currentPrice[6] = expenseSummary[i].price7?expenseSummary[i].price7:0;	// 과태료
			currentPrice[7] = expenseSummary[i].price8?expenseSummary[i].price8:0;	// 기타
			totalExpense = expenseSummary[i].priceAll?expenseSummary[i].priceAll:0;
		}
		jsonPriceArray.push(priceTemp);
	}
	var priceHtml = '';
	var priceAll = expenseSummaryPrev?expenseSummaryPrev.priceAll:0;
	var price1 = expenseSummaryPrev?expenseSummaryPrev.price1:0;
	var price2 = expenseSummaryPrev?expenseSummaryPrev.price2:0;
	var price3 = expenseSummaryPrev?expenseSummaryPrev.price3:0;
	var price4 = expenseSummaryPrev?expenseSummaryPrev.price4:0;
	var price5 = expenseSummaryPrev?expenseSummaryPrev.price5:0;
	var price6 = expenseSummaryPrev?expenseSummaryPrev.price6:0;
	var price7 = expenseSummaryPrev?expenseSummaryPrev.price7:0;
	var price8 = expenseSummaryPrev?expenseSummaryPrev.price8:0;
	
	priceHtml = ''+number_format(currentPrice[0])+'<fmt:message key='integrationReport.won7'/>';
	if(currentPrice[0] > price1){
		priceHtml += '<span>('+number_format(currentPrice[0] - price1)+'<fmt:message key='integrationReport.won8'/> <i class="icon up"></i> )</span>';
	}
	else if(currentPrice[0] < price1){
		priceHtml += '<span>('+number_format(price1 - currentPrice[0])+'<fmt:message key='integrationReport.won9'/> <i class="icon down"></i> )</span>';
	}
	else{
		priceHtml += '<span>(0<fmt:message key='integrationReport.won10'/> <i class="icon none">-</i> )</span>';
	}
	$(".refuel").html(priceHtml);
	
	priceHtml = ''+number_format(currentPrice[1])+'<fmt:message key='integrationReport.won11'/>';
	if(currentPrice[1] > price2){
		priceHtml += '<span>('+number_format(currentPrice[1] - price2)+'<fmt:message key='integrationReport.won12'/> <i class="icon up"></i> )</span>';
	}
	else if(currentPrice[1] < price2){
		priceHtml += '<span>('+number_format(price2 - currentPrice[1])+'<fmt:message key='integrationReport.won13'/> <i class="icon down"></i> )</span>';
	}
	else{
		priceHtml += '<span>(0<fmt:message key='integrationReport.won14'/> <i class="icon none">-</i> )</span>';
	}
	$(".clinic").html(priceHtml);
	
	priceHtml = ''+number_format(currentPrice[2])+'<fmt:message key='integrationReport.won15'/>';
	if(currentPrice[2] > price3){
		priceHtml += '<span>('+number_format(currentPrice[2] - price3)+'<fmt:message key='integrationReport.won16'/> <i class="icon up"></i> )</span>';
	}
	else if(currentPrice[2] < price3){
		priceHtml += '<span>('+number_format(price3 - currentPrice[2])+'<fmt:message key='integrationReport.won17'/> <i class="icon down"></i> )</span>';
	}
	else{
		priceHtml += '<span>(0<fmt:message key='integrationReport.won18'/> <i class="icon none">-</i> )</span>';
	}
	$(".park").html(priceHtml);
	
	priceHtml = ''+number_format(currentPrice[3])+'<fmt:message key='integrationReport.won19'/>';
	if(currentPrice[3] > price4){
		priceHtml += '<span>('+number_format(currentPrice[3] - price4)+'<fmt:message key='integrationReport.won20'/> <i class="icon up"></i> )</span>';
	}
	else if(currentPrice[3] < price4){
		priceHtml += '<span>('+number_format(price4 - currentPrice[3])+'<fmt:message key='integrationReport.won21'/> <i class="icon down"></i> )</span>';
	}
	else{
		priceHtml += '<span>(0<fmt:message key='integrationReport.won22'/> <i class="icon none">-</i> )</span>';
	}
	$(".toll").html(priceHtml);
	
	priceHtml = ''+number_format(currentPrice[4])+'<fmt:message key='integrationReport.won23'/>';
	if(currentPrice[4] > price5){
		priceHtml += '<span>('+number_format(currentPrice[4] - price5)+'<fmt:message key='integrationReport.won24'/> <i class="icon up"></i> )</span>';
	}
	else if(currentPrice[4] < price5){
		priceHtml += '<span>('+number_format(price5 - currentPrice[4])+'<fmt:message key='integrationReport.won25'/> <i class="icon down"></i> )</span>';
	}
	else{
		priceHtml += '<span>(0<fmt:message key='integrationReport.won26'/> <i class="icon none">-</i> )</span>';
	}
	$(".rental").html(priceHtml);
	
	priceHtml = ''+number_format(currentPrice[5])+'<fmt:message key='integrationReport.won27'/>';
	if(currentPrice[5] > price6){
		priceHtml += '<span>('+number_format(currentPrice[5] - price6)+'<fmt:message key='integrationReport.won28'/> <i class="icon up"></i> )</span>';
	}
	else if(currentPrice[5] < price6){
		priceHtml += '<span>('+number_format(price6 - currentPrice[5])+'<fmt:message key='integrationReport.won29'/> <i class="icon down"></i> )</span>';
	}
	else{
		priceHtml += '<span>(0<fmt:message key='integrationReport.won30'/> <i class="icon none">-</i> )</span>';
	}
	$(".wash").html(priceHtml);
	
	priceHtml = ''+number_format(currentPrice[6])+'<fmt:message key='integrationReport.won31'/>';
	if(currentPrice[6] > price7){
		priceHtml += '<span>('+number_format(currentPrice[6] - price7)+'<fmt:message key='integrationReport.won32'/> <i class="icon up"></i> )</span>';
	}
	else if(currentPrice[6] < price7){
		priceHtml += '<span>('+number_format(price7 - currentPrice[6])+'<fmt:message key='integrationReport.won33'/> <i class="icon down"></i> )</span>';
	}
	else{
		priceHtml += '<span>(0<fmt:message key='integrationReport.won34'/> <i class="icon none">-</i> )</span>';
	}
	$(".penalty").html(priceHtml);
	
	priceHtml = ''+number_format(currentPrice[7])+'<fmt:message key='integrationReport.won35'/>';
	if(currentPrice[7] > price8){
		priceHtml += '<span>('+number_format(currentPrice[7] - price8)+'<fmt:message key='integrationReport.won36'/> <i class="icon up"></i> )</span>';
	}
	else if(currentPrice[7] < price8){
		priceHtml += '<span>('+number_format(price8 - currentPrice[7])+'<fmt:message key='integrationReport.won37'/> <i class="icon down"></i> )</span>';
	}
	else{
		priceHtml += '<span>(0<fmt:message key='integrationReport.won38'/> <i class="icon none">-</i> )</span>';
	}
	$(".etc").html(priceHtml);

	var totalHtml = '';
	for(var i = 0;i < expenseSummaryGroup.length;i++){
		var vo = expenseSummaryGroup[i];
		var voPrev = expenseSummaryGroupPrev[i];
		var currentGroupPrice = vo.priceAll?vo.priceAll:0;
		var prevGroupPrice = voPrev.priceAll?voPrev.priceAll:0;
		
		if(currentGroupPrice != 0 || prevGroupPrice != 0)
		{
		totalHtml += '<tr>';
		totalHtml += '<td class="row_tit">'+vo.corpGroupNm+'</td>';
		totalHtml += '<td>';
		totalHtml += ''+number_format(currentGroupPrice)+'<fmt:message key='integrationReport.won39'/> ';
		if(currentGroupPrice > prevGroupPrice)
			totalHtml += '<span>('+number_format(currentGroupPrice - prevGroupPrice)+'<fmt:message key='integrationReport.won40'/> <i class="icon up"></i> )</span>';
		else if(currentGroupPrice < prevGroupPrice)
			totalHtml += '<span>('+number_format(prevGroupPrice - currentGroupPrice)+'<fmt:message key='integrationReport.won41'/> <i class="icon down"></i> )</span>';
		else totalHtml += '<span>(0<fmt:message key='integrationReport.won42'/> <i class="icon none">-</i> )</span>';
		totalHtml += '</td>';
		totalHtml += '</tr>';
		}
	}
	
	$(".expenseTotalTable tbody").empty();
	$(".expenseTotalTable tbody").append(totalHtml);

	$(".totalExpense").text(""+number_format(totalExpense)+"<fmt:message key="integrationReport.won43"/>");
	
	var ratio = ((totalExpense - priceAll) / totalExpense * 100).toFixed(1);
	if(!isFinite(ratio)) ratio = 0;
	
	if(totalExpense > priceAll)
		$(".compareExpenseRate").text("<fmt:message key="integrationReport.compareExpenseRate"/> "+ratio+"%("+number_format(totalExpense - priceAll)+"<fmt:message key="integrationReport.compareExpenseRateUp"/>");
	else if(totalExpense < priceAll)
		$(".compareExpenseRate").text("<fmt:message key="integrationReport.compareExpenseRate2"/> "+ratio+"%("+number_format(totalExpense - priceAll)+"<fmt:message key="integrationReport.compareExpenseRateDown"/>");
	else $(".compareExpenseRate").text("<fmt:message key="integrationReport.txt3"/>");
	
	jsonPriceArraySix = jsonPriceArray.slice(0);
	chartSix(jsonPriceArray);
}

function chartSix(jsonPriceArray){	
	var chart6 = new CanvasJS.Chart("chart6", {
		width:806,
		height:200,
		axisY: {
			labelFormatter:function(e){
				return CanvasJS.formatNumber(e.value/10000, "#,##0")+"<fmt:message key="integrationReport.txt4"/>" 
			}
			,labelFontSize: 12
			,labelFontWeight : "lighter"
			,labelFontColor: "black"
			//,interlacedColor: "#F0F8FF" 
			,lineThickness: 0
			,tickLength: 0
			,gridThickness: 1
		},
		axisX: {
			interval: 1
			,labelFormatter:function(e){				
				return (e.value+1)+"<fmt:message key="integrationReport.month3"/>";		
				
			}
			,labelFontSize: 12
			,labelFontWeight : "lighter"
			,labelFontColor: "black"
			,lineThickness: 1
			,tickLength: 5
		},
		
		toolTip : {
			fontStyle : "normal"
			,fontSize : 14
			,fontWeight : "normal"
			,fontFamily : "sans-serif"
			,contentFormatter: function(e){
		          var val = e.entries[0].dataPoint.y;
		          var str = CanvasJS.formatNumber(val, "#,###")+"<fmt:message key="integrationReport.won44"/>";
		          return str; 
		    }

		},
		data: [{
			type: "line"
			,markerSize: 12
			,lineColor : "#cccdc9"
			,color : "#cccdc9"
			,dataPoints: []
		}]
	});
	
	for(var i = 0;i < jsonPriceArray.length;i++){
		chart6.options.data[0].dataPoints.push(jsonPriceArray[i]);
	}
	chart6.render();

	
	
}

</script>
<input type="hidden" id="vehicleKey" value="">
<form id="vehicleFrm" style="display:none">
	<input type="hidden" name="searchGroup" value="" />
	<input type="hidden" name="searchText" value="" />
	<!-- <input type="hidden" name="searchUsed" value="1" /> -->
	<input type="hidden" name="searchType" value="plateNum" />
</form>
<div class="right_layout">
	<div class="auth admin contents">
		<h2 class="tit_sub noneHide">
			<fmt:message key='integrationReport.txt5'/>
			<p><fmt:message key='integrationReport.txt5_1'/></p>
		</h2>
		<div class="search_box1 search_type mgb0 noneHide">
			<div class="float_left">
				<input id="searchMonth" type="text" class="date datePickerMonth" style="height:36px; border:1px solid #c0c0c0;"/>
				<select class="select form" id="corpGroup" style="width:160px;">
					<option value=""><fmt:message key='integrationReport.allCorpGroup2'/></option>
				</select>
				<div class="car_choice_search_box" style="float:none; display:inline-block; height:36px; background-color:white;">
					<input type="text" class='btn_ajaxPop' name="searchVehicle" readonly style="padding-left:20px; height:36px;"/>
					<a href="#none" id="btn_ajaxPop" style="top:5px;"><img src="${pathCommon}/images/btn_search3.jpg" alt=""></a>
				</div>
			</div>
			<div class="float_right">
				<a href="#none" class="btn_search"><fmt:message key='integrationReport.search'/></a>
				<a href="#none" class="btn_black"><fmt:message key='integrationReport.reportPrint'/></a>
			</div>
		</div>
		<form id="printFrm">
		<div class="count_box mgt25">
			<fmt:message key='integrationReport.groupCount'/> <em class="mgr15 groupCnt">xx개</em>    <fmt:message key='integrationReport.userCount'/> <em class="mgr15 userCnt">xxx명</em>    <fmt:message key='integrationReport.vehicleCount'/> <em class="mgr15 vehicleCnt">xx대</em>
		</div>
		<!-- 운행정보 -->
		<h3 class="tit01 mgt30"><fmt:message key='integrationReport.avgDrivingInfo'/><i></i></h3>
		<div class="tbl_layout mgt20">
			<div class="row">
				<div class="col" style="width:310px;">
					<div id="myChart01" class="myChart">
						<div class="level">A <i class="icon up"></i> </div>
					</div>
					<div class="chart_explain">
						<table class="tbl tbl_explain">
							<colgroup>
								<col style="width:95px;"/>
								<col />
							</colgroup>
							<tr>
								<td><span class="color purple"></span><fmt:message key='integrationReport.fuelEfficiency3'/></td>
								<td><span id="fuelRank"></span></td>
							</tr>
							<tr>
								<td><span class="color pink"></span><fmt:message key='integrationReport.safeScore3'/></td>
								<td><span id="safeRank"></span></td>
							</tr>
							<tr>
								<td><span class="color blue"></span><fmt:message key='integrationReport.ecoScroe3'/></td>
								<td><span id="ecoRank"></span></td>
							</tr>
						</table>
					</div>
				</div>
				<div class="col" style="width:430px; float:right;">
					<table class="tbl tbl_type02">
						<colgroup>
							<col style="width:125px;" />
							<col />
						</colgroup>
						<tr>
							<th scope="row"><fmt:message key='integrationReport.distance'/></th>
							<td><span id="distance"></span></td>
						</tr>
						<tr>
							<th scope="row"><fmt:message key='integrationReport.drivingTime'/></th>
							<td><span id="drivingTime"></span></td>
						</tr>
						<tr>
							<th scope="row"><fmt:message key='integrationReport.fco'/></th>
							<td><span id="fco"></span></td>
						</tr>
						<tr>
							<th scope="row"><fmt:message key='integrationReport.oilPrice'/></th>
							<td><span id="oilPrice"></span><br/>(<fmt:message key='integrationReport.txt8'/>)</td>
						</tr>
					</table>
				</div>
			</div>
			<!-- 806x157 -->
			<div id="chart1" style="width:806px;height: 157px;"></div>
			<div class="graph_txt1">
				<span>
					<strong><fmt:message key='integrationReport.avgFco'/></strong>
					<em id="avgFco">0 km/L</em>
				</span>
				<span>
					<strong><fmt:message key='integrationReport.comFco'/></strong>
					<em id="comFco">0 km/L</em>
				</span>
			</div>
			<div class="map_txt1">
				<div>
					<span class="w109">
						<img src="${pathCommon}/images/m_img1.png" alt=""><fmt:message key='integrationReport.rapidStart'/> <strong class="rapidStart"><fmt:message key='integrationReport.zeroCount9'/></strong>
					</span><span class="w120">
						<img src="${pathCommon}/images/m_img2.png" alt=""><fmt:message key='integrationReport.rapidStop'/> <strong class="rapidStop"><fmt:message key='integrationReport.zeroCount10'/></strong>
					</span>
					<span class="w120">
						<img src="${pathCommon}/images/m_img4.png" alt=""><fmt:message key='integrationReport.rapidAccel'/> <strong class="rapidAccel"><fmt:message key='integrationReport.zeroCount11'/></strong>
					</span>
					<span class="w109">
						<img src="${pathCommon}/images/m_img5.png" alt=""><fmt:message key='integrationReport.rapidDeaccel'/> <strong class="rapidDeaccel"><fmt:message key='integrationReport.zeroCount12'/></strong>
					</span>
					<span class="w120">
						<img src="${pathCommon}/images/m_img10.png" alt=""><fmt:message key='integrationReport.rapidTurn'/> <strong class="rapidTurn"><fmt:message key='integrationReport.zeroCount13'/></strong>
					</span>
				</div>
				<div>
					<span class="w109">
						<img src="${pathCommon}/images/m_img11.png" alt=""><fmt:message key='integrationReport.rapidUtern'/> <strong class="rapidUtern"><fmt:message key='integrationReport.zeroCount14'/></strong>
					</span>
					<span class="w109">
						<img src="${pathCommon}/images/m_img6.png" alt=""><fmt:message key='integrationReport.overSpeed'/> <strong class="overSpeed"><fmt:message key='integrationReport.zeroCount15'/></strong>
					</span>
					<span class="w120">
						<img src="${pathCommon}/images/m_img7.png" alt=""><fmt:message key='integrationReport.overSpeedLong'/> <strong class="longTerm"><fmt:message key='integrationReport.zeroCount16'/></strong>
					</span>
					<%-- <span class="w178">
						<img src="${pathCommon}/images/m_img12.png" alt="">심야운전 <strong class="nightDrive">0회</strong>
					</span> --%>
				</div>
				<%-- <div>
					<span class="w178">
						<img src="${pathCommon}/images/m_img9.png" alt="">사고다발지역통과 <strong class="no_num">0회</strong>
					</span>
				</div> --%>
			</div>
			<table class="table1 bs_table mgt20 groupScoreTable">
				<colgroup>
					<col style="width:109px;" />
					<col  />
				</colgroup>
				<thead>
					<tr>
						<th><fmt:message key='integrationReport.corp'/></th>
						<th><fmt:message key='integrationReport.content'/></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
			
		</div>
		<!-- 운행정보 끝 -->
	
		<!-- 평균 운행률 -->
		<div class="gray_bg_box mgt80">
			<span><fmt:message key='integrationReport.groupCount2'/> <em class="groupCnt">xx개</em></span>
			<span><fmt:message key='integrationReport.userCount2'/> <em class="userCnt">xxx명</em></span>
			<span><fmt:message key='integrationReport.vehicleCount2'/> <em class="vehicleCnt">xx대</em></span>
		</div>
		<h3 class="tit01 mgt30"><fmt:message key='integrationReport.txt9'/><i></i></h3>
		<div class="tbl_layout">
			<div id="chart2" style="width:806px;"></div>
			<div class="bn_box mgt20">
				<span class="bn_left">
					<strong class="bg_green"></strong>
					<em class="color_green bizTime"><fmt:message key='integrationReport.bizTime2'/></em>
				</span>
				<span class="bn_left">
					<strong class="bg_red"></strong>
					<em class="color_red noneBizTime"><fmt:message key='integrationReport.noneBizTime2'/></em>
				</span>
				<p class="bn_right">(<fmt:message key='integrationReport.txt10'/>)</p>
			</div>
			<ul class="bn_p">
				<li><fmt:message key='integrationReport.txt11'/> <em class="color_red tripRate">xx%</em><fmt:message key='integrationReport.txt11_1'/> <em class="color_red comparePrevRate">x% 상승</em><fmt:message key='integrationReport.txt11_2'/></li>
				<li><fmt:message key='integrationReport.txt12'/> <em class="totalVehicleCnt">xx대</em><fmt:message key='integrationReport.txt12_1'/> <em class="color_red noneTripCnt">x대</em><fmt:message key='integrationReport.txt12_2'/> <em class="color_red"><fmt:message key='integrationReport.txt12_3'/></em> <fmt:message key='integrationReport.txt12_4'/> <em class="color_red noneGroupTripCnt">x개</em> <fmt:message key='integrationReport.txt12_5'/></li>
			</ul>
			
		</div>
		<!-- 평균 운행률 끝 -->
		<!-- 사용 목적 [기간별] -->
		<div class="gray_bg_box mgt80">
			<span><fmt:message key='integrationReport.vehicleDrivingType1'/> <em class="bizCnt1">xxx회</em></span>
			<span><fmt:message key='integrationReport.vehicleDrivingType2'/> <em class="bizCnt2">xx회</em></span>
			<span><fmt:message key='integrationReport.vehicleDrivingType3'/> <em class="bizCnt3">xx회</em></span>
			<span><fmt:message key='integrationReport.vehicleDrivingType4'/> <em class="bizRate">xx%</em></span>
		</div>
		<div class="bxtit mgt30">
			<h3 class="tit01"><fmt:message key='integrationReport.txt13'/><i></i></h3>
			<%-- <ul class="bx_icons">
				<li>
					<img src="${pathCommon}/images/main_icon9.jpg" alt="" />
					업무용
				</li>
				<li>
					<img src="${pathCommon}/images/main_icon8.jpg" alt="" />
					출퇴근용
				</li>
				<li>
					<img src="${pathCommon}/images/main_icon7.jpg" alt="" />
					비업무용
				</li>
			</ul> --%>
		</div>
		<div id="chart4" style="width:806px;height:200px;"></div>
		<!-- 사용 목적 [기간별] 끝 -->
	
		<!-- 사용 목적 [시간대별] -->
		<div class="bxtit mgt30">
			<h3 class="tit01"><fmt:message key='integrationReport.txt14'/><i></i></h3>
			<%-- <ul class="bx_icons">
				<li>
					<img src="${pathCommon}/images/main_icon9.jpg" alt="" />
					업무용
				</li>
				<li>
					<img src="${pathCommon}/images/main_icon8.jpg" alt="" />
					출퇴근용
				</li>
				<li>
					<img src="${pathCommon}/images/main_icon7.jpg" alt="" />
					비업무용
				</li>
			</ul> --%>
		</div>
		<div id="chart5" style="width:806px;height:200px;"></div>
		<table class="table1 bs_table mgt20 groupTripSummaryTable">
			<colgroup>
				<col style="width:177px;" />
				<col style="width:177px;" />
				<col  />
			</colgroup>
			<thead>
				<tr>
					<th><fmt:message key='integrationReport.corp2'/></th>
					<th><fmt:message key='integrationReport.item'/></th>
					<th><fmt:message key='integrationReport.content2'/></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		<ul class="bn_p">
			<li><fmt:message key='integrationReport.txt15'/> <em class="color_red useWorkRate">xx%</em><fmt:message key='integrationReport.txt15_1'/> <em class="quater">x분기</em> <fmt:message key='integrationReport.txt15_2'/> <em class="color_red corpTax">xx%</em><fmt:message key='integrationReport.txt15_3'/></li>
		</ul>
		<!-- 사용 목적 [시간대별] 끝 -->
	
		<!-- 평균 운영비 -->
		<div class="gray_bg_box mgt80">
			<span><fmt:message key='integrationReport.totalExpense'/> <em class="totalExpense">xxx,xxx,000원</em></span>
		</div>
		<h3 class="tit01 mgt30"><fmt:message key='integrationReport.avgExpense'/><i></i></h3>
		<div id="chart6" style="width:806px;height:200px;"></div>
		
		
		<div class="tbl_layout">
			<div class="row">
				<div class="col col-2">
					<table class="table1 table1_2 tbl_count" style="width:393px; float:left;">
						<colgroup>
							<col style="width:120px;"/>
							<col />
						</colgroup>
						<thead>
							<tr>
								<th scope="col"><fmt:message key='integrationReport.item2'/></th>
								<th scope="col"><fmt:message key='integrationReport.txt16'/></th>
							</tr>
						</thead>
						<tr>
							<td class="bg_bar"><fmt:message key='integrationReport.refuel'/></td>
							<td class="refuel">
								<fmt:message key='integrationReport.zeroWon1'/>
								<span>(<fmt:message key='integrationReport.zeroWon2'/> <i class="icon none">-</i> )</span>
							</td>
						</tr>
						<tr>
							<td class="bg_bar"><fmt:message key='integrationReport.park'/></td>
							<td class="park">
								<fmt:message key='integrationReport.zeroWon3'/>
								<span>(<fmt:message key='integrationReport.zeroWon4'/> <i class="icon none">-</i> )</span>
							</td>
						</tr>
						<tr>
							<td class="bg_bar"><fmt:message key='integrationReport.toll'/></td>
							<td class="toll">
								<fmt:message key='integrationReport.zeroWon5'/>
								<span>(<fmt:message key='integrationReport.zeroWon6'/> <i class="icon none">-</i> )</span>
							</td>
						</tr>
						<tr>
							<td class="bg_bar"><fmt:message key='integrationReport.rental'/></td>
							<td class="rental">
								<fmt:message key='integrationReport.zeroWon7'/>
								<span>(<fmt:message key='integrationReport.zeroWon8'/> <i class="icon none">-</i> )</span>
							</td>
						</tr>
					</table>
				</div>
				<div class="col col-2">
					<table class="table1 table1_2 tbl_count" style="width:393px; float:right;">
						<colgroup>
							<col style="width:120px;"/>
							<col />
						</colgroup>
						<thead>
							<tr>
								<th scope="col"><fmt:message key='integrationReport.item3'/></th>
								<th scope="col"><fmt:message key='integrationReport.txt16_1'/></th>
							</tr>
						</thead>
						<tr>
							<td class="bg_bar"><fmt:message key='integrationReport.wash'/></td>
							<td class="wash">
								<fmt:message key='integrationReport.zeroWon9'/>
								<span>(<fmt:message key='integrationReport.zeroWon10'/> <i class="icon none">-</i> )</span>
							</td>
						</tr>
						<tr>
							<td class="bg_bar"><fmt:message key='integrationReport.clinic'/></td>
							<td class="clinic">
								<fmt:message key='integrationReport.zeroWon11'/>
								<span>(<fmt:message key='integrationReport.zeroWon12'/> <i class="icon none">-</i> )</span>
							</td>
						</tr>
						<tr>
							<td class="bg_bar"><fmt:message key='integrationReport.penalty'/></td>
							<td class="penalty">
								<fmt:message key='integrationReport.zeroWon13'/>
								<span>(<fmt:message key='integrationReport.zeroWon14'/> <i class="icon none">-</i> )</span>
							</td>
						</tr>
						<tr>
							<td class="bg_bar"><fmt:message key='integrationReport.etc'/></td>
							<td class="etc">
								<fmt:message key='integrationReport.zeroWon15'/>
								<span>(<fmt:message key='integrationReport.zeroWon16'/> <i class="icon none">-</i> )</span>
							</td>
						</tr>
					</table>
					
				</div>
				
			</div>
			<table class="table1 tbl_count bs_table mgt20 expenseTotalTable">
				<colgroup>
					<col style="width:177px;" />
					<col  />
				</colgroup>
				<thead>
					<tr>
						<th><fmt:message key='integrationReport.corp3'/></th>
						<th><fmt:message key='integrationReport.totalSum'/></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
			<ul class="bn_p">
				<li><fmt:message key='integrationReport.txt17'/> <em class="color_red totalExpense">xxx,000원</em><fmt:message key='integrationReport.txt17_1'/> <em class="color_red compareExpenseRate"><fmt:message key='integrationReport.txt17_2'/></em><fmt:message key='integrationReport.txt17_3'/></li>
			</ul>
		</div>
		<!-- 평균 운영비 끝 -->
		</form>
	</div>
</div>
<div class="black">
	<!-- <div class="blackLoading" style="widht:50%;height:50%;position: fixed;padding-left: 43%;padding-top: 17%;">
    	<img src="/vdasPro/common/images/loading4.gif" style="width:300px;height:300px">
    </div> -->
</div>
<div class="white">
	<div class="blackLoading" style="widht:50%;height:50%;position: fixed;padding-left: 43%;padding-top: 17%;">
    	<img src="/vdasPro/common/images/loading4.gif" style="width:300px;height:300px">
    </div>
</div>

<div class="gray">
	<div class="blackLoading" style="widht:50%;height:50%;position: fixed;padding-left: 43%;padding-top: 17%;">
    	<img src="/vdasPro/common/images/loading4.gif" style="width:300px;height:300px">
    </div>
</div>

<div class="pop7 vehicle" style="display:none;">
	<div class="pop6_area">
		<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt="" /></a>
		<h1><fmt:message key='integrationReport.vehicleSelect'/></h1>
		<div class="txt">
			<p><fmt:message key='integrationReport.txt18'/></p>
		</div>
		<div class="car_choice_btn2">
			<div class="car_choice_search_box">
				<input type="text" placeholder="직접검색" id="searchTextVehicle"/><a href="#none" id="btn_searchVehicle"><img src="${pathCommon}/images/btn_search3.jpg" alt=""></a>
			</div>
		</div>
		<table class="table1" id="contentsTableVehicle">
			<colgroup>
				<col style="width:60px;" />
				<col style="width:70px;" />
				<col style="width:230px;" />
				<col style="width:70px;" />
				<col style="width:100px;" />
				<col style="width:112px;" />
				<col style="width:118px;" />
			</colgroup>
			<thead>
				<tr>
					<th></th>
					<th><fmt:message key='integrationReport.manufacture'/></th>
					<th><fmt:message key='integrationReport.vehicleInfo'/></th>
					<th><fmt:message key='integrationReport.volumn'/></th>
					<th><fmt:message key='integrationReport.vehicleBizType'/></th>
					<th><fmt:message key='integrationReport.yearAndDistance'/></th>
					<th><fmt:message key='integrationReport.colorAndFuelType'/></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		<br><br>
		<div class="btn_type1"><a href="#none" class="btn_cancel"><fmt:message key='integrationReport.cancle'/></a><a href="#none" class="btn_ok"><fmt:message key='integrationReport.ok'/></a></div>
	</div>
</div>
</c:if>
<c:if test="${VDAS.rule ne '0' and VDAS.rule ne '1' }">
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("M4001");
});
</script>
<div class="right_layout">
	<img src="${pathCommon}/images/${_LANG}/authError.png" alt="" style="width:100%">
</div>
</c:if>