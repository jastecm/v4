<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/index.js"></script>

<script type="text/javascript">
var rData = ${rData};
$(document).ready(function(){
	$(".sub_layout").css("padding-top","0px").css("padding-left","0px");
	draw(rData);
	
	setTimeout(function(){
		window.print();
	},100);
});



function draw(a){
	chart1Area(a.getInfoSummaryTotalMonth, a.getSuddenSummaryTotalAll, a.getScoreSummaryAccountAll);
	chart4Area(a.getBizTripSummaryTotalMonth);
	chart5Area(a.getBizTripSummaryTotalHours, a.getBizTripSummaryAccountAll, a.getBizTripSummaryTotalQuater);
	chart6Area(a.getExpenseSummaryTotalMonth, a.getExpenseSummaryTotalPrev, a.getExpenseSummaryAccountAll, a.getExpenseSummaryAccountPrev);
}


function SuddenSummaryInit(){
	$(".rapidStart").text("0회");
	$(".rapidStop").text("0회");
	$(".rapidAccel").text("0회");
	$(".rapidDeaccel").text("0회");
	$(".rapidTurn").text("0회");
	$(".rapidUtern").text("0회");
}

function suddenSummaryCall(a){
	$(".rapidStart").text(a.rapidStart+"회");
	$(".rapidStop").text(a.rapidStop+"회");
	$(".rapidAccel").text(a.rapidAccel+"회");
	$(".rapidDeaccel").text(a.rapidDeaccel+"회");
	$(".rapidTurn").text(a.rapidTurn+"회");
	$(".rapidUtern").text(a.rapidUtern+"회");
}

function chart1Area(infoSummary, suddenSummary, scoreSummary){
	var drivingTime;
	var score4;
	var safeScore;
	var ecoScore;
	var fuelRank;
	var safeRank;
	var ecoRank;
	var totalRank;
	var month=[0,0,0,0,0,0,0,0,0,0,0,0];
	var totalDistance = 0;
	var totalFco = 0;
	var totalFuelEff = 0;
	var count = 0;
	var selFlag=[null,null,null,null,null,null,null,null,null,null,null,null];
	$("#myChart01").html('<div class="level"><span id="totalRank"></span> <i class="icon totalIcon"></i></div>');
	$("#fuelRank").text("");
	$("#safeRank").text("");
	$("#ecoRank").text("");
	$("#distance").text(" km");
	$("#drivingTime").text("시간 분 초");
	$("#fco").text(" ℓ");
	$("#oilPrice").text(" 원");
	console.log(infoSummary);
	
	for(var i=0;i<infoSummary.length;i++){
		month[i] = infoSummary[i].officialFuelEff?parseInt(infoSummary[i].officialFuelEff):null;
		totalDistance += infoSummary[i].distance?parseInt(infoSummary[i].distance):0;
		totalFco += infoSummary[i].fco?parseInt(infoSummary[i].fco):0;
		totalFuelEff += month[i];
		if(infoSummary[i].officialFuelEff != null) count++;
		if(infoSummary[i].selFlag == "1"){
			selFlag[i] = "red";
			drivingTime = infoSummary[i].drivingTime?infoSummary[i].drivingTime:0;
			score4 = infoSummary[i].score4?infoSummary[i].score4:0;
			safeScore = infoSummary[i].safeScore?infoSummary[i].safeScore:0;
			ecoScore = infoSummary[i].ecoScore?infoSummary[i].ecoScore:0;
			
			fuelRank = calRank(score4);
			safeRank = calRank(safeScore);
			ecoRank = calRank(ecoScore);
			totalRank = calRank((score4+safeScore+ecoScore)/3);
			
		    // getInfoSummaryTotalMonth
			$("#myChart01").drawDoughnutChart([
		 	    { title: "연비", value : score4,  color: "#a150cb" },
		  	    { title: "안전점수", value : safeScore,  color: "#ea2b7e" },
		  	    { title: "에코점수", value:  ecoScore,   color: "#68b2c2" }
		    ],{animation : false});

			$("#totalRank").text(""+totalRank);
			if(true)
				$(".totalIcon").addClass("up");
			else
				$(".totalIcon").addClass("down");
			$("#fuelRank").text(""+fuelRank);
			$("#safeRank").text(""+safeRank);
			$("#ecoRank").text(""+ecoRank);
			$("#distance").text(""+number_format(parseFloat(infoSummary[i].distance?(infoSummary[i].distance/1000):0).toFixed(2))+" km");
			$("#drivingTime").text(""+parseInt(drivingTime/3600)+'시간 '+LPAD(''+parseInt((drivingTime%3600)/60),'0',2)+'분 '+LPAD(''+parseInt((drivingTime%3600)%60),'0',2)+'초');
			$("#fco").text(""+number_format(parseFloat(infoSummary[i].fco?(infoSummary[i].fco/1000):0).toFixed(2))+" ℓ");
			$("#oilPrice").text(""+number_format(infoSummary[i].oilPrice?infoSummary[i].oilPrice:0)+" 원");
			
			$("#avgFco").text(""+(totalDistance/totalFco).toFixed(2)+" km/L");
			$("#comFco").text(""+(totalFuelEff/count).toFixed(2)+" km/L");totalFuelEff
		}
	}
	
	if(suddenSummary!=null)
		suddenSummaryCall(suddenSummary);


	var tableHtml = "";
	for(var i=0;i<scoreSummary.length;i++){
		tableHtml += "<tr>";
		tableHtml += "<td class='first'>"+(scoreSummary[i].userNm?scoreSummary[i].userNm:"")+"</td>";
		tableHtml += "<td>";
		tableHtml += "<div class='bslist'>";
		tableHtml += "<span>연비 <b>"+calRank(scoreSummary[i].ecoScore?scoreSummary[i].ecoScore:0)+"</b></span>";
		tableHtml += "<span>안전점수 <b>"+calRank(scoreSummary[i].safeScore?scoreSummary[i].safeScore:0)+"</b></span>";
		tableHtml += "<span>에코점수 <b>"+calRank(scoreSummary[i].fuelScore?scoreSummary[i].fuelScore:0)+"</b></span>";
		tableHtml += "</div>";
		tableHtml += "</td>";
		tableHtml += "</tr>";
	}
	
	// getInfoSummaryTotalMonth
	var chart1 = new CanvasJS.Chart("chart1", {
		width:806,
		height:157,
		axisY: {
			labelFormatter:function(e){
				return e.value+"km/L";				
			}
			,labelFontSize: 12
			,labelFontWeight : "lighter"
			,labelFontColor: "black"
			//,interlacedColor: "#F0F8FF" 
			,lineThickness: 0
			,tickLength: 0
			,gridThickness: 1
		},
		axisX: {
			interval: 1
			,labelFormatter:function(e){				
				return (e.value+1)+"월";		
				
			}
			,labelFontSize: 12
			,labelFontWeight : "lighter"
			,labelFontColor: "black"
			,lineThickness: 1
			,tickLength: 5
		},
		
		toolTip : {
			fontStyle : "normal"
			,fontSize : 14
			
			,fontWeight : "normal"
			,fontFamily : "sans-serif"
		},
		data: [{
			type: "line"
			,markerSize: 12
			,lineColor : "#cccdc9"
			,color : "#cccdc9"
			,toolTipContent: "{y}km/L"
			,dataPoints: [
			  { y: month[0] ,color: selFlag[0]},
			  { y: month[1] ,color: selFlag[1]},
			  { y: month[2] ,color: selFlag[2]},
			  { y: month[3] ,color: selFlag[3]},
			  { y: month[4] ,color: selFlag[4]},
			  { y: month[5] ,color: selFlag[5]},
			  { y: month[6] ,color: selFlag[6]},
			  { y: month[7] ,color: selFlag[7]},
			  { y: month[8] ,color: selFlag[8]},
			  { y: month[9] ,color: selFlag[9]},
			  { y: month[10] ,color: selFlag[10]},
			  { y: month[11] ,color: selFlag[11]},
			]
		}]
	});
	chart1.render();
}

function chart4Area(bizTripSummary){
	var biz = new Array(bizTripSummary.length);
	var commute = new Array(bizTripSummary.length);
	var noneBiz = new Array(bizTripSummary.length);
	var jsonBizArray = new Array();
	var jsonCommuteArray = new Array();
	var jsonNoneBizArray = new Array();
	var jsonBizTemp;
	var jsonCommuteTemp;
	var jsonNoneBizTemp;
	var maximum = 0;
	
	for(var i = 0;i < bizTripSummary.length;i++){
		jsonBizTemp = new Object();
		jsonCommuteTemp = new Object();
		jsonNoneBizTemp = new Object();
		biz[i] = bizTripSummary[i].bizCnt1?bizTripSummary[i].bizCnt1:0;
		commute[i] = bizTripSummary[i].bizCnt2?bizTripSummary[i].bizCnt2:0;
		noneBiz[i] = bizTripSummary[i].bizCnt3?bizTripSummary[i].bizCnt3:0;
		if(maximum < biz[i]) maximum = biz[i];
		else if(maximum < commute[i]) maximum = commute[i];
		else if(maximum < noneBiz[i]) maximum = noneBiz[i];
		
		jsonBizTemp.y = biz[i];
		jsonCommuteTemp.y = commute[i];
		jsonNoneBizTemp.y = noneBiz[i];
		if(bizTripSummary[i].selFlag == "1")
			jsonBizTemp.indexLabel = "now";
		jsonBizArray.push(jsonBizTemp);
		jsonCommuteArray.push(jsonCommuteTemp);
		jsonNoneBizArray.push(jsonNoneBizTemp);
	}
	var interval = parseInt(maximum / 5);
	
	var chart4 = new CanvasJS.Chart("chart4", {
		width:806,
		height:200,
		axisY: {
			labelFormatter:function(e){				
				return e.value+"건";				
			}
			,labelFontSize: 12
			,labelFontWeight : "lighter"
			,labelFontColor: "black"
			,lineThickness: 0
			,tickLength: 0
			,gridThickness: 1
			,interval: interval
		},
		axisX: {
			interval: 1
			,labelFormatter:function(e){				
				return (e.value+1)+"월";
				
			}
			,labelFontSize: 12
			,labelFontWeight : "lighter"
			,labelFontColor: "black"
			,lineThickness: 1
			,tickLength: 5
		},
		toolTip : {
			fontStyle : "normal"
			,fontSize : 14
			,fontWeight : "normal"
			,fontFamily : "sans-serif"
		},
		legend:{
			fontStyle : "normal"
			,fontSize : 12
			,fontWeight : "normal"
			,fontFamily : "sans-serif"
	       	,horizontalAlign: "right" // left, center ,right 
	        ,verticalAlign: "top" // top, center, bottom
	      },
	      dataPointWidth: 15,
	      data: [{
				color : "#ffc71c"
				,legendText: "업무용"
				,showInLegend: true
				,toolTipContent: "업무용 {y}건"
				,dataPoints: []
			},{
				color : "#ca58ff"
				,legendText: "출퇴근용"
				,showInLegend: true
				,toolTipContent: "출퇴근용 {y}건"
				,dataPoints: []
			},{
				color : "#ed1c24"
				,legendText: "비업무용"
				,showInLegend: true
				,toolTipContent: "비업무용 {y}건"
				,dataPoints: []
			}]
	});
	
	for(var i = 0;i < jsonBizArray.length;i++){
		chart4.options.data[0].dataPoints.push(jsonBizArray[i]);
		chart4.options.data[1].dataPoints.push(jsonCommuteArray[i]);
		chart4.options.data[2].dataPoints.push(jsonNoneBizArray[i]);
	}
	chart4.render();
}

function chart5Area(bizTripSummary, bizTripSummaryAccount, bizTripSummaryQuater){
	var biz = new Array(bizTripSummary.length);
	var commute = new Array(bizTripSummary.length);
	var noneBiz = new Array(bizTripSummary.length);
	var jsonBizArray = new Array();
	var jsonCommuteArray = new Array();
	var jsonNoneBizArray = new Array();
	var jsonBizTemp;
	var jsonCommuteTemp;
	var jsonNoneBizTemp;
	var maximum = 0;
	
	for(var i = 0;i < bizTripSummary.length;i++){
		jsonBizTemp = new Object();
		jsonCommuteTemp = new Object();
		jsonNoneBizTemp = new Object();
		biz[i] = bizTripSummary[i].bizCnt1?bizTripSummary[i].bizCnt1:0;
		commute[i] = bizTripSummary[i].bizCnt2?bizTripSummary[i].bizCnt2:0;
		noneBiz[i] = bizTripSummary[i].bizCnt3?bizTripSummary[i].bizCnt3:0;
		if(maximum < biz[i]) maximum = biz[i];
		else if(maximum < commute[i]) maximum = commute[i];
		else if(maximum < noneBiz[i]) maximum = noneBiz[i];
		
		jsonBizTemp.y = biz[i];
		jsonCommuteTemp.y = commute[i];
		jsonNoneBizTemp.y = noneBiz[i];
		if(bizTripSummary[i].selFlag == "1")
			jsonBizTemp.indexLabel = "now";
		jsonBizArray.push(jsonBizTemp);
		jsonCommuteArray.push(jsonCommuteTemp);
		jsonNoneBizArray.push(jsonNoneBizTemp);
	}
	var interval = parseInt(maximum / 5);

	var chart5 = new CanvasJS.Chart("chart5", {
		width:806,
		height:200,
		axisY: {
			labelFormatter:function(e){				
				return e.value+"건";				
			}
			,labelFontSize: 12
			,labelFontWeight : "lighter"
			,labelFontColor: "black"
			,lineThickness: 0
			,tickLength: 0
			,gridThickness: 1
			,interval: interval
		},
		axisX: {
			interval: 2
			,labelFormatter:function(e){				
				return (e.value)+"시";		
				
			}
			,labelFontSize: 12
			,labelFontWeight : "lighter"
			,labelFontColor: "black"
			,lineThickness: 1
			,tickLength: 5
		},
		toolTip : {
			fontStyle : "normal"
			,fontSize : 14
			,fontWeight : "normal"
			,fontFamily : "sans-serif"
		},
		legend:{
			fontStyle : "normal"
			,fontSize : 12
			,fontWeight : "normal"
			,fontFamily : "sans-serif"
	       	,horizontalAlign: "right" // left, center ,right 
	        ,verticalAlign: "top" // top, center, bottom
	      },
	      
	      dataPointWidth: 17,
	      data: [{
				type: "stackedColumn"
				,color : "#ffc71c"
				,legendText: "업무용"
				,showInLegend: true 
				,toolTipContent: "업무용 {y}건"
				,dataPoints: []
			},{
				type: "stackedColumn"
				,color : "#ca58ff"
				,legendText: "출퇴근용"
				,showInLegend: true
				,toolTipContent: "출퇴근용 {y}건"
				,dataPoints: []
			},{
				type: "stackedColumn"
				,color : "#ed1c24"			
				,legendText: "비업무용"
				,showInLegend: true
				,toolTipContent: "비업무용 {y}건"
				,dataPoints: []
			}]
	});
	
	for(var i = 0;i < jsonBizArray.length;i++){
		chart5.options.data[0].dataPoints.push(jsonBizArray[i]);
		chart5.options.data[1].dataPoints.push(jsonCommuteArray[i]);
		chart5.options.data[2].dataPoints.push(jsonNoneBizArray[i]);
	}
	chart5.render();
	
	var tableHtml = '';
	var drivingTime = 0;
	var bizTime = 0;
	var bizCnt = [0,0,0];
	for(var i = 0;i < bizTripSummaryAccount.length;i++){
		var vo = bizTripSummaryAccount[i];

		var biz1Distance = vo.biz1Distance?(vo.biz1Distance/1000).toFixed(2):0;
		var biz2Distance = vo.biz2Distance?(vo.biz2Distance/1000).toFixed(2):0;
		var biz3Distance = vo.biz3Distance?(vo.biz3Distance/1000).toFixed(2):0;
		var bizDistance = (parseFloat(biz1Distance)+parseFloat(biz2Distance)+parseFloat(biz3Distance)).toFixed(2);
		
		var biz1Fco = vo.biz1Fco?Math.round(parseInt(vo.biz1Fco)/10)/100:0;
		var biz2Fco = vo.biz2Fco?Math.round(parseInt(vo.biz2Fco)/10)/100:0;
		var biz3Fco = vo.biz3Fco?Math.round(parseInt(vo.biz3Fco)/10)/100:0;
		
		var bizFco = biz1Fco+biz2Fco+biz3Fco;
		var biz1Price = vo.biz1Price?parseInt(vo.biz1Price):0;
		var biz2Price = vo.biz2Price?parseInt(vo.biz2Price):0;
		var biz3Price = vo.biz3Price?parseInt(vo.biz3Price):0;
		var bizPrice = biz1Price+biz2Price+biz3Price;
		drivingTime += vo.drivingTime?parseInt(vo.drivingTime):0;
		bizTime += vo.biz?parseInt(vo.biz):0;
		bizCnt[0] += vo.bizCnt1?vo.bizCnt1:0;
		bizCnt[1] += vo.bizCnt2?vo.bizCnt2:0;
		bizCnt[2] += vo.bizCnt3?vo.bizCnt3:0;
		
		tableHtml += '<tr>';
		tableHtml += '<td rowspan="4" class="row_tit">';
		tableHtml += '<b>'+(vo.userNm?vo.userNm:'')+'</b><br />';
		tableHtml += '(업무운행율 '+((vo.biz?vo.biz:0)/(vo.drivingTime?vo.drivingTime:1)*100).toFixed(1)+'%)';
		tableHtml += '</td>';
		tableHtml += '<td class="first bg_f7">';
		tableHtml += '<b>전체 합계</b>';
		tableHtml += '</td>';
		tableHtml += '<td class="bg_f7">';
		tableHtml += '<div class="bslist">';
		tableHtml += '<span class="wid30"><b>'+number_format(bizDistance)+' km</b></span>';
		tableHtml += '<span class="wid30"><b>'+number_format(bizFco)+' ℓ</b></span>';
		tableHtml += '<span class="wid30"><b>'+number_format(bizPrice)+' 원</b></span>';
		tableHtml += '</div>';
		tableHtml += '</td>';
		tableHtml += '</tr>';
		tableHtml += '<tr>';
		tableHtml += '<td class="first">';
		tableHtml += '업무용';	
		tableHtml += '</td>';
		tableHtml += '<td>';
		tableHtml += '<div class="bslist">';
		tableHtml += '<span class="wid30"><b>'+number_format(biz1Distance)+' km</b></span>';
		tableHtml += '<span class="wid30"><b>'+number_format(biz1Fco)+' ℓ</b></span>';
		tableHtml += '<span class="wid30"><b>'+number_format(biz1Price)+' 원</b></span>';
		tableHtml += '</div>';
		tableHtml += '</td>';
		tableHtml += '</tr>';
		tableHtml += '<tr>';
		tableHtml += '<td class="first">';
		tableHtml += '출퇴근용';
		tableHtml += '</td>';
		tableHtml += '<td>';
		tableHtml += '<div class="bslist">';
		tableHtml += '<span class="wid30"><b>'+number_format(biz2Distance)+' km</b></span>';
		tableHtml += '<span class="wid30"><b>'+number_format(biz2Fco)+' ℓ</b></span>';
		tableHtml += '<span class="wid30"><b>'+number_format(biz2Price)+' 원</b></span>';
		tableHtml += '</div>';
		tableHtml += '</td>';
		tableHtml += '</tr>';
		tableHtml += '<tr>';
		tableHtml += '<td class="first">';
		tableHtml += '비업무용';
		tableHtml += '</td>';
		tableHtml += '<td>';
		tableHtml += '<div class="bslist">';
		tableHtml += '<span class="wid30"><b>'+number_format(biz3Distance)+' km</b></span>';
		tableHtml += '<span class="wid30"><b>'+number_format(biz3Fco)+' ℓ</b></span>';
		tableHtml += '<span class="wid30"><b>'+number_format(biz3Price)+' 원</b></span>';
		tableHtml += '</div>';
		tableHtml += '</td>';
		tableHtml += '</tr>';
	}
	$(".accountTripSummaryTable tbody").empty();
	$(".accountTripSummaryTable tbody").append(tableHtml);
	
	var rate = bizTime / drivingTime * 100;
	var quaterRate = bizTripSummaryQuater.biz / bizTripSummaryQuater.drivingTime * 100;
	$(".useWorkRate").text(""+rate.toFixed(1)+"%");
	$(".quater").text(""+bizTripSummaryQuater.quarter+"분기");
	$(".corpTax").text(""+((drivingTime / bizTripSummaryQuater.drivingTime * 100) * (rate / 100)).toFixed(1)+"%");
	
	$(".bizCnt1").text(""+bizCnt[0]+"회");
	$(".bizCnt2").text(""+bizCnt[1]+"회");
	$(".bizCnt3").text(""+bizCnt[2]+"회");
	$(".bizRate").text(""+rate.toFixed(1)+"%");
}

function chart6Area(expenseSummary, expenseSummaryPrev, expenseSummaryAccount, expenseSummaryAccountPrev){
	var price = new Array(expenseSummary.length);
	var jsonPriceArray = new Array();
	var priceTemp;
	var maximum = 0;
	var currentPrice=[0,0,0,0,0,0,0,0];
	var totalExpense = 0;
	
	for(var i = 0;i < expenseSummary.length;i++){
		priceTemp = new Object();
		price[i] = expenseSummary[i].priceAll?expenseSummary[i].priceAll:null;
		if(maximum < price[i]) maximum = price[i];
		
		priceTemp.y = price[i];
		if(expenseSummary[i].selFlag == "1"){
			priceTemp.color = "red";
			currentPrice[0] = expenseSummary[i].price1?expenseSummary[i].price1:0;	// 주유
			currentPrice[1] = expenseSummary[i].price2?expenseSummary[i].price2:0;	// 정비
			currentPrice[2] = expenseSummary[i].price3?expenseSummary[i].price3:0;	// 주차
			currentPrice[3] = expenseSummary[i].price4?expenseSummary[i].price4:0;	// 통행료
			currentPrice[4] = expenseSummary[i].price5?expenseSummary[i].price5:0;	// 렌탈료
			currentPrice[5] = expenseSummary[i].price6?expenseSummary[i].price6:0;	// 세차
			currentPrice[6] = expenseSummary[i].price7?expenseSummary[i].price7:0;	// 과태료
			currentPrice[7] = expenseSummary[i].price8?expenseSummary[i].price8:0;	// 기타
			totalExpense = expenseSummary[i].priceAll?expenseSummary[i].priceAll:0;
		}
		jsonPriceArray.push(priceTemp);
	}
	
	var chart6 = new CanvasJS.Chart("chart6", {
		width:806,
		height:200,
		axisY: {
			labelFormatter:function(e){
				return CanvasJS.formatNumber(e.value/10000, "#,##0")+"만" 
			}
			,labelFontSize: 12
			,labelFontWeight : "lighter"
			,labelFontColor: "black"
			//,interlacedColor: "#F0F8FF" 
			,lineThickness: 0
			,tickLength: 0
			,gridThickness: 1
		},
		axisX: {
			interval: 1
			,labelFormatter:function(e){				
				return (e.value+1)+"월";		
				
			}
			,labelFontSize: 12
			,labelFontWeight : "lighter"
			,labelFontColor: "black"
			,lineThickness: 1
			,tickLength: 5
		},
		
		toolTip : {
			fontStyle : "normal"
			,fontSize : 14
			,fontWeight : "normal"
			,fontFamily : "sans-serif"
			,contentFormatter: function(e){
		          var val = e.entries[0].dataPoint.y;
		          var str = CanvasJS.formatNumber(val, "#,###")+"원";
		          return str; 
		    }

		},
		data: [{
			type: "line"
			,markerSize: 12
			,lineColor : "#cccdc9"
			,color : "#cccdc9"
			,dataPoints: []
		}]
	});
	
	for(var i = 0;i < jsonPriceArray.length;i++){
		chart6.options.data[0].dataPoints.push(jsonPriceArray[i]);
	}
	chart6.render();

	
	var priceHtml = '';
	var priceAll = expenseSummaryPrev?expenseSummaryPrev.priceAll:0;
	var price1 = expenseSummaryPrev?expenseSummaryPrev.price1:0;
	var price2 = expenseSummaryPrev?expenseSummaryPrev.price2:0;
	var price3 = expenseSummaryPrev?expenseSummaryPrev.price3:0;
	var price4 = expenseSummaryPrev?expenseSummaryPrev.price4:0;
	var price5 = expenseSummaryPrev?expenseSummaryPrev.price5:0;
	var price6 = expenseSummaryPrev?expenseSummaryPrev.price6:0;
	var price7 = expenseSummaryPrev?expenseSummaryPrev.price7:0;
	var price8 = expenseSummaryPrev?expenseSummaryPrev.price8:0;
	
	priceHtml = ''+number_format(currentPrice[0])+'원';
	if(currentPrice[0] > price1){
		priceHtml += '<span>('+number_format(currentPrice[0] - price1)+'원 <i class="icon up"></i> )</span>';
	}
	else if(currentPrice[0] < price1){
		priceHtml += '<span>('+number_format(price1 - currentPrice[0])+'원 <i class="icon down"></i> )</span>';
	}
	else{
		priceHtml += '<span>(0원 <i class="icon none">-</i> )</span>';
	}
	$(".refuel").html(priceHtml);
	
	priceHtml = ''+number_format(currentPrice[1])+'원';
	if(currentPrice[1] > price2){
		priceHtml += '<span>('+number_format(currentPrice[1] - price2)+'원 <i class="icon up"></i> )</span>';
	}
	else if(currentPrice[1] < price2){
		priceHtml += '<span>('+number_format(price2 - currentPrice[1])+'원 <i class="icon down"></i> )</span>';
	}
	else{
		priceHtml += '<span>(0원 <i class="icon none">-</i> )</span>';
	}
	$(".clinic").html(priceHtml);
	
	priceHtml = ''+number_format(currentPrice[2])+'원';
	if(currentPrice[2] > price3){
		priceHtml += '<span>('+number_format(currentPrice[2] - price3)+'원 <i class="icon up"></i> )</span>';
	}
	else if(currentPrice[2] < price3){
		priceHtml += '<span>('+number_format(price3 - currentPrice[2])+'원 <i class="icon down"></i> )</span>';
	}
	else{
		priceHtml += '<span>(0원 <i class="icon none">-</i> )</span>';
	}
	$(".park").html(priceHtml);
	
	priceHtml = ''+number_format(currentPrice[3])+'원';
	if(currentPrice[3] > price4){
		priceHtml += '<span>('+number_format(currentPrice[3] - price4)+'원 <i class="icon up"></i> )</span>';
	}
	else if(currentPrice[3] < price4){
		priceHtml += '<span>('+number_format(price4 - currentPrice[3])+'원 <i class="icon down"></i> )</span>';
	}
	else{
		priceHtml += '<span>(0원 <i class="icon none">-</i> )</span>';
	}
	$(".toll").html(priceHtml);
	
	priceHtml = ''+number_format(currentPrice[4])+'원';
	if(currentPrice[4] > price5){
		priceHtml += '<span>('+number_format(currentPrice[4] - price5)+'원 <i class="icon up"></i> )</span>';
	}
	else if(currentPrice[4] < price5){
		priceHtml += '<span>('+number_format(price5 - currentPrice[4])+'원 <i class="icon down"></i> )</span>';
	}
	else{
		priceHtml += '<span>(0원 <i class="icon none">-</i> )</span>';
	}
	$(".rental").html(priceHtml);
	
	priceHtml = ''+number_format(currentPrice[5])+'원';
	if(currentPrice[5] > price6){
		priceHtml += '<span>('+number_format(currentPrice[5] - price6)+'원 <i class="icon up"></i> )</span>';
	}
	else if(currentPrice[5] < price6){
		priceHtml += '<span>('+number_format(price6 - currentPrice[5])+'원 <i class="icon down"></i> )</span>';
	}
	else{
		priceHtml += '<span>(0원 <i class="icon none">-</i> )</span>';
	}
	$(".wash").html(priceHtml);
	
	priceHtml = ''+number_format(currentPrice[6])+'원';
	if(currentPrice[6] > price7){
		priceHtml += '<span>('+number_format(currentPrice[6] - price7)+'원 <i class="icon up"></i> )</span>';
	}
	else if(currentPrice[6] < price7){
		priceHtml += '<span>('+number_format(price7 - currentPrice[6])+'원 <i class="icon down"></i> )</span>';
	}
	else{
		priceHtml += '<span>(0원 <i class="icon none">-</i> )</span>';
	}
	$(".penalty").html(priceHtml);
	
	priceHtml = ''+number_format(currentPrice[7])+'원';
	if(currentPrice[7] > price8){
		priceHtml += '<span>('+number_format(currentPrice[7] - price8)+'원 <i class="icon up"></i> )</span>';
	}
	else if(currentPrice[7] < price8){
		priceHtml += '<span>('+number_format(price8 - currentPrice[7])+'원 <i class="icon down"></i> )</span>';
	}
	else{
		priceHtml += '<span>(0원 <i class="icon none">-</i> )</span>';
	}
	$(".etc").html(priceHtml);

	var totalHtml = '';
	for(var i = 0;i < expenseSummaryAccount.length;i++){
		var vo = expenseSummaryAccount[i];
		var voPrev = expenseSummaryAccountPrev[i];
		var currentAccountPrice = vo.priceAll?vo.priceAll:0;
		var prevAccountPrice = voPrev.priceAll?voPrev.priceAll:0;
		totalHtml += '<tr>';
		totalHtml += '<td class="row_tit">'+(vo.userNm?vo.userNm:'')+'</td>';
		totalHtml += '<td>';
		totalHtml += ''+number_format(currentAccountPrice)+'원 ';
		if(currentAccountPrice > prevAccountPrice)
			totalHtml += '<span>('+number_format(currentAccountPrice - prevAccountPrice)+'원 <i class="icon up"></i> )</span>';
		else if(currentAccountPrice < prevAccountPrice)
			totalHtml += '<span>('+number_format(prevAccountPrice - currentAccountPrice)+'원 <i class="icon down"></i> )</span>';
		else totalHtml += '<span>(0원 <i class="icon none">-</i> )</span>';
		totalHtml += '</td>';
		totalHtml += '</tr>';
	}
	
	$(".expenseTotalTable tbody").empty();
	$(".expenseTotalTable tbody").append(totalHtml);

	$(".totalExpense").text(""+number_format(totalExpense)+"원");
	if(totalExpense > priceAll)
		$(".compareExpenseRate").text("대비 "+((totalExpense - priceAll) / totalExpense * 100).toFixed(1)+"%("+number_format(totalExpense - priceAll)+"원) 상승");
	else if(totalExpense < priceAll)
		$(".compareExpenseRate").text("대비 "+((totalExpense - priceAll) / totalExpense * 100).toFixed(1)+"%("+number_format(totalExpense - priceAll)+"원) 감소");
	else $(".compareExpenseRate").text("대비 0.0%(0원) 유지");
	
}
</script>
<form id="searchFrm" style="display:none">
	<input type="hidden" name="startDate" value="" />
	<input type="hidden" name="searchLocation" value="" />
	<input type="hidden" name="searchLocationDetail" value="" />
	<input type="hidden" name="searchText" value="" />
	<input type="hidden" name="searchOrder" value="" />
</form>
<div class="right_layout">
	<div class="auth user contents">
		<!-- 운행정보 -->
		<h3 class="tit01 mgt30">평균 운행정보<i></i></h3>
		<div class="tbl_layout mgt20">
			<div class="row">
				<div class="col" style="width:310px;">
					<div id="myChart01" class="myChart">
						<div class="level">A <i class="icon up"></i> </div>
					</div>
					<div class="chart_explain">
						<table class="tbl tbl_explain">
							<colgroup>
								<col style="width:95px;"/>
								<col />
							</colgroup>
							<tr>
								<td><span class="color purple"></span>연비</td>
								<td><span id="fuelRank"></span></td>
							</tr>
							<tr>
								<td><span class="color pink"></span>안전점수</td>
								<td><span id="safeRank"></span></td>
							</tr>
							<tr>
								<td><span class="color blue"></span>에코점수</td>
								<td><span id="ecoRank"></span></td>
							</tr>
						</table>
					</div>
				</div>
				<div class="col" style="width:430px; float:right;">
					<table class="tbl tbl_type02">
						<colgroup>
							<col style="width:125px;" />
							<col />
						</colgroup>
						<tr>
							<th scope="row">거리</th>
							<td><span id="distance"></span></td>
						</tr>
						<tr>
							<th scope="row">시간</th>
							<td><span id="drivingTime"></span></td>
						</tr>
						<tr>
							<th scope="row">연료 소모량</th>
							<td><span id="fco"></span></td>
						</tr>
						<tr>
							<th scope="row">주유비</th>
							<td><span id="oilPrice"></span><br/>(전국평균유가 기준)</td>
						</tr>
					</table>
				</div>
			</div>
			<!-- 806x157 -->
			<div id="chart1" style="width:806px;height: 157px;"></div>
			<div class="graph_txt1">
				<span>
					<strong>ㆍ평균연비</strong>
					<em id="avgFco">0 km/L</em>
				</span>
				<span>
					<strong>ㆍ복합연비</strong>
					<em id="comFco">0 km/L</em>
				</span>
			</div>
			<div class="map_txt1">
				<div>
					<span class="w109">
						<img src="${pathCommon}/images/m_img1.png" alt="">급출발 <strong class="rapidStart">0회</strong>
					</span><span class="w120">
						<img src="${pathCommon}/images/m_img2.png" alt="">급정지 <strong class="rapidStop">0회</strong>
					</span>
					<span class="w120">
						<img src="${pathCommon}/images/m_img4.png" alt="">급가속 <strong class="rapidAccel">0회</strong>
					</span>
					<span class="w109">
						<img src="${pathCommon}/images/m_img5.png" alt="">급감속 <strong class="rapidDeaccel">0회</strong>
					</span>
					<span class="w120">
						<img src="${pathCommon}/images/m_img10.png" alt="">급회전 <strong class="rapidTurn">0회</strong>
					</span>
				</div>
				<div>
					<span class="w109">
						<img src="${pathCommon}/images/m_img11.png" alt="">급유턴 <strong class="rapidUtern">0회</strong>
					</span>
					<span class="w109">
						<img src="${pathCommon}/images/m_img6.png" alt="">과속 <strong class="overSpeed">0회</strong>
					</span>
					<span class="w120">
						<img src="${pathCommon}/images/m_img7.png" alt="">장기과속 <strong class="longTerm">0회</strong>
					</span>
					<%-- <span class="w178">
						<img src="${pathCommon}/images/m_img12.png" alt="">심야운전 <strong class="nightDrive">0회</strong>
					</span> --%>
				</div>
				<div>
					<span class="w178">
						<img src="${pathCommon}/images/m_img9.png" alt="">사고다발지역통과 <strong class="no_num">0회</strong>
					</span>
				</div>
			</div>
		</div>
		<!-- 운행정보 끝 -->
	
		
		<div class="mode business">
			<!-- 사용 목적 [기간별] -->
			<div class="gray_bg_box mgt80">
				<span>업무용 <em class="bizCnt1">xxx회</em></span>
				<span>출퇴근용 <em class="bizCnt2">xx회</em></span>
				<span>비업무용 <em class="bizCnt3">xx회</em></span>
				<span>업무운행율 <em class="bizRate">xx%</em></span>
			</div>
			<div class="bxtit mgt30">
				<h3 class="tit01">사용 목적 [기간별]<i></i></h3>
				<%-- <ul class="bx_icons">
					<li>
						<img src="${pathCommon}/images/main_icon9.jpg" alt="" />
						업무용
					</li>
					<li>
						<img src="${pathCommon}/images/main_icon8.jpg" alt="" />
						출퇴근용
					</li>
					<li>
						<img src="${pathCommon}/images/main_icon7.jpg" alt="" />
						비업무용
					</li>
				</ul> --%>
			</div>
			<div id="chart4" style="width:806px;height:200px;"></div>
			<!-- 사용 목적 [기간별] 끝 -->
			
			<!-- 사용 목적 [시간대별] -->
			<div class="bxtit mgt30">
				<h3 class="tit01">사용 목적 [시간대별]<i></i></h3>
				<%-- <ul class="bx_icons">
					<li>
						<img src="${pathCommon}/images/main_icon9.jpg" alt="" />
						업무용
					</li>
					<li>
						<img src="${pathCommon}/images/main_icon8.jpg" alt="" />
						출퇴근용
					</li>
					<li>
						<img src="${pathCommon}/images/main_icon7.jpg" alt="" />
						비업무용
					</li>
				</ul> --%>
			</div>
			<div id="chart5" style="width:806px;height:200px;"></div>
			<table class="table1 bs_table mgt20 accountTripSummaryTable">
				<colgroup>
					<col style="width:177px;" />
					<col style="width:177px;" />
					<col  />
				</colgroup>
				<thead>
					<tr>
						<th>사용자</th>
						<th>항목</th>
						<th>내용</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
			<ul class="bn_p">
				<li>ㆍ전체 업무사용(업무용+출퇴근용)비율은 <em class="color_red useWorkRate">xx%</em>이며 <em class="quater">x분기</em> 법인세 신고시에 약 <em class="color_red corpTax">xx%</em>가 될 것으로 예상됩니다.</li>
			</ul>
			<!-- 사용 목적 [시간대별] 끝 -->
	
			<!-- 평균 운영비 -->
			<div class="gray_bg_box mgt80">
				<span>총 운영비 <em class="totalExpense">xxx,xxx,000원</em></span>
			</div>
			<h3 class="tit01 mgt30">평균 운영비<i></i></h3>
			<div id="chart6" style="width:806px;height:200px;"></div>
			
			<div class="tbl_layout">
				<div class="row">
					<div class="col col-2">
						<table class="table1 table1_2 tbl_count" style="width:393px; float:left;">
							<colgroup>
								<col style="width:120px;"/>
								<col />
							</colgroup>
							<thead>
								<tr>
									<th scope="col">항목</th>
									<th scope="col">총 비용(전월대비)</th>
								</tr>
							</thead>
							<tr>
								<td class="bg_bar">주유</td>
								<td class="refuel">
									0원
									<span>(0원 <i class="icon none">-</i> )</span>
								</td>
							</tr>
							<tr>
								<td class="bg_bar">주차</td>
								<td class="park">
									0원
									<span>(0원 <i class="icon none">-</i> )</span>
								</td>
							</tr>
							<tr>
								<td class="bg_bar">통행료</td>
								<td class="toll">
									0원
									<span>(0원 <i class="icon none">-</i> )</span>
								</td>
							</tr>
							<tr>
								<td class="bg_bar">렌탈료</td>
								<td class="rental">
									0원
									<span>(0원 <i class="icon none">-</i> )</span>
								</td>
							</tr>
						</table>
					</div>
					<div class="col col-2">
						<table class="table1 table1_2 tbl_count" style="width:393px; float:right;">
							<colgroup>
								<col style="width:120px;"/>
								<col />
							</colgroup>
							<thead>
								<tr>
									<th scope="col">항목</th>
									<th scope="col">총 비용(전월대비)</th>
								</tr>
							</thead>
							<tr>
								<td class="bg_bar">세차</td>
								<td class="wash">
									0원
									<span>(0원 <i class="icon none">-</i> )</span>
								</td>
							</tr>
							<tr>
								<td class="bg_bar">정비</td>
								<td class="clinic">
									0원
									<span>(0원 <i class="icon none">-</i> )</span>
								</td>
							</tr>
							<tr>
								<td class="bg_bar">과태료</td>
								<td class="penalty">
									0원
									<span>(0원 <i class="icon none">-</i> )</span>
								</td>
							</tr>
							<tr>
								<td class="bg_bar">기타</td>
								<td class="etc">
									0원
									<span>(0원 <i class="icon none">-</i> )</span>
								</td>
							</tr>
						</table>
						
					</div>
					
				</div>
				<table class="table1 tbl_count bs_table mgt20 expenseTotalTable">
					<colgroup>
						<col style="width:177px;" />
						<col  />
					</colgroup>
					<thead>
						<tr>
							<th>사용자</th>
							<th>총 합계</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				<ul class="bn_p">
					<li>ㆍ이번달 전체 운영비는 <em class="color_red totalExpense">xxx,000원</em>이며, 전월 <em class="color_red compareExpenseRate">대비 xx%(xxx,000원) 상승</em>하였습니다.</li>
				</ul>
			</div>
			<!-- 평균 운영비 끝 -->
		</div> 
		<%--
		<div class="mode general" style="display:none">
			<!-- 운행 수 [시간대별] -->
			<div class="gray_bg_box mgt80">
				<span>총 운행 <em>xx회</em></span>
			</div>
			<h3 class="tit01 mgt30">운행 수 [시간대별]<i></i></h3>
			<div class="tbl_layout">
			</div>
			<!-- 운행 수 [시간대별] 끝 -->
			<!-- 평균 지출비 -->
			<div class="gray_bg_box mgt80">
				<span>총 지출비 <em>xxx,xxx원</em></span>
			</div>
			<h3 class="tit01 mgt30">평균 지출비<i></i></h3>
			<div class="tbl_layout">
				<div class="row mgt20">
					<div class="col col-2">
						<table class="table1 table1_2 tbl_count" style="width:393px; float:left;">
							<colgroup>
								<col style="width:120px;"/>
								<col />
							</colgroup>
							<thead>
								<tr>
									<th scope="col">항목</th>
									<th scope="col">총 비용(전월대비)</th>
								</tr>
							</thead>
							<tr>
								<td class="bg_bar">주유</td>
								<td>
									xx,000원
									<span>(xxx,000원 <i class="icon down"></i> )</span>
								</td>
							</tr>
							<tr>
								<td class="bg_bar">주차</td>
								<td>
									xx,000원
									<span>(xxx,000원 <i class="icon up"></i> )</span>
								</td>
							</tr>
							<tr>
								<td class="bg_bar">통행료</td>
								<td>
									xx,000원
									<span>(xxx,000원 <i class="icon none">-</i> )</span>
								</td>
							</tr>
							<tr>
								<td class="bg_bar">렌탈료</td>
								<td>
									xx,000원
									<span>(xxx,000원 <i class="icon up"></i> )</span>
								</td>
							</tr>
						</table>
					</div>
					<div class="col col-2">
						<table class="table1 table1_2 tbl_count" style="width:393px; float:right;">
							<colgroup>
								<col style="width:120px;"/>
								<col />
							</colgroup>
							<thead>
								<tr>
									<th scope="col">항목</th>
									<th scope="col">총 비용(전월대비)</th>
								</tr>
							</thead>
							<tr>
								<td class="bg_bar">세차</td>
								<td>
									xx,000원
									<span>(xxx,000원 <i class="icon up"></i> )</span>
								</td>
							</tr>
							<tr>
								<td class="bg_bar">정비</td>
								<td>
									xx,000원
									<span>(xxx,000원 <i class="icon up"></i> )</span>
								</td>
							</tr>
							<tr>
								<td class="bg_bar">과태료</td>
								<td>
									xx,000원
									<span>(xxx,000원 <i class="icon up"></i> )</span>
								</td>
							</tr>
							<tr>
								<td class="bg_bar">기타</td>
								<td>
									xx,000원
									<span>(xxx,000원 <i class="icon up"></i> )</span>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<ul class="bn_p">
				<li>ㆍ이번달 전체 운영비는 <em class="color_red">xxx,000원</em>이며, 전월 대비 <em class="color_red">xx%(xxx,000원) 상승</em>하였습니다.</li>
			</ul>
			</div>
			<!-- 평균 지출비 끝 -->
			<!-- 차량 소모품 -->
			<div class="gray_bg_box mgt80">
				<span>총 지출비 <em>xx,xx원</em></span>
			</div>
			<h3 class="tit01 mgt30">차량 소모품<i></i></h3>
			<div class="bn_box mgt20">
				<span class="bn_left">
					<strong class="bg_green"></strong>
					<em class="color_green">여유</em>
				</span>
				<span class="bn_left">
					<strong class="bg_red"></strong>
					<em class="color_red">교환필요</em>
				</span>
				<p class="bn_right">(교체주기 30일 미만인 경우)</p>
			</div>
			<!-- 차량 소모품 끝 -->
			<!-- 차량진단 항목 -->
			<div class="gray_bg_box mgt80">
				<span>현재 점검필요 <em>x건</em></span>
				<span>긴급 <em>x건</em></span>
			</div>
			<h3 class="tit01 mgt30">차량진단 항목<i></i></h3>
			<table class="table1 bs_table">
				<colgroup>
					<col style="width:127px;" />
					<col style="width:230px;" />
					<col  />
				</colgroup>
				<thead>
					<tr>
						<th>점검 일시</th>
						<th colspan="2">상세 내용</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="row_tit" rowspan="4"><b>20xx-xx-xx<br />xx:00</b></td>
						<td class="bg_f7" colspan="2"><b>고장코드 소거 <em class="color_red">x</em></b></td>
					</tr>
					<tr>
						<td class="first td_align_left">
							<span class="red_icon">점검필요</span>
							<b>[P2763] 밋션제어</b>
						</td>
						<td class="td_align_left">댐퍼 클러치 솔레노이드 밸브(SLU) 이상 - 신호값 낮음(GND 쇼트 or 단선)</td>
					</tr>
					<tr>
						<td class="first td_align_left">
							<span class="red_icon">긴급</span>
							<b>[P2763] 밋션제어</b>
						</td>
						<td class="td_align_left">댐퍼 클러치 솔레노이드 밸브(SLU) 이상 - 신호값 낮음(GND 쇼트 or 단선)</td>
					</tr>
					<tr>
						<td class="first td_align_left">
							<span class="red_icon">점검필요</span>
							<b>[P2763] 밋션제어</b>
						</td>
						<td class="td_align_left">댐퍼 클러치 솔레노이드 밸브(SLU) 이상 - 신호값 낮음(GND 쇼트 or 단선)</td>
					</tr>
					
					<tr>
						<td class="row_tit" rowspan="4"><b>20xx-xx-xx<br />xx:00</b></td>
						<td class="bg_f7" colspan="2"><b>점검필요 <em class="color_red">x</em> / 긴급 <em class="color_red">x</em></b></td>
					</tr>
					<tr>
						<td class="first td_align_left">
							<span class="red_icon">점검필요</span>
							<b>[P2763] 밋션제어</b>
						</td>
						<td class="td_align_left">댐퍼 클러치 솔레노이드 밸브(SLU) 이상 - 신호값 낮음(GND 쇼트 or 단선)</td>
					</tr>
					<tr>
						<td class="first td_align_left">
							<span class="red_icon">긴급</span>
							<b>[P2763] 밋션제어</b>
						</td>
						<td class="td_align_left">댐퍼 클러치 솔레노이드 밸브(SLU) 이상 - 신호값 낮음(GND 쇼트 or 단선)</td>
					</tr>
					<tr>
						<td class="first td_align_left">
							<span class="red_icon">점검필요</span>
							<b>[P2763] 밋션제어</b>
						</td>
						<td class="td_align_left">댐퍼 클러치 솔레노이드 밸브(SLU) 이상 - 신호값 낮음(GND 쇼트 or 단선)</td>
					</tr>
					<tr>
						<td class="row_tit" rowspan="4"><b>20xx-xx-xx xx:00</b></td>
						<td class="bg_f7" colspan="2"><b>점검필요 <em class="color_82">x</em> / 긴급 <em class="color_82">x</em></b></td>
					</tr>
				</tbody>
			</table>
			<!-- 차량진단 항목 끝 -->
		</div> 
		--%>
	</div>
</div>