<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script type="text/javascript">
var lazy;
var lazyPop;
var lazyVehicle;
$(document).ready(function(){
	
	initMenuSel("M4003");
	
	$("body").on("click","#btn_ajaxPop,.btn_ajaxPop",function(){
		$(".black").fadeIn();
		$(".vehicle").fadeIn();
		$.lazyLoader.search(lazyVehicle);
	});
	
	$("body").on("keyup","#searchTextVehicle",function(e){
		if(e.keyCode == 13) $("#btn_searchVehicle").trigger("click");
	});
	
	$("body").on("click","#btn_searchVehicle",function(){
		$.lazyLoader.search(lazyVehicle);
	});
	
	$("body").on("click",".btn_ok",function(){
		$("input[name=rdo_sel]").each(function(){
			if($(this).is(":checked")){
				var name = $(this).data("name");
				var key = $(this).data("key");
				$("#searchVehicle").val(name);
				$("#vehicleKey").val(key);
				return false;
			}
		})
		$(".pop7").fadeOut();
		$(".black").fadeOut();
	});
	
	$("body").on("click",".btn_vehicleDetail",function(){
		var vehicleKey = $(this).data("key");
		$VDAS.instance_post("/drivingMng/searchVehicleDetail.do",{vehicleKey:vehicleKey});
	});
	
	if(!lazyVehicle){
		lazyVehicle = $("#contentsTableVehicle").lazyLoader({
			searchFrmObj : $("#vehicleFrm")
			,searchFrmVal : {
				searchText : $("#searchTextVehicle")
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/com/getVehicleList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var strHtml = '';
				var data = r.rtvList;
				
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += '';
					strHtml += '<tr>';
					strHtml += '<td>';
					strHtml += '<input type="radio" name="rdo_sel" class="rdo_sel" data-name="'+vo.plateNum+'" data-key="'+vo.vehicleKey+'">';
					strHtml += '</td>';
					strHtml += '<td>';
					strHtml += '<p>'+vo.manufacture+'</p>';
					strHtml += '</td>';
					strHtml += '<th class="car_list">';
					strHtml += '<strong class="car_img"><img src="${pathCommon}/images/vehicle/'+vo.imgS+'.png" alt=""></strong>';
					strHtml += '<a href="#none" class="btn_vehicleDetail" data-key="'+vo.vehicleKey+'">';
					strHtml += '<span>'+vo.modelMaster+'</span>'+vo.plateNum+'</a>';
					strHtml += '</th>';
					strHtml += '<td>';
					strHtml += '<p>'+vo.volume+' cc</p>';
					strHtml += '</td>';
					strHtml += '<td>';
					if(vo.vehicleBizType == '1')
						strHtml += '<p>소유</p>';
					else if(vo.vehicleBizType == '2')
						strHtml += '<p>리스</p>';
					else if(vo.vehicleBizType == '3')
						strHtml += '<p>렌터</p>';
					else if(vo.vehicleBizType == '4')
						strHtml += '<p>개인</p>';
					strHtml += '</td>';
					strHtml += '<th>';
					strHtml += '<p>'+vo.year+'</p>';
					strHtml += '<p>'+number_format((vo.totDistance/1000).toFixed(1))+' km</p>';
					strHtml += '</th>';
					strHtml += '<th>';
					strHtml += '<p></p>';
					strHtml += '<p>'+vo.fuelType+'</p>';
					strHtml += '</th></tr>';
				}
				
				return strHtml;
			}
		});
	}
	
	$(".btn_search").on("click",function(){
		$.lazyLoader.search(lazy);
		$VDAS.http_post("/report/getVehicleListSummary.do"
				,{startDate : $("#startDate").val()
					,endDate : $("#endDate").val()
					,vehicleKey : $("#vehicleKey").val()
				}
			,{success : function(r){
				var d = r.rtv[0];		
				var cnt = number_format(d.cnt)+"대";
				var rto = d.ratio+"%";
			
				$("#sumCnt").html(cnt);
				$("#sumRto").html(rto);
			}	
		});
	});

	if(!lazy)
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
				startDate : $("#startDate")
				,endDate : $("#endDate")
				,vehicleKey : $("#vehicleKey")
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/report/getVehicleList.do"
			,initSearch : false
			,createDataHtml : function(r){
				var data = r.rtvList;
				var strHtml = "";
				
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					
					strHtml += '<tr>';
					strHtml += '<td><input type="checkbox" class="selObj" data-key="'+vo.vehicleKey+'"/></td>';
					strHtml += '<th class="car_list">';
					strHtml += '<strong class="car_img"><img src="${path}/common/images/vehicle/'+vo.imgS+'.png" alt="" /></strong>';
					strHtml += '<a href="#none" class="btn_vehicleDetail" data-key="'+vo.vehicleKey+'">';
					strHtml += '<span>'+vo.modelMaster+'</span>';
					strHtml += vo.plateNum;
					strHtml += '</a>';
					strHtml += '</th>';
					strHtml += '<td>'+number_format(Math.round(vo.distance/1000))+' km</td>';
					strHtml += '<td>'+number_format(Math.round(vo.biz/1000))+' km</td>';
					strHtml += '<td>'+number_format(Math.round(vo.noneBiz/1000))+' km</td>';
					strHtml += '<td>'+((vo.distance==0)?0:(Math.round(vo.biz/vo.distance*100,2)))+'%</td>';
					
					strHtml += '</tr>';					
					
				}
				
				return strHtml;
				
			}
		});

	
	
	
	$("body").on("click",".btn_down",function(){
		var target = $(this).data("target");
		var strKeys = "";
		$(".selObj:checked").each(function(){
			var key = $(this).data("key");
			if(strKeys.length == 0) strKeys = key;
			else strKeys += ","+key;
		});
		
		if(strKeys.length == 0) alert("차량을 선택해주세요.");
		else if($("#startDate").val().length==0||$("#endDate").val().length==0) alert("조회 기간을 선택해주세요.");
		else{
			
			$.ajax({
				url:"/vdasPro/report/exportReport.do"
				,dataType : 'json'
				,contentType: "application/x-www-form-urlencoded; charset=UTF-8"
				,type:"POST"
				,data:{startDate : $("#startDate").val()
						,endDate : $("#endDate").val()
						,vehicleKeys : strKeys
						,target : target
				}
				,cache:!1
				,success:function(t){
					var key = t.rtv;
					$VDAS.instance_post("/com/getUserImg.do",{uuid:key});
				}		
				,error:function(t){
					$(".contents").children().not(".noneHide").show();
					$(".white").fadeOut();
				}
				,beforeSend: function() {
					$(".contents").children().not(".noneHide").hide();
					$(".white").fadeIn();
				}
			}).done(function(){
				$(".contents").children().not(".noneHide").show();
				$(".white").fadeOut();
			});
		}
		
			
	});
	
	$("#allChecked").on("click",function(){
		if($(this).is(":checked"))
			$("#contentsTable tbody input[type=checkbox]").prop("checked",true);
		else $("#contentsTable tbody input[type=checkbox]").prop("checked",false);
	});
});

</script>
<form id="searchFrm">
	<input type="hidden" name="startDate" value="" />
	<input type="hidden" name="endDate" value="" />
	<input type="hidden" name="vehicleKey" value="" />
</form>
<form id="vehicleFrm" style="display:none">
	<input type="hidden" name="searchText" value="" />
	<input type="hidden" name="searchType" value="plateNum" />
</form>
<div class="right_layout">
	<h2 class="tit_sub">
		국세청 운행일지
		<p>별도의 작성 없이 운행일지를 간편하게 다운로드 할 수 있습니다.</p>
	</h2>
	<div class="sub_top1">
		<strong id="nowDate"></strong>
	</div>
	<div class="search_box1">
		<form id="frmSearch" method="post" onSubmit="return false;">
			<input type="text" style="width:100px; height:36px; border:1px solid #c0c0c0;" height:36px;" id="startDate" class="date datePickerMonth" value=""/>
			 ~ <input type="text" style="width:100px; height:36px; border:1px solid #c0c0c0;" height:36px;" id="endDate" class="date datePickerMonth" value=""/>
			<div class="car_choice_search_box" style="float:none; display:inline-block; height:36px; background-color:white;">
				<input type="text" class="btn_ajaxPop" id="searchVehicle" readonly style="padding-left:20px; height:36px;"/>
				<input type="hidden" name="searchVehicle" id="vehicleKey"/>
				<a href="#none" id="btn_ajaxPop" style="top:5px;"><img src="${pathCommon}/images/btn_search3.jpg" alt=""></a>
			</div>
			<a href="#none" class="btn_search">조회</a>
		</form>
	</div>
	<div class="exdown">
		<span>선택한 차량 운행일지 다운로드</span>
		<a href="#none" class="btn_down" data-target="1" style="width:170px"><img src="${pathCommon}/images/ico_ex.png" alt="" />국세청 제출용</a>
		<a href="#none" class="btn_down" data-target="2" style="width:170px"><img src="${pathCommon}/images/ico_ex.png" alt="" />통합 운행내역</a>	
	</div>
	<div class="tab_layout">
		<div style="margin:24px 0 10px; overflow: hidden;">
			<div class="sub_top1 float_left mgb0">
				<span>
					등록차량 수
					<b id="sumCnt">0대</b>
				</span>
				<span>
					업무사용비율
					<b id="sumRto">0%</b>
				</span>
			</div>
		</div>
		<table class="table1" id="contentsTable">
			<colgroup>
				<col style="width:70px;" />
				<col />
				<col style="width:120px;" />
				<col style="width:140px;" />
				<col style="width:120px;" />
				<col style="width:90px;" />
			</colgroup>
			<thead>
				<tr>
					<th><input type="checkbox" id="allChecked"></th>
					<th>차량정보</th>
					<th>총 주행거리</th>
					<th>업무용/출퇴근 사용거리</th>
					<th>비업무용 사용거리</th>
					<th>업무사용비율</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<div class="white">
	<div class="blackLoading" style="widht:50%;height:50%;position: fixed;padding-left: 43%;padding-top: 17%;">
    	<img src="/vdasPro/common/images/loading4.gif" style="width:300px;height:300px">
    </div>
</div>
<div class="black"></div>
<div class="pop7 vehicle" style="display:none;">
	<div class="pop6_area">
		<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt="" /></a>
		<h1>차량 선택</h1>
		<div class="txt">
			<p>위치조회를 할 차량을 선택해주세요.</p>
		</div>
		<div class="car_choice_btn2">
			<div class="car_choice_search_box">
				<input type="text" placeholder="직접검색" id="searchTextVehicle"/><a href="#none" id="btn_searchVehicle"><img src="${pathCommon}/images/btn_search3.jpg" alt=""></a>
			</div>
		</div>
		<table class="table1" id="contentsTableVehicle">
			<colgroup>
				<col style="width:60px;" />
				<col style="width:70px;" />
				<col style="width:230px;" />
				<col style="width:70px;" />
				<col style="width:100px;" />
				<col style="width:112px;" />
				<col style="width:118px;" />
			</colgroup>
			<thead>
				<tr>
					<th></th>
					<th>제조사</th>
					<th>차량정보</th>
					<th>배기량</th>
					<th>소유구분</th>
					<th>년식/주행거리</th>
					<th>색상/유종</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		<br><br>
		<div class="btn_type1"><a href="#none" class="btn_cancel">취소</a><a href="#none" class="btn_ok">확인</a></div>
	</div>
</div>
<%--
<c:if test="${VDAS.rule ne '0' and VDAS.rule ne '1' }">
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("M4003");
});
</script>
<div class="right_layout">
	<img src="${pathCommon}/images/${_LANG}/authError.png" alt="" style="width:100%">
</div>
</c:if>
 --%>