<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script type="text/javascript">
var lazy;
var lazyPop;
$(document).ready(function(){
	alert("step view");
	
	initMenuSel("M4004");
	
	$(".btn_search").on("click",function(){
		alert("todo! - SEARCH");
		//$.lazyLoader.search(lazy);
	});
	
	$('.ui_radio_box input[type=radio]').checkboxradio({
        icon: false
    });

	if(!lazy)
		lazy = $("#contentsGraph").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
				periodDb : $("#periodDb")
				,startDateDb : $("#startDateDb")
				,endDateDb : $("#endDateDb")
				,vehicleDb : $("#vehicleDb")
				,vehicleDetailDb : $("#vehicleDetailDb")
				,genderDb : $("#genderDb")
				,ageDb : $("#ageDb")
				,driverNmDb : $("#driverNmDb")
				,vehicleNumDb : $("#vehicleNumDb")
				,detailDb : $("#detailDb")
				,periodUser : $("#periodUser")
				,startDateUser : $("#startDateUser")
				,endDateUser : $("#endDateUser")
				,vehicleUser : $("#vehicleUser")
				,vehicleDetailUser : $("#vehicleDetailUser")
				,genderUser : $("#genderUser")
				,ageUser : $("#ageUser")
				,driverNmUser : $("#driverNmUser")
				,vehicleNumUser : $("#vehicleNumUser")
				,detailUser : $("#detailUser")
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/com/getVehicleList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.rtvList;
				
				var strHtml = '';
				
				return strHtml;
			}
		});
});
</script>
<form id="searchFrm">
	<input type="hidden" name="periodDb" value="" />
	<input type="hidden" name="startDateDb" value="" />
	<input type="hidden" name="endDateDb" value="" />
	<input type="hidden" name="vehicleDb" value="" />
	<input type="hidden" name="vehicleDetailDb" value="" />
	<input type="hidden" name="genderDb" value="" />
	<input type="hidden" name="ageDb" value="" />
	<input type="hidden" name="driverNmDb" value="" />
	<input type="hidden" name="vehicleNumDb" value="" />
	<input type="hidden" name="detailDb" value="" />
	<input type="hidden" name="periodUser" value="" />
	<input type="hidden" name="startDateUser" value="" />
	<input type="hidden" name="endDateUser" value="" />
	<input type="hidden" name="vehicleUser" value="" />
	<input type="hidden" name="vehicleDetailUser" value="" />
	<input type="hidden" name="genderUser" value="" />
	<input type="hidden" name="ageUser" value="" />
	<input type="hidden" name="driverNmUser" value="" />
	<input type="hidden" name="vehicleNumUser" value="" />
	<input type="hidden" name="detailUser" value="" />
</form>
<div class="right_layout">
	<h2 class="tit_sub">
		위험운전 비교분석
		<p>ViewCar 운행정보 DB와 등록차량/사용자별 위험운전습관을 비교·분석합니다.</p>
	</h2>
	<div class="risk_driving">
		<div class="risk_driving_1">
			<div class="risk_driving_1_1">
				<h1><img src="${pathCommon}/images/${_LANG}/risk_h1_1.png" alt="ViewCAR 운행정보 DB" /></h1>
				<div class="risk_driving_1_2" style="height:inherit">
					<ul>
						<li class="li1">운행기간</li>
						<li class="risk_driving_1_3 li2">
							<span class="tab_select_btn1" id="periodDb">
								<a href="#">전체</a>
								<a href="#">오늘</a>
								<a href="#">1주일</a>
								<a href="#">1개월</a>
								<a href="#">3개월</a>
								<a href="#">6개월</a>
							</span>
						</li>
					</ul>
					<ul>
						<li class="li1"></li>
						<li class="risk_driving_1_4_area li2">
							<div class="cal_area">
								<input type="text" id="startDateDb" class="date datePicker" style="width:106px; height:25px; border:1px solid #c0c0c0;"/>
								<span>~</span>
								<input type="text" id="endDateDb" class="date datePicker" style="width:106px; height:25px; border:1px solid #c0c0c0;"/>
							</div>
						</li>
					</ul>
					<ul>
						<li class="li1">운행차종</li>
						<li class="li2">
							<div class="select_type7">
								<div class="select_type1_1">
									<select class="sel" id="vehicleDb">
										<option value="1">1</option>
										<option value="2">2</option>
									</select>
								</div>
							</div>
						</li>
					</ul>
					<ul>
						<li class="li1"></li>
							<div class="select_type7">
								<div class="select_type1_1">
									<select class="sel" id="vehicleDetailDb">
										<option value="1">1</option>
										<option value="2">2</option>
									</select>
								</div>
							</div>
						</li>
					</ul>
					<ul>
						<li class="li1">운전자 성별</li>
						<li class="risk_driving_1_3 li2">
							<span class="tab_select_btn1" id="genderDb">
								<a href="#">전체</a>
								<a href="#">남자</a>
								<a href="#">여자</a>
							</span>
						</li>
					</ul>
					<ul>
						<li class="li1">운전자 연령</li>
						<li class="li2">
							<div class="select_type7">
								<div class="select_type1_1">
									<select class="sel" id="ageDb">
										<option value="1">1</option>
										<option value="2">2</option>
									</select>
								</div>
							</div>
						</li>
					</ul>
					<ul>
						<li class="li1">운전자 성명</li>
						<li class="li2">
							<input type="text" class="risk_input" id="driverNmDb"/>
						</li>
					</ul>
					<ul>
						<li class="li1">차량번호</li>
						<li class="li2">
							<input type="text" class="risk_input" id="vehicleNumDb"/>
						</li>
					</ul>
					<div class="btn">
						<a href="#" id="detailDb"><span>상세조회</span><img src="${pathCommon}/images/btn_detail.jpg" alt=""></a>
					</div>
				</div>
			</div>
			<div class="risk_driving_1_1">
				<h1><img src="${pathCommon}/images/${_LANG}/risk_h1_2.png" alt="사용자(그룹)" /></h1>
				<div class="risk_driving_1_2" style="height:inherit">
					<ul>
						<li class="li1">운행기간</li>
						<li class="risk_driving_1_3 li2">
							<span class="tab_select_btn1" id="periodUser">
								<a href="#">전체</a>
								<a href="#">오늘</a>
								<a href="#">1주일</a>
								<a href="#">1개월</a>
								<a href="#">3개월</a>
								<a href="#">6개월</a>
							</span>
						</li>
					</ul>
					<ul>
						<li class="li1"></li>
						<li class="risk_driving_1_4_area li2">
							<div class="cal_area">
								<input type="text" id="startDateUser" class="date datePicker" style="width:106px; height:25px; border:1px solid #c0c0c0;"/>
								<span>~</span>
								<input type="text" id="endDateUser" class="date datePicker" style="width:106px; height:25px; border:1px solid #c0c0c0;"/>
							</div>
						</li>
					</ul>
					<ul>
						<li class="li1">운행차종</li>
						<li class="li2">
							<div class="select_type7">
								<div class="select_type1_1">
									<select class="sel" id="vehicleUser">
										<option value="1">1</option>
										<option value="2">2</option>
									</select>
								</div>
							</div>
						</li>
					</ul>
					<ul>
						<li class="li1"></li>
							<div class="select_type7">
								<div class="select_type1_1">
									<select class="sel" id="vehicleDetailUser">
										<option value="1">1</option>
										<option value="2">2</option>
									</select>
								</div>
							</div>
						</li>
					</ul>
					<ul>
						<li class="li1">운전자 성별</li>
						<li class="risk_driving_1_3 li2">
							<span class="tab_select_btn1" id="genderUser">
								<a href="#">전체</a>
								<a href="#">남자</a>
								<a href="#">여자</a>
							</span>
						</li>
					</ul>
					<ul>
						<li class="li1">운전자 연령</li>
						<li class="li2">
							<div class="select_type7">
								<div class="select_type1_1">
									<select class="sel" id="ageUser">
										<option value="1">1</option>
										<option value="2">2</option>
									</select>
								</div>
							</div>
						</li>
					</ul>
					<ul>
						<li class="li1">운전자 성명</li>
						<li class="li2">
							<input type="text" class="risk_input" id="driverNmUser"/>
						</li>
					</ul>
					<ul>
						<li class="li1">차량번호</li>
						<li class="li2">
							<input type="text" class="risk_input" id="vehicleNumUser"/>
						</li>
					</ul>
					<div class="btn">
						<a href="#" id="detailUser"><span>상세조회</span><img src="${pathCommon}/images/btn_detail.jpg" alt=""></a>
					</div>
				</div>
			</div>
		</div>
		<div class="btn_area4"><a href="#" class="btn_search">조회</a></div>
		<div class="txt_area">
			ㆍViewCAR장착차량·운전자로부터 수집된 운행정보를 기준으로 사용자·그룹의 상대 위험운전 습관을 분석합니다.<br>
			ㆍ동일 사용자·그룹의 검색기간을 달리 설정하여 위험운전 개선 여부를 비교하실 수도 있습니다.<br>
			ㆍ교통안전공단 10대위험운전 행동기준을 중심으로 위험운전행동 건수를 비교하여 관리자의 운전 코칭자료로 활용될수 있도록 합니다.
		</div>
	</div>
	<div class="sub_top1">
		<span>비교분석 대상건수<b>30건</b></span>
	</div>
	<table class="table3">
		<colgroup>
			<col style="width:78px;" />
			<col />
			<col style="width:75px;" />								
			<col style="width:75px;" />
			<col style="width:75px;" />								
			<col style="width:79px;" />
			<col style="width:72px;" />								
			<col style="width:78px;" />
			<col style="width:95px;" />								
			<col style="width:95px;" />
		</colgroup>
		<thead>
			<tr>
				<th>대상</th>
				<td>총 운행횟수</td>
				<td>급출발</td>
				<td>급제동</td>
				<td>급가속</td>
				<td>급감속</td>
				<td>과속</td>
				<td>장기과속</td>
				<td>사고다발지점</td>
				<td>휴대폰사용</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th><b>ViewCar<br>DB</b></th>
				<td><b>x,xxx</b></td>
				<td>xx.xx%<span>(x,xxx회)</span></td>
				<td>xx.xx%<span>(x,xxx회)</span></td>
				<td>xx.xx%<span>(x,xxx회)</span></td>
				<td>xx.xx%<span>(x,xxx회)</span></td>
				<td>xx.xx%<span>(x,xxx회)</span></td>
				<td>xx.xx%<span>(x,xxx회)</span></td>
				<td>xx.xx%<span>(x,xxx회)</span></td>
				<td>xx.xx%<span>(x,xxx회)</span></td>
			</tr>
			<tr>
				<th><b>사용자<br>(그룹)</b></th>
				<td><b>xxx</b></td>
				<td>xx.xx%<span>(xx회)</span></td>
				<td>xx.xx%<span>(xx회)</span></td>
				<td>xx.xx%<span>(xx회)</span></td>
				<td>xx.xx%<span>(xx회)</span></td>
				<td>xx.xx%<span>(xx회)</span></td>
				<td>xx.xx%<span>(xx회)</span></td>
				<td>xx.xx%<span>(xx회)</span></td>
				<td>xx.xx%<span>(xx회)</span></td>
			</tr>
		</tbody>
	</table>
	<div class="graph_area">
		<img src="${pathCommon}/images/graph2.jpg" alt="" />
		<div class="risk_graph" id="contentsGraph">
			<div class="risk_graph2">
				<div class="risk_graph_1">
					<div class="graph_viewcar"></div>
					<div class="graph_user"></div>
				</div>
				<div class="risk_graph_2">
					<div class="graph_viewcar"></div>
					<div class="graph_user"></div>
				</div>
				<div class="risk_graph_3">
					<div class="graph_viewcar"></div>
					<div class="graph_user"></div>
				</div>
				<div class="risk_graph_4">
					<div class="graph_viewcar"></div>
					<div class="graph_user"></div>
				</div>
				<div class="risk_graph_5">
					<div class="graph_viewcar"></div>
					<div class="graph_user"></div>
				</div>
				<div class="risk_graph_6">
					<div class="graph_viewcar"></div>
					<div class="graph_user"></div>
				</div>
				<div class="risk_graph_7">
					<div class="graph_viewcar"></div>
					<div class="graph_user"></div>
				</div>
				<div class="risk_graph_8">
					<div class="graph_viewcar"></div>
					<div class="graph_user"></div>
				</div>
				<div class="risk_graph_9">
					<div class="graph_viewcar"></div>
					<div class="graph_user"></div>
				</div>
			</div>
		</div>
		<div class="risk_graph3">
			<span class="t1">급출발</span><span class="t2">급정지</span><span class="t3">급제동</span><span class="t4">급가속</span><span class="t5">급감속</span><span class="t6">과속</span><span class="t7">장기과속</span><span class="t8">사고다발지점</span><span class="t9">휴대폰사용</span>
		</div>
	</div>
	<div class="detail_box2">
		위험운전 상대비교<span class="i1"><strong></strong>ViewCar DB</span><span class="i2"><strong></strong>사용자(그룹)</span>
	</div>
</div>