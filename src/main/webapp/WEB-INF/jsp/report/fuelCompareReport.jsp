<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/index.js"></script>

<script type="text/javascript">

var rData;
$(document).ready(function(){
	initMenuSel("M4002");
	
	$VDAS.ajaxCreateSelect("/com/getGroupVehicleList.do",{},$("#searchVehicle"),false,"전체 차량","","${_LANG}",null,null);
	
	$(".btn_search").on("click",function(){
		var vehicleKey = $("#searchVehicle").val();

		var startDateA = replaceAll($("#startDateA").val(),"/","");
		var endDateA = replaceAll($("#endDateA").val(),"/","");
		var startDateB = replaceAll($("#startDateB").val(),"/","");
		var endDateB = replaceAll($("#endDateB").val(),"/","");
		
		if(startDateA == ""){
			alert("A구간 시작일을 지정해주세요.")
			return;
		}else if(endDateA == ""){
			alert("A구간 종료일을 지정해주세요.")
			return;
		}else if(startDateB == ""){
			alert("B구간 시작일을 지정해주세요.")
			return;
		}else if(endDateB == ""){
			alert("B구간 종료일을 지정해주세요.")
			return;
		}else if(vehicleKey == ""){
			alert("차량을 선택해주세요")
			return;
		}
		
		/* $.ajax({
			url:"/vdasPro/report/getFuelAverageCompare.do"
			,dataType : 'json'
			,contentType: "application/x-www-form-urlencoded; charset=UTF-8"
			,type:"POST"
			,data:{vehicleKey:vehicleKey, startDateA : startDateA, endDateA : endDateA, startDateB : startDateB , endDateB : endDateB} // data !!!!!!!!!!
			,cache:!1
			,success:function(t){
				var a = t.rtvList
				rData = a;
				
				$('#chartDiv').show();
				
				chartDrawModule.chartDrawLine(t.rtvList);
				chartDrawModule.chartDrawColumn(t.rtvList);
				
				
			}
			,error:function(t){
				console.log(t);	
			}
			,beforeSend: function() {
				
			}
		}).done(function(){
			
		}); */
		
		
		$VDAS.http_post("/report/getFuelAverageCompare.do",
				{vehicleKey:vehicleKey, startDateA : startDateA, endDateA : endDateA, startDateB : startDateB , endDateB : endDateB},{			
			success : function(t){
				var a = t.rtvList
				rData = a;
				
				$('#chartDiv').show();
				
				chartDrawModule.chartDrawLine(t.rtvList);
				chartDrawModule.chartDrawColumn(t.rtvList);
			}
		});
		
	});
	
	
	
});

var chartDrawModule = (function(){
	
	this.lineChartData = [];
	this.columnChartData = [];
	this.AavgFco;
	this.BavgFco;
	this.AallDistance;
	this.AallFco;
	this.AallOilPrice;
	this.BallDistance;
	this.BallFco;
	this.BallOilPrice;
	this.aColor = "#ffc71c";
	this.bColor = "#e81626";
	
	//Line 차트 데이터 가공
	function generatorLineChartData(data){
		
		this.lineChartData = [];
		
		var AType = data.getFuelAverageCompareA;
		var BType = data.getFuelAverageCompareB;
		
		this.AavgFco = data.getFuelSpeedCompareA.avgfco;
		this.BavgFco = data.getFuelSpeedCompareB.avgfco;

		//label을 붙이지 않으면 argument가 자동으로 늘어나서 여러개 만든다.
		for(var i = 0 ; i < AType.length ; i++){
			AType[i].label = AType[i].x;
			//canvas.js는 int형만 차트로 표현해준다
			if(AType[i].y != null){
				AType[i].y = parseFloat(AType[i].y)
			}
		}
		for(var j = 0 ; j < BType.length ; j++){
			BType[j].label = BType[j].x;
			if(BType[j].y != null){
				BType[j].y = parseFloat(BType[j].y)
			}
		}
		
		//A구간
		this.lineChartData.push({        
	        type: "line",
	        legendText: "A구간",
			showInLegend: true,
			color: this.aColor,
	        dataPoints: AType
	      });
		
		//B구간
		this.lineChartData.push({        
	        type: "line",
	        legendText: "B구간",
			showInLegend: true,
			color: this.bColor,
	        dataPoints: BType
	      });
		      
	}
	function generatorColumnChartData(data){
		
		this.columnChartData = [];
		
		
		var AType = data.getFuelSpeedCompareA;
		var BType = data.getFuelSpeedCompareB;
		
		this.AallDistance = AType.allDistance;
		this.AallFco = AType.allFco
		this.AallOilPrice = AType.allOilPrice
		
		this.BallDistance = BType.allDistance;
		this.BallFco = BType.allFco
		this.BallOilPrice = BType.allOilPrice
		
		var aUnder20 = AType.under20 / 1000;
		var aUnder40 = AType.under40 / 1000;
		var aUnder60 = AType.under60 / 1000;
		var aUnder80 = AType.under80 / 1000;
		var aUnder100 = AType.under100 / 1000;
		var aUnder120 = AType.under120 / 1000;
		var aUnder140 = AType.under140 / 1000;
		var aOver140 = AType.over140 / 1000;
		
		var bUnder20  = BType.under20 / 1000;
		var bUnder40  = BType.under40 / 1000;
		var bUnder60  = BType.under60 / 1000;
		var bUnder80  = BType.under80 / 1000;
		var bUnder100 = BType.under100 / 1000;
		var bUnder120 = BType.under120 / 1000;
		var bUnder140 = BType.under140 / 1000;
		var bOver140  = BType.over140 / 1000;
		
		this.columnChartData.push({        
	        type: "column",
	        legendText: "A구간",
			showInLegend: true,
			color: this.aColor,
	        dataPoints: [
	        { x: 1, y: parseFloat(aUnder20.toFixed(1)), label : "0~20"},
	        { x: 2, y: parseFloat(aUnder40.toFixed(1)), label : "21~40"},
	        { x: 3, y: parseFloat(aUnder60.toFixed(1)), label : "41~60" },
	        { x: 4, y: parseFloat(aUnder80.toFixed(1)), label : "61~80" },
	        { x: 5, y: parseFloat(aUnder100.toFixed(1)), label : "81~100" },
	        { x: 6, y: parseFloat(aUnder120.toFixed(1)), label : "101~120" },
	        { x: 7, y: parseFloat(aUnder140.toFixed(1)), label : "121~140" },
	        { x: 8, y: parseFloat(aOver140.toFixed(1)), label : "140~" }
	        ]
	      });

		this.columnChartData.push( {        
	        type: "column",
	        legendText: "B구간",
			showInLegend: true,
			color: this.bColor,
	        dataPoints: [
   	        { x: 1, y: parseFloat(bUnder20.toFixed(1)), label : "0~20"},
   	        { x: 2, y: parseFloat(bUnder40.toFixed(1)), label : "21~40"},
   	        { x: 3, y: parseFloat(bUnder60.toFixed(1)), label : "41~60" },
   	        { x: 4, y: parseFloat(bUnder80.toFixed(1)), label : "61~80" },
   	        { x: 5, y: parseFloat(bUnder100.toFixed(1)), label : "81~100" },
   	        { x: 6, y: parseFloat(bUnder120.toFixed(1)), label : "101~120" },
   	        { x: 7, y: parseFloat(bUnder140.toFixed(1)), label : "121~140" },
   	        { x: 8, y: parseFloat(bOver140.toFixed(1)), label : "140~" }
   	        ]
	      });
		
		
	}
	
	function lineChartDraw(){
		
		$('#fuelAverageA').html("A구간   &middot; 평균 연비 <span style='color:#c49507'>" + this.AavgFco + "Km/ℓ</span>");
		$('#fuelAverageB').html("B구간   &middot; 평균 연비 <span style='color:red'>" + this.BavgFco + "Km/ℓ</span>");
		
		var chart = new CanvasJS.Chart("chartContainer",
			    {
				  legend:{
					fontStyle : "normal"
						,fontSize : 12
						,fontWeight : "normal"
						,fontFamily : "sans-serif"
				       	,horizontalAlign: "right" // left, center ,right 
				        ,verticalAlign: "top" // top, center, bottom
				      },
			      data: this.lineChartData
			    });
		
		chart.render();
	}
	
	function columnChartDraw(){
		
		var forecastOilPriceA = parseInt(this.AallOilPrice)/parseInt(this.AallFco);
		var forecastOilPriceB = parseInt(this.BallOilPrice)/parseInt(this.BallFco);
		
		$('#fuelSpeedA').html("A구간  &nbsp;&nbsp;&nbsp;&nbsp;  &middot; 총 주행거리 <span style='color:#c49507'>" + this.AallDistance +"km" + "</span>&nbsp;&nbsp; &middot; 연료소모량 <span style='color:#c49507'>" + this.AallFco + "ℓ" + "</span>&nbsp;&nbsp; &middot; 예상유류비 <span style='color:#c49507'>"+ number_format(this.AallOilPrice)+"원 (1km기준"+number_format(forecastOilPriceA.toFixed(0))+"원)</span>");
		$('#fuelSpeedB').html("B구간  &nbsp;&nbsp;&nbsp;&nbsp;  &middot; 총 주행거리 <span style='color:red'>" + this.BallDistance +"km" + "</span>&nbsp;&nbsp; &middot; 연료소모량 <span style='color:red'>" + this.BallFco + "ℓ" + "</span>&nbsp;&nbsp; &middot; 예상유류비 <span style='color:red'>"+ number_format(this.BallOilPrice)+"원(1km기준"+number_format(forecastOilPriceB.toFixed(0))+"원)</span>");
		
		 var chart2 = new CanvasJS.Chart("chartContainer2",
		    	    {
			 		  legend:{
		    			fontStyle : "normal"
		    				,fontSize : 12
		    				,fontWeight : "normal"
		    				,fontFamily : "sans-serif"
		    		       	,horizontalAlign: "right" // left, center ,right 
		    		        ,verticalAlign: "top" // top, center, bottom
		    		      	},
		    		    toolTip :{
		    		    	content : "{label}: {y}ℓ"
		    		    },
		    		    data: this.columnChartData
		    	    });

		 chart2.render();
	};
	
	return {
		chartDrawLine : function(chartData){
			generatorLineChartData(chartData);
			lineChartDraw();
		},
		chartDrawColumn : function(chartData){
			generatorColumnChartData(chartData);
			columnChartDraw();
		}
	}
})();

</script>
<form id="searchFrm" style="display:none">
	<input type="hidden" name="startDate" value="" />
	<input type="hidden" name="searchLocation" value="" />
	<input type="hidden" name="searchLocationDetail" value="" />
	<input type="hidden" name="searchText" value="" />
	<input type="hidden" name="searchOrder" value="" />
</form>
<div class="right_layout">
	<div class="auth user contents">
		<h2 class="tit_sub noneHide">
			연비 비교 분석
			<p>기준일과 비교일을 설정하여 평균 연비를 비교해볼 수 있습니다.</p>
		</h2>
		<div class="search_box1 search_type mgb0 noneHide">
			<div class="">
				<div>
					<span class="tit2">A구간</span> 
					<input type="text" id="startDateA" class="date datePicker" style="width:65px; height:35px; border:1px solid #c0c0c0;"/>
					<span>~</span>
					<input type="text" id="endDateA" class="date datePicker" style="width:65px; height:35px; border:1px solid #c0c0c0;"/>
				</div>
				
				<br>
				
				<div>
					<span class="tit2">B구간</span>
					<input type="text" id="startDateB" class="date datePicker" style="width:65px; height:35px; border:1px solid #c0c0c0;"/>
					<span>~</span>
					<input type="text" id="endDateB" class="date datePicker" style="width:65px; height:35px; border:1px solid #c0c0c0;"/>
				</div>
				<br>
				
				<div>
					<select class="select form" id="searchVehicle" style="width: 230px; margin-left: 33px;">
					
					</select>
				</div>
				
				<br>
				<a href="#none" class="btn_search" style="margin-top: 20px; margin-left: 27px;">조회</a>
			</div>
			
		</div>
		
		<div id="chartDiv" style="width:100%;display:none">
			<!-- 운행정보 -->
			<h3 class="tit01 mgt30">연비평균 비교<i></i></h3>
			(km/ℓ,일)
			<div id="chartContainer" style="height: 300px; width: 100%;"></div>
			<div style="border:1px solid #bbbbbb; width: 100%; height: 25px; padding-top: 15px;text-align: center;">
				<span id="fuelAverageA" style="margin-right: 150px;font-weight: bold;"></span>
				<span id="fuelAverageB" style="font-weight: bold;"></span>
			</div>
			<br>
			<h3 class="tit01 mgt30">연비/속도 비교<i></i></h3>
			(km/ℓ,일)
			<div id="chartContainer2" style="height: 300px; width: 100%;"></div>
			<br>
			
			<div style="border:1px solid #bbbbbb; width: 100%; height: 25px; padding-top: 15px;text-align: center;">
				<span id="fuelSpeedA" style="font-weight: bold;"></span>
			</div>
			<div style="border:1px solid #bbbbbb; width: 100%; height: 25px; padding-top: 15px;text-align: center;">
				<span id="fuelSpeedB" style="font-weight: bold;"></span>	
			</div>
		</div>
	</div>
</div>