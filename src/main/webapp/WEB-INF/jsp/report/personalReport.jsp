<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<fmt:setLocale value="${_LANG}" scope="request" />
<fmt:setBundle basename="properties.messages.rest.message" />

<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/index.js"></script>

<script type="text/javascript">

var rData;
$(document).ready(function(){
	initMenuSel("M4002");
	
	var Now = new Date();
	var NowY = Now.getFullYear();
	var NowM = LPAD(''+(Now.getMonth()+1),'0',2);
	$("#searchMonth").val(NowY+"-"+NowM);
	
	<c:if test="${VDAS.rule eq '0' or VDAS.rule eq '1' }">
	$VDAS.ajaxCreateSelect("/com/getGroupUserList.do",{},$("#searchUser"),false,"<fmt:message key="personalReport.allUser"/>","","${_LANG}",null,null);
	</c:if>
	<c:if test="${VDAS.rule ne '0' and VDAS.rule ne '1' }">
	$VDAS.ajaxCreateSelect("/com/getGroupUserList.do",{},$("#searchUser"),false,null,"","${_LANG}",null,null);
	</c:if>
	
	$VDAS.ajaxCreateSelect("/com/getGroupVehicleList.do",{},$("#searchVehicle"),false,"<fmt:message key="personalReport.allVehicle"/>","","${_LANG}",null,null);
	$(".btn_search").on("click",function(){
		var date = replaceAll($("#searchMonth").val(),"-","");
		var vehicleKey = $("#searchVehicle").val();
		var accountKey = $("#searchUser").val();
		SuddenSummaryInit();
		$.ajax({
			url:"/vdasPro/report/getDrivingSummaryPersonal.do"
			,dataType : 'json'
			,contentType: "application/x-www-form-urlencoded; charset=UTF-8"
			,type:"POST"
			,data:{yyyymm:date, vehicleKey:vehicleKey, accountKey:accountKey} // data !!!!!!!!!!
			,cache:!1
			,success:function(t){
				var a = t.rtvList
				rData = a;
				
				draw(a);
			}
			,error:function(t){
				$(".contents").children().not(".noneHide").show();
				$(".white").fadeOut();
			}
			,beforeSend: function() {
				$(".contents").children().not(".noneHide").hide();
				$(".white").fadeIn();
			}
		}).done(function(){
			$(".contents").children().not(".noneHide").show();
			$(".white").fadeOut();
		});
	});
	
	$(".contents").children().not(".noneHide").hide();

	$(".btn_black").on("click",function(){
		if(rData) {
			$("body").append("<form id='popFrm'></form>");

			var strHtml = '<input type="text" name="rData" value='+replaceAll(JSON.stringify(rData)," ","&nbsp;")+' >';
			$("#popFrm").append(strHtml);
			$("#popFrm").attr("action","/vdasPro/report/personalReportPrint.do");
			$("#popFrm").attr("method","POST");
			$("#popFrm").attr("target","popForm");
			$("#popFrm").attr("accept-charset","UTF-8");
			 
			var p = window.open("" ,"popForm","");
			$("#popFrm").submit();
			
		}else{
			alert("<fmt:message key="personalReport.txt1"/>");
		}
	});
});


function draw(a){
	chart1Area(a.getInfoSummaryTotalMonth, a.getSuddenSummaryTotalAll, a.getScoreSummaryAccountAll);
	chart4Area(a.getBizTripSummaryTotalMonth);
	chart5Area(a.getBizTripSummaryTotalHours, a.getBizTripSummaryAccountAll, a.getBizTripSummaryTotalQuater);
	chart6Area(a.getExpenseSummaryTotalMonth, a.getExpenseSummaryTotalPrev, a.getExpenseSummaryAccountAll, a.getExpenseSummaryAccountPrev);
}


function SuddenSummaryInit(){
	$(".rapidStart").text("<fmt:message key="personalReport.zeroCount1"/>");
	$(".rapidStop").text("<fmt:message key="personalReport.zeroCount2"/>");
	$(".rapidAccel").text("<fmt:message key="personalReport.zeroCount3"/>");
	$(".rapidDeaccel").text("<fmt:message key="personalReport.zeroCount4"/>");
	$(".rapidTurn").text("<fmt:message key="personalReport.zeroCount5"/>");
	$(".rapidUtern").text("<fmt:message key="personalReport.zeroCount6"/>");
}

function suddenSummaryCall(a){
	$(".rapidStart").text(a.rapidStart+"<fmt:message key="personalReport.count1"/>");
	$(".rapidStop").text(a.rapidStop+"<fmt:message key="personalReport.count2"/>");
	$(".rapidAccel").text(a.rapidAccel+"<fmt:message key="personalReport.count3"/>");
	$(".rapidDeaccel").text(a.rapidDeaccel+"<fmt:message key="personalReport.count4"/>");
	$(".rapidTurn").text(a.rapidTurn+"<fmt:message key="personalReport.count5"/>");
	$(".rapidUtern").text(a.rapidUtern+"<fmt:message key="personalReport.count6"/>");
}

function chart1Area(infoSummary, suddenSummary, scoreSummary){
	var drivingTime;
	var score4;
	var safeScore;
	var ecoScore;
	var fuelRank;
	var safeRank;
	var ecoRank;
	var totalRank;
	var month=[0,0,0,0,0,0,0,0,0,0,0,0];
	var totalDistance = 0;
	var totalFco = 0;
	var totalFuelEff = 0;
	var count = 0;
	var selFlag=[null,null,null,null,null,null,null,null,null,null,null,null];
	$("#myChart01").html('<div class="level"><span id="totalRank"></span> <i class="icon totalIcon"></i></div>');
	$("#fuelRank").text("");
	$("#safeRank").text("");
	$("#ecoRank").text("");
	$("#distance").text(" km");
	$("#drivingTime").text("<fmt:message key="personalReport.timeText"/>");
	$("#fco").text(" ℓ");
	$("#oilPrice").text(" <fmt:message key="personalReport.won"/>");
	console.log(infoSummary);
	
	for(var i=0;i<infoSummary.length;i++){
		month[i] = infoSummary[i].fuelEff?parseFloat(infoSummary[i].fuelEff):null;
		totalDistance += infoSummary[i].distance?parseInt(infoSummary[i].distance):0;
		totalFco += infoSummary[i].fco?parseInt(infoSummary[i].fco):0;
		totalFuelEff += month[i];
		if(infoSummary[i].officialFuelEff != null) count++;
		if(infoSummary[i].selFlag == "1"){
			selFlag[i] = "red";
			drivingTime = infoSummary[i].drivingTime?infoSummary[i].drivingTime:0;
			score4 = infoSummary[i].score4?infoSummary[i].score4:0;
			safeScore = infoSummary[i].safeScore?infoSummary[i].safeScore:0;
			ecoScore = infoSummary[i].ecoScore?infoSummary[i].ecoScore:0;
			
			fuelRank = calRank(score4);
			safeRank = calRank(safeScore);
			ecoRank = calRank(ecoScore);
			totalRank = calRank((score4+safeScore+ecoScore)/3);
			
		    // getInfoSummaryTotalMonth
			$("#myChart01").drawDoughnutChart([
		 	    { title: "<fmt:message key="personalReport.fuelEfficiency"/>", value : score4,  color: "#a150cb" },
		  	    { title: "<fmt:message key="personalReport.safeScore"/>", value : safeScore,  color: "#ea2b7e" },
		  	    { title: "<fmt:message key="personalReport.ecoScroe"/>", value:  ecoScore,   color: "#68b2c2" }
		    ]);

			$("#totalRank").text(""+totalRank);
			if(true)
				$(".totalIcon").addClass("up");
			else
				$(".totalIcon").addClass("down");
			$("#fuelRank").text(""+fuelRank);
			$("#safeRank").text(""+safeRank);
			$("#ecoRank").text(""+ecoRank);
			$("#distance").text(""+number_format(parseFloat(infoSummary[i].distance?(infoSummary[i].distance/1000):0).toFixed(2))+" km");
			$("#drivingTime").text(""+parseInt(drivingTime/3600)+'<fmt:message key='personalReport.time'/> '+LPAD(''+parseInt((drivingTime%3600)/60),'0',2)+'<fmt:message key='personalReport.minute'/> '+LPAD(''+parseInt((drivingTime%3600)%60),'0',2)+'<fmt:message key='personalReport.second'/>');
			$("#fco").text(""+number_format(parseFloat(infoSummary[i].fco?(infoSummary[i].fco/1000):0).toFixed(2))+" ℓ");
			$("#oilPrice").text(""+number_format(infoSummary[i].oilPrice?infoSummary[i].oilPrice:0)+" <fmt:message key="personalReport.won2"/>");
			
			$("#avgFco").text(""+parseFloat(infoSummary[i].fuelEff?infoSummary[i].fuelEff:0).toFixed(2)+" km/L");
			$("#comFco").text(""+parseFloat(infoSummary[i].officialFuelEff?infoSummary[i].officialFuelEff:0).toFixed(2)+" km/L");
		}
	}
	
	if(suddenSummary!=null)
		suddenSummaryCall(suddenSummary);

	

	var tableHtml = "";
	for(var i=0;i<scoreSummary.length;i++){
		tableHtml += "<tr>";
		tableHtml += "<td class='first'>"+(scoreSummary[i].userNm?scoreSummary[i].userNm:"")+"</td>";
		tableHtml += "<td>";
		tableHtml += "<div class='bslist'>";
		tableHtml += "<span><fmt:message key="personalReport.fuelEfficiency2"/> <b>"+calRank(scoreSummary[i].ecoScore?scoreSummary[i].ecoScore:0)+"</b></span>";
		tableHtml += "<span><fmt:message key="personalReport.safeScore2"/> <b>"+calRank(scoreSummary[i].safeScore?scoreSummary[i].safeScore:0)+"</b></span>";
		tableHtml += "<span><fmt:message key="personalReport.ecoScroe2"/> <b>"+calRank(scoreSummary[i].fuelScore?scoreSummary[i].fuelScore:0)+"</b></span>";
		tableHtml += "</div>";
		tableHtml += "</td>";
		tableHtml += "</tr>";
	}
	
	// getInfoSummaryTotalMonth
	var chart1 = new CanvasJS.Chart("chart1", {
		width:806,
		height:157,
		axisY: {
			labelFormatter:function(e){
				return e.value+"km/L";				
			}
			,labelFontSize: 12
			,labelFontWeight : "lighter"
			,labelFontColor: "black"
			//,interlacedColor: "#F0F8FF" 
			,lineThickness: 0
			,tickLength: 0
			,gridThickness: 1
		},
		axisX: {
			interval: 1
			,labelFormatter:function(e){				
				return (e.value+1)+"<fmt:message key="personalReport.month"/>";		
				
			}
			,labelFontSize: 12
			,labelFontWeight : "lighter"
			,labelFontColor: "black"
			,lineThickness: 1
			,tickLength: 5
		},
		
		toolTip : {
			fontStyle : "normal"
			,fontSize : 14
			
			,fontWeight : "normal"
			,fontFamily : "sans-serif"
		},
		data: [{
			type: "line"
			,markerSize: 12
			,lineColor : "#cccdc9"
			,color : "#cccdc9"
			,toolTipContent: "{y}km/L"
			,dataPoints: [
			  { y: month[0] ,color: selFlag[0]},
			  { y: month[1] ,color: selFlag[1]},
			  { y: month[2] ,color: selFlag[2]},
			  { y: month[3] ,color: selFlag[3]},
			  { y: month[4] ,color: selFlag[4]},
			  { y: month[5] ,color: selFlag[5]},
			  { y: month[6] ,color: selFlag[6]},
			  { y: month[7] ,color: selFlag[7]},
			  { y: month[8] ,color: selFlag[8]},
			  { y: month[9] ,color: selFlag[9]},
			  { y: month[10] ,color: selFlag[10]},
			  { y: month[11] ,color: selFlag[11]},
			]
		}]
	});
	chart1.render();
}

function chart4Area(bizTripSummary){
	var biz = new Array(bizTripSummary.length);
	var commute = new Array(bizTripSummary.length);
	var noneBiz = new Array(bizTripSummary.length);
	var jsonBizArray = new Array();
	var jsonCommuteArray = new Array();
	var jsonNoneBizArray = new Array();
	var jsonBizTemp;
	var jsonCommuteTemp;
	var jsonNoneBizTemp;
	var maximum = 0;
	
	for(var i = 0;i < bizTripSummary.length;i++){
		jsonBizTemp = new Object();
		jsonCommuteTemp = new Object();
		jsonNoneBizTemp = new Object();
		biz[i] = bizTripSummary[i].bizCnt1?bizTripSummary[i].bizCnt1:0;
		commute[i] = bizTripSummary[i].bizCnt2?bizTripSummary[i].bizCnt2:0;
		noneBiz[i] = bizTripSummary[i].bizCnt3?bizTripSummary[i].bizCnt3:0;
		if(maximum < biz[i]) maximum = biz[i];
		else if(maximum < commute[i]) maximum = commute[i];
		else if(maximum < noneBiz[i]) maximum = noneBiz[i];
		
		jsonBizTemp.y = biz[i];
		jsonCommuteTemp.y = commute[i];
		jsonNoneBizTemp.y = noneBiz[i];
		if(bizTripSummary[i].selFlag == "1")
			jsonBizTemp.indexLabel = "now";
		jsonBizArray.push(jsonBizTemp);
		jsonCommuteArray.push(jsonCommuteTemp);
		jsonNoneBizArray.push(jsonNoneBizTemp);
	}
	var interval = parseInt(maximum / 5);
	
	var chart4 = new CanvasJS.Chart("chart4", {
		width:806,
		height:200,
		axisY: {
			labelFormatter:function(e){				
				return e.value+"<fmt:message key="personalReport.cnt"/>";				
			}
			,labelFontSize: 12
			,labelFontWeight : "lighter"
			,labelFontColor: "black"
			,lineThickness: 0
			,tickLength: 0
			,gridThickness: 1
			,interval: interval
		},
		axisX: {
			interval: 1
			,labelFormatter:function(e){				
				return (e.value+1)+"<fmt:message key="personalReport.month2"/>";
				
			}
			,labelFontSize: 12
			,labelFontWeight : "lighter"
			,labelFontColor: "black"
			,lineThickness: 1
			,tickLength: 5
		},
		toolTip : {
			fontStyle : "normal"
			,fontSize : 14
			,fontWeight : "normal"
			,fontFamily : "sans-serif"
		},
		legend:{
			fontStyle : "normal"
			,fontSize : 12
			,fontWeight : "normal"
			,fontFamily : "sans-serif"
	       	,horizontalAlign: "right" // left, center ,right 
	        ,verticalAlign: "top" // top, center, bottom
	      },
	      dataPointWidth: 15,
	      data: [{
				color : "#ffc71c"
				,legendText: "<fmt:message key="personalReport.drivingType1"/>"
				,showInLegend: true
				,toolTipContent: "<fmt:message key="personalReport.drivingType11"/> {y}<fmt:message key="personalReport.drivingType1_2"/>"
				,dataPoints: []
			},{
				color : "#ca58ff"
				,legendText: "<fmt:message key="personalReport.drivingType2"/>"
				,showInLegend: true
				,toolTipContent: "<fmt:message key="personalReport.drivingType21"/> {y}<fmt:message key="personalReport.drivingType2_2"/>"
				,dataPoints: []
			},{
				color : "#ed1c24"
				,legendText: "<fmt:message key="personalReport.drivingType3"/>"
				,showInLegend: true
				,toolTipContent: "<fmt:message key="personalReport.drivingType31"/> {y}<fmt:message key="personalReport.drivingType3_2"/>"
				,dataPoints: []
			}]
	});
	
	for(var i = 0;i < jsonBizArray.length;i++){
		chart4.options.data[0].dataPoints.push(jsonBizArray[i]);
		chart4.options.data[1].dataPoints.push(jsonCommuteArray[i]);
		chart4.options.data[2].dataPoints.push(jsonNoneBizArray[i]);
	}
	chart4.render();
}

function chart5Area(bizTripSummary, bizTripSummaryAccount, bizTripSummaryQuater){
	var biz = new Array(bizTripSummary.length);
	var commute = new Array(bizTripSummary.length);
	var noneBiz = new Array(bizTripSummary.length);
	var jsonBizArray = new Array();
	var jsonCommuteArray = new Array();
	var jsonNoneBizArray = new Array();
	var jsonBizTemp;
	var jsonCommuteTemp;
	var jsonNoneBizTemp;
	var maximum = 0;
	
	for(var i = 0;i < bizTripSummary.length;i++){
		jsonBizTemp = new Object();
		jsonCommuteTemp = new Object();
		jsonNoneBizTemp = new Object();
		biz[i] = bizTripSummary[i].bizCnt1?bizTripSummary[i].bizCnt1:0;
		commute[i] = bizTripSummary[i].bizCnt2?bizTripSummary[i].bizCnt2:0;
		noneBiz[i] = bizTripSummary[i].bizCnt3?bizTripSummary[i].bizCnt3:0;
		if(maximum < biz[i]) maximum = biz[i];
		else if(maximum < commute[i]) maximum = commute[i];
		else if(maximum < noneBiz[i]) maximum = noneBiz[i];
		
		jsonBizTemp.y = biz[i];
		jsonCommuteTemp.y = commute[i];
		jsonNoneBizTemp.y = noneBiz[i];
		if(bizTripSummary[i].selFlag == "1")
			jsonBizTemp.indexLabel = "now";
		jsonBizArray.push(jsonBizTemp);
		jsonCommuteArray.push(jsonCommuteTemp);
		jsonNoneBizArray.push(jsonNoneBizTemp);
	}
	var interval = parseInt(maximum / 5);

	var chart5 = new CanvasJS.Chart("chart5", {
		width:806,
		height:200,
		axisY: {
			labelFormatter:function(e){				
				return e.value+"<fmt:message key="personalReport.cnt2"/>";				
			}
			,labelFontSize: 12
			,labelFontWeight : "lighter"
			,labelFontColor: "black"
			,lineThickness: 0
			,tickLength: 0
			,gridThickness: 1
			,interval: interval
		},
		axisX: {
			interval: 2
			,labelFormatter:function(e){				
				return (e.value)+"<fmt:message key="personalReport.hour"/>";		
				
			}
			,labelFontSize: 12
			,labelFontWeight : "lighter"
			,labelFontColor: "black"
			,lineThickness: 1
			,tickLength: 5
		},
		toolTip : {
			fontStyle : "normal"
			,fontSize : 14
			,fontWeight : "normal"
			,fontFamily : "sans-serif"
		},
		legend:{
			fontStyle : "normal"
			,fontSize : 12
			,fontWeight : "normal"
			,fontFamily : "sans-serif"
	       	,horizontalAlign: "right" // left, center ,right 
	        ,verticalAlign: "top" // top, center, bottom
	      },
	      
	      dataPointWidth: 17,
	      data: [{
				type: "stackedColumn"
				,color : "#ffc71c"
				,legendText: "<fmt:message key="personalReport.drivingType1_1"/>"
				,showInLegend: true 
				,toolTipContent: "<fmt:message key="personalReport.drivingType1_1_1"/> {y}<fmt:message key="personalReport.drivingType1_2_1"/>"
				,dataPoints: []
			},{
				type: "stackedColumn"
				,color : "#ca58ff"
				,legendText: "<fmt:message key="personalReport.drivingType2_1"/>"
				,showInLegend: true
				,toolTipContent: "<fmt:message key="personalReport.drivingType2_1_1"/> {y}<fmt:message key="personalReport.drivingType2_2_1"/>"
				,dataPoints: []
			},{
				type: "stackedColumn"
				,color : "#ed1c24"			
				,legendText: "<fmt:message key="personalReport.drivingType3_1"/>"
				,showInLegend: true
				,toolTipContent: "<fmt:message key="personalReport.drivingType3_1_1"/> {y}<fmt:message key="personalReport.drivingType3_2_1"/>"
				,dataPoints: []
			}]
	});
	
	for(var i = 0;i < jsonBizArray.length;i++){
		chart5.options.data[0].dataPoints.push(jsonBizArray[i]);
		chart5.options.data[1].dataPoints.push(jsonCommuteArray[i]);
		chart5.options.data[2].dataPoints.push(jsonNoneBizArray[i]);
	}
	chart5.render();
	
	var tableHtml = '';
	var drivingTime = 0;
	var bizDistanceTot = 0;
	var bizTime = 0;
	var bizCnt = [0,0,0];
	for(var i = 0;i < bizTripSummaryAccount.length;i++){
		var vo = bizTripSummaryAccount[i];

		var biz1Distance = vo.biz1Distance?(vo.biz1Distance/1000).toFixed(2):0;
		var biz2Distance = vo.biz2Distance?(vo.biz2Distance/1000).toFixed(2):0;
		var biz3Distance = vo.biz3Distance?(vo.biz3Distance/1000).toFixed(2):0;
		var bizDistance = (parseFloat(biz1Distance)+parseFloat(biz2Distance)+parseFloat(biz3Distance)).toFixed(2);
		bizDistanceTot += vo.biz1Distance?vo.biz1Distance:0+vo.biz2Distance?vo.biz2Distance:0;
		
		var biz1Fco = vo.biz1Fco?Math.round(parseInt(vo.biz1Fco)/10)/100:0;
		var biz2Fco = vo.biz2Fco?Math.round(parseInt(vo.biz2Fco)/10)/100:0;
		var biz3Fco = vo.biz3Fco?Math.round(parseInt(vo.biz3Fco)/10)/100:0;
		
		var bizFco = biz1Fco+biz2Fco+biz3Fco;
		var biz1Price = vo.biz1Price?parseInt(vo.biz1Price):0;
		var biz2Price = vo.biz2Price?parseInt(vo.biz2Price):0;
		var biz3Price = vo.biz3Price?parseInt(vo.biz3Price):0;
		var bizPrice = biz1Price+biz2Price+biz3Price;
		drivingTime += vo.drivingTime?parseInt(vo.drivingTime):0;
		bizTime += vo.biz?parseInt(vo.biz):0;
		bizCnt[0] += vo.bizCnt1?vo.bizCnt1:0;
		bizCnt[1] += vo.bizCnt2?vo.bizCnt2:0;
		bizCnt[2] += vo.bizCnt3?vo.bizCnt3:0;
		
		tableHtml += '<tr>';
		tableHtml += '<td rowspan="4" class="row_tit">';
		tableHtml += '<b>'+(vo.userNm?vo.userNm:'')+'</b><br />';
		tableHtml += '(<fmt:message key='personalReport.txt2'/> '+((vo.biz?vo.biz:0)/(vo.drivingTime?vo.drivingTime:1)*100).toFixed(1)+'%)';
		tableHtml += '</td>';
		tableHtml += '<td class="first bg_f7">';
		tableHtml += '<b><fmt:message key='personalReport.totSum'/></b>';
		tableHtml += '</td>';
		tableHtml += '<td class="bg_f7">';
		tableHtml += '<div class="bslist">';
		tableHtml += '<span class="wid30"><b>'+number_format(bizDistance)+' km</b></span>';
		tableHtml += '<span class="wid30"><b>'+number_format(bizFco)+' ℓ</b></span>';
		tableHtml += '<span class="wid30"><b>'+number_format(bizPrice)+' <fmt:message key='personalReport.won3'/></b></span>';
		tableHtml += '</div>';
		tableHtml += '</td>';
		tableHtml += '</tr>';
		tableHtml += '<tr>';
		tableHtml += '<td class="first">';
		tableHtml += '<fmt:message key='personalReport.drivingType_1'/>';	
		tableHtml += '</td>';
		tableHtml += '<td>';
		tableHtml += '<div class="bslist">';
		tableHtml += '<span class="wid30"><b>'+number_format(biz1Distance)+' km</b></span>';
		tableHtml += '<span class="wid30"><b>'+number_format(biz1Fco)+' ℓ</b></span>';
		tableHtml += '<span class="wid30"><b>'+number_format(biz1Price)+' <fmt:message key='personalReport.won4'/></b></span>';
		tableHtml += '</div>';
		tableHtml += '</td>';
		tableHtml += '</tr>';
		tableHtml += '<tr>';
		tableHtml += '<td class="first">';
		tableHtml += '<fmt:message key='personalReport.drivingType_2'/>';
		tableHtml += '</td>';
		tableHtml += '<td>';
		tableHtml += '<div class="bslist">';
		tableHtml += '<span class="wid30"><b>'+number_format(biz2Distance)+' km</b></span>';
		tableHtml += '<span class="wid30"><b>'+number_format(biz2Fco)+' ℓ</b></span>';
		tableHtml += '<span class="wid30"><b>'+number_format(biz2Price)+' <fmt:message key='personalReport.won5'/></b></span>';
		tableHtml += '</div>';
		tableHtml += '</td>';
		tableHtml += '</tr>';
		tableHtml += '<tr>';
		tableHtml += '<td class="first">';
		tableHtml += '<fmt:message key='personalReport.drivingType_3'/>';
		tableHtml += '</td>';
		tableHtml += '<td>';
		tableHtml += '<div class="bslist">';
		tableHtml += '<span class="wid30"><b>'+number_format(biz3Distance)+' km</b></span>';
		tableHtml += '<span class="wid30"><b>'+number_format(biz3Fco)+' ℓ</b></span>';
		tableHtml += '<span class="wid30"><b>'+number_format(biz3Price)+' <fmt:message key='personalReport.won6'/></b></span>';
		tableHtml += '</div>';
		tableHtml += '</td>';
		tableHtml += '</tr>';
	}
	$(".accountTripSummaryTable tbody").empty();
	$(".accountTripSummaryTable tbody").append(tableHtml);
	
	var rate = bizTime / drivingTime * 100;
	var quaterRate = bizTripSummaryQuater.biz / bizTripSummaryQuater.drivingTime * 100;
	$(".useWorkRate").text(""+rate.toFixed(1)+"%");
	$(".quater").text(""+bizTripSummaryQuater.quarter+"<fmt:message key="personalReport.quater"/>");
	$(".corpTax").text(""+(((bizDistanceTot/1000)  /  (bizTripSummaryQuater.biz/1000) * 100) * (rate / 100)).toFixed(1)+"%");
	
	$(".bizCnt1").text(""+bizCnt[0]+"<fmt:message key="personalReport.count7"/>");
	$(".bizCnt2").text(""+bizCnt[1]+"<fmt:message key="personalReport.count8"/>");
	$(".bizCnt3").text(""+bizCnt[2]+"<fmt:message key="personalReport.count9"/>");
	$(".bizRate").text(""+rate.toFixed(1)+"%");
}

function chart6Area(expenseSummary, expenseSummaryPrev, expenseSummaryAccount, expenseSummaryAccountPrev){
	var price = new Array(expenseSummary.length);
	var jsonPriceArray = new Array();
	var priceTemp;
	var maximum = 0;
	var currentPrice=[0,0,0,0,0,0,0,0];
	var totalExpense = 0;
	
	for(var i = 0;i < expenseSummary.length;i++){
		priceTemp = new Object();
		price[i] = expenseSummary[i].priceAll?expenseSummary[i].priceAll:null;
		if(maximum < price[i]) maximum = price[i];
		
		priceTemp.y = price[i];
		if(expenseSummary[i].selFlag == "1"){
			priceTemp.color = "red";
			currentPrice[0] = expenseSummary[i].price1?expenseSummary[i].price1:0;	// 주유
			currentPrice[1] = expenseSummary[i].price2?expenseSummary[i].price2:0;	// 정비
			currentPrice[2] = expenseSummary[i].price3?expenseSummary[i].price3:0;	// 주차
			currentPrice[3] = expenseSummary[i].price4?expenseSummary[i].price4:0;	// 통행료
			currentPrice[4] = expenseSummary[i].price5?expenseSummary[i].price5:0;	// 렌탈료
			currentPrice[5] = expenseSummary[i].price6?expenseSummary[i].price6:0;	// 세차
			currentPrice[6] = expenseSummary[i].price7?expenseSummary[i].price7:0;	// 과태료
			currentPrice[7] = expenseSummary[i].price8?expenseSummary[i].price8:0;	// 기타
			totalExpense = expenseSummary[i].priceAll?expenseSummary[i].priceAll:0;
		}
		jsonPriceArray.push(priceTemp);
	}
	
	var chart6 = new CanvasJS.Chart("chart6", {
		width:806,
		height:200,
		axisY: {
			labelFormatter:function(e){
				return CanvasJS.formatNumber(e.value/10000, "#,##0")+"만" 
			}
			,labelFontSize: 12
			,labelFontWeight : "lighter"
			,labelFontColor: "black"
			//,interlacedColor: "#F0F8FF" 
			,lineThickness: 0
			,tickLength: 0
			,gridThickness: 1
		},
		axisX: {
			interval: 1
			,labelFormatter:function(e){				
				return (e.value+1)+"<fmt:message key="personalReport.month3"/>";		
				
			}
			,labelFontSize: 12
			,labelFontWeight : "lighter"
			,labelFontColor: "black"
			,lineThickness: 1
			,tickLength: 5
		},
		
		toolTip : {
			fontStyle : "normal"
			,fontSize : 14
			,fontWeight : "normal"
			,fontFamily : "sans-serif"
			,contentFormatter: function(e){
		          var val = e.entries[0].dataPoint.y;
		          var str = CanvasJS.formatNumber(val, "#,###")+"<fmt:message key="personalReport.won7"/>";
		          return str; 
		    }

		},
		data: [{
			type: "line"
			,markerSize: 12
			,lineColor : "#cccdc9"
			,color : "#cccdc9"
			,dataPoints: []
		}]
	});
	
	for(var i = 0;i < jsonPriceArray.length;i++){
		chart6.options.data[0].dataPoints.push(jsonPriceArray[i]);
	}
	chart6.render();

	
	var priceHtml = '';
	var priceAll = expenseSummaryPrev?expenseSummaryPrev.priceAll:0;
	var price1 = expenseSummaryPrev?expenseSummaryPrev.price1:0;
	var price2 = expenseSummaryPrev?expenseSummaryPrev.price2:0;
	var price3 = expenseSummaryPrev?expenseSummaryPrev.price3:0;
	var price4 = expenseSummaryPrev?expenseSummaryPrev.price4:0;
	var price5 = expenseSummaryPrev?expenseSummaryPrev.price5:0;
	var price6 = expenseSummaryPrev?expenseSummaryPrev.price6:0;
	var price7 = expenseSummaryPrev?expenseSummaryPrev.price7:0;
	var price8 = expenseSummaryPrev?expenseSummaryPrev.price8:0;
	
	priceHtml = ''+number_format(currentPrice[0])+'<fmt:message key='personalReport.won8'/>';
	if(currentPrice[0] > price1){
		priceHtml += '<span>('+number_format(currentPrice[0] - price1)+'<fmt:message key='personalReport.won9'/> <i class="icon up"></i> )</span>';
	}
	else if(currentPrice[0] < price1){
		priceHtml += '<span>('+number_format(price1 - currentPrice[0])+'<fmt:message key='personalReport.won10'/> <i class="icon down"></i> )</span>';
	}
	else{
		priceHtml += '<span>(0<fmt:message key='personalReport.won11'/> <i class="icon none">-</i> )</span>';
	}
	$(".refuel").html(priceHtml);
	
	priceHtml = ''+number_format(currentPrice[1])+'<fmt:message key='personalReport.won12'/>';
	if(currentPrice[1] > price2){
		priceHtml += '<span>('+number_format(currentPrice[1] - price2)+'<fmt:message key='personalReport.won13'/> <i class="icon up"></i> )</span>';
	}
	else if(currentPrice[1] < price2){
		priceHtml += '<span>('+number_format(price2 - currentPrice[1])+'<fmt:message key='personalReport.won14'/> <i class="icon down"></i> )</span>';
	}
	else{
		priceHtml += '<span>(0<fmt:message key='personalReport.won15'/> <i class="icon none">-</i> )</span>';
	}
	$(".clinic").html(priceHtml);
	
	priceHtml = ''+number_format(currentPrice[2])+'<fmt:message key='personalReport.won16'/>';
	if(currentPrice[2] > price3){
		priceHtml += '<span>('+number_format(currentPrice[2] - price3)+'<fmt:message key='personalReport.won17'/> <i class="icon up"></i> )</span>';
	}
	else if(currentPrice[2] < price3){
		priceHtml += '<span>('+number_format(price3 - currentPrice[2])+'<fmt:message key='personalReport.won18'/> <i class="icon down"></i> )</span>';
	}
	else{
		priceHtml += '<span>(0<fmt:message key='personalReport.won19'/> <i class="icon none">-</i> )</span>';
	}
	$(".park").html(priceHtml);
	
	priceHtml = ''+number_format(currentPrice[3])+'<fmt:message key='personalReport.won20'/>';
	if(currentPrice[3] > price4){
		priceHtml += '<span>('+number_format(currentPrice[3] - price4)+'<fmt:message key='personalReport.won21'/> <i class="icon up"></i> )</span>';
	}
	else if(currentPrice[3] < price4){
		priceHtml += '<span>('+number_format(price4 - currentPrice[3])+'<fmt:message key='personalReport.won22'/> <i class="icon down"></i> )</span>';
	}
	else{
		priceHtml += '<span>(0<fmt:message key='personalReport.won23'/> <i class="icon none">-</i> )</span>';
	}
	$(".toll").html(priceHtml);
	
	priceHtml = ''+number_format(currentPrice[4])+'<fmt:message key='personalReport.won24'/>';
	if(currentPrice[4] > price5){
		priceHtml += '<span>('+number_format(currentPrice[4] - price5)+'<fmt:message key='personalReport.won25'/> <i class="icon up"></i> )</span>';
	}
	else if(currentPrice[4] < price5){
		priceHtml += '<span>('+number_format(price5 - currentPrice[4])+'<fmt:message key='personalReport.won26'/> <i class="icon down"></i> )</span>';
	}
	else{
		priceHtml += '<span>(0<fmt:message key='personalReport.won27'/> <i class="icon none">-</i> )</span>';
	}
	$(".rental").html(priceHtml);
	
	priceHtml = ''+number_format(currentPrice[5])+'<fmt:message key='personalReport.won28'/>';
	if(currentPrice[5] > price6){
		priceHtml += '<span>('+number_format(currentPrice[5] - price6)+'<fmt:message key='personalReport.won29'/> <i class="icon up"></i> )</span>';
	}
	else if(currentPrice[5] < price6){
		priceHtml += '<span>('+number_format(price6 - currentPrice[5])+'<fmt:message key='personalReport.won30'/> <i class="icon down"></i> )</span>';
	}
	else{
		priceHtml += '<span>(0<fmt:message key='personalReport.won31'/> <i class="icon none">-</i> )</span>';
	}
	$(".wash").html(priceHtml);
	
	priceHtml = ''+number_format(currentPrice[6])+'<fmt:message key='personalReport.won32'/>';
	if(currentPrice[6] > price7){
		priceHtml += '<span>('+number_format(currentPrice[6] - price7)+'<fmt:message key='personalReport.won33'/> <i class="icon up"></i> )</span>';
	}
	else if(currentPrice[6] < price7){
		priceHtml += '<span>('+number_format(price7 - currentPrice[6])+'<fmt:message key='personalReport.won34'/> <i class="icon down"></i> )</span>';
	}
	else{
		priceHtml += '<span>(0<fmt:message key='personalReport.won35'/> <i class="icon none">-</i> )</span>';
	}
	$(".penalty").html(priceHtml);
	
	priceHtml = ''+number_format(currentPrice[7])+'<fmt:message key='personalReport.won36'/>';
	if(currentPrice[7] > price8){
		priceHtml += '<span>('+number_format(currentPrice[7] - price8)+'<fmt:message key='personalReport.won37'/> <i class="icon up"></i> )</span>';
	}
	else if(currentPrice[7] < price8){
		priceHtml += '<span>('+number_format(price8 - currentPrice[7])+'<fmt:message key='personalReport.won38'/> <i class="icon down"></i> )</span>';
	}
	else{
		priceHtml += '<span>(0<fmt:message key='personalReport.won39'/> <i class="icon none">-</i> )</span>';
	}
	$(".etc").html(priceHtml);

	var totalHtml = '';
	for(var i = 0;i < expenseSummaryAccount.length;i++){
		var vo = expenseSummaryAccount[i];
		var voPrev = expenseSummaryAccountPrev[i];
		var currentAccountPrice = vo.priceAll?vo.priceAll:0;
		var prevAccountPrice = voPrev.priceAll?voPrev.priceAll:0;
		if(currentAccountPrice != 0 || prevAccountPrice != 0)
		{
		totalHtml += '<tr>';
		totalHtml += '<td class="row_tit">'+(vo.userNm?vo.userNm:'')+'</td>';
		totalHtml += '<td>';
		totalHtml += ''+number_format(currentAccountPrice)+'<fmt:message key='personalReport.won40'/> ';
		if(currentAccountPrice > prevAccountPrice)
			totalHtml += '<span>('+number_format(currentAccountPrice - prevAccountPrice)+'<fmt:message key='personalReport.won41'/> <i class="icon up"></i> )</span>';
		else if(currentAccountPrice < prevAccountPrice)
			totalHtml += '<span>('+number_format(prevAccountPrice - currentAccountPrice)+'<fmt:message key='personalReport.won42'/> <i class="icon down"></i> )</span>';
		else totalHtml += '<span>(0<fmt:message key='personalReport.won43'/> <i class="icon none">-</i> )</span>';
		totalHtml += '</td>';
		totalHtml += '</tr>';
		}
	}
	
	$(".expenseTotalTable tbody").empty();
	$(".expenseTotalTable tbody").append(totalHtml);

	var ratio = ((totalExpense - priceAll) / totalExpense * 100).toFixed(1);
	if(!isFinite(ratio)) ratio = 0;
	
	$(".totalExpense").text(""+number_format(totalExpense)+"<fmt:message key="personalReport.won44"/>");
	if(totalExpense > priceAll)
		$(".compareExpenseRate").text("<fmt:message key="personalReport.compareExpenseRate"/> "+ratio+"%("+number_format(totalExpense - priceAll)+"<fmt:message key="personalReport.won45"/>) <fmt:message key="personalReport.compareExpenseRateUp"/>");
	else if(totalExpense < priceAll)
		$(".compareExpenseRate").text("<fmt:message key="personalReport.compareExpenseRate2"/> "+ratio+"%("+number_format(totalExpense - priceAll)+"<fmt:message key="personalReport.won46"/>) <fmt:message key="personalReport.compareExpenseRateDown"/>");
	else $(".compareExpenseRate").text("<fmt:message key="personalReport.txt4"/>");
	
}

function chartSix(jsonPriceArray){	
	var chart6 = new CanvasJS.Chart("chart6", {
		width:806,
		height:200,
		axisY: {
			labelFormatter:function(e){
				return CanvasJS.formatNumber(e.value/10000, "#,##0")+"<fmt:message key="personalReport.txt5"/>" 
			}
			,labelFontSize: 12
			,labelFontWeight : "lighter"
			,labelFontColor: "black"
			//,interlacedColor: "#F0F8FF" 
			,lineThickness: 0
			,tickLength: 0
			,gridThickness: 1
		},
		axisX: {
			interval: 1
			,labelFormatter:function(e){				
				return (e.value+1)+"<fmt:message key="personalReport.month4"/>";		
				
			}
			,labelFontSize: 12
			,labelFontWeight : "lighter"
			,labelFontColor: "black"
			,lineThickness: 1
			,tickLength: 5
		},
		
		toolTip : {
			fontStyle : "normal"
			,fontSize : 14
			,fontWeight : "normal"
			,fontFamily : "sans-serif"
			,contentFormatter: function(e){
		          var val = e.entries[0].dataPoint.y;
		          var str = CanvasJS.formatNumber(val, "#,###")+"<fmt:message key="personalReport.won47"/>";
		          return str; 
		    }

		},
		data: [{
			type: "line"
			,markerSize: 12
			,lineColor : "#cccdc9"
			,color : "#cccdc9"
			,dataPoints: []
		}]
	});
	
	for(var i = 0;i < jsonPriceArray.length;i++){
		chart6.options.data[0].dataPoints.push(jsonPriceArray[i]);
	}
	chart6.render();
}
</script>
<form id="searchFrm" style="display:none">
	<input type="hidden" name="startDate" value="" />
	<input type="hidden" name="searchLocation" value="" />
	<input type="hidden" name="searchLocationDetail" value="" />
	<input type="hidden" name="searchText" value="" />
	<input type="hidden" name="searchOrder" value="" />
</form>
<div class="right_layout">
	<div class="auth user contents">
		<h2 class="tit_sub noneHide">
			<fmt:message key='personalReport.txt6'/>
			<p><fmt:message key='personalReport.txt6_1'/></p>
		</h2>
		<div class="search_box1 search_type mgb0 noneHide">
			<div class="float_left">
				<input type="text" id="searchMonth" class="date datePickerMonth" style="width:113px; height:36px;"/>
				<select class="select form" id="searchVehicle" style="width:126px;">
					<option value="">12가3456</option>
				</select>
				<select class="select form" id="searchUser" style="width:126px;">
					<option value=""><fmt:message key='personalReport.txt7'/></option>
				</select>
				<a href="#none" class="btn_search"><fmt:message key='personalReport.search'/></a>
			</div>
			<div class="float_right">
				<a href="#none" class="btn_black"><fmt:message key='personalReport.reportPrint'/></a>
			</div>
		</div>

		<!-- 운행정보 -->
		<h3 class="tit01 mgt30"><fmt:message key='personalReport.avgDrivingInfo'/><i></i></h3>
		<div class="tbl_layout mgt20">
			<div class="row">
				<div class="col" style="width:310px;">
					<div id="myChart01" class="myChart">
						<div class="level">A <i class="icon up"></i> </div>
					</div>
					<div class="chart_explain">
						<table class="tbl tbl_explain">
							<colgroup>
								<col style="width:95px;"/>
								<col />
							</colgroup>
							<tr>
								<td><span class="color purple"></span><fmt:message key='personalReport.fuelEfficiency2'/></td>
								<td><span id="fuelRank"></span></td>
							</tr>
							<tr>
								<td><span class="color pink"></span><fmt:message key='personalReport.safeScore2'/></td>
								<td><span id="safeRank"></span></td>
							</tr>
							<tr>
								<td><span class="color blue"></span><fmt:message key='personalReport.ecoScroe2'/></td>
								<td><span id="ecoRank"></span></td>
							</tr>
						</table>
					</div>
				</div>
				<div class="col" style="width:430px; float:right;">
					<table class="tbl tbl_type02">
						<colgroup>
							<col style="width:125px;" />
							<col />
						</colgroup>
						<tr>
							<th scope="row"><fmt:message key='personalReport.distance'/></th>
							<td><span id="distance"></span></td>
						</tr>
						<tr>
							<th scope="row"><fmt:message key='personalReport.drivingTime'/></th>
							<td><span id="drivingTime"></span></td>
						</tr>
						<tr>
							<th scope="row"><fmt:message key='personalReport.fco'/></th>
							<td><span id="fco"></span></td>
						</tr>
						<tr>
							<th scope="row"><fmt:message key='personalReport.oilPrice'/></th>
							<td><span id="oilPrice"></span><br/>(<fmt:message key='personalReport.txt8'/>)</td>
						</tr>
					</table>
				</div>
			</div>
			<!-- 806x157 -->
			<div id="chart1" style="width:806px;height: 157px;"></div>
			<div class="graph_txt1">
				<span>
					<strong><fmt:message key='personalReport.avgFco'/></strong>
					<em id="avgFco">0 km/L</em>
				</span>
				<span>
					<strong><fmt:message key='personalReport.comFco'/></strong>
					<em id="comFco">0 km/L</em>
				</span>
			</div>
			<div class="map_txt1">
				<div>
					<span class="w109">
						<img src="${pathCommon}/images/m_img1.png" alt=""><fmt:message key='personalReport.rapidStart'/> <strong class="rapidStart"><fmt:message key='personalReport.zeroCount7'/></strong>
					</span><span class="w120">
						<img src="${pathCommon}/images/m_img2.png" alt=""><fmt:message key='personalReport.rapidStop'/> <strong class="rapidStop"><fmt:message key='personalReport.zeroCount8'/></strong>
					</span>
					<span class="w120">
						<img src="${pathCommon}/images/m_img4.png" alt=""><fmt:message key='personalReport.rapidAccel'/> <strong class="rapidAccel"><fmt:message key='personalReport.zeroCount9'/></strong>
					</span>
					<span class="w109">
						<img src="${pathCommon}/images/m_img5.png" alt=""><fmt:message key='personalReport.rapidDeaccel'/> <strong class="rapidDeaccel"><fmt:message key='personalReport.zeroCount10'/></strong>
					</span>
					<span class="w120">
						<img src="${pathCommon}/images/m_img10.png" alt=""><fmt:message key='personalReport.rapidTurn'/> <strong class="rapidTurn"><fmt:message key='personalReport.zeroCount11'/></strong>
					</span>
				</div>
				<div>
					<span class="w109">
						<img src="${pathCommon}/images/m_img11.png" alt=""><fmt:message key='personalReport.rapidUtern'/> <strong class="rapidUtern"><fmt:message key='personalReport.zeroCount12'/></strong>
					</span>
					<span class="w109">
						<img src="${pathCommon}/images/m_img6.png" alt=""><fmt:message key='personalReport.overSpeed'/> <strong class="overSpeed"><fmt:message key='personalReport.zeroCount13'/></strong>
					</span>
					<span class="w120">
						<img src="${pathCommon}/images/m_img7.png" alt=""><fmt:message key='personalReport.overSpeedLong'/> <strong class="longTerm"><fmt:message key='personalReport.zeroCount14'/></strong>
					</span>
					<%-- <span class="w178">
						<img src="${pathCommon}/images/m_img12.png" alt="">심야운전 <strong class="nightDrive">0회</strong>
					</span> --%>
				</div>
				<%-- <div>
					<span class="w178">
						<img src="${pathCommon}/images/m_img9.png" alt="">사고다발지역통과 <strong class="no_num">0회</strong>
					</span>
				</div> --%>
			</div>
		</div>
		<!-- 운행정보 끝 -->
	
		
		<div class="mode business">
			<!-- 사용 목적 [기간별] -->
			<div class="gray_bg_box mgt80">
				<span><fmt:message key='personalReport.vehicleDrivingType1'/> <em class="bizCnt1">xxx회</em></span>
				<span><fmt:message key='personalReport.vehicleDrivingType2'/> <em class="bizCnt2">xx회</em></span>
				<span><fmt:message key='personalReport.vehicleDrivingType3'/> <em class="bizCnt3">xx회</em></span>
				<span><fmt:message key='personalReport.vehicleDrivingType4'/> <em class="bizRate">xx%</em></span>
			</div>
			<div class="bxtit mgt30">
				<h3 class="tit01"><fmt:message key='personalReport.txt9'/><i></i></h3>
				<%-- <ul class="bx_icons">
					<li>
						<img src="${pathCommon}/images/main_icon9.jpg" alt="" />
						업무용
					</li>
					<li>
						<img src="${pathCommon}/images/main_icon8.jpg" alt="" />
						출퇴근용
					</li>
					<li>
						<img src="${pathCommon}/images/main_icon7.jpg" alt="" />
						비업무용
					</li>
				</ul> --%>
			</div>
			<div id="chart4" style="width:806px;height:200px;"></div>
			<!-- 사용 목적 [기간별] 끝 -->
			
			<!-- 사용 목적 [시간대별] -->
			<div class="bxtit mgt30">
				<h3 class="tit01"><fmt:message key='personalReport.txt10'/><i></i></h3>
				<%-- <ul class="bx_icons">
					<li>
						<img src="${pathCommon}/images/main_icon9.jpg" alt="" />
						업무용
					</li>
					<li>
						<img src="${pathCommon}/images/main_icon8.jpg" alt="" />
						출퇴근용
					</li>
					<li>
						<img src="${pathCommon}/images/main_icon7.jpg" alt="" />
						비업무용
					</li>
				</ul> --%>
			</div>
			<div id="chart5" style="width:806px;height:200px;"></div>
			<table class="table1 bs_table mgt20 accountTripSummaryTable">
				<colgroup>
					<col style="width:177px;" />
					<col style="width:177px;" />
					<col  />
				</colgroup>
				<thead>
					<tr>
						<th><fmt:message key='personalReport.user'/></th>
						<th><fmt:message key='personalReport.item'/></th>
						<th><fmt:message key='personalReport.content'/></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
			<ul class="bn_p">
				<li><fmt:message key='personalReport.txt11'/> <em class="color_red useWorkRate">xx%</em><fmt:message key='personalReport.txt11_1'/> <em class="quater">x분기</em> <fmt:message key='personalReport.txt11_2'/> <em class="color_red corpTax">xx%</em><fmt:message key='personalReport.txt11_3'/></li>
			</ul>
			<!-- 사용 목적 [시간대별] 끝 -->
	
			<!-- 평균 운영비 -->
			<div class="gray_bg_box mgt80">
				<span><fmt:message key='personalReport.txt12'/> <em class="totalExpense">xxx,xxx,000원</em></span>
			</div>
			<h3 class="tit01 mgt30"><fmt:message key='personalReport.txt13'/><i></i></h3>
			<div id="chart6" style="width:806px;height:200px;"></div>
			
			<div class="tbl_layout">
				<div class="row">
					<div class="col col-2">
						<table class="table1 table1_2 tbl_count" style="width:393px; float:left;">
							<colgroup>
								<col style="width:120px;"/>
								<col />
							</colgroup>
							<thead>
								<tr>
									<th scope="col"><fmt:message key='personalReport.item2'/></th>
									<th scope="col"><fmt:message key='personalReport.txt14'/></th>
								</tr>
							</thead>
							<tr>
								<td class="bg_bar"><fmt:message key='personalReport.refuel'/></td>
								<td class="refuel">
									<fmt:message key='personalReport.zeroWon'/>
									<span>(<fmt:message key='personalReport.zeroWon2'/> <i class="icon none">-</i> )</span>
								</td>
							</tr>
							<tr>
								<td class="bg_bar"><fmt:message key='personalReport.park'/></td>
								<td class="park">
									<fmt:message key='personalReport.zeroWon3'/>
									<span>(<fmt:message key='personalReport.zeroWon4'/> <i class="icon none">-</i> )</span>
								</td>
							</tr>
							<tr>
								<td class="bg_bar"><fmt:message key='personalReport.toll'/></td>
								<td class="toll">
									<fmt:message key='personalReport.zeroWon5'/>
									<span>(<fmt:message key='personalReport.zeroWon6'/> <i class="icon none">-</i> )</span>
								</td>
							</tr>
							<tr>
								<td class="bg_bar"><fmt:message key='personalReport.rent'/></td>
								<td class="rental">
									<fmt:message key='personalReport.zeroWon7'/>
									<span>(<fmt:message key='personalReport.zeroWon8'/> <i class="icon none">-</i> )</span>
								</td>
							</tr>
						</table>
					</div>
					<div class="col col-2">
						<table class="table1 table1_2 tbl_count" style="width:393px; float:right;">
							<colgroup>
								<col style="width:120px;"/>
								<col />
							</colgroup>
							<thead>
								<tr>
									<th scope="col"><fmt:message key='personalReport.item3'/></th>
									<th scope="col"><fmt:message key='personalReport.txt14_1'/></th>
								</tr>
							</thead>
							<tr>
								<td class="bg_bar"><fmt:message key='personalReport.wash'/></td>
								<td class="wash">
									<fmt:message key='personalReport.zeroWon9'/>
									<span>(<fmt:message key='personalReport.zeroWon10'/> <i class="icon none">-</i> )</span>
								</td>
							</tr>
							<tr>
								<td class="bg_bar"><fmt:message key='personalReport.clinic'/></td>
								<td class="clinic">
									<fmt:message key='personalReport.zeroWon11'/>
									<span>(<fmt:message key='personalReport.zeroWon12'/> <i class="icon none">-</i> )</span>
								</td>
							</tr>
							<tr>
								<td class="bg_bar"><fmt:message key='personalReport.penalty'/></td>
								<td class="penalty">
									<fmt:message key='personalReport.zeroWon13'/>
									<span>(<fmt:message key='personalReport.zeroWon14'/> <i class="icon none">-</i> )</span>
								</td>
							</tr>
							<tr>
								<td class="bg_bar"><fmt:message key='personalReport.etc'/></td>
								<td class="etc">
									<fmt:message key='personalReport.zeroWon15'/>
									<span>(<fmt:message key='personalReport.zeroWon16'/> <i class="icon none">-</i> )</span>
								</td>
							</tr>
						</table>
						
					</div>
					
				</div>
				<table class="table1 tbl_count bs_table mgt20 expenseTotalTable">
					<colgroup>
						<col style="width:177px;" />
						<col  />
					</colgroup>
					<thead>
						<tr>
							<th><fmt:message key='personalReport.user2'/></th>
							<th><fmt:message key='personalReport.totalSum'/></th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				<ul class="bn_p">
					<li><fmt:message key='personalReport.txt15'/> <em class="color_red totalExpense">xxx,000원</em><fmt:message key='personalReport.txt15_1'/> <em class="color_red compareExpenseRate"><fmt:message key='personalReport.txt15_2'/></em><fmt:message key='personalReport.txt15_3'/></li>
				</ul>
			</div>
			<!-- 평균 운영비 끝 -->
		</div> 
		<%--
		<div class="mode general" style="display:none">
			<!-- 운행 수 [시간대별] -->
			<div class="gray_bg_box mgt80">
				<span>총 운행 <em>xx회</em></span>
			</div>
			<h3 class="tit01 mgt30">운행 수 [시간대별]<i></i></h3>
			<div class="tbl_layout">
			</div>
			<!-- 운행 수 [시간대별] 끝 -->
			<!-- 평균 지출비 -->
			<div class="gray_bg_box mgt80">
				<span>총 지출비 <em>xxx,xxx원</em></span>
			</div>
			<h3 class="tit01 mgt30">평균 지출비<i></i></h3>
			<div class="tbl_layout">
				<div class="row mgt20">
					<div class="col col-2">
						<table class="table1 table1_2 tbl_count" style="width:393px; float:left;">
							<colgroup>
								<col style="width:120px;"/>
								<col />
							</colgroup>
							<thead>
								<tr>
									<th scope="col">항목</th>
									<th scope="col">총 비용(전월대비)</th>
								</tr>
							</thead>
							<tr>
								<td class="bg_bar">주유</td>
								<td>
									xx,000원
									<span>(xxx,000원 <i class="icon down"></i> )</span>
								</td>
							</tr>
							<tr>
								<td class="bg_bar">주차</td>
								<td>
									xx,000원
									<span>(xxx,000원 <i class="icon up"></i> )</span>
								</td>
							</tr>
							<tr>
								<td class="bg_bar">통행료</td>
								<td>
									xx,000원
									<span>(xxx,000원 <i class="icon none">-</i> )</span>
								</td>
							</tr>
							<tr>
								<td class="bg_bar">렌탈료</td>
								<td>
									xx,000원
									<span>(xxx,000원 <i class="icon up"></i> )</span>
								</td>
							</tr>
						</table>
					</div>
					<div class="col col-2">
						<table class="table1 table1_2 tbl_count" style="width:393px; float:right;">
							<colgroup>
								<col style="width:120px;"/>
								<col />
							</colgroup>
							<thead>
								<tr>
									<th scope="col">항목</th>
									<th scope="col">총 비용(전월대비)</th>
								</tr>
							</thead>
							<tr>
								<td class="bg_bar">세차</td>
								<td>
									xx,000원
									<span>(xxx,000원 <i class="icon up"></i> )</span>
								</td>
							</tr>
							<tr>
								<td class="bg_bar">정비</td>
								<td>
									xx,000원
									<span>(xxx,000원 <i class="icon up"></i> )</span>
								</td>
							</tr>
							<tr>
								<td class="bg_bar">과태료</td>
								<td>
									xx,000원
									<span>(xxx,000원 <i class="icon up"></i> )</span>
								</td>
							</tr>
							<tr>
								<td class="bg_bar">기타</td>
								<td>
									xx,000원
									<span>(xxx,000원 <i class="icon up"></i> )</span>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<ul class="bn_p">
				<li>ㆍ이번달 전체 운영비는 <em class="color_red">xxx,000원</em>이며, 전월 대비 <em class="color_red">xx%(xxx,000원) 상승</em>하였습니다.</li>
			</ul>
			</div>
			<!-- 평균 지출비 끝 -->
			<!-- 차량 소모품 -->
			<div class="gray_bg_box mgt80">
				<span>총 지출비 <em>xx,xx원</em></span>
			</div>
			<h3 class="tit01 mgt30">차량 소모품<i></i></h3>
			<div class="bn_box mgt20">
				<span class="bn_left">
					<strong class="bg_green"></strong>
					<em class="color_green">여유</em>
				</span>
				<span class="bn_left">
					<strong class="bg_red"></strong>
					<em class="color_red">교환필요</em>
				</span>
				<p class="bn_right">(교체주기 30일 미만인 경우)</p>
			</div>
			<!-- 차량 소모품 끝 -->
			<!-- 차량진단 항목 -->
			<div class="gray_bg_box mgt80">
				<span>현재 점검필요 <em>x건</em></span>
				<span>긴급 <em>x건</em></span>
			</div>
			<h3 class="tit01 mgt30">차량진단 항목<i></i></h3>
			<table class="table1 bs_table">
				<colgroup>
					<col style="width:127px;" />
					<col style="width:230px;" />
					<col  />
				</colgroup>
				<thead>
					<tr>
						<th>점검 일시</th>
						<th colspan="2">상세 내용</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="row_tit" rowspan="4"><b>20xx-xx-xx<br />xx:00</b></td>
						<td class="bg_f7" colspan="2"><b>고장코드 소거 <em class="color_red">x</em></b></td>
					</tr>
					<tr>
						<td class="first td_align_left">
							<span class="red_icon">점검필요</span>
							<b>[P2763] 밋션제어</b>
						</td>
						<td class="td_align_left">댐퍼 클러치 솔레노이드 밸브(SLU) 이상 - 신호값 낮음(GND 쇼트 or 단선)</td>
					</tr>
					<tr>
						<td class="first td_align_left">
							<span class="red_icon">긴급</span>
							<b>[P2763] 밋션제어</b>
						</td>
						<td class="td_align_left">댐퍼 클러치 솔레노이드 밸브(SLU) 이상 - 신호값 낮음(GND 쇼트 or 단선)</td>
					</tr>
					<tr>
						<td class="first td_align_left">
							<span class="red_icon">점검필요</span>
							<b>[P2763] 밋션제어</b>
						</td>
						<td class="td_align_left">댐퍼 클러치 솔레노이드 밸브(SLU) 이상 - 신호값 낮음(GND 쇼트 or 단선)</td>
					</tr>
					
					<tr>
						<td class="row_tit" rowspan="4"><b>20xx-xx-xx<br />xx:00</b></td>
						<td class="bg_f7" colspan="2"><b>점검필요 <em class="color_red">x</em> / 긴급 <em class="color_red">x</em></b></td>
					</tr>
					<tr>
						<td class="first td_align_left">
							<span class="red_icon">점검필요</span>
							<b>[P2763] 밋션제어</b>
						</td>
						<td class="td_align_left">댐퍼 클러치 솔레노이드 밸브(SLU) 이상 - 신호값 낮음(GND 쇼트 or 단선)</td>
					</tr>
					<tr>
						<td class="first td_align_left">
							<span class="red_icon">긴급</span>
							<b>[P2763] 밋션제어</b>
						</td>
						<td class="td_align_left">댐퍼 클러치 솔레노이드 밸브(SLU) 이상 - 신호값 낮음(GND 쇼트 or 단선)</td>
					</tr>
					<tr>
						<td class="first td_align_left">
							<span class="red_icon">점검필요</span>
							<b>[P2763] 밋션제어</b>
						</td>
						<td class="td_align_left">댐퍼 클러치 솔레노이드 밸브(SLU) 이상 - 신호값 낮음(GND 쇼트 or 단선)</td>
					</tr>
					<tr>
						<td class="row_tit" rowspan="4"><b>20xx-xx-xx xx:00</b></td>
						<td class="bg_f7" colspan="2"><b>점검필요 <em class="color_82">x</em> / 긴급 <em class="color_82">x</em></b></td>
					</tr>
				</tbody>
			</table>
			<!-- 차량진단 항목 끝 -->
		</div> 
		--%>
	</div>
</div>