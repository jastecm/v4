<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<fmt:setLocale value="${_LANG}" scope="request" />
<fmt:setBundle basename="properties.messages.rest.message" />
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script type="text/javascript">
var lazy;
var lazyUser;
var noneAuthMail = true;
var noneAuthPhone = true;

$(document).ready(function(){
	initMenuSel("M5001");


	var phone = $("#infoFrm input[name=phone]").val().split("-");
	var mobilePhone = $("#infoFrm input[name=mobilePhone]").val().split("-");
	
	<c:if test="${VDAS.rule eq '1' }">
		var corpNum = $("#infoFrm input[name=corpNum]").val().split("-");
		var corpPhone = $("#infoFrm input[name=corpPhone]").val().split("-");
		var authMail = $("#infoFrm input[name=authMail]").val().split("@");
		
		$("input[name=accountMail]").val(authMail[0]);
		$("input[name=accountMailDomain]").val(authMail[1]);
		$("#corpNumStart").val(corpNum[0]);
		$("#corpNumMid").val(corpNum[1]);
		$("#corpNumEnd").val(corpNum[2]);
		$("#corpPhoneMid").val(corpPhone[1]);
		$("#corpPhoneEnd").val(corpPhone[2]);
		$("#adminPhoneMid").val(mobilePhone[1]);
		$("#adminPhoneEnd").val(mobilePhone[2]);
		$("#emergencyPhoneMid").val(phone[1]);
		$("#emergencyPhoneEnd").val(phone[2]);

		$("input[name=accountMail]").on("change",function(){
			noneAuthMail = false;
			$("#mailAuth").show();
		});
		
		$("input[name=accountMailDomain]").on("change",function(){
			noneAuthMail = false;
			$("#mailAuth").show();
		});

		$("#selAccountMailDomain").on("change",function(){
			noneAuthMail = false;
			$("#mailAuth").show();
		});

		$("select[name=adminPhoneStart]").on("change",function(){
			noneAuthPhone = false;
			$("#phoneAuth").show();
		});
		
		$("#adminPhoneMid").on("change",function(){
			noneAuthPhone = false;
			$("#phoneAuth").show();
		});
		
		$("#adminPhoneEnd").on("change",function(){
			noneAuthPhone = false;
			$("#phoneAuth").show();
		});
		
		$("#selAccountMailDomain").on("change",function(){
			var _this = $(this).val();
			var _target = $("input[name=accountMailDomain]");
			if(_this.length == 0){
				_target.val("");
			}else{
				_target.val(_this);
			}
		})
		
		$("#mailAuth").on("click",function(){
			var _$this = $(this);
			var certMail = $("input[name=accountMail]").val()+"@"+$("input[name=accountMailDomain]").val();
			var authTime = 180;
	        var display = _$this.siblings('.code_box').find(".time");
			$VDAS.http_post("/account/mailCertReq.do",{mailCert:certMail}
			,{
				success:function(r){
					if(!_$this.data("checker")){
						_$this.data("checker","1");
						_$this.html('재전송');
						_$this.siblings('.code_box').css({'display':'inline-block'});
				        
				        mailAuthInterval = startTimer(authTime, display);	
					}else{
						clearInterval(mailAuthInterval);
				        mailAuthInterval = startTimer(authTime, display);
					}
				},loading:$("#mailAuth")
			});
		});
		
		$("#btn_mailAuthConfirm").on("click",function(){
			var _$this = $(this);
			if(!$("#mailAuthConfirm").val()){
				var certMail = $("input[name=accountMail]").val()+"@"+$("input[name=accountMailDomain]").val();
				var checker = $("#mailAuthChecker").val();
				if(checker.length > 0){ 
					
					$VDAS.http_post("/account/mailVeriReq.do",{mailCert:certMail,certNum:checker}
					,{
						success:function(r){
							var strHtml = "<div class='tip'>인증되었습니다.</div>";
							_$this.closest(".code_box").html(strHtml);
							$("#mailAuth").remove();
							$("#mailAuthWrong").remove();
							$("#mailAuthConfirm").val("1");
							noneAuthMail = true;
							$("input[name=accountMail],input[name=accountMailDomain]").prop("readonly",true);
							$("#selAccountMailDomain").prop("disabled",true);
						},error:function(r){
							$("#mailAuthWrong").remove();
							var strHtml = "<div class='tip' id='mailAuthWrong'>인증번호가 잘못되었습니다.</div>";
							$("#mailAuthConfirm").after(strHtml);
						},loading:$("#btn_mailAuthConfirm")
					});
				}else{
					$("#mailAuthWrong").remove();
					var strHtml = "<div class='tip' id='mailAuthWrong'>인증번호가 잘못되었습니다.</div>";
					$("#mailAuthConfirm").after(strHtml);
				}
			}
			
		});
		
		$("#phoneAuth").on("click",function(){
			
			var _$this = $(this);
			
			var certPhone = $("select[name=adminPhoneStart]").val()+"-"+$("#adminPhoneMid").val()+"-"+$("#adminPhoneEnd").val();
			var authTime = 180;
	        var display = _$this.siblings('.code_box').find(".time");
			$VDAS.http_post("/account/phoneCertReq.do",{phoneCert:certPhone}
			,{
				success:function(r){
					if(!_$this.data("checker")){
						_$this.data("checker","1");
						_$this.html('재전송');
						_$this.siblings('.code_box').css({'display':'inline-block'});
				        
						phoneAuthInterval = startTimer(authTime, display);	
					}else{
						clearInterval(phoneAuthInterval);
						phoneAuthInterval = startTimer(authTime, display);
					}
				},loading:$("#phoneAuth")
			});
		});
		
		$("#btn_phoneAuthConfirm").on("click",function(){
			
			var _$this = $(this);
			if(!$("#phoneAuthConfirm").val()){
				var certPhone = $("select[name=adminPhoneStart]").val()+"-"+$("#adminPhoneMid").val()+"-"+$("#adminPhoneEnd").val();
				var checker = $("#phoneAuthChecker").val();
				if(checker.length > 0){ 
					
					$VDAS.http_post("/account/phoneVeriReq.do",{phoneCert:certPhone,certNum:checker}
					,{
						success:function(r){
							var strHtml = "<div class='tip'>인증되었습니다.</div>";
							_$this.closest(".code_box").html(strHtml);
							$("#phoneAuth").remove();
							$("#phoneAuthWrong").remove();
							$("#phoneAuthConfirm").val("1");
							noneAuthPhone = true;
							$("#adminPhoneMid,#adminPhoneEnd").prop("readonly",true);
							$("select[name=adminPhoneStart]").prop("disabled",true);
						},error:function(r){
							$("#mailAuthWrong").remove();
							var strHtml = "<div class='tip' id='mailAuthWrong'>인증번호가 잘못되었습니다.</div>";
							$("#mailAuthConfirm").after(strHtml);
						},loading:$("#btn_mailAuthConfirm")
					});
				}else{
					$("#phoneAuthWrong").remove();
					var strHtml = "<div class='tip' id='phoneAuthWrong'>인증번호가 잘못되었습니다.</div>";
					$("#phoneAuthConfirm").after(strHtml);
				}
			}
		});
	</c:if>
	
	<c:if test="${VDAS.rule eq '2' }">
		$(".w38").css("padding-left","7px");
		
		$VDAS.ajaxCreateSelect("/com/getGroupCode.do",{},$("#corpGroup"),false,null,"${rtv.corpGroup}","${_LANG}",null,null);
		var accountPw = $("#infoFrm input[name=accountPw]").val().split("");

		var password = '';
		
		for(var i = 0;i < accountPw.length;i++){
			if(i < 2) password += accountPw[i];
			else password += '*';
		}
		
		$("#phoneMid").val(phone[1]);
		$("#phoneEnd").val(phone[2]);
		$("#mobilePhoneMid").val(mobilePhone[1]);
		$("#mobilePhoneEnd").val(mobilePhone[2]);
		$("#password").text(password);
		
		$('.ui_radio_box input[type=radio]').checkboxradio({
	        icon: false
	    });

		$(".ui_radio_box input[name=gender]").on("change",function(){
			$("#editFrm input[name=gender]").val($(this).val());
		});
		
		$(".ui_radio_box input[name=blood]").on("change",function(){
			$("#editFrm input[name=blood]").val($(this).val());
		});
	</c:if>

	$("body").on("click",".btn_accept",function(){
		if($("input[name=rule]").val() == '1'){
			$("#adminEditFrm input[name=corpNum]").val($("#corpNumStart").val() + "-" + $("#corpNumMid").val() + "-" + $("#corpNumEnd").val());
			$("#adminEditFrm input[name=corpPhone]").val($("select[name=corpPhoneStart]").val() + "-" + $("#corpPhoneMid").val() + "-" + $("#corpPhoneEnd").val());
			$("#adminEditFrm input[name=mobilePhone]").val($("select[name=adminPhoneStart]").val()  + "-" + $("#adminPhoneMid").val() + "-" + $("#adminPhoneEnd").val());
			$("#adminEditFrm input[name=phone]").val($("select[name=emergencyPhoneStart]").val()  + "-" + $("#emergencyPhoneMid").val() + "-" + $("#emergencyPhoneEnd").val());
			$("#adminEditFrm input[name=authMail]").val($("input[name=accountMail]").val() + "@" + $("input[name=accountMailDomain]").val());
			$("#adminEditFrm input[name=corpNm]").val($("#corpNm").val());
			$("#adminEditFrm input[name=ceoNm]").val($("#ceoNm").val());
			$("#adminEditFrm input[name=corpBusiness]").val($("#corpBusiness").val());
			$("#adminEditFrm input[name=corpItem]").val($("#corpItem").val());
			$("#adminEditFrm input[name=adminNm]").val($("#adminNm").val());
			$("#adminEditFrm input[name=groupNm]").val($("#groupNm").val());

			if(noneAuthMail&&noneAuthPhone?false:(!$("#phoneAuthConfirm").val() || !$("#mailAuthConfirm").val())){
				alert("인증이 완료되지 않았습니다.");
			}else{
				$VDAS.http_post("/user/updateUserConfig.do",$("#adminEditFrm").serialize(),{
					success:function(r){
						alert("저장되었습니다.");
						//$VDAS.instance_post("/main/index.do",{});
					}
				});
			}
		}
		else if($("input[name=rule]").val() == '2'){
			$("#editFrm input[name=phone]").val($("select[name=phoneStart]").val() + "-" + $("#phoneMid").val() + "-" + $("#phoneEnd").val());
			$("#editFrm input[name=mobilePhone]").val($("select[name=mobilePhoneStart]").val() + "-" + $("#mobilePhoneMid").val() + "-" + $("#mobilePhoneEnd").val());
			$("#editFrm input[name=office]").val($("#office").val());
			$("#editFrm input[name=accountImgKey]").val($("#accountImg").data("key"));
			$("#editFrm input[name=accountNm]").val($("#accountNm").val());
			$("#editFrm input[name=corpGroup]").val($("#corpGroup").val());
			$("#editFrm input[name=position]").val($("#position").val());
			$("#editFrm input[name=email]").val($("#email").val());
			$("#editFrm input[name=address]").val($("#address").val());
			$("#editFrm input[name=descript]").val($("#descript").val());

			$VDAS.http_post("/user/updateUserConfig.do",$("#editFrm").serialize(),{
				success:function(r){
					$VDAS.instance_post("/main/index.do",{});
				},
				error:function(r){
				}
			});
		}
		
		//$VDAS.instance_post("/main/index.do",{});
	});

	$("body").on("click",".btn_cancel",function(){
		$VDAS.instance_post("/main/index.do",{});
	});
});

function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    var objInterval = setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.html(minutes + ":" + seconds);

        if (--timer < 0) {
            clearInterval(objInterval);
        }
    }, 1000);
    
    return objInterval;
}

</script>
<input type="hidden" name="rule" value="${VDAS.rule}" />
<form id="infoFrm" style="display:none">
	<input type="hidden" name="corpNum" value="${rtv.corpNum }" />
	<input type="hidden" name="corpPhone" value="${rtv.corpPhone }" />
	<input type="hidden" name="phone" value="${rtv.phone }" />
	<input type="hidden" name="mobilePhone" value="${rtv.mobilePhone }" />
	<input type="hidden" name="authMail" value="${rtv.authMail }" />
	<input type="hidden" name="accountPw" value="${rtv.accountPw }" />
</form>
<form id="adminEditFrm" style="display:none">
	<input type="hidden" name="corpNum" value="" />
	<input type="hidden" name="corpPhone" value="" />
	<input type="hidden" name="phone" value="" />
	<input type="hidden" name="mobilePhone" value="" />
	<input type="hidden" name="authMail" value="" />
	<input type="hidden" name="corpNm" value="" />
	<input type="hidden" name="ceoNm" value="" />
	<input type="hidden" name="corpBusiness" value="" />
	<input type="hidden" name="corpItem" value="" />
	<input type="hidden" name="adminNm" value="" />
	<input type="hidden" name="groupNm" value="" />
</form>
<form id="editFrm" style="display:none">
	<input type="hidden" name="phone" value="" />
	<input type="hidden" name="mobilePhone" value="" />
	<input type="hidden" name="accountImgKey" value="" />
	<input type="hidden" name="office" value="" />
	<input type="hidden" name="accountNm" value="" />
	<input type="hidden" name="corpGroup" value="" />
	<input type="hidden" name="position" value="" />
	<input type="hidden" name="email" value="" />
	<input type="hidden" name="gender" value="${rtv.gender }" />
	<input type="hidden" name="blood" value="${rtv.blood }" />
	<input type="hidden" name="address" value="" />
	<input type="hidden" name="descript" value="" />
</form>
<div class="right_layout">
	<h2 class="tit_sub">
		회원정보 설정
		<p>서비스의 정상적인 이용을 위해 정확한 회원정보를 입력합니다.</p>
	</h2>
	<h3 class="h3tit">기본정보</h3>
	<c:if test="${VDAS.rule eq '1' }">
		<div class="member_box2">
			<ul>
				<li>
					<span class="tit">로그인 정보</span>
					<div class="m_layout">
						<div class="d1">
							<span class="tit2">아이디</span><span class="t1">${VDAS.accountId }</span>
						</div>
					</div>
					<div class="m_layout">
						<div class="d1">
							<span class="tit2">비밀번호</span><a href="${defaultPath}/user/changePassword.do" class="btn_searc_c">변경</a>
						</div>
					</div>
					<div class="m_layout">
						<div class="d1" style="width:100%;">
							<span class="tit2">보조 이메일</span>
	                        <input type="text" placeholder="" maxlength="20" style="width:125px;" name="accountMail" value="${reqVo.accountMail}" onchange="auth()"/>
	                        <span class="s2">@</span>
	                        <input type="text" placeholder="" maxlength="20" style="width:125px;" name="accountMailDomain" value="${reqVo.accountMailDomain}"/>
	                        <select class="select form" style="width:125px; margin-left: 5px;" id="selAccountMailDomain">
	                            <option value="">이메일 선택</option>
	                            <option value="gmail.com">gmail.com</option>
	                            <option value="naver.com">naver.com</option>
	                            <option value="daum.net">daum.net</option>
	                            <option value="hanmail.net">hanmail.net</option>
	                            <option value="nate.com">nate.com</option>
	                            <option value="hotmail.com">hotmail.com</option>
	                            <option value="ymail.com">ymail.com</option>
	                            <option value="rocketmail.com">rocketmail.com</option>
	                        </select>
	                        <c:if test="${empty reqVo.accountMailF}">
	                        	<a href="#none" class="btn_searc_c" id="mailAuth" style="display:none;">인증</a>
	                        	<div class="code_box" style="display:none;">
		                        	<div class="input_box" id="mailAuthBox">	                            
			                        	<input type="text" placeholder="인증번호 입력" style="width:100px;" id="mailAuthChecker"/>
			                            	<div class="time"></div>
			                        </div>
									<a href="#none" class="btn_searc_c" id="btn_mailAuthConfirm">확인</a>                        
			                    </div>
	                        </c:if>
	                        <c:if test="${not empty reqVo.accountMailF}">
	                        	<div class="code_box" style="display:inline-block">
	                        		<div class="tip">인증되었습니다.</div>
	                        	</div>
	                        </c:if>
	                        <input type="hidden" id="mailAuthConfirm" value="<c:if test="${not empty reqVo.accountMailF}">1</c:if>" />
							<div class="tip">
								* 비밀번호를 잃어버렸을 경우 사용되는 이메일입니다.
							</div>
						</div>
					</div>
				</li>
				<li>
					<span class="tit">기업 정보 <b>(필수입력)</b></span>
					<div class="m_layout">
						<div class="d1">
							<span class="tit2">기업명</span><input type="text" class="w147" id="corpNm" value="${rtv.corpNm }" />
						</div>
						<div class="d2">
							<span class="tit2">사업자등록번호</span><input type="text" class="w38" id="corpNumStart" value="123" /><span class="s1">-</span><input type="text" class="w27" id="corpNumMid" value="45" /><span class="s1">-</span><input type="text" class="w54" id="corpNumEnd" value="678910" />
						</div>
					</div>
					<div class="m_layout">
						<div class="d1">
							<span class="tit2">대표자명</span><input type="text" class="w147" id="ceoNm" value="${rtv.ceoName }" />
						</div>
						<div class="d2">
							<span class="tit2">휴대폰 번호</span>
							<select class="select form" style="width:50px;" name="corpPhoneStart">
	                        	<c:forEach var="vo" items="${phoneTitle}" varStatus="i">
									<option value="${vo.code }" <c:if test="${vo.code eq mobilePhone[0]}">selected</c:if> >${vo.code }</option>
								</c:forEach>  
							</select><span class="s1">-</span><input type="text" class="w38" id="corpPhoneMid" /><span class="s1">-</span><input type="text" id="corpPhoneEnd" class="w38" />
						</div>
					</div>
				</li>
				<li>
					<span class="tit">부가정보 (선택입력)</span>
					<div class="m_layout">
						<div class="d1">
							</select>
							<span class="tit2">업태</span><input type="text" id="corpBusiness" class="w147" value="${rtv.corpBusiness }" />
						</div>
						<div class="d2">
							<span class="tit2">종목</span><input type="text" id="corpItem" class="w147" value="${rtv.corpItem }" />
						</div>
					</div>
				</li>
			</ul>
		</div>
		<h3 class="h3tit">관리자 정보</h3>
		<div class="member_box2 mab0">
			<ul>
				<li>
					<span class="tit">기본 정보 <b>(필수입력)</b></span>
					<div class="m_layout">
						<div class="d1">
							<span class="tit2">관리자명</span><input type="text" class="w147" id="adminNm" value="${rtv.accountNm }" />
						</div>
					</div>
					<div class="m_layout">
						<div class="d1" style="width:100%;">
							<span class="tit2">휴대폰 번호</span>
							<select class="select form" style="width:50px;" name="adminPhoneStart">
	                        	<c:forEach var="vo" items="${phoneTitle}" varStatus="i">
									<option value="${vo.code }" <c:if test="${vo.code eq mobilePhone[0]}">selected</c:if> >${vo.code }</option>
								</c:forEach> 
							</select><span class="s1">-</span><input type="text" class="w38" id="adminPhoneMid" /><span class="s1">-</span><input type="text" class="w38" id="adminPhoneEnd" />
	                        <c:if test="${empty reqVo.mobilePhone}">
	                        	<a href="#none" class="btn_searc_c" id="phoneAuth" style="display:none;">인증</a>
	                        	<div class="code_box" style="display:none;">
		                        	<div class="input_box" id="phoneAuthBox">
		                                <input type="text" placeholder="인증번호 입력" style="width:100px;" id="phoneAuthChecker"/>
		                                <div class="time"></div>
		                            </div>
		                            <a href="#none" class="btn_searc_c" id="btn_phoneAuthConfirm">확인</a>
			                    </div>
	                        </c:if>
	                        <c:if test="${not empty reqVo.mobilePhone}">
	                        	<div class="code_box" style="display:inline-block">
	                        		<div class="tip">인증되었습니다.</div>
	                        	</div>
	                        </c:if>
	                        <input type="hidden" id="phoneAuthConfirm" value="<c:if test="${not empty reqVo.mobilePhone  }">1</c:if>"/>
						</div>
					</div>
					<div class="m_layout">
						<div class="d1">
							<span class="tit2">일반 연락처</span>
							<select class="select form" style="width:50px;" name="emergencyPhoneStart">
	                        	<c:forEach var="vo" items="${phoneTitle}" varStatus="i">
									<option value="${vo.code }" <c:if test="${vo.code eq phone[0]}">selected</c:if> >${vo.code }</option>
								</c:forEach>  
							</select>
							<span class="s1">-</span><input type="text" class="w38" id="emergencyPhoneMid" /><span class="s1">-</span><input type="text" class="w38" id="emergencyPhoneEnd" />
						</div>
					</div>
				</li>
				<li>
					<span class="tit">부가정보 (선택입력)</span>
					<div class="m_layout">
						<div class="d1">
							<span class="tit2">부서명</span><input type="text" class="w147" id="groupNm" value="${rtv.corpGroupNm }" />
						</div>
					</div>
				</li>
			</ul>
		</div>
		<p class="form_notice">
			ㆍ서비스의 정상적 이용을 위해서는 ViewCAR차량단말(VID)이 필요하며 동 단말은 <em><a href="http://item.gmarket.co.kr/Item?goodscode=1105160936&pos_shop_cd=SH&pos_class_cd=111111111&pos_class_kind=T&keyword_order=%ba%e4%c4%ab+%c7%c1%b7%ce&keyword_seqno=13111705236&search_keyword=%ba%e4%c4%ab+%c7%c1%b7%ce" target="_blank">여기</a></em>를 누르시면 관련 구매정보를 보실 수 있습니다.
		</p>
	</c:if>
	<c:if test="${VDAS.rule eq '2' }">
		<div class="member_box3">						
			<div class="repair_layout2 pat0">
				<div class="repair_box1">
					<div class="user_img">
						<div><img src="${pathCommon}/images/user_img6.jpg" id="accountImg" data-key="${rtv.imgId }" /></div>
						<a href="#none" class="btn_pic_up"><img src="${pathCommon}/images/${_LANG}/btn_img_modify.jpg" alt="" /></a>
					</div>
					<div class="repair_box2">
						<div class=" repair_box2_1 pal25 borB1">
							<div class="mab14">
								<span class="tit">소속</span>
								<div class="select_type7 select_type7_1">
									<div class="user_txt1">
										${rtv.corpNm }
									</div>
								</div>
								&nbsp;&nbsp;&nbsp;
								<span class="tit" style="float:none;">지점</span>
								<input type="text" class="w172" id="office" value="xxx" />
							</div>
							<div class="mab14">
								<span class="tit">성명</span>
								<input type="text" class="w172" id="accountNm" value="${rtv.accountNm }" />
								&nbsp;&nbsp;&nbsp;
								<span class="tit" style="float:none;">부서명</span>
								<div class="select_type7 w182">
									<div>
										<select class="select form" id="corpGroup" style="width:182px;">
											<option value=""></option>
										</select>
									</div>
								</div>
							</div>
							<div class="mab14">
								<span class="tit">직급</span>
								<div class="select_type7 w182">
									<div>
										<select class="sel" id="position">
											<option value="1">${rtv.corpPosition }</option>
										</select>
									</div>
								</div>
								&nbsp;&nbsp;&nbsp;
								<span class="tit" style="float:none;">이메일</span>
								<input type="text" class="w172" id="email" value="${rtv.authMail }" />
							</div>
							<div class="mab14">
								<span class="tit">전화번호</span>
		                        <select class="select form" style="width:50px;" name="phoneStart">
		                        	<c:forEach var="vo" items="${phoneTitle}" varStatus="i">
										<option value="${vo.code }" <c:if test="${vo.code eq mobilePhone[0]}">selected</c:if> >${vo.code }</option>
									</c:forEach>  
		                        </select><span class="user_txt2">-</span><input type="text" class="w38" id="phoneMid" /><span class="user_txt2">-</span><input type="text" class="w38 mar21" id="phoneEnd" />
								<span class="tit" style="float:none;">휴대폰</span>
		                        <select class="select form" style="width:50px;" name="mobilePhoneStart">
		                        	<c:forEach var="vo" items="${phoneTitle}" varStatus="i">
										<option value="${vo.code }" <c:if test="${vo.code eq mobilePhone[0]}">selected</c:if> >${vo.code }</option>
									</c:forEach>                            
		                        </select><span class="user_txt2">-</span><input type="text" class="w38" id="mobilePhoneMid" /><span class="user_txt2">-</span><input type="text" class="w38" id="mobilePhoneEnd" />
							</div>
							<div class="mab14">
								<span class="tit">성별</span>
								<div class="ui_radio_box" style="margin-right:109px" id="gender">
									<label for="genderM">남자</label>
									<input type="radio" name="gender" id="genderM" value="M" <c:if test="${rtv.gender eq 'M'}">checked="checked"</c:if> >
									<label for="genderF">여자</label>
									<input type="radio" name="gender" id="genderF" value="F" <c:if test="${rtv.gender eq 'F'}">checked="checked"</c:if> >
								</div>
								<span class="tit" style="float:none;">혈액형</span>
								<div class="ui_radio_box" id="blood">
									<label for="bloodA">A</label>
									<input type="radio" name="blood" id="bloodA" value="A" <c:if test="${rtv.blood eq 'A'}">checked="checked"</c:if>>
									<label for="bloodB">B</label>
									<input type="radio" name="blood" id="bloodB" value="B" <c:if test="${rtv.blood eq 'B'}">checked="checked"</c:if>>
									<label for="bloodO">O</label>
									<input type="radio" name="blood" id="bloodO" value="O" <c:if test="${rtv.blood eq 'O'}">checked="checked"</c:if>>
									<label for="bloodAB">AB</label>
									<input type="radio" name="blood" id="bloodAB" value="AB" <c:if test="${rtv.blood eq 'AB'}">checked="checked"</c:if>>
								</div>
							</div>
						</div>
						<div class="repair_box2_1 pal25">
							<span class="tit">아이디</span>
							<div class="user_txt1">${VDAS.accountId }</div>
						</div>
						<div class=" repair_box2_1 borB1 pal25">
							<span class="tit">패스워드</span>
							<div class="user_txt1" id="password"></div>
							<br><br>
						</div>
                        <div class="repair_box2_1 pal25 borB1">
							<span class="tit">자택주소</span>
								<input type="text" class="w172" id="address" value="${rtv.addr }" />
								<a href="#" class="btn_searc_c">설정</a>
								<br><br>
						</div>
						<div class="repair_box2_1 pal25 borB1">
							<span class="tit">비고</span>
							<input type="text" class="w451" id="descript" value="${rtv.descript }" />
							<br><br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</c:if>
	<div class="btn_area2"><a href="#" class="btn_cancel">취소</a><a href="#none" class="btn_accept">수정완료</a></div>
</div>