<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<c:if test="${VDAS.rule eq '0' or VDAS.rule eq '1' }">
<script type="text/javascript">
var lazy;
var lazyHstr;
$(document).ready(function(){
	
	initMenuSel("M5005");
	
	$(".tabItem").on("click",function(){
		$(".tabItem").removeClass("on");
		$(this).addClass("on");
		
		var tab = $(this).data("tab");
		if(tab == "1"){
			$(".tab1").show();
			$(".tab2").hide();
			$(".tab3").hide();
		}else if(tab == "2"){
			$(".tab1").hide();
			$(".tab2").show();
			$(".tab3").hide();
		}else if(tab == "3"){
			$(".tab1").hide();
			$(".tab2").hide();
			$(".tab3").show();
		}
	});
	
	$("#btn_geoFence").on("click",function(){
		var param = {};
		param.productType="geoFence";
		
		$VDAS.instance_post("/user/payRequest.do",param);
	});
	
	$("#btn_next").on("click",function(){
		var selObj = $(".selObj:checked");

		if(selObj.length == 0){
			alert("결제할 차량을 선택해 주세요.");
			return;
		}else{
			var param = {};
			param.vehicleKey = [];
			param.expireState = [];
			param.imgS = [];
			param.modelMaster = [];
			param.plateNum = [];
			param.deviceSeries = [];
			param.deviceSn = [];
			param.expireDate = [];
			selObj.each(function(i){
				param.vehicleKey.push($(this).data("vehiclekey"));
				param.expireState.push($(this).data("expirestate"));
				param.imgS.push($(this).data("imgs"));
				param.modelMaster.push($(this).data("modelmaster"));
				param.plateNum.push($(this).data("platenum"));
				param.deviceSeries.push($(this).data("deviceseries"));
				param.deviceSn.push($(this).data("devicesn"));
				param.expireDate.push($(this).data("expiredate"));
			});
			
			param.productType="device";
			
			$VDAS.instance_post("/user/payRequest.do",param);
		}
			
			
	});
	
	
	if(!lazy)
		lazy = $("#infoTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
				searchExpireState : $("#orderEffective")
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 10
			,loadUrl : "/com/getDeviceVehicleList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.rtvList;	
				var strHtml = '';
				
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
				
					strHtml += '<tr>';
					strHtml += '<td><input type="checkbox" class="selObj" data-vehiclekey="'+vo.vehicleKey
					+'" data-expirestate="'+vo.expireState
					+'" data-imgs="'+vo.imgS
					+'" data-modelmaster="'+vo.modelMaster
					+'" data-platenum="'+vo.plateNum
					+'" data-deviceseries="'+vo.deviceSeries
					+'" data-devicesn="'+vo.deviceSn
					+'" data-expiredate="'+vo.expireDate					
					+'"/></td>';
					strHtml += '<td>'+((vo.expireState=='1')?'개통':'미개통')+'</td>';					
					strHtml += '<td>';
					strHtml += '<p>'+vo.deviceSeries+'</p>';
					strHtml += '<p>'+vo.deviceSn+'</p>';
					strHtml += '</td>';					
					strHtml += '<th class="">';					
					strHtml += '<strong class="car_img"><img src="${path}/common/images/vehicle/'+vo.imgS+'.png" alt="" /></strong>';
					strHtml += '<a href="#none" class="btn_vehicleDetail" data-key="'+vo.vehicleKey+'">';
					strHtml += '<span>'+vo.modelMaster+'</span>';
					strHtml += vo.plateNum;
					strHtml += '</a>';
					strHtml += '</th>';
					strHtml += '<td>'+vo.expireDate+'</td>';
					strHtml += '</tr>';
					
				}
				return strHtml;
			}
		});
	
	if(!lazyHstr)
		lazyHstr = $("#hstrTable").lazyLoader({
			searchFrmObj : $("#searchFrmHstr")
			,searchFrmVal : {
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 10
			,loadUrl : "/user/getPaymentHstrHeaderList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.result;	
				var strHtml = '';
				
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					console.log(vo);
					
					strHtml += '<tr>';
					strHtml += '<td>'+vo.tranDate+'</td>';
					strHtml += '<td>';
					var serviceType = "";
					if(vo.productType == 'device') serviceType = "서비스 이용요금";
					if(vo.productType == 'geoFence') serviceType = "부가 서비스";
					strHtml += serviceType;
					strHtml += '</td>';
					strHtml += '<th class="">';
					
					if(vo.productType == 'device')
						strHtml += '<a href="#none" class="btn_payDetail" data-id="'+vo.paymentId+'" data-type="'+vo.productType+'">';
										
					strHtml += '<span>'+'[총 '+vo.detailCnt+'개] '+vo.productNm+'</span>';
					
					if(vo.productType == 'device')
						strHtml += '</a>';
					
					strHtml += '</th>';					
					var payType = "";
					if(vo.payType == '11') payType = "신용카드";
					else if(vo.payType == '21') payType = "계좌이체";
					strHtml += '<td>'+payType+'</td>';					
					strHtml += '<td>'+number_format(vo.amount)+'</td>';
					strHtml += '</tr>';
					
				}
				return strHtml;
			}
		});
	
	$("#chkAll").on("click",function(){
		if($(this).is(":checked"))
			$(".selObj","#infoTable tbody").prop("checked",true);
		else $(".selObj","#infoTable tbody").prop("checked",false);
	});
	
	
	$("body").on("click",".btn_payDetail",function(){
		var key = $(this).data("id");
		var type = $(this).data("type") ;
		
		$VDAS.instance_post("/user/payDetail.do",{paymentKey:key});
	});
	
	$VDAS.http_post("/vehicle/getVehicleCnt.do",{},{
		success : function(r){
			var data = r.result;
			
			$("#titleCnt1").text(data.used+"대");
		}
	});
	
	$("#orderEffective").on("change",function(){
		$.lazyLoader.search(lazy);		
	});
	
	$VDAS.http_post("/user/appendServiceInfo.do",{},{
		success : function(r){
			var geo = r.geoFence;
			if(geo){
				$("#geoLastTran").text(geo);
			}else $("#geoLastTran").text(" - ");
		}
	});
});


</script>
<form id="searchFrm" style="display:none">
	<input type="hidden" name="searchExpireState" id="searchExpireState" value="" />
</form>
<form id="searchFrmHstr" style="display:none">
</form>
<div class="right_layout">
	<h2 class="tit_sub">
		이용요금 조회·결제
		<p>서비스 이용요금을 조회하거나 결제하실 수 있습니다.</p>
	</h2>
	<div class="sub_top1">
		<span>등록차량수<b id="titleCnt1">0대</b></span>
		<strong id="nowDate"></strong>
	</div>
	
	<div class="tab_layout">
		<div class="pageTab">
			<ul class="tab_nav">
				<li class="tabItem on" data-tab='1'><a href="#none" >이용 현황</a></li>
				<li class='tabItem' data-tab='2'><a href="#none" >부가서비스 이용현황</a></li>
				<li class='tabItem' data-tab='3'><a href="#none" >결제 내역</a></li>
			</ul>
		</div>
		<div class="box3 tab1">
			<span style="width: 100%;text-align: center;padding-top:20px;font-size:12px;font-weight: 800;color: #424242">von-S31 : 차량 1대당 월 5,500</span>
			<div style="width: 100%;text-align: center;padding-top:0px;font-size:12px;font-weight: 600;color: #828282;text-align:left">
				이용요금에는 통신요금이 포함되어있어 구매 후에는 서비스 해지가 불가능합니다. 신중히 구매해주세요.
				</br>부가세와 결제 수수료가 포함되어있습니다.
				</br>환불 및 기타 결제 사항은 Easypay 규정을 준수합니다.
			</div>
			<span><a class="right" href="#none" id="btn_next">이용요금 결제</a></span>
		</div>
		<table class="table1 mgt10 tab1" id="infoTable">
			<colgroup>
				<col width="70">
				<col width="90">
				<col width="200">
				<col width="*">
				<col width="150">				
			</colgroup>
			<thead>			
				<tr>
					<th><input type="checkbox" id="chkAll"></th>
					<th><select id="orderEffective" class="select form">
							<option value="">상태</option>
							<option value="1">개통</option>
							<option value="0">미개통</option>
						</select>
					</th>
					<th>단말기정보</th>
					<th>등록차량</th>
					<th>이용만기일</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		
		<div class="box3 tab2" style="display:none">
			<div style="width: 100%;text-align: center;padding-top:0px;font-size:12px;font-weight: 600;color: #828282;text-align:left">
				이용요금에는 통신요금이 포함되어있어 구매 후에는 서비스 해지가 불가능합니다. 신중히 구매해주세요.
				</br>부가세와 결제 수수료가 포함되어있습니다.
				</br>환불 및 기타 결제 사항은 Easypay 규정을 준수합니다.
			</div>
		</div>
		<table class="table1 mgt10 tab2" id="subTable" style="display: none">
			<colgroup>
				<col width="120">
				<col width="*">
				<col width="150">				
				<col width="150">
			</colgroup>
			<thead>			
				<tr>
					<th>현재상태</th>
					<th>부가서비스</th>
					<th>최근결제일</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>총 ${VDAS.geoMaxCnt}개</td>
					<th style="line-height: 20px;"><b>운행스케쥴 거래처 추가 서비스</b>
						</br>거래처 추가는 10개 단위로 가능하며,
						</br>10개당 11,000원(부가세 포함)이 부가됩니다.
					</th>
					<td id="geoLastTran"></td>
					<td><a href="#none" id="btn_geoFence" style="display: inline-block;top: 50%;right: 16px;width: 123px;height: 38px;line-height: 38px;text-align: center;background: #4e4e4e;color: #fff;font-weight: 600;font-size: 13px;">부가서비스 결제</a></td>
				</tr>
			</tbody>
		</table>
		
		
		<table class="table1 mgt10 tab3" id="hstrTable" style="display: none">
			<colgroup>
				<col width="120">
				<col width="120">
				<col width="*">
				<col width="70">
				<col width="100">
			</colgroup>
			<thead>			
				<tr>
					<th>결제일</th>
					<th>서비스 형태</th>
					<th>내역</th>
					<th>결제방법</th>
					<th>총 결제금액</br>(부가세 포함)</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
</c:if>
<c:if test="${VDAS.rule ne '0' and VDAS.rule ne '1' }">
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("M5005");
});
</script>
<div class="right_layout">
	<img src="${pathCommon}/images/${_LANG}/authError.png" alt="" style="width:100%">
</div>
</c:if>