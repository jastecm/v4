<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/index.js"></script>

<script type="text/javascript">
var _f = true;
$(document).ready(function(){
	
	initMenuSel("M5005");
	
	$VDAS.http_post("/user/payRequestHstrDetail.do",{paymentKey:"${paymentKey}"},{
		success : function(r){
			var h = r.header;
			var d = r.detail;
			/*
			{"detail":[
			           {"paymentValue":"A0000002","paymentType":"device","paymentId":"ba56f793f68a160887a8c6bf690e7d67","paymentValuePrice":5500,"deviceSn":"A0000002","applyStartDate":"2021-01-01","paymentValueNote":"1","deviceSeries":"von-S31","applyEndDate":"2021-02-01"}],
			 "header":{"modDate":1505719222000,"accountKey":"6d6ea5a5-8867-11e7-b9e0-0002b3da4116","paymentId":"ba56f793f68a160887a8c6bf690e7d67","regDate":1505717706000,"state":"0000","payType":"11","reqPay":"5500","corp":"7","productNm":"123123","productType":"device"}
			}
			*/
			
			var pType = h.productType;
			
			$("#totCnt").text(d.length+"대");
			$("#totPay").text(number_format(h.reqPay));
			if(h.payType=='11') $("#selectPay").text("신용카드");
			else if(h.payType=='21') $("#selectPay").text("계좌이체");
			
			var strHtml = "";
			for(var i = 0 ; i < d.length ; i++){
				var vo = d[i];
				strHtml += "<tr class='f'>";
				strHtml += "<td>";
				strHtml += vo.deviceSeries+"/"+vo.deviceSn;
				strHtml += "</td>";
				strHtml += "<td>";
				strHtml += vo.paymentValueNote+"개월";
				strHtml += "</td>";
				strHtml += "<td>";
				strHtml += vo.applyStartDate + " ~ " + vo.applyEndDate 
				strHtml += "</td>";
				strHtml += "<td>";
				strHtml += number_format(vo.paymentValuePrice);
				strHtml += "</td>";
				strHtml += "</tr>";
			}
			
			$("#infoTable tbody").append(strHtml);
		}
	});
	
});
</script>
<div class="black"></div>


<div class="right_layout">
	<div class="detail_view">
		<a href="javascript:window.history.back();" class="btn_close"><img src="${pathCommon}/images/close.jpg" alt="" /></a>
		<h2 class="h2tit">
			이용요금 결제 상세
		</h2>
		<div class="sub_top1 sub_top2 mab0">
			<span>이용요금 결제 상세 내역입니다.</span>
		</div>
		<div class="repair_layout2">
				<div class="repair_box1 repair_box1_2">
					<div class="tit">결제정보</div>
						<div class="repair_box2 borB1">
							<div class="repair_box2_1">
								<span class="tit3" style="padding-left: 0px !important">총 단말기</span>						
								<div class="user_txt1" id="totCnt">0 대</div>
							</div>
							<table class="table1 mgt10 tab1" id="infoTable">
								<colgroup>
									<col width="*">
									<col width="70">
									<col width="240">
									<col width="70">
								</colgroup>
								<thead>			
									<tr>
										<th>단말기정보</th>
										<th>이용기간</th>
										<th>적용기간</th>
										<th>이용요금</th>								
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					
				
				<div class="total_payment" style="padding-left: 0px;"><b>결제 금액</b><span id="totPay">0</span>원(부가세 포함)</div>
			</div>
		</div>
		<div class="repair_layout2 repair_layout2_5">
			<div class="repair_box1 repair_box1_2">
				<div class="tit">결제방식</div>
				<div class="repair_box2">
					<div class="repair_box2_1 repair_box2_2">
						<div class="payment_radio" id="selectPay" style="padding-left: 0px;">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- 차량선택 팝업 -->
<div class="black"></div>
