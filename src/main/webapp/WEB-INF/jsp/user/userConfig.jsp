<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("M5001");

	$("#passwordText").on("keyup",function(e){
		if(e.keyCode == 13) $(".btn_login").trigger("click");
	});
	
	$("body").on("click",".btn_login",function(){
		var pw = $("#passwordText").val();
		var id = $("#idText").text();
		$VDAS.http_post("/user/userConfigChkPw.do",{accountPw:pw, accountId:id},{
			success : function(r){
				$VDAS.instance_post("/user/userConfigChk.do",{accountPw:pw, accountId:id});
			},error : function(r){
				alert("잘못된 비밀번호 입니다.");
			}
		});
		//;
	});
});
</script>

<form id="checkFrm" style="display:none">
	<input type="hidden" name="password" value="" />
</form>
<div class="right_layout">
	<h2 class="tit_sub">
		회원정보 설정
		<p>서비스의 정상적인 이용을 위해 정확한 회원정보를 입력합니다.</p>
	</h2>
	<br><br><br><br>
	<h2 class="h2tit">
		로그인
	</h2>
	<div class="login_box1">
		<p>
			외부로부터 <b>${VDAS.accountId }</b>님의 정보를 안전하게 보호하기 위해 비밀번호를 다시 한 번 확인 합니다.<br>
			항상 비밀번호는 타인에게 노출되지 않도록 주의해 주세요.
		</p>
		<ul>
			<li>
				<strong>아이디</strong><span id="idText">${VDAS.accountId }</span>
			</li>
			<li>
				<strong>비밀번호</strong><input type="password" id="passwordText" placeholder="비밀번호 입력" />
			</li>
		</ul>
		<div class="btn_area2"><a href="#" class="btn_login">확인</a>
	</div>
	<br><br><br><br>
</div>