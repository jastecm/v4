<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script type="text/javascript">
$(document).ready(function(){
	
	initMenuSel("M5002");

	$("#passwordText").on("keyup",function(e){
		if(e.keyCode == 13) $(".btn_login").trigger("click");
	});

	$("body").on("click",".btn_cancel",function(){
		$VDAS.instance_post("/main/index.do",{});
	});
	$("body").on("click",".btn_change",function(){
		
		if($("#currentPw").val().length == 0 ||$("#newPw").val().length == 0 ||$("#checkPw").val().length == 0 )
			alert("비밀번호를 입력해주세요.");
		else{
			$VDAS.http_post("/user/changePasswordConfirm.do",{currentPw:$("#currentPw").val(),newPw:$("#newPw").val(),checkPw:$("#checkPw").val()},{
				success : function(r){
					alert("변경되었습니다.");
					window.location.href=_defaultPath+"/main/index.do";
				},error : function(r){
					if($message[r.responseJSON.result])
						alert($message[r.responseJSON.result]);
					else {
						alert(r.status+"\n"+r.statusText);
					}
				}
			})
		}
	});
});
</script>

<form id="searchFrm" style="display:none">
	<input type="hidden" name="currentPw" value="" />
	<input type="hidden" name="newPw" value="" />
	<input type="hidden" name="checkPw" value="" />
</form>
<div class="right_layout">
	<h2 class="tit_sub">
		비밀번호 변경
		<p>변경할 새로운 비밀번호를 입력해주세요.</p>
	</h2>
	<br><br><br><br>
	<h2 class="h2tit">
		비밀번호 입력
	</h2>
	<div class="login_box1">
		<div class="login_box2">
			<img src="${pathCommon}/images/password_img.jpg" alt="" />
			<ul class="login_box2_1">
				<li>
					<strong>현재비밀번호</strong><input type="password" id="currentPw" placeholder="현재 비밀번호 입력" />
				</li>
			</ul>
		</div>
		<ul class="login_box2_1">
			<li>
				<strong>새로운 비밀번호</strong><input type="password" id="newPw" placeholder="새로운 비밀번호 입력" />
			</li>
			<li>
				<strong>비밀번호 확인</strong><input type="password" id="checkPw" placeholder="새로운 비밀번호 확인" />
			</li>
		</ul>
		<div class="btn_area2"><a href="#" class="btn_cancel">취소</a><a href="#" class="btn_change">변경</a>
	</div>
	<br><br><br><br>
</div>