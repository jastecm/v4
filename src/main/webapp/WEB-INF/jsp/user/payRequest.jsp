<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/index.js"></script>
<script language="javascript" src="${pageContext.request.contextPath}/common/easypay/js/default.js" type="text/javascript"></script>
<!-- Test -->
<!-- <script type="text/javascript" src="http://testpg.easypay.co.kr/webpay/EasypayCard_Web.js"></script> -->
<!-- Real -->
<script type="text/javascript" src="https://pg.easypay.co.kr/webpay/EasypayCard_Web.js"></script>

<script type="text/javascript">
var _f = true;
$(document).ready(function(){
	
	initMenuSel("M5005");
	
	calcFee();
	f_init();
	
	<c:if test="${productType eq 'device'}">
	$(".product").on("change",function(){
		calcFee();
	});
	
	$(".productAll").on("change",function(){
		var val = $(this).val();
		$(".product").each(function(){
			$(this).val($(this).data("series")+"."+val);
		});
		calcFee();
	});
	</c:if>
	
	<c:if test="${productType eq 'geoFence'}">
		$("#btn_plus").on("click",function(){
			var cnt = $("#input_cnt").val();
			var c = ${VDAS.geoMaxCnt/10};
			var max = 10-c;
			if(max > parseInt(cnt)){
				$("#input_cnt").val(parseInt(cnt)+1);
				calcFee();	
			}else{
				alert("최대 100개 까지만 추가할수 있습니다.");
			}
				
		});
		$("#btn_minus").on("click",function(){
			var cnt = $("#input_cnt").val();
			if(parseInt(cnt) > 1){
				$("#input_cnt").val(parseInt(cnt)-1);
				calcFee();
			}
		});
	</c:if>
	
});

function calcFee(){
	
	var totpay = 0 ;
	
	<c:if test="${productType eq 'device'}">
	$(".paymentFee").each(function(i){
		var product = $(this).closest("tr").find(".product").val();
		var productSeries = product.split(".")[0];
		var productMonth = product.split(".")[1];
		var pay = 0;
		if(productSeries == 'von-S31') {
			if(productMonth == '3'){
				pay = 5500*0.95;
			}else if(productMonth == '12'){
				pay = 5500*0.8;
			}else{
				pay = 5500;	
			}
			
			
		}else pay = 0;
		
		$(this).html(number_format( pay*productMonth));
		totpay += pay*productMonth;
	});
	</c:if>
	
	<c:if test="${productType eq 'geoFence'}">
	var cnt = $("#input_cnt").val();
	var pay = 0;
	totpay = parseInt(cnt)*11000;
	
	$(".paymentFee").text(number_format(totpay));
	$("#txt_cnt").text(parseInt(cnt)*10+"개");
	</c:if>
	
	$("#totPay").html(number_format( totpay));
}

/* 입력 자동 Setting */
function f_init()
{
    var frm_pay = document.frm_pay;

    var today = new Date();
    var year  = today.getFullYear();
    var month = today.getMonth() + 1;
    var date  = today.getDate();
    var time  = today.getTime();

    if(parseInt(month) < 10)
    {
        month = "0" + month;
    }

    if(parseInt(date) < 10)
    {
        date = "0" + date;
    }


    frm_pay.EP_mall_id.value = "05531138";
    frm_pay.EP_mall_nm.value = "(주)자스텍엠";
    
    //frm_pay.EP_mall_id.value = "T5102001";
    //frm_pay.EP_mall_nm.value = "자스텍엠";
    
    frm_pay.EP_vacct_end_date.value = "" + year + month + date;
    frm_pay.EP_vacct_end_time.value = "235959";
    frm_pay.EP_memb_user_no.value = "${VDAS.accountKey}";                           
    
    frm_pay.EP_user_id.value = "${VDAS.accountKey}";
    frm_pay.EP_user_nm.value = "${VDAS.name}";
    frm_pay.EP_user_mail.value = "${VDAS.authMail}";
    frm_pay.EP_user_phone1.value = "${VDAS.phone}";
    frm_pay.EP_user_phone2.value = "${VDAS.mobilePhone}";
    
    
    frm_pay.EP_product_nm.value = "";
    frm_pay.EP_product_amt.value = "0";
    frm_pay.EP_return_url.value = "https://vdaspro.viewcar.co.kr/vdasPro/easypay/order_res.do";
    /* frm_pay.EP_return_url.value = "http://localhost:8080${defaultPath}/easypay/order_res.do"; */
    
}
function urldecode( str )
{
    // 공백 문자인 + 를 처리하기 위해 +('%20') 을 공백으로 치환
    return decodeURIComponent((str + '').replace(/\+/g, '%20'));
}
function f_start_pay()
{
	<c:if test="${productType eq 'device'}">
		var item = "${deviceSn}";
		var itemProduct = "";
		$(".product").each(function(i){
			if(i==0)
				itemProduct = $(this).val();
			else itemProduct += ","+$(this).val();
		});
		
		 <c:if test = "${fn:length(fn:split(deviceSn,',')) eq 1}">
	     var itemNm = "${deviceSn}"; 
	     </c:if>
	     <c:if test = "${fn:length(fn:split(deviceSn,',')) ne 1}">
	     var itemNm = "${fn:split(deviceSn,',')[0]}" + " ( 외 "+ "${fn:length(fn:split(deviceSn,','))-1}"+"건)"; 
	     </c:if>
    </c:if>
    <c:if test="${productType eq 'geoFence'}">
    	var item=$("#input_cnt").val();
    	var itemProduct = "geoFence";
    	var itemNm = "운행스케쥴 거래처 추가 서비스";
    </c:if>
	
    $VDAS.http_post("/easypay/paymentUid.do",{item:item,itemProduct:itemProduct,itemNm:itemNm},{
    	success : function(rtv){
    		if(rtv.result){
    			var pay = rtv.resultPay;
    			
    			var frm_pay = document.frm_pay;
				
    			frm_pay.EP_order_no.value = rtv.result;
    			
                frm_pay.EP_product_nm.value = itemNm;
                frm_pay.EP_product_amt.value = pay;
                frm_pay.EP_pay_type.value = $("input[name=payment]:checked").val();                
                /* UTF-8 사용가맹점의 경우 EP_charset 값 셋팅 필수 */
                if(_f&&frm_pay.EP_charset.value == "UTF-8")
                {
                    // 한글이 들어가는 값은 모두 encoding 필수.
                    frm_pay.EP_mall_nm.value      = encodeURIComponent( frm_pay.EP_mall_nm.value );
                    frm_pay.EP_product_nm.value   = encodeURIComponent( frm_pay.EP_product_nm.value );
                    _f = false;
                }

                easypay_webpay(frm_pay,"${defaultPath}/easypay/normal/iframe_req.do","hiddenifr","0","0",frm_pay.EP_window_type.value,30);
    		}else{
    			alert("error");
    		}
    	}
	});
}

function f_submit()
{
    var frm_pay = document.frm_pay;
    /* UTF-8 사용가맹점의 경우 EP_charset 값 셋팅 필수 */
    if( frm_pay.EP_charset.value == "UTF-8" )
    {
        // 한글이 들어가는 값은 모두 encoding 필수.
        frm_pay.EP_mall_nm.value      = decodeURIComponent( frm_pay.EP_mall_nm.value );
        frm_pay.EP_product_nm.value   = decodeURIComponent( frm_pay.EP_product_nm.value );
    }
    
    frm_pay.target = "_self";
    frm_pay.action = "${defaultPath}/easypay/easypay_request.do";
    frm_pay.submit();
}


</script>
<div class="black"></div>


<form name="frm_pay" method="post" action="" style="display:none">
<!--------------------------->
<!-- ::: 공통 인증 요청 값 -->
<!--------------------------->
<input type="hidden" id="EP_currency"       name="EP_currency"          value="00">       <!-- 통화코드 // 00 : 원화-->
<input type="hidden" id="EP_return_url"     name="EP_return_url"        value="">         <!-- 가맹점 CALLBACK URL // -->
<input type="hidden" id="EP_ci_url"         name="EP_ci_url"            value="">         <!-- CI LOGO URL // -->
<input type="hidden" id="EP_lang_flag"      name="EP_lang_flag"         value="">         <!-- 언어 // -->
<input type="hidden" id="EP_charset"        name="EP_charset"           value="UTF-8">   <!-- 가맹점 CharSet // -->
<input type="hidden" id="EP_user_type"      name="EP_user_type"         value="">         <!-- 사용자구분 // -->
<input type="hidden" id="EP_user_id"        name="EP_user_id"           value="">         <!-- 가맹점 고객ID // -->
<input type="hidden" id="EP_memb_user_no"   name="EP_memb_user_no"      value="">         <!-- 가맹점 고객일련번호 // -->
<input type="hidden" id="EP_user_nm"        name="EP_user_nm"           value="">         <!-- 가맹점 고객명 // -->
<input type="hidden" id="EP_user_mail"      name="EP_user_mail"         value="">         <!-- 가맹점 고객 E-mail // -->
<input type="hidden" id="EP_user_phone1"    name="EP_user_phone1"       value="">         <!-- 가맹점 고객 연락처1 // -->
<input type="hidden" id="EP_user_phone2"    name="EP_user_phone2"       value="">         <!-- 가맹점 고객 연락처2 // -->
<input type="hidden" id="EP_user_addr"      name="EP_user_addr"         value="">         <!-- 가맹점 고객 주소 // -->
<input type="hidden" id="EP_user_define1"   name="EP_user_define1"      value="">         <!-- 가맹점 필드1 // -->
<input type="hidden" id="EP_user_define2"   name="EP_user_define2"      value="">         <!-- 가맹점 필드2 // -->
<input type="hidden" id="EP_user_define3"   name="EP_user_define3"      value="">         <!-- 가맹점 필드3 // -->
<input type="hidden" id="EP_user_define4"   name="EP_user_define4"      value="">         <!-- 가맹점 필드4 // -->
<input type="hidden" id="EP_user_define5"   name="EP_user_define5"      value="">         <!-- 가맹점 필드5 // -->
<input type="hidden" id="EP_user_define6"   name="EP_user_define6"      value="">         <!-- 가맹점 필드6 // -->
<input type="hidden" id="EP_product_type"   name="EP_product_type"      value="">         <!-- 상품정보구분 // -->
<input type="hidden" id="EP_product_expr"   name="EP_product_expr"      value="">         <!-- 서비스 기간 // (YYYYMMDD) -->
<input type="hidden" id="EP_disp_cash_yn"   name="EP_disp_cash_yn"      value="">         <!-- 현금영수증 화면표시여부 //미표시 : "N", 그외: DB조회 -->

<!--------------------------->
<!-- ::: 카드 인증 요청 값 -->
<!--------------------------->

<input type="hidden" id="EP_usedcard_code"      name="EP_usedcard_code"     value="">      <!-- 사용가능한 카드 LIST // FORMAT->카드코드:카드코드: ... :카드코드 EXAMPLE->029:027:031 // 빈값 : DB조회-->
<input type="hidden" id="EP_quota"              name="EP_quota"             value="">      <!-- 할부개월 (카드코드-할부개월) -->
<input type="hidden" id="EP_os_cert_flag"       name="EP_os_cert_flag"      value="2">     <!-- 해외안심클릭 사용여부(변경불가) // -->
<input type="hidden" id="EP_noinst_flag"        name="EP_noinst_flag"       value="">      <!-- 무이자 여부 (Y/N) // -->
<input type="hidden" id="EP_noinst_term"        name="EP_noinst_term"       value="">      <!-- 무이자 기간(카드코드-더할할부개월) // -->
<input type="hidden" id="EP_set_point_card_yn"  name="EP_set_point_card_yn" value="">      <!-- 카드사포인트 사용여부 (Y/N) // -->
<input type="hidden" id="EP_point_card"         name="EP_point_card"        value="">      <!-- 포인트카드 LIST  // -->
<input type="hidden" id="EP_join_cd"            name="EP_join_cd"           value="">      <!-- 조인코드 // -->
<input type="hidden" id="EP_kmotion_useyn"      name="EP_kmotion_useyn"     value="Y">     <!-- 국민앱카드 사용유무 // -->
<input type="hidden" id="EP_cert_type"          name="EP_cert_type"         value="">      <!-- 인증타입 빈값:일반 0:인증 1:비인증 // -->

<!------------------------------->
<!-- ::: 가상계좌 인증 요청 값 -->
<!------------------------------->

<input type="hidden" id="EP_vacct_bank"      name="EP_vacct_bank"     value="">      <!-- 가상계좌 사용가능한 은행 LIST // -->
<input type="hidden" id="EP_vacct_end_date"  name="EP_vacct_end_date" value="">      <!-- 입금 만료 날짜 // -->
<input type="hidden" id="EP_vacct_end_time"  name="EP_vacct_end_time" value="">      <!-- 입금 만료 시간 // -->

<!------------------------------->
<!-- ::: 선불카드 인증 요청 값 -->
<!------------------------------->

<input type="hidden" id="EP_prepaid_cp"    name="EP_prepaid_cp"     value="">      <!-- 선불카드 CP // FORMAT->코드:코드: ... :코드 EXAMPLE->CCB:ECB // 빈값 : DB조회-->

<!--------------------------------->
<!-- ::: 인증응답용 인증 요청 값 -->
<!--------------------------------->

<input type="hidden" id="EP_res_cd"          name="EP_res_cd"         value="">      <!--  응답코드 // -->
<input type="hidden" id="EP_res_msg"         name="EP_res_msg"        value="">      <!--  응답메세지 // -->
<input type="hidden" id="EP_tr_cd"           name="EP_tr_cd"          value="">      <!--  결제창 요청구분 // -->
<input type="hidden" id="EP_ret_pay_type"    name="EP_ret_pay_type"   value="">      <!--  결제수단 // -->
<input type="hidden" id="EP_ret_complex_yn"  name="EP_ret_complex_yn" value="">      <!--  복합결제 여부 (Y/N) // -->
<input type="hidden" id="EP_card_code"       name="EP_card_code"      value="">      <!--  카드코드 (ISP:KVP카드코드 MPI:카드코드) // -->
<input type="hidden" id="EP_eci_code"        name="EP_eci_code"       value="">      <!--  MPI인 경우 ECI코드 // -->
<input type="hidden" id="EP_card_req_type"   name="EP_card_req_type"  value="">      <!--  거래구분 // -->
<input type="hidden" id="EP_save_useyn"      name="EP_save_useyn"     value="">      <!--  카드사 세이브 여부 (Y/N) // -->
<input type="hidden" id="EP_trace_no"        name="EP_trace_no"       value="">      <!--  추적번호 // -->
<input type="hidden" id="EP_sessionkey"      name="EP_sessionkey"     value="">      <!--  세션키 // -->
<input type="hidden" id="EP_encrypt_data"    name="EP_encrypt_data"   value="">      <!--  암호화전문 // -->
<input type="hidden" id="EP_pnt_cp_cd"       name="EP_pnt_cp_cd"      value="">      <!--  포인트 CP 코드 // -->
<input type="hidden" id="EP_spay_cp"         name="EP_spay_cp"        value="">      <!--  간편결제 CP 코드 // -->
<input type="hidden" id="EP_card_prefix"     name="EP_card_prefix"    value="">      <!--  신용카드prefix // -->
<input type="hidden" id="EP_card_no_7"       name="EP_card_no_7"      value="">      <!--  신용카드번호 앞7자리 // -->

<input type="hidden" id="EP_mall_id" name="EP_mall_id" value="" maxlength="8" class="input_F">
<input type="hidden" id="EP_mall_nm" name="EP_mall_nm" value="">         <!-- 가맹점명-->

<input type="hidden" id="EP_pay_type" name="EP_pay_type" value="" size="50" maxlength="8" class="input_F">
<input type="hidden" id="EP_window_type" name="EP_window_type" value="iframe" size="50" maxlength="8" class="input_F">
<%--
            <select id="EP_pay_type" name="EP_pay_type" class="input_F">
                <option value="21" >계좌이체</option>
                <option value="50" >선불결제</option>
                <option value="60" >간편결제</option>
                <option value="22" >무통장입금</option>
                
                <option value="11" selected>신용카드</option>
                <option value="31" >휴대폰</option>
            </select>
            <select id="EP_window_type" name="EP_window_type" class="input_F">
                <option value="iframe" selected>iframe</option>
                <option value="popup" >popup</option>
            </select>
--%>         
<input type="hidden" id="EP_order_no" name="EP_order_no" value="" size="50" class="input_F">
<input type="hidden" id="EP_product_nm" name="EP_product_nm" value="" size="50" class="input_A">
<input type="hidden" id="EP_product_amt" name="EP_product_amt" value="" size="50" class="input_A">
</form>
<div class="right_layout">
	<c:set var="vehicleKey" value="${fn:split(vehicleKey,',')}" />
	<c:set var="expireState" value="${fn:split(expireState,',')}" />
	<c:set var="imgS" value="${fn:split(imgS,',')}" />
	<c:set var="modelMaster" value="${fn:split(modelMaster,',')}" />
	<c:set var="plateNum" value="${fn:split(plateNum,',')}" />
	<c:set var="deviceSeries" value="${fn:split(deviceSeries,',')}" />
	<c:set var="deviceSn" value="${fn:split(deviceSn,',')}" />
	<c:set var="expireDate" value="${fn:split(expireDate,',')}" />
	<div class="detail_view">
		<a href="javascript:window.history.back();" class="btn_close"><img src="${pathCommon}/images/close.jpg" alt="" /></a>
		<h2 class="h2tit">
			이용요금 결제
		</h2>
		<div class="sub_top1 sub_top2 mab0" style="height:40px;">
			<span>이용금액과 기간을 확인한 후, 결제해주세요.
			</br>1년 이용권 구매시 20%, 3개월 이용권 구매시 5% 할인이 적용됩니다.
			</span>
		</div>
		<div class="repair_layout2">
				<div class="repair_box1 repair_box1_2">
					<div class="tit">결제정보</div>
		
					<c:if test="${productType eq 'device'}">
				
						<div class="repair_box2 borB1">
							<div class="repair_box2_1">
								<span class="tit3" style="padding-left: 0px !important">총 단말기</span>						
								<div class="user_txt1">${fn:length(deviceSn)} 대</div>
							</div>
							<table class="table1 mgt10 tab1" id="infoTable">
								<colgroup>
									<col width="70">
									<col width="*">
									<col width="150">
									<col width="130">
									<col width="100">
												
								</colgroup>
								<thead>			
									<tr>
										<th>개통상태</th>
										<th>등록차량</th>
										<th>단말기정보</th>
										<th>이용기간</th>
										<th>이용요금</th>								
									</tr>
								</thead>
								<tbody>
									<c:forEach var="vo" items="${deviceSn}" varStatus="i">
										<tr class="f">
											<td>
												<c:if test="${expireState[i.index] eq '1' }">개통</c:if>
												<c:if test="${expireState[i.index] eq '0' }">미개통</c:if> 
											</td>
											<th class="">
												<strong class="car_img"><img src="${path}/common/images/vehicle/${imgS[i.index]}.png" alt="" /></strong>
												<a href="#none" class="btn_vehicleDetail">
													<span>${modelMaster[i.index]}</span>
													${plateNum[i.index]}
												</a>
											</th>
											<td>
												<p>${deviceSeries[i.index]}</p>
												<p>${deviceSn[i.index]}</p>
											</td>
											<td>
												<c:if test="${deviceSeries[i.index] eq 'von-S31'}">
													<select class="select form product" data-series='von-S31'>
														<option value="von-S31.1">1개월</option>
														<option value="von-S31.3">3개월</option>
														<!-- <option value="von-S31.6">6개월</option> -->
														<option value="von-S31.12">12개월</option>
													</select>
												</c:if>
											</td>
											<td class="paymentFee">
											</td>
											
										</tr>
									</c:forEach>
								</tbody>
							</table>
							<div class="repair_box2_1" style="padding-bottom: 15px;">
								<span class="tit4">전체 이용기간 변경</span>
								<div class="select_type7 w139 mar38">
									<div>
										<select class="select form productAll" >
											<option value="1">1개월</option>
											<option value="3">3개월</option>
											<!-- <option value="6">6개월</option> -->
											<option value="12">12개월</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					
					</c:if>
					<c:if test="${productType eq 'geoFence'}">
					
						<div class="repair_box2 borB1">
							<div class="repair_box2_1">
								<span class="tit3" style="padding-left: 0px !important">현재 사용중</span>						
								<div class="user_txt1">${VDAS.geoMaxCnt}개</div>
							</div>
							<table class="table1 mgt10 tab1" id="infoTable">
								<colgroup>
									<col width="*">
									<col width="150">
									<col width="100">
								</colgroup>
								<thead>			
									<tr>
										<th>부가서비스</th>
										<th>추가 수량</th>
										<th>이용 요금</th>
									</tr>
								</thead>
								<tbody>
									<tr class="f">
										<td>
											<b>운행스케쥴 거래처 추가 서비스</b> 
										</td>
										<td>
											<b id="txt_cnt">10개</b> 
											<input type="hidden" id="input_cnt" value="1"/>
											<a id="btn_minus" href="#none" class="" ><img src="${pathCommon}/images/btn_minus.jpg" ></a>
											<a id="btn_plus" href="#none" class="" ><img src="${pathCommon}/images/btn_puls.jpg" ></a>
	 									</td>
										<td class="paymentFee">
											11,000
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					
					</c:if>
				
				<div class="total_payment" style="padding-left: 0px;"><b>결제할 금액</b><span id="totPay">0</span>원(부가세 포함)</div>
			</div>
		</div>
		<div class="repair_layout2 repair_layout2_5">
			<div class="repair_box1 repair_box1_2">
				<div class="tit">결제방식</div>
				<div class="repair_box2">
					<div class="repair_box2_1 repair_box2_2">
						<div class="payment_radio" id="selectPay" style="padding-left: 0px;">
							<input type="radio" name="payment" id="radio1" value="11" checked="checked"/>
							<label for="radio1">신용카드</label>
							<input type="radio" name="payment" id="radio2" value="21"/>
							<label for="radio2">계좌이체</label>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="btn_area2"><a href="javascript:window.history.back();" class="btn_cancel">취소</a><a href="javascript:f_start_pay();" class="btn_pay">결제하기</a></div>
	</div>
</div>
<!-- 차량선택 팝업 -->
<div class="black"></div>
