<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("M5003");

	
	$VDAS.http_post("/user/getFavoriteMenu.do",{},{
		success : function(r){
			var data = r.result;
			$("input[type=checkbox]").each(function(index){
				if($(this).val() == data.favo1)
					$(this).attr("checked", true);
				else if($(this).val() == data.favo2)
					$(this).attr("checked", true);
				else if($(this).val() == data.favo3)
					$(this).attr("checked", true);
				else if($(this).val() == data.favo4)
					$(this).attr("checked", true);
				else if($(this).val() == data.favo5)
					$(this).attr("checked", true);
				else if($(this).val() == data.favo6)
					$(this).attr("checked", true);
			});
			console.log(r);
		}
	});
	
	$("body").on("click",".btn_setting",function(){
		if($("input[type=checkbox]:checked").length <= 6){
			var selectMenu = "";
			$("input[type=checkbox]").each(function(index){
				if($(this).is(":checked"))
					selectMenu += $(this).val() + ",";
			});
			
			$VDAS.http_post("/user/updateFavoriteMenu.do",{selectMenu:selectMenu},{
				success : function(){
					$VDAS.instance_post("/main/index.do",{});
				}
			});
		}
		else
			alert("선택 가능한 메뉴는 최대 6개입니다.");
	});
	
	$("body").on("click",".btn_cancel",function(){
		$VDAS.instance_post("/main/index.do",{});
	});
	
});


</script>
<div class="right_layout">
	<h2 class="tit_sub">
		자주 사용하는 메뉴 설정
		<p>홈페이지 상단에 자주 사용하는 메뉴를 설정하여 편리하게 서비스를 이용하실 수 있습니다.</p>
	</h2>
	<div class="sub_top1">
		<span>자주 사용할 메뉴는 해당메뉴의 오른쪽 체크박스를 클릭하시고 확인버튼을 누르면 설정됩니다. 최대 6개까지 설정가능합니다.</span>
	</div>
	<h3 class="h3tit">운행관리</h3>
	<table class="table2">
		<colgroup>
			<col style="width:127px;" />
			<col />
			<col style="width:104px;" />
		</colgroup>
		<thead>
			<tr>
				<th>메뉴얼</th>
				<th>메뉴설명</th>
				<th>자주사용 설정</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th><b>차량·사용자</b></th>
				<th>등록된 차량·사용자의 상태를 실시간으로 확인할 수 있습니다.</th>
				<td><input type="checkbox" id="m1l1" value="M1001"/></td>
			</tr>
			<tr>
				<th><b>운행기록 조회</b></th>
				<th>등록차량의 특정시점 사용자 및 운행 기록을 조회하실 수 있습니다.</th>
				<td><input type="checkbox" id="m1l2" value="M1002"/></td>
			</tr>
			<tr>
				<th><b>차량위치 조회</b></th>
				<th>실시간 차량위치, 상태 정보 및 여러 대의 차량을 동시에 모니터링 할 수 있습니다.</th>
				<td><input type="checkbox" id="m1l3" value="M1003"/></td>
			</tr>
			<tr>
				<th><b>이수지역 확인</b></th>
				<th>소속 직원의 이수지역 정보를 확인할 수 있습니다.</th>
				<td><input type="checkbox" id="m1l4" value="M1004"/></td>
			</tr>
			<tr>
				<th><b>사고차량 조회</b></th>
				<th>등록차량의 중대 교통사고를 조회ㆍ관리하고 사고시 긴급출동기관으로 신고합니다.</th>
				<td><input type="checkbox" id="m1l5" value="M1005"/></td>
			</tr>
		</tbody>
	</table>
	<h3 class="h3tit">차량관리</h3>
	<table class="table2">
		<colgroup>
			<col style="width:127px;" />
			<col />
			<col style="width:104px;" />
		</colgroup>
		<thead>
			<tr>
				<th>메뉴얼</th>
				<th>메뉴설명</th>
				<th>자주사용 설정</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th><b>차량 보고서</b></th>
				<th>차량보고서 조회ㆍ제출 사용차량의 운행내역ㆍ경비보고 또는 반납을 위한 보고서를제출합니다.</th>
				<td><input type="checkbox" id="m2l1" value="M2001"/></td>
			</tr>
			<tr>
				<th><b>차량 경비</b></th>
				<th>등록차량의 모든 경비내역을 관리합니다.</th>
				<td><input type="checkbox" id="m2l2" value="M2002"/></td>
			</tr>
			<tr>
				<th><b>벌금/과태료</b></th>
				<th>등록차량의 운행중 발생한 벌점·과태료를 틍록하거나 조회합니다.</th>
				<td><input type="checkbox" id="m2l3" value="M2003"/></td>
			</tr>
		</tbody>
	</table>
	<h3 class="h3tit">차량정비</h3>
	<table class="table2">
		<colgroup>
			<col style="width:127px;" />
			<col />
			<col style="width:104px;" />
		</colgroup>
		<thead>
			<tr>
				<th>메뉴얼</th>
				<th>메뉴설명</th>
				<th>자주사용 설정</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th><b>정비현황</b></th>
				<th>등록차량의 차량 정비 및 소모품 현황을 조회·등록할 수 있습니다.</th>
				<td><input type="checkbox" id="m3l1" value="M3001"/></td>
			</tr>
			<tr>
				<th><b>고장ㆍ소모품 현황</b></th>
				<th>등록차량의 고장 및 소모품 교체 알림내역을 조회할 수 있습니다.</th>
				<td><input type="checkbox" id="m3l2" value="M3002"/></td>
			</tr>
		</tbody>
	</table>
	<h3 class="h3tit">리포트</h3>
	<table class="table2">
		<colgroup>
			<col style="width:127px;" />
			<col />
			<col style="width:104px;" />
		</colgroup>
		<thead>
			<tr>
				<th>메뉴얼</th>
				<th>메뉴설명</th>
				<th>자주사용 설정</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th><b>회사 통합 보고서</b></th>
				<th>회사 모든 차량의 운영내역을 기간별로 확인할 수 있습니다.</th>
				<td><input type="checkbox" id="m4l1" value="M4001"/></td>
			</tr>
			<tr>
				<th><b>기간별 차량 비교</b></th>
				<th></th>
				<td><input type="checkbox" id="m4l2" value="M4002"/></td>
			</tr>
			<tr>
				<th><b>국세청 운행일지</b></th>
				<th>별도의 작성 없이 운행일지를 간편하게 다운로드 할 수 있습니다.</th>
				<td><input type="checkbox" id="m4l3" value="M4003"/></td>
			</tr>
		</tbody>
	</table>
	<div class="btn_area2"><a href="#" class="btn_cancel">취소</a><a href="#none" class="btn_setting">설정완료</a>
	<br><br><br><br>
</div>