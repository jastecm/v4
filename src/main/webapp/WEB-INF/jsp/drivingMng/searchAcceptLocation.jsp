<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<fmt:setLocale value="${_LANG}" scope="request" />
<fmt:setBundle basename="properties.messages.rest.message" />
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<c:if test="${VDAS.rule eq '0' or VDAS.rule eq '1' }">
<script type="text/javascript">
var lazy;
var lazyUser;
$(document).ready(function(){
	//alert("todo - ");
	
	initMenuSel("M1004");

	$("#dialog").hide();
    
	$(".btn_search").on("click",function(){
		$("#searchFrm input[name=searchDate]").val(replaceAll($("#searchDate").val(),"/",""));
		
		$.lazyLoader.search(lazy);
	});
	
	$("#searchText").on("keyup",function(e){
		if(e.keyCode == 13) $(".btn_search").trigger("click");
	});

	$VDAS.ajaxCreateSelect("/com/getCityLev1.do",{},$("#cityLev1"),false,"<fmt:message key="searchacceptlocation.cityLev1_1"/>","","${_LANG}",false,null);

	$("#cityLev1").on("change",function(){
		$VDAS.ajaxCreateSelect("/com/getCityLev2.do",{city:$(this).val()},$("#cityLev2"),false,"<fmt:message key="searchacceptlocation.cityLev2_1"/>","","${_LANG}",false,null);	
		
	});
	
	$("#searchOrder").on("change",function(){
		$.lazyLoader.search(lazy);
	});
	
	if(!lazy){
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
				cityLev1 : $("#cityLev1")
				,cityLev2 : $("#cityLev2")
				,searchText : $("#searchText")
				,searchOrder : $("#searchOrder")
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/driving/getDrivingList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.rtvList;
				
				var strHtml = '';
				
				console.log(r);
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += '<tr>';
					strHtml += '<td class="car_list">';
					strHtml += '<strong class="car_img"><img src="${pathCommon}/images/vehicle/'+vo.imgS+'.png" alt=""></strong>';
					strHtml += '<a href="#" class="btn_vehicleDetail" data-key="'+vo.vehicleKey+'">';
					strHtml += '<span>'+vo.modelMaster+'</span>';
					strHtml += ' '+vo.plateNum;
					strHtml += '</a></td>';
					strHtml += '<td>';
					strHtml += '<a href="#" class="btn_accountDetail" data-key="'+vo.accountKey+'">';
					strHtml += '<span>'+convertNullString(vo.driverGroupNm)+'/</span>';
					strHtml += ' '+convertNullString(vo.driverNm);
					strHtml += '</a></td>';
					strHtml += '<td>';
					strHtml += '<div class="start_end txt_left">';
					strHtml += '<p><b><fmt:message key='searchacceptlocation.start'/> </b><span title="'+convertNullString(vo.startAddr)+'" style="font-weight:500;">'+convertNullString(vo.startDate)+' '+addrString(convertNullString(vo.startAddr))+'</span></p>';
					strHtml += '<p><b><fmt:message key='searchacceptlocation.end'/> </b><span title="'+convertNullString(vo.endAddr)+'" style="font-weight:500;">'+convertNullString(vo.endDate)+' '+addrString(convertNullString(vo.endAddr))+'</span></p>';
					strHtml += '</div>';
					strHtml += '</td>';
					strHtml += '<td>';
					strHtml += '<input type="hidden" class="startInfo" value="'+convertNullString(vo.startDate)+' '+convertNullString(vo.startAddr)+'">';
					strHtml += '<input type="hidden" class="endInfo" value="'+convertNullString(vo.endDate)+' '+convertNullString(vo.endAddr)+'">';
					strHtml += '<a href="#none" class="btn_pop" data-gps="'+vo.tripKey+'"><img src="${pathCommon}/images/gps.jpg" alt=""></a>';
					strHtml += '</td></tr>';
				}
				
				return strHtml;
				

				//<em class="color_red unline">이탈</em> <img src="images/m_img3.png" alt="" />
				//<em class="color_green">정상</em>
			}
		});
	}
    $(".showAccept").click(function() {
        $( "#dialog" ).dialog({
            width:800,
            dialogClass:'alert',
            draggable: false,
            modal:true,
            height:"auto",
            resizable:false,
            closeOnEscape:true,
            buttons:{
                '확인':function(){
                    $(this).dialog("close");
                },
                // 'cancel':function(){
                // 	$(this).dialog("close");
                // },

            }
        });
        $('.pop_close').click(function(){
            $(this).parent().dialog("close");
        });
    });
    
	$("body").on("click",".btn_vehicleDetail",function(){
		var key = $(this).data("key");
		$VDAS.instance_post("/drivingMng/searchVehicleDetail.do",{vehicleKey:key});
	});
    
	$("body").on("click",".btn_accountDetail",function(){
		var key = $(this).data("key");
		$VDAS.instance_post("/drivingMng/searchUserDetail.do",{accountKey:key});
	});

	$("body").on("click",".btn_userDetail",function(){
		$VDAS.instance_post("/drivingMng/searchUserDetail.do",{});
	});

	$("#acceptLocationMap").attr("src","${path}/map/trip.do?searchType=default");
	$("body").on("click",".btn_pop",function(){
		$("#popStart").text($(this).parent("td").find(".startInfo").val());
		$("#popEnd").text($(this).parent("td").find(".endInfo").val());
		
		$("#acceptLocationMap").get(0).contentWindow.searchType = "trip";
		$("#acceptLocationMap").get(0).contentWindow.tripKey = $(this).data("gps");
		$("#acceptLocationMap").get(0).contentWindow._o = $("#acceptLocationMap").get(0).contentWindow.createOption();
		$("#acceptLocationMap").get(0).contentWindow.outerReset();
		
		$(".pop9").fadeIn();
		$(".black").fadeIn();
	});
});


function addrString(a){
	var result = '';

	for(var i=0;i < a.length;i++){
		if(i >= 18){
			result += '...';
			break;
		}
		else
			result += a[i];
	}
		
	return result;
}

</script>
<form id="searchFrm" style="display:none">
	<input type="hidden" name="searchDate" value="" />
	<input type="hidden" name="cityLev1" value="" />
	<input type="hidden" name="cityLev2" value="" />
	<input type="hidden" name="searchText" value="" />
	<input type="hidden" name="searchType" value="plateNum" />
	<input type="hidden" name="searchOrder" value="" />
	<input type="hidden" name="workingArea" value="1" />
</form>
<div class="right_layout">
	<h2 class="tit_sub"><fmt:message key='searchacceptlocation.txt1'/>
		<p><fmt:message key='searchacceptlocation.txt1_1'/></p>
	</h2>
	<div class="sub_top1">
		<strong id="nowDate"></strong>
	</div>
	<div class="search_box1 mgb0">
		<form id="frmSearch" method="post" onSubmit="return false;">
			<input type="text" id="searchDate" class="date datePicker" style="width:85px; height:36px; border:1px solid #c0c0c0;"/>
			<select class="select form" style="width:160px;" id="cityLev1">
				<option value=""><fmt:message key='searchacceptlocation.cityLev1'/></option>
			</select>
			<select class="select form" style="width:160px;" id="cityLev2">
				<option value=""><fmt:message key='searchacceptlocation.cityLev2'/></option>
			</select>
			<div class="search_box1_2" style="width:200px;">
				<input type="text" class="search_input" id="searchText"/>
			</div><a href="#" class="btn_search"><fmt:message key='searchacceptlocation.search'/></a>
		</form>
	</div>
	
	<div class="tab_layout">
		<div class="mgt30">
			<select id="searchOrder" class="select form" style="width:105px;">
				<option value="driving"><fmt:message key='searchacceptlocation.orderDrving'/></option>
				<option value="regDate"><fmt:message key='searchacceptlocation.orderReg'/></option>
			</select>
		</div>
		<table class="table1 pd_table mgt10" id="contentsTable">
			<colgroup>
				<col style="width:195px;" />
				<col style="width:130px;" />
				<col />
				<col style="width:100px;" />
			</colgroup>
			<thead>
				<tr>
					<th><fmt:message key='searchacceptlocation.vehicleInfo'/></th>
					<th><fmt:message key='searchacceptlocation.user'/></th>
					<th><fmt:message key='searchacceptlocation.drivingInfo'/></th>
					<th><fmt:message key='searchacceptlocation.area'/></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div> 
<div class="black"></div>
<div class="pop9">
	<div class="pop_layout">
		<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt="" /></a>
		<h1><fmt:message key='searchacceptlocation.areaConfirm'/></h1>
		<h2 class="title3 pat42">
			<p><b><fmt:message key='searchacceptlocation.start1'/> </b><span id="popStart"></span></p>
			<p><b><fmt:message key='searchacceptlocation.end1'/> </b><span id="popEnd"></span></p>
		</h2>
		<div class="map_area">
			<iframe id="acceptLocationMap" src="#none" width="760px" height="339px"></iframe>
		</div>
		<div class="map_txt4"><fmt:message key='searchacceptlocation.txt2'/></div>
	</div>
</div>
</c:if>
<c:if test="${VDAS.rule ne '0' and VDAS.rule ne '1' }">
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("M1004");
});
</script>
<div class="right_layout">
	<img src="${pathCommon}/images/${_LANG}/authError.png" alt="" style="width:100%">
</div>
</c:if>