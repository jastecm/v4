<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script type="text/javascript">
var lazy;
var lazyUser;
$(document).ready(function(){
	alert("step view");
	
	initMenuSel("M1005");

	$(".btn_search").on("click",function(){
		alert("todo! - SEARCH");
		//$.lazyLoader.search(lazy);
	});
	
	
	$("#searchText").on("keyup",function(e){
		if(e.keyCode == 13) $(".btn_search").trigger("click");
	});
	
	if(!lazy){
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
				searchType : $("#searchType")
				,searchText : $("#searchText")
				,startDate : $("#startDate")
				,endDate : $("#endDate")
				,searchPeriod : $("#searchPeriod")
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/com/getVehicleList.do"
			,initSearch : true
			,createDataHtml : function(r){
				
				var strHtml = '';
				strHtml += '<tr>';
				strHtml += '<th class="car_list">';
				strHtml += '<a href="#" class="btn_vehicleDetail">';
				strHtml += '<span>아반떼HD</span>';
				strHtml += '54서 5022';
				strHtml += '</a></th>';
				strHtml += '<th>';
				strHtml += '<a href="#" class="btn_userDetail">';
				strHtml += '<span>아이피앤</span>';
				strHtml += '영업부/정준하';
				strHtml += '</a></th>';
				strHtml += '<td>남/A</td>';
				strHtml += '<td>010-123-4567</td>';
				strHtml += '<th>';
				strHtml += '<p><span>접수일시 </span>2015.12.01,13:00</p>';
				strHtml += '<p><span>사고장소 </span>서울시 광진구 구의동 15-1</p>';
				strHtml += '</th>';
				strHtml += '<td>';
				
				strHtml += '<input type="hidden" class="accidentDate" value="2015.12.01,13:00">';
				strHtml += '<input type="hidden" class="accidentLocation" value="서울시 광진구 구의동 15-1">';
				strHtml += '<a href="#none" class="btn_view_map"><img src="${pathCommon}/images/gps.jpg" alt="" /></a></td>';
				strHtml += '</tr>';
				
				return strHtml+strHtml+strHtml+strHtml+strHtml+strHtml+strHtml;
			}
		});
	}
    $(".showAccept").click(function() {
        $( "#dialog" ).dialog({
            width:800,
            dialogClass:'alert',
            draggable: false,
            modal:true,
            height:"auto",
            resizable:false,
            closeOnEscape:true,
            buttons:{
                '확인':function(){
                    $(this).dialog("close");
                },
                // 'cancel':function(){
                // 	$(this).dialog("close");
                // },

            }
        });
        $('.pop_close').click(function(){
            $(this).parent().dialog("close");
        });
    });
    
	$("body").on("click",".btn_vehicleDetail",function(){
		$VDAS.instance_post("/drivingMng/searchVehicleDetail.do",{});
	});

	$("body").on("click",".btn_userDetail",function(){
		$VDAS.instance_post("/drivingMng/searchUserDetail.do",{});
	});

	$("#accidentMap").attr("src","${path}/map/trip.do?searchType=default");
	$("body").on("click",".btn_view_map",function(){
		$("#popDate").text($(this).parent("td").find(".accidentDate").val());
		$("#popLocation").text($(this).parent("td").find(".accidentLocation").val());
		
		//$("#accidentMap").get(0).contentWindow.searchType = "trip";
		//$("#accidentMap").get(0).contentWindow.tripKey = $(this).data("gps");
		//$("#accidentMap").get(0).contentWindow._o = $("#accidentMap").get(0).contentWindow.createOption();
		//$("#accidentMap").get(0).contentWindow.outerReset();
		
		$(".pop9").fadeIn();
		$(".black").fadeIn();
	});
	
	$("body").on("click",".tab_select_btn1 a",function(){
		$(".tab_select_btn1 a").removeClass("on");
		$(this).addClass("on");
	});
});


</script>
<form id="searchFrm" style="display:none">
	<input type="hidden" name="startDate" value="" />
	<input type="hidden" name="endDate" value="" />
	<input type="hidden" name="searchPeriod" value="" />
	<input type="hidden" name="searchType" value="" />
	<input type="hidden" name="searchText" value="" />
</form>
<div class="right_layout">
	<h2 class="tit_sub">사고차량 조회
		<p>등록차량의 중대 교통사고를 조회ㆍ관리하고 사고시 긴급출동기관으로 신고합니다.</p>
	</h2>
	<div class="sub_top1">
		<span>등록차량수 <b>xx대</b></span><span>사고신고차량 <b>x대</b></span>
		<strong id="nowDate"></strong>
	</div>
	<div class="search_box1 mab0">
		<form id="frmSearch" method="post" onSubmit="return false;">
			<div class="search_box1_1">
				<div class="select_type1">
					<div class="select_type1_1">
						<select class="sel" id="searchType">
							<option value="">전체</option>
							<option value="plateNum">차량번호</option>
							<option value="modelMaster">차종</option>
							<option value="useNm">사용자명</option>
							<option value="drivingLocation">운행지역</option>
						</select>
					</div>
				</div><input type="text" class="search_input" id="searchText" />
			</div><a href="#" class="btn_search">조회</a>
		</form>
	</div>
	<div class="pat36">
		<div class="tab_select tab_select2">
			<span class="tab_select_btn1" id="searchPeriod">
				<a href="#" id="entire" class="on">전체</a>
				<a href="#" id="today">오늘</a>
				<a href="#" id="weekly">1주일</a>
				<a href="#" id="month">1개월</a>
				<a href="#" id="threemonth">3개월</a>
				<a href="#" id="halfyear">6개월</a>
			</span><a href="#none" class="btn_cal">직접입력</a>
			<div></div>
			<div class="cal_layout">
				<div class="cal_area">
					<input type="text" id="startDate" class="date datePicker" style="width:65px; height:25px; border:1px solid #c0c0c0;"/>
					<span>~</span>
					<input type="text" id="endDate" class="date datePicker" style="width:65px; height:25px; border:1px solid #c0c0c0;"/><br/>
				</div>
				<a href="#none" class="btn_enter">적용</a>
				<br/><br/><p>년,월,일순으로 입력 ex)2016.01.05</p>
			</div>
		</div>
	</div>
	<table class="table1" id="contentsTable">
		<colgroup>
			<col style="width:140px;" />
			<col style="width:150px;" />
			<col style="width:94px;" />
			<col style="width:110px;" />
			<col />
			<col style="width:84px;" />
		</colgroup>
		<thead>
			<tr>
				<th>차량정보</th>
				<th>사용자</th>
				<th>성별/혈액형</th>
				<th>연락처</th>
				<th>사고정보</th>
				<th>위치보기</th>
			</tr>
		</thead>	
		<tbody>
		</tbody>
	</table>
	<div class="excel">
		<a href="#" class="btn_excel" style="position: inherit;float:right;margin-top:15px;"><img src="${pathCommon}/images/${_LANG}/excel.jpg" alt="" /></a>
	</div>
	<div class="txt1">
		<h1>사고지역 위치보기 안내</h1>
		<p><img src="${pathCommon}/images/img1.png" alt="" /> 버튼을 클릭하면 사용자 휴대폰 App에 기록된 사고위치가 아래 지도에 표시됩니다.<br>사용자의 GPS 활용 미동의, GPS 오동작, 지하주차장 등과 GPS수신 상태로 고르지 못할 경우 위치가 정확하지 않을 수 있습니다.</p>
	</div>
</div>
<div class="black"></div>
<div class="pop9">
	<div class="pop_layout">
		<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt="" /></a>
		<h1>사고장소 위치</h1>
		<h2 class="title3 pat42">
			<p><b>접수일시 </b><span id="popDate"></span></p>
			<p><b>사고장소 </b><span id="popLocation"></span></p>
		</h2>
		<div class="map_area">
			<div id="map_canvas">
				<iframe id="accidentMap" src="#none" width="760px" height="339px"></iframe>
			</div>
		</div>
		<div class="map_txt4">사용자의 GPS 활용 미동의, GPS 오동작, 지하주차장 등과 GPS수신 상태로 고르지 못할 경우 위치가 정확하지 않을 수 있습니다.</div>
	</div>
</div>