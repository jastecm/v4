<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<fmt:setLocale value="${_LANG}" scope="request" />
<fmt:setBundle basename="properties.messages.rest.message" />
<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/index.js"></script>

<script type="text/javascript">
var lazy;
var lazyRoute;
var tWidth;

var tripKey;
var vehicleInfo;
var start;
var end;
var initSearch = true;
$(document).ready(function(){
	tWidth = Math.floor(($("#contentsTable").width() - 216) / 3) - 1;
	initMenuSel("M1003");
    
	var Now = new Date();
	var NowY = Now.getFullYear();
	var NowM = LPAD(''+(Now.getMonth()+1),'0',2);
	var NowD = LPAD(''+Now.getDate(),'0',2);
	
	$("#searchFrm_account input[name=searchMonth]").val(NowY+NowM);
	$("#searchMonth").val(NowY+"-"+NowM);
	
	$("#btnSearchVehicle").on("click",function(){
		$("#searchFrm_account input[name=searchMonth]").val(replaceAll($("#searchMonth").val(),"-",""));
		$("#myChart01").html('<div class="level"><span id="totalRank"></span></div>');
		drivingSummary();
		$.lazyLoader.search(lazy);
	});
	
	$(".tabItem").on("click",function(){
		$(".tabItem").removeClass("on");
		$(this).addClass("on");
		
		$(".div1").hide();
		var tab = $(this).data("tab");
		$("."+tab).show();
		
		if(tab == 'driving') $.lazyLoader.search(lazy);
		else {
			if($("#contentsTablePop input[type='radio']").eq(0).attr("checked","checked").length > 0 && initSearch){
				$(".btn_accept").trigger("click");
				initSearch = false;
			}
			//$("#drivingMap").get(0).contentWindow.searchType = "trip";
			//$("#drivingMap").get(0).contentWindow.tripKey = selectKey;
			//$("#drivingMap").get(0).contentWindow._o = $("#drivingMap").get(0).contentWindow.createOption();
			//$("#drivingMap").get(0).contentWindow.outerReset();
		}
		
		//$.lazyLoader.search(lazy);
	});
    
	if(!lazy){
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm_account") // form을 3개로 분할
			,searchFrmVal : {
				searchAccountKey : $("#accountKey")
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/driving/getDrivingList.do"
			,initSearch : true
			,createDataHtml : function(r){
				console.log(r);
				
				var strHtml = '';
				var data = r.rtvList;
				
				var hasScrollPx = 0;
				if($("#contentsTable tbody tr").length+data.length > 2) hasScrollPx = 17;
				
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += '<tr>';
					strHtml += '<td width="80px">';
					strHtml += '<div class="table1_box1">';
					strHtml += '<img src="${pathCommon}/images/vehicle/'+vo.imgS+'.png" alt="" />';
					strHtml += '</div>';
					strHtml += '<div class="table1_box2">';
					strHtml += '<img src='+userImg(vo.driverImg)+' alt="" />';
					strHtml += '</div>';
					strHtml += '</td>';
					strHtml += '<td width="120px">';
					strHtml += '<div class="table1_box1 txt_left">';
					strHtml += '<a href="#none" class="a_vehicleDetail" data-key="'+vo.vehicleKey+'">';
					strHtml += '<span>'+vo.modelMaster+'</span>';
					strHtml += ' '+vo.plateNum;
					strHtml += '</a>';
					strHtml += '</div>';
					strHtml += '<div class="table1_box2 txt_left">';
					strHtml += '<a href="#none" class="a_user">';
					strHtml += '<span>'+convertNullString(vo.driverGroupNm)+'/</span>';
					strHtml += ' '+convertNullString(vo.driverNm);
					strHtml += '</a>';
					strHtml += '<br>';
					strHtml += '<br>';
					if(vo.instantAllocate == '1')
						strHtml += '<a href="#none" class="btn btn_edit"><fmt:message key='searchuserdetail.change'/></a>';
					strHtml += '</div>';
					strHtml += '</td>';
					strHtml += '<td colspan="3">';
					strHtml += '<div class="table1_box1 txt_left">';
					strHtml += '<input type="hidden" class="startInfo" value="'+convertNullString(vo.startDate)+' '+convertNullString(vo.startAddr)+'">';
					strHtml += '<input type="hidden" class="startInfoDate" value="'+convertNullString(vo.startDate)+'">';
					strHtml += '<input type="hidden" class="endInfo" value="'+convertNullString(vo.endDate)+' '+convertNullString(vo.endAddr)+'">';
					strHtml += '<input type="hidden" class="vehicleInfo" value="'+convertNullString(vo.modelMaster)+'/'+convertNullString(vo.plateNum)+'">';
					if(convertNullString(vo.startAddr) == ''){
						if(vo.tripDetailState == 1){
							if("${VDAS.lang}" == "ko") vo.startAddr = '주행중';
							else vo.startAddr = 'now Driving';
						}else if(vo.tripDetailState == 2){
							if("${VDAS.lang}" == "ko") vo.startAddr = '주행정보를 생성중입니다.';
							else vo.startAddr = 'creating driving info';
						}else if(vo.tripDetailState == 3){
							if("${VDAS.lang}" == "ko") vo.startAddr = '주행정보를 생성중입니다.';
							else vo.startAddr = 'creating driving info';
						}
					}
					if(convertNullString(vo.endAddr) == ''){
						if(vo.tripDetailState == 1){
							if("${VDAS.lang}" == "ko") vo.endAddr = '주행중';
							else vo.endAddr = 'now Driving';
						}else if(vo.tripDetailState == 2){
							if("${VDAS.lang}" == "ko") vo.endAddr = '주행정보를 생성중입니다.';
							else vo.endAddr = 'creating driving info';
						}else if(vo.tripDetailState == 3){
							if("${VDAS.lang}" == "ko") vo.endAddr = '주행정보를 생성중입니다.';
							else vo.endAddr = 'creating driving info';
						}
					}
					strHtml += '<p><b><fmt:message key='searchuserdetail.start'/> </b><span title="'+convertNullString(vo.startAddr)+'" style="font-weight:500;">'+convertNullString(vo.startDate)+' '+addrString(convertNullString(vo.startAddr))+'</span></p>';
					strHtml += '<p><b><fmt:message key='searchuserdetail.end'/> </b><span title="'+convertNullString(vo.endAddr)+'" style="font-weight:500;">'+convertNullString(vo.endDate)+' '+addrString(convertNullString(vo.endAddr))+'</span></p>';
					var gpsRightPx = 117-hasScrollPx;
					strHtml += '<a href="#none" style="right:'+gpsRightPx+'px;" class="btn_gps type02" data-gps="'+vo.tripKey+'" data-vehicle="'+vo.vehicleKey+'" data-account="'+vo.accountKey+'"><img src="${pathCommon}/images/gps.jpg" alt=""></a>';
					/* if((vo.workingAreaWarning == null || vo.workingAreaWarning == '')
							&& (vo.workingTimeWarning == null || vo.workingTimeWarning == ''))
						strHtml += '<div class="p_time"><p>-&nbsp;&nbsp;&nbsp;&nbsp;</p></div>';
					else if((vo.workingAreaWarning != null && vo.workingAreaWarning != '')
							&& (vo.workingTimeWarning == null || vo.workingTimeWarning == ''))
						strHtml += '<div class="p_time"><p class="t3">이수지역</p></div>';
					else if((vo.workingAreaWarning == null || vo.workingAreaWarning == '')
							&& (vo.workingTimeWarning != null && vo.workingTimeWarning != ''))
						strHtml += '<div class="p_time"><p class="t3">업무시간</p></div>';
					else if((vo.workingAreaWarning != null && vo.workingAreaWarning != '')
							&& (vo.workingTimeWarning != null && vo.workingTimeWarning != '')){
						strHtml += '<div class="p_time"><p class="t3">이수지역<br>업무시간</p></div>';
					} */
					var stateChk = true;
					
					if((vo.workingAreaWarning == null || vo.workingAreaWarning == '')
							&& (vo.workingTimeWarning == null || vo.workingTimeWarning == '')
							&& (vo.dtcCnt == null || vo.dtcCnt == '0')){

						var px = 40-hasScrollPx;
						strHtml += '<div class="p_time" style="right:'+px+'px;"><p>-</p></div>';
						stateChk = false;
					}
					
					if(stateChk){
						var first = true;
						var strState = "";
						
						if(vo.dtcCnt != null && vo.dtcCnt != '0'){
						//if(true){
							if(!first) strState += '<br>'; 
							strState += "<fmt:message key="searchuserdetail.trouble"/>";
							first = false;
						}
						//if(true){
						if(vo.workingAreaWarning != null && vo.workingAreaWarning != ''){
							if(!first) strState += '<br>';
							strState += "<fmt:message key="searchuserdetail.area"/>";
							first = false;
						}
						if(vo.workingTimeWarning != null && vo.workingTimeWarning != ''){
						//if(true){
							if(!first) strState += '<br>';
							strState += "<fmt:message key="searchuserdetail.workingTime"/>";
							first = false;
						}
						
						var rightPx = 22-hasScrollPx;
						if(strState.length == 2) rightPx = 34-hasScrollPx; 
						strHtml += '<div class="p_time" style="right:'+rightPx+'px;"><p class="t3" >'+strState+'</p></div>';
					}
					strHtml += '</div>';
					strHtml += '<div class="table1_box2 txt_left">';
					strHtml += '<div>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchuserdetail.safeScore'/> </b>'+calScore(parseFloat(vo.safeScore).toFixed(1),"${_LANG}")+'</span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchuserdetail.fuelEfficiency'/> </b>'+calScore((vo.avgFco/vo.fuelEfficiency*100).toFixed(1),"${_LANG}")+'</span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchuserdetail.ecoScore'/> </b>'+calScore(parseFloat(vo.ecoScore).toFixed(1),"${_LANG}")+'</span>';
					strHtml += '</div>';
					strHtml += '</div>';
					strHtml += '<div class="table1_box2 txt_left">';
					strHtml += '<div>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchuserdetail.drivingTime'/> </b>'+secondToTime(vo.drivingTime,"${_LANG}")+'</span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchuserdetail.meanSpeed'/> </b>'+vo.meanSpeed+' km/h</span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchuserdetail.coolantTemp'/> </b>'+vo.coolantTemp+' ℃</span>';
					strHtml += '</div>';
					strHtml += '<div>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchuserdetail.distance'/> </b>'+minusError(number_format((vo.distance/1000).toFixed(1)))+' km</span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchuserdetail.highSpeed'/> </b>'+vo.highSpeed+' km/h</span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchuserdetail.ecuVolt'/> </b>'+number_format(parseFloat(vo.ecuVolt).toFixed(1))+' v</span>';
					strHtml += '</div>';
					strHtml += '<div>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchuserdetail.avgFco'/> </b>'+number_format(parseFloat(vo.avgFco).toFixed(1))+' km/ℓ</span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchuserdetail.fco'/> </b>'+number_format((vo.fco/1000).toFixed(1))+' ℓ</span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchuserdetail.deviceVolt'/> </b>'+number_format(parseFloat(vo.deviceVolt).toFixed(1))+' v</span>';
					strHtml += '</div>';
					strHtml += '<div>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchuserdetail.co2Emission'/> </b>'+number_format(vo.co2Emission)+' ㎎</span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchuserdetail.fuelCutTime'/> </b>'+number_format(vo.fuelCutTime)+' <fmt:message key='searchuserdetail.second1'/></span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchuserdetail.idleTime'/> </b>'+number_format(vo.idleTime)+' <fmt:message key='searchuserdetail.second2'/></span>';
					strHtml += '</div>';
					strHtml += '<div>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchuserdetail.warmupTime'/> </b>'+number_format(vo.warmupTime)+' <fmt:message key='searchuserdetail.second3'/></span>';
					/* strHtml += '<span style="width:'+tWidth+'px;"><b style="font-size:smaller;">엔진오일온도 </b>'+parseFloat(vo.engineOilTemp).toFixed(1)+' ℃</span>'; */
					strHtml += '</div>';
					if(vo.tripDetailState == '1'){
						strHtml += '<div>';
						strHtml += '<span style="width:100%"><b style="width:100%;color:red;"><fmt:message key='searchuserdetail.txt1'/></b></span>';
						strHtml += '</div>';
					}
					if(vo.tripDetailState == '2' || vo.tripDetailState == '3'){
						strHtml += '<div>';
						strHtml += '<span style="width:100%"><b style="width:100%;color:red;"><fmt:message key='searchuserdetail.txt2'/></b></span>';
						strHtml += '</div>';	
					}
					strHtml += '</div>';
					strHtml += '</td>';
					strHtml += '</tr>';
				}
				return strHtml;

			}
		});
	}

	var popWidth = Math.floor(($("#contentsTablePop").width() - 201) / 3) - 1;
	if(!lazyRoute)
		lazyRoute = $("#contentsTablePop").lazyLoader({
			searchFrmObj : $("#searchFrm_route")
			,searchFrmVal : {
				searchAccountKey : $("#accountKey")
				,searchVehicleState : $("#popState")
				,searchWarning : $("#popWarning")
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/driving/getDrivingList.do"
			,initSearch : true
			,createDataHtml : function(r){
				console.log(r);

				var strHtml = '';
				var data = r.rtvList;
				var hasScrollPx = 0;
				if($("#contentsTablePop tbody tr").length+data.length > 2) hasScrollPx = 17;
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += '<tr>';
					strHtml += '<td width="121px">';
					strHtml += '<div class="table1_box2 txt_left">';
					strHtml += '<a href="#none" class="a_vehicleDetail" data-key="'+vo.vehicleKey+'">';
					strHtml += '<span>'+vo.modelMaster+'</span>';
					strHtml += ' '+vo.plateNum;
					strHtml += '</a>';
					strHtml += '</div>';
					strHtml += '</td>';
					strHtml += '<td colspan="1">';
					strHtml += '<div class="table1_box1 txt_left">';
					strHtml += '<p style="max-width:550px"><b><fmt:message key='searchuserdetail.start1'/> </b><span title="'+convertNullString(vo.startAddr)+'" style="font-weight:500;">'+convertNullString(vo.startDate)+' '+convertNullString(vo.startAddr)+'</span></p>';
					strHtml += '<p style="max-width:550px"><b><fmt:message key='searchuserdetail.end1'/> </b><span title="'+convertNullString(vo.endAddr)+'" style="font-weight:500;">'+convertNullString(vo.endDate)+' '+convertNullString(vo.endAddr)+'</span></p>';
					/* if((vo.workingAreaWarning == null || vo.workingAreaWarning == '')
							&& (vo.workingTimeWarning == null || vo.workingTimeWarning == ''))
						strHtml += '<div class="p_time"><p>-&nbsp;&nbsp;&nbsp;&nbsp;</p></div>';
					else if((vo.workingAreaWarning != null && vo.workingAreaWarning != '')
							&& (vo.workingTimeWarning == null || vo.workingTimeWarning == ''))
						strHtml += '<div class="p_time"><p class="t3">이수지역</p></div>';
					else if((vo.workingAreaWarning == null || vo.workingAreaWarning == '')
							&& (vo.workingTimeWarning != null && vo.workingTimeWarning != ''))
						strHtml += '<div class="p_time"><p class="t3">업무시간</p></div>';
					else if((vo.workingAreaWarning != null && vo.workingAreaWarning != '')
							&& (vo.workingTimeWarning != null && vo.workingTimeWarning != '')){
						strHtml += '<div class="p_time"><p class="t3">이수지역<br>업무시간</p></div>';
					} */
					
					strHtml += '</div>';
					strHtml += '<div class="table1_box2 txt_left">';
					strHtml += '<div>';
					strHtml += '<span style="width:'+popWidth+'px;"><b><fmt:message key='searchuserdetail.safeScore1'/> </b>'+calScore(parseFloat(vo.safeScore).toFixed(1),"${_LANG}")+'</span>';
					strHtml += '<span style="width:'+popWidth+'px;"><b><fmt:message key='searchuserdetail.fuelEfficiency1'/> </b>'+calScore((vo.avgFco/vo.fuelEfficiency*100).toFixed(1),"${_LANG}")+'</span>';
					strHtml += '<span style="width:'+popWidth+'px;"><b><fmt:message key='searchuserdetail.ecoScore1'/> </b>'+calScore(parseFloat(vo.ecoScore).toFixed(1),"${_LANG}")+'</span>';
					strHtml += '</div>';
					strHtml += '</div>';
					strHtml += '<div class="table1_box2 txt_left">';
					strHtml += '<div>';
					strHtml += '<span style="width:'+popWidth+'px;"><b><fmt:message key='searchuserdetail.drivingTime1'/> </b>'+secondToTime(vo.drivingTime,"${_LANG}")+'</span>';
					strHtml += '<span style="width:'+popWidth+'px;"><b><fmt:message key='searchuserdetail.meanSpeed1'/> </b>'+vo.meanSpeed+' km/h</span>';
					strHtml += '<span style="width:'+popWidth+'px;"><b><fmt:message key='searchuserdetail.coolantTemp1'/> </b>'+vo.coolantTemp+' ℃</span>';
					strHtml += '</div>';
					strHtml += '<div>';
					strHtml += '<span style="width:'+popWidth+'px;"><b><fmt:message key='searchuserdetail.distance1'/> </b>'+minusError(number_format((vo.distance/1000).toFixed(1)))+' km</span>';
					strHtml += '<span style="width:'+popWidth+'px;"><b><fmt:message key='searchuserdetail.highSpeed1'/> </b>'+vo.highSpeed+' km/h</span>';
					strHtml += '<span style="width:'+popWidth+'px;"><b><fmt:message key='searchuserdetail.ecuVolt1'/> </b>'+number_format(parseFloat(vo.ecuVolt).toFixed(1))+' v</span>';
					strHtml += '</div>';
					strHtml += '<div>';
					strHtml += '<span style="width:'+popWidth+'px;"><b><fmt:message key='searchuserdetail.avgFco1'/> </b>'+number_format(parseFloat(vo.avgFco).toFixed(1))+' km/ℓ</span>';
					strHtml += '<span style="width:'+popWidth+'px;"><b><fmt:message key='searchuserdetail.fco1'/> </b>'+number_format((vo.fco/1000).toFixed(1))+' ℓ</span>';
					strHtml += '<span style="width:'+popWidth+'px;"><b><fmt:message key='searchuserdetail.deviceVolt1'/> </b>'+number_format(parseFloat(vo.deviceVolt).toFixed(1))+' v</span>';
					strHtml += '</div>';
					strHtml += '<div>';
					strHtml += '<span style="width:'+popWidth+'px;"><b><fmt:message key='searchuserdetail.co2Emission1'/> </b>'+number_format(vo.co2Emission)+' ㎎</span>';
					strHtml += '<span style="width:'+popWidth+'px;"><b><fmt:message key='searchuserdetail.fuelCutTime1'/> </b>'+number_format(vo.fuelCutTime)+' <fmt:message key='searchuserdetail.second4'/></span>';
					strHtml += '<span style="width:'+popWidth+'px;"><b><fmt:message key='searchuserdetail.idleTime1'/> </b>'+number_format(vo.idleTime)+' <fmt:message key='searchuserdetail.second5'/></span>';
					strHtml += '</div>';
					strHtml += '<div>';
					strHtml += '<span style="width:'+popWidth+'px;"><b><fmt:message key='searchuserdetail.warmupTime1'/> </b>'+number_format(vo.warmupTime)+' <fmt:message key='searchuserdetail.second6'/></span>';
					/* strHtml += '<span style="width:'+tWidth+'px;"><b style="font-size:smaller;">엔진오일온도 </b>'+parseFloat(vo.engineOilTemp).toFixed(1)+' ℃</span>'; */
					strHtml += '</div>';
					if(vo.tripDetailState == '1'){
						strHtml += '<div>';
						strHtml += '<span style="width:100%"><b style="width:100%;color:red;"><fmt:message key='searchuserdetail.txt3'/></b></span>';
						strHtml += '</div>';
					}
					if(vo.tripDetailState == '2' || vo.tripDetailState == '3'){
						strHtml += '<div>';
						strHtml += '<span style="width:100%"><b style="width:100%;color:red;"><fmt:message key='searchuserdetail.txt4'/></b></span>';
						strHtml += '</div>';	
					}
					strHtml += '</div>';
					strHtml += '</td>';
					strHtml += '<td width="64px">';
					strHtml += '<input type="radio" name="selectOtherRoute" data-start="'+convertNullString(vo.startDate)+' '+convertNullString(vo.startAddr)+'" data-end="'+convertNullString(vo.endDate)+' '+convertNullString(vo.endAddr)+'" data-gps="'+vo.tripKey+'" data-vehicle="'+vo.modelMaster+'/'+vo.plateNum+'">';
					strHtml += '</td>';
					strHtml += '</tr>';
				}
				return strHtml;
			}
		});
	
	$("body").on("click",".a_vehicleDetail",function(){
		var key = $(this).data("key");
		$VDAS.instance_post("/drivingMng/searchVehicleDetail.do",{vehicleKey:key});
	});
	
	$("body").on("click","#searchPeriod a",function(){
		$("#searchPeriod a").removeClass("on");
		$(this).addClass("on");
	});
	
	$("body").on("click","#searchPeriod2 a",function(){
		$("#searchPeriod2 a").removeClass("on");
		$(this).addClass("on");
	});
	
	$("body").on("click",".btn_other",function(){	
		$.lazyLoader.search(lazyRoute);	
		$(".list").fadeIn();
		$(".black").fadeIn();
	});

	$("#accountMap").attr("src","${path}/map/trip.do?searchType=default");
	$("body").on("click",".btn_detail",function(){
		tripKey = $(this).data("gps");
		star = $(this).data("start");
		end = $(this).data("end");
		vehicleInfo = $(this).data("vehicle");

		$("#popStart").text(star);
		$("#popEnd").text(end);
		$("#popVehicle").text(vehicleInfo);
		
		$("#accountMap").get(0).contentWindow.searchType = "trip";
		$("#accountMap").get(0).contentWindow.tripKey = tripKey;

		$(".accountMap").fadeIn();
		$(".black").fadeIn();
		
		$("#accountMap").get(0).contentWindow._o = $("#accountMap").get(0).contentWindow.createOption();
		$("#accountMap").get(0).contentWindow.outerReset();
	});

	$("#drivingMap").attr("src","${path}/map/trip.do?searchType=default");
	$("body").on("click",".btn_accept",function(){
		if($("input[name=selectOtherRoute]:checked").length > 0){
			var _this = $("input[name=selectOtherRoute]:checked").eq(0);			
			tripKey = _this.data("gps");
			vehicleInfo = _this.data("vehicle");
			start = _this.data("start");
			end = _this.data("end"); 
			
			$("#routeStart").text(convertNullString(start));
			$("#routeEnd").text(convertNullString(end));
			$("#routeVehicle").text(vehicleInfo);
			
			$("#drivingMap").get(0).contentWindow.searchType = "trip";
			$("#drivingMap").get(0).contentWindow.tripKey = tripKey;
			$("#drivingMap").get(0).contentWindow._o = $("#drivingMap").get(0).contentWindow.createOption();
			$("#drivingMap").get(0).contentWindow.outerReset();

			$(".list").fadeOut();
			$(".black").fadeOut();
		}else{
			alert("<fmt:message key="searchuserdetail.txt5"/>");
		}
			
		
		
	});
	
	$("#accountMap").attr("src","${path}/map/trip.do?searchType=default");
	$("body").on("click",".btn_gps",function(){
		$("#popStart").text($(this).parent("div").find(".startInfo").val());
		$("#popEnd").text($(this).parent("div").find(".endInfo").val());
		$("#popVehicle").text($(this).parent("div").find(".vehicleInfo").val());

		var t = 3600*(24*4);
		$("#accountMap").get(0).contentWindow.searchType = "trip";
		$("#accountMap").get(0).contentWindow.tripKey = $(this).data("gps");
		//$("#accountMap").get(0).contentWindow.accountKey = $(this).data("account");
		//$("#accountMap").get(0).contentWindow.searchTime = t;
		$("#accountMap").get(0).contentWindow._o = $("#accountMap").get(0).contentWindow.createOption();
		$("#accountMap").get(0).contentWindow.outerReset();

		$(".accountMap").fadeIn();
		$(".black").fadeIn();
	});
	
	initUserInfo();
	drivingSummary();
});

function SuddenSummaryInit(){
	$(".rapidStart").text("<fmt:message key="searchuserdetail.count1"/>");
	$(".rapidStop").text("<fmt:message key="searchuserdetail.count2"/>");
	$(".rapidAccel").text("<fmt:message key="searchuserdetail.count3"/>");
	$(".rapidDeaccel").text("<fmt:message key="searchuserdetail.count4"/>");
	$(".rapidTurn").text("<fmt:message key="searchuserdetail.count5"/>");
	$(".rapidUtern").text("<fmt:message key="searchuserdetail.count6"/>");
	$(".overSpeed").text("<fmt:message key="searchuserdetail.count7"/>");
	$(".overSpeedLong").text("<fmt:message key="searchuserdetail.count8"/>");
}

function outerSuddenSummaryCall(a){
	console.log(a);
	$(".rapidStart").text(a[0]+"<fmt:message key="searchuserdetail.count9"/>");
	$(".rapidStop").text(a[1]+"<fmt:message key="searchuserdetail.count10"/>");
	$(".rapidAccel").text(a[2]+"<fmt:message key="searchuserdetail.count11"/>");
	$(".rapidDeaccel").text(a[3]+"<fmt:message key="searchuserdetail.count12"/>");
	$(".rapidTurn").text(a[4]+"<fmt:message key="searchuserdetail.count13"/>");
	$(".rapidUtern").text(a[5]+"<fmt:message key="searchuserdetail.count14"/>");
	$(".overSpeed").text(a[6]+"<fmt:message key="searchuserdetail.count15"/>");
	$(".overSpeedLong").text(a[7]+"<fmt:message key="searchuserdetail.count16"/>");
}



function addrString(a){
	var result = '';

	for(var i=0;i < a.length;i++){
		if(i >= 18){
			result += '...';
			break;
		}
		else
			result += a[i];
	}
		
	return result;
}




function userImg(a){
	if(a==null) return '${pathCommon}/images/user_none.png'
	else return '${pathCommon}/images/'+a+'.png'
}

var initUserInfo = function(){
	var accountKey = $("#accountKey").val();
	
	$VDAS.http_post("/user/getUserInfo.do",{accountKey:accountKey},{
		success : function(r){
			var obj = r.result;

			console.log(obj);

			if(obj != null && obj != ''){
				var drivingStateHtml ="";
				if(obj.vehicleState == "2")  
					drivingStateHtml += '<span class="ico7">';
				else drivingStateHtml += '<span class="ico'+obj.accountState+'">';	
				switch(obj.accountState){
					case "1":
						drivingStateHtml += '<fmt:message key='searchuserdetail.drivingType1'/>';
						break;
					case "2":
						drivingStateHtml += '<fmt:message key='searchuserdetail.drivingType2'/>';
						break;
					case "3":
						drivingStateHtml += '<fmt:message key='searchuserdetail.drivingType3'/>';
						break;
					case "4":
						drivingStateHtml += '<fmt:message key='searchuserdetail.drivingType4'/>';
						break;
				}
				drivingStateHtml += '</span>';
				
				$("#drivingState").after(drivingStateHtml);
				if(obj.imgId&&obj.imgId.length > 0)
					$(".user_detail_left").append('<img class="svg-clipped" src="${defaultPath}/com/getUserImg.do?uuid='+obj.imgId+'" alt="" />');
				else $(".user_detail_left").append('<img class="svg-clipped" src="${pathCommon}/images/user_img7.jpg" alt=""/>');
				$("#corpGroup").after(""+convertNullString(obj.corpNm));
				$("#position").after(""+convertNullString(obj.corpPosition)+"/"+convertNullString(obj.name));
				$("#phone").after(""+convertNullString(obj.phone));
				$("#mobilePhone").after(""+convertNullString(obj.mobilePhone));
				$("#email").after(""+convertNullString(obj.authMail));
			}
		}
	});
	
	$VDAS.http_post("/user/getUserLastTask.do",{accountKey:accountKey},{
		success : function(r){
			var obj = r.result;

			console.log(obj,"lastTast");
			if(obj != null && obj != ''){
				$(".car_info2_1").append('<img src="${pathCommon}/images/vehicle/'+obj.imgS+'.png" alt="" />');
				$("#taskNm").after("&nbsp;&nbsp;"+convertNullString(obj.title));
				$("#taskPlan").after(""+convertNullString(obj.contents));
				$("#taskDate").after("<fmt:message key="searchuserdetail.taskStart"/> "+convertNullString(obj.startDate)+"&nbsp;&nbsp;<fmt:message key="searchuserdetail.taskEnd"/> "+convertNullString(obj.endDate));
				
				$("#deviceNm").after(""+convertNullString(obj.deviceSeries)+"/"+convertNullString(obj.deviceSn));
				$("#vehicleInfo").after(""+convertNullString(obj.modelMaster)+"/"+convertNullString(obj.plateNum));
				$("#plateNum").after(""+convertNullString(obj.plateNum));
				$("#volume").after(""+convertNullString(obj.volume)+" cc");
				$("#transmission").after(""+convertNullString(obj.transmission));
				$("#distance").after(""+number_format((obj.totDistance/1000).toFixed(1))+" km");
				$("#modelMaster").after(""+convertNullString(obj.manufacture)+"/"+convertNullString(obj.modelMaster));
				$("#modelHeader").after(""+convertNullString(obj.modelHeader)+"/"+convertNullString(obj.year));
				$("#fuel").after(""+convertNullString(obj.fuelType));
				$("#color").after(""+convertNullString(obj.color));
				/*
				$(".btn_detail").attr("data-gps",''+obj.tripKey);
				$(".btn_detail").attr("data-start",''+convertNullString(obj.startDate) + ' ' + convertNullString(obj.startAddr));
				$(".btn_detail").attr("data-end",''+convertNullString(obj.endDate) + ' ' + convertNullString(obj.endAddr));
				$(".btn_detail").attr("data-vehicle",''+convertNullString(obj.modelMaster) + '/' + convertNullString(obj.plateNum));
				*/
			}
		}
	});
	
}

function drivingSummary(){
	
	var accountKey = $("#accountKey").val();
	var tt = ""+replaceAll($("#searchMonth").val(),"-","");
	
	$VDAS.http_post("/driving/getAccountSummary.do",{accountKey : accountKey, yyyymm : tt},{
		success:function(r){
			console.log(r);
			
			var data = r.rtv?r.rtv:{};
			var pData = r.rtvPrev?r.rtvPrev:{};
			var distance = data?data.distance:0;
			var drivingTime = data?data.drivingTime:0;
			var fco = data?data.fco:0;
			var oilPrice = data?data.oilPrice:0;
			
			var safeScore = data?data.safeScore:0;
			var ecoScore = data?data.ecoScore:0;
			var safeScoreP = pData.safeScore?pData.safeScore:0;
			var ecoScoreP = pData.ecoScore?pData.ecoScore:0;
			
			var bizCnt1 = data?data.bizCnt1:0;
			var bizCnt2 = data?data.bizCnt2:0;
			var bizCnt3 = data?data.bizCnt3:0;
			var bizTot = parseInt(bizCnt1)+parseInt(bizCnt2)+parseInt(bizCnt3);
			
			var bizRatio1 = bizTot==0?0:Math.round((bizCnt1/bizTot)*1000)/10;
			var bizRatio2 = bizTot==0?0:Math.round((bizCnt2/bizTot)*1000)/10;
			var bizRatio3 = bizTot==0?0:Math.round((bizCnt3/bizTot)*1000)/10;
					
			
			
			$("#bizRatio1").text(bizRatio1+"%");
			$("#bizRatio2").text(bizRatio2+"%");
			$("#bizRatio3").text(bizRatio3+"%");
						
			$("#myChart01").drawDoughnutChart([
              { title: "<fmt:message key="searchuserdetail.vehicleDrivingType1"/>", value : bizRatio1,  color: "#a150cb" },
              { title: "<fmt:message key="searchuserdetail.vehicleDrivingType2"/>", value:  bizRatio2,   color: "#ea2b7e" },
              { title: "<fmt:message key="searchuserdetail.vehicleDrivingType3"/>", value:  bizRatio3,   color: "#68b2c2" }
            ]);
			
			$("#distanceS").text(""+number_format(parseFloat(data?(data.distance/1000):0).toFixed(1))+" km");
			$("#drivingTime").text(""+parseInt(drivingTime/3600)+'<fmt:message key='searchuserdetail.time'/> '+LPAD(''+parseInt((drivingTime%3600)/60),'0',2)+'<fmt:message key='searchuserdetail.minute'/> '+LPAD(''+parseInt((drivingTime%3600)%60),'0',2)+'<fmt:message key='searchuserdetail.second7'/>');
			$("#fco").text(""+number_format(parseFloat(data?(data.fco/1000):0).toFixed(1))+" ℓ");
			$("#oilPrice").text(""+number_format(data?data.oilPrice:0)+" <fmt:message key="searchuserdetail.won"/>");

			$("#safeScore").text(safeScore+"<fmt:message key="searchuserdetail.score1"/>");
			$("#ecoScore").text(ecoScore+"<fmt:message key="searchuserdetail.score2"/>");
			$("#safeScoreP").text("(<fmt:message key="searchuserdetail.lastMonth1"/>"+(pData?safeScoreP+"<fmt:message key="searchuserdetail.score3"/>":" - ")+")");
			$("#ecoScoreP").text("(<fmt:message key="searchuserdetail.lastMonth2"/>"+(pData?ecoScoreP+"<fmt:message key="searchuserdetail.score4"/>":" - ")+")");
			
			
            $("#totalRank").text((bizRatio1+bizRatio2)+"%");
		}
	});
	
}

</script>
<input type="hidden" id="accountKey" value="${accountKey}" />
<input type="hidden" id="vehicleKey" value="" />
<form id="searchFrm_account" style="display:none">
	<input type="hidden" name="vehicleKey" value="" />
	<input type="hidden" name="searchAccountKey" value="" />
	<input type="hidden" name="searchOrder" value="driving" />
	<input type="hidden" name="searchMonth" value="" />
</form>
<form id="searchFrm_route" style="display:none">
	<input type="hidden" name="vehicleKey" value="" />
	<input type="hidden" name="searchAccountKey" value="" />
	<input type="hidden" name="searchOrder" value="driving" />
	<input type="hidden" name="searchVehicleState" value="" />
	<input type="hidden" name="searchWarning" value="" />
</form>
<div class="right_layout">
	<div class="detail_view">
		<a href="javascript:window.history.back();" class="btn_close"><img src="${pathCommon}/images/close.jpg" alt="" /></a>
		<h2 class="h2tit">
			<fmt:message key='searchuserdetail.userDetailInfo'/>
		</h2>
		<div class="step_box sub_top1" style="text-align:left; padding:7px 12px 6px 21px;">
			<span style="padding-top:5px;" id="drivingState"><fmt:message key='searchuserdetail.drivingState'/></span>
			<strong id="nowDate" style="padding-top:5px;"></strong>
		</div>
		<div class="user_detail" style="padding-top:17px;">
			<div class="user_detail_left">
				<!-- img size : 가로 345-->
				
			</div>
			<ul class="car_detail_right">
				<li>
					<div>
						<p><span id="corpGroup"><fmt:message key='searchuserdetail.corpGroup'/> </span></p>
						<p><span id="position"><fmt:message key='searchuserdetail.position'/> </span></p>
					</div>
					<div>
						<p><span id="phone"><fmt:message key='searchuserdetail.phone'/> </span></p>
						<p><span id="mobilePhone"><fmt:message key='searchuserdetail.mobilePhone'/> </span></p>
						<p><span id="email"><fmt:message key='searchuserdetail.email'/> </span></p>
					</div>
				</li>
				<li>
					<p><span id="taskNm"><fmt:message key='searchuserdetail.taskName'/> </span></p>
					<p><span><fmt:message key='searchuserdetail.taskPlan'/> </span><strong id="taskPlan"></strong></p>
					<p><span><fmt:message key='searchuserdetail.taskDate'/> </span><strong id="taskDate"></strong></p>
				</li>
				<li class="car_info">
					<h1><span></span>사용 차량정보</h1>
					<div class="car_info2">
						<div class="car_info2_1"></div>
						<div class="car_info2_2">
							<p><span id="vehicleInfo"><fmt:message key='searchuserdetail.vehicleSelect'/> </span></p>
							<p><span id="deviceNm"><fmt:message key='searchuserdetail.deviceName'/> </span></p>
						</div>
					</div>
				</li>
				<li>
					<div>
						<p><span id="plateNum"><fmt:message key='searchuserdetail.plateNum'/> </span></p>
						<p><span id="volume"><fmt:message key='searchuserdetail.volume'/> </span></p>
						<p><span id="transmission"><fmt:message key='searchuserdetail.transmission'/> </span></p>
						<p><span id="distance"><fmt:message key='searchuserdetail.totaldistance'/> </span></p>
					</div>
					<div>
						<p><span id="modelMaster"><fmt:message key='searchuserdetail.modelMaster'/> </span></p>
						<p><span id="modelHeader"><fmt:message key='searchuserdetail.modelHeader'/> </span></p>
						<p><span id="fuel"><fmt:message key='searchuserdetail.fuel'/> </span></p>
						<p><span id="color"><fmt:message key='searchuserdetail.color'/> </span></p>
					</div>
				</li>
			</ul>
		</div>
		<div class="tab_layout">
			<div class="pageTab">
				<ul class="tab_nav">
					<li class="tabItem on" data-tab='driving'><a href="#none"><fmt:message key='searchuserdetail.txt6'/></a></li>
					<li class='tabItem' data-tab='route'><a href="#none"><fmt:message key='searchuserdetail.txt7'/></a></li>
				</ul>
			</div>
			<div class="div1 driving">
				<h4 class="h4tit"><fmt:message key='searchuserdetail.txt8'/></h4>
				<div class="search_box1">
					<input id="searchMonth" type="text" class="date datePickerMonth" />
					<a href="#none" id="btnSearchVehicle" class="btn_search style02"><fmt:message key='searchuserdetail.search'/></a>
				</div>
				<div class="tbl_layout">
					<div class="row">
						<div class="col" style="width:310px;">
							<div id="myChart01" class="myChart">
								<div class="level" style="font-size: 25px;"><span id="totalRank"></span></div>
							</div>
							<div class="chart_explain">
								<table class="tbl tbl_explain">
									<colgroup>
										<col style="width:95px;"/>
										<col />
									</colgroup>
									<tr>
										<td><span class="color purple"></span><fmt:message key='searchuserdetail.vehicleDrivingType1_1'/></td>
										<td style="padding-left:0px;"><span id="bizRatio1"></span></td>
									</tr>
									<tr>
										<td><span class="color pink"></span><fmt:message key='searchuserdetail.vehicleDrivingType2_2'/></td>
										<td style="padding-left:0px;"><span id="bizRatio2"></span></td>
									</tr>
									<tr>
										<td><span class="color blue"></span><fmt:message key='searchuserdetail.vehicleDrivingType3_3'/></td>
										<td style="padding-left:0px;"><span id="bizRatio3"></span></td>
									</tr>
								</table>
							</div>
						</div>
						<div class="col" style="width:430px; float:right;">
							<table class="tbl tbl_type02">
								<colgroup>
									<col style="width:125px;" />
									<col />
								</colgroup>
								<tr>
									<th scope="row"><fmt:message key='searchuserdetail.distanceS'/></th>
									<td><span id="distanceS"></span></td>
								</tr>
								<tr>
									<th scope="row"><fmt:message key='searchuserdetail.drivingTime2'/></th>
									<td><span id="drivingTime"></span></td>
								</tr>
								<tr>
									<th scope="row"><fmt:message key='searchuserdetail.fcouse'/></th>
									<td><span id="fco"></span></td>
								</tr>
								<tr>
									<th scope="row"><fmt:message key='searchuserdetail.oilPrice'/></th>
									<td><span id="oilPrice"></span><br/><fmt:message key='searchuserdetail.txt9'/></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<div class="sub_box1"  style="margin-top:35px;">
					<span class="s1">
					</span><span class="s2">
						<img src="${pathCommon}/images/sub_icon1.jpg" alt="" /><fmt:message key='searchuserdetail.safeQuotient'/> <b id="safeScore"><fmt:message key='searchuserdetail.safeQuotientScore'/></b> <strong id="safeScoreP"><fmt:message key='searchuserdetail.safeQuotientScoreLastMonth'/></strong>
					</span><span class="s3">
						<img src="${pathCommon}/images/sub_icon2.jpg" alt="" /><fmt:message key='searchuserdetail.ecoQuotient'/> <b id="ecoScore"><fmt:message key='searchuserdetail.ecoQuotientScore'/></b> <strong id="ecoScoreP"><fmt:message key='searchuserdetail.ecoQuotientScoreLastMonth'/></strong>
					</span>
				</div>
				<h4 class="h4tit"><fmt:message key='searchuserdetail.txt9'/></h4>
				<table class="table1 table1_2" id="contentsTable">
					<colgroup>
						<col style="width:80px;"/>
						<col style="width:120px;"/>
						<col />
						<col style="width:100px;"/>
						<col style="width:80px;"/>
					</colgroup>
					<thead>
						<tr style="display:none;">
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
						</tr>
						<tr>
							<th colspan="2"><fmt:message key='searchuserdetail.vehicleAndUser'/></th>
							<th><fmt:message key='searchuserdetail.drivingInfo'/></th>
							<th><fmt:message key='searchuserdetail.location'/></th>
							<th><fmt:message key='searchuserdetail.warning'/></th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
			<div class="div1 route" style="display:none;">
				<h2 class="title3 title3_2 pat36">
					<p><b><fmt:message key='searchuserdetail.start2'/> </b><span id="routeStart"></span></p>
					<p><b><fmt:message key='searchuserdetail.end2'/> </b><span id="routeEnd"></span></p>
					<a href="#none" class="btn_other"><fmt:message key='searchuserdetail.otherLocation'/></a>
				</h2>
				<h3 class="title4" style="padding-bottom:0px;"><b><fmt:message key='searchuserdetail.routeVehicle'/> </b><span id="routeVehicle"></span></h3>
				<div class="map_area">
					<div id="map_canvas">
						<iframe id="drivingMap" src="#none" width="760px" height="339px"></iframe>
					</div>
				</div>
				<div class="map_txt1">
					<div>
						<span class="w109">
							<img src="${pathCommon}/images/m_img1.png" alt=""><fmt:message key='searchuserdetail.rapidStart'/> <strong class="rapidStart"><fmt:message key='searchuserdetail.count17'/></strong>
						</span><span class="w120">
							<img src="${pathCommon}/images/m_img2.png" alt=""><fmt:message key='searchuserdetail.rapidStop'/> <strong class="rapidStop"><fmt:message key='searchuserdetail.count18'/></strong>
						</span>
						<span class="w120">
							<img src="${pathCommon}/images/m_img4.png" alt=""><fmt:message key='searchuserdetail.rapidAccel'/> <strong class="rapidAccel"><fmt:message key='searchuserdetail.count19'/></strong>
						</span>
						<span class="w109">
							<img src="${pathCommon}/images/m_img5.png" alt=""><fmt:message key='searchuserdetail.rapidDeaccel'/> <strong class="rapidDeaccel"><fmt:message key='searchuserdetail.count20'/></strong>
						</span>
						<span class="w120">
							<img src="${pathCommon}/images/m_img10.png" alt=""><fmt:message key='searchuserdetail.rapidTurn'/><fmt:message key='searchinfo.vehicleUserTxt'/> <strong class="rapidTurn"><fmt:message key='searchuserdetail.count21'/></strong>
						</span>
					</div>
					<div>
						<span class="w109">
							<img src="${pathCommon}/images/m_img11.png" alt=""><fmt:message key='searchuserdetail.rapidUtern'/> <strong class="rapidUtern"><fmt:message key='searchuserdetail.count22'/></strong>
						</span>
						<span class="w109">
							<img src="${pathCommon}/images/m_img6.png" alt=""><fmt:message key='searchuserdetail.overSpeed'/> <strong class="overSpeed"><fmt:message key='searchuserdetail.count22'/></strong>
						</span>
						<span class="w120">
							<img src="${pathCommon}/images/m_img7.png" alt=""><fmt:message key='searchuserdetail.overSpeedLong'/> <strong class="overSpeedLong"><fmt:message key='searchuserdetail.count24'/></strong>
						</span>
						<%-- <span class="w178">
							<img src="${pathCommon}/images/m_img12.png" alt="">심야운전 <strong class="nightDrive">0회</strong>
						</span> --%>
					</div>
				</div>
				<div class="map_txt2"><fmt:message key='searchuserdetail.txt10'/></div>
			</div>
		</div>
	</div>
</div>
<div class="black"></div>
<div class="pop2 accountMap">
	<div class="pop_layout">
		<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt="" /></a>
		<h1><fmt:message key='searchuserdetail.txt11'/></h1>
		<h2 class="title3 pat42">
			<p><b><fmt:message key='searchuserdetail.start3'/> </b><span id="popStart"></span></p>
			<p><b><fmt:message key='searchuserdetail.end3'/> </b><span id="popEnd"></span></p>
		</h2>
		<h3 class="title4" style="padding-bottom:0px;"><fmt:message key='searchuserdetail.routeVehicle2'/> <b id="popVehicle"></b></h3>
		<div class="map_area">
			<div id="map_canvas">
				<iframe id="accountMap" src="#none" width="760px" height="339px"></iframe>
			</div>
		</div>
		<div class="map_txt1">
			<div>
				<span class="w109">
					<img src="${pathCommon}/images/m_img1.png" alt=""><fmt:message key='searchuserdetail.rapidStart2'/> <strong class="rapidStart"><fmt:message key='searchuserdetail.count25'/></strong>
				</span><span class="w120">
					<img src="${pathCommon}/images/m_img2.png" alt=""><fmt:message key='searchuserdetail.rapidStop2'/> <strong class="rapidStop"><fmt:message key='searchuserdetail.count26'/></strong>
				</span>
				<span class="w120">
					<img src="${pathCommon}/images/m_img4.png" alt=""><fmt:message key='searchuserdetail.rapidAccel2'/> <strong class="rapidAccel"><fmt:message key='searchuserdetail.count27'/></strong>
				</span>
				<span class="w109">
					<img src="${pathCommon}/images/m_img5.png" alt=""><fmt:message key='searchuserdetail.rapidDeaccel2'/> <strong class="rapidDeaccel"><fmt:message key='searchuserdetail.count28'/></strong>
				</span>
				<span class="w120">
					<img src="${pathCommon}/images/m_img10.png" alt=""><fmt:message key='searchuserdetail.rapidTurn2'/> <strong class="rapidTurn"><fmt:message key='searchuserdetail.count29'/></strong>
				</span>
			</div>
			<div>
				<span class="w109">
					<img src="${pathCommon}/images/m_img11.png" alt=""><fmt:message key='searchuserdetail.rapidUtern2'/> <strong class="rapidUtern"><fmt:message key='searchuserdetail.count30'/></strong>
				</span>
				<span class="w109">
					<img src="${pathCommon}/images/m_img6.png" alt=""><fmt:message key='searchuserdetail.overSpeed2'/> <strong class="overSpeed"><fmt:message key='searchuserdetail.count31'/></strong>
				</span>
				<span class="w120">
					<img src="${pathCommon}/images/m_img7.png" alt=""><fmt:message key='searchuserdetail.overSpeedLong2'/> <strong class="overSpeedLong"><fmt:message key='searchuserdetail.count32'/></strong>
				</span>
				<%-- <span class="w178">
					<img src="${pathCommon}/images/m_img12.png" alt="">심야운전 <strong class="nightDrive">0회</strong>
				</span> --%>
			</div>
		</div>
		<div class="map_txt2"><fmt:message key='searchuserdetail.txt12'/></div>
	</div>
</div>
<div class="pop7 list" style="display:none;">
	<div class="pop6_area">
		<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt="" /></a>
		<h1><fmt:message key='searchuserdetail.txt13'/></h1>
		<div class="txt">
			<p><fmt:message key='searchuserdetail.txt14'/></p>
		</div>
		<table class="table1" id="contentsTablePop">
			<colgroup>
				<col style="width:121px;"/>
				<col />
				<col style="width:80px;"/>
			</colgroup>
			<thead>
				<tr style="display:none;">
					<th></th>
					<th></th>
					<th></th>
				</tr>
				<tr>
					<th><fmt:message key='searchuserdetail.user'/></th>
					<th><fmt:message key='searchuserdetail.drivingInfo2'/></th>
					<td><fmt:message key='searchuserdetail.select'/></td>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		<br><br>
		<div class="btn_type1"><a href="#none" class="btn_accept"><fmt:message key='searchuserdetail.ok'/></a></div>
	</div>
</div>