<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 

<fmt:setLocale value="${_LANG}" scope="request" />
<fmt:setBundle basename="properties.messages.rest.message" />


<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script type="text/javascript">
var lazy;
var lazyUser;
var lazyDtc;
$(document).ready(function(){

	
	initMenuSel("M1002");

	$VDAS.ajaxCreateSelect("/com/getCityLev1.do",{},$("#cityLev1"),false,"<fmt:message key='searchDriving.city'/>","","${_LANG}",false,null);

	$("#cityLev1").on("change",function(){
		$VDAS.ajaxCreateSelect("/com/getCityLev2.do",{city:$(this).val()},$("#cityLev2"),false,"<fmt:message key='searchDriving.country'/>","","${_LANG}",false,null);	
		searchArealev1 = $(this).val();
	});
	
	$(".selectPeriod input[name=selectPeriod]").on("click",function(){
		var period = $(this).val();
		var endDate = new Date();
		var startDate = new Date();
		if(period < 30)
			startDate.setDate(endDate.getDate() - period);
		else
			startDate.setMonth(endDate.getMonth() - (period / 30));
		if(period != "" && period != null){
			$("#startDate").val(startDate.getFullYear() + "/" + LPAD(''+(startDate.getMonth() + 1),'0',2) + "/" + LPAD(''+startDate.getDate(),'0',2));
			$("#endDate").val(endDate.getFullYear() + "/" + LPAD(''+(endDate.getMonth() + 1),'0',2) + "/" + LPAD(''+endDate.getDate(),'0',2));
		}
		else{
			$("#startDate").val("");
			$("#endDate").val("");
		}
			
	});
	
	$(".btn_search").on("click",function(){
		var start = replaceAll($("#startDate").val(),"/","");
		var end = replaceAll($("#endDate").val(),"/","");
		$("#searchFrm input[name=startDate]").val(start);
		$("#searchFrm input[name=endDate]").val(end);
		
		$("#searchFrm input[name=selectWeek]").val("");
		$(".selectWeek input[name=selectWeek]").each(function(index){
			if($(this).is(":checked")){
				if($("#searchFrm input[name=selectWeek]").val() != "")
					$("#searchFrm input[name=selectWeek]").val($("#searchFrm input[name=selectWeek]").val() + ",");
				$("#searchFrm input[name=selectWeek]").val($("#searchFrm input[name=selectWeek]").val() + $(this) .val());
			}
		});
		
		$.lazyLoader.search(lazy);
		searchCnt();
	});
	
	$('.ui_radio_box input[type=radio]').checkboxradio({
        icon: false
    });
	
	$('.ui_radio_box input[type=checkbox]').checkboxradio({
        icon: false
    });
	
	$(".selectPeriod label").css("padding","0.4em 0.67em");
	$(".selectWeek label").css("padding","0.4em 1.1em");
	
	$("body").on("click",".period a",function(){
		$(".period a").removeClass("on");
		$(this).addClass("on");
	});
	
	$("body").on("click",".week a",function(){
		$(".week a").removeClass("on");
		$(this).addClass("on");
	});
	
	$("#searchText").on("keyup",function(e){
		if(e.keyCode == 13) $(".btn_search").trigger("click");
	});
	
	$("#searchOrder").on("change",function(){
		$.lazyLoader.search(lazy);
	});
	
	$("body").on("click",".btn_edit",function(){
		var key = $(this).data("key");
		var allocateKey = $(this).data("allocatekey");
		if(confirm("<fmt:message key='searchDriving.instanceAllocate'/>\n<fmt:message key='searchDriving.instanceAllocate2'/>")){
			$VDAS.http_post("/driving/pushTripAccount.do",{tripKey:key,allocateKey:allocateKey},{success : function(r){
				alert("<fmt:message key='searchDriving.confirmed'/>");
				$(".btn_search").trigger("click");
			}});
		}
	});
	
	$("#searchWarning,#searchVehicleState").on("change",function(){
		$.lazyLoader.search(lazy);
		searchCnt();
	});
	
	
	
	if(!lazy){
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
				searchType : $("#searchType")
				,searchText : $("#searchText")
				,searchStartH : $("#searchStartH")
				,searchStartM : $("#searchStartM")
				,searchEndH : $("#searchEndH")
				,searchEndM : $("#searchEndM")
				,searchMeanSpeedMin : $("#searchMeanSpeedMin")
				,searchMeanSpeedMax : $("#searchMeanSpeedMax")
				,searchHighSpeedMin : $("#searchHighSpeedMin")
				,searchHighSpeedMax : $("#searchHighSpeedMax")
				,searchFcoMin : $("#searchFcoMin")
				,searchFcoMax : $("#searchFcoMax")
				,cityLev1 : $("#cityLev1")
				,cityLev2 : $("#cityLev2")
				,searchOrder : $("#searchOrder")
				,searchWarning : $("#searchWarning")
				,searchVehicleState : $("#searchVehicleState")
				
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/driving/getDrivingList.do"
			,initSearch : true
			,createDataHtml : function(r){
				console.log(r);
				var data = r.rtvList;
				var strHtml = '';
				
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];

					strHtml += '<tr>';
					strHtml += '<td width="100px">';
					strHtml += '<div class="table1_box1">';
					strHtml += '<img src="${pathCommon}/images/vehicle/'+vo.imgS+'.png" alt="" />';
					strHtml += '</div>';
					strHtml += '<div class="table1_box2">';
					strHtml += '<img src='+userImg(vo.driverImg)+' alt="" />';
					strHtml += '</div>';
					strHtml += '</td>';
					strHtml += '<td width="120px">';
					strHtml += '<div class="table1_box1 txt_left">';
					strHtml += '<a href="#none" class="btn_vehicleDetail" data-key="'+vo.vehicleKey+'">';
					strHtml += '<span>'+vo.modelMaster+'</span>';
					strHtml += ' '+vo.plateNum;
					strHtml += '</a>';
					strHtml += '</div>';
					strHtml += '<div class="table1_box2 txt_left">';
					strHtml += '<a href="#none" class="btn_userDetail" data-key="'+vo.accountKey+'">';
					strHtml += '<span>'+convertNullString(vo.driverGroupNm)+'/</span>';
					strHtml += ' '+convertNullString(vo.driverNm);
					strHtml += '</a>';
					strHtml += '<br>';
					strHtml += '<br>';
					if(vo.instantAllocate == '1')
						strHtml += '<a href="#none" class="btn btn_edit" data-key="'+vo.tripKey+'" data-allocateKey="'+vo.allocateKey+'" data-target="1"><fmt:message key='searchDriving.confirm'/></a>';
					strHtml += '</div>';
					strHtml += '</td>';
					strHtml += '<th class="table1_box_area">';
					strHtml += '<div class="table1_box1">';
					if(convertNullString(vo.startAddr) == ''){
						if(vo.tripDetailState == 1){
							if("${VDAS.lang}" == "ko") vo.startAddr = '<fmt:message key='searchDriving.nowDriving'/>';
							else vo.startAddr = 'now Driving';
						}else if(vo.tripDetailState == 2){
							if("${VDAS.lang}" == "ko") vo.startAddr = '<fmt:message key='searchDriving.createDrivingInfo'/>';
							else vo.startAddr = 'creating driving info';
						}else if(vo.tripDetailState == 3){
							if("${VDAS.lang}" == "ko") vo.startAddr = '<fmt:message key='searchDriving.createDrivingInfo2'/>';
							else vo.startAddr = 'creating driving info';
						}
					}
					if(convertNullString(vo.endAddr) == ''){
						if(vo.tripDetailState == 1){
							if("${VDAS.lang}" == "ko") vo.endAddr = '<fmt:message key='searchDriving.nowDriving2'/>';
							else vo.endAddr = 'now Driving';
						}else if(vo.tripDetailState == 2){
							if("${VDAS.lang}" == "ko") vo.endAddr = '<fmt:message key='searchDriving.createDrivingInfo3'/>';
							else vo.endAddr = 'creating driving info';
						}else if(vo.tripDetailState == 3){
							if("${VDAS.lang}" == "ko") vo.endAddr = '<fmt:message key='searchDriving.createDrivingInfo4'/>';
							else vo.endAddr = 'creating driving info';
						}
					}
					strHtml += '<p><b><fmt:message key='searchDriving.start'/> </b><span title="'+convertNullString(vo.startAddr)+'" style="font-weight:500;">'+convertNullString(vo.startDate)+' '+addrString(convertNullString(vo.startAddr))+'</span></p>';
					strHtml += '<p><b><fmt:message key='searchDriving.end'/> </b><span title="'+convertNullString(vo.endAddr)+'" style="font-weight:500;">'+convertNullString(vo.endDate)+' '+addrString(convertNullString(vo.endAddr))+'</span></p>';
					strHtml += '<input type="hidden" class="startInfo" value="'+convertNullString(vo.startDate)+' '+convertNullString(vo.startAddr)+'">';
					strHtml += '<input type="hidden" class="endInfo" value="'+convertNullString(vo.endDate)+' '+convertNullString(vo.endAddr)+'">';
					strHtml += '<input type="hidden" class="vehicleInfo" value="'+convertNullString(vo.modelMaster)+'/'+convertNullString(vo.plateNum)+'">';
					if(vo.endAddr != '' && vo.endAddr != 'null')
						strHtml += '<a href="#none" class="btn_gps" data-gps="'+vo.tripKey+'"><img src="${pathCommon}/images/gps.jpg" alt=""></a>';
					strHtml += '</div>';
					
					
					
					
					//123123
					strHtml += '<div class="table1_box2">';
					strHtml += '<div>';
					strHtml += '<span style=""><b><fmt:message key='searchDriving.safeScore'/> </b>'+calScore(parseFloat(vo.safeScore).toFixed(1),"${_LANG}")+'</span>';
					strHtml += '<span style=""><b><fmt:message key='searchDriving.fuelScore'/> </b>'+calScore((vo.avgFco/vo.fuelEfficiency*100).toFixed(1),"${_LANG}")+'</span>';
					strHtml += '</div>';
					strHtml += '</div>';
					//strHtml += '<div class="table1_box2">';
					//strHtml += '<div><span><b>주행시간 </b>'+parseInt(vo.drivingTime/3600)+'시간 '+LPAD(''+parseInt((vo.drivingTime%3600)/60),'0',2)+'분 '+LPAD(''+parseInt((vo.drivingTime%3600)%60),'0',2)+'초</span><span><b>주행거리 </b>'+number_format((vo.distance/1000).toFixed(1))+' km</span></div>';
					//strHtml += '<div><span><b>평균속도 </b>'+parseFloat(vo.meanSpeed).toFixed(1)+' km/h</span><span><b>최고속도 </b>'+parseFloat(vo.highSpeed).toFixed(1)+' km/h</span></div>';
					//strHtml += '<div><span><b>평균연비 </b>'+number_format(parseFloat(vo.avgFco).toFixed(1))+' km/ℓ</span></div>';
					//strHtml += '<div><span><b>안전지수 </b>'+convertNullString(vo.safeScore)+'</span><span><b>에코지수 </b>'+convertNullString(vo.ecoScore)+'</span></div>';
					//strHtml += '</div></th>';
					
					
					strHtml += '<div class="table1_box2">';
					strHtml += '<div>';
					strHtml += '<span style=""><b><fmt:message key='searchDriving.drivingTime'/> </b>'+parseInt(vo.drivingTime/3600)+'<fmt:message key='searchDriving.H'/> '+LPAD(''+parseInt((vo.drivingTime%3600)/60),'0',2)+'<fmt:message key='searchDriving.M'/> '+LPAD(''+parseInt((vo.drivingTime%3600)%60),'0',2)+'<fmt:message key='searchDriving.S'/></span>';
					strHtml += '<span style=""><b><fmt:message key='searchDriving.avgSpeed'/> </b>'+vo.meanSpeed+' km/h</span>';
					//strHtml += '<span style=""><b>냉각수 </b>'+parseFloat(vo.coolantTemp).toFixed(1)+' ℃</span>';
					strHtml += '</div>';
					strHtml += '<div>';
					strHtml += '<span style=""><b><fmt:message key='searchDriving.distance'/> </b>'+minusError(number_format((vo.distance/1000).toFixed(1)))+' km</span>';
					strHtml += '<span style=""><b><fmt:message key='searchDriving.highSpeed'/> </b>'+vo.highSpeed+' km/h</span>';
					//strHtml += '<span style=""><b>배터리 </b>'+number_format(parseFloat(vo.ecuVolt).toFixed(1))+' v</span>';
					strHtml += '</div>';
					strHtml += '<div>';
					strHtml += '<span style=""><b><fmt:message key='searchDriving.fco'/>  </b>'+number_format(parseFloat(vo.avgFco).toFixed(1))+' km/ℓ</span>';
					strHtml += '<span style=""><b><fmt:message key='searchDriving.fuel'/>  </b>'+number_format((vo.fco/1000).toFixed(1))+' ℓ</span>';
					//strHtml += '<span style=""><b>발전기 </b>'+number_format(parseFloat(vo.deviceVolt).toFixed(1))+' v</span>';
					strHtml += '</div>';
					strHtml += '<div>';					
					strHtml += '<span style=""><b><fmt:message key='searchDriving.co2'/>  </b>'+number_format(vo.co2Emission)+' ㎎</span>';
					strHtml += '<span style=""><b><fmt:message key='searchDriving.fuleCut'/>  </b>'+number_format(vo.fuelCutTime)+' <fmt:message key='searchDriving.S'/></span>';
					//strHtml += '<span style=""><b>공회전 </b>'+number_format(parseFloat(vo.idleTime).toFixed(1))+' 초</span>';
					strHtml += '</div>';
					strHtml += '<div>';
					strHtml += '<span style=""><b><fmt:message key='searchDriving.warmUp'/>  </b>'+number_format(vo.warmupTime)+' <fmt:message key='searchDriving.S'/></span>';
					strHtml += '</div></th>';
					
					
					
					strHtml += '<th class="table1_box_area" width="140px" colspan="1">';
					if(vo.dtcCnt)
						strHtml += '<div class="table1_box1" style="text-align:center;"><a href="#none" class="btn_dtc" data-key="'+vo.dtcKey+'"><div class="status off" style="display:inline-block;"></div></a></div>';
					else
						strHtml += '<div class="table1_box1" style="text-align:center;"><div class="status on" style="display:inline-block;"></div></div>';
						
					
					strHtml += '<div class="table1_box2 table1_box2_2">';
					strHtml += '<div>';
					strHtml += '<b style="width:50px;"><fmt:message key='searchDriving.ecoScore'/> </b>'+calScore(parseFloat(vo.ecoScore).toFixed(1),"${_LANG}")+'';					
					strHtml += '</div>';
					strHtml += '</div>';
					
					
					strHtml += '<div class="table1_box2 table1_box2_2">';
					strHtml += '<div><b><fmt:message key='searchDriving.cooleant'/> </b>'+convertNullString(vo.coolantTemp)+' ℃</div>';
					strHtml += '<div><b><fmt:message key='searchDriving.battery'/> </b>'+parseFloat(vo.ecuVolt).toFixed(1)+' v</div>';
					strHtml += '<div><b><fmt:message key='searchDriving.generator'/> </b>'+parseFloat(vo.deviceVolt).toFixed(1)+' v</div>';
					strHtml += '<div><b><fmt:message key='searchDriving.idle'/> </b>'+vo.idleTime+' <fmt:message key='searchDriving.S'/></div>';
					//strHtml += '<span style=""><b>발전기 </b>'+number_format(parseFloat(vo.deviceVolt).toFixed(1))+' v</span>';
					//strHtml += '<span style=""><b>공회전 </b>'+number_format(parseFloat(vo.idleTime).toFixed(1))+' 초</span>';
					
					strHtml += '</div></th>';
					strHtml += '<th class="table1_box_area" width="53px">';
					if((vo.workingAreaWarning == null || vo.workingAreaWarning == '')
							&& (vo.workingTimeWarning == null || vo.workingTimeWarning == ''))
						strHtml += '<div class="table1_box1"><p>&nbsp;&nbsp;&nbsp;-</p></div>';
					else if((vo.workingAreaWarning != null && vo.workingAreaWarning != '')
							&& (vo.workingTimeWarning == null || vo.workingTimeWarning == ''))
						strHtml += '<div class="table1_box1"><p class="t3"><fmt:message key='searchDriving.workingArea'/></p></div>';
					else if((vo.workingAreaWarning == null || vo.workingAreaWarning == '')
							&& (vo.workingTimeWarning != null && vo.workingTimeWarning != ''))
						strHtml += '<div class="table1_box1"><p class="t3"><fmt:message key='searchDriving.workingTime'/></p></div>';
					else if((vo.workingAreaWarning != null && vo.workingAreaWarning != '')
							&& (vo.workingTimeWarning != null && vo.workingTimeWarning != '')){
						strHtml += '<div class="table1_box1"><p class="t3"><fmt:message key='searchDriving.workingArea'/><br><fmt:message key='searchDriving.workingTime'/></p></div>';
					}
					strHtml += '<div class="table1_box2 table1_box2_2"></div>';
					strHtml += '<div class="table1_box2 table1_box2_2" style="margin-top:27px;"></div>';
					strHtml += '</th></tr>';
				}
			
				return strHtml;
				//<span class="ico1">운행중</span>
				//<span class="ico2">주차중</span>
				//<span class="ico3">미배차</span>
				//<span class="ico4">운행대기</span>
				//<span class="ico5">운행중</span>	
				
				//<div class="status off"></div>
				//<div class="status on"></div>
				//
			}
		});
	}
	
	
	if(!lazyDtc){
		lazyDtc = $("#contentsTableDtc").lazyLoader({
			searchFrmObj : $("#searchDtcFrm")
			,searchFrmVal : {}
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/clinic/getDtcList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.result;
				
				console.log(data);
				var strHtml = '';
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += '<tr>';
					strHtml += '<td>';
					strHtml += ''+convertNullString(vo.regDate);
					strHtml += '</td>';
					strHtml += '<td>';
					strHtml += ''+convertNullString(vo.dtcCode);
					strHtml += '</td>';
					strHtml += '<td>';
					if(_lang == 'ko'){
						strHtml += ''+convertNullString(vo.typeName);
						strHtml += '</td>';
						strHtml += '<td>';
						strHtml += ''+convertNullString(vo.descript);
					} 
					else {
						strHtml += ''+convertNullString(vo.typeNameEn);
						strHtml += '</td>';
						strHtml += '<td>';
						strHtml += ''+convertNullString(vo.descriptEn);
					}
					strHtml += '</td>';
					strHtml += '</tr>';
				}
				return strHtml;
			}
		});
	}
	
	$("body").on("click",".btn_dtc",function(){
		$("#searchDtcFrm input[name=dtcKey]").val($(this).data("key"));
		$(".breakdown").show();
		$.lazyLoader.search(lazyDtc);
		
		$(".pop7").fadeIn();
		$(".black").fadeIn();
	});
	
	$("body").on("click",".btn_ok",function(){
		$(".pop7").fadeOut();
		$(".black").fadeOut();
	});

	$("body").on("click",".btn_vehicleDetail",function(){
		var key = $(this).data("key");
		$VDAS.instance_post("/drivingMng/searchVehicleDetail.do",{vehicleKey:key});
	});
	
	$("body").on("click",".btn_userDetail",function(){
		var key = $(this).data("key");
		$VDAS.instance_post("/drivingMng/searchUserDetail.do",{accountKey:key});
	});

	$("#drivingMap").attr("src","${path}/map/trip.do?searchType=default");
	$("body").on("click",".btn_gps",function(){
		$("#popStart").text($(this).parent("div").find(".startInfo").val());
		$("#popEnd").text($(this).parent("div").find(".endInfo").val());
		$("#popVehicle").text($(this).parent("div").find(".vehicleInfo").val());
		
		$("#drivingMap").get(0).contentWindow.searchType = "trip";
		$("#drivingMap").get(0).contentWindow.tripKey = $(this).data("gps");
		
		$(".pop2").fadeIn();
		$(".black").fadeIn();
		
		$("#drivingMap").get(0).contentWindow._o = $("#drivingMap").get(0).contentWindow.createOption();
		$("#drivingMap").get(0).contentWindow.outerReset();
	});
	
	$("body").on("click",".btn_close",function(){
		SuddenSummaryInit();
	});
	
	totalCnt();
	searchCnt();
	
	
});

function totalCnt(){
	$VDAS.http_post("/driving/getDrivingCnt.do",{},{
		success : function(r){
			$("#totalCnt").text(r.result.total + "<fmt:message key='searchDriving.cnt'/>");
		}
	});
}

function searchCnt(){
	$VDAS.http_post("/driving/getSearchDrivingCnt.do",$("#searchFrm").serialize(),{
		success : function(r){
			$("#searchCnt").text(r.result.total + "<fmt:message key='searchDriving.cnt'/>");
		}
	});
}

function SuddenSummaryInit(){
	$(".rapidStart").text("0<fmt:message key='searchDriving.cnt2'/>");
	$(".rapidStop").text("0<fmt:message key='searchDriving.cnt2'/>");
	$(".rapidAccel").text("0<fmt:message key='searchDriving.cnt2'/>");
	$(".rapidDeaccel").text("0<fmt:message key='searchDriving.cnt2'/>");
	$(".rapidTurn").text("0<fmt:message key='searchDriving.cnt2'/>");
	$(".rapidUtern").text("0<fmt:message key='searchDriving.cnt2'/>");
	$(".overSpeed").text("0<fmt:message key='searchDriving.cnt2'/>");
	$(".overSpeedLong").text("0<fmt:message key='searchDriving.cnt2'/>");
}

function outerSuddenSummaryCall(a){
	console.log(a);
	$(".rapidStart").text(a[0]+"<fmt:message key='searchDriving.cnt2'/>");
	$(".rapidStop").text(a[1]+"<fmt:message key='searchDriving.cnt2'/>");
	$(".rapidAccel").text(a[2]+"<fmt:message key='searchDriving.cnt2'/>");
	$(".rapidDeaccel").text(a[3]+"<fmt:message key='searchDriving.cnt2'/>");
	$(".rapidTurn").text(a[4]+"<fmt:message key='searchDriving.cnt2'/>");
	$(".rapidUtern").text(a[5]+"<fmt:message key='searchDriving.cnt2'/>");
	$(".overSpeed").text(a[6]+"<fmt:message key='searchDriving.cnt2'/>");
	$(".overSpeedLong").text(a[7]+"<fmt:message key='searchDriving.cnt2'/>");
}

function userImg(a){
	if(a==null) return '${pathCommon}/images/user_none.png'
	else return '${pathCommon}/images/'+a+'.png'
}

function addrString(a){
	var result = '';

	for(var i=0;i < a.length;i++){
		if(i >= 18){
			result += '...';
			break;
		}
		else
			result += a[i];
	}
		
	return result;
}



</script>
<form id="searchFrm" style="display:none">
	<input type="hidden" name="searchType" value="" />
	<input type="hidden" name="searchText" value="" />
	<input type="hidden" name="startDate" value="" />
	<input type="hidden" name="endDate" value="" />
	<input type="hidden" name="selectWeek" value="" />
	<input type="hidden" name="searchStartH" value="" />
	<input type="hidden" name="searchStartM" value="" />
	<input type="hidden" name="searchEndH" value="" />
	<input type="hidden" name="searchEndM" value="" />
	<input type="hidden" name="searchMeanSpeedMin" value="" />
	<input type="hidden" name="searchMeanSpeedMax" value="" />
	<input type="hidden" name="searchHighSpeedMin" value="" />
	<input type="hidden" name="searchHighSpeedMax" value="" />
	<input type="hidden" name="searchFcoMin" value="" />
	<input type="hidden" name="searchFcoMax" value="" />
	<input type="hidden" name="cityLev1" value="" />
	<input type="hidden" name="cityLev2" value="" />
	<input type="hidden" name="searchOrder" value="" />
	<input type="hidden" name="searchVehicleState" value="" />
	<input type="hidden" name="searchWarning" value="" />
</form>
<form id="searchDtcFrm" style="display:none;">
	<input type="hidden" name="dtcKey" value="" />
</form>
<div class="right_layout">
	<h2 class="tit_sub"><fmt:message key='searchDriving.searchDriving'/>
		<p><fmt:message key='searchDriving.searchDrivingTxt'/></p>
	</h2>
	<div class="sub_top1">
		<span><fmt:message key='searchDriving.totDrivingCnt'/> <b id="totalCnt">xx<fmt:message key='searchDriving.ea'/></b></span>
		<strong id="nowDate"></strong>
	</div>
	<div class="search_box1 mab0">
		<form id="frmSearch" method="post" onSubmit="return false;">
			<div class="search_box1_1">
				<div class="select_type1">
					<div class="select_type1_1">
						<select class="sel" id="searchType">
							<option value=""><fmt:message key='searchDriving.all'/></option>
							<option value="plateNum"><fmt:message key='searchDriving.plateNum'/></option>
							<option value="modelMaster"><fmt:message key='searchDriving.model'/></option>
							<option value="useNm"><fmt:message key='searchDriving.name'/></option>
							<option value="drivingLocation"><fmt:message key='searchDriving.location'/></option>
						</select>
					</div>
				</div><input type="text" id="searchText" class="search_input" />
			</div><a href="#none" class="btn_search"><fmt:message key='searchDriving.search'/></a>
		</form>
	</div>
	<div class="search_box2">
		<form id="frmSearch" method="post" onSubmit="return false;">
			<div class="search_box2_1">
				<span class="tit"><b></b><fmt:message key='searchDriving.drivingDate'/></span>
				<ul>
					<li>
						<span class="tit2"><fmt:message key='searchDriving.date'/></span>
						<div class="tab_select tab_select2">
							<span class="tab_select_btn1 period">
								<div class="ui_radio_box selectPeriod">
									<label for="wholePeriod"><fmt:message key='searchDriving.all2'/></label>
									<input type="radio" name="selectPeriod" id="wholePeriod" value="" checked="checked">
									<label for="today"><fmt:message key='searchDriving.today'/></label>
									<input type="radio" name="selectPeriod" id="today" value="0">
									<label for="week"><fmt:message key='searchDriving.1W'/></label>
									<input type="radio" name="selectPeriod" id="week" value="7">
									<label for="month"><fmt:message key='searchDriving.1M'/></label>
									<input type="radio" name="selectPeriod" id="month" value="30">
									<label for="threeMonth"><fmt:message key='searchDriving.3M'/></label>
									<input type="radio" name="selectPeriod" id="threeMonth" value="90">
									<label for="halfYear"><fmt:message key='searchDriving.6M'/></label>
									<input type="radio" name="selectPeriod" id="halfYear" value="180">
								</div>
							</span>
						</div>
						<div class="cal_layout">
							<div class="cal_area">
								<span class="tit2"><fmt:message key='searchDriving.date2'/></span>
								<input type="text" id="startDate" class="date datePicker" style="width:52.5px; height:23px; border:1px solid #c0c0c0;"/>
								<span>~</span>
								<input type="text" id="endDate" class="date datePicker" style="width:52.5px; height:23px; border:1px solid #c0c0c0;"/>
							</div>
						</div>
					</li>
					<li>
						<span class="tit2"><fmt:message key='searchDriving.dayOfWeek'/></span>
						<div class="tab_select tab_select2">
							<div class="ui_radio_box selectWeek">
								<label for="dayMon"><fmt:message key='searchDriving.mon'/></label>
								<input type="checkbox" name="selectWeek" id="dayMon" value="1">
								<label for="dayTue"><fmt:message key='searchDriving.thu'/></label>
								<input type="checkbox" name="selectWeek" id="dayTue" value="2">
								<label for="dayWed"><fmt:message key='searchDriving.wen'/></label>
								<input type="checkbox" name="selectWeek" id="dayWed" value="3">
								<label for="dayThu"><fmt:message key='searchDriving.the'/></label>
								<input type="checkbox" name="selectWeek" id="dayThu" value="4">
								<label for="dayFri"><fmt:message key='searchDriving.fri'/></label>
								<input type="checkbox" name="selectWeek" id="dayFri" value="5">
								<label for="daySat"><fmt:message key='searchDriving.sat'/></label>
								<input type="checkbox" name="selectWeek" id="daySat" value="6">
								<label for="daySun"><span style="color: red"><fmt:message key='searchDriving.sun'/></span></label>
								<input type="checkbox" name="selectWeek" id="daySun" value="0">
							</div>
						</div>
						<div class="select_time">
							<span class="tit2"><fmt:message key='searchDriving.time'/></span>
							<input type="text" class="hourOnly numberOnly" id="searchStartH" style="width:22px"/><span class="time1"><fmt:message key='searchDriving.H2'/></span>
							<input type="text" class="minuteOnly numberOnly" id="searchStartM" style="width:22px"/><span class="time1"><fmt:message key='searchDriving.M2'/></span><span class="time1">~</span>
							<input type="text" class="hourOnly numberOnly" id="searchEndH" style="width:22px"/><span class="time1"><fmt:message key='searchDriving.H2'/></span>
							<input type="text" class="minuteOnly numberOnly" id="searchEndM" style="width:22px"/><span class="time1"><fmt:message key='searchDriving.M2'/></span>
						</div>
					</li>
				</ul>
			</div>
		</form>
		<div class="search_box2_2 mat26">
			<span class="tit"><b></b><fmt:message key='searchDriving.drivingInfo'/></span>
			<ul class="search_box2_3">
				<li>
					<span class="tit2"><fmt:message key='searchDriving.avgSpeed2'/></span><div class="tab_select mar26" style="background:none; border:none;">
						<input type="text" id="searchMeanSpeedMin" class="numberOnly" style="width:30px; height:23px; border:1px solid #c0c0c0;"/>
						<span>~</span>
						<input type="text" id="searchMeanSpeedMax" class="numberOnly" style="width:30px; height:23px; border:1px solid #c0c0c0;"/>
					</div><span class="tit2"><fmt:message key='searchDriving.highSpeed2'/></span><div class="tab_select mar26" style="background:none; border:none;">
						<input type="text" id="searchHighSpeedMin" class="numberOnly"" style="width:30px; height:23px; border:1px solid #c0c0c0;"/>
						<span>~</span>
						<input type="text" id="searchHighSpeedMax" class="numberOnly" style="width:30px; height:23px; border:1px solid #c0c0c0;"/>
					</div><span class="tit2"><fmt:message key='searchDriving.fco2'/></span><div class="tab_select mar26" style="background:none; border:none;">
						<input type="text" id="searchFcoMin" class="numberOnly" style="width:30px; height:23px; border:1px solid #c0c0c0;"/>
						<span>~</span>
						<input type="text" id="searchFcoMax" class="numberOnly" style="width:30px; height:23px; border:1px solid #c0c0c0;"/>
					</div>
				</li>
			</ul>
		</div>
		<div class="search_box2_2 search_box2_3 mat26">
			<span class="tit"><b></b><fmt:message key='searchDriving.drivingLocation'/></span>
			<ul class="search_box2_3">
				<li>
					<span class="tit2"><fmt:message key='searchDriving.city2'/></span><div class="tab_select select_type2_1 mar26">
						<div class="select_type2" style="height:27px;">
							<div class="select_type1_1" style="height:27px;">
								<select class="select form" style="width:160px;" id="cityLev1">
									<option value=""><fmt:message key='searchDriving.city3'/></option>
								</select>
							</div>
						</div>
					</div><span class="tit2"><fmt:message key='searchDriving.country2'/></span><div class="tab_select select_type2_1 mar6">
						<div class="select_type2" style="height:27px;">
							<div class="select_type1_1" style="height:27px;">
								<select class="select form" style="width:160px;" id="cityLev2">
									<option value=""><fmt:message key='searchDriving.country3'/></option>
								</select>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<div class="select_box1">
		<span class="select_box1_txt"><fmt:message key='searchDriving.searchResult'/>  <b id="searchCnt"></b></span>
		<div class="tab_select select_type2_1 mar4">
			<div class="select_type2">
				<div>
					<select id="searchOrder" class="select form">
						<option value="driving"><fmt:message key='searchDriving.orderDriving'/></option>
						<option value="regDate"><fmt:message key='searchDriving.orderReg'/></option>
					</select>
				</div>
			</div>
		</div>
	</div>
	<table class="table1 table1_2" id="contentsTable" >
		<colgroup>
			<col style="width:90px;" />
			<col style="width:120px;" />
			<col />
			<col style="width:145px;" />								
			<col style="width:69px;" />
		</colgroup>
		<thead>
			<tr>
				<th style="height:0px;"></th>
				<th style="height:0px;"></th>
				<th style="height:0px;"></th>
				<th style="height:0px;"></th>
				<th style="height:0px;"></th>
			</tr>
			<tr>
				<th colspan="2"><fmt:message key='searchDriving.vehicleUser'/></th>
				<th><fmt:message key='searchDriving.drivingInfo2'/></th>
				<th>
					<select class='th_select' id="searchVehicleState">
						<option value=""><fmt:message key='searchDriving.vehicleStatus'/></option>
						<option value="1"><fmt:message key='searchDriving.normal'/></option>
						<option value="2"><fmt:message key='searchDriving.trouble'/></option>
					</select>
				</th>
				<th>
					<select class='th_select' id="searchWarning">
						<option value=""><fmt:message key='searchDriving.warning'/></option>
						<option value="warningLocation"><fmt:message key='searchDriving.area'/></option>
						<option value="warningTime"><fmt:message key='searchDriving.time'/></option>
					</select>
				</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
	<div class="txt1">
		<h1><fmt:message key='searchDriving.txt1'/></h1>
		<p><b><fmt:message key='searchDriving.txt2_1'/> </b><fmt:message key='searchDriving.txt2_2'/></p>
		<p><b><fmt:message key='searchDriving.txt3_1'/> </b><fmt:message key='searchDriving.txt3_2'/></p>
		<p><b><fmt:message key='searchDriving.txt4_1'/> </b><fmt:message key='searchDriving.txt4_2'/></p>
		<p><b><fmt:message key='searchDriving.txt5_1'/> </b><fmt:message key='searchDriving.txt5_2'/></p>
		<p><b><fmt:message key='searchDriving.txt6_1'/> </b><fmt:message key='searchDriving.txt6_2'/></p>
		<br/><br/>
		<h1><fmt:message key='searchDriving.txt7_1'/></h1>
		<p><span class="status on"></span><fmt:message key='searchDriving.txt7_2'/></p>
		<p><span class="status off"></span><fmt:message key='searchDriving.txt7_3'/></p>
		<br/><br/>
		<h1><fmt:message key='searchDriving.parkingLocation'/></h1>
		<p><fmt:message key='searchDriving.parkingLocationTxt1'/><br/><fmt:message key='searchDriving.parkingLocationTxt2'/></p>
	</div>
</div>
<div class="black"></div>
<div class="pop2">
	<div class="pop_layout">
		<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt="" /></a>
		<h1><fmt:message key='searchDriving.viewTrak'/></h1>
		<h2 class="title3 pat42">
			<p><b><fmt:message key='searchDriving.start2'/> </b><span id="popStart"></span></p>
			<p><b><fmt:message key='searchDriving.end2'/> </b><span id="popEnd"></span></p>
		</h2>
		<h3 class="title4" style="padding-bottom:0px;"><b><fmt:message key='searchDriving.vehicle'/> </b><i id="popVehicle"></i></h3>
		<div class="map_area">
			<div id="map_canvas">
				<iframe id="drivingMap" src="#none" width="760px" height="339px"></iframe>
			</div>
		</div>
		<div class="map_txt1">
			<div>
				<span class="w109">
					<img src="${pathCommon}/images/m_img1.png" alt=""><fmt:message key='searchDriving.rapidStart'/> <strong class="rapidStart">0<fmt:message key='searchDriving.cnt3'/></strong>
				</span><span class="w120">
					<img src="${pathCommon}/images/m_img2.png" alt=""><fmt:message key='searchDriving.rapidStop'/> <strong class="rapidStop">0<fmt:message key='searchDriving.cnt3'/></strong>
				</span>
				<span class="w120">
					<img src="${pathCommon}/images/m_img4.png" alt=""><fmt:message key='searchDriving.rapidAccel'/> <strong class="rapidAccel">0<fmt:message key='searchDriving.cnt3'/></strong>
				</span>
				<span class="w109">
					<img src="${pathCommon}/images/m_img5.png" alt=""><fmt:message key='searchDriving.rapidDeaccel'/> <strong class="rapidDeaccel">0<fmt:message key='searchDriving.cnt3'/></strong>
				</span>
				<span class="w120">
					<img src="${pathCommon}/images/m_img10.png" alt=""><fmt:message key='searchDriving.rapidTurn'/> <strong class="rapidTurn">0<fmt:message key='searchDriving.cnt3'/></strong>
				</span>
			</div>
			<div>
				<span class="w109">
					<img src="${pathCommon}/images/m_img11.png" alt=""><fmt:message key='searchDriving.rapidUTurn'/> <strong class="rapidUtern">0<fmt:message key='searchDriving.cnt3'/></strong>
				</span>
				<span class="w109">
					<img src="${pathCommon}/images/m_img6.png" alt=""><fmt:message key='searchDriving.overSpeed'/> <strong class="overSpeed">0<fmt:message key='searchDriving.cnt3'/></strong>
				</span>
				<span class="w120">
					<img src="${pathCommon}/images/m_img7.png" alt=""><fmt:message key='searchDriving.overSpeedLong'/> <strong class="overSpeedLong">0<fmt:message key='searchDriving.cnt3'/></strong>
				</span>
				<%-- <span class="w178">
					<img src="${pathCommon}/images/m_img12.png" alt="">심야운전 <strong class="nightDrive">0회</strong>
				</span> --%>
			</div>
		</div>
		<div class="map_txt2"><fmt:message key='searchDriving.poiTxt'/></div>
	</div>
</div>
<div class="pop7" style="display:none;">
	<div class="pop6_area">
		<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt=""></a>
		<h1><fmt:message key='searchDriving.dtcHstr'/></h1>
		<table class="table1 breakdown" id="contentsTableDtc">
			<colgroup>
				<col style="width:130px;"/>
				<col style="width:60px;"/>
				<col style="width:90px;"/>
				<col />
			</colgroup>
			<thead>
				<tr>
					<th><fmt:message key='searchDriving.dtcTime'/></th>
					<th><fmt:message key='searchDriving.dtcCode'/></th>
					<th><fmt:message key='searchDriving.dtcPart'/></th>
					<th><fmt:message key='searchDriving.dtcContents'/></th>
				</tr>
			</thead>
			<tbody>	
			</tbody>
		</table>
		<br><br>
		<div class="btn_type1"><a href="#none" class="btn_ok"><fmt:message key='searchDriving.confirm2'/></a></div>
	</div>
</div>
