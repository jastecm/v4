<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<fmt:setLocale value="${_LANG}" scope="request" />
<fmt:setBundle basename="properties.messages.rest.message" />
<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<%-- <c:if test="${viewRule eq '1'}"> --%>
<c:if test="${VDAS.rule ne '2' }">
<script type="text/javascript">
var lazyAccount;
var lazyVehicle;
var lazyMulti;
var vehicleState;
var allocateState;
var selectUser;
var corpGroup;
var vehicleKey;
var accountKey;
var searchArealev1;
var searchArealev2;
var searchTime = 24;
var addBoxChk;
var addBoxSelect;
var addMapIndex = 1;

$(document).ready(function(){
	
	initMenuSel("M1003");
	
	$(".tabItem").on("click",function(){
		$(".tabItem").removeClass("on");
		$(this).addClass("on");
		
		$(".div1").hide();
		var tab = $(this).data("tab");
		$("."+tab).show();
		
		//$.lazyLoader.search(lazy);
	});

	$('.ui_radio_box input[type=radio]').checkboxradio({
        icon: false
    });

	$VDAS.ajaxCreateSelect("/com/getGroupCode.do",{},$("#corpGroup"),false,"전체 부서","${rtv.groupKey}","${_LANG}",null,null);
	
	$VDAS.ajaxCreateSelect("/com/getCityLev1.do",{},$("#cityLev1"),false,"<fmt:message key="searchlocation.city"/>","","${_LANG}",false,null);

	$("#cityLev1").on("change",function(){
		$VDAS.ajaxCreateSelect("/com/getCityLev2.do",{city:$(this).val()},$("#cityLev2"),false,"<fmt:message key="searchlocation.country"/>","","${_LANG}",false,null);	
		searchArealev1 = $(this).val();
	});
	
	$("#cityLev2").on("change",function(){
		searchArealev2 = $(this).val();
	});
	
	$("#corpGroup").on("change",function(){
		corpGroup = $(this).val();
		if(selectUser == 'vehicle'){
			vehicleKey = "";
			$("input[name=searchText]").val("");
			$("#vehicleFrm input[name=searchGroup]").val(corpGroup);
		}
		else if(selectUser == 'account'){
			accountKey = "";
			$("input[name=searchText]").val("");
			$("#accountFrm input[name=searchGroup]").val(corpGroup);
		}
	});
	
	$("#searchHour").on("change",function(){
		searchTime = $(this).val();
		
		var mapObj = $("#locationMap").get(0).contentWindow;
		mapObj.lastestTime = searchTime;
		mapObj._o = mapObj.createOption();
	});
	
	$("#selectUser").on("change",function(){
		if($(this).val()){
			selectUser = $(this).val();
			$("#searchBox").show();
		}
		else {
			selectUser = null;
			$("#searchBox").hide();
		}
	});
	
	$("body").on("click",".btn_ok",function(){
		$("input[name=rdo_sel]").each(function(){
			if($(this).is(":checked")){
				var name = $(this).data("name");
				var key = $(this).data("key");
				if(selectUser == 'vehicle'){
					vehicleKey = key;
				}
				else if(selectUser == 'account'){
					accountKey = key;
				}
				$("input[name=searchText]").val(name);
				return false;
			}
		})
		$(".pop7").fadeOut();
		$(".black").fadeOut();
	});
	
	$("body").on("click","#searchBox input[name=searchText]",function(e){
		$("#searchBox a").trigger("click");
	});
	
	$("body").on("keyup","#searchTextVehicle",function(e){
		if(e.keyCode == 13) $("#btn_searchVehicle").trigger("click");
	});
	
	$("body").on("keyup","#searchTextAccount",function(e){
		if(e.keyCode == 13) $("#btn_searchAccount").trigger("click");
	});
	
	$("body").on("click","#btn_searchVehicle",function(){
		$.lazyLoader.search(lazyVehicle);
	})
	
	$("body").on("click","#btn_searchAccount",function(){
		$.lazyLoader.search(lazyAccount);
	})
	
	$("body").on("click","#searchBox a",function(){
		$(".black").fadeIn();
		if(selectUser == 'vehicle'){
			$(".vehicle").fadeIn();
			$.lazyLoader.search(lazyVehicle);
		}
		else if(selectUser == 'account'){
			$(".account").fadeIn();
			$.lazyLoader.search(lazyAccount);
		}
	});

	$("#locationMap").attr("src","${path}/map/trip.do?searchType=vehicle");
	$("body").on("click",".btn_search2",function(){
		var mapObj = $("#locationMap").get(0).contentWindow;
		mapObj.searchType = "vehicle";
		mapObj.vehicleState = vehicleState;
		mapObj.allocateState = allocateState;
		if(selectUser == "vehicle"){
			mapObj.accountGroup = null;
			mapObj.accountKey = null;
			mapObj.vehicleGroup = corpGroup;
			mapObj.vehicleKey = vehicleKey;
		}
		else if(selectUser == "account"){
			mapObj.vehicleGroup = null;
			mapObj.vehicleKey = null;
			mapObj.accountGroup = corpGroup;
			mapObj.accountKey = accountKey;
		}
		else {
			mapObj.vehicleGroup = null;
			mapObj.vehicleKey = null;
			mapObj.accountGroup = null;
			mapObj.accountKey = null;
		}
		mapObj.searchArealev1 = searchArealev1;
		mapObj.searchArealev2 = searchArealev2;
		mapObj.lastestTime = searchTime;
		mapObj._o = mapObj.createOption();
		mapObj.outerReset();
	});
		//$("#locationMap").get(0).alwaysPopup;

	
	$(".btn_add").click(function(){
		var selectedKey;
		$(".multimap").removeClass("last");
		$("#add_box").before(function(){
			var strHtml = "";

			addMapIndex++;
			strHtml += '<li class="map_element">';
			strHtml += '<div class="map_box">';
			strHtml += '<div class="map">';
			strHtml += '<iframe class="multimap last" src="#none" width="400px" height="276px"></iframe>';
			strHtml += '</div>';
			strHtml += '<div class="map_tool">';
			strHtml += '<div class="radio_box">';
			strHtml += '<input type="radio" name="r_'+addMapIndex+'" class="accountRadio" value="account"';
				if(addBoxChk == "account") strHtml += ' checked="checked"';
			strHtml += '>';
			strHtml += '<label for=""><fmt:message key='searchlocation.user'/></label>';
			strHtml += '<input type="radio" name="r_'+addMapIndex+'" class="vehicleRadio" value="vehicle"';
				if(addBoxChk == "vehicle") strHtml += ' checked="checked"';
			strHtml += ' style="margin-left: 15px;">';
			strHtml += '<label for=""><fmt:message key='searchlocation.vehicle'/></label>';
			strHtml += '<select class="select form btn_multiSelectSearch" style="width:100px; margin-left: 15px;">';			
			strHtml += '<option value=""></option>';
			strHtml += '</select>';
			strHtml += '</div>';
			strHtml += '<div class="btn_box btn_box2">';
			strHtml += '<a href="#none" class="btn_del" style="float:right;"></a>';
			strHtml += '</div></div></div></li>';

			return strHtml;
		});
		
		if(addBoxChk == "account")
			$(".last").attr("src","${path}/map/trip.do?searchType=vehicle&accountKey="+addBoxSelect+"");
		else if(addBoxChk == "vehicle")
			$(".last").attr("src","${path}/map/trip.do?searchType=vehicle&vehicleKey="+addBoxSelect+"");
		else
			$(".last").attr("src","${path}/map/trip.do?searchType=vehicle");
		
		var divObj = $(".last").parents("li");

		if(addBoxChk == "account")
			$VDAS.ajaxCreateSelect("/com/getGroupUserList.do",{},divObj.find(".select"),false,"<fmt:message key="searchlocation.allUser"/>",addBoxSelect,"${VDAS.lang}",null,null);
		else if(addBoxChk == "vehicle")
			$VDAS.ajaxCreateSelect("/com/getGroupVehicleList.do",{},divObj.find(".select"),false,"<fmt:message key="searchlocation.allVehicle"/>",addBoxSelect,"${VDAS.lang}",null,null);
	});
	
	$("body").on("click",'.radio_box input[type=radio]',function(){
		var divObj = $(this).parent("div");
		if($(this).val() == "account")
			$VDAS.ajaxCreateSelect("/com/getGroupUserList.do",{},divObj.find(".select"),false,"<fmt:message key="searchlocation.allUser1"/>","","${_LANG}",null,null);
		else if($(this).val() == "vehicle")
			$VDAS.ajaxCreateSelect("/com/getGroupVehicleList.do",{},divObj.find(".select"),false,"<fmt:message key="searchlocation.allVehicle1"/>","","${_LANG}",null,null);
	});
	
	$('.add_radio_box input[type=radio]').on("click",function(){
		addBoxChk = $(this).val();
		addBoxSelect = $('.add_radio_box .select').val();
	});
	
	$('.add_radio_box .select').on("change",function(){
		addBoxSelect = $(this).val();
	});
	
	$("body").on("change",".btn_multiSelectSearch",function(){
		
		var radioVal = $(this).parent("div");
		var mapObj = $(this).parentsUntil(".map_element").find(".multimap").get(0).contentWindow;
		
		if(radioVal.find(".accountRadio").is(":checked")){
			mapObj.vehicleKey = null;
			mapObj.accountKey = $(this).val();
			mapObj._o = mapObj.createOption();
			mapObj.outerReset();
		}
		else if(radioVal.find(".vehicleRadio").is(":checked")){
			mapObj.accountKey = null;
			mapObj.vehicleKey = $(this).val();
			mapObj._o = mapObj.createOption();
			mapObj.outerReset();
		}
		
	});
	
	setInterval(function(){		
		$(".btn_multiSelectSearch").trigger("change");
		//$(".btn_search2").trigger("click");
	},30000);	
	
	$("body").on("click",".btn_del",function(){
		$(this).parentsUntil(".map_list").remove();
	});

	$('#vehicleState input[type=radio]').on("click",function(){
        vehicleState = $(this).val();
    });
	
	$('#allocateState input[type=radio]').on("click",function(){
        allocateState = $(this).val();
    });
	
	if(!lazyVehicle){
		lazyVehicle = $("#contentsTableVehicle").lazyLoader({
			searchFrmObj : $("#vehicleFrm")
			,searchFrmVal : {
				searchText : $("#searchTextVehicle")
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/com/getVehicleList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var strHtml = '';
				var data = r.rtvList;
				console.log(data);
				
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += '';
					strHtml += '<tr>';
					strHtml += '<td>';
					strHtml += '<input type="radio" name="rdo_sel" class="rdo_sel" data-name="'+vo.plateNum+'" data-key="'+vo.vehicleKey+'">';
					strHtml += '</td>';
					strHtml += '<td>';
					strHtml += '<p>'+vo.manufacture+'</p>';
					strHtml += '</td>';
					strHtml += '<th class="car_list">';
					strHtml += '<strong class="car_img"><img src="${pathCommon}/images/vehicle/'+vo.imgS+'.png" alt=""></strong>';
					strHtml += '<a href="#none">';
					strHtml += '<span>'+vo.modelMaster+'</span>'+vo.plateNum+'</a>';
					strHtml += '</th>';
					strHtml += '<td>';
					strHtml += '<p>'+vo.volume+' cc</p>';
					strHtml += '</td>';
					strHtml += '<td>';
					if(vo.vehicleBizType == '1')
						strHtml += '<p><fmt:message key='searchlocation.vehicleBizType1'/></p>';
					else if(vo.vehicleBizType == '2')
						strHtml += '<p><fmt:message key='searchlocation.vehicleBizType2'/></p>';
					else if(vo.vehicleBizType == '3')
						strHtml += '<p><fmt:message key='searchlocation.vehicleBizType3'/></p>';
					else if(vo.vehicleBizType == '4')
						strHtml += '<p><fmt:message key='searchlocation.vehicleBizType4'/></p>';
					strHtml += '</td>';
					strHtml += '<th>';
					strHtml += '<p>'+vo.year+'</p>';
					strHtml += '<p>'+number_format((vo.totDistance/1000).toFixed(1))+' km</p>';
					strHtml += '</th>';
					strHtml += '<th>';
					strHtml += '<p></p>';
					strHtml += '<p>'+vo.fuelType+'</p>';
					strHtml += '</th></tr>';
				}
				
				return strHtml;
			}
		});
	}
	
	if(!lazyAccount){
		lazyAccount = $("#contentsTableAccount").lazyLoader({
			searchFrmObj : $("#accountFrm")
			,searchFrmVal : {
				searchText:$("#searchTextAccount")					
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/com/getUserList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.rtvList;
				var strHtml = "";
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += "<tr>";
					strHtml += '<td>';
					strHtml += '<input type="radio" name="rdo_sel" class="rdo_sel" data-name="'+vo.name+'" data-key="'+vo.accountKey+'">';
					strHtml += '</td>';					
					strHtml += '<th>';
					strHtml += '<strong class="car_img">';
					if(vo.imgId&&vo.imgId.length > 0)
						strHtml += '<img class="svg-clipped" style="width:49px;height:49px;" src="${defaultPath}/com/getUserImg.do?uuid='+vo.imgId+'" alt="" />';
					else strHtml += '<img class="svg-clipped" src="${pathCommon}/images/user_img7.jpg" alt="" style="width:49px;height:49px;"/>';					
					strHtml += '</strong>';
					strHtml += '<a href="#none">';
					strHtml += '<span>'+vo.accountNm+'</span>';
					strHtml += vo.name;
					strHtml += '</a>';
					strHtml += '</th>';
					strHtml += '<td>';
					strHtml += '<p>'+vo.corpNm+'/'+vo.corpGroupNm+'/'+vo.corpPosition+'</p>';
					strHtml += '</td>';
					strHtml += '<td>';
					strHtml += '<p>'+vo.phone+'</p>';
					strHtml += '<p>'+vo.mobilePhone+'</p>';
					strHtml += '</td>';
					strHtml += '<td>';
					strHtml += '<p>'+vo.authMail+'</p>';
					strHtml += '</td>';
					strHtml += '<td>'+vo.descript+'</td>';
					strHtml += '</tr>';	
				}
				
				
				return strHtml;
			}
		});
	}
	
	$(".radio_box input[type=radio]").trigger("click");
	$(".multimap").attr("src","${path}/map/trip.do?searchType=vehicle");
	getCnts();
});
function getCnts(){
	$VDAS.http_post("/vehicle/getVehicleCnt.do",{},{
		success : function(r){
			var data = r.result;
			$("#titleCnt1").text(data.used+"<fmt:message key="searchlocation.vehicleCnt1"/>");
			$("#titleCnt2").text(data.using+"<fmt:message key="searchlocation.vehicleCnt2"/>");
			$("#titleCnt3").text((data.used-data.using)+"<fmt:message key="searchlocation.vehicleCnt3"/>");
		}
	});
}
</script>
<form id="vehicleFrm" style="display:none">
	<input type="hidden" name="searchGroup" value="" />
	<input type="hidden" name="searchText" value="" />
	<input type="hidden" name="searchUsed" value="1" />
	<input type="hidden" name="searchType" value="plateNum" />
</form>
<form id="accountFrm" style="display:none">
	<input type="hidden" name="searchGroup" value="" />
	<input type="hidden" name="searchText" value="" />
	<input type="hidden" name="searchType" value="useNm" />
</form>
<div class="right_layout">
	<div class="tit_sub"><fmt:message key='searchlocation.txt1'/>
		<p><fmt:message key='searchlocation.txt1_1'/></p>
	</div>
	<div class="sub_top1">
		<span>
			<fmt:message key='searchlocation.regVehicleCount'/>
			<b id="titleCnt1"></b></span>
			<span><fmt:message key='searchlocation.useVehicleCount'/>
			<b id="titleCnt2"></b></span>
			<span><fmt:message key='searchlocation.stayVehicleCount'/>
			<b id="titleCnt3"></b></span>
		<strong id="nowDate"></strong>
	</div>
	<div class="tab_layout">
		<div class="pageTab">
			<ul class="tab_nav">
				<li class="tabItem on" data-tab='single'><a href="#none"><fmt:message key='searchlocation.singleVehicle'/></a></li>
				<li class="tabItem" data-tab='multi'><a href="#none"><fmt:message key='searchlocation.multiVehicle'/></a></li>
			</ul>
		</div>
		<div class="div1 single">
			<form id="frmSearch" method="post" onSubmit="return false;">
				<div class="filter_box">
					<div class="filter_row">
						<span class="f_tit"><fmt:message key='searchlocation.vehicle1'/></span>
						<div class="ui_radio_box" id="vehicleState">
							<label for="wholeVehicle"><fmt:message key='searchlocation.wholeVehicle'/></label>
							<input type="radio" name="vehicleState" id="wholeVehicle" value="" checked="checked">
							<label for="driving"><fmt:message key='searchlocation.driving'/></label>
							<input type="radio" name="vehicleState" id="driving" value="1">
							<label for="parking"><fmt:message key='searchlocation.parking'/></label>
							<input type="radio" name="vehicleState" id="parking" value="0">
						</div>
						<div class="ui_radio_box" id="allocateState">
							<label for="wholeAllocation"><fmt:message key='searchlocation.wholeVehicle1'/></label>
							<input type="radio" name="allocateState" id="wholeAllocation" value="" checked="checked">
							<label for="allocate"><fmt:message key='searchlocation.allocate'/></label>
							<input type="radio" name="allocateState" id="allocate" value="1">
							<label for="release"><fmt:message key='searchlocation.release'/></label>
							<input type="radio" name="allocateState" id="release" value="0">
						</div>
					</div>
					<div class="filter_row">
						<span class="f_tit"><fmt:message key='searchlocation.user1'/></span>
						<select class="select form" style="width:160px;" id="selectUser">
							<option value=""><fmt:message key='searchlocation.all'/></option>
							<option value="vehicle"><fmt:message key='searchlocation.vehicle2'/></option>
							<option value="account"><fmt:message key='searchlocation.user2'/></option>
						</select>
						<span id="searchBox" style="display:none;">
							<select class="select form" style="width:160px;" id="corpGroup">
								<option value=""><fmt:message key='searchlocation.corpGroup'/></option>
							</select>
							<div class="car_choice_search_box" style="float:none; display:inline-block;">
								<input type="text" name="searchText" readonly style="padding-left:20px"/><a href="#none" id="btn_ajaxPop"><img src="${pathCommon}/images/btn_search3.jpg" alt=""></a>
							</div>
						</span>
					</div>
					<div class="filter_row">
						<span class="f_tit"><fmt:message key='searchlocation.area'/></span>
						<select class="select form" style="width:160px;" id="cityLev1">
							<option value=""><fmt:message key='searchlocation.cityLev1'/></option>
						</select>
						<select class="select form" style="width:160px;" id="cityLev2">
							<option value=""><fmt:message key='searchlocation.cityLev2'/></option>
						</select>
					</div>
					<span>
						<div class="btn_area2"><a href="#none" class="btn_search2"><fmt:message key='searchlocation.search'/></a></div>
					</span>
				</div>
			</form>
			<div class="map_box">
				<div id="map_canvas">
					<iframe id="locationMap" src="#none" width="806px" height="339px"></iframe>
				</div>
				<div class="map_tool">
					<span class="map_tit" style="width:inherit">
						<fmt:message key='searchlocation.txt2'/>
					</span>
					<select class="select form" style="width:105px;" id="searchHour">
						<option value="24"><fmt:message key='searchlocation.searchLast24Hour'/></option>
						<option value="12"><fmt:message key='searchlocation.searchLast12Hour'/></option>
						<option value="6"><fmt:message key='searchlocation.searchLast6Hour'/></option>
					</select>
				</div>
			</div>
		</div>
		<div class="div1 multi" style="display:none">
			<ul class="map_list">
				<li class="map_element">
					<div class="map_box">
						<div class="map">
							<iframe class="multimap" src="#none" width="400px" height="276px"></iframe>
						</div>
						<div class="map_tool">
							<div class="radio_box">
								<input type="radio" name="r_1" class="accountRadio" value="account" >
															
								<label for=""><fmt:message key='searchlocation.user3'/></label>
								<input type="radio" name="r_1" class="vehicleRadio" value="vehicle" style="margin-left: 15px;" >
								<label for=""><fmt:message key='searchlocation.vehicle3'/></label>
								<select class="select form btn_multiSelectSearch" style="width:100px; margin-left: 15px;">
									<option value=""></option>
								</select>
							</div>
							<div class="btn_box btn_box2">
								<a href="#none" class="btn_del" style="float:right;"></a>
							</div>
						</div>
					</div>
				</li>
				<li id="add_box">
					<div class="map_add_box" style="padding:68px 0;">
						<h3><fmt:message key='searchlocation.txt3'/></h3>
						<div class="radio_box add_radio_box">
							<input type="radio" name="r_a" value="account" >
							<label><fmt:message key='searchlocation.user4'/></label>
							<input type="radio" name="r_a" value="vehicle" style="margin-left: 15px;">
							<label><fmt:message key='searchlocation.vehicle4'/></label><p></p>
							<select class="select form multiSearch" style="width:220px;" id="selectAdd">
								<option value=""><fmt:message key='searchlocation.select'/></option>
							</select>
							<div class="btn_area2"><a href="#none" class="btn_cancel btn_add"><fmt:message key='searchlocation.add'/></a></div>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<div class="desc_box">
		<div class="icon_desc">
			<span><em><fmt:message key='searchlocation.txt4'/></em></span>
			<span><fmt:message key='searchlocation.txt4_1'/><img src="${pathCommon}/images/ico_map_car_on.png" alt=""></span>
			<span><fmt:message key='searchlocation.txt4_2'/><img src="${pathCommon}/images/ico_map_car_off.png" alt=""></span>
			<span>|</span>
			<span><em><fmt:message key='searchlocation.txt5'/></em></span>
			<span><fmt:message key='searchlocation.txt5_1'/><img src="${pathCommon}/images/ico_map_p_on.png" alt=""></span>
			<span><fmt:message key='searchlocation.txt5_2'/><img src="${pathCommon}/images/ico_map_p_off.png" alt=""></span>
			<%-- <span>|</span>
			<span><em><fmt:message key='searchlocation.txt6'/></em><img src="${pathCommon}/images/ico_map_notice.png" alt=""></span> --%>
		</div>

		<h3 class="desc_tit"><fmt:message key='searchlocation.txt7'/></h3>
		<p class="desc_txt">
			<fmt:message key='searchlocation.txt7_1'/>
			<br/>
			<fmt:message key='searchlocation.txt7_2'/>
		</p>
	</div>
</div>
<div class="black"></div>
<div class="pop7 account" style="display:none;">
	<div class="pop6_area">
		<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt=""></a>
		<h1><fmt:message key='searchlocation.userSelect'/></h1>
		<div class="txt">
			<p><fmt:message key='searchlocation.txt8'/></p>
		</div>
		<div class="car_choice_btn2">
			<strong class="car_choice_search_box">
				<input type="text" placeholder="직접검색" id="searchTextAccount"/><a href="#none" id="btn_searchAccount"><img src="${pathCommon}/images/btn_search2.jpg" alt=""></a>
			</strong>
		</div>
		<table class="table1" id="contentsTableAccount">
			<colgroup>
				<col style="width:60px;"/>
				<col />
				<col style="width:130px;"/>
				<col style="width:125px;"/>
				<col style="width:140px;"/>
				<col style="width:136px;"/>
			</colgroup>
			<thead>
				<tr>
					<th><fmt:message key='searchlocation.select1'/></th>
					<th><fmt:message key='searchlocation.userInfo'/></th>
					<th><fmt:message key='searchlocation.corpCompRank'/></th>
					<th><fmt:message key='searchlocation.telephone'/></th>
					<th><fmt:message key='searchlocation.email'/></th>
					<th><fmt:message key='searchlocation.descript'/></th>
				</tr>
			</thead>
			<tbody>	
			</tbody>
		</table>
		<br><br>
		<div class="btn_type1"><a href="#none" class="btn_cancel"><fmt:message key='searchlocation.cancle'/></a><a href="#none" class="btn_ok"><fmt:message key='searchlocation.ok'/></a></div>
	</div>
</div>
<div class="pop7 vehicle" style="display:none;">
	<div class="pop6_area">
		<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt="" /></a>
		<h1><fmt:message key='searchlocation.selectVehicle'/></h1>
		<div class="txt">
			<p><fmt:message key='searchlocation.selectVehicleDesc'/></p>
		</div>
		<div class="car_choice_btn2">
			<div class="car_choice_search_box">
				<input type="text" placeholder="직접검색" id="searchTextVehicle"/><a href="#none" id="btn_searchVehicle"><img src="${pathCommon}/images/btn_search3.jpg" alt=""></a>
			</div>
		</div>
		<table class="table1" id="contentsTableVehicle">
			<colgroup>
				<col style="width:60px;" />
				<col style="width:70px;" />
				<col style="width:230px;" />
				<col style="width:70px;" />
				<col style="width:100px;" />
				<col style="width:112px;" />
				<col style="width:118px;" />
			</colgroup>
			<thead>
				<tr>
					<th></th>
					<th><fmt:message key='searchlocation.manufacture'/></th>
					<th><fmt:message key='searchlocation.vehicleInfo'/></th>
					<th><fmt:message key='searchlocation.volumn'/></th>
					<th><fmt:message key='searchlocation.vehicleBizType'/></th>
					<th><fmt:message key='searchlocation.yearAndDistance'/></th>
					<th><fmt:message key='searchlocation.colorAndFuelType'/></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		<br><br>
		<div class="btn_type1"><a href="#none" class="btn_cancel"><fmt:message key='searchlocation.cancle1'/></a><a href="#none" class="btn_ok"><fmt:message key='searchlocation.ok1'/></a></div>
	</div>
</div>
</c:if>
<c:if test="${VDAS.rule eq '2'}">
$(document).ready(function(){
	initMenuSel("M1003");
});
</script>
<div class="right_layout">
	<img src="${pathCommon}/images/${_LANG}/authError.png" alt="" style="width:100%">
</div>
</c:if>