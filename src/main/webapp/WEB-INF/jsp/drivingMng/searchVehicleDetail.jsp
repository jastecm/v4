<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<fmt:setLocale value="${_LANG}" scope="request" />
<fmt:setBundle basename="properties.messages.rest.message" />
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/index.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jqplot/jquery.jqplot.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jqplot/jqplot.barRenderer.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jqplot/jqplot.highlighter.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jqplot/jqplot.cursor.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jqplot/jqplot.pointLabels.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jqplot/jqplot.categoryAxisRenderer.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jqplot/jqplot.canvasAxisTickRenderer.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jqplot/jqplot.canvasTextRenderer.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jqplot/jqplot.json2.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/common/css/jquery.jqplot.css" />

<style>
	.spantd{
		width: auto !important;
	}
</style>
<script type="text/javascript">
var lazy;
var lazyRoute;
var lazyClinic;
var lazyDtc;
var tWidth;

var calendar;
var tabMapInitSearch = true;

$(document).ready(function(){
	initMenuSel("M1001");

	var Now = new Date();
	var NowY = Now.getFullYear();
	var NowM = LPAD(''+(Now.getMonth()+1),'0',2);
	var NowD = LPAD(''+Now.getDate(),'0',2);
	$("#searchFrm_vehicle input[name=searchMonth]").val(NowY+NowM);
	$("#searchMonth").val(NowY+"-"+NowM);
	//$("#searchFrm_route input[name=searchDate]").val(NowY+NowM+NowD);
	//$("#searchDateRoute").val(NowY+"/"+NowM+"/"+NowD);
	
	tWidth = Math.floor(($("#contentsTable1").width() - 216) / 3) - 1;

	$(".tabItem").on("click",function(){
		$(".tabItem").removeClass("on");
		$(this).addClass("on");
		
		$(".div1").hide();
		var tab = $(this).data("tab");
		$("."+tab).show();
		
		if(tab == 'route') {
			if(tabMapInitSearch){
				var tt = NowY+"/"+NowM+"/"+NowD;
				$("#searchDateRoute").val(tt);
				if($("#contentsTable1 input.startInfoDate").eq(0).val().trim().length != 0){
					$("#searchDateRoute").val($("#contentsTable1 input.startInfoDate").eq(0).val().substring(0,10));
				}
				
				$("#searchMap").trigger("click");
				tabMapInitSearch = false;
			}
		}
		else if(tab == 'clinic'){
			initPlot();
			$.lazyLoader.search(lazyClinic);
		}
	});
		
	$("body").on("click","#searchMap",function(){
		mapSearch();
	});

    

    initVehicleInfo();
    drivingSummary();
    
	if(!lazy){
		lazy = $("#contentsTable1").lazyLoader({
			searchFrmObj : $("#searchFrm_vehicle") // form을 3개로 분할
			,searchFrmVal : {
				vehicleKey : $("#vehicleKey")
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/driving/getDrivingList.do"
			,initSearch : true
			,createDataHtml : function(r){
				
				var strHtml = '';
				var data = r.rtvList;
				var hasScrollPx = 0;
				if($("#contentsTable1 tbody tr").length+data.length > 2) hasScrollPx = 17;
				
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += '<tr>';
					strHtml += '<td width="80px">';
					strHtml += '<div class="table1_box1" style="padding-bottom:15px;">';
					strHtml += '<img src="${pathCommon}/images/vehicle/'+vo.imgS+'.png" alt="" />';
					strHtml += '</div>';
					strHtml += '<div class="table1_box2">';
					strHtml += '<img src='+userImg(vo.driverImg)+' alt="" />';
					strHtml += '</div>';
					strHtml += '</td>';
					strHtml += '<td width="120px">';
					strHtml += '<div class="table1_box1 txt_left" style="padding-bottom:15px;">';
					strHtml += '<a href="#none">';
					strHtml += '<span>'+vo.modelMaster+'</span>';
					strHtml += ' '+vo.plateNum;
					strHtml += '</a>';
					strHtml += '</div>';
					strHtml += '<div class="table1_box2 txt_left">';
					strHtml += '<a href="#none" class="a_user" data-key="'+vo.accountKey+'">';
					strHtml += '<span>'+convertNullString(vo.driverGroupNm)+'/</span>';
					strHtml += ' '+convertNullString(vo.driverNm);
					strHtml += '</a>';
					strHtml += '<br>';
					strHtml += '<br>';
					if(vo.instantAllocate == '1')
						strHtml += '<a href="#none" class="btn btn_edit" data-key="'+vo.tripKey+'" data-allocateKey="'+vo.allocateKey+'" data-target="1">변경</a>';
					strHtml += '</div>';
					strHtml += '</td>';
					strHtml += '<td colspan="3" class="spantd" >';
					strHtml += '<div class="table1_box1 txt_left" style="padding-bottom:15px;">';
					
					if(convertNullString(vo.startAddr) == ''){
						if(vo.tripDetailState == 1){
							if("${VDAS.lang}" == "ko") vo.startAddr = '주행중';
							else vo.startAddr = 'now Driving';
						}else if(vo.tripDetailState == 2){
							if("${VDAS.lang}" == "ko") vo.startAddr = '주행정보를 생성중입니다.';
							else vo.startAddr = 'creating driving info';
						}else if(vo.tripDetailState == 3){
							if("${VDAS.lang}" == "ko") vo.startAddr = '주행정보를 생성중입니다.';
							else vo.startAddr = 'creating driving info';
						}
					}
					if(convertNullString(vo.endAddr) == ''){
						if(vo.tripDetailState == 1){
							if("${VDAS.lang}" == "ko") vo.endAddr = '주행중';
							else vo.endAddr = 'now Driving';
						}else if(vo.tripDetailState == 2){
							if("${VDAS.lang}" == "ko") vo.endAddr = '주행정보를 생성중입니다.';
							else vo.endAddr = 'creating driving info';
						}else if(vo.tripDetailState == 3){
							if("${VDAS.lang}" == "ko") vo.endAddr = '주행정보를 생성중입니다.';
							else vo.endAddr = 'creating driving info';
						}
					}
					
					
					strHtml += '<input type="hidden" class="startInfo" value="'+convertNullString(vo.startDate)+' '+convertNullString(vo.startAddr)+'">';
					strHtml += '<input type="hidden" class="startInfoDate" value="'+convertNullString(vo.startDate)+'">';
					strHtml += '<input type="hidden" class="endInfo" value="'+convertNullString(vo.endDate)+' '+convertNullString(vo.endAddr)+'">';
					
					strHtml += '<p><b><fmt:message key='searchvehicledetail.start'/> </b><span title="'+convertNullString(vo.startAddr)+'" style="font-weight:500;">'+convertNullString(vo.startDate)+' '+addrString(convertNullString(vo.startAddr))+'</span></p>';
					strHtml += '<p><b><fmt:message key='searchvehicledetail.end'/> </b><span class="endInfo" title="'+convertNullString(vo.endAddr)+'" style="font-weight:500;">'+convertNullString(vo.endDate)+' '+addrString(convertNullString(vo.endAddr))+'</span></p>';
					var gpsRightPx = 117-hasScrollPx;
					strHtml += '<a href="#none" style="right:'+gpsRightPx+'px;" class="btn_gps type02" data-gps="'+vo.tripKey+'" data-vehicle="'+vo.vehicleKey+'" data-account="'+vo.accountKey+'"><img src="${pathCommon}/images/gps.jpg" alt=""></a>';
					
					var stateChk = true;
					if((vo.workingAreaWarning == null || vo.workingAreaWarning == '')
							&& (vo.workingTimeWarning == null || vo.workingTimeWarning == '')
							&& (vo.dtcCnt == null || vo.dtcCnt == '0')){
						var px = 37-hasScrollPx;						
						strHtml += '<div class="p_time" style="right:'+px+'px;"><p>-</p></div>';
						stateChk = false;
					}
					
					if(stateChk){
						var first = true;
						var strState = "";
						
						if(vo.dtcCnt != null && vo.dtcCnt != '0'){
						//if(true){
							if(!first) strState += '<br>'; 
							strState += "<fmt:message key="searchvehicledetail.trouble"/>";
							first = false;
						}
						//if(true){
						if(vo.workingAreaWarning != null && vo.workingAreaWarning != ''){
							if(!first) strState += '<br>';
							strState += "<fmt:message key="searchvehicledetail.area"/>";
							first = false;
						}
						if(vo.workingTimeWarning != null && vo.workingTimeWarning != ''){
						//if(true){
							if(!first) strState += '<br>';
							strState += "<fmt:message key="searchvehicledetail.workingTime"/>";
							first = false;
						}
						
						var rightPx = 17-hasScrollPx;
						if(strState.length == 2) rightPx = 28-hasScrollPx; 
						strHtml += '<div class="p_time" style="right:'+rightPx+'px;"><p class="t3" >'+strState+'</p></div>';
					}
					
						
					strHtml += '</div>';
					strHtml += '<div class="table1_box2 txt_left">';
					strHtml += '<div>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.safeScore'/> </b>'+calScore(parseFloat(vo.safeScore).toFixed(1),"${_LANG}")+'</span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.fuelEfficiency'/> </b>'+calScore((vo.avgFco/vo.fuelEfficiency*100).toFixed(1),"${_LANG}")+'</span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.ecoScore'/> </b>'+calScore(parseFloat(vo.ecoScore).toFixed(1),"${_LANG}")+'</span>';
					strHtml += '</div>';
					strHtml += '</div>';
					strHtml += '<div class="table1_box2 txt_left">';
					strHtml += '<div>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.drivingTime'/> </b>'+secondToTime(vo.drivingTime,"${_LANG}")+'</span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.meanSpeed'/> </b>'+vo.meanSpeed+' km/h</span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.coolantTemp'/> </b>'+vo.coolantTemp+' ℃</span>';
					strHtml += '</div>';
					strHtml += '<div>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.distance'/> </b>'+minusError(number_format((vo.distance/1000).toFixed(1)))+' km</span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.highSpeed'/> </b>'+vo.highSpeed+' km/h</span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.ecuVolt'/> </b>'+number_format(parseFloat(vo.ecuVolt).toFixed(1))+' v</span>';
					strHtml += '</div>';
					strHtml += '<div>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.avgFco'/> </b>'+number_format(parseFloat(vo.avgFco).toFixed(1))+' km/ℓ</span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.fco'/> </b>'+number_format((vo.fco/1000).toFixed(1))+' ℓ</span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.deviceVolt'/> </b>'+number_format(parseFloat(vo.deviceVolt).toFixed(1))+' v</span>';
					strHtml += '</div>';
					strHtml += '<div>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.co2Emission'/> </b>'+number_format(vo.co2Emission)+' ㎎</span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.fuelCutTime'/> </b>'+number_format(vo.fuelCutTime)+' <fmt:message key='searchvehicledetail.second1'/></span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.idleTime'/> </b>'+number_format(vo.idleTime)+' <fmt:message key='searchvehicledetail.second2'/></span>';
					strHtml += '</div>';
					strHtml += '<div>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.warmupTime'/> </b>'+number_format(vo.warmupTime)+' <fmt:message key='searchvehicledetail.second3'/></span>';
					
					
					/* strHtml += '<span style="width:'+tWidth+'px;"><b style="font-size:smaller;">엔진오일온도 </b>'+parseFloat(vo.engineOilTemp).toFixed(1)+' ℃</span>'; */
					strHtml += '</div>';
					if(vo.tripDetailState == '1'){
						strHtml += '<div>';
						strHtml += '<span style="width:100%"><b style="width:100%;color:red;"><fmt:message key='searchvehicledetail.txt1'/></b></span>';
						strHtml += '</div>';
					}
					if(vo.tripDetailState == '2' || vo.tripDetailState == '3'){
						strHtml += '<div>';
						strHtml += '<span style="width:100%"><b style="width:100%;color:red;"><fmt:message key='searchvehicledetail.txt2'/></b></span>';
						strHtml += '</div>';	
					}
					strHtml += '</div>';
					strHtml += '</td>';
					strHtml += '</tr>';
					
					
					
					/*
					co2배출량 : co2Emission (mg)
					퓨얼컷 : fuelCutTime (초)
					공회전 : idleTime (초)				
					웜업시간 : warmupTime (초)
					엔진오일온도 : engineOilTemp (온도)
					strHtml += '<div>';
					strHtml += '<span style="width:180px;"><b>aa </b>xx</span>';
					strHtml += '<span style="width:180px;"><b>aa </b>xx</span>';
					strHtml += '<span style="width:180px;"><b>aa </b>xx</span>';
					strHtml += '</div>';*/
				}
				return strHtml;

			}
		});
	}

	if(!lazyRoute)
		lazyRoute = $("#contentsTable2").lazyLoader({
			searchFrmObj : $("#searchFrm_route")
			,searchFrmVal : {
				vehicleKey : $("#vehicleKey")
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/driving/getDrivingList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var strHtml = '';
				var data = r.rtvList;
				
				var hasScrollPx = 0;
				if($("#contentsTable2 tbody tr").length+data.length > 2) hasScrollPx = 17;
				
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += '<tr>';
					strHtml += '<td width="80px">';
					strHtml += '<div class="table1_box1" style="padding-bottom:15px;">';
					strHtml += '<img src="${pathCommon}/images/vehicle/'+vo.imgS+'.png" alt="" />';
					strHtml += '</div>';
					strHtml += '<div class="table1_box2">';
					strHtml += '<img src='+userImg(vo.driverImg)+' alt="" />';
					strHtml += '</div>';
					strHtml += '</td>';
					strHtml += '<td width="120px">';
					strHtml += '<div class="table1_box1 txt_left" style="padding-bottom:15px;">';
					strHtml += '<a href="#none">';
					strHtml += '<span>'+vo.modelMaster+'</span>';
					strHtml += ' '+vo.plateNum;
					strHtml += '</a>';
					strHtml += '</div>';
					strHtml += '<div class="table1_box2 txt_left">';
					strHtml += '<a href="#none" class="a_user" data-key="'+vo.accountKey+'">';
					strHtml += '<span>'+convertNullString(vo.driverGroupNm)+'/</span>';
					strHtml += ' '+convertNullString(vo.driverNm);
					strHtml += '</a>';
					strHtml += '<br>';
					strHtml += '<br>';
					if(vo.instantAllocate == '1')
						strHtml += '<a href="#none" class="btn btn_edit" data-key="'+vo.tripKey+'" data-allocateKey="'+vo.allocateKey+'" data-target="2">변경</a>';
					strHtml += '</div>';
					strHtml += '</td>';
					strHtml += '<td colspan="3" class="spantd">';
					strHtml += '<div class="table1_box1 txt_left" style="padding-bottom:15px;">';
					if(convertNullString(vo.startAddr) == ''){
						if(vo.tripDetailState == 1){
							if("${VDAS.lang}" == "ko") vo.startAddr = '주행중';
							else vo.startAddr = 'now Driving';
						}else if(vo.tripDetailState == 2){
							if("${VDAS.lang}" == "ko") vo.startAddr = '주행정보를 생성중입니다.';
							else vo.startAddr = 'creating driving info';
						}else if(vo.tripDetailState == 3){
							if("${VDAS.lang}" == "ko") vo.startAddr = '주행정보를 생성중입니다.';
							else vo.startAddr = 'creating driving info';
						}
					}
					if(convertNullString(vo.endAddr) == ''){
						if(vo.tripDetailState == 1){
							if("${VDAS.lang}" == "ko") vo.endAddr = '주행중';
							else vo.endAddr = 'now Driving';
						}else if(vo.tripDetailState == 2){
							if("${VDAS.lang}" == "ko") vo.endAddr = '주행정보를 생성중입니다.';
							else vo.endAddr = 'creating driving info';
						}else if(vo.tripDetailState == 3){
							if("${VDAS.lang}" == "ko") vo.endAddr = '주행정보를 생성중입니다.';
							else vo.endAddr = 'creating driving info';
						}
					}
					strHtml += '<input type="hidden" class="startInfo" value="'+convertNullString(vo.startDate)+' '+convertNullString(vo.startAddr)+'">';
					strHtml += '<input type="hidden" class="endInfo" value="'+convertNullString(vo.endDate)+' '+convertNullString(vo.endAddr)+'">';
					strHtml += '<p><b><fmt:message key='searchvehicledetail.start2'/> </b><span title="'+convertNullString(vo.startAddr)+'" style="font-weight:500;">'+convertNullString(vo.startDate)+' '+addrString(convertNullString(vo.startAddr))+'</span></p>';
					strHtml += '<p><b><fmt:message key='searchvehicledetail.end2'/> </b><span class="endInfo" title="'+convertNullString(vo.endAddr)+'" style="font-weight:500;">'+convertNullString(vo.endDate)+' '+addrString(convertNullString(vo.endAddr))+'</span></p>';
					
					var gpsRightPx = 140-hasScrollPx;
					strHtml += '<a href="#none" class="btn_gps type02" style="right:'+gpsRightPx+'px;" data-gps="'+vo.tripKey+'" data-vehicle="'+vo.vehicleKey+'" data-account="'+vo.accountKey+'"><img src="${pathCommon}/images/gps.jpg" alt=""></a>';
					/* if((vo.workingAreaWarning == null || vo.workingAreaWarning == '')
							&& (vo.workingTimeWarning == null || vo.workingTimeWarning == ''))
						strHtml += '<div class="p_time"><p>-&nbsp;&nbsp;&nbsp;&nbsp;</p></div>';
					else if((vo.workingAreaWarning != null && vo.workingAreaWarning != '')
							&& (vo.workingTimeWarning == null || vo.workingTimeWarning == ''))
						strHtml += '<div class="p_time"><p class="t3">이수지역</p></div>';
					else if((vo.workingAreaWarning == null || vo.workingAreaWarning == '')
							&& (vo.workingTimeWarning != null && vo.workingTimeWarning != ''))
						strHtml += '<div class="p_time"><p class="t3">업무시간</p></div>';
					else if((vo.workingAreaWarning != null && vo.workingAreaWarning != '')
							&& (vo.workingTimeWarning != null && vo.workingTimeWarning != '')){
						strHtml += '<div class="p_time"><p class="t3">이수지역<br>업무시간</p></div>';
					} */
					
					var stateChk = true;
					
					if((vo.workingAreaWarning == null || vo.workingAreaWarning == '')
							&& (vo.workingTimeWarning == null || vo.workingTimeWarning == '')
							&& (vo.dtcCnt == null || vo.dtcCnt == '0')){

						var px = 45-hasScrollPx;
						strHtml += '<div class="p_time" style="right:'+px+'px;"><p>-</p></div>';
						stateChk = false;
					}
					
					if(stateChk){
						var first = true;
						var strState = "";
						
						if(vo.dtcCnt != null && vo.dtcCnt != '0'){
						//if(true){
							if(!first) strState += '<br>'; 
							strState += "<fmt:message key="searchvehicledetail.trouble2"/>";
							first = false;
						}
						//if(true){
						if(vo.workingAreaWarning != null && vo.workingAreaWarning != ''){
							if(!first) strState += '<br>';
							strState += "<fmt:message key="searchvehicledetail.area2"/>";
							first = false;
						}
						
						if(vo.workingTimeWarning != null && vo.workingTimeWarning != ''){
						//if(true){
							if(!first) strState += '<br>';
							strState += "<fmt:message key="searchvehicledetail.workingTime2"/>";
							first = false;
						}
						
						
						var rightPx = 27-hasScrollPx;
						if(strState.length == 2) rightPx = 37-hasScrollPx; 
						strHtml += '<div class="p_time" style="right:'+rightPx+'px;"><p class="t3" >'+strState+'</p></div>';
					}
						
					
					
					strHtml += '</div>';
					strHtml += '<div class="table1_box2 txt_left">';
					strHtml += '<div>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.safeScore2'/> </b>'+calScore(parseFloat(vo.safeScore).toFixed(1),"${_LANG}")+'</span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.fuelEfficiency2'/> </b>'+calScore((vo.avgFco/vo.fuelEfficiency*100).toFixed(1),"${_LANG}")+'</span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.ecoScore2'/> </b>'+calScore(parseFloat(vo.ecoScore).toFixed(1),"${_LANG}")+'</span>';
					strHtml += '</div>';
					strHtml += '</div>';
					strHtml += '<div class="table1_box2 txt_left">';
					strHtml += '<div>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.drivingTime2'/> </b>'+secondToTime(vo.drivingTime,"${_LANG}")+'</span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.meanSpeed2'/> </b>'+vo.meanSpeed+' km/h</span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.coolantTemp2'/> </b>'+vo.coolantTemp+' ℃</span>';
					strHtml += '</div>';
					strHtml += '<div>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.distance2'/> </b>'+minusError(number_format((vo.distance/1000).toFixed(1)))+' km</span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.highSpeed2'/> </b>'+vo.highSpeed+' km/h</span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.ecuVolt2'/> </b>'+number_format(parseFloat(vo.ecuVolt).toFixed(1))+' v</span>';
					strHtml += '</div>';
					strHtml += '<div>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.avgFco2'/> </b>'+number_format(parseFloat(vo.avgFco).toFixed(1))+' km/ℓ</span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.fco2'/> </b>'+number_format((vo.fco/1000).toFixed(1))+' ℓ</span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.deviceVolt2'/> </b>'+number_format(parseFloat(vo.deviceVolt).toFixed(1))+' v</span>';
					strHtml += '</div>';
					strHtml += '<div>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.co2Emission2'/> </b>'+number_format(vo.co2Emission)+' ㎎</span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.fuelCutTime2'/> </b>'+number_format(vo.fuelCutTime)+' <fmt:message key='searchvehicledetail.second4'/></span>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.idleTime2'/> </b>'+number_format(vo.idleTime)+' <fmt:message key='searchvehicledetail.second5'/></span>';
					strHtml += '</div>';
					strHtml += '<div>';
					strHtml += '<span style="width:'+tWidth+'px;"><b><fmt:message key='searchvehicledetail.warmupTime2'/> </b>'+number_format(vo.warmupTime)+' <fmt:message key='searchvehicledetail.second6'/></span>';
					/* strHtml += '<span style="width:'+tWidth+'px;"><b style="font-size:smaller;">엔진오일온도 </b>'+parseFloat(vo.engineOilTemp).toFixed(1)+' ℃</span>'; */
					strHtml += '</div>';
					if(vo.tripDetailState == '1'){
						strHtml += '<div>';
						strHtml += '<span style="width:100%"><b style="width:100%;color:red;"><fmt:message key='searchvehicledetail.txt1_1'/></b></span>';
						strHtml += '</div>';
					}
					if(vo.tripDetailState == '2' || vo.tripDetailState == '3'){
						strHtml += '<div>';
						strHtml += '<span style="width:100%"><b style="width:100%;color:red;"><fmt:message key='searchvehicledetail.txt2_1'/></b></span>';
						strHtml += '</div>';	
					}
					strHtml += '</div>';
					strHtml += '</td>';
					strHtml += '</tr>';
					
					/*
					co2배출량 : co2Emission (mg)
					퓨얼컷 : fuelCutTime (초)
					공회전 : idleTime (초)				
					웜업시간 : warmupTime (초)
					엔진오일온도 : engineOilTemp (온도)
					strHtml += '<div>';
					strHtml += '<span style="width:180px;"><b>aa </b>xx</span>';
					strHtml += '<span style="width:180px;"><b>aa </b>xx</span>';
					strHtml += '<span style="width:180px;"><b>aa </b>xx</span>';
					strHtml += '</div>';*/
				}
				return strHtml;
			}
		});
	
	if(!lazyClinic)
		lazyClinic = $("#contentsTable3").lazyLoader({
			searchFrmObj : $("#searchFrm_clinic")
			,searchFrmVal : {
				vehicleKey : $("#vehicleKey")
			}
			,scrollRow : 5
			,rowHeight : 40
			,limit : 5
			,loadUrl : "/clinic/getMainteanceList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.result;
				
				var strHtml = '';
				var contents;
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					if(vo.maintenanceContents != null)
						contents = vo.maintenanceContents.split(",",-1);
					else
						contents = "";
					strHtml += '<tr>';
					strHtml += '<th class="car_list2">';
					strHtml += '<p>'+vo.modelMaster+'</p>';
					strHtml += ''+vo.plateNum;
					strHtml += '</th>';
					strHtml += '<th>'+vo.clinicDate+', '+LPAD(vo.clinicH,'0',2)+':'+LPAD(vo.clinicM,'0',2)+'</th>';
					strHtml += '<th>'+contents[0];
					if(contents.length > 1)
						strHtml +=' <fmt:message key='searchvehicledetail.other'/> '+(contents.length - 1)+'<fmt:message key='searchvehicledetail.count'/></th>';
					strHtml += '<td>'+number_format(vo.partsTotalPay)+' <fmt:message key='searchvehicledetail.won'/></td>';
					strHtml += '<td>'+number_format(vo.totalWage)+' <fmt:message key='searchvehicledetail.won2'/></td>';
					if(vo.partsQuality == '0')
						strHtml += '<td>A</td>';
					else if(vo.partsQuality == '1')
						strHtml += '<td>B</td>';
					else if(vo.partsQuality == '2')
						strHtml += '<td>C</td>';
					strHtml += '<td>'+vo.clinicNm+'</td>';
					strHtml += '<td>';
					strHtml += '<a href="#" class="a_detail" data-id="'+vo.id+'"><img src="${pathCommon}/images/btn7.jpg" alt="" /></a>';
					strHtml += '</td>'; 
					strHtml += '</tr>';
				}
				
				return strHtml;
			}
		});
	
	$("body").on("click",".a_user",function(){
		var key = $(this).data("key");
		$VDAS.instance_post("/drivingMng/searchUserDetail.do",{accountKey:key});
	});

	$("#drivingMap").attr("src","${path}/map/trip.do?searchType=default");
	$("#searchDateMap").attr("src","${path}/map/trip.do?searchType=default");
	
	$("body").on("click",".btn_gps",function(){
		
		$VDAS.http_post("/map/matched.do",{tripKey:$(this).data("gps")},{
			
				success : function(r){
				
				},sync:true
			}
		);	
		
		$("#popStart").text($(this).parent("div").find(".startInfo").val());
		$("#popEnd").text($(this).parent("div").find(".endInfo").val());
		
		$("#drivingMap").get(0).contentWindow.searchType = "trip";
		$("#drivingMap").get(0).contentWindow.tripKey = $(this).data("gps");
		
		$(".pop2").fadeIn();
		$(".black").fadeIn();
		
		$("#drivingMap").get(0).contentWindow._o = $("#drivingMap").get(0).contentWindow.createOption();
		$("#drivingMap").get(0).contentWindow.outerReset();
		
		
		//$("#drivingMap").attr("src","${path}/map/trip.do?searchType=trip&tripKey="+$(this).data("gps"));
		
		//var t = 3600*(24*4);
		//var tt = "20170717";
		//$("#drivingMap").attr("src","${path}/map/trip.do?searchType=time&searchTime="+t+"&vehicleKey="+$(this).data("vehicle"));
		//$("#drivingMap").attr("src","${path}/map/trip.do?searchType=time&searchTime="+t+"&accountKey="+$(this).data("account"));
		//$("#drivingMap").attr("src","${path}/map/trip.do?searchType=date&searchDate="+tt+"&vehicleKey="+$(this).data("vehicle"));
		//$("#drivingMap").attr("src","${path}/map/trip.do?searchType=vehicle");
		//$("#drivingMap").attr("src","${path}/map/trip.do?searchType=default");
		
	});
	
	$("#btnSearchVehicle").on("click",function(){
		$("#searchFrm_vehicle input[name=searchMonth]").val(replaceAll($("#searchMonth").val(),"-",""));
		
		$("#myChart01").html('<div class="level"><span id="totalRank"></span> <i class="icon totalIcon"></i></div>');
		drivingSummary();
		$.lazyLoader.search(lazy);
	});
	
	$("body").on("click",".tab_select_btn1 a",function(){
		$(".tab_select_btn1 a").removeClass("on");
		$(this).addClass("on");
	});
	
	$("body").on("click",".btn_edit",function(){
		var key = $(this).data("key");
		var allocateKey = $(this).data("allocatekey");
		var target = $(this).data("target");
		if(confirm("<fmt:message key="searchvehicledetail.txt3"/>\n<fmt:message key="searchvehicledetail.txt3_1"/>")){
			$VDAS.http_post("/driving/pushTripAccount.do",{tripKey:key,allocateKey:allocateKey},{success : function(r){
				alert("<fmt:message key="searchvehicledetail.txt4"/>");
				if(target == 1)
					$("#btnSearchVehicle").trigger("click");
				else mapSearch();
			}});
		}
	});
	
	$("body").on("click",".btn_close",function(){
		SuddenSummaryInit();
	});
	
	$("body").on("click",".btn_apply",function(){
		var itemKey = $(this).data("key");
		var name = $(this).parent("li").find(".name").val();
		var val = $(this).parent("li").find(".val").val();
		var max = $(this).parent("li").find(".max").val();
		
		$VDAS.http_post("/clinic/updateItems.do",{type:"item",sub:"replace",itemKey:itemKey,name:name,val:val,max:max,vehicleKey:$("#vehicleKey").val()},{
			success : function(){
				clinicDetail();
				initPlot();
			}
		});
	});
	
	$("body").on("click",".btn_delete",function(){
		var itemKey = $(this).data("key");
		var name = $(this).parent("li").find(".name").val();
		var val = $(this).parent("li").find(".val").val();
		var max = $(this).parent("li").find(".max").val();
		
		$VDAS.http_post("/clinic/updateItems.do",{type:"item",sub:"delete",itemKey:itemKey,vehicleKey:$("#vehicleKey").val()},{
			success : function(){
				clinicDetail();
				initPlot();
			}
		});
	});
	
	$("body").on("click",".a_detail",function(){
		var id = $(this).data("id");
		$VDAS.instance_post("/clinic/vehicleMaintenanceDetail.do",{id:id});
	});
	
	$("body").on("click",".btn_addItem",function(){
		var name = $(this).parent("li").find(".name").val();
		var val = $(this).parent("li").find(".val").val();
		var max = $(this).parent("li").find(".max").val();
		
		$VDAS.http_post("/clinic/updateItems.do",{type:"item",sub:"add",name:name,val:val,max:max,vehicleKey:$("#vehicleKey").val()},{
			success : function(){
				clinicDetail();
				initPlot();
			}
		});
	});
	
	$("body").on("click",".a_btn6",function(){
		clinicDetail();
	});
	
	$("body").on("click",".btn_accept",function(){
		$(".pop8").fadeOut();
		$(".black").fadeOut();
	});
	
	$("body").on("click",".btn_add",function(){
		strHtml = "";
		
		strHtml += '<li>';
		strHtml += '<span class="s1">';
		strHtml += '<input type="text" class="s4 name" style="width:115px" value="" />';
		strHtml += '</span><span class="s2">';
		strHtml += '<strong class="c1 graph" style="width:0%"></strong>';
		strHtml += '</span><span class="s3">';
		strHtml += '<fmt:message key='searchvehicledetail.remainingDistance'/>';
		strHtml += '</span><input type="text" class="s4 val" value="" /><span class="s5">';
		strHtml += 'Km';
		strHtml += '</span><span class="s6">';
		strHtml += '/ <b><fmt:message key='searchvehicledetail.exchangeCycle'/></b>';
		strHtml += '</span><input type="text" class="s4 max" value=""/><span class="s5">';
		strHtml += 'Km';
		strHtml += '</span><a href="#none" class="btn_addItem"><fmt:message key='searchvehicledetail.apply'/></a>';
		strHtml += '</li>';
		
		$(".pop8_list").append(strHtml);
	});
	
	$("body").on("click",".btn_mapReload",function(){
		if($(".tabItem.on").data("tab") == 'route')
			mapSearch();
	});
	
});

function mapSearch(){
	$.lazyLoader.search(lazyRoute);
	SuddenSummaryInit();

	var mapFrame = $("#searchDateMap").get(0).contentWindow;

	mapFrame.searchType = "date";
	mapFrame.searchDate = $("#searchDateRoute").val();
	mapFrame.vehicleKey = $("#vehicleKey").val();
	
	mapFrame._o = mapFrame.createOption();
	mapFrame.outerReset();
}

function clinicDetail(){
	var vehicleKey = $("#vehicleKey").val();
	
	$VDAS.http_post("/clinic/getItems.do",{vehicleKey:vehicleKey},{
		success : function(r){
			var data = r.result;
			var dataCnt = data.length;
			strHtml = "";			
			$(".pop8_list").empty();			
			for(var i = dataCnt-1 ; i >= 0 ; i--){
				var vo = data[i];
				strHtml += '<li>';
				strHtml += '<span class="s1">';
				strHtml += '<input type="text" class="s4 name" style="width:115px" value="'+vo.name+'" />';
				strHtml += '</span><span class="s2">';
				if((vo.val*100)/vo.max<10)
					strHtml += '<strong class="c1 graph" style="width:'+(vo.val*100)/vo.max+'%"></strong>';
				else
					strHtml += '<strong class="c2 graph" style="width:'+(vo.val*100)/vo.max+'%"></strong>';
				strHtml += '</span><span class="s3">';
				strHtml += '<fmt:message key='searchvehicledetail.remainingDistance2'/>';
				strHtml += '</span><input type="text" class="s4 val" value="'+vo.val+'" /><span class="s5">';
				strHtml += 'Km';
				strHtml += '</span><span class="s6">';
				strHtml += '/ <b><fmt:message key='searchvehicledetail.exchangeCycle2'/></b>';
				strHtml += '</span><input type="text" class="s4 max" value="'+vo.max+'"/><span class="s5">';
				strHtml += 'Km';
				strHtml += '</span><a href="#none" class="btn_apply" data-key="'+vo.itemKey+'"><fmt:message key='searchvehicledetail.apply2'/></a>';
				strHtml += '</span><a href="#none" class="btn_delete" data-key="'+vo.itemKey+'"><fmt:message key='searchvehicledetail.del'/></a>';
				strHtml += '</li>';
			}
			
			$(".pop8_list").append(strHtml);
		}
	});
}

function initPlot(){
	var vehicleKey = $("#vehicleKey").val();

	$VDAS.http_post("/clinic/getItems.do",{vehicleKey:vehicleKey},{
		success : function(r){
			var data = r.result;
			var dataCnt = data.length;
			var $div = $(".res_graph_1");
			$div.html("").css("height",dataCnt*35);
			var plot;
			var	chartTick = [],chartData = [],chartLabel =[],chartColor = [];
			for(var i = dataCnt-1 ; i >= 0 ; i--){
				chartTick.push(data[i].name);
				chartData.push(data[i].val>0?(data[i].val*100)/data[i].max:0);
				chartLabel.push(data[i].name);
				chartColor.push(chartData[dataCnt-1-i]<10?'#e81626':'#92cc70');
			}
			
			plot = $div.jqplot('plot',[chartData],{	    	    
				animate: !$.jqplot.use_excanvas
				,grid:{
					drawGridlines : false
					,drawBorder : false
					,shadow : false
				}
			   	,seriesColors:chartColor
		       	,seriesDefaults:{
					renderer:$.jqplot.BarRenderer
		           	,rendererOptions: {
						animation: {
							speed: 2500
						}
						,varyBarColor: true
						,barDirection: 'horizontal'
					}
				}      
		        ,cursor: {
		            show: false
		            ,zoom: false
		            ,looseZoom: false
		            ,showTooltip: false
		        }
		 		,axes: {
			        yaxis: {
			        	renderer: $.jqplot.CategoryAxisRenderer
			        	,tickRenderer: $.jqplot.CanvasAxisTickRenderer
			            ,ticks: chartTick
			    	}
			        ,xaxis: {                    
			               min:0
			               ,max:100
			        }
			    }
		        ,series:chartLabel
			});
		}
	});
}



function SuddenSummaryInit(){
	$(".rapidStart").text("<fmt:message key="searchvehicledetail.zeroCount1"/>");
	$(".rapidStop").text("<fmt:message key="searchvehicledetail.zeroCount2"/>");
	$(".rapidAccel").text("<fmt:message key="searchvehicledetail.zeroCount3"/>");
	$(".rapidDeaccel").text("<fmt:message key="searchvehicledetail.zeroCount4"/>");
	$(".rapidTurn").text("<fmt:message key="searchvehicledetail.zeroCount5"/>");
	$(".rapidUtern").text("<fmt:message key="searchvehicledetail.zeroCount6"/>");
	$(".overSpeed").text("<fmt:message key="searchvehicledetail.zeroCount7"/>");
	$(".overSpeedLong").text("<fmt:message key="searchvehicledetail.zeroCount8"/>");
}

function outerSuddenSummaryCall(a){
	console.log(a);
	$(".rapidStart").text(a[0]+"<fmt:message key="searchvehicledetail.count1"/>");
	$(".rapidStop").text(a[1]+"<fmt:message key="searchvehicledetail.count2"/>");
	$(".rapidAccel").text(a[2]+"<fmt:message key="searchvehicledetail.count3"/>");
	$(".rapidDeaccel").text(a[3]+"<fmt:message key="searchvehicledetail.count4"/>");
	$(".rapidTurn").text(a[4]+"<fmt:message key="searchvehicledetail.count5"/>");
	$(".rapidUtern").text(a[5]+"<fmt:message key="searchvehicledetail.count6"/>");
	$(".overSpeed").text(a[6]+"<fmt:message key="searchvehicledetail.count7"/>");
	$(".overSpeedLong").text(a[7]+"<fmt:message key="searchvehicledetail.count8"/>");
}


function addrString(a){
	var result = '';

	for(var i=0;i < a.length;i++){
		if(i >= 18){
			result += '...';
			break;
		}
		else
			result += a[i];
	}
		
	return result;
}





function userImg(a){
	if(a==null) return '${pathCommon}/images/user_none.png'
	else return '${pathCommon}/images/'+a+'.png'
}

var initVehicleInfo = function(){
	var vehicleKey = "${vehicleKey}";
	
	$VDAS.http_post("/vehicle/getVehicleInfo.do",{vehicleKey:vehicleKey},{
		success : function(r){
			var obj = r.result;
			var drivingStateHtml ="";
			if(obj.vehicleState == "2")  
				drivingStateHtml += '<span class="ico7">';
			else drivingStateHtml += '<span class="ico'+obj.vehicleState+'">';	
			switch(obj.vehicleState){
				case "1":
					drivingStateHtml += '<fmt:message key='searchvehicledetail.drivingState1'/>';
					break;
				case "2":
					drivingStateHtml += '<fmt:message key='searchvehicledetail.drivingState2'/>';
					break;
				case "3":
					drivingStateHtml += '<fmt:message key='searchvehicledetail.drivingState3'/>';
					break;
				case "4":
					drivingStateHtml += '<fmt:message key='searchvehicledetail.drivingState4'/>';
					break;
			}
			drivingStateHtml += '</span>';
			
			$("#drivingState").after(drivingStateHtml);
			$(".car_detail_left").append('<img src="${pathCommon}/images/vehicle/'+obj.imgL+'.png" alt="" />');
			$("#deviceSn").after(""+convertNullString(obj.deviceSeries)+"/"+convertNullString(obj.deviceSn));
			$("#vehicleNum").after(""+obj.plateNum);
			$("#displacement").after(""+number_format(obj.volume)+" cc");
			$("#gear").after(""+obj.transmission);
			$("#totalDistance").after(""+minusError(number_format((obj.totDistance/1000).toFixed(1)))+" km");
			$("#manufactureNm").after(""+obj.manufacture+"/"+obj.modelHeader);
			$("#modelNm").after(""+obj.modelMaster+"/"+obj.year);
			$("#fuel").after(""+obj.fuelType);
			$("#color").after(convertNullString(obj.color));
			$("#registerDate").after(convertNullString(obj.vehicleRegDate));
			$("#dueDate").after(convertNullString(obj.vehicleMaintenanceEndDate));
			$("#vinNum").after(convertNullString(obj.vehicleBodyNum));
			$("#popVehicle").after(obj.modelMaster+'/'+obj.plateNum);
			
			if(obj.dtcCnt > 0){
				$("#dtcWarning").addClass("warning").html("<b></b><fmt:message key="searchvehicledetail.breakdown"/>").data("key",obj.dtcKey);
				
				$("#searchDtcFrm input[name=dtcKey]").val(obj.dtcKey);
				
				$("#searchDtc").on("click",function(){
					
					if(!lazyDtc) 
						lazyDtc = $("#contentsTableDtc").lazyLoader({
							searchFrmObj : $("#searchDtcFrm")
							,searchFrmVal : {}
							,rowHeight : 80
							,limit : 5
							,loadUrl : "/clinic/getDtcList.do"
							,initSearch : true
							,createDataHtml : function(r){
								var data = r.result;
								
								console.log(data);
								var strHtml = '';
								for(var i = 0 ; i < data.length ; i++){
									var vo = data[i];
									strHtml += '<tr>';
									strHtml += '<td>';
									strHtml += ''+convertNullString(vo.regDate);
									strHtml += '</td>';
									strHtml += '<td>';
									strHtml += ''+convertNullString(vo.dtcCode);
									strHtml += '</td>';
									strHtml += '<td>';
									if(_lang == 'ko'){
										strHtml += ''+convertNullString(vo.typeName);
										strHtml += '</td>';
										strHtml += '<td>';
										strHtml += ''+convertNullString(vo.descript);
									} 
									else {
										strHtml += ''+convertNullString(vo.typeNameEn);
										strHtml += '</td>';
										strHtml += '<td>';
										strHtml += ''+convertNullString(vo.descriptEn);
									}
									strHtml += '</td>';
									strHtml += '</tr>';
								}
								return strHtml;
							}
						});
					
					$(".pop7").fadeIn();
					$(".black").fadeIn();
				});
				
				$("body").on("click",".btn_ok",function(){
					$(".pop7").fadeOut();
					$(".black").fadeOut();
				});
				
			}else{
				$("#dtcWarning").html("<b></b><fmt:message key="searchvehicledetail.normal"/>");
			}
		}
	});
	
	
	
	
}

function drivingSummary(){
	var vehicleKey = $("#vehicleKey").val();
	var tt = ""+replaceAll($("#searchMonth").val(),"-","");
	
	$VDAS.http_post("/driving/getDrivingSummary.do",{vehicleKey : vehicleKey, yyyymm : tt},{
		success:function(r){
			console.log(r);
			var data = r.rtvList;
			var drivingTime = data?data.drivingTime:0;
			var score4 = data?data.score4:0;
			var safeScore = data?data.safeScore:0;
			var ecoScore = data?data.ecoScore:0;
			
			var fuelRank = calRank(score4);
			var safeRank = calRank(safeScore);
			var ecoRank = calRank(ecoScore);
			var totalRank = calRank((score4+safeScore+ecoScore)/3);

			$("#myChart01").drawDoughnutChart([
           	    { title: "<fmt:message key="searchvehicledetail.avgFco3"/>", value : score4,  color: "#a150cb" },
           	    { title: "<fmt:message key="searchvehicledetail.safeScore3"/>", value : safeScore,  color: "#ea2b7e" },
           	    { title: "<fmt:message key="searchvehicledetail.ecoScore3"/>", value:  ecoScore,   color: "#68b2c2" }
           	]);

			//distance m
			//drivingTime sec
			//fco ml
			//oilPrice 원
			
			
			//safe score 안전
			//eco score 에코
			//score4 연비점수
			
			//20점 단위로 abcde
			
			$("#totalRank").text(""+totalRank);
			if(true)
				$(".totalIcon").addClass("up");
			else
				$(".totalIcon").addClass("down");
			$("#fuelRank").text(""+fuelRank);
			$("#safeRank").text(""+safeRank);
			$("#ecoRank").text(""+ecoRank);
			$("#distance").text(""+number_format(parseFloat(data?(data.distance/1000):0).toFixed(1))+" km");
			$("#drivingTime").text(""+parseInt(drivingTime/3600)+'<fmt:message key='searchvehicledetail.time'/> '+LPAD(''+parseInt((drivingTime%3600)/60),'0',2)+'<fmt:message key='searchvehicledetail.minute'/> '+LPAD(''+parseInt((drivingTime%3600)%60),'0',2)+'<fmt:message key='searchvehicledetail.second7'/>');
			$("#fco").text(""+number_format(parseFloat(data?(data.fco/1000):0).toFixed(1))+" ℓ");
			$("#oilPrice").text(""+number_format(data?data.oilPrice:0)+" <fmt:message key="searchvehicledetail.won3"/>");
			
			
			var ex = r.expense;
			if(!ex) ex = {price1 : 0,price2 : 0,price3 : 0,price4 : 0,price5 : 0,price6 : 0,price7 : 0,price8 : 0,priceAll : 0}
			var pex = r.expensePrev;
			if(!pex) pex = {price1 : 0,price2 : 0,price3 : 0,price4 : 0,price5 : 0,price6 : 0,price7 : 0,price8 : 0,priceAll : 0}
			
			var arrStrHtml = {};
			
			for(key in ex){
				if(key != 'priceAll'){
					var strHtml = number_format(eval("ex."+key))+"<fmt:message key="searchvehicledetail.won4"/><span>("+number_format(eval("pex."+key))+"<fmt:message key="searchvehicledetail.won5"/> <i class='";

					if(eval("ex."+key) > eval("pex."+key)) strHtml += "icon up'></i> )</span>";
					else if(eval("ex."+key) < eval("pex."+key)) strHtml += "icon down'></i> )</span>";
					else strHtml += "icon none' style='vertical-align:baseline'>-</i> )</span>";
					
					arrStrHtml[key] = strHtml;
				}
			}
			
			$("#price1").html(arrStrHtml["price1"]);
			$("#price2").html(arrStrHtml["price2"]);
			$("#price3").html(arrStrHtml["price3"]);
			$("#price4").html(arrStrHtml["price4"]);
			$("#price5").html(arrStrHtml["price5"]);
			$("#price6").html(arrStrHtml["price6"]);
			$("#price7").html(arrStrHtml["price7"]);
			$("#price8").html(arrStrHtml["price8"]);
			
			$("#totPrice").html(number_format(ex.priceAll)+"<fmt:message key="searchvehicledetail.won6"/>");
			
		}
	});
	
}

</script>

<input type="hidden" id="vehicleKey" value="${vehicleKey}" />
<form id="searchFrm_vehicle" style="display:none">
	<input type="hidden" name="searchMonth" value="" />
	<input type="hidden" name="vehicleKey" value="" />
	<input type="hidden" name="searchOrder" value="driving" />
</form>
<form id="searchFrm_clinic" style="display:none">
	<input type="hidden" name="vehicleKey" value="" />
</form>
<form id="searchDtcFrm" style="display:none;">
	<input type="hidden" name="dtcKey" value="" />
</form>
<div class="right_layout">
	<div class="detail_view">
		<a href="javascript:window.history.back();" class="btn_close"><img src="${pathCommon}/images/close.jpg" alt="" /></a>
		<h2 class="h2tit">
			<fmt:message key='searchvehicledetail.regVehicleDetail'/>
		</h2>
		<div class="step_box sub_top1" style="text-align:left; padding:7px 12px 6px 21px;">
			<span style="padding-top:5px;" id="drivingState"><fmt:message key='searchvehicledetail.drivingState'/></span>
			<strong id="nowDate" style="padding-top:5px;"></strong>
		</div>
		<div class="car_detail" style="padding-top:17px;">
			<div class="car_detail_left">
			</div>
			<ul class="car_detail_right">
				<li>
					<p><span><fmt:message key='searchvehicledetail.corp'/> </span>${VDAS.corpNm }</p>
					<p><span id="deviceSn"><fmt:message key='searchvehicledetail.deviceSn'/> </span></p>
				</li>
				<li>
					<div>
						<p><span id="vehicleNum"><fmt:message key='searchvehicledetail.vehicleNum'/> </span></p>
						<p><span id="displacement"><fmt:message key='searchvehicledetail.displacement'/> </span></p>
						<p><span id="gear"><fmt:message key='searchvehicledetail.gear'/> </span></p>
						<p><span id="totalDistance"><fmt:message key='searchvehicledetail.totalDistance'/> </span></p>
					</div>
					<div>
						<p><span id="manufactureNm"><fmt:message key='searchvehicledetail.manufactureNm'/> </span></p>
						<p><span id="modelNm"><fmt:message key='searchvehicledetail.modelNm'/> </span></p>
						<p><span id="fuel"><fmt:message key='searchvehicledetail.fuel'/> </span></p>
						<p><span id="color"><fmt:message key='searchvehicledetail.color'/> </span></p>
					</div>
				</li>
				<li class="other">
					<div>
						<p><span id="registerDate"><fmt:message key='searchvehicledetail.registerDate'/> </span></p>
						<p><span id="dueDate"><fmt:message key='searchvehicledetail.dueDate'/> </span></p>
					</div>
					<div>
						<p><span id="vinNum"><fmt:message key='searchvehicledetail.vinNum'/> </span></p>
					</div>
				</li>
			</ul>
		</div>
		<div class="tab_layout">
			<div class="pageTab">
				<ul class="tab_nav">
					<li class="tabItem on" data-tab='driving'><a href="#none" style="padding: 0 45px;"><fmt:message key='searchvehicledetail.txt5'/></a></li>
					<li class='tabItem' data-tab='route'><a href="#none" style="padding: 0 45px;"><fmt:message key='searchvehicledetail.txt6'/></a></li>
					<li class='tabItem' data-tab='clinic'><a href="#none" style="padding: 0 47px 0 46px;"><fmt:message key='searchvehicledetail.txt7'/></a></li>
				</ul>
			</div>
			<div class="div1 driving">
				<div class="detail_box1">
					<h4 class="h4tit"><fmt:message key='searchvehicledetail.txt8'/></h4>
					<div class="tab_select_3">
						<a href="#none" id="searchDtc"><p><strong><b></b><fmt:message key='searchvehicledetail.vehicleState'/></strong><span id="dtcWarning"></span></p></a>
					</div>
				</div>
				<div class="search_box1">
					<input id="searchMonth" type="text" class="date datePickerMonth" />
					<a href="#none" id="btnSearchVehicle" class="btn_search style02"><fmt:message key='searchvehicledetail.search'/></a>
				</div>
				<div class="tbl_layout">
					<div class="row">
						<div class="col" style="width:310px;">
							<div id="myChart01" class="myChart">
								<div class="level"><span id="totalRank"></span> <i class="icon totalIcon"></i></div>
							</div>
							<div class="chart_explain">
								<table class="tbl tbl_explain">
									<colgroup>
										<col style="width:95px;"/>
										<col />
									</colgroup>
									<tr>
										<td><span class="color purple"></span><fmt:message key='searchvehicledetail.fuelRank'/></td>
										<td><span id="fuelRank"></span></td>
									</tr>
									<tr>
										<td><span class="color pink"></span><fmt:message key='searchvehicledetail.safeRank'/></td>
										<td><span id="safeRank"></span></td>
									</tr>
									<tr>
										<td><span class="color blue"></span><fmt:message key='searchvehicledetail.ecoRank'/></td>
										<td><span id="ecoRank"></span></td>
									</tr>
								</table>
							</div>
						</div>
						<div class="col" style="width:430px; float:right;">
							<table class="tbl tbl_type02">
								<colgroup>
									<col style="width:125px;" />
									<col />
								</colgroup>
								<tr>
									<th scope="row"><fmt:message key='searchvehicledetail.distanceS'/></th>
									<td><span id="distance"></span></td>
								</tr>
								<tr>
									<th scope="row"><fmt:message key='searchvehicledetail.time2'/></th>
									<td><span id="drivingTime"></span></td>
								</tr>
								<tr>
									<th scope="row"><fmt:message key='searchvehicledetail.fco3'/></th>
									<td><span id="fco"></span></td>
								</tr>
								<tr>
									<th scope="row"><fmt:message key='searchvehicledetail.oilPrice'/></th>
									<td><span id="oilPrice"></span><br/><fmt:message key='searchvehicledetail.txt9'/></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<div class="count_box">
					<fmt:message key='searchvehicledetail.totPrice'/> <em id="totPrice"></em>
				</div>
				<div class="tbl_layout">
					<div class="row">
						<div class="col col-2">
							<table class="table1 table1_2 tbl_count" style="width:373px; float:left;">
								<colgroup>
									<col style="width:120px;"/>
									<col />
								</colgroup>
								<thead>
									<tr>
										<th scope="col"><fmt:message key='searchvehicledetail.item'/></th>
										<th scope="col"><fmt:message key='searchvehicledetail.totPriceLastMonth'/></th>
									</tr>
								</thead>
								<tr>
									<td><fmt:message key='searchvehicledetail.oiling'/></td>
									<td id="price1"></td>
								</tr>
								<tr>
									<td><fmt:message key='searchvehicledetail.parking'/></td>
									<td id="price3"></td>
								</tr>
								<tr>
									<td><fmt:message key='searchvehicledetail.rentPrice'/></td>
									<td id="price5"></td>
								</tr>
								<tr>
									<td><fmt:message key='searchvehicledetail.penaltyPrice'/></td>
									<td id="price7"></td>
								</tr>
							</table>
						</div>
						<div class="col col-2">
							<table class="table1 table1_2 tbl_count" style="width:373px; float:right;">
								<colgroup>
									<col style="width:120px;"/>
									<col />
								</colgroup>
								<thead>
									<tr>
										<th scope="col"><fmt:message key='searchvehicledetail.item2'/></th>
										<th scope="col"><fmt:message key='searchvehicledetail.totPriceLastMonth2'/></th>
									</tr>
								</thead>
								<tr>
									<td><fmt:message key='searchvehicledetail.repair'/></td>
									<td id="price2"></td>
								</tr>
								<tr>
									<td><fmt:message key='searchvehicledetail.tollfee'/></td>
									<td id="price4"></td>
								</tr>
								<tr>
									<td><fmt:message key='searchvehicledetail.carwash'/></td>
									<td id="price6"></td>
								</tr>
								<tr>
									<td><fmt:message key='searchvehicledetail.etc'/></td>
									<td id="price8"></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<h4 class="h4tit"><fmt:message key='searchvehicledetail.txt10'/></h4>
				<table class="table1 table1_2" id="contentsTable1">
					<colgroup>
						<col style="width:80px;"/>
						<col style="width:120px;"/>
						<col />
						<col style="width:100px;"/>
						<col style="width:80px;"/>
					</colgroup>
					<thead>
						<tr style="display:none;">
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
						</tr>
						<tr>
							<th colspan="2"><fmt:message key='searchvehicledetail.vehicleAndUser'/></th>
							<th><fmt:message key='searchvehicledetail.drivingInfo'/></th>
							<th><fmt:message key='searchvehicledetail.location'/></th>
							<th><fmt:message key='searchvehicledetail.warning'/></th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
			<div class="div1 route" style="display:none;">
				<div class="search_box1 mt40">
					<form id="searchFrm_route">
						<input type="hidden" name="vehicleKey" value="" />
						<input type="hidden" name="searchOrder" value="driving" />
						<input type="text" id="searchDateRoute" name="searchDate" class="date datePicker" style="height:25px; border:1px solid #c0c0c0;"/>
						<a href="#none" class="btn_search style02" id="searchMap"><fmt:message key='searchvehicledetail.search2'/></a>
					</form>
				</div>
				<div class="map_area">
					<div id="map_canvas">
						<iframe id="searchDateMap" src="#none" width="760px" height="339px"></iframe>
					</div>
				</div>

				<div class="map_txt1">
					<div>
						<span class="w109">
							<img src="${pathCommon}/images/m_img1.png" alt=""><fmt:message key='searchvehicledetail.rapidStart'/> <strong class="rapidStart"><fmt:message key='searchvehicledetail.zeroCount9'/></strong>
						</span><span class="w120">
							<img src="${pathCommon}/images/m_img2.png" alt=""><fmt:message key='searchvehicledetail.rapidStop'/> <strong class="rapidStop"><fmt:message key='searchvehicledetail.zeroCount10'/></strong>
						</span>
						<span class="w120">
							<img src="${pathCommon}/images/m_img4.png" alt=""><fmt:message key='searchvehicledetail.rapidAccel'/> <strong class="rapidAccel"><fmt:message key='searchvehicledetail.zeroCount11'/></strong>
						</span>
						<span class="w109">
							<img src="${pathCommon}/images/m_img5.png" alt=""><fmt:message key='searchvehicledetail.rapidDeaccel'/> <strong class="rapidDeaccel"><fmt:message key='searchvehicledetail.zeroCount12'/></strong>
						</span>
						<span class="w120">
							<img src="${pathCommon}/images/m_img10.png" alt=""><fmt:message key='searchvehicledetail.rapidTurn'/> <strong class="rapidTurn"><fmt:message key='searchvehicledetail.zeroCount13'/></strong>
						</span>
					</div>
					<div>
						<span class="w109">
							<img src="${pathCommon}/images/m_img11.png" alt=""><fmt:message key='searchvehicledetail.rapidUtern'/> <strong class="rapidUtern"><fmt:message key='searchvehicledetail.zeroCount14'/></strong>
						</span>
						<span class="w109">
							<img src="${pathCommon}/images/m_img6.png" alt=""><fmt:message key='searchvehicledetail.overSpeed'/> <strong class="overSpeed"><fmt:message key='searchvehicledetail.zeroCount15'/></strong>
						</span>
						<span class="w120">
							<img src="${pathCommon}/images/m_img7.png" alt=""><fmt:message key='searchvehicledetail.overSpeedLong'/> <strong class="overSpeedLong"><fmt:message key='searchvehicledetail.zeroCount16'/></strong>
						</span>
						<%-- <span class="w178">
							<img src="${pathCommon}/images/m_img12.png" alt="">심야운전 <strong class="nightDrive">0회</strong>
						</span> --%>
					</div>
				</div>
				<div class="map_txt2"><fmt:message key='searchvehicledetail.txt11'/></div>
				<h4 class="h4tit"><fmt:message key='searchvehicledetail.txt10_1'/></h4>
				<table class="table1 table1_2" id="contentsTable2">
					<colgroup>
						<col style="width:80px;"/>
						<col style="width:120px;"/>
						<col />
						<col style="width:100px;"/>
						<col style="width:100px;"/>
					</colgroup>
					<thead>
						<tr style="display:none;">
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
						</tr>
						<tr>
							<th colspan="2"><fmt:message key='searchvehicledetail.vehicleAndUser2'/></th>
							<th><fmt:message key='searchvehicledetail.drivingInfo2'/></th>
							<th><fmt:message key='searchvehicledetail.location2'/></th>
							<th><fmt:message key='searchvehicledetail.warning2'/></th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
			<div class="div1 clinic" style="display:none;">
				<div style="display:inline">
					<h4 class="h4tit"><fmt:message key='searchvehicledetail.txt12'/><a href="#none" class="a_btn6" style="position: inherit;float:right;"><img src="${pathCommon}/images/${_LANG}/btn9.jpg" alt="" /></a></h4>
				</div>
				<div class="graph_area">
					<div class="res_graph_1" id="plot" style="width:100%;height:0px"></div>
				</div>
				<div class="detail_box2">
					<fmt:message key='searchvehicledetail.txt12_1'/><span class="i1"><strong></strong><fmt:message key='searchvehicledetail.txt13'/></span><span class="i2"><strong></strong><fmt:message key='searchvehicledetail.txt14'/></span>
				</div>
				<h4 class="h4tit"><fmt:message key='searchvehicledetail.txt15'/></h4>
				<table class="table2 table2_2" id="contentsTable3">
					<colgroup>
						<col style="width:138px;" />
						<col style="width:130px;" />
						<col />
						<col style="width:92px;" />
						<col style="width:92px;" />
						<col style="width:60px;" />
						<col style="width:96px;" />
						<col style="width:50px;" />
					</colgroup>
					<thead>
						<tr>
							<th><fmt:message key='searchvehicledetail.vehicleInfo'/></th>
							<th><fmt:message key='searchvehicledetail.maintenanceDate'/></th>
							<th><fmt:message key='searchvehicledetail.maintenanceItem'/></th>
							<th><fmt:message key='searchvehicledetail.partsAmount'/></th>
							<th><fmt:message key='searchvehicledetail.repairAmount'/></th>
							<th><fmt:message key='searchvehicledetail.rank'/></th>
							<th><fmt:message key='searchvehicledetail.maintenanceShop'/></th>
							<th><fmt:message key='searchvehicledetail.detailDescript'/></th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="black"></div>
<div class="pop2">
	<div class="pop_layout">
		<a href="#none" class="btn_close btn_mapReload"><img src="${pathCommon}/images/close3.jpg" alt="" /></a>
		<h1><fmt:message key='searchvehicledetail.txt16'/></h1>
		<h2 class="title3 pat42">
			<p><b><fmt:message key='searchvehicledetail.start'/> </b><span id="popStart"></span></p>
			<p><b><fmt:message key='searchvehicledetail.end'/> </b><span id="popEnd"></span></p>
		</h2>
		<h3 class="title4" style="padding-bottom:0px;"><b id="popVehicle"><fmt:message key='searchvehicledetail.useVehicle'/> </b></h3>
		<div class="map_area">
			<div id="map_canvas">
				<iframe id="drivingMap" src="#none" width="760px" height="339px"></iframe>
			</div>
			<!-- 
			<div class="map_icon_area">
				<div class="car_start"><img src="${pathCommon}/images/${_LANG}/start.png" alt=""></div>
				<div class="car_icon1"><img src="${pathCommon}/images/m_icon1.png" alt=""></div>
				<div class="car_icon2"><img src="${pathCommon}/images/m_icon1.png" alt=""></div>
				<div class="car_icon3"><img src="${pathCommon}/images/m_icon2.png" alt=""></div>
				<div class="car_icon4 btn_car1">
					<img src="${pathCommon}/images/m_icon3.png" alt="">
					<div class="car_txt_area">
						<div class="car_txt1">
							<h1>급제동</h1>
							<ul>
								<li>
									<b>ㆍ사용자</b>홍길동
								</li>
								<li>
									<b>ㆍ차량</b>SM5/54가5011
								</li>
								<li>
									<b>ㆍ위치</b>서울 구로구 구로동 222-3
								</li>
								<li>
									<b>ㆍ날짜</b>2015.07.11  17:54:35
								</li>
								<li>
									<span>ㆍ제한속도</span>80km/h
								</li>
								<li>
									<span>ㆍ주행속도</span>90km/h
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="car_icon5"><img src="${pathCommon}/images/m_icon1.png" alt=""></div>
				<div class="car_end"><img src="${pathCommon}/images/${_LANG}/end.png" alt=""></div>
			</div>
			 -->
		</div>
		<div class="map_txt1">
			<div>
				<span class="w109">
					<img src="${pathCommon}/images/m_img1.png" alt=""><fmt:message key='searchvehicledetail.rapidStart2'/> <strong class="rapidStart"><fmt:message key='searchvehicledetail.zeroCount17'/></strong>
				</span><span class="w120">
					<img src="${pathCommon}/images/m_img2.png" alt=""><fmt:message key='searchvehicledetail.rapidStop2'/> <strong class="rapidStop"><fmt:message key='searchvehicledetail.zeroCount18'/></strong>
				</span>
				<span class="w120">
					<img src="${pathCommon}/images/m_img4.png" alt=""><fmt:message key='searchvehicledetail.rapidAccel2'/> <strong class="rapidAccel"><fmt:message key='searchvehicledetail.zeroCount19'/></strong>
				</span>
				<span class="w109">
					<img src="${pathCommon}/images/m_img5.png" alt=""><fmt:message key='searchvehicledetail.rapidDeaccel2'/> <strong class="rapidDeaccel"><fmt:message key='searchvehicledetail.zeroCount20'/></strong>
				</span>
				<span class="w120">
					<img src="${pathCommon}/images/m_img10.png" alt=""><fmt:message key='searchvehicledetail.rapidTurn2'/> <strong class="rapidTurn"><fmt:message key='searchvehicledetail.zeroCount21'/></strong>
				</span>
			</div>
			<div>
				<span class="w109">
					<img src="${pathCommon}/images/m_img11.png" alt=""><fmt:message key='searchvehicledetail.rapidUtern2'/> <strong class="rapidUtern"><fmt:message key='searchvehicledetail.zeroCount22'/></strong>
				</span>
				<span class="w109">
					<img src="${pathCommon}/images/m_img6.png" alt=""><fmt:message key='searchvehicledetail.overSpeed2'/> <strong class="overSpeed"><fmt:message key='searchvehicledetail.zeroCount23'/></strong>
				</span>
				<span class="w120">
					<img src="${pathCommon}/images/m_img7.png" alt=""><fmt:message key='searchvehicledetail.overSpeedLong2'/> <strong class="overSpeedLong"><fmt:message key='searchvehicledetail.zeroCount24'/></strong>
				</span>
				<%-- <span class="w178">
					<img src="${pathCommon}/images/m_img12.png" alt="">심야운전 <strong class="nightDrive">0회</strong>
				</span> --%>
			</div>
		</div>
		<div class="map_txt2"><fmt:message key='searchvehicledetail.txt11_2'/></div>
	</div>
</div>
<div class="pop8" style="display:none;">
	<div class="pop_layout">
		<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt="" /></a>
		<h1><fmt:message key='searchvehicledetail.txt12'/></h1>
		<h2><fmt:message key='searchvehicledetail.txt13'/></h2>
		<div class="pop_scroll">
			<ul class="pop8_list">
			</ul>
			</div>
			<div class="btn_type1"><a href="#none" class="btn_accept"><fmt:message key='searchvehicledetail.ok'/></a><a href="#none" class="btn_add"><fmt:message key='searchvehicledetail.add'/></a></div>
		</div>
	</div>
</div>
<div class="pop7" style="display:none;">
	<div class="pop6_area">
		<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt=""></a>
		<h1><fmt:message key='searchvehicledetail.breakdownDesc'/></h1>
		<table class="table1 breakdown" id="contentsTableDtc">
			<colgroup>
				<col style="width:130px;"/>
				<col style="width:60px;"/>
				<col style="width:90px;"/>
				<col />
			</colgroup>
			<thead>
				<tr>
					<th><fmt:message key='searchvehicledetail.checkDate'/></th>
					<th><fmt:message key='searchvehicledetail.breakdownCode'/></th>
					<th><fmt:message key='searchvehicledetail.relatParts'/></th>
					<th><fmt:message key='searchvehicledetail.breakdownContent'/></th>
				</tr>
			</thead>
			<tbody>	
			</tbody>
		</table>
		<br><br>
		<div class="btn_type1"><a href="#none" class="btn_ok"><fmt:message key='searchvehicledetail.ok2'/></a></div>
	</div>
</div>