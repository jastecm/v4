<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<fmt:setLocale value="${_LANG}" scope="request" />
<fmt:setBundle basename="properties.messages.rest.message" />
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<c:if test="${VDAS.rule eq '0' or VDAS.rule eq '1' }">
<script type="text/javascript">
var lazy;
$(document).ready(function(){
	initMenuSel("M1005");

	$(".btn_search").on("click",function(){
		$.lazyLoader.search(lazy);
	});
	
	
	$("#searchText").on("keyup",function(e){
		if(e.keyCode == 13) $(".btn_search").trigger("click");
	});

	$("body").on("click",".btn_del",function(){
		var _this = $(this);
		var key = $(this).data("key");
		
		$VDAS.http_post("/drivingMng/delAlarmList.do",{pushKey:key},{
			success : function(r){
				_this.closest("tr").remove();
				$.lazyLoader.resize(lazy);
			}
		});
	});

	if(!lazy)
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
				searchType : $("#searchMonth")
				,searchText : $("#searchText")
			}
			,setSearchFunc : function(){
				
			}
			,scrollRow : 7
			,rowHeight : 50
			,limit : 10			
			,loadUrl : "/drivingMng/getAlarmList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.rtvList;				
				var strHtml = '';
				
				console.log(data);
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += '<tr>';
					strHtml += '<th class="">';
					strHtml += vo.regDate;
					strHtml += '</th>';
					strHtml += '<th class="">';
					strHtml += vo.pushMsg;
					strHtml += '</th>';
					strHtml += '<th class="">';
					strHtml += '<a href="#none" class="btn_del" data-key="'+vo.pushKey+'"><img src="${pathCommon}/images/${_LANG}/btn_delete.jpg"></a>';					
					strHtml += '</th>';
					strHtml += '</tr>';
					
				}
				
				return strHtml;

			}
		});
});

</script>
<div class="right_layout">
	<h2 class="tit_sub">
		<fmt:message key='alarmlist.txt1'/>
		<p><fmt:message key='alarmlist.txt1_1'/></p>
	</h2>
	<div class="sub_top1">
		<strong id="nowDate"></strong>
	</div>
		<form id="searchFrm" method="post" onSubmit="return false;">
	
	<!-- <div class="search_box1 mab0">
			
			<input id="searchMonth" type="text" class="date datePickerMonth" name="searchMonth" style="height:36px; border:1px solid #c0c0c0;"/>
			<div class="search_box1_1">
				<input type="text" id="searchText" class="search_input" name="searchText"/>
			</div>
			<a href="#none" class="btn_search">조회</a>
	</div> -->
		<div class="search_box1">		
			<input id="searchMonth" type="text" class="date datePickerMonth" name="searchMonth" style="height:36px; border:1px solid #c0c0c0;"/>
			<div class="search_box1_2" style="left:0px">			
				<input type="text" id="searchText" class="search_input" name="searchText"/>
				<a href="#none" class="btn_search" style="width:75px;height:36px;"><fmt:message key='alarmlist.search'/></a>				
			</div>
		</div>
		</form>
	<div class="tab_layout">
		<table class="table1" id="contentsTable" >
			<colgroup>
				<col style="width:150px;" />
				<col style="width:*;" />
				<col style="width:70px;" />
			</colgroup>
			<thead>
				<tr>
					<th><fmt:message key='alarmlist.date1'/></th>
					<th><fmt:message key='alarmlist.contents1'/></th>
					<th><fmt:message key='alarmlist.delete1'/></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
</c:if>
<c:if test="${VDAS.rule ne '0' and VDAS.rule ne '1' }">
<script type="text/javascript">
$(document).ready(function(){
	initMenuSel("M1005");
});
</script>
<div class="right_layout">
	<img src="${pathCommon}/images/${_LANG}/authError.png" alt="" style="width:100%">
</div>
</c:if>