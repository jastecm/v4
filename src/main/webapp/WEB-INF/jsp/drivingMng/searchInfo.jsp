<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fmt:setLocale value="${_LANG}" scope="request" />
<fmt:setBundle basename="properties.messages.rest.message" />

 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<style>.svg-clipped{-webkit-clip-path:url(#svgPath);clip-path:url(#svgPath);}</style>
<svg height="0" width="0">
	<defs>
		<clipPath id="svgPath" clipPathUnits="objectBoundingBox">
  			<circle cx=".5" cy=".5" r=".5" />
   		</clipPath>
	</defs>
</svg>
<script type="text/javascript">
var lazy;
var lazyUser;
var lazyDtc;
var searchKey = 'vehicle';
$(document).ready(function(){
	initMenuSel("M1001");

	$("#locationMap").attr("src","${path}/map/trip.do?searchType=default");	
	
	$(".tabItem").on("click",function(){
		$(".tabItem").removeClass("on");
		$(this).addClass("on");
		
		$(".table1").hide();
		var tab = $(this).data("tab");
		searchKey = tab;
		$("."+tab).show();
		
		if(tab == 'vehicle') $.lazyLoader.search(lazy);
		else $.lazyLoader.search(lazyUser);
				
	});
	
	$(".btn_search").on("click",function(){
		var tab = $(".tabItem.on").data("tab");
		if(tab == 'vehicle') $.lazyLoader.search(lazy);
		else $.lazyLoader.search(lazyUser);
		
	});
	
	
	$("#searchText").on("keyup",function(e){
		if(e.keyCode == 13) $(".btn_search").trigger("click");
	});

	$("#searchdrivingState").on("change",function(e){
		$.lazyLoader.search(lazy);
	});

	$("#searchUserState").on("change",function(e){
		$.lazyLoader.search(lazyUser);
	});
	
	if(!lazy){
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
				searchType : $("#searchType")
				,searchText : $("#searchText")
				,searchOrder : $("#searchOrder")
				,searchdrivingState : $("#searchdrivingState")
				,searchUsed : $("#searchUsed")
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/com/getVehicleList.do"
			,initSearch : false
			,createDataHtml : function(r){
				var data = r.rtvList;				
				var strHtml = '';
				
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += '<tr style="height:97px">';
					strHtml += '<th class="">';					
					strHtml += '<strong class="car_img"><img src="${path}/common/images/vehicle/'+vo.imgS+'.png" alt="" /></strong>';
					strHtml += '<a href="#none" class="btn_vehicleDetail" data-key="'+vo.vehicleKey+'">';
					strHtml += '<span>'+vo.modelMaster+'</span>';
					strHtml += vo.plateNum;
					strHtml += '</a>';
					strHtml += '</th>';
					
					strHtml += '<td ><a href="#none" class="btn_userDetail" data-key="'+vo.accountKey+'"><span>'+vo.lastestGroupNm+'</span>'+vo.lastestUserNm+'</a></td>';
					strHtml += '<td ><div class="start_end txt_left"><p><span><fmt:message key='searchinfo.startEnd'/> </span>'+vo.lastestTripDateStart+'~'+vo.lastestTripDate+'</p><p><span><fmt:message key='searchinfo.distance'/> </span>'+number_format(vo.lastestTripDistance)+' km</p></div></td>';
					
					if(vo.deviceKey){
						if(vo.vehicleState == "2")  
							strHtml += '<td><span class="ico7">';
						else strHtml += '<td><span class="ico'+vo.vehicleState+'">';	
						switch(vo.vehicleState){
							case "1":
								strHtml += '<fmt:message key='searchinfo.driving'/>';
								break;
							case "2":
								strHtml += '<fmt:message key='searchinfo.wait'/>';
								break;
							case "3":
								strHtml += '<fmt:message key='searchinfo.parking'/>';
								break;
							case "4":
								strHtml += '<fmt:message key='searchinfo.notUsed'/>';
								break;
						}
						strHtml += '</span><br/><a href="#none" class="btn_checkLocation" class="btn_check" data-key="'+vo.vehicleKey+'"><fmt:message key='searchinfo.location'/></a></td>';
					}else{
						strHtml += '<td><span class="ico3"><fmt:message key='searchinfo.stop'/></span></td>';
					}
					
					if(vo.dtcCnt > 0)
						strHtml += '<td><a href="#none" class="btn_dtc" data-key="'+vo.dtcKey+'"><div class="status off"></div></a></td>';
					else
						strHtml += '<td><div class="status on"></div></td>';
						
					strHtml += '</tr>';
					
					/*
					1 주행중 - 배차와 상관없이 주행중 (device로부터 10분간 패킷이 없을시 종료 취급)
					2 운행대기 - 배차가 있고 배차 트립이 없고 주행종료
					3 주차중 - 배차가 있고 배차트립이 있고 주행종료
					4 미배차 - 현재 배차없음
					*/
					//<span class="ico1">운행중</span>
					//<span class="ico5">주차중</span>	
					//<span class="ico2">운행중</span>
					//<span class="ico3">운행중</span>
					//<span class="ico4">운행중</span>
					
					//<div class="status off"></div>
					//<div class="status on"></div>
				}
				
				return strHtml;

			}
		});
		
	}
	
	$(".tabItem.on").eq(0).trigger("click");
	
	if(!lazyUser)
		lazyUser = $("#contentsTable2").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
				searchType : $("#searchType")
				,searchText : $("#searchText")
				,searchOrder : $("#searchOrder")
				,searchState : $("#searchUserState")
			}
			,setSearchFunc : function(){
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/com/getUserList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.rtvList;
				var strHtml = '';
				
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					/*
					var overlapChk = data[(i!=0)?i-1:0].accountKey;
					var accountKey = vo.accountKey;
					if(accountKey == overlapChk){
						$(".overlap").after("<p>"+vo.acceptLocation+"</p>")
					}
					else{}
						*/
					strHtml += '<tr>';
					strHtml += '<th class="">';
					if(vo.imgId&&vo.imgId.length > 0)
						strHtml += '<strong class="car_img"><img class="svg-clipped" style="width:49px;height:49px;" src="${defaultPath}/com/getUserImg.do?uuid='+vo.imgId+'" alt="" /></strong>';
					else strHtml += '<strong class="car_img"><img class="svg-clipped" src="${pathCommon}/images/user_img7.jpg" alt="" style="width:49px;height:49px;"/></strong>';
					strHtml += '<a href="#none" class="btn_userDetail" data-key="'+vo.accountKey+'">';
					strHtml += '<span>'+vo.accountNm+'</span>';
					strHtml += vo.name;
					strHtml += '</a>';
					strHtml += '</th>';
					strHtml += '<td><div class="start_end txt_left"><p>'+vo.corpGroupNm+'/'+vo.corpPosition+'</p></div></td>';					
					strHtml += '<td class="posr txt_left"><p class="accept overlap" alt="'+vo.areaCodeNm+'">'+overLength(vo.areaCodeNm)+'</p>';
					//strHtml += '<div class="add_txt"></div>';
					strHtml += '</td>';
					if(vo.accountState == "2")  
						strHtml += '<td><span class="ico7">';
					else strHtml += '<td><span class="ico'+vo.accountState+'">';					
					switch(vo.accountState){
						case "1":
							strHtml += '<fmt:message key='searchinfo.driving2'/>';
							break;
						case "2":
							strHtml += '<fmt:message key='searchinfo.wait2'/>';
							break;
						case "3":
							strHtml += '<fmt:message key='searchinfo.parking2'/>';
							break;
						case "4":
							strHtml += '<fmt:message key='searchinfo.notUsed2'/>';
							break;
					}
					strHtml += '</span><br/><a href="#none" class="btn_checkLocation" data-key="'+vo.accountKey+'"><fmt:message key='searchinfo.location2'/></a></td>';
					strHtml += '</tr>';
				}
				
				return strHtml;
			}
		});
	
	
	$("#searchOrder,#searchUsed").on("change",function(){
		$.lazyLoader.search(lazy);
		$.lazyLoader.search(lazyUser);
	});

	$("body").on("click",".btn_vehicleDetail",function(){
		var key = $(this).data("key");
		$VDAS.instance_post("/drivingMng/searchVehicleDetail.do",{vehicleKey:key});
	});
	
	$("body").on("click",".btn_userDetail",function(){
		var key = $(this).data("key");
		$VDAS.instance_post("/drivingMng/searchUserDetail.do",{accountKey:key});
	});
	
	$("body").on("click",".btn_cancel",function(){
		$(".pop2").fadeOut();
		$(".black").fadeOut();
	});

	<c:if test="${VDAS.rule ne '2'}">
	$("body").on("click",".btn_checkLocation",function(){
		var key = $(this).data("key")
		var t = 3600*(24*4);
		
		$("#locationMap").get(0).contentWindow.searchType = 'vehicle';
		if(searchKey == 'vehicle')	{
			$("#locationMap").get(0).contentWindow.accountKey = null;
			$("#locationMap").get(0).contentWindow.vehicleKey = key;
		}
		else {
			$("#locationMap").get(0).contentWindow.accountKey = key;
			$("#locationMap").get(0).contentWindow.vehicleKey = null;
		}
		
		$(".pop2").fadeIn();
		$(".black").fadeIn();
		
		$("#locationMap").get(0).contentWindow._o = $("#locationMap").get(0).contentWindow.createOption();
		$("#locationMap").get(0).contentWindow.outerReset();
		/*
		$("#locationMap").get(0).contentWindow._o = $("#locationMap").get(0).contentWindow.createOption();
		$("#locationMap").get(0).contentWindow.outerReset();

		var load = $("#locationMap").get(0).contentWindow.getLoadComplate();
		if(load){
			$(".pop2").fadeIn();
			$(".black").fadeIn();
		}
		*/
	});
	</c:if>
	<c:if test="${VDAS.rule eq '2'}">
	$("body").on("click",".btn_checkLocation",function(){
		alert("<fmt:message key='searchinfo.notAuthLocation'/>");
	});
	</c:if>
	
	/*
	$("body").on("click",".btn_accountCkh",function(){
		var key = $(this).data("key");
		console.log(key);
		//var t = 3600*(24*4);
		//$("#locationMap").get(0).contentWindow.searchType = "default";
		//$("#locationMap").get(0).contentWindow.accountKey = key;
		//$("#locationMap").get(0).contentWindow.searchTime = t;
		//
		//$("#locationMap").get(0).contentWindow._o = $("#locationMap").get(0).contentWindow.createOption();
		//$("#locationMap").get(0).contentWindow.outerReset();

		$(".pop2").fadeIn();
		$(".black").fadeIn();
	});*/
	getCnts();
	
	
	
	if(!lazyDtc){
		lazyDtc = $("#contentsTableDtc").lazyLoader({
			searchFrmObj : $("#searchDtcFrm")
			,searchFrmVal : {}
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/clinic/getDtcList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.result;
				
				console.log(data);
				var strHtml = '';
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += '<tr>';
					strHtml += '<td>';
					strHtml += ''+convertNullString(vo.regDate);
					strHtml += '</td>';
					strHtml += '<td>';
					strHtml += ''+convertNullString(vo.dtcCode);
					strHtml += '</td>';
					strHtml += '<td>';
					if(_lang == 'ko'){
						strHtml += ''+convertNullString(vo.typeName);
						strHtml += '</td>';
						strHtml += '<td>';
						strHtml += ''+convertNullString(vo.descript);
					} 
					else {
						strHtml += ''+convertNullString(vo.typeNameEn);
						strHtml += '</td>';
						strHtml += '<td>';
						strHtml += ''+convertNullString(vo.descriptEn);
					}
					
					
					strHtml += '</td>';
					strHtml += '</tr>';
				}
				return strHtml;
			}
		});
	}
	
	$("body").on("click",".btn_dtc",function(){
		$("#searchDtcFrm input[name=dtcKey]").val($(this).data("key"));
		$(".breakdown").show();
		$.lazyLoader.search(lazyDtc);
		
		$(".pop7").fadeIn();
		$(".black").fadeIn();
	});
	
	$("body").on("click",".btn_ok",function(){
		$(".pop7").fadeOut();
		$(".black").fadeOut();
	});
});

function getCnts(){
	$VDAS.http_post("/vehicle/getVehicleCnt.do",{},{
		success : function(r){
			var data = r.result;
			
			$("#titleCnt1").text(data.used+"<fmt:message key='searchinfo.ea'/>");
			$("#titleCnt2").text(data.using+"<fmt:message key='searchinfo.ea'/>");
		}
	});
}

function setAddr(addr,detail){
	$("#returnLocation").val(convertNullString(addr)+" "+convertNullString(detail));
}

function overLength(a){
	var result = '';

	for(var i=0;i < a.length;i++){
		if(i >= 28){
			result += '...';
			break;
		}
		else
			result += a[i];
	}
		
	return result;
}

</script>
<form id="searchFrm" style="display:none">
	<input type="hidden" name="searchType" value="" />
	<input type="hidden" name="searchText" value="" />
	<input type="hidden" name="searchOrder" value="" />
	<input type="hidden" name="searchdrivingState" value="" />
	<input type="hidden" name="searchState" value="" />
	<input type="hidden" name="searchUsed" value="" />
</form>
<form id="searchDtcFrm" style="display:none;">
	<input type="hidden" name="dtcKey" value="" />
</form>
<div class="right_layout">
	<h2 class="tit_sub">
		<fmt:message key='searchinfo.vehicleUser'/>
		<p><fmt:message key='searchinfo.vehicleUserTxt'/> </p>
	</h2>
	<div class="sub_top1">
		<span><fmt:message key='searchinfo.regVehicle'/><b id="titleCnt1"></b></span><span><fmt:message key='searchinfo.usedVehicle'/><b id="titleCnt2"></b></span>
		<strong id="nowDate"></strong>
	</div>
	
	<div class="search_box1">
		<div class="search_box1_1">
			<div class="select_type1">
				<div class="select_type1_1">
					<select class="sel" id="searchType">
						<option value=""><fmt:message key='searchinfo.all'/></option>
						<option value="plateNum"><fmt:message key='searchinfo.plateNum'/></option>
						<option value="modelMaster"><fmt:message key='searchinfo.model'/></option>
						<option value="useNm"><fmt:message key='searchinfo.name'/></option>
						<option value="drivingLocaiton"><fmt:message key='searchinfo.drivingLocation'/></option>
					</select>
				</div>
			</div><input type="text" id="searchText" class="search_input" />
		</div><a href="#none" class="btn_search"><fmt:message key='searchinfo.search'/></a>
	</div>
	<div class="tab_layout">
		<div class="pageTab">
			<ul class="tab_nav">
				<li class="tabItem on" data-tab='vehicle'><a href="#none" ><fmt:message key='searchinfo.vehicle'/></a></li>
				<li class='tabItem' data-tab='user'><a href="#none" ><fmt:message key='searchinfo.user'/></a></li>
			</ul>
		</div>
		
		<div style="margin-bottom: 10px;">
			<select id="searchOrder" class="select form" style="width:105px;">
				<option value="driving"><fmt:message key='searchinfo.orderDrving'/></option>
				<option value="regDate"><fmt:message key='searchinfo.orderReg'/></option>
			</select>
			<select id="searchUsed" class="select form" style="width:105px;">
				<option value="1"><fmt:message key='searchinfo.used'/></option>
				<option value="0"><fmt:message key='searchinfo.stop2'/></option>
			</select>
		</div>		
		<table class="table1 vehicle" id="contentsTable" >
			<colgroup>
				<col style="width:200px;" />
				<col style="width:120px;" />
				<col />
				<col style="width:100px;" />
				<col style="width:100px;" />
			</colgroup>
			<thead>
				<tr>
					<th><fmt:message key='searchinfo.vehicleInfo'/></th>
					<th><fmt:message key='searchinfo.lastestUser'/></th>
					<th><fmt:message key='searchinfo.lastestDrving'/></th>
					<th>
						<select class='th_select' id="searchdrivingState">
							<option value=""><fmt:message key='searchinfo.used2'/></option>
							<option value="1"><fmt:message key='searchinfo.driving3'/></option>
							<option value="2"><fmt:message key='searchinfo.wait3'/></option>
							<option value="3"><fmt:message key='searchinfo.parking3'/></option>
							<option value="4"><fmt:message key='searchinfo.notUsed3'/></option>
						</select>
					</th>
					<th><fmt:message key='searchinfo.vehicleState'/></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		<table class="table1 user" id="contentsTable2" style="display:none">
			<colgroup>
				<col style="width:270px;" />
				<col style="width:120px;" />
				<col />
				<col style="width:100px;" />
			</colgroup>
			<thead>
				<tr>
					<th><fmt:message key='searchinfo.userInfo'/></th>
					<th><fmt:message key='searchinfo.userInfoDetail'/></th>
					<th><fmt:message key='searchinfo.workingArea'/></th>
					<th>
						<select class='th_select' id="searchUserState">
							<option value=""><fmt:message key='searchinfo.used3'/></option>
							<option value="1"><fmt:message key='searchinfo.driving4'/></option>
							<option value="2"><fmt:message key='searchinfo.wait4'/></option>
							<option value="3"><fmt:message key='searchinfo.parking4'/></option>
							<option value="4"><fmt:message key='searchinfo.notUsed4'/></option>
						</select>
					</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		<div class="txt1">
			<h1><fmt:message key='searchinfo.txt1'/></h1>
			<p><b><fmt:message key='searchinfo.txt2_1'/> </b><fmt:message key='searchinfo.txt2_2'/></p>
			<p><b><fmt:message key='searchinfo.txt3_1'/> </b><fmt:message key='searchinfo.txt3_2'/></p>
			<p><b><fmt:message key='searchinfo.txt4_1'/> </b><fmt:message key='searchinfo.txt4_2'/></p>
			<p><b><fmt:message key='searchinfo.txt5_1'/> </b><fmt:message key='searchinfo.txt5_2'/></p>
			<p><b><fmt:message key='searchinfo.txt6_1'/> </b><fmt:message key='searchinfo.txt6_2'/></p>
			<br/><br/>
			<h1><fmt:message key='searchinfo.txt7'/></h1>
			<p><span class="status on"></span><fmt:message key='searchinfo.txt7_1'/></p>
			<p><span class="status off"></span><fmt:message key='searchinfo.txt7_2'/></p>
			<br/><br/>
			<h1><fmt:message key='searchinfo.txt8'/></h1>
			<p><fmt:message key='searchinfo.txt8_1'/><br/><fmt:message key='searchinfo.txt8_2'/></p>
		</div>
	</div>
</div>
<div class="black"></div>
<div class="pop2" style="display:none;">
	<div class="pop_layout">
		<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt="" /></a>
		<h1><fmt:message key='searchinfo.parkingLocationConfrim'/></h1>
		<div class="map_area">
			<div id="map_canvas" style="height:inherit">
				<iframe id="locationMap" src="#none" width="760px" height="400px"></iframe>
			</div>
		</div>
		<div class="map_txt3">
			<span class="tit"><fmt:message key='searchinfo.parkingLocation'/></span>
			<div class="return_box1">
				<input type="text" id="returnLocation" value="" disabled/>
				<div class="map_txt3_1"><fmt:message key='searchinfo.parkingLocationTxt1'/>
				<span><fmt:message key='searchinfo.parkingLocationTxt2'/></span></div>
			</div>
		</div>
		<div style="height:31px"></div>
		<div class="return_btn">
			<a href="#none" class="btn_cancel"><fmt:message key='searchinfo.confirm'/></a>
		</div>
	</div>
</div>
<div class="pop7" style="display:none;">
	<div class="pop6_area">
		<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt=""></a>
		<h1><fmt:message key='searchinfo.dtcHstr'/></h1>
		<table class="table1 breakdown" id="contentsTableDtc">
			<colgroup>
				<col style="width:130px;"/>
				<col style="width:60px;"/>
				<col style="width:90px;"/>
				<col />
			</colgroup>
			<thead>
				<tr>
					<th><fmt:message key='searchinfo.dtcTime'/></th>
					<th><fmt:message key='searchinfo.dtcCode'/></th>
					<th><fmt:message key='searchinfo.dtcPart'/></th>
					<th><fmt:message key='searchinfo.dtcContent'/></th>
				</tr>
			</thead>
			<tbody>	
			</tbody>
		</table>
		<br><br>
		<div class="btn_type1"><a href="#none" class="btn_ok"><fmt:message key='searchinfo.confrim2'/></a></div>
	</div>
</div>