<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>




<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta name="description" content="VIEWCAR - For Your Smart Driving">
<meta property="og:title" content="VIEWCAR">
<meta property="og:description" content="VIEWCAR - For Your Smart Driving">

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/js/jqueryui/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/content.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/content_costom.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/style.css" />
<link rel="manifest" href="${pageContext.request.contextPath}/common/json/manifest.json">
<%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/jsDatePick_ltr.min.css" media="all" /> --%>

<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script src="${pageContext.request.contextPath}/common/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/common.js"></script>
<script src="${pageContext.request.contextPath}/common/js/topup.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jqueryui/jquery-ui.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.stylish-select.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jsDatePick.jquery.min.1.3.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/util.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.form.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.MultiFile.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/message/ko.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/firebasejs/4.1.3/firebase.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/commonDev.js"></script>
<script src="https://api2.sktelecom.com/tmap/js?version=1&format=javascript&appKey=43e32609-b337-47d5-b058-7813f232c2d5"></script>

<script type="text/javascript">



var _objMap = null;
var _o = {};

var _lat = "${lat}";
var _lon = "${lon}";
var _label = "";
var _driving = "";
var _fixed = "${fixed}";

var createOption = function(lon,lat,label,driving){
	_lat = lat;
	_lon = lon;
	_label = label;
	_driving = driving;
	return _o = {lat:_lat,lon:_lon,label:_label,driving:_driving};
}

$(document).ready(function(){
	
	$.tMapLoader = {
		destroy : function(tMapLoaderObj){
			var action = tMapLoaderObj.data('tMapLoaderAction');
			tMapLoaderObj = action.destroy(tMapLoaderObj);
		},
		reset : function(tMapLoaderObj,options){
			var action = tMapLoaderObj.data('tMapLoaderAction');
			tMapLoaderObj = action.reset(tMapLoaderObj,options);
		},move : function(tMapLoaderObj,options){
			var action = tMapLoaderObj.data('tMapLoaderAction');
			tMapLoaderObj = action.move(tMapLoaderObj,options);
		},
		getLoadComplate : function(tMapLoaderObj){
			return tMapLoaderObj.data('loadComplate');
		}
	}
	
	$.fn.tMapLoader = function(options){
		
		return this.each(function(){
			var settings = $.extend({
				div : "map_div"
				,zoom : 14
				,mapW : '100%'
				,mapH : '100%'						
				,maxZoom : 6
				,minZoom : 15
				,devisionVar : 5
				,devisionRtv : 500
				,initCenter : ""
				,tripKey : ""
				,vehicleKey : ""
				,accountKey : ""
				,searchTime : ""
				,searchDate : ""
				,searchType : "${searchType}"
				,objData : {}
				,mapProjection : "EPSG:3857"
				,idx : 1
				,colorCd : ["#ed1c24","#1cb0ed","#2bc981","#b42bd6","#e4a01e"] //빨 파 초 보 노
				,objMap : null
				,alwaysPopup : false
				,move : null
		    }, options );
			
			var pr_3857 = new Tmap.Projection("EPSG:3857");
			 
			//pr_4326 인스탄스 생성.
			var pr_4326 = new Tmap.Projection("EPSG:4326");
			 
			var get3857LonLat = function(coordX, coordY){
			    return new Tmap.LonLat(coordX, coordY).transform(pr_4326, pr_3857);
			}
			       
			var get4326LonLat = function(coordX, coordY){
			    return new Tmap.LonLat(coordX, coordY).transform(pr_3857, pr_4326);
			}
			
			var tMapLoaderAction = {
				destroy : function(objTmap){
					settings.objMap.destroy();
					$("#"+settings.div).html("");
				},reset : function(objTmap,__o){
					tMapLoaderAction.destroy(objTmap);
					return $("#"+settings.div).tMapLoader(__o);
				},move : function(objTmap,__o){
					drawNewMarker(__o.lon,__o.lat,__o.label,__o.driving);
				}
			}
			
			
			
			$(this).data('tMapLoaderAction',tMapLoaderAction);
			
			
			var objMap;
			var objMapMarkerLayer = [];
			var objMapFixedMarkerLayer = [];
			var objMapFixedPoint;
			
			var initMap = function(){
				objMap = new Tmap.Map({div:settings.div
					, width:settings.mapW
					, height:settings.mapH
					, animation:true
				});
			
				settings.objMap = objMap;
				
				if(settings.searchType != 'one')
					$VDAS.http_post("/hyundaimotor/registedGeofenceList?all=${all}",{}
						,{
							sync : true
							,success:function(r){
								objMapFixedPoint = r.result;
								$(this).data('loadComplate',true);
							}
					});
				
				//찍기용
				//objMap.events.register("click", objMap, onClickMap);
				
			}
			
			var onClickMap = function(e){
			    var p = objMap.getLonLatFromPixel(new Tmap.Pixel(e.clientX, e.clientY));
			    
			    p = get4326LonLat(p.lon,p.lat);
			    drawNewMarker(p.lon,p.lat);
			    
			    var lonlat = new Tmap.LonLat(p.lon,p.lat);
			    
			    loadGetAddressFromLonLat(lonlat)
			    
			    
			    
			};
			
			var loadGetAddressFromLonLat = function(point){
					var tdata = new Tmap.TData();
		    	    var optionObj = {version:2,format:"json",coordType:"WGS84GEO",addressType:"A04"};
		    	
			    	tdata.events.register("onComplete", tdata, onCompleteLoadGetAddressFromLonLat);
		    	    tdata.getAddressFromLonLat(point, optionObj);
		    	
		    	}
			var onCompleteLoadGetAddressFromLonLat = function(){
				var addr = $(this.responseXML).find("addressInfo").find("fullAddress").text();
				window.parent&&window.parent.setAddr&&window.parent.setAddr(addr);
			}
			
			var drawNewMarker = function(lon,lat,label,driving){
				_marker&&_markerLayer.removeMarker(_marker);
			    _circleFeature&&_vectorLayer.removeFeatures(_circleFeature);
			    
				var point = get3857LonLat(lon,lat);
				var size = new Tmap.Size(36,50);
				var offset = new Tmap.Pixel(-(size.w/2), -size.h);
				var drivingMode = "";
				
				console.log(driving);
				if(driving == "0") drivingMode = "ico_map_car_on";
				else drivingMode = "ico_map_p_off";
				
				var icon = new Tmap.Icon($VDAS.path+"/common/images/"+drivingMode+".png", size, offset);
				
				var strIconStr = "<div><ul><li style='width:100%'>"+label+"</li></ul></div>";
				var label = new Tmap.Label(strIconStr);
				
				_marker =  new Tmap.Markers(point, icon,label);
				_markerLayer.addMarker(_marker);
				
				
				_marker.events.register("mouseover", _marker.popup, function(o){_marker.popup.show();});
				_marker.events.register("mouseout", _marker.popup, function(o){_marker.popup.hide();});
				
				//찍기용
				/*
				if(settings.searchType != 'one'){
					_circle = new Tmap.Geometry.Circle(point.lon, point.lat, 50, {unit:"m"});
					_circleFeature = new Tmap.Feature.Vector(_circle, null);
					_vectorLayer.addFeatures([_circleFeature]);	
				}
				*/
				
				setCenter();
				
				window.parent&&window.parent.setLonLat&&window.parent.setLonLat(lon,lat);
			}
			
				
			initMap();
			
			//////////////init//////////
			
			if(settings.searchType=='default') return $(this);
			
			
			
			var _markerLayer;
			var _fixedMarkerLayer;
			var _marker;
			var _fixedMarker = [];
			var _circle;
			var _fixedCircle = [];
			var _circleFeature;
			var _fixedCircleFeature = [];
			var _vectorLayer;
			var _fixedVectorLayer;
			
			if(_lat&&_lon){
				var point = get3857LonLat(_lon,_lat);
				var size = new Tmap.Size(36,50);
				var offset = new Tmap.Pixel(-(size.w/2), -size.h);
				var icon = new Tmap.Icon($VDAS.path+"/common/images/icon_mapPin.png", size, offset);
				
				var strIconStr = "<div><ul><li style='width:100%'></li></ul></div>";
				var label = new Tmap.Label(strIconStr);
				_marker =  new Tmap.Markers(point, icon,label);
				
				if(settings.searchType != 'one')
					//_circle = new Tmap.Geometry.Circle(point.lon, point.lat, 1000, {unit:"m"});		
					_circle = new Tmap.Geometry.Circle(point.lon, point.lat, 50, {unit:"m"});		
				
			}else{
				var point = get3857LonLat(127.0,37.0);
				var size = new Tmap.Size(36,50);
				var offset = new Tmap.Pixel(-(size.w/2), -size.h);
				var icon = new Tmap.Icon($VDAS.path+"/common/images/icon_mapPin.png", size, offset);
				_marker =  new Tmap.Markers(point, icon);
				
				if(settings.searchType != 'one')
					_circle = new Tmap.Geometry.Circle(point.lon, point.lat, 50, {unit:"m"});	
			}
			
			if(objMapFixedPoint){
				for(var i = 0 ; i < objMapFixedPoint.length ; i++){
					var fixPoint = objMapFixedPoint[i];
					if(fixPoint.lat != _lat || fixPoint.lon != _lon){
						
						var point = get3857LonLat(fixPoint.lon,fixPoint.lat);
						var size = new Tmap.Size(36,50);
						var offset = new Tmap.Pixel(-(size.w/2), -size.h);
						var icon = new Tmap.Icon($VDAS.path+"/common/images/icon_mapPin.png", size, offset);
						var alies = fixPoint.alies;
						var strIconStr = "<div><ul><li style='width:100%'>"+alies+"</li></ul></div>";
						var label = new Tmap.Label(strIconStr);
						
						_fixedMarker.push(new Tmap.Markers(point, icon,label));
						_fixedCircle.push(new Tmap.Geometry.Circle(point.lon, point.lat, 50, {unit:"m"}));
					}
				}	
			}
			
			var drawMap = function(){
				_vectorLayer = new Tmap.Layer.Vector("l");
				objMap.addLayer(_vectorLayer);
				
				if(_circle){
					_circleFeature = new Tmap.Feature.Vector(_circle, null);
					_vectorLayer.addFeatures([_circleFeature]);	
				}
				
				_fixedVectorLayer = new Tmap.Layer.Vector("l2");
				objMap.addLayer(_fixedVectorLayer);
				
				_markerLayer = new Tmap.Layer.Markers("m");
				objMap.addLayer(_markerLayer);
				
				_fixedMarkerLayer = new Tmap.Layer.Markers("m2");
				objMap.addLayer(_fixedMarkerLayer);
				
				
				if(_marker){
					_markerLayer.addMarker(_marker);
				}
				
				if(_fixedMarker && _fixedCircle){
					
					for(var i = 0 ; i < _fixedMarker.length ; i++){
						var style_red = {
								     fillColor:"#FF0000",
								     fillOpacity:0.2,
								     strokeColor: "#FF0000",
								     strokeWidth: 1,
								     pointRadius: 60
								};
						_fixedCircleFeature.push(new Tmap.Feature.Vector(_fixedCircle[i], null,style_red));
					}
					_fixedVectorLayer.addFeatures(_fixedCircleFeature);
					
					
					$(_fixedMarker).each(function(i){
						const mak = _fixedMarker[i];
						_fixedMarkerLayer.addMarker(mak);
						
						mak.events.register("mouseover", mak.popup, function(o){mak.popup.show();});
						mak.events.register("mouseout", mak.popup, function(o){mak.popup.hide();});						
						
					});
					
				}
				
			}
			
			var setCenter = function(){
				var minLeft;
				var minBot;
				var maxRight;
				var maxTop;
				if(_markerLayer){
					var v = _markerLayer;
					var bounds = v.getDataExtent();
					if(bounds){
						minLeft = bounds.left-100;
						minBot = bounds.bottom-100;
						maxRight = bounds.right+100;
						maxTop = bounds.top+100;
						
						var bounds = new Tmap.Bounds(minLeft, minBot, maxRight, maxTop);
						 
						objMap.zoomToExtent(bounds);
					}
				}
			}
			
			
			//create tmapClassObj
			//console.log($(this).data('loadComplate'),"123123");
			
			drawMap();
			setCenter();
				
		});
		
		
	}
	
	
	_objMap = $("#map_div").tMapLoader(_o);
});

function outerReset(){
	$.tMapLoader.reset(_objMap,_o);
}

function move(lon,lat,label,driving){
	$.tMapLoader.move(_objMap,createOption(lon,lat,label,driving));
}

function getLoadComplate(){
	$.tMapLoader.getLoadComplate(_objMap);
}

</script>

<script type="text/javascript">


	

</script>

    </head>
    <body>
        <div id="map_div">
        </div>        
    </body>
</html>