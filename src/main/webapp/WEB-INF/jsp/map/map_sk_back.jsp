<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>




<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta name="description" content="VIEWCAR - For Your Smart Driving">
<meta property="og:title" content="VIEWCAR">
<meta property="og:description" content="VIEWCAR - For Your Smart Driving">

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/js/jqueryui/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/content.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/content_costom.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/style.css" />
<link rel="manifest" href="${pageContext.request.contextPath}/common/json/manifest.json">
<%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/jsDatePick_ltr.min.css" media="all" /> --%>

<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script src="${pageContext.request.contextPath}/common/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/common.js"></script>
<script src="${pageContext.request.contextPath}/common/js/topup.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jqueryui/jquery-ui.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.stylish-select.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jsDatePick.jquery.min.1.3.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/util.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.form.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.MultiFile.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/message/${_LANG}.js"></script>

<script type="text/javascript" src="https://www.gstatic.com/firebasejs/4.1.3/firebase.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/commonDev.js"></script>
<script src="https://api2.sktelecom.com/tmap/js?version=1&format=javascript&appKey=43e32609-b337-47d5-b058-7813f232c2d5"></script>

<script type="text/javascript">
var tripKey = "${tripKey}";
var searchType = "trip" ;//"${searchType}" //trip,time,date
var vehicleKey = "";
var accountKey = "";
var searchTime = "";
var searchDate = "";

var _o = createOption();

function createOption(){
	if(searchType == 'trip'){
		return {searchType : searchType , tripKey : tripKey};		
	}else if(searchType == 'time'){
		return {searchType : searchType , vehicleKey : vehickeKey , accountKey : accountKey , searchTime : searchTime}
	}else if(searchType == 'date'){
		return {searchType : searchType , vehicleKey : vehickeKey , accountKey : accountKey , searchDate : searchDate}
	}
}

$(document).ready(function(){
	
	
	
	$.fn.tMapLoader = function(options){
		
		return this.each(function(){
			var settings = $.extend({
				div : "map_div"
				,zoom : 14
				,mapW : '100%'
				,mapH : '100%'						
				,maxZoom : 6
				,minZoom : 15
				,devisionVar : 5
				,devisionRtv : 500
				,initCenter : ""
				,tripKey : ""
				,vehicleKey : ""
				,accountKey : ""
				,searchTime : ""
				,searchDate : ""
				,searchType : ""
				,objData : {}
				,mapProjection : "EPSG:3857"
		    }, options );
			
			var pr_3857 = new Tmap.Projection("EPSG:3857");
			 
			//pr_4326 인스탄스 생성.
			var pr_4326 = new Tmap.Projection("EPSG:4326");
			 
			var get3857LonLat = function(coordX, coordY){
			    return new Tmap.LonLat(coordX, coordY).transform(pr_4326, pr_3857);
			}
			       
			var get4326LonLat = function(coordX, coordY){
			    return new Tmap.LonLat(coordX, coordY).transform(pr_3857, pr_4326);
			}
			
			var getTdr = function(tripKey){
				
				var data = null;
				$VDAS.http_post("/map/getTdr.do",_o,{
					sync : true
					,success : function(r){
						data = r.result;
					}
				});
				
				return data;
			}
			
			
			var objMap,objMapMarkerLayer,objMapVectorLayer;
			
			var initMap = function(){
				objMap = new Tmap.Map({div:settings.div
					, width:settings.mapW
					, height:settings.mapH
					, animation:true
				});
			
				objMapMarkerLayer = new Tmap.Layer.Markers( "MarkerLayer" );
				objMapVectorLayer = new Tmap.Layer.Vector("vectorLayerID");  
			}
			
			initMap();
			
			var tClassObj = {};
			tClassObj.vector = null;
			tClassObj.arrPoiMarker = [];
			tClassObj.startMarker = null;
			tClassObj.endMarker = null;
			tClassObj.centerPoint = null;
			tClassObj.mapLev = settings.maxZoom;
			
			var drawMap = function(){
				objMap.setCenter(tClassObj.centerPoint,tClassObj.mapLev);
				
				objMap.addLayer(objMapVectorLayer);
				objMapVectorLayer.addFeatures(tClassObj.vector);
				
				objMap.addLayer(objMapMarkerLayer);
				
				for(var i = 0 ; i < tClassObj.arrPoiMarker.length ; i++){
					const marker = tClassObj.arrPoiMarker[i];
					
					objMapMarkerLayer.addMarker(marker);
					
					marker.events.register("mouseover", marker.popup, function(o){
						//$("div img[id=FramedLabel_FrameDecorationImg_0]").attr("src","${pathCommon}/images/cloud-popup-relative1.png");
						//$("div img[id=FramedLabel_FrameDecorationImg_1]").remove();
						//$("div img[id=FramedLabel_FrameDecorationImg_2]").attr("src","${pathCommon}/images/cloud-popup-relative1.png");
						//$("div img[id=FramedLabel_FrameDecorationImg_3]").remove();
						//$("div img[id=FramedLabel_FrameDecorationImg_4]").remove();
						this.show();
						
						//$("div[id=FramedLabel_FrameDecorationDiv_0]").hide();
					});
					marker.events.register("mouseout", marker.popup, function(o){this.hide();});
				}

				objMapMarkerLayer.addMarker(tClassObj.startMarker);
				objMapMarkerLayer.addMarker(tClassObj.endMarker);
				
			}
			
			if(settings.tripKey){
				settings.objData = getTdr(settings.tripKey);
			}
			
			//create tmapClassObj
			if(settings.objData){
				//rtv
				var geoJason = eval("(" + settings.objData.geoJason + ')');
				var arrPoint = geoJason.features[0].geometry.coordinates; 
				
				var arrTmapPoint = [];
				for(var i = 0 ; i <arrPoint.length ; i++){
					var objLngLat = get3857LonLat(arrPoint[i][0],arrPoint[i][1]);
					var point = new Tmap.Geometry.Point(objLngLat.lon, objLngLat.lat);
					arrTmapPoint.push(point);
				}
				
				
				var startPoint = get3857LonLat(arrPoint[0][0],arrPoint[0][1]);
				var endPoint = get3857LonLat(arrPoint[arrPoint.length-1][0],arrPoint[arrPoint.length-1][1]);
				
				var size = new Tmap.Size(28,37);
				var offset = new Tmap.Pixel(-(size.w/2), -size.h);
				var icon = new Tmap.Icon($VDAS.path+"/common/images/${_LANG}/start.png", size, offset);
				var sMarker = new Tmap.Marker(startPoint, icon);
				var icon = new Tmap.Icon($VDAS.path+"/common/images/${_LANG}/end.png", size, offset);
				var eMarker = new Tmap.Marker(endPoint, icon);
				
				tClassObj.startMarker = sMarker;
				tClassObj.endMarker = eMarker;
				
				var lineString = new Tmap.Geometry.LineString(arrTmapPoint);
				
				var style_red = {
					     fillColor:"#FF0000",
					     fillOpacity:0.2,
					     strokeColor: "#FF0000",
					     strokeWidth: 4,
					     strokeDashstyle: "solid",
					     pointRadius: 60,
					  };
				var mLineFeature = new Tmap.Feature.Vector(lineString, null, style_red);
				
				//create complate vector
				tClassObj.vector = [mLineFeature];
				
				
				//totDistance dist
				//centerPosition [lng,lat]
				var totDistance = settings.objData.totDistance;
				
				if(totDistance/settings.devisionVar < settings.devisionRtv){
					tClassObj.mapLev = 15;
				}else if(totDistance/settings.devisionVar < settings.devisionRtv*2){
					tClassObj.mapLev = 14;				
				}else if(totDistance/settings.devisionVar < settings.devisionRtv*2*2){
					tClassObj.mapLev = 13;				
				}else if(totDistance/settings.devisionVar < settings.devisionRtv*2*2*2){
					tClassObj.mapLev = 12;				
				}else if(totDistance/settings.devisionVar < settings.devisionRtv*2*2*2*2){
					tClassObj.mapLev = 11;
				}else if(totDistance/settings.devisionVar < settings.devisionRtv*2*2*2*2*2){
					tClassObj.mapLev = 10;
				}else if(totDistance/settings.devisionVar < settings.devisionRtv*2*2*2*2*2*2){
					tClassObj.mapLev = 9;
				}else if(totDistance/settings.devisionVar < settings.devisionRtv*2*2*2*2*2*2*2){
					tClassObj.mapLev = 8;
				}else if(totDistance/settings.devisionVar < settings.devisionRtv*2*2*2*2*2*2*2*2){
					tClassObj.mapLev = 7;
				}else{
					tClassObj.mapLev = 6;
				}
				//create complate mapLev
				
				
				var centerPosition = eval(settings.objData.centerPosition);
				
				tClassObj.centerPoint = get3857LonLat(centerPosition[0],centerPosition[1]);
				//create complate centerPoint
				
				
				
				
				
				
				for(var i = 0 ; i <settings.objData.poi.length ; i++){
					var objPoi = settings.objData.poi[i];
					var poiCnt = objPoi.rapidStart+objPoi.rapidStop+objPoi.rapidAccel+objPoi.rapidDeaccel+objPoi.rapidTurn+objPoi.rapidUtern+objPoi.overSpeed+objPoi.overSpeedLong;
					
					var size = new Tmap.Size(24,24);
					var offset = new Tmap.Pixel(-(size.w/2), -size.h);
					var icon = new Tmap.Icon($VDAS.path+"/common/images/m_img3.png", size, offset);
					var strIconStr = "";
					/*
					<div><ul style="width:100%"><li>급출발</li></ul></div>
					*/
					
					strIconStr = "<div style='width:100px'><ul style='width:100%'>"; 
					
					if(poiCnt == 1){
						if(objPoi.rapidStart > 0) {
							//m_img1.png - old
							
							icon = new Tmap.Icon($VDAS.path+"/common/images/m_icon1.png", size, offset);
							strIconStr += "<li style='width:100%'>";
							strIconStr += "<div style='width:20%;display:inline-block;'><img src='"+$VDAS.path+"/common/images/m_icon1.png' /></div>";
							strIconStr += "<div style='width:80%;display:inline-block;'>급출발</div>";
							strIconStr += "</li>";
						}
						else if(objPoi.rapidStop > 0) {
							icon = new Tmap.Icon($VDAS.path+"/common/images/m_icon2.png", size, offset);
							strIconStr += "<li style='width:100%'>";
							strIconStr += "<div style='width:20%;display:inline-block;'><img src='"+$VDAS.path+"/common/images/m_icon2.png' /></div>";
							strIconStr += "<div style='width:80%;display:inline-block;'>급정지</div>";
							strIconStr += "</li>";
						}
						else if(objPoi.rapidAccel > 0) {
							icon = new Tmap.Icon($VDAS.path+"/common/images/m_icon4.png", size, offset);
							strIconStr += "<li style='width:100%'>";
							strIconStr += "<div style='width:20%;display:inline-block;'><img src='"+$VDAS.path+"/common/images/m_icon4.png' /></div>";
							strIconStr += "<div style='width:80%;display:inline-block;'>급가속</div>";
							strIconStr += "</li>";
						}
						else if(objPoi.rapidDeaccel > 0) {
							icon = new Tmap.Icon($VDAS.path+"/common/images/m_icon5.png", size, offset);
							strIconStr += "<li style='width:100%'>";
							strIconStr += "<div style='width:20%;display:inline-block;'><img src='"+$VDAS.path+"/common/images/m_icon5.png' /></div>";
							strIconStr += "<div style='width:80%;display:inline-block;'>급감속</div>";
							strIconStr += "</li>";
						}
						else if(objPoi.rapidTurn > 0) {
							icon = new Tmap.Icon($VDAS.path+"/common/images/m_icon10.png", size, offset);
							strIconStr += "<li style='width:100%'>";
							strIconStr += "<div style='width:20%;display:inline-block;'><img src='"+$VDAS.path+"/common/images/m_icon10.png' /></div>";
							strIconStr += "<div style='width:80%;display:inline-block;'>급회전</div>";
							strIconStr += "</li>";
						}
						else if(objPoi.rapidUtern > 0) {
							icon = new Tmap.Icon($VDAS.path+"/common/images/m_icon11.png", size, offset);
							strIconStr += "<li style='width:100%'>";
							strIconStr += "<div style='width:20%;display:inline-block;'><img src='"+$VDAS.path+"/common/images/m_icon11.png' /></div>";
							strIconStr += "<div style='width:80%;display:inline-block;'>급유턴</div>";
							strIconStr += "</li>";
						}
						/* m_imgX.png
						6 과속
						7 장기과속
						12 심야운전
						*/
						
						
					}else{
						icon = new Tmap.Icon($VDAS.path+"/common/images/m_icon3.png", size, offset);
						strIconStr = "<div style='width:100px'><ul style='width:100%'>"; 
						/*
						<div><ul style='width:100%'>
							<li style='width:25%'><img src='"+$VDAS.path+"/common/images/m_img1.png' /></li>
							<li style='width:50%'>급출발</li>
							<li style='width:25%'>"+objPoi.rapidStart+" 회</li>
						</ul></div>
						*/
						
						if(objPoi.rapidStart > 0) {
							//strIconStr += "<img src='"+$VDAS.path+"/common/images/m_img1.png' />급출발 : "+objPoi.rapidStart+" 회";
							strIconStr += "<li style='width:100%'>";
							strIconStr += "<div style='width:20%;display:inline-block;'><img src='"+$VDAS.path+"/common/images/m_icon1.png' /></div>";
							strIconStr += "<div style='width:50%;display:inline-block;'>급출발</div>";
							strIconStr += "<div style='width:30%;display:inline-block;'>"+objPoi.rapidStart+" 회</div>";
							strIconStr += "</li>";
						}
						if(objPoi.rapidStop > 0) {
							//if(strIconStr.length != 0) strIconStr += "</br>";
							//strIconStr += "<img src='"+$VDAS.path+"/common/images/m_img2.png' />급정지 : "+objPoi.rapidStop+" 회";
							strIconStr += "<li style='width:100%'>";
							strIconStr += "<div style='width:20%;display:inline-block;'><img src='"+$VDAS.path+"/common/images/m_icon2.png' /></div>";
							strIconStr += "<div style='width:50%;display:inline-block;'>급정지</div>";
							strIconStr += "<div style='width:30%;display:inline-block;'>"+objPoi.rapidStop+" 회</div>";
							strIconStr += "</li>";
						}
						if(objPoi.rapidAccel > 0) {
							//if(strIconStr.length != 0) strIconStr += "</br>";
							//strIconStr += "<img src='"+$VDAS.path+"/common/images/m_img4.png' />급가속 : "+objPoi.rapidAccel+" 회";
							strIconStr += "<li style='width:100%'>";
							strIconStr += "<div style='width:20%;display:inline-block;'><img src='"+$VDAS.path+"/common/images/m_icon4.png' /></div>";
							strIconStr += "<div style='width:50%;display:inline-block;'>급가속</div>";
							strIconStr += "<div style='width:30%;display:inline-block;'>"+objPoi.rapidAccel+" 회</div>";
							strIconStr += "</li>";
						}
						if(objPoi.rapidDeaccel > 0) {
							//if(strIconStr.length != 0) strIconStr += "</br>";
							//strIconStr += "<img src='"+$VDAS.path+"/common/images/m_img5.png' />급감속 : "+objPoi.rapidDeaccel+" 회";
							strIconStr += "<li style='width:100%'>";
							strIconStr += "<div style='width:20%;display:inline-block;'><img src='"+$VDAS.path+"/common/images/m_icon5.png' /></div>";
							strIconStr += "<div style='width:50%;display:inline-block;'>급감속</div>";
							strIconStr += "<div style='width:30%;display:inline-block;'>"+objPoi.rapidDeaccel+" 회</div>";
							strIconStr += "</li>";
						}
						if(objPoi.rapidTurn > 0) {
							//if(strIconStr.length != 0) strIconStr += "</br>";
							//strIconStr += "<img src='"+$VDAS.path+"/common/images/m_img10.png' />급회전 : "+objPoi.rapidTurn+" 회";
							strIconStr += "<li style='width:100%'>";
							strIconStr += "<div style='width:20%;display:inline-block;'><img src='"+$VDAS.path+"/common/images/m_icon10.png' /></div>";
							strIconStr += "<div style='width:50%;display:inline-block;'>급회전</div>";
							strIconStr += "<div style='width:30%;display:inline-block;'>"+objPoi.rapidTurn+" 회</div>";
							strIconStr += "</li>";
						}
						if(objPoi.rapidUtern > 0) {
							//if(strIconStr.length != 0) strIconStr += "</br>";
							//strIconStr += "<img src='"+$VDAS.path+"/common/images/m_img11.png' />급유턴 : "+objPoi.rapidUtern+" 회";
							strIconStr += "<li style='width:100%'>";
							strIconStr += "<div style='width:20%;display:inline-block;'><img src='"+$VDAS.path+"/common/images/m_icon11.png' /></div>";
							strIconStr += "<div style='width:50%;display:inline-block;'>급유턴</div>";
							strIconStr += "<div style='width:30%;display:inline-block;'>"+objPoi.rapidUtern+" 회</div>";
							strIconStr += "</li>";
						}
						strIconStr += "</ul></div>"; 
						
						
					}
					
					var label = new Tmap.Label(strIconStr);
					var point = get3857LonLat(objPoi.lon,objPoi.lat);
					var markers = new Tmap.Markers(point, icon, label);	
					
					tClassObj.arrPoiMarker.push(markers);
					//create complate poiMarkers
				}
				
				//draw
				drawMap();
				
			}
			
		});
	}
	
	$("#map_div").tMapLoader(_o);
});

</script>

<script type="text/javascript">


	

</script>

    </head>
    <body>
        <div id="map_div">
        </div>        
    </body>
</html>