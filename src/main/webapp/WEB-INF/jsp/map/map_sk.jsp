<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<html lang="ko">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="title" content="차량운행 관리 서비스 VIEW CAR" />
    <meta name="description" content="" />

<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/earlyaccess/notosanskr.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/common/new/css/jquery-ui.min.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/common/new/css/style.css" />

<%@include file="/WEB-INF/jsp/common/common.jsp"%>
	

	<script src="${pageContext.request.contextPath}/common/new/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/common/new/js/jquery-ui.min.js"></script>
    <script src="${pageContext.request.contextPath}/common/new/js/jquery-debounce.js"></script>
    <script src="${pageContext.request.contextPath}/common/new/js/base64.js"></script>
    <script src="${pageContext.request.contextPath}/common/new/js/chart.js"></script>
    <script src="${pageContext.request.contextPath}/common/new/js/common.js"></script>
    
    
    <script src="${pageContext.request.contextPath}/common/js/xlsx/xlsx.full.min.js" type="text/javascript"></script>
    
    <script src="${pageContext.request.contextPath}/common/js/util.js"></script>
    <script src="${pageContext.request.contextPath}/common/new/js/v4.js"></script>
    <script src="${pageContext.request.contextPath}/common/new/js/commonV4.js"></script>
        
	<script src="https://api2.sktelecom.com/tmap/js?version=1&format=javascript&appKey=43e32609-b337-47d5-b058-7813f232c2d5"></script>
             

<script type="text/javascript">
var searchType = "${searchType}" ;//"${searchType}" //trip,time,date,vehicle,default
var tripKey = "${tripKey}"; //used trip
var vehicleKeys = "${vehicleKeys}"; //used vehicle
var searchDriverKey = "${searchDriverKey}"; //used vehicle
var searchDrivingState = "${searchDrivingState}" // 1주행중 : 0주차중 //used vehicle
var searchAllocateSate = "${searchAllocateSate}";  //1배차중 : 0미배차 //used vehicle
var searchVehicleGroup = "${searchVehicleGroup}";  //차량 그룹 //used vehicle
var searchDriverGroup = "${searchDriverGroup}";  //사용자 그룹 //used vehicle
var searchAreaLev1 = "${searchAreaLev1}"; //used vehicle
var searchAreaLev2 = "${searchAreaLev2}"; //used vehicle
var lastestTime = "${lastestTime}"; //used vehicle
var alwaysPopup = "${alwaysPopup}"; //used vehicle

var vehicleKey = "${vehicleKey}"; //used trip,date,time
var accountKey = "${accountKey}"; //used trip,date,time
var searchTime = "${searchTime}"; //searchTime unit = sec //used time
var searchDate = "${searchDate}"; //searchDate unit = "YYYYMMDD" //used date



var _time = null;

var _objMap = null;

var _o = createOption();

var _historyBackOption = null;

var loadComplate = true;

function createOption(){
	if(searchType == 'trip'){
		_historyBackOption = null;
		return {searchType : searchType , vehicleKey : vehicleKey , tripKey : tripKey};		
	}else if(searchType == 'time'){
		_historyBackOption = null;
		return {searchType : searchType , vehicleKey : vehicleKey , accountKey : accountKey , searchTime : searchTime}
	}else if(searchType == 'date'){
		_historyBackOption = null;
		return {searchType : searchType , vehicleKey : vehicleKey , accountKey : accountKey , searchDate : searchDate}
	}else if(searchType == 'vehicle'){		
		_historyBackOption = null;
		return {searchType : searchType  , vehicleKeys : vehicleKeys 
			, searchDriverKey : searchDriverKey 
			, searchDrivingState : searchDrivingState, searchAllocateSate : searchAllocateSate
			, searchVehicleGroup : searchVehicleGroup , searchDriverGroup : searchDriverGroup 
			, searchAreaLev1 : searchAreaLev1 , searchAreaLev2 : searchAreaLev2
			, alwaysPopup : alwaysPopup
			, lastestTime : lastestTime};
	}else if(searchType == 'default'){		
		_historyBackOption = null;
		return {searchType : searchType};
	}
	
}

$(document).ready(function(){
	
	$.tMapLoader = {
			destroy : function(tMapLoaderObj){
				var action = tMapLoaderObj.data('tMapLoaderAction');
				tMapLoaderObj = action.destroy(tMapLoaderObj);
			},
			reset : function(tMapLoaderObj,options){
				var action = tMapLoaderObj.data('tMapLoaderAction');
				tMapLoaderObj = action.reset(tMapLoaderObj,options);
			}
		}
	
	$.fn.tMapLoader = function(options){
		
		return this.each(function(){
			var settings = $.extend({
				div : "map_div"
				,zoom : 14
				,mapW : '100%'
				,mapH : '100%'						
				,maxZoom : 6
				,minZoom : 15
				,devisionVar : 5
				,devisionRtv : 500
				,initCenter : ""
				,tripKey : ""
				,vehicleKey : ""
				,accountKey : ""
				,searchTime : ""
				,searchDate : ""
				,searchType : ""
				,objData : {}
				,mapProjection : "EPSG:3857"
				,idx : 1
				,colorCd : ["#ed1c24","#1cb0ed","#2bc981","#b42bd6","#e4a01e"] //빨 파 초 보 노
				,objMap : null
				,alwaysPopup : false
				,debugMode : false
				,gpsAccuracy : 1000
		    }, options );
			
			var pr_3857 = new Tmap.Projection("EPSG:3857");
			 
			//pr_4326 인스탄스 생성.
			var pr_4326 = new Tmap.Projection("EPSG:4326");
			 
			var get3857LonLat = function(coordX, coordY){
			    return new Tmap.LonLat(coordX, coordY).transform(pr_4326, pr_3857);
			}
			       
			var get4326LonLat = function(coordX, coordY){
			    return new Tmap.LonLat(coordX, coordY).transform(pr_3857, pr_4326);
			}
			
			var tMapLoaderAction = {
				destroy : function(objTmap){
					settings.objMap.destroy();
					$("#"+settings.div).html("");
				},reset : function(objTmap,__o){
					tMapLoaderAction.destroy(objTmap);
					return $("#"+settings.div).tMapLoader(__o);
				}
			}
			
			
			
			$(this).data('tMapLoaderAction',tMapLoaderAction);
			
			
			var objMap;
			var objMapMarkerLayer = [];
			var objMapVectorLayer = [];
			
			var outerSuddenList = [0,0,0,0,0,0,0,0];
			
			var initMap = function(){
				objMap = new Tmap.Map({div:settings.div
					, width:settings.mapW
					, height:settings.mapH
					, animation:true
				});
			
				settings.objMap = objMap;
				
				if(_historyBackOption)
					
					objMap.events.register("click", objMap, function(o){
						
						if(confirm("뒤로 돌아가시겟습니까?")){
							searchType = _historyBackOption.searchType;
							vehicleKeys = _historyBackOption.vehicleKeys;
							searchDriverKey = _historyBackOption.searchDriverKey;
							searchDrivingState = _historyBackOption.searchDrivingState;
							searchAllocateSate = _historyBackOption.searchAllocateSate;
							searchVehicleGroup = _historyBackOption.searchVehicleGroup;
							searchDriverGroup = _historyBackOption.searchDriverGroup;
							searchAreaLev1 = _historyBackOption.searchAreaLev1;
							searchAreaLev2 = _historyBackOption.searchAreaLev2;
							alwaysPopup = _historyBackOption.alwaysPopup;
							lastestTime = _historyBackOption.lastestTime;
							
							_o = createOption();
							
							outerReset();	
						}
						
					});
			}
			
			var getTdr = function(){
				var data = null;
				
				if(settings.searchType=='vehicle'){
					$V4.http_post("/api/v1/vehicle/location",_o,{
						sync : true
						,requestMethod:"GET"
						,header:{key:"${_KEY}"}
						,success : function(r){
							data = r.result;
							if(data == null || data.length == 0) {
								alert("주행내역이 없습니다.");
								
								loadComplate = false;
							}else{
								loadComplate = true;
							}
						}
					});
					
				}else{
					var url = "";
					if(settings.searchType == 'trip'){
						url = "/api/v1/vehicle/"+settings.vehicleKey+"/trackRecord/"+settings.tripKey;
					}else if(settings.searchType == 'date'){
						url = "/api/v1/vehicle/"+settings.vehicleKey+"/trackRecord/date/"+settings.searchDate;
					}else if(settings.searchType == 'time'){
						url = "/api/v1/vehicle/"+settings.vehicleKey+"/trackRecord/recentTime/"+settings.searchTime;
					}
					
					if(url){
						$V4.http_post(url,{gpsAccuracy : settings.gpsAccuracy},{
							sync : true
							,requestMethod:"GET"
							,header:{key:"${_KEY}"}
							,success : function(r){
								data = r.result;
								if(data == null || data.length == 0) {
									data = [];
									alert("주행내역이 없습니다.");
									loadComplate = false;
								}else{
									if(settings.searchType == 'trip') data = [data];
									loadComplate = true;	
								}
								
							},error :function(t){
								
								t.responseJSON?alert(t.status+"\n"+t.responseJSON.result):null==_o.error&&alert(t.status+"\n"+t.statusText);
								loadComplate = false;
								
								if(_historyBackOption){
									searchType = _historyBackOption.searchType;
									vehicleKey = _historyBackOption.vehicleKey;
									accountKey = _historyBackOption.accountKey;
									vehicleState = _historyBackOption.vehicleState;
									allocateState = _historyBackOption.allocateState;
									vehicleGroup = _historyBackOption.vehicleGroup;
									accountGroup = _historyBackOption.accountGroup;
									searchArealev1 = _historyBackOption.searchArealev1;
									searchArealev2 = _historyBackOption.searchArealev2;
									alwaysPopup = _historyBackOption.alwaysPopup;
									lastestTime = _historyBackOption.lastestTime;
									
									_o = createOption();
									
									outerReset();
								}
							}
						});	
					}else{
						data = [];
					}
					
					
					
				}
				
				
				
				return data;
			}
			
			var createGeoData = function(){
				if(settings.objData){
										
					tClassObj.vector = [];
					tClassObj.startMarker = [];
					tClassObj.endMarker = [];
					
					for(var z = 0 ; z < settings.objData.length ; z++){
						
						
						var d = settings.objData[z];
						//settings.searchType trip = obj , else = list[obj]
						
						
						if(settings.searchType == 'vehicle'){
							var vo = d;
							var size = new Tmap.Size(25,35);
							var offset = new Tmap.Pixel(-(size.w/2), -size.h);
							var icoNm = $V4.path+"/common/images/";
							if(vo.drivingState == '1') icoNm += "ico_map_car_";
							else icoNm += "ico_map_p_";
							if(vo.allocateState == '1') icoNm += "on.png";
							else icoNm += "off.png";
							
							var icon = new Tmap.Icon(icoNm, size, offset);
							
							var driverNm = getProperty(vo,"driver.name");
							var strIconStr = vo.plateNum+((driverNm)?"/"+driverNm:"")+"<input type='hidden' data-vehiclekey = '"+vo.vehicleKey+"' class='data'/>";
														
							var label = new Tmap.Label(strIconStr);
							var location = getProperty(vo,"latestLocation.location");
							var point = get3857LonLat(location.split(",")[0],location.split(",")[1]);
							var markers = new Tmap.Markers(point, icon, label);	
							
							tClassObj.arrPoiMarker.push(markers);	
							
							
							window.parent&&window.parent.setAddr&&window.parent.setAddr(getProperty(vo,'latestLocation.locationAddr'),getProperty(vo,'latestLocation.locationAddrDetail'));
							
								
						}else{
							var vo = d;
							
							var arrTrackRecord = [];
							
							if(vo.tripMatched == '1') arrTrackRecord = vo.matchedTrackRecord;
							else arrTrackRecord = vo.trackRecord;
							
							var arrTmapPoint = [];
							var tempStartPoiList = [];
							
							for(var i = 0 ; i <arrTrackRecord.length ; i++){
								if(!arrTrackRecord[i].gpsAccuracyOut){ //이상좌표 아닌것만 TODO 검증테스트 필요
									var objLngLat = get3857LonLat(arrTrackRecord[i].lon,arrTrackRecord[i].lat);
									var point = new Tmap.Geometry.Point(objLngLat.lon, objLngLat.lat);
									arrTmapPoint.push(point);
									
									
									if(settings.debugMode){
										var size = new Tmap.Size(28,37);
										var offset = new Tmap.Pixel(-(size.w/2), -size.h);
										var idx = i+1;
										if(idx > 50) idx = idx%50;
										if(idx == 0 ) idx = idx+1;
										var icon = new Tmap.Icon($V4.path+"/common/images/m_number_"+((idx))+".png", size, offset);
										var sMarker = new Tmap.Marker(objLngLat, icon);
										tClassObj.startMarker.push(sMarker);
									}
									
								}
							} 
							
							var startPoint = get3857LonLat(vo.startLon,vo.startLat);
							arrTmapPoint.unshift(new Tmap.Geometry.Point(startPoint.lon, startPoint.lat));
							var endPoint = get3857LonLat(vo.endLon,vo.endLat);
							arrTmapPoint.push(new Tmap.Geometry.Point(endPoint.lon, endPoint.lat));
							
							var size = new Tmap.Size(28,37);
							var offset = new Tmap.Pixel(-(size.w/2), -size.h);
							if(settings.searchType=='trip'){
								var icon = new Tmap.Icon($V4.path+"/common/images/${_LANG}/start.png", size, offset);
								var sMarker = new Tmap.Marker(startPoint, icon);
								var iconE = new Tmap.Icon($V4.path+"/common/images/${_LANG}/end.png", size, offset);
								var eMarker = new Tmap.Marker(endPoint, iconE);
								
								tClassObj.startMarker.push(sMarker);
								tClassObj.endMarker.push(eMarker);
							}else{
								var icon = new Tmap.Icon($V4.path+"/common/images/m_number_"+((settings.idx))+".png", size, offset);
								var sMarker = new Tmap.Marker(startPoint, icon);
								var iconE = new Tmap.Icon($V4.path+"/common/images/m_number_"+((settings.idx))+".png", size, offset);
								var eMarker = new Tmap.Marker(endPoint, iconE);
								
								tClassObj.startMarker.push(sMarker);
								tClassObj.endMarker.push(eMarker);
							}
							
							var lineString = new Tmap.Geometry.LineString(arrTmapPoint);
							
							var style_red = {
								     fillColor:settings.colorCd[z%(settings.colorCd.length)],
								     fillOpacity:0.2,
								     strokeColor: settings.colorCd[z%(settings.colorCd.length)],
								     strokeWidth: 4,
								     strokeDashstyle: "solid",
								     pointRadius: 60,
								  };
							var mLineFeature = new Tmap.Feature.Vector(lineString, null, style_red);
							
							//create complate vector
							tClassObj.vector.push(mLineFeature);
							settings.idx++;
						}
					}
				}
			}
			
			var createPoiData = function(){
				tClassObj.arrPoiMarker = [];
				for(var z = 0 ; z < settings.objData.length ; z++){
					var vo = settings.objData[z];
					
					var arrTrackRecord = [];
					
					if(vo.tripMatched == '1') arrTrackRecord = vo.matchedTrackRecord;
					else arrTrackRecord = vo.trackRecord;
					
					for(var i = 0 ; i < arrTrackRecord.length ; i++){
						var rapidStart = getProperty(arrTrackRecord[i],"rapidStart");
						var rapidStop = getProperty(arrTrackRecord[i],"rapidStop");
						var rapidAccel = getProperty(arrTrackRecord[i],"rapidAccel");
						var rapidDeaccel = getProperty(arrTrackRecord[i],"rapidDeaccel");
						var rapidTurn = getProperty(arrTrackRecord[i],"rapidTurn");
						var rapidUtern = getProperty(arrTrackRecord[i],"rapidUtern");
						var overSpeed = getProperty(arrTrackRecord[i],"overSpeed");
						var overSpeedLong = getProperty(arrTrackRecord[i],"overSpeedLong");
						var poiCnt = rapidStart+rapidStop+rapidAccel+rapidDeaccel+rapidTurn+rapidUtern+overSpeed+overSpeedLong;
						
						if(poiCnt > 0){
							var size = new Tmap.Size(24,24);
							var offset = new Tmap.Pixel(-(size.w/2), -size.h);
							var icon = new Tmap.Icon($V4.path+"/common/images/m_img3.png", size, offset);
							var strIconStr = "";
							
							strIconStr = "<div style='width:100px'><ul style='width:100%'>";
							
							if(poiCnt == 1){
								strIconStr += "<li style='width:100%'>";
								if(rapidStart > 0) {
									outerSuddenList[0] += rapidStart;
									icon = new Tmap.Icon($V4.path+"/common/images/m_icon1.png", size, offset);
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon1.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>급출발</div>";
								}
								else if(rapidStop > 0) {
									outerSuddenList[1] += rapidStop;
									icon = new Tmap.Icon($V4.path+"/common/images/m_icon2.png", size, offset);
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon2.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>급정지</div>";
								}
								else if(rapidAccel > 0) {
									outerSuddenList[2] += rapidAccel;
									icon = new Tmap.Icon($V4.path+"/common/images/m_icon4.png", size, offset);
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon4.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>급가속</div>";
								}
								else if(rapidDeaccel > 0) {
									outerSuddenList[3] += rapidDeaccel;
									icon = new Tmap.Icon($V4.path+"/common/images/m_icon5.png", size, offset);
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon5.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>급감속</div>";
								}
								else if(rapidTurn > 0) {
									outerSuddenList[4] += rapidTurn;
									icon = new Tmap.Icon($V4.path+"/common/images/m_icon10.png", size, offset);
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon10.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>급회전</div>";
								}
								else if(rapidUtern > 0) {
									outerSuddenList[5] += rapidUtern;
									icon = new Tmap.Icon($V4.path+"/common/images/m_icon11.png", size, offset);
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon11.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>급유턴</div>";
								}
								else if(overSpeed > 0) {
									outerSuddenList[6] += overSpeed;
									icon = new Tmap.Icon($V4.path+"/common/images/m_icon6.png", size, offset);
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon6.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>과속</div>";
								}
								else if(overSpeedLong > 0) {
									outerSuddenList[7] += overSpeedLong;
									icon = new Tmap.Icon($V4.path+"/common/images/m_icon7.png", size, offset);
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon7.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>장기과속</div>";
								}
								strIconStr += "</li>";
								
								
							}else if(poiCnt > 1){
								icon = new Tmap.Icon($V4.path+"/common/images/m_icon3.png", size, offset);
								strIconStr = "<div style='width:100px'><ul style='width:100%'>"; 
								
								if(rapidStart > 0) {
									outerSuddenList[0] = outerSuddenList[0]+rapidStart;
									strIconStr += "<li style='width:100%'>";
									/* strIconStr += "<div style='width:20%;display:inline-block;'><img src='"+$V4.path+"/common/images/m_icon1.png' /></div>";
									strIconStr += "<div style='width:50%;display:inline-block;'>급출발</div>";
									strIconStr += "<div style='width:30%;display:inline-block;'>"+rapidStart+" 회</div>"; */
									
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon1.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>급출발 "+rapidStart+" 회</div>";
									
									strIconStr += "</li>";
								}
								if(rapidStop > 0) {
									outerSuddenList[1] = outerSuddenList[1]+rapidStop;
									strIconStr += "<li style='width:100%'>";
									/* strIconStr += "<div style='width:20%;display:inline-block;'><img src='"+$V4.path+"/common/images/m_icon2.png' /></div>";
									strIconStr += "<div style='width:50%;display:inline-block;'>급정지</div>";
									strIconStr += "<div style='width:30%;display:inline-block;'>"+rapidStop+" 회</div>"; */
									
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon2.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>급정지 "+rapidStop+" 회</div>";
									
									strIconStr += "</li>";
								}
								if(rapidAccel > 0) {
									outerSuddenList[2] = outerSuddenList[2]+rapidAccel;
									strIconStr += "<li style='width:100%'>";
									/* strIconStr += "<div style='width:20%;display:inline-block;'><img src='"+$V4.path+"/common/images/m_icon4.png' /></div>";
									strIconStr += "<div style='width:50%;display:inline-block;'>급가속</div>";
									strIconStr += "<div style='width:30%;display:inline-block;'>"+rapidAccel+" 회</div>"; */
									
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon4.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>급가속 "+rapidAccel+" 회</div>";
									strIconStr += "</li>";
								}
								if(rapidDeaccel > 0) {
									outerSuddenList[3] = outerSuddenList[3]+rapidDeaccel;
									
									strIconStr += "<li style='width:100%'>";
									/* strIconStr += "<div style='width:20%;display:inline-block;'><img src='"+$V4.path+"/common/images/m_icon5.png' /></div>";
									strIconStr += "<div style='width:50%;display:inline-block;'>급감속</div>";
									strIconStr += "<div style='width:30%;display:inline-block;'>"+rapidDeaccel+" 회</div>"; */
									
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon5.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>급감속 "+rapidDeaccel+" 회</div>";
									
									strIconStr += "</li>";
								}
								if(rapidTurn > 0) {
									outerSuddenList[4] = outerSuddenList[4]+rapidTurn;
									
									strIconStr += "<li style='width:100%'>";
									/* strIconStr += "<div style='width:20%;display:inline-block;'><img src='"+$V4.path+"/common/images/m_icon10.png' /></div>";
									strIconStr += "<div style='width:50%;display:inline-block;'>급회전</div>";
									strIconStr += "<div style='width:30%;display:inline-block;'>"+rapidTurn+" 회</div>"; */
									
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon10.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>급회전 "+rapidTurn+" 회</div>";
									
									strIconStr += "</li>";
								}
								if(rapidUtern > 0) {
									outerSuddenList[5] = outerSuddenList[5]+rapidUtern;
									
									strIconStr += "<li style='width:100%'>";
									/* strIconStr += "<div style='width:20%;display:inline-block;'><img src='"+$V4.path+"/common/images/m_icon11.png' /></div>";
									strIconStr += "<div style='width:50%;display:inline-block;'>급유턴</div>";
									strIconStr += "<div style='width:30%;display:inline-block;'>"+rapidUtern+" 회</div>"; */
									
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon11.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>급유턴 "+rapidUtern+" 회</div>";
									
									strIconStr += "</li>";
								}
								
								if(overSpeed > 0) {
									outerSuddenList[6] = outerSuddenList[6]+overSpeed;
									
									strIconStr += "<li style='width:100%'>";
									/* strIconStr += "<div style='width:20%;display:inline-block;'><img src='"+$V4.path+"/common/images/m_icon6.png' /></div>";
									strIconStr += "<div style='width:50%;display:inline-block;'>과속</div>";
									strIconStr += "<div style='width:30%;display:inline-block;'>"+overSpeed+" 회</div>"; */
									
									
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon6.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>과속 "+overSpeed+" 회</div>";
									strIconStr += "</li>";
								}
								
								if(overSpeedLong > 0) {
									outerSuddenList[7] = outerSuddenList[7]+overSpeedLong;
									
									strIconStr += "<li style='width:100%'>";
									/* strIconStr += "<div style='width:20%;display:inline-block;'><img src='"+$V4.path+"/common/images/m_icon7.png' /></div>";
									strIconStr += "<div style='width:50%;display:inline-block;'>장기과속</div>";
									strIconStr += "<div style='width:30%;display:inline-block;'>"+overSpeedLong+" 회</div>"; */
									
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon7.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>장기과속 "+overSpeedLong+" 회</div>";
									
									strIconStr += "</li>";
								}
								strIconStr += "</ul></div>"; 
								
								
							}
							
							var label = new Tmap.Label(strIconStr);
							var point = get3857LonLat(arrTrackRecord[i].lon,arrTrackRecord[i].lat);
							var markers = new Tmap.Markers(point, icon, label);	
						
							tClassObj.arrPoiMarker.push(markers);
							//create complate poiMarkers
						}
						
					}
					
				}
			}
			
			
			
			
			var drawMap = function(){
				var markerLayer = new Tmap.Layer.Markers( "markerLayer" + (settings.idx));
				var vectorLayer = new Tmap.Layer.Vector( "vectorLayer" + (settings.idx));
								
				if(tClassObj.vector.length > 0) {
					objMap.addLayer(vectorLayer);
					vectorLayer.addFeatures(tClassObj.vector);
				}
				//var bounds = objMapVectorLayer.getDataExtent();
				
				//objMap.zoomToExtent(bounds);
				
				objMap.addLayer(markerLayer);
				
				for(var i = 0 ; i < tClassObj.arrPoiMarker.length ; i++){
					const marker = tClassObj.arrPoiMarker[i];
					
					markerLayer.addMarker(marker);
					
					if(alwaysPopup) marker.popup.show();
					else{
						marker.events.register("mouseover", marker.popup, function(o){this.show();});
						marker.events.register("mouseout", marker.popup, function(o){this.hide();});	
					}
					
					if(settings.searchType == 'vehicle'){
						marker.events.register("click", marker, function(o){
							
							$.extend(_historyBackOption = {}, _o );
							
							_o.searchType = 'time';
							var strHtml = marker.popup.contentHTML;
							strHtml = strHtml.substr(strHtml.indexOf("<input"));
							_o.vehicleKey = $(strHtml).data("vehiclekey"); 
							_o.searchTime = _o.lastestTime?_o.lastestTime*3600*1000:3600*24*1000;							
									
							outerReset();
						});
					}
					
				}

				for(var i = 0 ; i < tClassObj.startMarker.length ; i++)
					markerLayer.addMarker(tClassObj.startMarker[i]);
				for(var i = 0 ; i < tClassObj.endMarker.length ; i++)
					markerLayer.addMarker(tClassObj.endMarker[i]);
			
				objMapMarkerLayer.push(markerLayer);
				if(tClassObj.vector.length > 0) objMapVectorLayer.push(vectorLayer);  
				
				settings.idx = settings.idx+1;
			}
			
			var setCenter = function(){
				var minLeft;
				var minBot;
				var maxRight;
				var maxTop;
				if(objMapVectorLayer.length > 0)					
					for(var i = 0 ; i < objMapVectorLayer.length ; i++){
						var v = objMapVectorLayer[i];
						var bounds = v.getDataExtent();
						
						if(i == 0){
							minLeft = bounds.left;
							minBot = bounds.bottom;
							maxRight = bounds.right;
							maxTop = bounds.top;
						}else{
							if(minLeft > bounds.left) minLeft = bounds.left;
							if(minBot > bounds.bottom) minBot = bounds.bottom;
							if(maxRight < bounds.right) maxRight = bounds.right;
							if(maxTop < bounds.top) maxTop = bounds.top;
						}
					}
				else 
					for(var i = 0 ; i < objMapMarkerLayer.length ; i++){
						var v = objMapMarkerLayer[i];
						var bounds = v.getDataExtent();
						
						if(i == 0){
							minLeft = bounds.left;
							minBot = bounds.bottom;
							maxRight = bounds.right;
							maxTop = bounds.top;
						}else{
							if(minLeft > bounds.left) minLeft = bounds.left;
							if(minBot > bounds.bottom) minBot = bounds.bottom;
							if(maxRight < bounds.right) maxRight = bounds.right;
							if(maxTop < bounds.top) maxTop = bounds.top;
						}
						
						if(objMapMarkerLayer.length == 1){
							minLeft += -200;
							minBot += -200;
							maxRight += 200;
							maxTop += 200;
						}
					}
				var bounds = new Tmap.Bounds(minLeft, minBot, maxRight, maxTop);
				 
				objMap.zoomToExtent(bounds);
				
				
			}
			
			initMap();
			
			if(settings.searchType=='default') return $(this);
			
			var tClassObj = {};
			tClassObj.vector = [];
			tClassObj.arrPoiMarker = [];
			tClassObj.startMarker = [];
			tClassObj.endMarker = [];
			
			settings.objData = getTdr();
			
			if(settings.objData && settings.objData.length > 0){
				createGeoData();
				if(settings.searchType!='vehicle') createPoiData();
				drawMap();
				setCenter();
				window.parent&&window.parent.outerSuddenSummaryCall&&window.parent.outerSuddenSummaryCall(outerSuddenList);	
			}
			
		});
		
		
	}
	
	/*	
	var xxx = 0;
	setInterval(function(){
		if(xxx%2 == 0) {
			_objMap = $("#map_div").tMapLoader(_o);
		}else{
			$.tMapLoader.destroy(_objMap);
		}
		
		
		if(xxx == 0) _objMap = $("#map_div").tMapLoader(_o);
		else $.tMapLoader.reset(_objMap,_o);
		xxx++;
		
	},5000);
	*/
	
	_objMap = $("#map_div").tMapLoader(_o);
});

function outerReset(){
	$.tMapLoader.reset(_objMap,_o);
}
function getLoadComplate(){
	return loadComplate;
}
</script>

<script type="text/javascript">


	

</script>

    </head>
    <body>
        <div id="map_div">
        </div>        
    </body>
</html>