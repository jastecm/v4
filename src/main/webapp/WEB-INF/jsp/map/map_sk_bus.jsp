<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>




<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta name="description" content="VIEWCAR - For Your Smart Driving">
<meta property="og:title" content="VIEWCAR">
<meta property="og:description" content="VIEWCAR - For Your Smart Driving">

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/js/jqueryui/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/content.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/content_costom.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/style.css" />
<link rel="manifest" href="${pageContext.request.contextPath}/common/json/manifest.json">
<%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/jsDatePick_ltr.min.css" media="all" /> --%>

<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script src="${pageContext.request.contextPath}/common/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/common.js"></script>
<script src="${pageContext.request.contextPath}/common/js/topup.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jqueryui/jquery-ui.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.stylish-select.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jsDatePick.jquery.min.1.3.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/util.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.form.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.MultiFile.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/message/${_LANG}.js"></script>

<script type="text/javascript" src="https://www.gstatic.com/firebasejs/4.1.3/firebase.js"></script>
<script src="${pageContext.request.contextPath}/common/js/util.js"></script>
<script src="${pageContext.request.contextPath}/common/new/js/v4.js"></script>
<script src="${pageContext.request.contextPath}/common/new/js/commonV4.js"></script>
<!-- <script src="https://apis.skplanetx.com/tmap/js?version=1&format=javascript&appKey=991c618e-8e49-373a-ac09-53d53b776ed9"></script> -->
<script src="https://api2.sktelecom.com/tmap/js?version=1&format=javascript&appKey=e457a644-6d96-4ed6-ba81-6ed26b17e037"></script>

<script type="text/javascript">
var busRouteKey = "${busRouteKey}";

var _objMap = null;

var _o = createOption();

var loadComplate = true;

function createOption(){
	return {busRouteKey : busRouteKey};
}

$(document).ready(function(){
	
	$.tMapLoader = {
			destroy : function(tMapLoaderObj){
				var action = tMapLoaderObj.data('tMapLoaderAction');
				tMapLoaderObj = action.destroy(tMapLoaderObj);
			},
			reset : function(tMapLoaderObj,options){
				var action = tMapLoaderObj.data('tMapLoaderAction');
				tMapLoaderObj = action.reset(tMapLoaderObj,options);
			}
		}
	
	$.fn.tMapLoader = function(options){
		
		return this.each(function(){
			var settings = $.extend({
				div : "map_div"
				,zoom : 14
				,mapW : '100%'
				,mapH : '100%'						
				,maxZoom : 6
				,minZoom : 15
				,devisionVar : 5
				,devisionRtv : 500
				,initCenter : ""
				,tripKey : ""
				,vehicleKey : ""
				,accountKey : ""
				,searchTime : ""
				,searchDate : ""
				,searchType : ""
				,objData : {}
				,mapProjection : "EPSG:3857"
				,idx : 1
				,colorCd : ["#ed1c24","#1cb0ed","#2bc981","#b42bd6","#e4a01e"] //빨 파 초 보 노
				,objMap : null
				,alwaysPopup : false
		    }, options );
			
			var pr_3857 = new Tmap.Projection("EPSG:3857");
			 
			//pr_4326 인스탄스 생성.
			var pr_4326 = new Tmap.Projection("EPSG:4326");
			 
			var get3857LonLat = function(coordX, coordY){
			    return new Tmap.LonLat(coordX, coordY).transform(pr_4326, pr_3857);
			}
			       
			var get4326LonLat = function(coordX, coordY){
			    return new Tmap.LonLat(coordX, coordY).transform(pr_3857, pr_4326);
			}
			
			var tMapLoaderAction = {
				destroy : function(objTmap){
					settings.objMap.destroy();
					$("#"+settings.div).html("");
				},reset : function(objTmap,__o){
					tMapLoaderAction.destroy(objTmap);
					return $("#"+settings.div).tMapLoader(__o);
				}
			}
			
			
			
			$(this).data('tMapLoaderAction',tMapLoaderAction);
			
			
			var getTdr = function(){
				var data = null;
				$V4.http_post("/api/1/map/bus",_o,{
					sync : true
					,requestMethod:"GET"
					,header:{key:"${_KEY}"}
					,success : function(r){
						data = r;
						loadComplate = true;
					}
				});
				return data;
			}
			
			
			var objMap;
			var objMapMarkerLayer = [];
			
			var initMap = function(){
				objMap = new Tmap.Map({div:settings.div
					, width:settings.mapW
					, height:settings.mapH
					, animation:true
				});
			
				settings.objMap = objMap;
				
			}
			
			initMap();
			
			if(settings.objData){
				settings.objData = getTdr();
			}
			
			var tClassObj = {};
			tClassObj.vector = null;
			tClassObj.arrPoiMarker = [];
			tClassObj.startMarker = [];
			tClassObj.endMarker = [];
			//tClassObj.centerPoint = null;
			//tClassObj.mapLev = settings.maxZoom;
			
			var drawMap = function(){
				var markerLayer_bus = new Tmap.Layer.Markers( "markerLayer" + (settings.idx));
				var markerLayer_route = new Tmap.Layer.Markers( "markerLayer" + (settings.idx+1));
								
				
				objMap.addLayer(markerLayer_route);
				objMap.addLayer(markerLayer_bus);
				////////////////////
				
				for(var i = 0 ; i < tClassObj.arrPoiMarker.length ; i++){
					const marker = tClassObj.arrPoiMarker[i];
					
					markerLayer_route.addMarker(marker);
					
					//marker.events.register("mouseover", marker.popup, function(o){console.log(this);this.show();});
					//marker.events.register("mouseout", marker.popup, function(o){this.hide();});	
				}
				
				
				for(var i = 0 ; i < tClassObj.startMarker.length ; i++)
					markerLayer_route.addMarker(tClassObj.startMarker[i]);
				for(var i = 0 ; i < tClassObj.endMarker.length ; i++)
					markerLayer_route.addMarker(tClassObj.endMarker[i]);
			
				objMapMarkerLayer.push(markerLayer_route);
				///////////////////////////////////////
				
				for(var i = 0 ; i < tClassObj.arrBusMarker.length ; i++){
					const marker = tClassObj.arrBusMarker[i];
					
					markerLayer_bus.addMarker(marker);
					
					marker.events.register("mouseover", marker.popup, function(o){console.log(this);this.show();});
					marker.events.register("mouseout", marker.popup, function(o){this.hide();});	
				}
				objMapMarkerLayer.push(markerLayer_bus);
			}
			
			var setCenter = function(){
				var minLeft;
				var minBot;
				var maxRight;
				var maxTop;
				
				for(var i = 0 ; i < objMapMarkerLayer.length ; i++){
					
					var v = objMapMarkerLayer[i];
					
					var bounds = v.getDataExtent();
					console.log(bounds);
					/*
					if(i == 0){
						minLeft = bounds.left;
						minBot = bounds.bottom;
						maxRight = bounds.right;
						maxTop = bounds.top;
					}else{
						if(minLeft > bounds.left) minLeft = bounds.left;
						if(minBot > bounds.bottom) minBot = bounds.bottom;
						if(maxRight < bounds.right) maxRight = bounds.right;
						if(maxTop < bounds.top) maxTop = bounds.top;
					}
					*/
				}
				
				//var bounds = new Tmap.Bounds(minLeft, minBot, maxRight, maxTop);
				objMap.zoomToExtent(objMapMarkerLayer[0].getDataExtent());
				
			}
			
			
			
			//create tmapClassObj
			if(settings.objData){
				
				var routeInfo = settings.objData.routeInfo.busStation;
				var busInfo = settings.objData.vehicleInfo;
				
				tClassObj.arrPoiMarker = [];
				tClassObj.startMarker = [];
				tClassObj.endMarker = [];
				tClassObj.arrBusMarker = [];
				
				for(var z = 0 ; z < routeInfo.length ; z++){
					var station = routeInfo[z];
					
					if(z == 0){
						var startPoint = get3857LonLat(station.lon,station.lat);
						var size = new Tmap.Size(28,37);
						var offset = new Tmap.Pixel(-(size.w/2), -size.h);
						var icon = new Tmap.Icon($V4.path+"/common/images/busStart.png", size, offset);
						var sMarker = new Tmap.Marker(startPoint, icon);
						tClassObj.startMarker.push(sMarker);
					}else if(z == routeInfo.length-1){
						var endPoint = get3857LonLat(station.lon,station.lat);
						var size = new Tmap.Size(28,37);
						var offset = new Tmap.Pixel(-(size.w/2), -size.h);
						var icon = new Tmap.Icon($V4.path+"/common/images/busEnd.png", size, offset);
						var eMarker = new Tmap.Marker(endPoint, icon);
						tClassObj.endMarker.push(eMarker);
					}else{
						var point = get3857LonLat(station.lon,station.lat);
						var size = new Tmap.Size(27,27);
						var offset = new Tmap.Pixel(-(size.w/2), -size.h);
						var icon = new Tmap.Icon($V4.path+"/common/images/m_number_"+z+".png", size, offset);
						var marker = new Tmap.Marker(point, icon);
						
						tClassObj.arrPoiMarker.push(marker);
					}
				}
				//////////////////////////////////////
				if(busInfo != null){
					for(var z = 0 ; z < busInfo.length ; z++){
						
						var bus = busInfo[z];
						debugger
						var lonlat = bus.latestLocation.location.split(',');
						var point = get3857LonLat(lonlat[0],lonlat[1]);
						var size = new Tmap.Size(25,35);
						var offset = new Tmap.Pixel(-(size.w/2), -size.h);
						var icoNm = $V4.path+"/common/images/";
						//1이면 운행중
						if(bus.vehicleState == '1') icoNm += "ico_map_car_on.png";
						else icoNm += "ico_map_p_on.png";
						
						var icon = new Tmap.Icon(icoNm, size, offset);										
						
						var strLabel = bus.plateNum;
						var label = new Tmap.Label(strLabel);
						 
						var marker = new Tmap.Markers(point, icon,label);
						tClassObj.arrBusMarker.push(marker);
					}
				}
				
				drawMap();
				setCenter();
				
				//$(".tmPopup").css("width","150px;").css("height","150px");
			}
		});
		
		
	}
	
	_objMap = $("#map_div").tMapLoader(_o);
});

function outerReset(){
	$.tMapLoader.reset(_objMap,_o);
}
function getLoadComplate(){
	return loadComplate;
}
</script>

<script type="text/javascript">


	

</script>

    </head>
    <body >
        <div id="map_div">
        </div>        
    </body>
</html>