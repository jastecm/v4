<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<html lang="ko">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="title" content="차량운행 관리 서비스 VIEW CAR" />
    <meta name="description" content="" />

	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/earlyaccess/notosanskr.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/common/new/css/jquery-ui.min.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/common/new/css/style.css" />

	<%@include file="/WEB-INF/jsp/common/common.jsp"%>
	

	<script src="${pageContext.request.contextPath}/common/new/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/common/new/js/jquery-ui.min.js"></script>
    <script src="${pageContext.request.contextPath}/common/new/js/jquery-debounce.js"></script>
    <script src="${pageContext.request.contextPath}/common/new/js/base64.js"></script>
    <script src="${pageContext.request.contextPath}/common/new/js/chart.js"></script>
    <script src="${pageContext.request.contextPath}/common/new/js/common.js"></script>
    
    
    <script src="${pageContext.request.contextPath}/common/js/xlsx/xlsx.full.min.js" type="text/javascript"></script>
    
    <script src="${pageContext.request.contextPath}/common/js/util.js"></script>
    <script src="${pageContext.request.contextPath}/common/new/js/v4.js"></script>
    <script src="${pageContext.request.contextPath}/common/new/js/commonV4.js"></script>
    
	<script src="https://api2.sktelecom.com/tmap/js?version=1&format=javascript&appKey=43e32609-b337-47d5-b058-7813f232c2d5"></script>
	<style type="text/css">
	  html, body { height: 100%; margin: 0; padding: 0; }
	  #map_div { height: 100%; }
	</style>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBtg0jjCHhN6wG0KoUBSi2E2oX6nJ8SIIA&callback=init"></script>

<script type="text/javascript">
var searchType = "${searchType}" ;//"${searchType}" //trip,time,date,vehicle,default
var tripKey = "${tripKey}"; //used trip
var vehicleKeys = "${vehicleKeys}"; //used vehicle
var searchDriverKey = "${searchDriverKey}"; //used vehicle
var searchDrivingState = "${searchDrivingState}" // 1주행중 : 0주차중 //used vehicle
var searchAllocateSate = "${searchAllocateSate}";  //1배차중 : 0미배차 //used vehicle
var searchVehicleGroup = "${searchVehicleGroup}";  //차량 그룹 //used vehicle
var searchDriverGroup = "${searchDriverGroup}";  //사용자 그룹 //used vehicle
var searchAreaLev1 = "${searchAreaLev1}"; //used vehicle
var searchAreaLev2 = "${searchAreaLev2}"; //used vehicle
var lastestTime = "${lastestTime}"; //used vehicle
var alwaysPopup = "${alwaysPopup}"; //used vehicle

var vehicleKey = "${vehicleKey}"; //used trip,date,time
var accountKey = "${accountKey}"; //used trip,date,time
var searchTime = "${searchTime}"; //searchTime unit = sec //used time
var searchDate = "${searchDate}"; //searchDate unit = "YYYYMMDD" //used date

var _time = null;

var _objMap = null;

var _o = createOption();

var _historyBackOption = null;

var loadComplate = true;

var meters_per_pixel_ary = []; 
meters_per_pixel_ary [0] = 156543.03392; 
meters_per_pixel_ary [1] = 78271.51696; 
meters_per_pixel_ary [2] = 39135.75848; 
meters_per_pixel_ary [3] = 19567.87924; 
meters_per_pixel_ary [4] = 9783.93962; 
meters_per_pixel_ary [5] = 4891.96981; 
meters_per_pixel_ary [6] = 2445.98490; 
meters_per_pixel_ary [7] = 1222.99245; 
meters_per_pixel_ary [8] = 611.49622; 
meters_per_pixel_ary [9] = 305.74811; 
meters_per_pixel_ary [10] = 152.87405; 
meters_per_pixel_ary [11] = 76.43702; 
meters_per_pixel_ary [12] = 38.21851; 
meters_per_pixel_ary [13] = 19.10925; 
meters_per_pixel_ary [14] = 9.55462; 
meters_per_pixel_ary [15] = 4.77731; 
meters_per_pixel_ary [16] = 2.38865; 
meters_per_pixel_ary [17] = 1.19432; 
meters_per_pixel_ary [18] = 0.59716; 
meters_per_pixel_ary [19] = 0.29858;
function createOption(){
	if(searchType == 'trip'){
		_historyBackOption = null;
		return {searchType : searchType , vehicleKey : vehicleKey , tripKey : tripKey};		
	}else if(searchType == 'time'){
		_historyBackOption = null;
		return {searchType : searchType , vehicleKey : vehicleKey , accountKey : accountKey , searchTime : searchTime}
	}else if(searchType == 'date'){
		_historyBackOption = null;
		return {searchType : searchType , vehicleKey : vehicleKey , accountKey : accountKey , searchDate : searchDate}
	}else if(searchType == 'vehicle'){		
		_historyBackOption = null;
		return {searchType : searchType  , vehicleKeys : vehicleKeys 
			, searchDriverKey : searchDriverKey 
			, searchDrivingState : searchDrivingState, searchAllocateSate : searchAllocateSate
			, searchVehicleGroup : searchVehicleGroup , searchDriverGroup : searchDriverGroup 
			, searchAreaLev1 : searchAreaLev1 , searchAreaLev2 : searchAreaLev2
			, alwaysPopup : alwaysPopup
			, lastestTime : lastestTime};
	}else if(searchType == 'default'){		
		_historyBackOption = null;
		return {searchType : searchType};
	}
	
}
    
    
$(document).ready(function(){
    $.googleMapLoader = {
		destroy : function(googleMapLoaderObj){
			var action = tMapLoaderObj.data('googleMapLoaderAction');
			googleMapLoaderObj = action.destroy(googleMapLoaderObj);
		},
		reset : function(googleMapLoaderObj,options){
			var action = googleMapLoaderObj.data('googleMapLoaderAction');
			googleMapLoaderObj = action.reset(googleMapLoaderObj,options);
		}
	}
    
    $.fn.googleMapLoader = function(options){
    	return this.each(function(){
    		var settings = $.extend({
				div : "map_div"
				,zoom : 14
				,mapW : '100%'
				,mapH : '100%'						
				,maxZoom : 6
				,minZoom : 16
				,devisionVar : 5
				,devisionRtv : 500
				,initCenter : ""
				,tripKey : ""
				,vehicleKey : ""
				,accountKey : ""
				,searchTime : ""
				,searchDate : ""
				,searchType : ""
				,objData : {}
				,mapProjection : "EPSG:3857"
				,idx : 1
				,colorCd : ["#ed1c24","#1cb0ed","#2bc981","#b42bd6","#e4a01e"] //빨 파 초 보 노
				,objMap : null
				,alwaysPopup : false
				,center : {lat: 37.5010226, lng: 127.0396037} //sk
				,debugMode : false
				,gpsAccuracy : 1000
				//{lat : 37.403348 , lon : 127.102056} //jastec
		    }, options );
    		
    		var googleMapLoaderAction = {
   				destroy : function(objTmap){
   					settings.objMap = null;
   					$("#"+settings.div).html("");
   				},reset : function(objTmap,__o){
   					googleMapLoaderAction.destroy(objTmap);
   					return $("#"+settings.div).googleMapLoader(__o);
   				}
   			}
    		
    		$(this).data('googleMapLoaderAction',googleMapLoaderAction);
    		
    		var objMap;
			var objMapMarkerLayer = [];
			var objMapVectorLayer = [];
			var outerSuddenList = [0,0,0,0,0,0,0,0];
			var objCenter = {lat : 37.403348 , lon : 127.102056};
			var objMaxPoint = {lat : 37.403348 , lon : 127.102056};
			var objMinPoint = {lat : 37.403348 , lon : 127.102056};
			var objZoomLev = 16;
			
    		var initialize = function () {
    			objMap = new google.maps.Map(document.getElementById(settings.div), {
    		        center: settings.center,
    		        zoom: objZoomLev
    			});
    			
    			settings.objMap = objMap;
    			
				if(_historyBackOption)
					objMap.addListener('click',function(){
						if(confirm("뒤로 돌아가시겟습니까?")){
							searchType = _historyBackOption.searchType;
							vehicleKeys = _historyBackOption.vehicleKeys;
							searchDriverKey = _historyBackOption.searchDriverKey;
							searchDrivingState = _historyBackOption.searchDrivingState;
							searchAllocateSate = _historyBackOption.searchAllocateSate;
							searchVehicleGroup = _historyBackOption.searchVehicleGroup;
							searchDriverGroup = _historyBackOption.searchDriverGroup;
							searchAreaLev1 = _historyBackOption.searchAreaLev1;
							searchAreaLev2 = _historyBackOption.searchAreaLev2;
							alwaysPopup = _historyBackOption.alwaysPopup;
							lastestTime = _historyBackOption.lastestTime;
							
							_o = createOption();
							
							outerReset();	
						}
					});
    		}
    		
    		var getData = function(){
				var data = null;
				
				if(settings.searchType=='vehicle'){
					$V4.http_post("/api/v1/vehicle/location",_o,{
						sync : true
						,requestMethod:"GET"
						,header:{key:"${_KEY}"}
						,success : function(r){
							data = r.result;
							if(data == null || data.length == 0) {
								alert("주행내역이 없습니다.");
								loadComplate = false;
							}else{
								loadComplate = true;
								data = [data];
							}
						}
					});
					
				}else{
					var url = "";
					if(settings.searchType == 'trip'){
						url = "/api/v1/vehicle/"+settings.vehicleKey+"/trackRecord/"+settings.tripKey;
					}else if(settings.searchType == 'date'){
						url = "/api/v1/vehicle/"+settings.vehicleKey+"/trackRecord/date/"+settings.searchDate;
					}else if(settings.searchType == 'time'){
						url = "/api/v1/vehicle/"+settings.vehicleKey+"/trackRecord/recentTime/"+settings.searchTime;
					}
					
					if(url){
						$V4.http_post(url,{gpsAccuracy : settings.gpsAccuracy},{
							sync : true
							,requestMethod:"GET"
							,header:{key:"${_KEY}"}
							,success : function(r){
								data = r.result;
								if(data == null || data.length == 0) {
									data = [];
									alert("주행내역이 없습니다.");
									loadComplate = false;
								}else{
									if(settings.searchType == 'trip') data = [data];
									loadComplate = true;	
								}
								
							},error :function(t){
								
								t.responseJSON?alert(t.status+"\n"+t.responseJSON.result):null==_o.error&&alert(t.status+"\n"+t.statusText);
								loadComplate = false;
								
								if(_historyBackOption){
									searchType = _historyBackOption.searchType;
									vehicleKey = _historyBackOption.vehicleKey;
									accountKey = _historyBackOption.accountKey;
									vehicleState = _historyBackOption.vehicleState;
									allocateState = _historyBackOption.allocateState;
									vehicleGroup = _historyBackOption.vehicleGroup;
									accountGroup = _historyBackOption.accountGroup;
									searchArealev1 = _historyBackOption.searchArealev1;
									searchArealev2 = _historyBackOption.searchArealev2;
									alwaysPopup = _historyBackOption.alwaysPopup;
									lastestTime = _historyBackOption.lastestTime;
									
									_o = createOption();
									
									outerReset();
								}
							}
						});	
					}else{
						data = [];
					}
					
					
					
				}
				
				
				
				return data;
			}
    		
    		var createGeoData = function(){
				if(settings.objData){
					
					tClassObj.vector = [];
					tClassObj.startMarker = [];
					tClassObj.endMarker = [];
					
					for(var z = 0 ; z < settings.objData.length ; z++){
						
						
						var d = settings.objData[z];
						//settings.searchType trip = obj , else = list[obj]
						
						
						if(settings.searchType == 'vehicle'){
							var vo = d;
							var icoNm = $V4.path+"/common/images/";
							if(vo.drivingState == '1') icoNm += "ico_map_car_";
							else icoNm += "ico_map_p_";
							if(vo.allocateState == '1') icoNm += "on.png";
							else icoNm += "off.png";
							var driverNm = getProperty(vo,"driver.name");
							var strIconStr = vo.plateNum+((driverNm)?"/"+driverNm:"");														
							
							var location = getProperty(vo,"latestLocation.location");
							var marker = new google.maps.Marker({
							    position: new google.maps.LatLng(location.split(",")[1],location.split(",")[0]),
							    animation : google.maps.Animation.BOUNCE,
							    icon:icoNm,
							    title : strIconStr
							});
							
							marker.vehicleKey = vo.vehicleKey;
							marker.strInfoHtml = "";
							
							tClassObj.arrPoiMarker.push(marker);	
							
							window.parent&&window.parent.setAddr&&window.parent.setAddr(getProperty(vo,'latestLocation.locationAddr'),getProperty(vo,'latestLocation.locationAddrDetail'));
							
								
						}else{
							var vo = d;
							
							var arrTrackRecord = [];
							
							if(vo.tripMatched == '1') arrTrackRecord = vo.matchedTrackRecord;
							else arrTrackRecord = vo.trackRecord;
							
							var arrTmapPoint = [];
							var tempStartPoiList = [];
							
							for(var i = 0 ; i <arrTrackRecord.length ; i++){
								if(!arrTrackRecord[i].gpsAccuracyOut){ //이상좌표 아닌것만 TODO 검증테스트 필요
									arrTmapPoint.push(new google.maps.LatLng(arrTrackRecord[i].lat,arrTrackRecord[i].lon));
								}
							}
							
						    var startPoint = new google.maps.LatLng(vo.startLat,vo.startLon);
						    var endPoint = new google.maps.LatLng(vo.endLat,vo.endLon);
							
							arrTmapPoint.unshift(startPoint);
							arrTmapPoint.push(endPoint);
							
							if(settings.searchType=='trip'){
								var icon = $V4.path+"/common/images/${_LANG}/start.png";
								var sMarker = new google.maps.Marker({
								    position: new google.maps.LatLng(vo.startLat,vo.startLon),
								    animation : google.maps.Animation.BOUNCE,
								    icon:icon
								});
								var iconE = $V4.path+"/common/images/${_LANG}/end.png";
								var eMarker = new google.maps.Marker({
									    position: new google.maps.LatLng(vo.endLat,vo.endLon),
									    animation : google.maps.Animation.BOUNCE,
									    icon:iconE
									});
								
								tClassObj.startMarker.push(sMarker);
								tClassObj.endMarker.push(eMarker);
							}else{
								var icon = $V4.path+"/common/images/m_number_"+((settings.idx))+".png";
								var sMarker = new google.maps.Marker({
									position: new google.maps.LatLng(vo.startLat,vo.startLon),
								    animation : google.maps.Animation.BOUNCE,
								    icon:icon
								});
								var iconE = $V4.path+"/common/images/m_number_"+((settings.idx))+".png";
								var eMarker = new google.maps.Marker({
										position: new google.maps.LatLng(vo.endLat,vo.endLon),
									    animation : google.maps.Animation.BOUNCE,
									    icon:iconE
									});
								
								tClassObj.startMarker.push(sMarker);
								tClassObj.endMarker.push(eMarker);
							}
							
							var flightPath = new google.maps.Polyline({
							    path: arrTmapPoint,
							    geodesic: true,
							    strokeColor: settings.colorCd[z%(settings.colorCd.length)],
							    strokeOpacity: 1.0,
							    strokeWeight: 5
							});
							
							//create complate vector
							tClassObj.vector.push(flightPath);
							settings.idx++;
						}
					}
				}
			}
    		
    		var createPoiData = function(){
				tClassObj.arrPoiMarker = [];
				for(var z = 0 ; z < settings.objData.length ; z++){
					var vo = settings.objData[z];
					
					var arrTrackRecord = [];
					
					if(vo.tripMatched == '1') arrTrackRecord = vo.matchedTrackRecord;
					else arrTrackRecord = vo.trackRecord;
					
					for(var i = 0 ; i < arrTrackRecord.length ; i++){
						var rapidStart = getProperty(arrTrackRecord[i],"rapidStart");
						var rapidStop = getProperty(arrTrackRecord[i],"rapidStop");
						var rapidAccel = getProperty(arrTrackRecord[i],"rapidAccel");
						var rapidDeaccel = getProperty(arrTrackRecord[i],"rapidDeaccel");
						var rapidTurn = getProperty(arrTrackRecord[i],"rapidTurn");
						var rapidUtern = getProperty(arrTrackRecord[i],"rapidUtern");
						var overSpeed = getProperty(arrTrackRecord[i],"overSpeed");
						var overSpeedLong = getProperty(arrTrackRecord[i],"overSpeedLong");
						var poiCnt = rapidStart+rapidStop+rapidAccel+rapidDeaccel+rapidTurn+rapidUtern+overSpeed+overSpeedLong;
						
						if(poiCnt > 0){
							var icon = $V4.path+"/common/images/m_img3.png";
							var strIconStr = "";
							
							if(poiCnt == 1){
								strIconStr = "<div style='width:100px;overflow:hidden;'><ul style='width:100%;overflow:hidden;'>";
								strIconStr += "<li style='width:100%;overflow:hidden;'>";
								if(rapidStart > 0) {
									outerSuddenList[0] += rapidStart;
									icon = $V4.path+"/common/images/m_icon1.png";
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon1.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>급출발</div>";
								}
								else if(rapidStop > 0) {
									outerSuddenList[1] += rapidStop;
									icon = $V4.path+"/common/images/m_icon2.png";
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon2.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>급정지</div>";
								}
								else if(rapidAccel > 0) {
									outerSuddenList[2] += rapidAccel;
									icon = $V4.path+"/common/images/m_icon4.png";
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon4.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>급가속</div>";
								}
								else if(rapidDeaccel > 0) {
									outerSuddenList[3] += rapidDeaccel;
									icon = $V4.path+"/common/images/m_icon5.png";
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon5.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>급감속</div>";
								}
								else if(rapidTurn > 0) {
									outerSuddenList[4] += rapidTurn;
									icon = $V4.path+"/common/images/m_icon10.png";
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon10.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>급회전</div>";
								}
								else if(rapidUtern > 0) {
									outerSuddenList[5] += rapidUtern;
									icon = $V4.path+"/common/images/m_icon11.png";
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon11.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>급유턴</div>";
								}
								else if(overSpeed > 0) {
									outerSuddenList[6] += overSpeed;
									icon = $V4.path+"/common/images/m_icon6.png";
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon6.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>과속</div>";
								}
								else if(overSpeedLong > 0) {
									outerSuddenList[7] += overSpeedLong;
									icon = $V4.path+"/common/images/m_icon7.png";
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon7.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>장기과속</div>";
								}
								strIconStr += "</li>";
								
								
							}else if(poiCnt > 1){
								icon = $V4.path+"/common/images/m_icon3.png";
								strIconStr = "<div style='width:100px;overflow:hidden;'><ul style='width:100%;overflow:hidden;'>";
								
								if(rapidStart > 0) {
									outerSuddenList[0] = outerSuddenList[0]+rapidStart;
									strIconStr += "<li style='width:100%'>";
									/* strIconStr += "<div style='width:20%;display:inline-block;'><img src='"+$V4.path+"/common/images/m_icon1.png' /></div>";
									strIconStr += "<div style='width:50%;display:inline-block;'>급출발</div>";
									strIconStr += "<div style='width:30%;display:inline-block;'>"+rapidStart+" 회</div>"; */
									
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon1.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>급출발 "+rapidStart+" 회</div>";
									
									strIconStr += "</li>";
								}
								if(rapidStop > 0) {
									outerSuddenList[1] = outerSuddenList[1]+rapidStop;
									strIconStr += "<li style='width:100%'>";
									/* strIconStr += "<div style='width:20%;display:inline-block;'><img src='"+$V4.path+"/common/images/m_icon2.png' /></div>";
									strIconStr += "<div style='width:50%;display:inline-block;'>급정지</div>";
									strIconStr += "<div style='width:30%;display:inline-block;'>"+rapidStop+" 회</div>"; */
									
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon2.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>급정지 "+rapidStop+" 회</div>";
									
									strIconStr += "</li>";
								}
								if(rapidAccel > 0) {
									outerSuddenList[2] = outerSuddenList[2]+rapidAccel;
									strIconStr += "<li style='width:100%'>";
									/* strIconStr += "<div style='width:20%;display:inline-block;'><img src='"+$V4.path+"/common/images/m_icon4.png' /></div>";
									strIconStr += "<div style='width:50%;display:inline-block;'>급가속</div>";
									strIconStr += "<div style='width:30%;display:inline-block;'>"+rapidAccel+" 회</div>"; */
									
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon4.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>급가속 "+rapidAccel+" 회</div>";
									
									strIconStr += "</li>";
								}
								if(rapidDeaccel > 0) {
									outerSuddenList[3] = outerSuddenList[3]+rapidDeaccel;
									
									strIconStr += "<li style='width:100%'>";
									/* strIconStr += "<div style='width:20%;display:inline-block;'><img src='"+$V4.path+"/common/images/m_icon5.png' /></div>";
									strIconStr += "<div style='width:50%;display:inline-block;'>급감속</div>";
									strIconStr += "<div style='width:30%;display:inline-block;'>"+rapidDeaccel+" 회</div>"; */
									
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon5.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>급감속 "+rapidDeaccel+" 회</div>";
									
									strIconStr += "</li>";
								}
								if(rapidTurn > 0) {
									outerSuddenList[4] = outerSuddenList[4]+rapidTurn;
									
									strIconStr += "<li style='width:100%'>";
									/* strIconStr += "<div style='width:20%;display:inline-block;'><img src='"+$V4.path+"/common/images/m_icon10.png' /></div>";
									strIconStr += "<div style='width:50%;display:inline-block;'>급회전</div>";
									strIconStr += "<div style='width:30%;display:inline-block;'>"+rapidTurn+" 회</div>"; */
									
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon10.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>급회전 "+rapidTurn+" 회</div>";
									
									strIconStr += "</li>";
								}
								if(rapidUtern > 0) {
									outerSuddenList[5] = outerSuddenList[5]+rapidUtern;
									
									strIconStr += "<li style='width:100%'>";
									/* strIconStr += "<div style='width:20%;display:inline-block;'><img src='"+$V4.path+"/common/images/m_icon11.png' /></div>";
									strIconStr += "<div style='width:50%;display:inline-block;'>급유턴</div>";
									strIconStr += "<div style='width:30%;display:inline-block;'>"+rapidUtern+" 회</div>"; */
									
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon11.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>급유턴 "+rapidUtern+" 회</div>";
									
									strIconStr += "</li>";
								}
								
								if(overSpeed > 0) {
									outerSuddenList[6] = outerSuddenList[6]+overSpeed;
									
									strIconStr += "<li style='width:100%'>";
									/* strIconStr += "<div style='width:20%;display:inline-block;'><img src='"+$V4.path+"/common/images/m_icon6.png' /></div>";
									strIconStr += "<div style='width:50%;display:inline-block;'>과속</div>";
									strIconStr += "<div style='width:30%;display:inline-block;'>"+overSpeed+" 회</div>"; */
									
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon6.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>과속 "+overSpeed+" 회</div>";
									
									strIconStr += "</li>";
								}
								
								if(overSpeedLong > 0) {
									outerSuddenList[7] = outerSuddenList[7]+overSpeedLong;
									
									strIconStr += "<li style='width:100%'>";
									/* strIconStr += "<div style='width:20%;display:inline-block;'><img src='"+$V4.path+"/common/images/m_icon7.png' /></div>";
									strIconStr += "<div style='width:50%;display:inline-block;'>장기과속</div>";
									strIconStr += "<div style='width:30%;display:inline-block;'>"+overSpeedLong+" 회</div>"; */
									
									strIconStr += "<div style='width:20%;position: relative;float: left;'><img src='"+$V4.path+"/common/images/m_icon7.png' /></div>";
									strIconStr += "<div style='width:80%;position: relative;margin-left: 25px;padding-left: 10px;height: 27px;padding-top: 6px;'>장기과속 "+overSpeedLong+" 회</div>";
									
									strIconStr += "</li>";
								}
								strIconStr += "</ul></div>"; 
								
								
							}
							
							//var label = new Tmap.Label(strIconStr);
							var marker = new google.maps.Marker({
								position: new google.maps.LatLng(arrTrackRecord[i].lat,arrTrackRecord[i].lon),
							    animation : google.maps.Animation.BOUNCE,
							    icon:icon
							});
							marker.strInfoHtml	= strIconStr;				
							tClassObj.arrPoiMarker.push(marker);
							//create complate poiMarkers
						}
					}
					
				}
			}
    		
    		var drawMap = function(){
				for(var i = 0 ; i < tClassObj.arrPoiMarker.length ; i++){
					const marker = tClassObj.arrPoiMarker[i];
					
					marker.setMap(settings.objMap);
					
					if(alwaysPopup) {
						if(marker.strInfoHtml){
							infowindow = new google.maps.InfoWindow({
							   content: marker.strInfoHtml
							});	
							infowindow.open(settings.objMap, marker);
						}						
					}else{
						if(marker.strInfoHtml){							
							marker.addListener('mouseover', function() {
								infowindow = new google.maps.InfoWindow({
								   content: marker.strInfoHtml
								});
							    infowindow.open(settings.objMap, marker);
							});	
							marker.addListener('mouseout', function() {
							    infowindow.close();
							});
						}
					}
					
					if(settings.searchType == 'vehicle'){
						marker.addListener("click", function(o){
							
							$.extend(_historyBackOption = {}, _o );
							
							_o.searchType = 'time';
							if(marker.vehicleKey){
								_o.vehicleKey = marker.vehicleKey;
								_o.searchTime = _o.lastestTime?_o.lastestTime*3600*1000:3600*24*1000;
								outerReset();
							}
						});
					}
					
				}

				for(var i = 0 ; i < tClassObj.startMarker.length ; i++)
					tClassObj.startMarker[i].setMap(settings.objMap);
				for(var i = 0 ; i < tClassObj.endMarker.length ; i++)
					tClassObj.endMarker[i].setMap(settings.objMap);
			
				if(tClassObj.vector.length > 0) drawLine();
								
			}
    		
    		var drawLine = function(){
    			for( i=0; i< tClassObj.vector.length ; i++)
    			{
    				tClassObj.vector[i].setMap(settings.objMap);
    			}
    			
    		}
    		
    		var calcCenter = function(){
    			for(var z = 0 ; z < settings.objData.length ; z++){
					var d = settings.objData[z];
					
					if(settings.searchType == 'vehicle'){
						var vo = d;
						var location = getProperty(vo,"latestLocation.location");
						var lat = location.split(",")[1];
						var lon = location.split(",")[0];
						setMaxPoint(lat,lon);
						setMinPoint(lat,lon);
					}else{
						var vo = d;
						
						var arrTrackRecord = [];
						
						if(vo.tripMatched == '1') arrTrackRecord = vo.matchedTrackRecord;
						else arrTrackRecord = vo.trackRecord;
						
						var arrTmapPoint = [];
						var tempStartPoiList = [];
						
						for(var i = 0 ; i <arrTrackRecord.length ; i++){
							if(!arrTrackRecord[i].gpsAccuracyOut){
								setMaxPoint(arrTrackRecord[i].lat,arrTrackRecord[i].lon);
								setMinPoint(arrTrackRecord[i].lat,arrTrackRecord[i].lon);
							}
						}
						
					    setMaxPoint(vo.startLat,vo.startLon);
					    setMinPoint(vo.startLat,vo.startLon);
					    setMaxPoint(vo.endLat,vo.endLon);
					    setMinPoint(vo.endLat,vo.endLon);
					}
				}
    			
    			
    		}
    		
    		var setCenter = function(){
    			
    			var centerLat = (objMaxPoint.lat+objMinPoint.lat)/2;
    			var centerLon = (objMaxPoint.lon+objMinPoint.lon)/2;
    			
				settings.objMap.setCenter({lat: centerLat, lng: centerLon});
    			
    			objCenter.lat = centerLat;
    			objCenter.lon = centerLon;
    			    			
    		}
    		
			var setZoom = function(){
    			var longDistance = moreLongXY(objMaxPoint,objMinPoint);
    			var distance = calcDistance(objMaxPoint,objMinPoint);
    			var defaultUnit = 1113200; // 1degre = 111.32km 
    			var unitDistance = longDistance.val*defaultUnit;
    			
    			var devisionVal = 1000;
    			devisionVal = (longDistance.target == "Y")?devisionVal*1.25:devisionVal;
    			var zoomLev = 0;
    			for(var i = 0 ; i < meters_per_pixel_ary.length ; i++){
    				if(meters_per_pixel_ary[i]*devisionVal < unitDistance){
    					zoomLev = i+1;
    					break;
    				}
    			}
    			if(zoomLev < 0) zoomLev = 0;
    			settings.objMap.setZoom(zoomLev);
    		}
			
			var setMaxPoint = function(lat,lon){
				if(objMaxPoint.lat < lat) objMaxPoint.lat = lat;
				if(objMaxPoint.lon < lon) objMaxPoint.lon = lon;
			}
			var setMinPoint = function(lat,lon){
				if(objMinPoint.lat > lat) objMinPoint.lat = lat;
				if(objMinPoint.lon > lon) objMinPoint.lon = lon;
			}
			var calcDistance = function(maxPoint,minPoint){
				
				var disX = maxPoint.lon - minPoint.lon;
				var disY = maxPoint.lat - minPoint.lat;

				return Math.sqrt(Math.abs(disX*disX) + Math.abs(disY*disY));
			}
			var moreLongXY = function(maxPoint,minPoint){
				
				var disX = maxPoint.lon - minPoint.lon;
				var disY = maxPoint.lat - minPoint.lat;
				disX = disX*0.75; //화면비 4:3 예상
				if(Math.abs(disX) > Math.abs(disY)) return {target : "X" ,val : Math.abs(disX)};
				else return {target : "Y" , val : Math.abs(disY)};
			}
    		
    		initialize();
			if(settings.searchType=='default') return $(this);
			
			var tClassObj = {};
			tClassObj.vector = [];
			tClassObj.arrPoiMarker = [];
			tClassObj.startMarker = [];
			tClassObj.endMarker = [];
			
			settings.objData = getData();
			if(settings.objData && settings.objData.length > 0){
				createGeoData();
				if(settings.searchType!='vehicle') createPoiData();
				drawMap();
				calcCenter();
				setZoom();
				setCenter();
				window.parent&&window.parent.outerSuddenSummaryCall&&window.parent.outerSuddenSummaryCall(outerSuddenList);	
			}
    	});
    }
    
    
}); 

function init(){
	_objMap = $("#map_div").googleMapLoader(_o);
}

function outerReset(){
	$.googleMapLoader.reset(_objMap,_o);
}
function getLoadComplate(){
	return loadComplate;
}
</script>

<body>
	<div id="map_div"></div>
</body>
</html>
