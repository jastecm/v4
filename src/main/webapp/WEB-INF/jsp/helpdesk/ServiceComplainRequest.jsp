<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script src='${pageContext.request.contextPath}/common/js/jquery.MultiFile.js' type="text/javascript" language="javascript"></script>
<script type="text/javascript">
var pageUrl = _defaultPath+"/driving/searchAccident.do";

$(document).ready(function(){
	initMenuSel("M6003");
	
	$('#frm').ajaxForm({
		beforeSubmit: function (data,form,option) {
			var returnVal = submitContents();
			$("#editor1").val(returnVal);
			for(var i = 0 ; i < data.length ; i++){
				if(data[i].name=="contents") data[i].value=returnVal;
			}
			
			if($.trim($("#frm input[name='title']").val()).length == 0) {alert("제목을 입력해주세요."); return false;}
			else if($.trim($("#editor1").val()) == '<br>') {alert("내용을 입력해주세요."); return false;}
			else return true;
			
	    	return true;
		}
		,success: function(res,status){
	    	
	    	if(res.rtvCd == "1"){
	    		alert("저장되었습니다.");
				document.location.href = "${defaultPath}/helpdesk/serviceComplain.do";
	    	}else{
	    		alert("error");
	    	}
		},
		error: function(){
	    	//에러발생을 위한 code페이지
		}                               
	});
	
	$("#frm").submit(function(){return false});
	
	$("#btn_save").on("click",function(){
		$('#frm').submit();
	});
});








</script>
<div class="right_layout">
	<div class="board_layout pat0">
		<div class="detail_view">
			<a href="javascript:window.history.back();" class="btn_close"><img src="${pathCommon}/images/close.jpg" alt="" /></a>
			<h2 class="h2tit">서비스 불편신고</h2>
				<form id="frm" method="POST" action="${defaultPath }/helpdesk/serviceComplainSave.do" enctype="multipart/form-data">
				<c:if test="${!empty rtv}">
				<input type="hidden" name="boardKey" value="${rtv.boardKey }" />
				</c:if>
				<div class="sub_top2">
					불편한 사항이나 오류, 개선, 제안에 관한 사항을 알려주세요. 보다 안정된 서비스가 될 수 있도록 최선을 다하겠습니다.
				</div>
				<div class="repair_box2_1 pal25 borB1">
	            	<span class="tit">제목</span>
	               	<input type="text" name="title" style="width:600px;" <c:if test="${!empty rtv }">readonly value="${rtv.title }"</c:if>/><br><br>
	            </div>
	            <c:if test="${!empty rtv.fileId}">
					<div class="date">
						<a href="${defaultPath}/com/getFile.do?uuid=${rtv.fileId}"><b>[첨부파일]</b> &nbsp; ${rtv.orgFileNm }          &nbsp; &nbsp; <img src="${pageContext.request.contextPath}/common/images/btn6.jpg" alt="" /></a>
					</div>
				</c:if>
	            <c:if test="${!empty rtv }">
		            <div class="con3">
						${rtv.contents }
					</div>
				</c:if>
				<div class="bo_textarea">
					<c:import url="/editor/smartEditor.jsp">
						<c:param name="contents" value=""></c:param>
					</c:import>
				</div>
				<c:if test="${empty rtv }">
					<div class="car_info4 car_info5_3">
						<span class="tit">&nbsp;&nbsp;첨부파일</span><div class="car_info4_1">
							<div class="car_info4_2">
								<span class="tit3">파일추가</span><fieldset style="border:0">
									<form action="" class="P10">
										<div class="btn_file"><input type="file" name="fileId" class="multi" maxlength="1"/></div>
									</form>
								</fieldset>
							</div>
						</div>
					</div>
				</c:if>
			</form>
			<div class="btn_area2"><a href="#none" class="btn_cancle">취소</a><a href="#none" id="btn_save">확인</a></div>
		</div>
	</div>
</div>