<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script src='${pageContext.request.contextPath}/common/js/jquery.MultiFile.js' type="text/javascript" language="javascript"></script>
<script type="text/javascript">
var pageUrl = _defaultPath+"/driving/searchAccident.do";
var lazyVehicle;
$(document).ready(function(){
	initMenuSel("M6004");
	
	$(".btn_close").on("click",function(){
		$( "#dialogVehicle" ).dialog("destroy");
	});
	
	$('#frm').ajaxForm({
		beforeSubmit: function (data,form,option) {
			var returnVal = submitContents();
			$("#editor1").val(returnVal);
			for(var i = 0 ; i < data.length ; i++){
				if(data[i].name=="contents") data[i].value=returnVal;
			}
			if($.trim($("#editor1").val()) == '<br>') {alert("내용을 입력해주세요."); return false;}
			else if($.trim($("#adDevice").val()).length == 0 || $.trim($("#asVehicle").val()).length == 0) {alert("차량,단말을 선택해주세요."); return false;}
			else return true;
		}
		,success: function(res,status){
	    	
	    	if(res.rtvCd == "1"){
	    		alert("저장되었습니다.");
				document.location.href = "${defaultPath}/helpdesk/afterService.do";
	    	}else{
	    		alert("error");
	    	}
		},
		error: function(){
	    	//에러발생을 위한 code페이지
		}                               
	});
	
	$("#frm").submit(function(){return false;});
	
	$("#btn_save").on("click",function(){
		$('#frm').submit();
	});
	
	
	$("body").on("click",".btn_car_search",function(){
		if(lazyVehicle)
			$.lazyLoader.search(lazyVehicle);
		
		$( "#dialogVehicle" ).dialog({
	        width:800,
	        dialogClass:'alert',
	        draggable: false,
	        modal:true,
	        height:"auto",
	        resizable:false,
	        closeOnEscape:true,
	        buttons:{
	        	'취소':function(){
         			$(this).dialog("close");
         		}
	            ,'확인':function(){
	            	var selObj = $('.selPopObj:checked',$(this));
	            	if(selObj.length == 0)
	            		alert("차량이 선택되지 않았습니다.");
	            	else{
        				var vehicleKey = selObj.data("vehiclekey");	  
        				var plateNum = selObj.data("platenum");	  
        				var deviceKey = selObj.data("devicekey");	  
        				var deviceSeries = selObj.data("deviceseries");	  
        				var deviceSn = selObj.data("devicesn");	  
	    				
	    				$("#adDevice").val(deviceKey);
	    				$("#asVehicle").val(vehicleKey);
	    				$("#plateNum").after(plateNum);
	    				$("#deviceSeries").after(deviceSeries);
	    				$("#deivceSn").after(deviceSn);
	    					
	            		$(this).dialog("close");
	            	}
	            	
	            }
	        }
	    });
		
		if(!lazyVehicle)
			lazyVehicle = $("#dialogVehicle table").lazyLoader({
				searchFrmObj : $("#popupSearchFormVehicle")
				,searchFrmVal : {searchOrder:$("#popupSearchFormVehicle input[name=searchOrder]")
								,searchType:$("#popupSearchFormVehicle input[name=searchType]")
								,searchText:$("#popupSearchFormVehicle input[name=searchText]")
				}
				,scrollRow : 4
				,rowHeight : 80
				,limit : 5
				,loadUrl : "/com/getVehicleList.do"
				,initSearch : true
				,createDataHtml : function(r){
					var data = r.rtvList;
					var strHtml = "";
					for(var i = 0 ; i < data.length ; i++){
						var vo = data[i];
						
						var used = "";
						if(vo.deviceId.length>0)
							used = '1';
						else
							used = '0';
						
						strHtml += "<tr>";
						strHtml += '<td><input type="radio" class="selPopObj" data-vehiclekey="'+vo.vehicleKey
						+'" data-vehiclekey="'+vo.vehicleKey
						+'" data-platenum="'+vo.plateNum
						+'" data-devicekey="'+vo.deviceKey
						+'" data-deviceseries="'+vo.deviceSeries
						+'" data-devicesn="'+vo.deviceSn
						+'"/></td>';
						strHtml += '<th>';
						strHtml += '<strong class="car_img"><img src="${path}/common/images/vehicle/'+vo.imgS+'.png" alt="" /></strong>';
						strHtml += '<a href="#none">';
						strHtml += '<span>'+vo.modelMaster+'</span>';
						strHtml += vo.plateNum;
						strHtml += '</a>';
						strHtml += '</th>';
						strHtml += '<td><div class="start_end txt_left">';
						strHtml += '<p>'+vo.year+'</p>';
						strHtml += '<p>'+number_format((vo.totDistance/1000).toFixed(1))+'</p>';
						strHtml += '</div></td>';
						strHtml += '<td><div class="start_end txt_left">';
						strHtml += '<p>'+vo.deviceSeries+'</p>';
						strHtml += '<p>'+vo.deviceSn+'</p>';
						strHtml += '</div></td>';
						if(vo.deviceId.length>0)
							strHtml += '<td>사용중</td>';
						else
							strHtml += '<td>사용중지</td>';
						strHtml += '</tr>';	
					}
					
					return strHtml;
				}
			});
	});	
	
	
});
</script>
<div class="right_layout">
	<div class="board_layout pat0">
		<div class="detail_view">
			<a href="javascript:window.history.back();" class="btn_close"><img src="${pathCommon}/images/close.jpg" alt="" /></a>
			<h2 class="h2tit">제품 A/S접수</h2>
			<form id="frm" method="POST" action="${defaultPath }/helpdesk/afterServiceSave.do" enctype="multipart/form-data">
				<c:if test="${!empty rtv}">
				<input type="hidden" name="boardKey" value="${rtv.boardKey }" />
				</c:if>
				<div class="sub_top2">
					접수 후, 신속하게 처리될 수 있도록 하겠습니다. 불편을 끼쳐 드려 죄송합니다.
				</div>
				<div class="input_area1">
					<input type="hidden" name="asDevice" id="adDevice" value="${rtv.asDevice }" />
					<input type="hidden" name="asVehicle" id="asVehicle" value="${rtv.asVehicle}"/>
					<span class="s1">
						<b id="plateNum">차량번호 : </b><c:if test="${!empty rtv }">${rtv.plateNumber }</c:if>
					</span><span class="s2">
						<b id="deviceSeries">단말타입 : </b><c:if test="${!empty rtv }">${rtv.deviceSeries }</c:if>
					</span><span class="s3">
						<b id="deivceSn">단말시리얼 : </b><c:if test="${!empty rtv }">${rtv.deivceSn }"</c:if><c:if test="${empty rtv }"><a href="#none" class="btn_car_search" style="margin-left:30px;"><img src="${pathCommon}/images/btn_search2.jpg" alt=""></a></c:if>
					</span>
				</div>
				<c:if test="${!empty rtv.fileId}">
					<div class="date">
						<a href="${defaultPath}/com/getFile.do?uuid=${rtv.fileId}"><b>[첨부파일]</b> &nbsp; ${rtv.orgFileNm }          &nbsp; &nbsp; <img src="${pageContext.request.contextPath}/common/images/btn6.jpg" alt="" /></a>
					</div>
				</c:if>
				<c:if test="${!empty rtv }">
		            <div class="con3">
						${rtv.contents }
					</div>
				</c:if>
				<div class="bo_textarea">
					<c:import url="/editor/smartEditor.jsp">
						<c:param name="contents" value=""></c:param>
					</c:import>
				</div>
				<c:if test="${empty rtv }">
					<div class="car_info4 car_info5_3">
						<span class="tit">&nbsp;&nbsp;첨부파일</span><div class="car_info4_1">
							<div class="car_info4_2">
								<span class="tit3">파일추가</span><fieldset style="border:0">
									<form action="" class="P10">
										<div class="btn_file"><input type="file" name="fileId" class="multi" maxlength="1"/></div>
									</form>
								</fieldset>
							</div>
						</div>
					</div>
				</c:if>
			</form>
			<div class="btn_area2"><a href="javascript:window.history.back(-2);" class="btn_cancle">취소</a><a href="#none" id="btn_save">확인</a></div>
		</div>
	</div>
</div>
<div id="dialogVehicle" title="차량 선택" class="myPopup" style="display: none">
	<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt=""></a>		
	<div class="p_relative">
		<form id='popupSearchFormVehicle' onSubmit="return false;">
			<input type="hidden" name="searchType" value="plateNum"/>
			<div class="tab_select">
				<select class="select form" name="searchOrder" style="height:22px;">
					<option value="regDate">최근 등록순</option>
				</select>
			</div>
			<strong class="search_area">
				<input type="text" placeholder="직접검색" id="input_searchUserPop" name="searchText" class='btn_searchUserPopText'/><a href="#none" class="btn_searchUserPop" data-e="vehicle"><img src="${pathCommon}/images/btn_search2.jpg" alt=""></a>
			</strong>
		</form>
	</div>
	<table class="table1 table1_1">
		<colgroup>
			<col width="60">
			<col width="170">
			<col width="150">
			<col width="*">
			<col width="80">
		</colgroup>
		<thead>
			<tr>
				<th>선택</th>
				<th>차량정보</th>
				<th>년식/주행거리</th>
				<th>단말</th>
				<th>사용여부</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>