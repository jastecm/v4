<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script type="text/javascript">
var lazy;

$(document).ready(function(){
	initMenuSel("M6002");
	
	$("#searchType").on("change",function(){
		$("#searchFrm input[name=contentsType]").val($(this).val());
		$.lazyLoader.search(lazy);
	});
	
	$(".btn_close").on("click",function(){
		$( "#pop" ).dialog("destroy");
	});
	
	$("body").on("click",".btn_view",function(){
		var id = $(this).data("id");
		if(id.length == 0) return;
		else{
			
			$VDAS.http_post("/helpdesk/board/2/"+id,{},{
				success : function(r){
					var vo = r.result;
					console.log(r.result);
					$( "#pop" ).attr("title",vo.title);
					$("#pop .contents").html(vo.contents);
					if(vo.fileId){
						$("#pop .info").html('첨부파일 : <a href="${defaultPath}/com/getFile.do?uuid='+vo.fileId+'">'+vo.orgFileNm+'</a>').show();
					}else{
						$("#pop .info").html("").hide();
					}
					
					
					$( "#pop" ).dialog({
				        width:1000,
				        dialogClass:'alert',
				        draggable: false,
				        modal:true,
				        height:"auto",
				        resizable:false,
				        closeOnEscape:true,
				        buttons:{
				        	'확인':function(){
				     			$(this).dialog("destroy");
				     		}
				        }
				    });
					
					
				}
			});
		}		
	});
	
	
	
	if(!lazy)
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
			}
			,scrollRow : 8
			,rowHeight : 50
			,limit : 10			
			,loadUrl : "/helpdesk/board/2"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.result;				
				var strHtml = '';
				
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += '<tr>';
					strHtml += '<td>'+vo.rNum+'</td>';					
					strHtml += '<th>'+vo.contentTypeNm+'</th>';					
					strHtml += '<th><a href="#none" class="btn_view" data-id="'+vo.boardKey+'">'+vo.title+'</a></th>';					
					strHtml += '<td>'+vo.readCount+'</td>';					
					strHtml += '</tr>';
				}
				
				return strHtml;

			}
		});
});








</script>
<div class="right_layout">
	<form id="searchFrm">
		<input type="hidden" name="boardType" value="1" />
		<input type="hidden" name="contentsType" value="" />
		<input type="hidden" name="serviceType" value="1" />
	</form>
	<h2 class="tit_sub">
		FAQ
		<p>고객님이 자주 묻는 질문과 답변이 있습니다.</p>
	</h2>
	<div class="board_layout" style="padding-top:0px">
		<h3 class="h3tit">FAQ목록</h3>
		<table class="table1 mgt10" id="contentsTable">
			<colgroup>
				<col width="65">
				<col width="143">
				<col width="*">
				<col width="80">
			</colgroup>
			<thead>
				<tr>
					<th>No.</th>
					<th>
						<select class="select form" id="searchType">
							<option value="">분류</option>
							<c:forEach var="vo" items="${contentsType }">
								<option value="${vo.code}">${vo.codeName }</option>	
							</c:forEach>																	
						</select>
					</th>
					<th>제목</th>
					<th>조회</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<div id="pop" title="" class="myPopup" style="display: none">
	<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt=""></a>
	<div class="info"></div>		
	<div class="contents">
	</div>
</div>
<style>
#pop .info{width: 100%;height: 30px;border-bottom: 1px solid #747474;margin-bottom: 20px;display:none}
#pop .contents div,p{line-height: 18px; font-size: 13px;} 
</style>
    
