<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script type="text/javascript">
var lazy;

$(document).ready(function(){
	initMenuSel("M6003");
	
	$("#searchType").on("change",function(){
		$("#searchFrm input[name=contentsType]").val($(this).val());
		$.lazyLoader.search(lazy);
	});
	
	$(".btn_close").on("click",function(){
		$( "#pop" ).dialog("destroy");
	});
	
	$("body").on("click",".btn_view",function(){
		var id = $(this).data("id");
		if(id.length == 0) return;
		else{
			
			$VDAS.http_post("/helpdesk/board/3/"+id,{},{
				success : function(r){
					var vo = r.result;
					console.log(r.result);
					$( "#pop" ).attr("title",vo.title);
					$("#pop .contents").html(vo.contents);
					
					if(vo.contentsReple&&vo.contentsReple.length != 0)
						$("#pop .contentsReple").html(vo.contentsReple).show();
					else $("#pop .contentsReple").html("").hide();
					
					if(vo.fileId){
						$("#pop .info").html('첨부파일 : <a href="${defaultPath}/com/getFile.do?uuid='+vo.fileId+'">'+vo.orgFileNm+'</a>').show();
					}else{
						$("#pop .info").html("").hide();
					}
					
					$( "#pop" ).dialog({
				        width:1000,
				        dialogClass:'alert',
				        draggable: false,
				        modal:true,
				        height:"auto",
				        resizable:false,
				        closeOnEscape:true,
				        buttons:{
				        	'확인':function(){
				     			$(this).dialog("destroy");
				     		}
				        }
				    });
					
					
				}
			});
		}		
	});
	
	
	
	if(!lazy)
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
			}
			,scrollRow : 8
			,rowHeight : 50
			,limit : 10			
			,loadUrl : "/helpdesk/board/3"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.result;				
				var strHtml = '';
				
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += '<tr>';
					strHtml += '<td>'+vo.rNum+'</td>';					
					strHtml += '<th><a href="#none" class="btn_view" data-id="'+vo.boardKey+'">'+vo.title+'</a></th>';
					strHtml += '<td>'+vo.regDate.substr(0,10)+'</td>';
					
					var strState = "";
					var strStateNm = "";
					if(vo.contentsReple&&vo.contentsReple.length != 0) strState = "re_ok";
					else strState = "re_ready";
					if(vo.contentsReple&&vo.contentsReple.length != 0) strStateNm = "답변완료";					
					else strStateNm = "답변대기";
					strHtml += '<td><span class="'+strState+'">'+strStateNm+'</span></td>';
					strHtml += '</tr>';
				}
				
				return strHtml;

			}
		});
});








</script>
<div class="right_layout">
	<form id="searchFrm">
		<input type="hidden" name="boardType" value="1" />
		<input type="hidden" name="contentsType" value="" />
		<input type="hidden" name="serviceType" value="1" />
	</form>
	<h2 class="tit_sub">
		서비스 불편신고
		<p>서비스 불편신고 등록을 하거나 신고에 대한 답변을 확인 할 수 있습니다.</p>
	</h2>
	<h3 class="h3tit">서비스 불편신고</h3>
	<div class="box1">
		서비스를 이용하면서 겪은 불편사항, 장애, 오류 및 서비스에 대한 개선방안, 제안 등을 등록 합니다.
		<a href="${defaultPath}/helpdesk/serviceComplainRequest.do">서비스 불편신고</a>
	</div>
	<div class="board_layout" style="padding-top:0px">
		<h3 class="h3tit">나의 불편신고</h3>
		<table class="table1 mgt10" id="contentsTable">
			<colgroup>
				<col width="65">
				<col width="*">
				<col width="112">
				<col width="80">
			</colgroup>
			<thead>
				<tr>
					<th>No.</th>
					<th>제목</th>
					<th>작성일</th>
					<th>답변</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<div id="pop" title="" class="myPopup" style="display: none">
	<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt=""></a>
	<div class="info"></div>		
	<div class="contents"></div>
	<div class="contentsReple"></div>
</div>
<style>
#pop .info{width: 100%;height: 30px;border-bottom: 1px solid #747474;margin-bottom: 20px;display:none}
#pop .contents p{line-height: 18px; font-size: 13px;} 
#pop .contents div{line-height: 18px; font-size: 13px;} 
#pop .contentsReple{border-top: 1px solid #747474;display:none;padding-top:20px;} 
#pop .contentsReple div{line-height: 18px; font-size: 13px} 
#pop .contentsReple p{line-height: 18px; font-size: 13px} 
</style>
    
