<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script type="text/javascript">
var pageUrl = _defaultPath+"/driving/searchAccident.do";

$(document).ready(function(){
	
});








</script>
<div class="right_layout">
	<h1 class="title1"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/title2_txt1.jpg" alt="" /></h1>
	<div class="join_top">
		<img src="${pageContext.request.contextPath}/common/images/${_LANG}/join_txt4.png" alt="" />
	</div>
	<h1 class="title2"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/join_h1_4.jpg" alt=""></h1>
	<div class="join_complete">
		<div class="img">
			<img src="${pageContext.request.contextPath}/common/images/join_complete.jpg" alt="" />
		</div>
		<div class="j_txt1">
			${reqVo.corpNm }님! 환영합니다.<br>
			지금 업무차량을 등록하시겠습니까?<br><br>
			위 정보등록은 서비스 이용전까지 등록하셔도 됩니다.
		</div>
		<div class="join_txt3">
			ㆍ서비스의 정상적 이용을 위해서는 ViewCAR차량단말(VID)이 필요하며 동 단말은 <a href="http://item.gmarket.co.kr/Item?goodscode=1105160936&pos_shop_cd=SH&pos_class_cd=111111111&pos_class_kind=T&keyword_order=%ba%e4%c4%ab+%c7%c1%b7%ce&keyword_seqno=13111705236&search_keyword=%ba%e4%c4%ab+%c7%c1%b7%ce" target="_blank">여기</a>를 누르시면 관련 구매정보를 보실 수 있습니다.
		</div>
	</div>
	<div class="btn_area2"><a href="${pageContext.request.contextPath}/main/index.do">메인화면 이동</a></div>
</div>