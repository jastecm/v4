<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script type="text/javascript">
var pageUrl = _defaultPath+"/regist/memberInfo.do";

$(document).ready(function(){
	
	$("#btn_back").on("click",function(){
		$VDAS.submit_post("/regist/join.do",$("#frm"));
	});
	$("#btn_next").on("click",function(){
		$VDAS.submit_post("/regist/joinComplate.do",$("#frm"));
	});
});


</script>
<form id="frm" style="display:none" method="post">
	<input type="text" name="accountId" value="${reqVo.accountId }" />
	<input type="text" name="accountIdMail" value="${reqVo.accountIdMail }" />
	<input type="text" name="accountIdF" value="${reqVo.accountIdF }" />
	<input type="text" name="accountMail" value="${reqVo.accountMail }" />
	<input type="text" name="accountMailDomain" value="${reqVo.accountMailDomain }" />
	<input type="text" name="accountMailF" value="${reqVo.accountMailF }" />
	<input type="text" name="corpNum1" value="${reqVo.corpNum1 }" />
	<input type="text" name="corpNum2" value="${reqVo.corpNum2 }" />
	<input type="text" name="corpNum3" value="${reqVo.corpNum3 }" />
	<input type="text" name="corpNum" value="${reqVo.corpNum }" />
	<input type="text" name="corpPhone" value="${reqVo.corpPhone }" />
	<input type="text" name="corpPhone1" value="${reqVo.corpPhone1 }" />
	<input type="text" name="corpPhone2" value="${reqVo.corpPhone2 }" />
	<input type="text" name="corpPhone3" value="${reqVo.corpPhone3 }" />
	<input type="text" name="mobilePhone1" value="${reqVo.mobilePhone1 }" />
	<input type="text" name="mobilePhone2" value="${reqVo.mobilePhone2 }" />
	<input type="text" name="mobilePhone3" value="${reqVo.mobilePhone3 }" />
	<input type="text" name="mobilePhone" value="${reqVo.mobilePhone }" />
	<input type="text" name="phone1" value="${reqVo.phone1 }" />
	<input type="text" name="phone2" value="${reqVo.phone2 }" />
	<input type="text" name="phone3" value="${reqVo.phone3 }" />
	<input type="text" name="phone" value="${reqVo.phone }" />
	<input type="text" name="accountPw" value="${reqVo.accountPw }" />
	<input type="text" name="accountPwConfirm" value="${reqVo.accountPwConfirm }" />
	<input type="text" name="corpNm" value="${reqVo.corpNm }" />
	<input type="text" name="ceoNm" value="${reqVo.ceoNm }" />
	<input type="text" name="corpBusiness" value="${reqVo.corpBusiness }" />
	<input type="text" name="corpItem" value="${reqVo.corpItem }" />
	<input type="text" name="userNm" value="${reqVo.userNm }" />
	<input type="text" name="corpGroupNm" value="${reqVo.corpGroupNm }" />
	<input type="text" name="serviceAccepted" value="${reqVo.serviceAccepted }" />
	<input type="text" name="privateAccepted" value="${reqVo.privateAccepted }" />
	<input type="text" name="locationAccepted" value="${reqVo.locationAccepted }" />
	<input type="text" name="marketingAccepted" value="${reqVo.marketingAccepted }" />
</form>
<div class="right_layout">
	<div class="tit_sub">
		회원가입
	</div>
	<div class="join_top">
		<img src="${pathCommon}/images/${_LANG}/join_txt3.png" alt=""/>
	</div>
	<h2 class="h2tit">회원정보 확인</h2>
	<p class="join_txt1">스마트드라이빙! ViewCAR 차량운행관리 서비스에 입력하신 정보가 정확하게 기재되었는지 확인해주세요.</p>
	<h3 class="h3tit">기본정보</h3>
	<div class="join_box2">
		<div class="join_box2_1">
			<div class="join_box2_2">로그인 정보 <span>(필수입력)</span></div>
			<ul>
				<li>
					<span class="t1">아이디</span> <span>${reqVo.accountIdF }</span>
				</li>
				<li>
					<span class="t1">보조 이메일</span> <span>${reqVo.accountMailF }</span>
				</li>
			</ul>
		</div>
		<div class="join_box2_1">
			<div class="join_box2_2">기업 정보 <span>(필수입력)</span></div>
			<ul>
				<li>
					<span class="t1">기업명</span> <span class="size165">${reqVo.corpNm }</span>
					<span class="t2">사업자등록번호</span> <span>${reqVo.corpNum }</span>
				</li>
				<li>
					<span class="t1">대표자명</span> <span class="size165">${reqVo.ceoNm }</span>
					<span class="t2">연락처</span> <span>${reqVo.corpPhone }</span>
				</li>
			</ul>
		</div>
		<div class="join_box2_1">
			<div class="join_box2_2">부가정보 (선택입력)</div>
			<ul>
				<li class="borB0">
					<span class="t1">업태</span> <span class="size165">${reqVo.corpBusiness }</span>
					<span class="t2">종목</span> <span>${reqVo.corpItem }</span>
				</li>
			</ul>
		</div>
	</div>
	<h3 class="h3tit">관리자 정보</h3>
	<div class="join_box2">
		<div class="join_box2_1">
			<div class="join_box2_2">기본 정보 <span>(필수입력)</span></div>
			<ul>
				<li>
					<span class="t1">관리자명</span> <span>${reqVo.userNm }</span>
				</li>
				<li>
					<span class="t1">휴대폰 번호</span> <span>${reqVo.mobilePhone }</span>
				</li>
				<li>
					<span class="t1">비상 연락처</span> <span>${reqVo.phone }</span>
				</li>
			</ul>
		</div>
		<div class="join_box2_1">
			<div class="join_box2_2">부가 정보 (선택입력)</div>
			<ul>
				<li class="borB0">
					<span class="t1">부서명</span> <span>${reqVo.corpGroupNm }</span>
				</li>
			</ul>
		</div>
	</div>
	<div class="btn_area2"><a href="#none" class="btn_cancle" id="btn_back">이전단계</a><a href="#none" id="btn_next">가입완료</a></div>
	<%--
	${reqVo.accountId }@${reqVo.accountIdMail }
	${reqVo.accountMail }@${reqVo.accountMailDomain }
	${reqVo.corpNum1 }-${reqVo.corpNum2 }-${reqVo.corpNum3 }
	${reqVo.corpPhone1 }-${reqVo.corpPhone2 }-${reqVo.corpPhone3 }
	${reqVo.mobilePhone1 }-${reqVo.mobilePhone2 }-${reqVo.mobilePhone3 }
	${reqVo.phone1 }-${reqVo.phone2 }-${reqVo.phone3 }
	${reqVo.accountPw }
	${reqVo.accountPwConfirm }
	${reqVo.serviceAccepted }
	${reqVo.privateAccepted }
	${reqVo.locationAccepted }
	${reqVo.marketingAccepted }
	<h1 class="title1"><img src="${pageContext.request.contextPath}/common/images/title2_txt1.jpg" alt="${_C.label1}" /></h1>
	<div class="join_top">
		<img src="${pageContext.request.contextPath}/common/images/join_txt3.png" alt="" />
	</div>
	<h1 class="title2"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/join_h1_3.jpg" alt="${_C.label2}"></h1>
	<p class="join_txt1">${_C.label3}</p>
	<div class="join_txt2"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/join_txt4.jpg" alt="${_C.label4}" /></div>
	<div class="join_box2">
		<div class="join_box2_1">
			<div class="join_box2_2"><span>${_C.label5}</span></div>
			<ul>
				<li>
					<span>http://vdas.viewcar.co.kr/</span><span class="input_ok">${reqVo.corpDomain }</span>
				</li>
				<li>
					<b>${_C.label6}</b>
				</li>
			</ul>
		</div>
	</div>
	<div class="join_txt2"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/join_txt5.jpg" alt="${_C.label7}" /></div>
	<div class="join_box2">
		<div class="join_box2_1">
			<div class="join_box2_2"> <span>${_C.label8}</span></div>
			<ul>
				<li>
					<span class="t1">${_C.label9}</span>${reqVo.userId }
				</li>
				<li>
					<span class="t1">${_C.label10}</span>*********
				</li>
			</ul>
		</div>
		<div class="join_box2_1">
			<div class="join_box2_2"><span>${_C.label11}</span></div>
			<ul>
				<li>
					<span class="t1">${_C.label12}</span><span class="w151">${reqVo.corpNm }</span><span class="t2">${_C.label13}</span>${reqVo.corpNum1 }-${reqVo.corpNum2 }-${reqVo.corpNum3 }
				</li>
				<li>
					<span class="t1">${_C.label14}</span><span class="w151">${reqVo.ceoNm }</span><span class="t2">${_C.label15}</span>${reqVo.corpPhone1 }-${reqVo.corpPhone2 }-${reqVo.corpPhone3 }
				</li>
			</ul>
		</div>
		<div class="join_box2_1">
			<div class="join_box2_2">${_C.label16}</div>
			<ul>
				<li class="borB0">
					<c:if test="${reqVo.allocationUsed eq '1' }">${_C.label17}</c:if>
					<c:if test="${reqVo.allocationUsed eq '0' }">${_C.label18}</c:if>
				</li>
			</ul>
		</div>
		<div class="join_box2_1">
			<div class="join_box2_2">${_C.label19}</div>
			<ul>
				<li class="borB0">
					<span class="t1">${_C.label20}</span><span class="w151">${reqVo.corpBusiness }</span><span class="t2">${_C.label21}</span>${reqVo.corpItem }
				</li>
			</ul>
		</div>
	</div>
	<div class="join_txt2"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/join_txt6.jpg" alt="${_C.label22}" /></div>
	<div class="join_box2">
		<div class="join_box2_1">
			<div class="join_box2_2"><span>${_C.label23}</span></div>
			<ul>
				<li>
					<span class="t1">${_C.label24}</span>${reqVo.userNm }
				</li>
				<li>
					<span class="t1">${_C.label25}</span><span class="w151">${reqVo.phone1 }-${reqVo.phone2 }-${reqVo.phone3 }</span><span class="t4">${_C.label25}</span>${reqVo.mobilePhone1 }-${reqVo.mobilePhone2 }-${reqVo.mobilePhone3 }
				</li>
			</ul>
		</div>
		<div class="join_box2_1">
			<div class="join_box2_2">${_C.label19}</div>
			<ul>
				<li class="borB0">
					<span class="t1">${_C.label26}</span>${reqVo.corpGroupNm }
				</li>
			</ul>
		</div>
	</div>
	<div class="join_txt3">
		ㆍ${_C.label27}<br>
		ㆍ${_C.label31} <a href="#none" target="_blank">${_C.label32}</a>${_C.label33}
	</div>
	 --%>
</div>