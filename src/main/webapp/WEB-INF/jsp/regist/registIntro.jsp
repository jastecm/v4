<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<fmt:setLocale value="${_LANG}" scope="request" />
<fmt:setBundle basename="properties.messages.rest.message" /> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script type="text/javascript">
$(document).ready(function(){
	
	
});








</script>
<div class="right_con">
	<div class="right_layout">
		<div class="tit_sub">
			회원가입
		</div>
		<div class="join_top">
			<img src="${pageContext.request.contextPath}/common/images/join_guide.png" alt="" />
		</div>
		<h2 class="h2tit">안내 사항</h2>
		<div class="join_guid_box">
			<div class="img_box">
				<img src="${pageContext.request.contextPath}/common/images/ico_notice.png" alt="" />
			</div>
			<p>
				여러 대의 차량을 관리하는 <em>개인 사업자 / 법인 사업자만</em>
				<br/>
				현재 웹페이지에서 회원가입이 가능합니다.
			</p>
			<a href="${pageContext.request.contextPath}/regist/agreeStipulation.do" class="btn btn_normal btn_red">기업회원 가입하기</a>
		</div>
		<div class="join_guid_box">
			<p>
				차량을 개인적으로 이용하시는 <em>일반 회원은</em>
				<br/>
				아래 어플을 다운받아 회원가입을 진행해주세요.
			</p>
			<div>
				<a href="https://play.google.com/store/apps/details?id=com.jastecm.viewcar.android" target="_blank"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_btn1.jpg" alt="Google play 앱다운받기"/></a>
			</div>
			<div style="margin-top: 8px;">
				<a href="https://itunes.apple.com/kr/app/%EB%B7%B0%EC%B9%B4-%ED%94%84%EB%A1%9C/id1221803378?mt=8" target="_blank"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/main_btn2.jpg" alt="App Store 앱다운받기"/></a>
			</div>
		</div>
	</div>
</div>