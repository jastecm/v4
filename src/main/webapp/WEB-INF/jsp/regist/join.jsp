<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<fmt:setLocale value="${_LANG}" scope="request" />
<fmt:setBundle basename="properties.messages.rest.message" />
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script type="text/javascript">
var mailAuthInterval;
var phoneAuthInterval;


$(document).ready(function(){
	
	$("#btn_next").on("click",function(){
		
		if(!$("#phoneAuthConfirm").val() || !$("#mailAuthConfirm").val()){
			alert("인증이 완료되지 않았습니다.");
		}else{
			$("#frm input[name=accountIdF]").val($("#frm input[name=accountId]").val()+"@"+$("#frm input[name=accountIdMail]").val());
			$("#frm input[name=accountMailF]").val($("#frm input[name=accountMail]").val()+"@"+$("#frm input[name=accountMailDomain]").val())
			$("#frm input[name=corpNum]").val($("#frm input[name=corpNum1]").val()+"-"+$("#frm input[name=corpNum2]").val()+"-"+$("#frm input[name=corpNum3]").val());
			$("#frm input[name=corpPhone]").val($("#frm select[name=corpPhone1]").val()+"-"+$("#frm input[name=corpPhone2]").val()+"-"+$("#frm input[name=corpPhone3]").val());
			$("#frm input[name=mobilePhone]").val($("#frm input[name=mobilePhone1]").val()+"-"+$("#frm input[name=mobilePhone2]").val()+"-"+$("#frm input[name=mobilePhone3]").val());
			$("#frm input[name=phone]").val($("#frm select[name=phone1]").val()+"-"+$("#frm input[name=phone2]").val()+"-"+$("#frm input[name=phone3]").val());
			$VDAS.http_post("/regist/validChk.do",$("#frm").serialize(),{
				success:function(r){
					$VDAS.submit_post("/regist/memberInfo.do",$("#frm"));					
				},
				error:function(r){
					var obj = JSON.parse(r.responseText).result;
					
					for (var key in obj) {
					  if (obj.hasOwnProperty(key)) {						  
						  alert($message[obj[key]]);
						  if($("#frm [name="+key+"]")) $("#frm [name="+key+"]").focus();
						  break;
					  }
					}
				}
			});
			
		}				
	});
	
	
	$("#sel_accountMailDomain").on("change",function(){
		var _this = $(this).val();
		var _target = $("#frm input[name=accountMailDomain]");
		if(_this.length == 0){
			_target.val("");
		}else{
			_target.val(_this);
		}
	})
	
	$("#mailAuth").on("click",function(){
		var _$this = $(this);
		var certMail = $("#frm input[name=accountMail]").val()+"@"+$("#frm input[name=accountMailDomain]").val();
		var authTime = 180;
        var display = _$this.siblings('.code_box').find(".time");
		$VDAS.http_post("/account/mailCertReq.do",{mailCert:certMail}
		,{
			success:function(r){
				if(!_$this.data("checker")){
					_$this.data("checker","1");
					_$this.html('재전송');
					_$this.siblings('.code_box').css({'display':'inline-block'});
			        
			        mailAuthInterval = startTimer(authTime, display);	
				}else{
					clearInterval(mailAuthInterval);
			        mailAuthInterval = startTimer(authTime, display);
				}
			},loading:$("#mailAuth")
		});
	});
	
	$("#btn_mailAuthConfirm").on("click",function(){
		var _$this = $(this);
		if(!$("#mailAuthConfirm").val()){
			var certMail = $("#frm input[name=accountMail]").val()+"@"+$("#frm input[name=accountMailDomain]").val();
			var checker = $("#mailAuthChecker").val();
			if(checker.length > 0){ 
				
				$VDAS.http_post("/account/mailVeriReq.do",{mailCert:certMail,certNum:checker}
				,{
					success:function(r){
						var strHtml = "<div class='tip'>인증되었습니다.</div>";
						_$this.closest(".code_box").html(strHtml);
						$("#mailAuth").remove();
						$("#mailAuthWrong").remove();
						$("#mailAuthConfirm").val("1");
						$("#frm input[name=accountMail],#frm input[name=accountMailDomain]").attr("readonly",true);
						$("#accountMailCouple,#sel_accountMailDomain").prop("disabled",true);
					},error:function(r){
						$("#mailAuthWrong").remove();
						var strHtml = "<div class='tip' id='mailAuthWrong'>인증번호가 잘못되었습니다.</div>";
						$("#mailAuthConfirm").after(strHtml);
					},loading:$("#btn_mailAuthConfirm")
				});
			}else{
				$("#mailAuthWrong").remove();
				var strHtml = "<div class='tip' id='mailAuthWrong'>인증번호가 잘못되었습니다.</div>";
				$("#mailAuthConfirm").after(strHtml);
			}
		}
		
	});
	
	$("#accountMailCouple").on("click",function(){
		if($(this).is(":checked")){
			$("#frm input[name=accountMail]").val($("#frm input[name=accountId]").val());
			$("#frm input[name=accountMailDomain]").val($("#frm input[name=accountIdMail]").val());
		}
	});
	
	$("#phoneAuth").on("click",function(){
		
		var _$this = $(this);
		
		var certPhone = $("#mobilePhone1").val()+"-"+$("#frm input[name=mobilePhone2]").val()+"-"+$("#frm input[name=mobilePhone3]").val();
		var authTime = 180;
        var display = _$this.siblings('.code_box').find(".time");
		$VDAS.http_post("/account/phoneCertReq.do",{phoneCert:certPhone}
		,{
			success:function(r){
				if(!_$this.data("checker")){
					_$this.data("checker","1");
					_$this.html('재전송');
					_$this.siblings('.code_box').css({'display':'inline-block'});
			        
					phoneAuthInterval = startTimer(authTime, display);	
				}else{
					clearInterval(phoneAuthInterval);
					phoneAuthInterval = startTimer(authTime, display);
				}
			},loading:$("#phoneAuth")
		});
	});
	
	$("#btn_phoneAuthConfirm").on("click",function(){
		
		var _$this = $(this);
		if(!$("#phoneAuthConfirm").val()){
			var certPhone = $("#mobilePhone1").val()+"-"+$("#frm input[name=mobilePhone2]").val()+"-"+$("#frm input[name=mobilePhone3]").val();
			var checker = $("#phoneAuthChecker").val();
			if(checker.length > 0){ 
				
				$VDAS.http_post("/account/phoneVeriReq.do",{phoneCert:certPhone,certNum:checker}
				,{
					success:function(r){
						var strHtml = "<div class='tip'>인증되었습니다.</div>";
						_$this.closest(".code_box").html(strHtml);
						$("#phoneAuth").remove();
						$("#phoneAuthWrong").remove();
						$("#phoneAuthConfirm").val("1");
						$("#frm input[name=mobilePhone2],#frm input[name=mobilePhone3]").prop("readonly",true);
						$("#mobilePhone1").prop("disabled",true);
						$("#frm input[name=mobilePhone1]").val($("#mobilePhone1").val());
					},error:function(r){
						$("#mailAuthWrong").remove();
						var strHtml = "<div class='tip' id='mailAuthWrong'>인증번호가 잘못되었습니다.</div>";
						$("#mailAuthConfirm").after(strHtml);
					},loading:$("#btn_mailAuthConfirm")
				});
			}else{
				$("#phoneAuthWrong").remove();
				var strHtml = "<div class='tip' id='phoneAuthWrong'>인증번호가 잘못되었습니다.</div>";
				$("#phoneAuthConfirm").after(strHtml);
			}
		}
	});
	
});


function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    var objInterval = setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.html(minutes + ":" + seconds);

        if (--timer < 0) {
            clearInterval(objInterval);
        }
    }, 1000);
    
    return objInterval;
}




	





</script>
<div class="right_layout">
	<form id="frm" method="POST">
		<input type="hidden" name="serviceAccepted" value="${reqVo.serviceAccepted}" />
		<input type="hidden" name="privateAccepted" value="${reqVo.privateAccepted}" />
		<input type="hidden" name="locationAccepted" value="${reqVo.locationAccepted}" />
		<input type="hidden" name="marketingAccepted" value="${reqVo.marketingAccepted}" />
		<input type="hidden" name="accountIdF" value="${reqVo.accountIdF}" />
		<input type="hidden" name="accountMailF" value="${reqVo.accountMailF}" />
		<input type="hidden" name="corpNum" value="${reqVo.corpNum}" />
		<input type="hidden" name="corpPhone" value="${reqVo.corpPhone}" />
		<input type="hidden" name="mobilePhone" value="${reqVo.mobilePhone}" />
		<input type="hidden" name="phone" value="${reqVo.phone}" />
        <div class="tit_sub">
            회원가입
        </div>
        <div class="join_top">
            <img src="${pageContext.request.contextPath}/common/images/${_LANG}/join_txt2.png" alt=""/>
        </div>
        <h2 class="h2tit">회원정보 입력</h2>
        <p class="join_txt1">스마트드라이빙! ViewCAR 차량운행관리 서비스의 정상적인 이용을 위해 아래 정보의 입력이 필요합니다. 정확하게 기재해주세요.</p>
        <h3 class="h3tit">기본정보</h3>
        <div class="join_box2">
            <div class="join_box2_1">
                <div class="join_box2_2">로그인 정보 <span>(필수입력)</span></div>
                <ul>
                    <li>
                        <span class="t1">아이디</span>
                        <input type="text" placeholder="" maxlength="20" style="width:125px;" name="accountId" value="${reqVo.accountId}"/>
                        <span class="t3">@</span>
                        <input type="text" placeholder="회사 도메인 주소" maxlength="20" style="width:125px;" name="accountIdMail" value="${reqVo.accountIdMail}"/>
                        <div class="error">
                            * @ 뒤 도메인 주소는 회사를 구분하는 용도로 쓰여지므로, 회사 이메일을 사용해주세요.
                            <br/>
                            (회사 이메일이 없는 경우, 임의로 회사 명을 작성해주세요.)
                        </div>
                    </li>
                    <li>
                        <span class="t1">비밀번호</span>
                        <input type="password" placeholder="" maxlength="20" class="w157" name="accountPw" style="font-size:12px;"/>
                        <span class="t2">비밀번호 확인</span>
                        <input type="password" placeholder="" maxlength="20" class="w157" name="accountPwConfirm" style="font-size:12px;"/>
                        <div class="tip">
                            * 8~20자리 대소문자, 숫자, 특수문자 조합
                        </div>
                    </li>
                    <li>
                        <span class="t1">보조 이메일</span>
                        <input type="text" placeholder="" maxlength="20" style="width:125px;" name="accountMail" value="${reqVo.accountMail}"/>
                        <span class="t3">@</span>
                        <input type="text" placeholder="" maxlength="20" style="width:125px;" name="accountMailDomain" value="${reqVo.accountMailDomain}"/>
                        <select class="select form" style="width:125px; margin-left: 5px;" id="sel_accountMailDomain">
                            <option value="">이메일 선택</option>
                            <option value="gmail.com">gmail.com</option>
                            <option value="naver.com">naver.com</option>
                            <option value="daum.net">daum.net</option>
                            <option value="hanmail.net">hanmail.net</option>
                            <option value="nate.com">nate.com</option>
                            <option value="hotmail.com">hotmail.com</option>
                            <option value="ymail.com">ymail.com</option>
                            <option value="rocketmail.com">rocketmail.com</option>
                        </select>
                        <c:if test="${empty reqVo.accountMailF}">
                        	<a href="#none" class="btn" id="mailAuth">인증</a>
                        	<div class="code_box">
	                        	<div class="input_box" id="mailAuthBox">	                            
		                        	<input type="text" placeholder="인증번호 입력" style="width:100px;" id="mailAuthChecker"/>
		                            	<div class="time"></div>
		                        </div>
								<a href="#none" class="btn" id="btn_mailAuthConfirm">확인</a>                        
		                    </div>
                        </c:if>
                        <c:if test="${not empty reqVo.accountMailF}">
                        	<div class="code_box" style="display:inline-block">
                        		<div class="tip">인증되었습니다.</div>
                        	</div>
                        </c:if>
                        <input type="hidden" id="mailAuthConfirm" value="<c:if test="${not empty reqVo.accountMailF}">1</c:if>" />
                        <div class="tip">
                            * 비밀번호를 잃어버렸을 경우 사용되는 이메일입니다.
                        </div>
                        <div class="cw">
                            <input id="accountMailCouple" class="checkbox" type="checkbox" <c:if test="${not empty reqVo.accountMailF  }">disabled="disabled"</c:if> />
                            <label for="accountMailCouple">아이디와 동일하게 사용</label>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="join_box2_1">
                <div class="join_box2_2">기업 정보 <span>(필수입력)</span></div>
                <ul>
                    <li>
                        <span class="t1">기업명</span><input type="text" maxlength="20" class="w157" name="corpNm" value="${reqVo.corpNm }"/>
                        <span class="t2">사업자등록번호</span><input type="text" maxlength="3" class="w48 join_input1 numberOnly" name="corpNum1" value="${reqVo.corpNum1 }"/>
                        <span class="t3">-</span><input type="text" maxlength="2" class="w37 join_input1 numberOnly" name="corpNum2" value="${reqVo.corpNum2 }"/>
                        <span class="t3">-</span><input type="text" maxlength="5" class="w64 join_input1 numberOnly" name="corpNum3" value="${reqVo.corpNum3 }"/>
                    </li>
                    <li>
                        <span class="t1">대표자명</span><input type="text" class="w157" name="ceoNm" maxlength="20" value="${reqVo.ceoNm }"/>
                        <span class="t2">연락처</span>
                        <select class="select form" style="width:50px;" name="corpPhone1">
                        	<c:forEach var="vo" items="${phoneTitle}" varStatus="i">
								<option value="${vo.code }" <c:if test="${vo.code eq reqVo.corpPhone1}">selected</c:if> >${vo.code }</option>
							</c:forEach>                            
                        </select>
                        <span class="t3">-</span><input type="text" maxlength="4" class="w64 join_input1 numberOnly" name="corpPhone2" value="${reqVo.corpPhone2 }"/>
                        <span class="t3">-</span><input type="text" maxlength="4" class="w37 join_input1 numberOnly" name="corpPhone3" value="${reqVo.corpPhone3 }"/>
                    </li>
                </ul>
            </div>
            <div class="join_box2_1">
                <div class="join_box2_2">부가정보 (선택입력)</div>
                <ul>
                    <li class="borB0">
                        <span class="t1">업태</span><input type="text" maxlength="10" class="w157" name="corpBusiness" value="${reqVo.corpBusiness }"/>
                        <span class="t2">종목</span><input type="text" maxlength="10" class="w157" name="corpItem" value="${reqVo.corpItem }"/>
                    </li>
                </ul>
            </div>
        </div>
        <h3 class="h3tit">관리자 정보</h3>
        <div class="join_box2">
            <div class="join_box2_1">
                <div class="join_box2_2">기본 정보 <span>(필수입력)</span></div>
                <ul>
                    <li>
                        <span class="t1">관리자명</span><input type="text" maxlength="20" class="w113" name="userNm" value="${reqVo.userNm }"/>
                    </li>
                    <li>
                        <span class="t1">휴대폰 번호</span>
                        <select class="select form" style="width:50px;" id="mobilePhone1">
                            <c:forEach var="vo" items="${mPhoneTitle}" varStatus="i">
								<option value="${vo.code }" <c:if test="${vo.code eq reqVo.mobilePhone1}">selected</c:if> >${vo.code }</option>
							</c:forEach>   
                        </select>
                        <input type="hidden" name="mobilePhone1" value="${reqVo.mobilePhone1 }" />
                        <span class="t3">-</span><input type="text" maxlength="4" class="w37 join_input1 numberOnly" name="mobilePhone2" value="${reqVo.mobilePhone2 }"/>
                        <span class="t3">-</span><input type="text" maxlength="4" class="w64 join_input1 numberOnly" name="mobilePhone3" value="${reqVo.mobilePhone3 }"/>
                        
                        <c:if test="${empty reqVo.mobilePhone}">
                        	<a href="#none" class="btn" id="phoneAuth">인증</a>
                        	<div class="code_box">
	                        	<div class="input_box" id="phoneAuthBox">
	                                <input type="text" placeholder="인증번호 입력" style="width:100px;" id="phoneAuthChecker"/>
	                                <div class="time"></div>
	                            </div>
	                            <a href="#none" class="btn" id="btn_phoneAuthConfirm">확인</a>
		                    </div>
                        </c:if>
                        <c:if test="${not empty reqVo.mobilePhone}">
                        	<div class="code_box" style="display:inline-block">
                        		<div class="tip">인증되었습니다.</div>
                        	</div>
                        </c:if>
                        <input type="hidden" id="phoneAuthConfirm" value="<c:if test="${not empty reqVo.mobilePhone  }">1</c:if>"/>
                    </li>
                    <li>
                        <span class="t1">일반 연락처</span>
                        <select class="select form" style="width:50px;" name="phone1">
                            <c:forEach var="vo" items="${phoneTitle}" varStatus="i">
								<option value="${vo.code }" <c:if test="${vo.code eq reqVo.phone1}">selected</c:if> >${vo.code }</option>
							</c:forEach>
                        </select>
                        <span class="t3">-</span><input type="text" maxlength="4" class="w37 join_input1 numberOnly" name="phone2" value="${reqVo.phone2 }"/>
                        <span class="t3">-</span><input type="text" maxlength="4" class="w64 join_input1 numberOnly" name="phone3" value="${reqVo.phone3 }"/>
                    </li>
                </ul>
            </div>
            <div class="join_box2_1">
                <div class="join_box2_2">부가 정보 (선택입력)</div>
                <ul>
                    <li class="borB0">
                        <span class="t1">부서명</span><input type="text" class="w157" maxlength="20" name="corpGroupNm" value="${reqVo.corpGroupNm }"/>
                    </li>
                </ul>
            </div>
        </div>
		<div class="btn_area2"><a href="${pageContext.request.contextPath}/regist/agreeStipulation.do" class="btn_cancle">이전</a><a href="#none" id="btn_next">다음</a></div>
	</form>
</div>