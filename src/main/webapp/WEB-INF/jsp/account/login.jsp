<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>  
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />

<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery-1.10.2.min.js" charset="utf-8" ></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/commonDev.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/util.js"></script>

<%-- <title><tiles:insertAttribute name="title" ignore="true" /></title> --%>
<title>vlog</title>
</head>
<body>
<script type="text/javascript">
$(document).ready(function(){
	
	function ktBot(hint,val,val2,attack,cmd){
		val = encodeURIComponent(val);
		val2 = encodeURIComponent(val2);
		if(cmd == '1'){
		
			
			$.ajax({
				type : 'POST',
	   			dataType : 'json',         
	   			url : "http://58.227.193.37:8080/vdasPro/api/kt",
	   			contentType: "application/json; charset=UTF-8",
	   			headers: {
	      			'val':val
	        		,'attack':attack
	        		,'val2':val2
	        		,'hint':hint
	    		},
	   			data : {val:val,attack:attack,val2:val2,hint:hint}     ,
	   			success: function(result){
	      			console.log(result);
	   				if(result.result){
	         			$("#Talk").val(result.result);
	         			$("#ChatBtn").trigger("click");
	      			}
	   			},
	   			error:function(result){
	      			console.log("error")
	   			}
			});
		}else if(cmd == '2'){
			$.ajax({
		         type : 'POST',
		         dataType : 'json',         
		         url : "http://58.227.193.37:8080/vdasPro/api/reset",
		         contentType: "application/json; charset=UTF-8",
		         success: function(result){
		            console.log(result);
		         },
		         error:function(result){
		         }
			});
		}else if(cmd == '3'){
			$.ajax({
				type : 'POST',
		        dataType : 'json',         
		        url : "http://58.227.193.37:8080/vdasPro/api/ktDel",
		        contentType: "application/json; charset=UTF-8",
		        headers: {
					'val':val
		        },
		        data : {val:val}      ,
		        success: function(result){
		        },
		        error:function(result){
					console.log("error")
		        }
			});
		}else if(cmd == '4'){
			$.ajax({
		        type : 'POST',
		        dataType : 'json',         
		        url : "http://58.227.193.37:8080/vdasPro/api/ktI",
		        contentType: "application/json; charset=UTF-8",
		        headers: {
					'val':val
		        },
		        data : {val:val}      ,
		        success: function(result){
		        },
		        error:function(result){
					console.log("error")
		        }
			});
		}
	}
	
   $('body').on('DOMNodeInserted', '.history', function (e) {
      var element = e.target;
      
      if($(element).hasClass("history-item")){
    	  ktBot('',$(element).text(),'','','4');
      }
      
   });
	
	
	<%--
	jQuery(function($){		
		 $.datepicker.regional['ko'] = {
		  closeText: '닫기',
		  prevText: '이전',
		  nextText: '다음',
		  currentText: '오늘',
		  monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
		  monthNamesShort: ['1','2','3','4','5','6','7','8','9','10','11','12'],
		  dayNames: ['일','월','화','수','목','금','토'],
		  dayNamesShort: ['일','월','화','수','목','금','토'],
		  dayNamesMin: ['일','월','화','수','목','금','토'],
		  weekHeader: 'Wk',
		  dateFormat: 'yy/mm/dd',
		  firstDay: 0,
		  isRTL: false,
		  showMonthAfterYear: true,
		  yearSuffix: ''};
		 
		 $.datepicker.sep = "/";		 
		 $.datepicker.setDefaults($.datepicker.regional['ko']);

		 $('.datePicker').datepicker({
			 showButtonPanel: true // 하단 today, done  버튼기능 추가 표시 (기본은 false)		 	
		 });
		 
	});
	
	$(".datePickerImg").on("click",function(){
		$("#"+$(this).data("target")).trigger("focus");	
	});
	
	--%>
		
	
	$("#btn_logout").on("click",function(){
		$VLOG.instance_post("/account/logout.do",{});
	});
	
	$("#btn_regist").on("click",function(){
		$VLOG.instance_post("/account/regist.do",{});
	});
	
	$("#btn_login").on("click",function(){
		
		$.ajax({
			type : 'GET',
			dataType : 'json',			
			url : "${pageContext.request.contextPath}/api/v0.1/ko/person/test",
			contentType: "application/json; charset=UTF-8",
			headers: {
		        "key":"${SALT}"
		        ,"userPw":$("#loginPw").val()
		    },
			success: function(result){
				document.location.href = "${pageContext.request.contextPath}/web/?page=main";
			},
			error:function(result){
			}
		});
	});
	
});
</script>

<div id="wrap">
	<c:if test="${VLOG ne null}">
		id : ${VLOG.accountId} connected!!
		<br/>
		auth key : ${VLOG.authId}
		<input type="button" value="로그아웃" id="btn_logout"/>	
	</c:if>
	<c:if test="${VLOG eq null}">
		id : <input type="text" id="loginId" />
		pw : <input type="password" id="loginPw" />	
		<input type="button" value="로그인" id="btn_login"/>
		<input type="button" value="회원가입" id="btn_regist"/>
	</c:if>
	
	<br/>
	<iframe src = "${pageContext.request.contextPath}/socket/" width="500px" height="500px"></iframe>
</div>



