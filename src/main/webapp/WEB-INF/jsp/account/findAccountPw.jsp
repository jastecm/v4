<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>  

<script type="text/javascript">
$(document).ready(function(){
	$(".tab").on("click",function(){
		var tab = $(this).data("tab");
		$(".tab").closest("li").removeClass("on");
		$(this).closest("li").addClass("on");
		$("#step3_btn_click").html("인증");
		$("#step3_btn_click").next('.code_box').css({'display':'none'});
		
		if(tab == "account"){
			$(".account").hide();			
			$(".pw").hide();
			
			$(".account.step1").show();
		}else if(tab == "pw"){
			$(".pw").hide();
			$(".account").hide();
			
			$(".pw.step1").show();
		}
	});
	
	
	$("#step1_btn_click").on("click",function(){
		var p = {};
		p.name = $("#step1_name").val();
		p.phone = $("#step1_phone1").val()+"-"+$("#step1_phone2").val()+"-"+$("#step1_phone3").val();
		
		var url = _defaultPath+"/api/v1/findAccount?name="+p.name;		
		
		$.ajax({
			url:encodeURI(url)
			,dataType : 'json'
			,contentType: "application/x-www-form-urlencoded; charset=UTF-8"
			,type:"GET"
			,headers: {
		        "phone":p.phone
		    }
			,success:function(r){
				var d = r.result;
				var strHtml = "";
				for(var i = 0 ; i < d.length ; i++){
					var vo = d[i];
					
					strHtml += '<div class="pageTab_cont account step2">';
					strHtml += '<div class="frm_row">';
					strHtml += '<div class="frm_item">';
					strHtml +=	'<h4 class="frm_tit">아이디</h4>';
					strHtml +=	'<div class="frm_cont">'+vo.userId+'</div>';
					strHtml += '</div></div>';
					strHtml += '<div class="frm_row">';
					strHtml += '<div class="frm_item">';
					strHtml +=	'<h4 class="frm_tit">보조이메일</h4>';
					strHtml +=	'<div class="frm_cont">'+vo.authMail+'</div>';
					strHtml += '</div></div>';
					strHtml += '<div class="frm_row">';
					strHtml += '<div class="frm_item">';
					strHtml +=  '<h4 class="frm_tit">가입일자</h4>';
					strHtml +=  '<div class="frm_cont">'+vo.regDate+'</div>';
					strHtml += '</div></div></div>';
				}
				
				strHtml += '<div class="btn_area2 account step2"><a href="#none" class="btn_cancel style02" id="step2_btn_all">아이디 전체 확인하기</a></div>'; //<a href="#none">로그인하기</a>
				
				$(".btn_area2.account.step1").after(strHtml);				
				$(".account.step1").hide();
			},error:function(r){
				if(r.status == 404){
					alert("일치하는 아이디가 없습니다.");
				}
			}
		});
	});
		
	$(".pageTab").on("click","#step2_btn_all",function(){
		$(".account.step2").hide();
		$("#step3_phone1").val($("#step1_phone1").val());
		$("#step3_phone2").val($("#step1_phone2").val());
		$("#step3_phone3").val($("#step1_phone3").val());
		$(".account.step3").show();
	});
	
	$("#step3_btn_click").on("click",function(){
		var phone = $("#step3_phone1").val()+"-"+$("#step3_phone2").val()+"-"+$("#step3_phone3").val();
		var url = _defaultPath+"/api/v1/smsCertReq";		
		
		$.ajax({
			url:encodeURI(url)
			,dataType : 'json'
			,contentType: "application/x-www-form-urlencoded; charset=UTF-8"
			,type:"GET"
			,headers: {
		        "phone":phone
		    }
			,success:function(r){
				$("#step3_btn_click").html('재전송');
				$("#step3_btn_click").next('.code_box').css({'display':'inline-block'});
				
				step3_timer();
		        return false;
			},error:function(r){
				if(r.status == 404){
					alert("인증번호가 일치하지 않습니다.");
				}
			}
		});
	});
	
	
	$("#step3_btn_confrim").on("click",function(){
		var phone = $("#step3_phone1").val()+"-"+$("#step3_phone2").val()+"-"+$("#step3_phone3").val();
		var auth = $("#step3_auth").val();
		var name = $("#step1_name").val();
		var url = _defaultPath+"/api/v1/smsVeriReq";		
		
		$.ajax({
			url:encodeURI(url)
			,dataType : 'json'
			,contentType: "application/x-www-form-urlencoded; charset=UTF-8"
			,type:"GET"
			,headers: {
		        "phone":phone
		        ,"certNum":auth
		    }
			,success:function(r){
				url =  _defaultPath+"/api/v1/findAccountVeriComplate?name="+name;				
				
				$.ajax({
					url:encodeURI(url)
					,dataType : 'json'
					,contentType: "application/x-www-form-urlencoded; charset=UTF-8"
					,type:"GET"
					,headers: {
				        "phone":phone
				    }
					,success:function(r){
						$(".account.step2").remove();
						var d = r.result;
						var strHtml = "";
						for(var i = 0 ; i < d.length ; i++){
							var vo = d[i];
							
							strHtml += '<div class="pageTab_cont account step2">';
							strHtml += '<div class="frm_row">';
							strHtml += '<div class="frm_item">';
							strHtml +=	'<h4 class="frm_tit">아이디</h4>';
							strHtml +=	'<div class="frm_cont">'+vo.userId+'</div>';
							strHtml += '</div></div>';
							strHtml += '<div class="frm_row">';
							strHtml += '<div class="frm_item">';
							strHtml +=	'<h4 class="frm_tit">보조이메일</h4>';
							strHtml +=	'<div class="frm_cont">'+vo.authMail+'</div>';
							strHtml += '</div></div>';
							strHtml += '<div class="frm_row">';
							strHtml += '<div class="frm_item">';
							strHtml +=  '<h4 class="frm_tit">가입일자</h4>';
							strHtml +=  '<div class="frm_cont">'+vo.regDate+'</div>';
							strHtml += '</div></div></div>';
						}
						
						strHtml += '<div class="btn_area2 account step2"></div>'; //<a href="#none">로그인하기</a>
						
						$(".account.step3").hide();
						$(".btn_area2.account.step1").after(strHtml);
					},error:function(r){
						if(r.status == 401){
							alert("인증이 유효하지 않습니다.");
						}
					}
				});
				
				
		        return false;
			},error:function(r){
				if(r.status == 404){
					alert("일치하는 아이디가 없습니다.");
				}
			}
		});
	});
	
	///////////////
	
	$("#_step1_mail_sel").on("change",function(){
		if($(this).val().trim().length == 0){
			$("#_step1_mail2").val("");			
		}else{
			$("#_step1_mail2").val($(this).val());
		}
	});
	
	$("#_step1_auth,#_step1_auth2").on("click",function(){
		var name = $("#_step1_name").val();
		var mail = $("#_step1_mail1").val()+"@"+ $("#_step1_mail2").val();
		
		var url = "";
		
		if($(this).attr("id") == '_step1_auth') url = _defaultPath+"/api/v1/findPasswordToId?name="+name+"&web=1";
		else url = _defaultPath+"/api/v1/findPassword?name="+name+"&web=1";
		
		$.ajax({
			url:encodeURI(url)
			,contentType: "application/x-www-form-urlencoded; charset=UTF-8"
			,type:"GET"
			,headers: {
		        "email":mail
		    }
			,success:function(r){
				$("#_step2_text").html(name+"님,"+r.result+"으로 전송한<br/>비밀번호 재설정 링크를 확인해주세요.");
				step2_timer();
				$(".pw.step1").hide();
				$(".pw.step2").show();
			},error:function(r){
				if(r.status == 404){
					alert("일치하지는 사용자를 찾을수 없습니다.");
				}
			}
		});
		
	});
});

var step3_timer_obj;
var step3_timer_sec = 180;
function step3_timer(){
	if(step3_timer_obj) clearInterval(step3_timer_obj);
	step3_timer_sec = 180;
	step3_timer_tic();
	step3_timer_obj = setInterval(step3_timer_tic,1000);	
}

function step3_timer_tic(){
	if(step3_timer_sec >= 0){
		var m = Math.floor(step3_timer_sec/60);
		var s = step3_timer_sec%60;
		if(s < 10)  s = LPAD(s+"","0",2);			
		$("#step3_timer").html(m+":"+s);
		step3_timer_sec--;
	}	
}

var step2_timer_obj;
var step2_timer_sec = 10800;
function step2_timer(){
	if(step2_timer_obj) clearInterval(step2_timer_obj);
	step2_timer_sec = 10800;
	step2_timer_tic();
	step2_timer_obj = setInterval(step2_timer_tic,1000);	
}

function step2_timer_tic(){
	if(step2_timer_sec >= 0){
		var h = Math.floor(step2_timer_sec/3600);
		var m = Math.floor(Math.floor(step2_timer_sec%3600)/60);
		var s = Math.floor(step2_timer_sec%3600)%60;		
		if(m < 10)  m = LPAD(m+"","0",2);			
		if(s < 10)  s = LPAD(s+"","0",2);
		$("#_step2_timer").html(h+":"+m+":"+s);
		step2_timer_sec--;
	}	
}
</script>

<div class="join_layout">
	<div class="tit_sub">
	    아이디/비밀번호찾기
	</div>
	<div class="pageTab">
	    <ul class="tab_nav">
	        <li class="on"><a href="#none" class="tab" data-tab="account">아이디 찾기</a></li>
	        <li><a href="#none" class="tab" data-tab="pw">비밀번호 찾기</a></li>
	    </ul>
	    <div class="pageTab_cont account step1">
	        <div class="cont_item">
	            <h4 class="item_tit">기본 정보 <em>(필수입력)</em></h4>
	            <div class="page_cont">
	                <div class="frm_row">
	                    <div class="frm_item">
	                        <h4 class="frm_tit">
	                            이름
	                        </h4>
	                        <div class="frm_cont">
	                            <input type="text" class="text" id="step1_name" style="width:160px;" value=""/>
	                        </div>
	                    </div>
	                </div>
	                <div class="frm_row">
	                    <div class="frm_item">
	                        <h4 class="frm_tit">
	                            휴대폰 번호
	                        </h4>
	                        <div class="frm_cont">
	                            <select class="select form" id="step1_phone1">
	                                <option value="010">010</option>
	                            </select>
	                            -
	                            <input type="text" class="text" id="step1_phone2" style="width:50px;" value=""/>
	                            -
	                            <input type="text" class="text" id="step1_phone3" style="width:50px;" value=""/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="btn_area2 account step1"><a href="#none" id="step1_btn_click">확인</a></div>
        
        <h2 class="h2tit account step3" style="display: none;">아이디 전체 확인하기</h2>
	    <p class="sub_tit_txt  account step3" style="display: none;">개인정보 도용에 대한 피해 방지를 위하여 아이디 뒷자리를 표기하지 않습니다. 아이디 뒷자리를 확인하시려면 휴대폰 인증 절차를 진행해주세요.</p>
	    <div class="send_box  account step3" style="display: none;">
	        <span class="send_tit">휴대폰 번호</span>
	        <select class="select form" id="step3_phone1" readonly>
	            <option value="010">010</option>
	        </select>
	        -
	        <input type="text" class="text" style="width:50px;" id="step3_phone2" value="" readonly/>
	       -
	       <input type="text" class="text" style="width:50px;" id="step3_phone3" value="" readonly/>
	       <a href="#none" class="btn form" id="step3_btn_click">인증</a>
	       <div class="code_box">
	           <div class="input_box">
	               <input type="text" class="text" id="step3_auth" placeholder="인증번호 입력" style="width:140px;">
	                <div class="time" id="step3_timer">3:00</div>
	            </div>
	            <a href="#none" class="btn form" id="step3_btn_confrim">확인</a>
	        </div>
	    </div>
	    <div class="btn_area2 account step3" style="display: none;"><a href="#" class="btn_cancel">취소</a></div>
        
        
        <!-- pw -->
        <div class="pageTab_cont pw step1" style="display: none;">
	        <div class="cont_item">
	            <h4 class="item_tit">기본 정보 <em>(필수입력)</em></h4>
	            <div class="page_cont">
	                <div class="frm_row">
	                    <div class="frm_item">
	                        <h4 class="frm_tit">
	                            이름
	                        </h4>
	                        <div class="frm_cont">
	                            <input type="text" class="text" id="_step1_name" style="width:160px;" value=""/>
	                        </div>
	                    </div>
	                </div>
	                <div class="frm_row">
	                    <div class="frm_item">
	                        <h4 class="frm_tit">
	                            아이디
	                        </h4>
	                        <div class="frm_cont">
	                            <input type="text" class="text" style="width:120px;" id="_step1_mail1" value=""/>
	                            <span class="at">@</span>
	                            <input type="text" class="text" style="width:120px;" id="_step1_mail2" value=""/>
	                            <select class="select form" style="width:120px;" id="_step1_mail_sel">
                                    <option value="">이메일 선택</option>
                                    <option value="naver.com">네이버</option>
                                    <option value="google.com">구글</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="btn_area2 pw step1" style="display: none;"><a href="#none" id="_step1_auth">인증번호 전송</a><a href="#none" id="_step1_auth2" style="width:175px">인증번호 전송(보조이메일)</a></div>               
        
        <div class="pageTab_cont pw step2" style="display: none;">
            <div class="find_box">
                <p class="txt" id="_step2_text">
                    홍길동님,abcd123@naver.com으로 전송한<br/>비밀번호 재설정 링크를 확인해주세요.
                </p>
                <p class="form_notice">*해당 링크는 메일이 발송된 시점부터 3시간동안 유효합니다.</p>
                <div class="find_time" id="_step2_timer">
                    
                </div>
            </div>
        </div>
    </div>
</div>



