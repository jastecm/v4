<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>  

<script type="text/javascript">
$(document).ready(function(){
	$("#btn_ok").on("click",function(){
		
		if($("#newPw").val().trim().length == 0 ){
			alert("비밀번호를 입력해주세요.");
		}else{
			if($("#newPw").val() != $("#newPwConfirm").val()){
				alert("확인 비밀번호가 일치하지 않습니다.");
			}else{
				var url = _defaultPath+"/api/v1/forceChangePassword";
				
				$.ajax({
					url:encodeURI(url)
					,dataType : 'json'
					,contentType: "application/x-www-form-urlencoded; charset=UTF-8"
					,type:"PUT"					
					,headers: {
				        "key":"${key}"
				        ,pw:$("#newPw").val()
				    }
					,success:function(r){
						alert("저장되었습니다.");
						window.location.href = _defaultPath+"/main/index.do";
					},error:function(r){
						if(r.status == 400){
							alert("비밀번호 패턴이 잘못되었습니다.");
						}else if(r.status == 401){
							alert("유요하지 않은 접근입니다.");
						}
					}
				});
			}
		}
			
		
		
		
		
		
	});
});
</script>

<div class="join_layout">
    
	<h2 class="h2tit">비밀번호 재설정</h2>
	<p class="sub_tit_txt">비밀번호를 재설정해주세요.</p>
	
	<div class="pageTab">
	    <div class="pageTab_cont">
	        <div class="frm_row">
	            <div class="frm_item">
	                <h4 class="frm_tit">
	                    새 비밀번호
	                </h4>
	                <div class="frm_cont">
	                    <input type="password" class="text" style="width:160px;" id="newPw"/>
	               </div>
	           </div>
	       </div>
	       <div class="frm_row">
	           <div class="frm_item">
	               <h4 class="frm_tit">
	                   새 비밀번호 확인
	               </h4>
	               <div class="frm_cont">
	                   <input type="password" class="text" style="width:160px;" id="newPwConfirm"/>
	                        <p class="form_notice">* 8~20자리 대소문자, 숫자, 특수문자 조합</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="btn_area2"><a href="#none" id="btn_ok">확인</a></div>
    </div>
</div>



