<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>  
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	
	$("#mapFrame").attr("src",_defaultPath+"/hyundaimotor/mapPointPOCAll");
	
});
</script>
<div class="top_wrap">
	<form id="searchFrm">
		<input type="hidden" name="fenceGroup" />
	</form>
	<div class="sub_layout" style="padding-left:0px;width:1024px;">
		<div class="right_layout" style="padding-left:0px;">
			<h2 class="tit_sub">
				현대자동차 아산공장 POC 차량위치 
				&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="${pageContext.request.contextPath}/hyundaimotor/vehicleList?key=${key}" class="btn_cancel" style="color:white;">리스트 보기</a>
				<p></p>
			</h2>			
			<div class="sub_top1">
			</div>
			<div class="tab_layout">
				<div class="map_area">
					<div id="map_canvas" style="height:inherit;text-align: center">
						<iframe id="mapFrame" src="#none" width="760px" height="400px"></iframe>
					</div>
				</div>
			</div>
			<!-- <div class="sub_top1">
				<span>test<b id="titleCnt1">100대</b></span><span>test<b id="titleCnt2">100대</b></span>
				<span>test<b id="titleCnt1">100대</b></span><span>test<b id="titleCnt2">100대</b></span>
				<span>test<b id="titleCnt1">100대</b></span><span>test<b id="titleCnt2">100대</b></span>
				<span>test<b id="titleCnt1">100대</b></span><span>test<b id="titleCnt2">100대</b></span>
			</div> -->
			<c:if test="${fn:length(rtv1) > 0 }">
				<table class="table1 vehicle" id="contentsTable1" style="padding:10px 0;">
					<colgroup>
						<col style="width:20%" />
						<col style="width:20%" />
						<col style="width:20%" />
						<col style="width:20%" />
						<col style="width:20%" />
					</colgroup>
					<thead>
						<tr>
							<c:forEach var="vo" items="${rtv1}" varStatus="i">
								<th>${vo.alies }</th>
							</c:forEach>
						</tr>
					</thead>
					<tbody>
						<tr>
							<c:forEach var="vo" items="${rtv1}" varStatus="i">
								<td>${vo.cnt }</td>
							</c:forEach>
						</tr>
					</tbody>
				</table>
			</c:if>
			<c:if test="${fn:length(rtv2) > 0 }">
				<table class="table1 vehicle" id="contentsTable1" style="padding:10px 0;">
					<colgroup>
						<col style="width:20%" />
						<col style="width:20%" />
						<col style="width:20%" />
						<col style="width:20%" />
						<col style="width:20%" />
					</colgroup>
					<thead>
						<tr>
							<c:forEach var="vo" items="${rtv2}" varStatus="i">
								<th>${vo.alies }</th>
							</c:forEach>
						</tr>
					</thead>
					<tbody>
						<tr>
							<c:forEach var="vo" items="${rtv2}" varStatus="i">
								<td>${vo.cnt }</td>
							</c:forEach>
						</tr>
					</tbody>
				</table>
			</c:if>
			<c:if test="${fn:length(rtv3) > 0 }">
				<table class="table1 vehicle" id="contentsTable1" style="padding:10px 0;">
					<colgroup>
						<col style="width:20%" />
						<col style="width:20%" />
						<col style="width:20%" />
						<col style="width:20%" />
						<col style="width:20%" />
					</colgroup>
					<thead>
						<tr>
							<c:forEach var="vo" items="${rtv3}" varStatus="i">
								<th>${vo.alies }</th>
							</c:forEach>
						</tr>
					</thead>
					<tbody>
						<tr>
							<c:forEach var="vo" items="${rtv3}" varStatus="i">
								<td>${vo.cnt }</td>
							</c:forEach>
						</tr>
					</tbody>
				</table>
			</c:if>
			<c:if test="${fn:length(rtv4) > 0 }">
				<table class="table1 vehicle" id="contentsTable1" style="padding:10px 0;">
					<colgroup>
						<col style="width:20%" />
						<col style="width:20%" />
						<col style="width:20%" />
						<col style="width:20%" />
						<col style="width:20%" />
					</colgroup>
					<thead>
						<tr>
							<c:forEach var="vo" items="${rtv4}" varStatus="i">
								<th>${vo.alies }</th>
							</c:forEach>
						</tr>
					</thead>
					<tbody>
						<tr>
							<c:forEach var="vo" items="${rtv4}" varStatus="i">
								<td>${vo.cnt }</td>
							</c:forEach>
						</tr>
					</tbody>
				</table>
			</c:if>
			<c:if test="${fn:length(rtv5) > 0 }">
				<table class="table1 vehicle" id="contentsTable1" style="padding:10px 0;">
					<colgroup>
						<col style="width:20%" />
						<col style="width:20%" />
						<col style="width:20%" />
						<col style="width:20%" />
						<col style="width:20%" />
					</colgroup>
					<thead>
						<tr>
							<c:forEach var="vo" items="${rtv5}" varStatus="i">
								<th>${vo.alies }</th>
							</c:forEach>
						</tr>
					</thead>
					<tbody>
						<tr>
							<c:forEach var="vo" items="${rtv5}" varStatus="i">
								<td>${vo.cnt }</td>
							</c:forEach>
						</tr>
					</tbody>
				</table>
			</c:if>
		</div>
	</div>
</div>




