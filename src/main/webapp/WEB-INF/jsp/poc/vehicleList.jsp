<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>  
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	var lazy;
	
	if(!lazy){
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
				fenceGroup : $("#searchType")
				,vin : $("#searchText")
			}
			,scrollRow : 8
			,rowHeight : 50
			,limit : 10			
			,loadUrl : "/hyundaimotor/getVehicleList?key=${key}"
			,initSearch : true
			,createDataHtml : function(r){
				console.log(r);
				var data = r.rtvList;				
				var strHtml = '';
				
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];

					strHtml += '<tr">';
					strHtml += '<td style="line-height:5px;">'+vo.fenceGroup+'</td>';
					strHtml += '<td style="line-height:5px;">'+vo.plateNum+'</td>';
					strHtml += '<td style="line-height:5px;">'+vo.deviceSn+'</td>';
					strHtml += '<td style="line-height:5px;">'+new Date(vo.lastestLocationTime).yyyymmddTime()+'</td>';
					
					strHtml += '<td style="line-height:5px;"><a href="#none" class="btn_map" data-lat="'+vo.lastestLocation.split(",")[1]+'" data-lon="'+vo.lastestLocation.split(",")[0]+'" data-vin="'+vo.plateNum+'" data-driving="'+vo.geofenceMonitor+'"><img src="${pathCommon}/images/gps.jpg" alt=""></a></td>';
					strHtml += '<td style="line-height:5px;"><a href="#none" class="btn_tdr" data-vin="'+vo.plateNum+'">주행기록</a>&nbsp;&nbsp;<a href="#none" class="btn_map2" data-vin="'+vo.plateNum+'"><img src="${pathCommon}/images/gps.jpg" alt=""></a></td>';
					
					
					strHtml += '</tr>';
				}
				return strHtml;

			}
		});
		
	}
	
	$(".btn_search").on("click",function(){
		$.lazyLoader.search(lazy);
	});
	
	$("#mapFrame").attr("src",_defaultPath+"/hyundaimotor/mapPointPOC");
	
	
	$("body").on("click",".btn_map",function(){		

		var lat = $(this).data("lat");
		var lon = $(this).data("lon");
		var vin = $(this).data("vin");
		var driving = $(this).data("driving");
		//$("#updateDialog").attr("title",alies);
		//$("#alies").val(alies);
		
		$(".black").show();
		$(".pop2").show();

		$("#mapFrame").get(0).contentWindow.move(lon,lat,vin,driving);
		
	});
	
	$("body").on("click",".btn_map2",function(){		

		var vin = $(this).data("vin");
		
		$("#mapFrame2").attr("src",_defaultPath+"/hyundaimotor/mapLinePOC?vin="+vin);
		
		$(".black").show();
		$(".pop3").show();

	});
	
	$("body").on("click",".btn_tdr",function(){		

		var vin = $(this).data("vin");
		window.location.href = _defaultPath+"/hyundaimotor/tdrHstr?key=${key}&vin="+vin;
		
	});
	
	
	$(".btn_cancel").on("click",function(){
		$(".black").hide();
		$(".pop2").hide();
		$(".pop3").hide();
	});
});
</script>
<div class="top_wrap">
	<form id="searchFrm">
		<input type="hidden" name="fenceGroup" />
		<input type="hidden" name="vin" />
	</form>
	<div class="sub_layout" style="padding-left:0px;width:1024px;">
		<div class="right_layout" style="padding-left:0px;">
			<h2 class="tit_sub">
				현대자동차 아산공장 POC 차량위치 
				&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="${pageContext.request.contextPath}/hyundaimotor/vehicleLocation?key=${key}" class="btn_cancel" style="color:white;">위치 보기</a>
				<p></p>
			</h2>
			<div class="sub_top1">
				<!-- <span>test<b id="titleCnt1">100대</b></span><span>test<b id="titleCnt2">100대</b></span>
				<span>test<b id="titleCnt1">100대</b></span><span>test<b id="titleCnt2">100대</b></span>
				<span>test<b id="titleCnt1">100대</b></span><span>test<b id="titleCnt2">100대</b></span>
				<span>test<b id="titleCnt1">100대</b></span><span>test<b id="titleCnt2">100대</b></span> -->
			</div>
			<div class="search_box1">
				<div class="search_box1_1">
					<div class="select_type1">
						<div class="select_type1_1">
							
							<select class="sel" id="searchType">
								<option value="">전체</option>
								<c:forEach var="f" items="${fenceList}" varStatus="i">
									<option value="${f.alies}">${f.alies}</option>
								</c:forEach>
								<option value="행불">행불</option>
							</select>
							
						</div>
					</div><input type="text" id="searchText" class="search_input" />
				</div><a href="#none" class="btn_search">조회</a>
			</div>
			<div class="tab_layout">
				<!-- <div style="margin-bottom: 10px;">
					<select id="searchOrder" class="select form" style="width:105px;">
						<option value="driving">최근 운행순</option>
						<option value="regDate">최근 등록순</option>
					</select>
					<select id="searchUsed" class="select form" style="width:105px;">
						<option value="1">사용중</option>
						<option value="0">사용정지중</option>
					</select>
				</div>	 -->	
				<table class="table1 vehicle" id="contentsTable" style="padding:10px 0;">
					<colgroup>
						<col style="width:150px;" />
						<col />
						<col style="width:170px;" />
						<col style="width:150px;" />
						<col style="width:200px;" />
					</colgroup>
					<thead>
						<tr>
							<th>위치</th>
							<th>차대번호</th>
							<th>단말기</th>
							<th>시간</th>
							<th>위치</th>
							<th>주행기록</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				<!-- <div class="txt1">
					<h1>운행유무 표기 안내</h1>
					<p><b>주행중 </b>차량이 현재 운행중으로 엔진종료 후 현재 운행정보 시작/종료일시, 이동거리가 시스템에 기록됩니다.</p>
					<p><b>운행대기 </b>배차 등록/승인된 사용자가 있으나 아직 운행을 시작하는 않은 차량입니다.</p>
					<p><b>주차중 </b>배차 등록된 사용자가 현재 차량을 주차중입니다. </p>
					<p><b>미배차 </b>배차 등록된 사용자 없는 차량입니다.</p>
					<p><b>사용중지 </b>사용을 하지 않는 차량입니다.</p>
					<br/><br/>
					<h1>차량상태 안내</h1>
					<p><span class="status on"></span>차량상태가 정상입니다.</p>
					<p><span class="status off"></span>차량에 이상이 있습니다. 마우스를 롤오버하면 상세내용이 보입니다.</p>
					<br/><br/>
					<h1>주차위치 안내</h1>
					<p>위치확인을 클릭하시면 사용자 휴대폰 App을 통해 최종 등록된 주차위치가 아래 지도에 표시됩니다.<br/>(사용자의 GPS 활용 미동의, GPS 오동작, 지하주차장 이동 등 GPS 수신상태로 고르지 못할 경우 위치조회가 정확하지 않을 수 있습니다.)</p>
				</div> -->
			</div>
		</div>
		<div class="black"></div>
		<div class="pop2" style="display:none;height:566px">
			<div class="pop_layout">
				<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt="" /></a>
				<h1>차량위치</h1>
				<div class="map_area">
					<div id="map_canvas" style="height:inherit">
						<iframe id="mapFrame" src="#none" width="760px" height="400px"></iframe>
					</div>
				</div>
				<div style="height:31px"></div>
				<div class="return_btn">
					<a href="#none" class="btn_cancel">확인</a>
				</div>
			</div>
		</div>
		<div class="black"></div>
		<div class="pop3" style="display:none;height:566px">
			<div class="pop_layout">
				<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt="" /></a>
				<h1>주행기록</h1>
				<div class="map_area">
					<div id="map_canvas" style="height:inherit">
						<iframe id="mapFrame2" src="#none" width="760px" height="400px"></iframe>
					</div>
				</div>
				<div style="height:31px"></div>
				<div class="return_btn">
					<a href="#none" class="btn_cancel">확인</a>
				</div>
			</div>
		</div>
	</div>
</div>




