<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>  
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	var lazy;
	
	if(!lazy){
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
			}
			,scrollRow : 8
			,rowHeight : 50
			,limit : 20			
			,loadUrl : "/hyundaimotor/getTdrHstr?vin=${vin}"
			,initSearch : true
			,createDataHtml : function(r){
				console.log(r);
				var data = r.result;
				var strHtml = '';
				
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += '<tr">';
					strHtml += '<td style="line-height:5px;">'+vo.deviceSn+'</td>';
					strHtml += '<td style="line-height:5px;">'+vo.lat+'</td>';
					strHtml += '<td style="line-height:5px;">'+vo.lon+'</td>';
					strHtml += '<td style="line-height:5px;">'+vo.rpm+'</td>';
					strHtml += '<td style="line-height:5px;">'+vo.speed+'</td>';
					strHtml += '<td style="line-height:5px;">'+(vo.distance/1000).toFixed(1)+' km/h</td>';
					strHtml += '<td style="line-height:5px;">'+(vo.fco/1000).toFixed(2)+' ℓ</td>';
					strHtml += '<td style="line-height:5px;">'+new Date(vo.deviceTime).yyyymmddTime()+'</td>';
					strHtml += '</tr>';
				}
				return strHtml;

			}
		});
		
	}
	
});
</script>
<div class="top_wrap">
	<form id="searchFrm">
	</form>
	<div class="sub_layout" style="padding-left:0px;width:1024px;">
		<div class="right_layout" style="padding-left:0px;">
			<h2 class="tit_sub">
				현대자동차 아산공장 POC 주행기록 히스토리 - ${vin}
				&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="${pageContext.request.contextPath}/hyundaimotor/vehicleList?key=${key}" class="btn_cancel" style="color:white;">리스트 보기</a>
				<p></p>
			</h2>
			<div class="sub_top1">
				<!-- <span>test<b id="titleCnt1">100대</b></span><span>test<b id="titleCnt2">100대</b></span>
				<span>test<b id="titleCnt1">100대</b></span><span>test<b id="titleCnt2">100대</b></span>
				<span>test<b id="titleCnt1">100대</b></span><span>test<b id="titleCnt2">100대</b></span>
				<span>test<b id="titleCnt1">100대</b></span><span>test<b id="titleCnt2">100대</b></span> -->
			</div>
			<div class="tab_layout">
				<table class="table1 vehicle" id="contentsTable" style="padding:10px 0;">
					<colgroup>
						<col />
						<col />
						<col />
						<col />
						<col />
						<col />
						<col />
						<col style="width:150px;"/>
					</colgroup>
					<thead>
						<tr>
							<th>단말번호</th>
							<th>위도</th>
							<th>경도</th>
							<th>rpm</th>
							<th>속도</th>
							<th>주행거리</th>
							<th>연료소모량</th>
							<th>수집시간</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				<!-- <div class="txt1">
					<h1>운행유무 표기 안내</h1>
					<p><b>주행중 </b>차량이 현재 운행중으로 엔진종료 후 현재 운행정보 시작/종료일시, 이동거리가 시스템에 기록됩니다.</p>
					<p><b>운행대기 </b>배차 등록/승인된 사용자가 있으나 아직 운행을 시작하는 않은 차량입니다.</p>
					<p><b>주차중 </b>배차 등록된 사용자가 현재 차량을 주차중입니다. </p>
					<p><b>미배차 </b>배차 등록된 사용자 없는 차량입니다.</p>
					<p><b>사용중지 </b>사용을 하지 않는 차량입니다.</p>
					<br/><br/>
					<h1>차량상태 안내</h1>
					<p><span class="status on"></span>차량상태가 정상입니다.</p>
					<p><span class="status off"></span>차량에 이상이 있습니다. 마우스를 롤오버하면 상세내용이 보입니다.</p>
					<br/><br/>
					<h1>주차위치 안내</h1>
					<p>위치확인을 클릭하시면 사용자 휴대폰 App을 통해 최종 등록된 주차위치가 아래 지도에 표시됩니다.<br/>(사용자의 GPS 활용 미동의, GPS 오동작, 지하주차장 이동 등 GPS 수신상태로 고르지 못할 경우 위치조회가 정확하지 않을 수 있습니다.)</p>
				</div> -->
			</div>
		</div>
		<div class="black"></div>
		<div class="pop2" style="display:none;height:566px">
			<div class="pop_layout">
				<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt="" /></a>
				<h1>차량위치</h1>
				<div class="map_area">
					<div id="map_canvas" style="height:inherit">
						<iframe id="mapFrame" src="#none" width="760px" height="400px"></iframe>
					</div>
				</div>
				<div style="height:31px"></div>
				<div class="return_btn">
					<a href="#none" class="btn_cancel">확인</a>
				</div>
			</div>
		</div>
	</div>
</div>




