<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script type="text/javascript">
var lazy;
var lazyManage;
$(document).ready(function(){
	initMenuSel("M2002");
	
	var temp = $("#editFrm input[name=searchDate]").val().split(" ");
	$("#editFrm input[name=searchDate]").val(replaceAll(temp[0],"/",""));
	$("#searchDate").val(temp[0]);

	$VDAS.ajaxCreateSelect("/com/getAllocateList.do",{searchDate:replaceAll(temp[0],"/","")},$("#allocateKey"),false,"선택","${reqVo.allocateKey}|${reqVo.vehicleKey}|${reqVo.accountKey}","${_LANG}",null,null);
	$VDAS.ajaxCreateSelect("/com/getGroupUserList.do",{},$("#selectUser"),false,"전체 사용자","${reqVo.accountKey}","${_LANG}",null,null);
	$VDAS.ajaxCreateSelect("/com/getCode.do",{code:"C018"},$("#selectKind"),false,"선택","${reqVo.expenseType}","${_LANG}",null,null);
	
	$("body").on("change",".searchDate",function(){
		var date = replaceAll($(this).val(),"/","");
		$("#editFrm input[name=searchDate]").val(replaceAll($(this).val(),"/",""));
		$VDAS.ajaxCreateSelect("/com/getAllocateList.do",{searchDate:date},$("#startDate"),false,"선택","","${_LANG}",null,null);
	});
	
	$("body").on("click",".a_edit",function(){
		$("#editFrm input[name=selectUser]").val($("#selectUser").val());
		$("#editFrm input[name=allocateKey]").val($("#allocateKey").val());
		$("#editFrm input[name=selectKind]").val($("#selectKind").val());
		$("#editFrm input[name=inputCost]").val($("#inputCost").val());
		$("#editFrm input[name=inputNote]").val($("#inputNote").val());
		
		$VDAS.http_post("/allocate/updateFee.do",$("#editFrm").serialize(),{
			success:function(){
				$VDAS.instance_post("/vehicleMng/vehicleFee.do",{});
			}
		});
	});
	
	$("body").on("click",".a_close",function(){
		$VDAS.instance_post("/vehicleMng/vehicleFee.do",{});
	});
	
	$("body").on("click",".btn_delete3",function(){
		var key = $(this).data("key");
		$VDAS.http_post("/allocate/deleteFee.do",{expenseKey:key},{
			success:function(){
				$VDAS.instance_post("/vehicleMng/vehicleFee.do",{});
			}
		});
	});
});


</script>
<form id="editFrm">
	<input type="hidden" name="searchDate" value="${reqVo.startDate }" />
	<input type="hidden" name="expenseKey" value="${reqVo.expenseKey }" />
	<input type="hidden" name="selectUser" value="" />
	<input type="hidden" name="allocateKey" value="" />
	<input type="hidden" name="selectKind" value="" />
	<input type="hidden" name="inputCost" value="" />
	<input type="hidden" name="inputNote" value="" />
	<input type="hidden" name="state" value="${reqVo.state }" />
</form>
<div class="right_layout">
	<div class="detail_view">
		<h2 class="h2tit">차량경비 수정</h2>
		<a href="#none" class="btn_delete3" data-key="${reqVo.expenseKey }"><img src="${pathCommon}/images/${_LANG}/btn_delete.jpg" alt="삭제" /></a>
		<div class="gcbox">
			<c:if test="${reqVo.state eq '3' }">
				<div class="gct">
					반려 사유 : ${reqVo.rejectNote }
				</div>
			</c:if>
			<div class="gct">
				<input type="text" id="searchDate" class="date datePicker" style="width:160px; height:32px; border:1px solid #c0c0c0;"/>				
				<select id="allocateKey" class="select form" style="width: 228px;">
				</select>
			</div>
			<div class="gcb">
				<div class="gcb_sel">
					<select id="selectKind" class="select form" style="width: 160px;">
					</select>
					<div class="gc_input">
						<input id="inputCost" type="text" class="text" style="width:198px;" value="${reqVo.price }" />
						<span>원</span>
					</div>
				</div>
				<div class="gcb_area">
					<textarea id="inputNote" cols="30" rows="10" placeholder="메모">${reqVo.note }</textarea>
				</div>
			</div>
		</div>
		<!-- 버튼 -->
		<div class="btn_area2 mgt20">
		<a href="#" class="btn_cancle a_close">취소</a>
		<a href="#" class="a_edit">수정</a>
	</div>
</div>