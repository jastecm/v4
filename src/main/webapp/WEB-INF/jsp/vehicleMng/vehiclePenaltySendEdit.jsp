<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script type="text/javascript">
var lazy;
$(document).ready(function(){
	alert("step view");
	
	initMenuSel("M2003");
    
	$(".btn_drive_list").on("click",function(){
		alert("todo! - SEARCH");
		//$.lazyLoader.search(lazy);
	});
    
	if(!lazy)
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
				searchDrivingList : $("#searchDrivingList")
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 1
			,loadUrl : "/com/getVehicleList.do"
			,initSearch : true
			,createDataHtml : function(r){
				
				var strHtml = '';
				
				strHtml += '<tr>';
				strHtml += '<td><input type="radio" name="rdo_sel" class="rdo_sel"/></td>';
				strHtml += '<th class="car_list">';
				strHtml += '<strong class="car_img"><img src="${pathCommon}/images/vehicle/S53.png" alt="" /></strong>';
				strHtml += '<a href="#none" class=""><span>QM5</span></a>';
				strHtml += '</th><th>';
				strHtml += '<p><span>출발 </span>20xx.xx.xx xx:xx:xx 경기도 평택시 청북면 889-6</p>';
				strHtml += '<p><span>도착 </span>20xx.xx.xx xx:xx:xx 경기도 평택시 청북면 율북리 870-1</p>';
				strHtml += '</th>';
				strHtml += '<td class="good">정상</td></tr>';
				
				return strHtml+strHtml+strHtml+strHtml+strHtml+strHtml;

			}
		});
	
	$("body").on("click",".btn_close",function(){
		$VDAS.instance_post("/vehicleMng/vehiclePenalty.do",{});
	});
	
	$("body").on("click",".btn_cancle",function(){
		$VDAS.instance_post("/vehicleMng/vehiclePenalty.do",{});
	});
	
	$("body").on("click",".btn_edit",function(){
		$VDAS.instance_post("/vehicleMng/vehiclePenalty.do",{});
	});
});


</script>
<form id="searchFrm" style="display:none">
	<input type="hidden" name="intervalContent" value="" />
	<input type="hidden" name="intervalContentDesc" value="" />
	<input type="hidden" name="startDate" value="" />
	<input type="hidden" name="regulateH" value="" />
	<input type="hidden" name="regulateM" value="" />
	<input type="hidden" name="regulateType" value="" />
	<input type="hidden" name="regulateSubType" value="" />
	<input type="hidden" name="regulateReson" value="" />
	<input type="hidden" name="regulateAddr" value="" />
	<input type="hidden" name="regulateStation" value="" />
	<input type="hidden" name="regulatePayState" value="" />
	<input type="hidden" name="endDate" value="" />
	<input type="hidden" name="searchDrivingList" value="" />
</form>
<div class="right_layout">
	<div class="detail_view">
		<a href="javascript:window.history.back();" class="btn_close"><img src="${pathCommon}/images/close.jpg" alt="" /></a>
		<h2 class="h2tit">
			벌점/과태료 등록
		</h2>
		<div class="sub_top1 sub_top2" style="text-align:left; padding-left:20px;">
			<span>등록차량의 운행중 발생한 벌점/과태료 내용을 등록합니다.</span>
		</div>	
		<div class="user_detail pab0">
			<div class="user_detail_left">
				<!-- img size : 가로 345-->
				<img src="${pathCommon}/images/user_none.png" alt="" />
			</div>
			<ul class="car_detail_right2">
				<li>
					<div class="f_l" id="info1">
						<p id="corpNm"><span>소속  </span></p>
						<p id="userNm"><span>부서/이름 </span></p>
					</div>
					<div class="f_l" id="info2">
						<p id="phone"><span>전화번호 </span></p>
						<p id="mobilePhone"><span>휴대폰 </span></p>
						<p id="email"><span>이메일 </span></p>
					</div>
				</li>
				<li>
					<div class="car_info car_info_layout2" style="width:100% !important;">
						<div class="car_info5 car_info5_3">
							<span class="tit"  style="height:41px">개시일시</span>
							<div class="cal_layout" style="width:100% !important;">
								<div class="cal_area">
									<div><span id="intervalStartDate"></span>
									</div>
								</div>
							</div>
						</div>
						<div class="car_info5 car_info5_3">
							<span class="tit" style="height:41px">종료일시</span>
							<div class="cal_layout" style="width:100% !important;">
								<div class="cal_area">
									<div><span id="intervalStartDate"></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li>
					<p><span>업무명 </span><input type="text" class="input1" id="intervalContent" readonly placeholder="" value=""></p>
					<p><span>활용계획 </span><textarea class="textarea1" id="intervalContentDesc" readonly ></textarea></p>
				</li>
			</ul>
		</div>
		<div class="repair_layout2 repair_layout2_3">
			<div class="repair_box3">
				<div class="tit"><img src="${pathCommon}/images/${_LANG}/tit_img1.jpg" alt="운행내역" /></div>
				<div class="repair_box2_1 repair_box2_2">
					<span class="tit">운행내역</span><a href="#none" id="searchDrivingList" class="btn_drive_list mat0"><img src="${pathCommon}/images/btn_img1.png" alt="">운행내역 조회</a>
					<p class="text_2">
						ㆍ운행내역 조회버튼을 눌려, 보고할 운행내역을 선택하면 아래화면에 운행차량정보, 상세운행내역이 제공됩니다.<br>
						ㆍ운행내역 선택 후, 아래화면에서 벌점/과태료 정보를 입력해주세요.
					</p>
				</div>
			</div>
		</div>
		<div class="btn_delete2_area">
			<a href="#none" class="btn_delete2"><img src="${pathCommon}/images/${_LANG}/btn_delete.jpg" alt="" /></a>
		</div>
		<table class="table1 table1_1" id="contentsTable">
			<colgroup>
				<col style="width:55px;" />
				<col style="width:210px;" />
				<col />
				<col style="width:115px;" />
			</colgroup>
			<thead>
				<tr>
					<th>선택</th>
					<th>차량정보</th>							
					<th>운행정보</th>
					<th>차량상태</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		<div class="penalty_layout">
			<div class="penalty_tit"><img src="${pathCommon}/images/${_LANG}/h2_9.png" alt="벌점/과태료"></div>
			<div class="car_info">
				<div class="car_info5 car_info5_2 car_info5_3 borB1">
					<span class="tit">단속일시</span>
					<div class="cal_layout">
						<div class="cal_area">
							<div>
								<input type="text" id="startDate" class="date datePicker" style="width:65px; height:23px; border:1px solid #c0c0c0;"/>
								<div class="select_type5">
									<select class="sel" id="regulateH">
										<option value="0">0</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option value="0">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
										<option value="13">13</option>
										<option value="14">14</option>
										<option value="15">15</option>
										<option value="16">16</option>
										<option value="17">17</option>
										<option value="18">18</option>
										<option value="29">19</option>
										<option value="20">20</option>
										<option value="21">21</option>
										<option value="22">22</option>
										<option value="23">23</option>
									</select>
								</div><span>시</span>
								<div class="select_type5">
									<select class="sel" id="regulateM">
										<option value="0">0</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
										<option value="13">13</option>
										<option value="14">14</option>
										<option value="15">15</option>
										<option value="16">16</option>
										<option value="17">17</option>
										<option value="18">18</option>
										<option value="19">19</option>
										<option value="20">20</option>
										<option value="21">21</option>
										<option value="22">22</option>
										<option value="23">23</option>
										<option value="24">24</option>
										<option value="25">25</option>
										<option value="26">26</option>
										<option value="27">27</option>
										<option value="28">28</option>
										<option value="29">29</option>
										<option value="30">30</option>
										<option value="31">31</option>
										<option value="32">32</option>
										<option value="33">33</option>
										<option value="34">34</option>
										<option value="35">35</option>
										<option value="36">36</option>
										<option value="37">37</option>
										<option value="38">38</option>
										<option value="39">39</option>
										<option value="40">40</option>
										<option value="41">41</option>
										<option value="42">42</option>
										<option value="43">43</option>
										<option value="44">44</option>
										<option value="45">45</option>
										<option value="46">46</option>
										<option value="47">47</option>
										<option value="48">48</option>
										<option value="49">49</option>
										<option value="50">50</option>
										<option value="51">51</option>
										<option value="52">52</option>
										<option value="53">53</option>
										<option value="54">54</option>
										<option value="55">55</option>
										<option value="56">56</option>
										<option value="57">57</option>
										<option value="58">58</option>
										<option value="59">59</option>
									</select>
								</div><span>분</span>
							</div>
							<p>년.월.일 입력 ex)2016.01.05</p>
						</div>
					</div>
				</div>
				<div class="car_info4  car_info5_3">
					<span class="tit">단속내용</span><div class="car_info4_1">
						<div class="car_info4_2">
							<span class="tit2">구분</span>
							<div class="select_type6">
								<select id="sel" id="regulateType" style="width:99px;height:25px;">
									<option value="">선택</option>
									<option value="1">과태료</option>
									<option value="2">벌점</option>
								</select>
							</div>
							<span class="tit2" style="margin-left:20px">항목</span>
							<div class="select_type6">
								<select id="sel" id="regulateSubType" style="width:99px;height:25px;">
									<option value="">선택</option>
									<option value="1">신호위반</option>
									<option value="2">주차위반</option>
									<option value="3">속도위반</option>
								</select>
							</div>
						</div>
						<div class="car_info4_3">													
							<span class="tit2">사유</span><textarea id="regulateReson" placeholder="위반사유를 자세하게 입력하세요."></textarea>
						</div>
					</div>
				</div>
				<div class="car_info4  car_info5_3"><span class="tit">단속장소</span>
					<div class="car_info4_1">
						<div class="car_info4_2">
							<input type="text" id="regulateAddr" placeholder="고지서의 단속장소를 정확하게 입력하세요." class="p_input1" value=""/>
							<span class="tit2" style="margin-left:20px">관할</span><input type="text" id="regulateStation" placeholder="고지서의 관할처를 정확하게 입력하세요." class="p_input1" value=""/>
						</div>
					</div>
				</div>
				<div class="car_info4 car_info5_3"><span class="tit">고지서</span>
					<div class="car_info4_2"><span class="tit3">고지서첨부</span>
						<fieldset style="border:0">
							<form action="" class="P10">
								<div class="btn_file"><input type="file" id="regulateFine" class="multi" accept="jpg|gif|png" maxlength="1"/></div>
							</form>
						</fieldset>
					</div>
				</div>
				<div class="car_info5 car_info5_2 car_info5_3">
					<span class="tit">납부</span><span class="tit2">여부</span><div class="select_type6">
					<div class="select_type6">
						<select id="sel" id="regulatePayState" style="width:99px;height:25px;">
							<option value="">선택</option>
							<option value="1">납부완료</option>
							<option value="2">납부예정</option>
							<option value="3">미납</option>
						</select>
					</div>
					</div><span class="tit4" style="margin-left:30px">마감일</span>
					<div class="cal_layout">
						<div class="cal_area">
							<div>
								<input type="text" id="endDate" class="date datePicker" style="width:99px; height:23px; border:1px solid #c0c0c0;"/>
							</div>
							<p>년.월.일 입력 ex)2016.01.05</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<br><br>
		<div class="return_btn">
			<a href="#none" class="btn_cancle">취소</a>
			<a href="#none" class="btn_edit">수정</a>
		</div>
	</div>
</div>