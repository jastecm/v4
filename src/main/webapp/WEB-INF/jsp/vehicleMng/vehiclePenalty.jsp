<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>


<script type="text/javascript">
var lazy;
var lazyUser;
$(document).ready(function(){
	alert("step view");
	
	initMenuSel("M2003");

	$(".btn_search").on("click",function(){
		alert("todo! - SEARCH");
		//$.lazyLoader.search(lazy);
	});
	
	$("#searchText").on("keyup",function(e){
		if(e.keyCode == 13) $(".btn_search").trigger("click");
	});
	/*
	$("body").on("click",".check_box_1",function(){
		if($(this).hasClass("on")){
			$(this).removeClass("on");
		}else{
			$(this).addClass("on");
		}
	});*/
	
	$("body").on("click",".a_reg",function(){
		$VDAS.instance_post("/vehicleMng/vehiclePenaltySend.do",{});
	});
	
	$("body").on("click",".a_detail",function(){
		$VDAS.instance_post("/vehicleMng/vehiclePenaltySendEdit.do",{});
	});
	
	if(!lazy)
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
				searchType : $("#searchType")
				,searchText : $("#searchText")
				,searchOrder : $("#searchOrder")
				,searchPeriod : $("#searchPeriod")
				,startDate : $("#startDate")
				,endDate : $("#endDate")
				,selectKind : $("#selectKind")
				,selectViolation : $("#selectViolation")
				,selectPayState : $("#selectPayState")
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/com/getVehicleList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.rtvList;
				
				var strHtml = '';

				strHtml += '<tr>';
				strHtml += '<td><span class="check_box check_box_1"><input type="checkbox" class="checkboxes delChk" data-id="17"/></span></td>';
				strHtml += '<td>과태료</td>';
				strHtml += '<td>신호위반</td>';
				strHtml += '<th>';
				strHtml += '<div class="po_real">';
				strHtml += '<p><span>일시 </span>20xx-xx-xx xx:00</p>';
				strHtml += '<p><span>장소 </span>test/test</p>';
				strHtml += '<a href="#none" class="btn_gps a_detail" data-id="17"><img src="${pathCommon}/images/btn3.jpg" alt=""></a>';
				strHtml += '</div>';
				strHtml += '</th>';
				strHtml += '<td>';
				strHtml += '<a href="#" class="btn_vehicleDetail">';
				strHtml += '<span>QM5</span>';
				strHtml += '50호9926';
				strHtml += '</a>';
				strHtml += '</td>';
				strHtml += '<td>';
				strHtml += '<a href="#" class="btn_userDetail">';
				strHtml += '<span>개인</span>';
				strHtml += '/박태수';
				strHtml += '</a>';
				strHtml += '</td>';
				strHtml += '<td>';
				strHtml += '<a href="#"><img src="${pathCommon}/images/btn6.jpg" alt="" /></a>';
				strHtml += '</td>';
				strHtml += '<td>';
				strHtml += '<span class="pa	yment_ok">납부예정</span>';
				strHtml += '</td>';
				strHtml += '</tr>';

				return strHtml+strHtml+strHtml+strHtml+strHtml+strHtml+strHtml;
				
			}
		});
	//left menu on
	
	$("body").on("click",".btn_vehicleDetail",function(){
		$VDAS.instance_post("/drivingMng/searchVehicleDetail.do",{});
	});

	$("body").on("click",".btn_userDetail",function(){
		$VDAS.instance_post("/drivingMng/searchUserDetail.do",{});
	});
	
	$("body").on("click",".tab_select_btn1 a",function(){
		$(".tab_select_btn1 a").removeClass("on");
		$(this).addClass("on");
	});
});


</script>
<form id="searchFrm" style="display:none">
	<input type="hidden" name="searchType" value="" />
	<input type="hidden" name="searchText" value="" />
	<input type="hidden" name="searchOrder" value="" />
	<input type="hidden" name="searchPeriod" value="" />
	<input type="hidden" name="startDate" value="" />
	<input type="hidden" name="endDate" value="" />
	<input type="hidden" name="selectKind" value="" />
	<input type="hidden" name="selectViolation" value="" />
	<input type="hidden" name="selectPayState" value="" />
</form>
<div class="right_layout">
	<h2 class="tit_sub">
		벌점/과태료 조회·등록
		<p>등록차량의 운행중 발생한 벌점·과태료를 틍록하거나 조회합니다.</p>
	</h2>
	<div class="sub_top1">
		<span>등록차량수<b>xx대</b></span><span>벌금 부과차량<b>xx대</b></span><span>과태료 부과차량<b>xx대</b></span>
		<strong id="nowDate"></strong>
	</div>
	<h3 class="h3tit">벌점/과태료 등록</h3>
	<div class="box1">
		등록차량의 운행중 발생한 벌점/과태료 내용을 등록합니다.
		<a href="#" class="a_reg">벌점/과태료등록</a>
	</div>
	<h3 class="h3tit">벌점/과태료 조회</h3>
	<div class="search_box1">
		<form id="frmSearch" method="post" onSubmit="return false;">
			<div class="search_box1_1">
				<div class="select_type1">
					<div class="select_type1_1">
						<select class="sel" id="searchType">
							<option value="">선택</option>
							<option value="plateNum">차량번호</option>
							<option value="modelMaster">차종</option>
							<option value="useNm">사용자명</option>
						</select>
					</div>
				</div><input type="text" id="searchText" class="search_input" />
			</div><a href="#none" class="btn_search">조회</a>
		</form>
	</div>
	<div style="margin-bottom: 10px;">
		<select id="searchOrder" class="select form" style="width:105px;">
			<option value="regDate">등록순</option>
			<option value="policeDate">단속일순</option>
		</select>
		<div class="tab_select tab_select2">
			<span class="tab_select_btn1" id="searchPeriod">
				<a href="#" id="entire" class="on">전체</a>
				<a href="#" id="today">오늘</a>
				<a href="#" id="weekly">1주일</a>
				<a href="#" id="month">1개월</a>
				<a href="#" id="threemonth">3개월</a>
				<a href="#" id="halfyear">6개월</a>
			</span><a href="#none" class="btn_cal">직접입력</a>
			<div></div>
			<div class="cal_layout">
				<div class="cal_area">
					<input type="text" id="startDate" class="date datePicker" style="width:65px; height:25px; border:1px solid #c0c0c0;"/>
					<span>~</span>
					<input type="text" id="endDate" class="date datePicker" style="width:65px; height:25px; border:1px solid #c0c0c0;"/><br/>
				</div>
				<a href="#none" class="btn_enter">적용</a>
				<br><br><p>년,월,일순으로 입력 ex)2016.01.05</p>
			</div>
		</div>
		<div class="btn_area3" style="float:right;">
			<a href="#none" id="btn_del"><img src="${pathCommon}/images/delete.png" alt="" />삭제</a>
		</div>
	</div>
	<table class="table1" id="contentsTable">
		<colgroup>
			<col style="width:37px;" />
			<col style="width:70px;" />
			<col style="width:80px;" />
			<col />
			<col style="width:110px;" />
			<col style="width:130px;" />
			<col style="width:60px;" />
			<col style="width:95px;" />
		</colgroup>
		<thead>
			<tr>
				<th>
					<span class="check_box chk_all"><input type="checkbox" id="checker" /></span>
				</th>
				<th>
					<select class='th_select' id="selectKind" style="width:65px;font-size:11px;">
						<option value="">구분</option>
						<option value="1">벌점</option>
						<option value="2">과태료</option>
					</select>
				</th>
				<th>
					<select class='th_select' id="selectViolation" style="width:75px;font-size:11px;">
						<option value="">항목</option>
						<option value="1">신호위반</option>
						<option value="2">주차위반</option>
						<option value="3">속도위반</option>
					</select>
				</th>
				<th>단속정보</th>
				<th>차량정보</th>
				<th>최근 사용자</th>
				<th>고지서</th>
				<th>
					<select class='th_select' id="selectPayState" style="width:80px;font-size:11px;">
						<option value="">납부</option>
						<option value="1">납부완료</option>
						<option value="2">납부예정</option>
						<option value="3">미납</option>
					</select>
				</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
	<div class="txt1">
		<h1>납부 표기 안내</h1>
		<p><b>납부완료 </b>과태료가 정상적으로 납부되었습니다.</p>
		<p><b>납부예정 </b>납부예정일이 남아있는 미납상태입니다.</p>
		<p><b>미납 </b>납부예정일이 지났습니다.</p>
	</div>
</div>