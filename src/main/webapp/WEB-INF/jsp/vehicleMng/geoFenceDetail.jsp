<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<style>.svg-clipped{-webkit-clip-path:url(#svgPath);clip-path:url(#svgPath);}</style>
<svg height="0" width="0">
	<defs>
		<clipPath id="svgPath" clipPathUnits="objectBoundingBox">
  			<circle cx=".5" cy=".5" r=".5" />
   		</clipPath>
	</defs>
</svg>
<script type="text/javascript">
var lazy;
$(document).ready(function(){
	initMenuSel("M2003");
/*
	$(".btn_search").on("click",function(){
		$.lazyLoader.search(lazy);
	});
	
	if(!lazy)
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
				searchMonth : $("#searchMonth")
			}
			,setSearchFunc : function(){
				
			}
			,scrollRow : 5
			,rowHeight : 50
			,limit : 10			
			,loadUrl : "/vehicleMng/getGeofenceUserList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.result;				
				var strHtml = '';
				
				console.log(data);
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += '<tr>';
					strHtml += '<th class="">';
					if(vo.imgId&&vo.imgId.length > 0)
						strHtml += '<strong class="car_img" style="padding-left:15px;"><img class="svg-clipped" style="width:49px;height:49px;" src="${defaultPath}/com/getUserImg.do?uuid='+vo.imgId+'" alt="" /></strong>';
					else strHtml += '<strong class="car_img" style="padding-left:15px;"><img class="svg-clipped" src="${pathCommon}/images/user_img7.jpg" alt="" style="width:49px;height:49px;"/></strong>';
					strHtml += '<a href="#none" class="btn_userDetail" data-key="'+vo.accountKey+'">';
					strHtml += '<span>'+vo.accountNm+'</span>';
					strHtml += vo.name;
					strHtml += '</a>';
					strHtml += '</th>';
					strHtml += '<td>'+((vo.corpGroupNm)?vo.corpGroupNm:"")+'/'+((vo.corpPosition)?vo.corpPosition:"")+'</td>';					
					strHtml += '<th class="">';					
					strHtml += '<strong class="car_img" style="padding-left:15px;"><img src="${path}/common/images/vehicle/'+vo.imgS+'.png" alt="" /></strong>';
					strHtml += '<a href="#none" class="btn_vehicleDetail" data-key="'+vo.vehicleKey+'">';
					strHtml += '<span>'+vo.modelMaster+'</span>';
					strHtml += vo.plateNum;
					strHtml += '</a>';
					strHtml += '</th>';
					strHtml += '<td><a href="#none" class="btnDetail" data-key='+vo.accountKey+'><img src="${path}/common/images/btn3.jpg" alt=""></a></td>';
					strHtml += '</tr>';
					
				}
				
				return strHtml;

			}
		});
	
	
	$("body").on("click",".btnDetail",function(){
		var key = $(this).data("key");
		$VDAS.instance_post("/vehicleMng/geoFenceDetail.do",{accountKey:key});
	});
	*/
	
	$VDAS.http_post("/user/getUserInfo.do",{accountKey:"${accountKey}"},{		
		success : function(r){
			var obj = r.result;

			console.log(obj);

			if(obj != null && obj != ''){
				var drivingStateHtml ="";
				if(obj.vehicleState == "2")  
					drivingStateHtml += '<span class="ico7">';
				else drivingStateHtml += '<span class="ico'+obj.accountState+'">';	
				switch(obj.accountState){
					case "1":
						drivingStateHtml += '주행중';
						break;
					case "2":
						drivingStateHtml += '운행대기';
						break;
					case "3":
						drivingStateHtml += '주차중';
						break;
					case "4":
						drivingStateHtml += '미배차';
						break;
				}
				drivingStateHtml += '</span>';
				
				$("#drivingState").after(drivingStateHtml);
				if(obj.imgId&&obj.imgId.length > 0)
					$(".user_detail_left").append('<img class="svg-clipped" src="${defaultPath}/com/getUserImg.do?uuid='+obj.imgId+'" alt="" />');
				else $(".user_detail_left").append('<img class="svg-clipped" src="${pathCommon}/images/user_img7.jpg" alt=""/>');
				$("#corpGroup").after(""+convertNullString(obj.corpNm));
				$("#position").after(""+convertNullString(obj.corpPosition)+"/"+convertNullString(obj.name));
				$("#phone").after(""+convertNullString(obj.phone));
				$("#mobilePhone").after(""+convertNullString(obj.mobilePhone));
				$("#email").after(""+convertNullString(obj.authMail));
			}
		}
	});
	
	if(!lazy)
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
				searchState : $("#sel_chk") 
			}
			,setSearchFunc : function(){
				
			}
			,scrollRow : 5
			,rowHeight : 86
			,limit : 10			
			,loadUrl : "/vehicleMng/getGeofenceUserDetail.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.result;				
				var strHtml = '';
				
				console.log(data);
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += '<tr>';
					strHtml += '<td>';
					if(vo.chk == '1')
						strHtml += '<b>진입</b>';
					if(vo.chk == '0')
						strHtml += '<b>이탈</b>';					
					strHtml += '</td>';
					strHtml += '<td>';
					strHtml += vo.regDate;
					strHtml += '</td>';
					strHtml += '<td>';
					strHtml += vo.alies;
					strHtml += '</td>';
					strHtml += '<td>';
					strHtml += addrString(vo.addr);
					strHtml += '</td>';
					strHtml += '<th class="">';					
					strHtml += '<strong class="car_img" style="padding-left:15px;"><img src="${path}/common/images/vehicle/'+vo.imgS+'.png" alt="" /></strong>';
					strHtml += '<a href="#none" class="btn_vehicleDetail" data-key="'+vo.vehicleKey+'">';
					strHtml += '<span>'+vo.modelMaster+'</span>';
					strHtml += vo.plateNum;
					strHtml += '</a>';
					strHtml += '</th>';
					strHtml += '<td>';
					strHtml += '<a href="#none" class="btn_del" data-key="'+vo.fenceHstrKey+'"><img src="${pathCommon}/images/${_LANG}/btn_delete.jpg"></a>';
					strHtml += '</td>';
					strHtml += '</tr>';
					
				}
				
				return strHtml;

			}
		});
	
	$("body").on("click",".btn_del",function(){
		var _this = $(this);
		var key = $(this).data("key");
		
		$VDAS.http_post("/vehicleMng/delGeofenceHstr.do",{fenceKey:key},{
			success : function(r){
				_this.closest("tr").remove();
				$.lazyLoader.resize(lazy);
			},error : function(r){
				if($message[r.responseJSON.result])
					alert($message[r.responseJSON.result]);
				else {
					alert(r.status+"\n"+r.statusText);
				}
			}
		});
		
	});
	
	$("#sel_chk").on("change",function(){
		$.lazyLoader.search(lazy);
	});
	
	$("body").on("click",".btn_vehicleDetail",function(){
		var key = $(this).data("key");
		$VDAS.instance_post("/drivingMng/searchVehicleDetail.do",{vehicleKey:key});
	});
});

function addrString(a){
	var result = '';

	for(var i=0;i < a.length;i++){
		if(i >= 18){
			result += '...';
			break;
		}
		else
			result += a[i];
	}
		
	return result;
}

</script>
<div class="right_layout">
	<form id="searchFrm" method="post" onSubmit="return false;">
		<input type="hidden" name="accountKey" value="${accountKey}" />
		<input type="hidden" name="searchState" value="" />
	</form>
	<div class="detail_view">
		<a href="javascript:window.history.back();" class="btn_close"><img src="${pathCommon}/images/close.jpg" alt="" /></a>
		<h2 class="h2tit">
			운행 스케쥴 상세정보
		</h2>
		<div class="step_box sub_top1" style="text-align:left; padding:7px 12px 6px 21px;">
			<span style="padding-top:5px;" id="drivingState">운행여부</span>
			<strong id="nowDate" style="padding-top:5px;"></strong>
		</div>
		<div class="user_detail" style="padding-top:17px;">
			<div class="user_detail_left">
				<!-- img size : 가로 345-->
			</div>
			<ul class="car_detail_right">
				<li>
					<div>
						<p><span id="corpGroup">소속/지점 </span></p>
						<p><span id="position">부서/이름 </span></p>
					</div>
					<div>
						<p><span id="phone">전화번호 </span></p>
						<p><span id="mobilePhone">휴대폰 </span></p>
						<p><span id="email">이메일 </span></p>
					</div>
				</li>
			</ul>
		</div>
		<div class="tab_layout">
			<table class="table1" id="contentsTable" >
				<colgroup>
					<col style="width:60px;" />
					<col style="width:105px;" />
					<col style="width:115px;" />
					<col style="width:*;" />
					<col style="width:190px;" />
					<col style="width:70px;" />
				</colgroup>
				<thead>
					<tr>
						<th>
							<select id="sel_chk" class="select form">
								<option value="">상태</option>							
								<option value="1">진입</option>							
								<option value="0">이탈</option>							
							</select> 
						</th>
						<th>시간</th>
						<th>거래처명</th>
						<th>주소</th>
						<th>이용차량</th>
						<th>삭제</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>