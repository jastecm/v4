<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script type="text/javascript">
var lazy;
var lazyManage;
$(document).ready(function(){
	initMenuSel("M2002");
	
	$("body").on("click",".a_exit",function(){
		$VDAS.instance_post("/vehicleMng/vehicleFee.do",{});
	});
	
	$("#priceResult").text(number_format($("#price").val())+"원");
	
	var note = replaceAll($("#note").val(),"\n","<br/>");
	console.log(note);
	$(".detailNote").html(""+convertNullString(note));
	
	<c:if test="${VDAS.rule eq '0' or VDAS.rule eq '1' }">
	
	$("body").on("click",".a_cancle",function(){
		var key = $(this).data("key");
		$VDAS.http_post("/allocate/updateFeeState.do",{expenseKey:key,state:"1"},{
			success : function(){
				$VDAS.instance_post("/vehicleMng/vehicleFee.do",{});
			}
		});
	});
	$("body").on("click",".a_del",function(){
		var key = $(this).data("key");
		$VDAS.http_post("/allocate/deleteFee.do",{expenseKey:key},{
			success:function(){
				$VDAS.instance_post("/vehicleMng/vehicleFee.do",{});
			}
		});
	});
	
	</c:if>
});


</script>
<input type="hidden" id="price" value="${reqVo.price }">
<input type="hidden" id="note" value="${reqVo.note }">
<div class="right_layout">
	<div class="detail_view">
		<h2 class="h2tit">차량경비 확인</h2>
		<div class="gray_box">
			<p>승인이 완료된 후, 경비 수정은 불가능합니다.</p>
		</div>
		<div class="dl_wrap">
			<ul class="dl_text">
				<li>
					<c:if test="${reqVo.state eq '1'}">
						<span class="black_icon mgr10">미승인</span> ${reqVo.stateDate }
					</c:if>
					<c:if test="${reqVo.state eq '2'}">
						<span class="blue_icon mgr10">승인</span> ${reqVo.stateDate }
					</c:if>
					<c:if test="${reqVo.state eq '3'}">
						<span class="red_icon mgr10">반려</span> ${reqVo.stateDate }
					</c:if>
				</li>
				<c:if test="${reqVo.state eq '3'}">
					<li>
						<dl class="dl_wid">
						<dt>반려 사유</dt>
						<dd>${reqVo.rejectNote}</dd>
					</li>
				</c:if>
				<li>
					<dl class="dl_wid">
						<dt>배차 시작</dt>
						<dd>${reqVo.startDate }</dd>
						<dt>업무명</dt>
						<dd style="width:inherit;">${reqVo.title }</dd>
					</dl>
					<dl class="dl_wid">
						<dt>배차 종료</dt>
						<dd>${reqVo.endDate }</dd>
						<dt>사용차량</dt>
						<dd style="width:inherit;">${reqVo.modelMaster }/${reqVo.plateNum }</dd>
					</dl>
					<dl class="dl_wid">
						<dt>사용일</dt>
						<dd>${reqVo.allocateDate }</dd>						
					</dl>
				</li>
				<li>
					<dl>
						<dt>항목</dt>
						<dd>${reqVo.expenseNm }</dd>
					</dl>
					<dl>
						<dt>금액</dt>
						<dd id="priceResult"></dd>
					</dl>
					<dl>
						<dt>메모</dt>
						<dd class="detailNote"></dd>
					</dl>
					<dl class="mgt35">
						<dt>작성일</dt>
						<dd>${reqVo.regDate }</dd>
					</dl>
				</li>
			</ul>
		</div>
		<!-- 버튼 -->
		<div class="btn_area2 mgt20"><a href="#" class="a_exit">확인</a><c:if test="${(VDAS.rule eq '0' or VDAS.rule eq '1')}">
			 <c:if test="${reqVo.state eq '2'}"><a href="#" class="a_cancle" data-key='${reqVo.expenseKey}' style="background:#4e4e4e">승인취소</a></c:if>
			 <c:if test="${reqVo.state eq '3'}"><a href="#" class="a_cancle" data-key='${reqVo.expenseKey}' style="background:#4e4e4e">반려취소</a></c:if>
			 <a href="#" class="a_del" data-key='${reqVo.expenseKey}' style="background:#4e4e4e">삭제</a>
		</c:if></div>
	</div>
</div>