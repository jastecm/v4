<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script type="text/javascript">
var lazy;
var lazyManage;
$(document).ready(function(){
	
	initMenuSel("M2002");
	
	$VDAS.ajaxCreateSelect("/com/getGroupCode.do",{},$("#corpGroup"),false,"전체 부서","${rtv.groupKey}","${_LANG}",null,function(){
		$VDAS.ajaxCreateSelect("/com/getGroupUserList.do",{corpGroup:"${rtv.groupKey}"},$("#accountKey"),false,"전체 사용자","${rtv.accountKey}","${_LANG}",null,null);
	});
	
	$("#corpGroup").on("change",function(){
		$VDAS.ajaxCreateSelect("/com/getGroupUserList.do",{corpGroup:$(this).val()},$("#accountKey"),false,"전체 사용자","","${_LANG}",null,null);	
	});
	
	$(".btn_search").on("click",function(){
		$("#searchFrm input[name=searchDate]").val($("#searchDate").val()?replaceAll($("#searchDate").val(),"-","")+"01":"");
		$.lazyLoader.search(lazy);
		feeSummary();
	});
	
	$("#searchOrder, #searchState").on("change",function(){
		$.lazyLoader.search(lazy);
		feeSummary();
	});
	
	$("#searchText").on("keyup",function(e){
		if(e.keyCode == 13) $(".btn_search").trigger("click");
	});

	$("body").on("click",".a_check",function(){
		var key = $(this).data("key");
		$VDAS.instance_post("/vehicleMng/vehicleFeeDetail.do",{expenseKey:key,code:"C018"});
	});

	$("body").on("click",".a_reg",function(){
		if($(this).data("target") == "admin")
			$VDAS.instance_post("/vehicleMng/vehicleFeeRegisterAdmin.do",{});
		else
			$VDAS.instance_post("/vehicleMng/vehicleFeeRegister.do",{});
	});
	
	$("body").on("click",".btn_pop",function(){
		var temp = $(this).data("state");
		var key = $(this).data("key");
		var state = temp.split(" ");
		popContents(key);
		if(state[0] == "1") $("."+state[1]).fadeIn();
		else if(state[0] == "2") $(".acceptResult").fadeIn();
		else if(state[0] == "3") $(".turnDownResult").fadeIn();
		$(".black").fadeIn();
	});

	if(!lazy)
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
				accountKey:$("#accountKey")
				,searchText:$("#searchText")
				,searchGroup:$("#corpGroup")
				,searchOrder:$("#searchOrder")
				,searchState:$("#searchState")
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/allocate/getFeeList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.result;
				console.log(data);
				var strHtml = "";
				for(var i = 0 ; i < data.length; i++){
					var vo = data[i];
					
					var stateDate = ["",""];
					if(vo.stateDate != null && vo.stateDate != '')
						stateDate = vo.stateDate.split(" ");
					
					strHtml += '<tr>';
					strHtml += '<td>';
					strHtml += '<ul class="sylist">';
					if(vo.state == '1'){
						strHtml += '<li>';
						strHtml += '<a href="#none" class="btn_pop" data-key="'+vo.expenseKey+'" data-state="'+vo.state+' accept" style="margin:0px;">';
						strHtml += '<strong>승인</strong>';
						strHtml += '</a>';
						strHtml += '<br />';
						strHtml += '<span>-</span>';
						strHtml += '</li>';
						strHtml += '<li>';
						strHtml += '<a href="#none" class="btn_pop" data-key="'+vo.expenseKey+'" data-state="'+vo.state+' turnDown" style="margin:0px;">';
						strHtml += '<strong>반려</strong>';
						strHtml += '</a>';
						strHtml += '<br />';
						strHtml += '<span>-</span>';
						strHtml += '</li>';
					}
					else if(vo.state == '2'){
						strHtml += '<li>';
						strHtml += '<a href="#none" class="btn_pop" data-key="'+vo.expenseKey+'" data-state="'+vo.state+' " style="margin:0px;">';
						strHtml += '<strong class="color_red">승인</strong>';
						strHtml += '</a>';
						strHtml += '<br />';
						strHtml += '<span style="font-weight:normal;font-size:11px;">'+stateDate[0]+'<br />'+stateDate[1]+'</span>';
						strHtml += '</li>';
						strHtml += '<li class="off">';
						strHtml += '<strong>반려</strong><br />';
						strHtml += '<span>-</span>';
						strHtml += '</li>';
					}
					else if(vo.state == '3'){
						strHtml += '<li class="off">';
						strHtml += '<strong>승인</strong><br />';
						strHtml += '<span>-</span>';
						strHtml += '</li>';
						strHtml += '<li>';
						strHtml += '<a href="#none" class="btn_pop" data-key="'+vo.expenseKey+'" data-state="'+vo.state+' " style="margin:0px;">';
						strHtml += '<strong class="color_red">반려</strong>';
						strHtml += '</a>';
						strHtml += '<br />';
						strHtml += '<span style="font-weight:normal;font-size:11px;">'+stateDate[0]+'<br />'+stateDate[1]+'</span>';
						strHtml += '</li>';
					}
					strHtml += '</ul>';
					strHtml += '</td>';
					strHtml += '<td class="align_left">';
					strHtml += '<b>부서</b> '+convertNullString(vo.corpGroupNm)+'<br />';
					strHtml += '<b>이름</b> '+vo.name;
					strHtml += '</td>';
					strHtml += '<td class="align_left">';
					strHtml += '<b>시작</b> '+convertNullString(vo.startDate)+'<br />';
					strHtml += '<b>종료</b> '+convertNullString(vo.endDate)+'<br />';
					strHtml += '<b>사용일</b> '+vo.allocateDate;
					strHtml += '</td>';
					strHtml += '<td class="align_left">';
					strHtml += '<b>업무명</b> '+convertNullString(vo.title)+'<br />';
					strHtml += '<b>사용차량</b> '+convertNullString(vo.modelMaster);
					strHtml += '</td>';
					strHtml += '<td class="align_left">';
					$VDAS.http_post("/com/getCode.do",{code:"C018", val:vo.expenseType},{
						sync : true
						,success : function(r){
							var code = r.rtvList[0];
							strHtml += '<b>항목</b> <em class="color_red">'+code.codeName+'</em><br />';
						}
					});
					strHtml += '<b>금액</b> <em class="color_red">'+number_format(vo.price)+'원</em>';
					strHtml += '</td>';
					strHtml += '<td><a href="#none" class="a_check" data-key="'+vo.expenseKey+'"><img src="${pathCommon}/images/btn3.jpg" alt=""></a></td>';
					strHtml += '</tr>';
				}
				
				return strHtml;
			}
		});
	$("body").on("click",".btn_cancel",function(){
		$(".pop1, .pop4").fadeOut();
	});
	
	$("body").on("click",".btn_accept",function(){
		var key = $(this).data("key");
		$VDAS.http_post("/allocate/updateFeeState.do",{expenseKey:key,state:"2"},{
			success : function(){
				$.lazyLoader.search(lazy);
				feeSummary();
				$(".accept").fadeOut();
			}
		});
	});
	
	$("body").on("click",".btn_turnDown",function(){
		var key = $(this).data("key");
		var rejectNote = $(".rejectNote").val();
		if(rejectNote != null && rejectNote != ""){
			$VDAS.http_post("/allocate/updateFeeState.do",{expenseKey:key,rejectNote:rejectNote,state:"3"},{
				success : function(){
					$.lazyLoader.search(lazy);
					feeSummary();
					$(".turnDown").fadeOut();
				}
			});
		}
		else {
			$(".black").fadeIn();
			alert("반려사유를 작성해주세요.");
		}
	});
	
	$("body").on("click",".btn_ok",function(){
		$(".turnDownResult, .acceptResult").fadeOut();
	});
	
	feeSummary();
});


function popContents(key){
	$VDAS.http_post("/allocate/getFeeInfo.do",{expenseKey:key,code:"C018"},{
		success : function(r){
			var data = r.result;
			console.log(data);
			$(".detailGroup").text(""+convertNullString(data.corpGroupNm));
			$(".detailNm").text(""+convertNullString(data.name));
			$(".detailStartDate").text(""+convertNullString(data.startDate));
			$(".detailTitle").text(""+convertNullString(data.title));
			$(".detailEndDate").text(""+convertNullString(data.endDate));
			$(".detailEnpenseType").text(""+data.expenseNm);
			$(".detailVehicle").text(""+data.modelMaster+"/"+data.plateNum);
			$(".detailPrice").text(""+number_format(data.price)+"원");
			console.log(data.note);
			var note = replaceAll(data.note,"\n","<br/>");
			$(".detailNote").html(""+convertNullString(note));
			$(".detailDate").text(""+convertNullString(data.regDate));
			$(".rejectReson").val(""+convertNullString(data.rejectNote));
			$(".rejectNote").val("");
			$(".btn_ok").data("key",data.expenseKey);
			
		}
	});
	        
}

function feeSummary(){
	$VDAS.http_post("/allocate/getExpenseList.do",$("#searchFrm").serialize(),{
		success : function(r){
			var data = r.result;
			var totalMain = 0;
			var totalEtc = 0;
			$("#refuel").text(" 0원");
			$("#clinic").text(" 0원");
			$("#parking").text(" 0원");
			$("#toll").text(" 0원");
			$("#rental").text(" 0원");
			$("#wash").text(" 0원");
			$("#penalty").text(" 0원");
			$("#etc").text(" 0원");
			for(var i = 0;i < data.length;i++){
				var vo = data[i];
				console.log(vo);
				switch(vo.expenseType){
					case "1":		// 주유
						$("#refuel").text(" "+number_format(vo.price)+"원");
						totalMain += parseInt(vo.price);
						break;
					case "2":		// 정비
						$("#clinic").text(" "+number_format(vo.price)+"원");
						totalMain += parseInt(vo.price);
						break;
					case "3":		// 주차
						$("#parking").text(" "+number_format(vo.price)+"원");
						totalMain += parseInt(vo.price);
						break;
					case "4":		// 통행료
						$("#toll").text(" "+number_format(vo.price)+"원");
						totalMain += parseInt(vo.price);
						break;
					case "5":		// 렌탈료
						$("#rental").text(" "+number_format(vo.price)+"원");
						totalEtc += parseInt(vo.price);
						break;
					case "6":		// 세차
						$("#wash").text(" "+number_format(vo.price)+"원");
						totalEtc += parseInt(vo.price);
						break;
					case "7":		// 과태료
						$("#penalty").text(" "+number_format(vo.price)+"원");
						totalEtc += parseInt(vo.price);
						break;
					case "8":		// 기타
						$("#etc").text(" "+number_format(vo.price)+"원");
						totalEtc += parseInt(vo.price);
						break;
				}
			}
			$(".totalMain").text("= "+number_format(totalMain)+"원");
			$(".totalEtc").text("= "+number_format(totalEtc)+"원");
			$(".total").text("= "+number_format(totalMain+totalEtc)+"원");
		}
	});
	
}
</script>
<form id="searchFrm">
	<input type="hidden" name="searchDate" value="" />
	<input type="hidden" name="accountKey" value="" />
	<input type="hidden" name="searchGroup" value="" />
	<input type="hidden" name="searchText" value="" />
	<input type="hidden" name="searchOrder" value="" />
	<input type="hidden" name="searchState" value="" />
	<input type="hidden" name="searchType" value="" />
	<input type="hidden" name="rejectNote" value="" />
</form>
<div class="right_layout">
	<h2 class="tit_sub">
		차량경비 조회
		<p>등록차량의 모든 경비내역을 관리합니다.</p>
	</h2>
	<div class="sub_top1">
		<strong id="nowDate"></strong>
	</div>
	<h3 class="h3tit">경비 등록</h3>
	<div class="box1">
		새로운 경비를 등록하시려면 우측 경비 등록하기 버튼을 눌려 신청서를 작성해주세요.
		<a href="#" class="a_reg" data-target='admin' style="right:142px">유지비 등록</a>
		<a href="#" class="a_reg">경비 등록</a>
	</div>
	<h3 class="h3tit">경비 신청 조회·수정</h3>
	<!-- 관리자 -->
	<div class="search_box1">	
		<input type="text" style="width:85px; height:36px; border:1px solid #c0c0c0;" height:36px;" id="searchDate" class="date datePickerMonth" name="vehicleRegDate" value=""/>
		<select class="select form" id="corpGroup" style="width:160px;">
			<option value="">전체 부서</option>
		</select>
		<select class="select form" id="accountKey" style="width:160px;">
			<option value="">전체 사용자</option>
		</select>
		<!-- <div class="search_box1_2" style="width:200px;">
			<input type="text" id="searchText" class="search_input" />
		</div> -->
		<a href="#none" class="btn_search">조회</a>
	</div>
	<div class="txtbox">
		<p>
			<strong>①</strong> 
			<strong>주차</strong><b id="parking"> 0원</b>
			+ 
			<strong>통행료</strong><b id="toll"> 0원</b>
			+ 
			<strong>주유</strong><b id="refuel"> 0원</b>
			+ 
			<strong>정비</strong><b id="clinic"> 0원</b>
			
			<strong class="color_red totalMain"> 0원</strong>
		</p>
		<p>
			<strong>②</strong> 
			<strong>세차</strong> <b id="wash"> 0원</b>
			+ 
			<strong>렌탈료</strong> <b id="rental"> 0원</b>
			+ 
			<strong>과태료 </strong> <b id="penalty"> 0원</b>
			+ 
			<strong>기타</strong> <b id="etc"> 0원</b>
			
			<strong class="color_red totalEtc"> 0원</strong>
		</p>
	</div>
	<div class="txtbox2">
		<strong>①</strong>
		+ 
		<strong>②</strong> 
		<strong class="color_red total"> 0원</strong>
	</div>
	<div class="tab_layout">
		<div class="mgt30">
			<select id="searchOrder" class="select form" style="width:105px;">
				<option value="driving">최근 운행순</option>
				<option value="regDate">최근 등록순</option>
			</select>
			<table class="table1 mgt10" id="contentsTable">
				<colgroup>
					<col style="width:145px;" />
					<col style="width:120px;" />
					<col />
					<col />
					<col style="width:90px;" />
					<col style="width:50px;" />
				</colgroup>
				<thead>
					<tr>
						<th>
							<select class='th_select' id="searchState">
								<option value="">상태</option>
								<option value="1">미결제</option>
								<option value="2">승인</option>
								<option value="3">반려</option>
							</select>
						</th>
						<th>사용자</th>
						<th>배차 시간</th>
						<th>배차 정보</th>
						<th>경비</th>
						<th>상세</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="black"></div>
<div class="pop4 accept" style="display:none;">
	<div class="pop_layout">
		<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt="" /></a>
		<h1>아래 내역을 승인하시겠습니까?</h1>
		<div class="dl_wrap pat42">
			<ul class="dl_text">
				<li>
					<dl>
						<dt>부서</dt>
						<dd class="detailGroup"></dd>
					</dl>
					<dl>
						<dt>이름</dt>
						<dd class="detailNm"></dd>
					</dl>
				</li>
				<li>
					<dl class="dl_wid">
						<dt>배차 시작</dt>
						<dd class="detailStartDate"></dd>
						<dt>업무명</dt>
						<dd class="detailTitle" style="width:inherit;"></dd>
					</dl>
					<dl class="dl_wid">
						<dt>배차 종료</dt>
						<dd class="detailEndDate"></dd>
						<dt>사용차량</dt>
						<dd class="detailVehicle" style="width:inherit;"></dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>항목</dt>
						<dd class="detailEnpenseType"></dd>
					</dl>
					<dl>
						<dt>금액</dt>
						<dd class="detailPrice"></dd>
					</dl>
					<dl>
						<dt>메모</dt>
						<dd class="detailNote"></dd>
					</dl>
					<dl class="mgt35">
						<dt>작성일</dt>
						<dd class="detailDate"></dd>
					</dl>
				</li>
			</ul>
		</div>
		<div class="return_btn pat42">
			<a href="#none" class="btn_cancel">취소</a><a href="#none" class="btn_ok btn_accept">승인</a>
		</div>
	</div>
</div>
<div class="pop1 turnDown" style="display:none; padding-left:0px;">
	<div class="pop_layout">
		<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt="" /></a>
		<h1>아래 내역을 반려하시겠습니까?</h1>
		<h2 class="title3 pat42">
			<p>반려 사유를 작성해주세요.</p>
		</h2>
		<table class="br_table">
			<colgroup>
				<col style="width:100px;" />
				<col />
			</colgroup>
			<tbody>
				<th>반려 사유</th>
				<td><input class="text rejectNote" type="text" style="width:100%;" /></td>
			</tbody>
		</table>
		<div class="dl_wrap">
			<ul class="dl_text">
				<li>
					<dl>
						<dt>부서</dt>
						<dd class="detailGroup"></dd>
					</dl>
					<dl>
						<dt>이름</dt>
						<dd class="detailNm"></dd>
					</dl>
				</li>
				<li>
					<dl class="dl_wid">
						<dt>배차 시작</dt>
						<dd class="detailStartDate"></dd>
						<dt>업무명</dt>
						<dd class="detailTitle" style="width:inherit;"></dd>
					</dl>
					<dl class="dl_wid">
						<dt>배차 종료</dt>
						<dd class="detailEndDate"></dd>
						<dt>사용차량</dt>
						<dd class="detailVehicle" style="width:inherit;"></dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>항목</dt>
						<dd class="detailEnpenseType"></dd>
					</dl>
					<dl>
						<dt>금액</dt>
						<dd class="detailPrice"></dd>
					</dl>
					<dl>
						<dt>메모</dt>
						<dd class="detailNote"></dd>
					</dl>
					<dl class="mgt35">
						<dt>작성일</dt>
						<dd class="detailDate"></dd>
					</dl>
				</li>
			</ul>
		</div>
		<div class="return_btn pat42">
			<a href="#none" class="btn_cancel">취소</a><a href="#none" class="btn_ok btn_turnDown">반려</a>
		</div>
	</div>
</div>
<div class="pop4 acceptResult" style="display:none;">
	<div class="pop_layout">
		<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt="" /></a>
		<h1>아래 내역은 승인되었습니다.</h1>
		<div class="dl_wrap pat42">
			<ul class="dl_text">
				<li>
					<dl>
						<dt>부서</dt>
						<dd class="detailGroup"></dd>
					</dl>
					<dl>
						<dt>이름</dt>
						<dd class="detailNm"></dd>
					</dl>
				</li>
				<li>
					<dl class="dl_wid">
						<dt>배차 시작</dt>
						<dd class="detailStartDate"></dd>
						<dt>업무명</dt>
						<dd class="detailTitle" style="width:inherit;"></dd>
					</dl>
					<dl class="dl_wid">
						<dt>배차 종료</dt>
						<dd class="detailEndDate"></dd>
						<dt>사용차량</dt>
						<dd class="detailVehicle" style="width:inherit;"></dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>항목</dt>
						<dd class="detailEnpenseType"></dd>
					</dl>
					<dl>
						<dt>금액</dt>
						<dd class="detailPrice"></dd>
					</dl>
					<dl>
						<dt>메모</dt>
						<dd class="detailNote"></dd>
					</dl>
					<dl class="mgt35">
						<dt>작성일</dt>
						<dd class="detailDate"></dd>
					</dl>
				</li>
			</ul>
		</div>
		<div class="return_btn pat42">
			<a href="#none" class="btn_ok">확인</a>
		</div>
	</div>
</div>
<div class="pop4 turnDownResult" style="display:none;">
	<div class="pop_layout">
		<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt="" /></a>
		<h1>아래 내역은 반려되었습니다.</h1>
		<table class="br_table pat42">
			<colgroup>
				<col style="width:100px;" />
				<col />
			</colgroup>
			<tbody>
				<th>반려 사유</th>
				<td><input class="text rejectReson" type="text" style="width:100%;" readonly value=""/></td>
			</tbody>
		</table>
		<div class="dl_wrap">
			<ul class="dl_text">
				<li>
					<dl>
						<dt>부서</dt>
						<dd class="detailGroup"></dd>
					</dl>
					<dl>
						<dt>이름</dt>
						<dd class="detailNm"></dd>
					</dl>
				</li>
				<li>
					<dl class="dl_wid">
						<dt>배차 시작</dt>
						<dd class="detailStartDate"></dd>
						<dt>업무명</dt>
						<dd class="detailTitle" style="width:inherit;"></dd>
					</dl>
					<dl class="dl_wid">
						<dt>배차 종료</dt>
						<dd class="detailEndDate"></dd>
						<dt>사용차량</dt>
						<dd class="detailVehicle" style="width:inherit;"></dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>항목</dt>
						<dd class="detailEnpenseType"></dd>
					</dl>
					<dl>
						<dt>금액</dt>
						<dd class="detailPrice"></dd>
					</dl>
					<dl>
						<dt>메모</dt>
						<dd class="detailNote"></dd>
					</dl>
					<dl class="mgt35">
						<dt>작성일</dt>
						<dd class="detailDate"></dd>
					</dl>
				</li>
			</ul>
		</div>
		<div class="return_btn">
			<a href="#none" class="btn_ok">확인</a>
		</div>
	</div>
</div>