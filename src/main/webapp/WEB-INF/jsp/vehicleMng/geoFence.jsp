<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<style>.svg-clipped{-webkit-clip-path:url(#svgPath);clip-path:url(#svgPath);}</style>
<svg height="0" width="0">
	<defs>
		<clipPath id="svgPath" clipPathUnits="objectBoundingBox">
  			<circle cx=".5" cy=".5" r=".5" />
   		</clipPath>
	</defs>
</svg>
<script type="text/javascript">
var lazy;
$(document).ready(function(){
	initMenuSel("M2003");

	$VDAS.http_post("/vehicleMng/getGeofenceCnt.do",{},{
		success: function(r){
			var cnt = r.result;
			$("#totCnt1").text(cnt+"개");
		}
	});
	
	$VDAS.http_post("/vehicle/getVehicleCnt.do",{},{
		success : function(r){
			var data = r.result;
			$("#totCnt2").text(data.used+"대");
		}
	});
	
	$VDAS.http_post("/account/getAccountCnt.do",{},{
		success : function(r){
			var data = r.result;
			
			$("#totCnt3").text(data.total+"명");
		}
	});
		
		
		
	$(".btn_search").on("click",function(){
		$.lazyLoader.search(lazy);
	});
	
	if(!lazy)
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
				searchMonth : $("#searchMonth")
			}
			,setSearchFunc : function(){
				
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 10			
			,loadUrl : "/vehicleMng/getGeofenceUserList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.result;				
				var strHtml = '';
				
				console.log(data);
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += '<tr>';
					strHtml += '<th class="">';
					if(vo.imgId&&vo.imgId.length > 0)
						strHtml += '<strong class="car_img" style="padding-left:15px;"><img class="svg-clipped" style="width:49px;height:49px;" src="${defaultPath}/com/getUserImg.do?uuid='+vo.imgId+'" alt="" /></strong>';
					else strHtml += '<strong class="car_img" style="padding-left:15px;"><img class="svg-clipped" src="${pathCommon}/images/user_img7.jpg" alt="" style="width:49px;height:49px;"/></strong>';
					strHtml += '<a href="#none" class="btn_userDetail" data-key="'+vo.accountKey+'">';
					strHtml += '<span>'+vo.accountNm+'</span>';
					strHtml += vo.name;
					strHtml += '</a>';
					strHtml += '</th>';
					strHtml += '<td>'+((vo.corpGroupNm)?vo.corpGroupNm:"")+'/'+((vo.corpPosition)?vo.corpPosition:"")+'</td>';					
					strHtml += '<th class="">';					
					strHtml += '<strong class="car_img" style="padding-left:15px;"><img src="${path}/common/images/vehicle/'+vo.imgS+'.png" alt="" /></strong>';
					strHtml += '<a href="#none" class="btn_vehicleDetail" data-key="'+vo.vehicleKey+'">';
					strHtml += '<span>'+vo.modelMaster+'</span>';
					strHtml += vo.plateNum;
					strHtml += '</a>';
					strHtml += '</th>';
					strHtml += '<td><a href="#none" class="btnDetail" data-key='+vo.accountKey+'><img src="${path}/common/images/btn3.jpg" alt=""></a></td>';
					strHtml += '</tr>';
					
				}
				
				return strHtml;

			}
		});
	
	
	$("body").on("click",".btnDetail",function(){
		var key = $(this).data("key");
		$VDAS.instance_post("/vehicleMng/geoFenceDetail.do",{accountKey:key});
	});
	
	$("body").on("click",".btn_vehicleDetail",function(){
		var vehicleKey = $(this).data("key");
		$VDAS.instance_post("/drivingMng/searchVehicleDetail.do",{vehicleKey:vehicleKey});
	});

	$("body").on("click",".btn_userDetail",function(){
		var accountKey = $(this).data("key");
		$VDAS.instance_post("/drivingMng/searchUserDetail.do",{accountKey:accountKey});
	});
});

</script>
<div class="right_layout">
	<h2 class="tit_sub">
		운행 스케쥴
		<p>등록한 거래처 1km이내 접근 시, 직원의 운행스케쥴에 기록됩니다.</p>
	</h2>
	<div class="sub_top1">
		<span>등록 거래처 수 <b id="totCnt1">0개</b> </span>
		<span>등록 차량 <b id="totCnt2">0개</b> </span>
		<span>등록 사용자 <b id="totCnt3">0개</b> </span>
		<strong id="nowDate"></strong>
	</div>
		<form id="searchFrm" method="post" onSubmit="return false;">
		<div class="search_box1">		
			<input id="searchMonth" type="text" class="date datePicker" name="searchMonth" style="height:36px; border:1px solid #c0c0c0;"/>
			<div class="search_box1_2" style="left:0px;width: 75px;">			
				<a href="#none" class="btn_search" style="width:75px;height:36px;">조회</a>
			</div>
		</div>
		</form>
	<div class="tab_layout">
		<table class="table1" id="contentsTable" >
			<colgroup>
				<col style="width:250px;" />
				<col style="width:150px;" />
				<col style="width:*;" />
				<col style="width:70px;" />
			</colgroup>
			<thead>
				<tr>
					<th>사용자 정보</th>
					<th>부서/직급</th>
					<th>최근 이용차량</th>
					<th>운행스케쥴</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>