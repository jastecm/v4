<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script type="text/javascript">
var lazy;
var tWidth;

$(document).ready(function(){
	tWidth = Math.floor(($("#contentsTable").width() - 216) / 3) - 1;
	initMenuSel("M2001");
	
	$("body").on("click",".btn_send",function(){
		var allocateKey = $("#allocateKey").val();
		
		var lon = $("#map_lon").val();
		var lat = $("#map_lat").val();
		var addr = $("#map_addr").val();
		var addrDetail = $("#map_addrDetail").val();
		
		$VDAS.http_post("/vehicleMng/sendReport.do",{allocateKey:allocateKey,lon:lon,lat:lat,addr:addr,addrDetail:addrDetail},{
			success : function(r){
				alert("저장되었습니다.");
				$VDAS.instance_post("/vehicleMng/vehicleReport.do",{});
			}
		});
	});
	
	$("body").on("click",".btn_back",function(){
		$VDAS.instance_post("/vehicleMng/vehicleReport.do",{});
	});

	$("#drivingMap").attr("src","${path}/map/trip.do?searchType=default");
	$("body").on("click",".btn_gps",function(){
		SuddenSummaryInit();
		$("#popStart").text($(this).parent("div").find(".startInfo").val());
		$("#popEnd").text($(this).parent("div").find(".endInfo").val());
		$("#popVehicle").text($(this).parent("div").find(".vehicleInfo").val());

		var t = 3600*(24*4);
		$("#drivingMap").get(0).contentWindow.searchType = "trip";
		$("#drivingMap").get(0).contentWindow.tripKey = $(this).data("gps");
		//$("#accountMap").get(0).contentWindow.accountKey = $(this).data("account");
		//$("#accountMap").get(0).contentWindow.searchTime = t;
		$("#drivingMap").get(0).contentWindow._o = $("#drivingMap").get(0).contentWindow.createOption();
		$("#drivingMap").get(0).contentWindow.outerReset();

		$(".drivingMap").fadeIn();
		$(".black").fadeIn();
	});

	
	$("body").on("click",".btn_return2",function(){
		if(lazyFirst.endLat && lazyFirst.endLon){
			$("#drivingMap").attr("src",_defaultPath+"/map/mapPoint.do?lat="+lazyFirst.endLat+"&lon="+lazyFirst.endLon+"&searchType=one");
			
			$(".drivingMap").fadeIn();
			$(".black").fadeIn();	
		}else{
			alert("주행기록이 없습니다.");
		}
		
		
	});

	$( '#checker' ).click(function(){
		$( '.checkboxes' ).attr( 'checked', $( this ).is( ':checked' ) );
		$( '.checkboxes' ).parent().attr( 'checked', $( this ).is( ':checked' ) );
	});
	initUserInfo();
	
});

function SuddenSummaryInit(){
	$(".rapidStart").text("0회");
	$(".rapidStop").text("0회");
	$(".rapidAccel").text("0회");
	$(".rapidDeaccel").text("0회");
	$(".rapidTurn").text("0회");
	$(".rapidUtern").text("0회");
}

function outerSuddenSummaryCall(a){
	$(".rapidStart").text(a[0]+"회");
	$(".rapidStop").text(a[1]+"회");
	$(".rapidAccel").text(a[2]+"회");
	$(".rapidDeaccel").text(a[3]+"회");
	$(".rapidTurn").text(a[4]+"회");
	$(".rapidUtern").text(a[5]+"회");
	$(".overSpeed").text(a[6]+"회");
	$(".overSpeedLong").text(a[7]+"회");
}



function addrString(a){
	var result = '';

	for(var i=0;i < a.length;i++){
		if(i >= 18){
			result += '...';
			break;
		}
		else
			result += a[i];
	}
		
	return result;
}



function userImg(a){
	if(a==null) return '${pathCommon}/images/user_none.png'
	else return '${pathCommon}/images/'+a+'.png'
}

var initUserInfo = function(){
	var allocateKey = $("#allocateKey").val();

	$VDAS.http_post("/allocate/getAllocateUser.do",{allocateKey:allocateKey},{
		success : function(r){
			var obj = r.result;

			console.log(obj);
			if(obj != null && obj != ''){
				if(obj.imgId&&obj.imgId.length > 0)
					$(".user_detail_left").append('<img class="svg-clipped" src="${defaultPath}/com/getUserImg.do?uuid='+obj.imgId+'" alt="" />');
				else $(".user_detail_left").append('<img class="svg-clipped" src="${pathCommon}/images/user_img7.jpg" alt=""/>');
				$("#corpGroup").after(""+convertNullString(obj.corpNm));
				$("#position").after(""+convertNullString(obj.corpPosition)+"/"+convertNullString(obj.name));
				$("#phone").after(""+convertNullString(obj.phone));
				$("#mobilePhone").after(""+convertNullString(obj.mobilePhone));
				$("#email").after(""+convertNullString(obj.authMail));
				$(".car_info2_1").append('<img src="${pathCommon}/images/vehicle/'+obj.imgS+'.png" alt="" />');
				$("#taskNm").after("&nbsp;&nbsp;"+convertNullString(obj.title));
				$("#taskPlan").after(""+convertNullString(obj.contents));
				$("#taskStartDate").text(convertNullString(obj.startDate));
				$("#taskEndDate").text(convertNullString(obj.endDate));
				$("#searchFrm input[name=startDate]").val(replaceAll(obj.startDate.substring(0,10),"/",""));
				$("#searchFrm input[name=endDate]").val(replaceAll(obj.endDate.substring(0,10),"/",""));
				$("#deviceNm").after(""+convertNullString(obj.deviceSeries)+"/"+convertNullString(obj.deviceSn));
				$("#vehicleInfo").after(""+convertNullString(obj.modelMaster)+"/"+convertNullString(obj.plateNum));
				$("#plateNum").after(""+convertNullString(obj.plateNum));
				$("#volume").after(""+convertNullString(obj.volume)+" cc");
				$("#transmission").after(""+convertNullString(obj.transmission));
				$("#distance").after(""+number_format((obj.totDistance/1000).toFixed(1))+" km");
				$("#modelMaster").after(""+convertNullString(obj.manufacture)+"/"+convertNullString(obj.modelMaster));
				$("#modelHeader").after(""+convertNullString(obj.modelHeader)+"/"+convertNullString(obj.year));
				$("#fuel").after(""+convertNullString(obj.fuelType));
				$("#color").after(""+convertNullString(obj.color));
				
				lazyFirst = {};
				if(obj.returnLocationAddr)lazyFirst.endAddr = obj.returnLocationAddr;
				if(obj.returnLocationAddrDetail)lazyFirst.endAddrDetail = obj.returnLocationAddrDetail;
				if(obj.returnLocation)lazyFirst.endLon = obj.returnLocation.split(",")[0];
				if(obj.returnLocation)lazyFirst.endLat = obj.returnLocation.split(",")[1];
				$("#map_addr").val(lazyFirst.endAddr);
				$("#map_addrDetail").val(lazyFirst.endAddrDetail);
				$("#text_addr").html(lazyFirst.endAddr+" "+lazyFirst.endAddrDetail);
				$("#map_lon").val(lazyFirst.endLon);
				$("#map_lat").val(lazyFirst.endLat);
				
				lazy();
			}
		}
	});
}

var lazyFirst;
function lazy(){
	$("#contentsTable").lazyLoader({
		searchFrmObj : $("#searchFrm") // form을 3개로 분할
		,searchFrmVal : {
			searchAllocatekey : $("#allocateKey")
		}
		,rowHeight : 80
		,limit : 5
		,loadUrl : "/driving/getDrivingList.do"
		,initSearch : true
		,createDataHtml : function(r){
			console.log(r);
			
			var strHtml = '';
			var data = r.rtvList;
			for(var i = 0 ; i < data.length ; i++){
				var vo = data[i];
				
				if(!lazyFirst.endAddr){
					lazyFirst.endAddr = vo.endAddr;
					lazyFirst.endLat = vo.endLat;
					lazyFirst.endLon = vo.endLon;
					lazyFirst.endAddrDetail = "";
					
					$("#map_addr").val(vo.endAddr);
					$("#text_addr").html(vo.endAddr);
					$("#map_lon").val(vo.endLon);
					$("#map_lat").val(vo.endLat);
				}
				
				strHtml += '<tr>';
				strHtml += '<td width="80px">';
				strHtml += '<div class="table1_box1">';
				strHtml += '<img src="${pathCommon}/images/vehicle/'+vo.imgS+'.png" alt="" />';
				strHtml += '</div>';
				strHtml += '<div class="table1_box2">';
				strHtml += '<img src='+userImg(vo.driverImg)+' alt="" />';
				strHtml += '</div>';
				strHtml += '</td>';
				strHtml += '<td width="120px">';
				strHtml += '<div class="table1_box1 txt_left">';
				strHtml += '<a href="#none" class="a_vehicleDetail" data-key="'+vo.vehicleKey+'">';
				strHtml += '<span>'+vo.modelMaster+'</span>';
				strHtml += ' '+vo.plateNum;
				strHtml += '</a>';
				strHtml += '</div>';
				strHtml += '<div class="table1_box2 txt_left">';
				strHtml += '<a href="#none" class="a_user">';
				strHtml += '<span>'+convertNullString(vo.driverGroupNm)+'/</span>';
				strHtml += ' '+convertNullString(vo.driverNm);
				strHtml += '</a>';
				strHtml += '<br>';
				strHtml += '<br>';
				if(vo.instantAllocate == '1')
					strHtml += '<a href="#none" class="btn btn_edit">변경</a>';
				strHtml += '</div>';
				strHtml += '</td>';
				strHtml += '<td colspan="3">';
				strHtml += '<div class="table1_box1 txt_left">';
				strHtml += '<input type="hidden" class="startInfo" value="'+convertNullString(vo.startDate)+' '+convertNullString(vo.startAddr)+'">';
				strHtml += '<input type="hidden" class="endInfo" value="'+convertNullString(vo.endDate)+' '+convertNullString(vo.endAddr)+'">';
				strHtml += '<input type="hidden" class="endLat" value="'+convertNullString(vo.endLat)+'">';
				strHtml += '<input type="hidden" class="endLon" value="'+convertNullString(vo.endLon)+'">';
				strHtml += '<input type="hidden" class="vehicleInfo" value="'+convertNullString(vo.modelMaster)+'/'+convertNullString(vo.plateNum)+'">';
				if(convertNullString(vo.startAddr) == ''){
					if(vo.tripDetailState == 1){
						if("${VDAS.lang}" == "ko") vo.startAddr = '주행중';
						else vo.startAddr = 'now Driving';
					}else if(vo.tripDetailState == 2){
						if("${VDAS.lang}" == "ko") vo.startAddr = '주행정보를 생성중입니다.';
						else vo.startAddr = 'creating driving info';
					}else if(vo.tripDetailState == 3){
						if("${VDAS.lang}" == "ko") vo.startAddr = '주행정보를 생성중입니다.';
						else vo.startAddr = 'creating driving info';
					}
				}
				if(convertNullString(vo.endAddr) == ''){
					if(vo.tripDetailState == 1){
						if("${VDAS.lang}" == "ko") vo.endAddr = '주행중';
						else vo.endAddr = 'now Driving';
					}else if(vo.tripDetailState == 2){
						if("${VDAS.lang}" == "ko") vo.endAddr = '주행정보를 생성중입니다.';
						else vo.endAddr = 'creating driving info';
					}else if(vo.tripDetailState == 3){
						if("${VDAS.lang}" == "ko") vo.endAddr = '주행정보를 생성중입니다.';
						else vo.endAddr = 'creating driving info';
					}
				}
				strHtml += '<p><b>출발 </b><span title="'+convertNullString(vo.startAddr)+'" style="font-weight:500;">'+convertNullString(vo.startDate)+' '+addrString(convertNullString(vo.startAddr))+'</span></p>';
				strHtml += '<p><b>도착 </b><span title="'+convertNullString(vo.endAddr)+'" style="font-weight:500;">'+convertNullString(vo.endDate)+' '+addrString(convertNullString(vo.endAddr))+'</span></p>';
				strHtml += '<a href="#none" class="btn_gps type02" data-gps="'+vo.tripKey+'" data-vehicle="'+vo.vehicleKey+'" data-account="'+vo.accountKey+'"><img src="${pathCommon}/images/gps.jpg" alt=""></a>';
				if((vo.workingAreaWarning == null || vo.workingAreaWarning == '')
						&& (vo.workingTimeWarning == null || vo.workingTimeWarning == ''))
					strHtml += '<div class="p_time"><p>-&nbsp;&nbsp;&nbsp;&nbsp;</p></div>';
				else if((vo.workingAreaWarning != null && vo.workingAreaWarning != '')
						&& (vo.workingTimeWarning == null || vo.workingTimeWarning == ''))
					strHtml += '<div class="p_time"><p class="t3">이수지역</p></div>';
				else if((vo.workingAreaWarning == null || vo.workingAreaWarning == '')
						&& (vo.workingTimeWarning != null && vo.workingTimeWarning != ''))
					strHtml += '<div class="p_time"><p class="t3">이수시간</p></div>';
				else if((vo.workingAreaWarning != null && vo.workingAreaWarning != '')
						&& (vo.workingTimeWarning != null && vo.workingTimeWarning != '')){
					strHtml += '<div class="p_time"><p class="t3">이수지역<br>이수시간</p></div>';
				}
				strHtml += '</div>';
				strHtml += '<div class="table1_box2 txt_left">';
				strHtml += '<div>';
				strHtml += '<span style="width:'+tWidth+'px;"><b>안전점수 </b>'+calScore(parseFloat(vo.safeScore).toFixed(1))+'</span>';
				strHtml += '<span style="width:'+tWidth+'px;"><b>연비점수 </b>'+calScore((vo.avgFco/vo.fuelEfficiency*100).toFixed(1))+'</span>';
				strHtml += '<span style="width:'+tWidth+'px;"><b>에코점수 </b>'+calScore(parseFloat(vo.ecoScore).toFixed(1))+'</span>';
				strHtml += '</div>';
				strHtml += '</div>';
				strHtml += '<div class="table1_box2 txt_left">';
				strHtml += '<div>';
				strHtml += '<span style="width:'+tWidth+'px;"><b>주행시간 </b>'+secondToTime(vo.drivingTime,"${_LANG}")+'</span>';
				strHtml += '<span style="width:'+tWidth+'px;"><b>평균속도 </b>'+vo.meanSpeed+' km/h</span>';
				strHtml += '<span style="width:'+tWidth+'px;"><b>냉각수 </b>'+vo.coolantTemp+' ℃</span>';
				strHtml += '</div>';
				strHtml += '<div>';
				strHtml += '<span style="width:'+tWidth+'px;"><b>주행거리 </b>'+minusError(number_format((vo.distance/1000).toFixed(1)))+' km</span>';
				strHtml += '<span style="width:'+tWidth+'px;"><b>최고속도 </b>'+vo.highSpeed+' km/h</span>';
				strHtml += '<span style="width:'+tWidth+'px;"><b>배터리 </b>'+number_format(parseFloat(vo.ecuVolt).toFixed(1))+' v</span>';
				strHtml += '</div>';
				strHtml += '<div>';
				strHtml += '<span style="width:'+tWidth+'px;"><b>연비 </b>'+number_format(parseFloat(vo.avgFco).toFixed(1))+' km/ℓ</span>';
				strHtml += '<span style="width:'+tWidth+'px;"><b>연료 </b>'+number_format((vo.fco/1000).toFixed(1))+' ℓ</span>';
				strHtml += '<span style="width:'+tWidth+'px;"><b>발전기 </b>'+number_format(parseFloat(vo.deviceVolt).toFixed(1))+' v</span>';
				strHtml += '</div>';				
				strHtml += '<div>';
				strHtml += '<span style="width:'+tWidth+'px;"><b>co2배출량 </b>'+number_format(vo.co2Emission)+' ㎎</span>';
				strHtml += '<span style="width:'+tWidth+'px;"><b>퓨얼컷 </b>'+number_format(vo.fuelCutTime)+' 초</span>';
				strHtml += '<span style="width:'+tWidth+'px;"><b>공회전 </b>'+number_format(vo.idleTime)+' 초</span>';
				strHtml += '</div>';
				strHtml += '<div>';
				strHtml += '<span style="width:'+tWidth+'px;"><b>웜업시간 </b>'+number_format(vo.warmupTime)+' 초</span>';
				/* strHtml += '<span style="width:'+tWidth+'px;"><b style="font-size:smaller;">엔진오일온도 </b>'+parseFloat(vo.engineOilTemp).toFixed(1)+' ℃</span>'; */
				strHtml += '</div>';
				strHtml += '</div>';
				strHtml += '</td>';
				strHtml += '</tr>';
			}
			return strHtml;

		}
	});
	
	$(".btn_ok").on("click",function(){
		$(".drivingMap").fadeOut();
		$(".black").fadeOut();
	});
}

function setLonLat(lon,lat){
	$("#map_lon").val(lon);
	$("#map_lat").val(lat);
}

function setAddr(addr){
	$("#map_addr").val(addr);
	$("#text_addr").html(addr);	
}

</script>
<input type="hidden" id="allocateKey" value="${allocateKey }">
<form id="searchFrm" style="display:none">
	<input type="hidden" name="searchAllocatekey" value="" />
	<input type="hidden" name="searchOrder" value="driving" />
</form>
<div class="right_layout">
	<div class="detail_view">
		<a href="javascript:window.history.back();" class="btn_close"><img src="${pathCommon}/images/close.jpg" alt="" /></a>
		<h2 class="h2tit">
			차량사용 보고서 제출
		</h2>
		<div class="step_box" style="text-align:left; padding-left:20px;">
			<span>사용차량의 운행내역ㆍ경비보고 또는 반납을 위한 보고서를 제출합니다.</span>
		</div>
		<div class="user_detail" style="padding-top:17px;">
			<div class="user_detail_left" style="padding-top:17px;">
				<!-- img size : 가로 345-->
			</div>
			<ul class="car_detail_right">
				<li>
					<div>
						<p><span class="c1">사용개시 </span><strong id="taskStartDate"></strong></p>
					</div>
					<div>
						<p><span class="c1">사용종료 </span><strong id="taskEndDate"></strong></p>
					</div>
				</li>
				<li>
					<div>
						<p><span id="corpGroup">소속/지점 </span></p>
						<p><span id="position">부서/이름 </span></p>
					</div>
					<div>
						<p><span id="phone">전화번호 </span></p>
						<p><span id="mobilePhone">휴대폰 </span></p>
						<p><span id="email">이메일 </span></p>
					</div>
				</li>
				<li>
					<p><span id="taskNm">업무명 </span></p>
					<p><span>활용계획 </span><strong id="taskPlan"></strong></p>
				</li>
				<li class="car_info">
					<h1><span></span>사용 차량정보</h1>
					<div class="car_info2">
						<div class="car_info2_1"></div>
						<div class="car_info2_2">
							<p><span id="vehicleInfo">차량선택 </span></p>
							<p><span id="deviceNm">ViewCar 모델명/일련번호 </span></p>
							<%-- <a href="#" class="car_detail1"><span>선택차량 상세정보</span></a>
							<a href="#" class="car_detail2"><img src="${pathCommon}/images/car_detail.png" alt="" /></a> --%>
						</div>
					</div>
				</li>
				<li>
					<div>
						<p><span id="plateNum">차량번호 </span></p>
						<p><span id="volume">배기량 </span></p>
						<p><span id="transmission">기어방식 </span></p>
						<p><span id="distance">총주행거리 </span></p>
					</div>
					<div>
						<p><span id="modelMaster">제조사/차종 </span></p>
						<p><span id="modelHeader">모델명/년식 </span></p>
						<p><span id="fuel">유종 </span></p>
						<p><span id="color">색상 </span></p>
					</div>
				</li>
			</ul>
		</div>
		<div class="line1"></div>
		<h2 class="title3 pat42"><img src="${pathCommon}/images/${_LANG}/h2_5.png" alt="운행기록 현황" /></h2>
		<p class="txt2">해당 배차시간동안 주행한 내역입니다.</p>
		</br>
		<table class="table1 table1_2" id="contentsTable">
			<colgroup>
				<col style="width:80px;"/>
				<col style="width:120px;"/>
				<col />
				<col style="width:100px;"/>
				<col style="width:80px;"/>
			</colgroup>
			<thead>
				<tr style="display:none;">
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
				</tr>
				<tr>
					<th colspan="2">차량 및 사용자</th>
					<th>운행정보</th>
					<th>위치</th>
					<th>경고</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		<div class="txt1">
			<h1>출발/도착지 미기록</h1>
			<p>GPS 미동작, 지하주차장 이동 등 GPS 수신상태가 고르지 못할 경우 출발지 및 도착지가 기록되지 않을 수 있습니다.</p>
		</div>
		<h2 class="title3 pat42"><img src="${pathCommon}/images/${_LANG}/h2_6.png" alt="차량반납 주차 위치"></h2>
		<div class="record_box2">
			<span id="text_addr"></span>
			<a href="#none" class="btn_return2">차량반납 주차위치 확인</a>
		</div>
		<div class="return_btn">
			<a href="#" class="btn_cancel btn_back">취소</a><a href="#none" class="btn_send">보고서 제출</a>
		</div>
	</div>
</div>
<div class="black"></div>
<div class="pop2 drivingMap" style="height:600px;">
	<div class="pop_layout">
		<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt="" /></a>
		<h1>주차위치</h1>
		<div class="map_area">
			<div id="map_canvas">
				<iframe id="drivingMap" src="#none" width="760px" height="339px"></iframe>
			</div>
		</div>
		<div class="map_txt1" style="padding: 14px 0 14px 20px;">
			<div>
				주&nbsp;&nbsp;&nbsp;&nbsp;소 : <input type="text" id="map_addr" disabled style="width: 400px;padding-left:10px;height: 25px;border: 1px solid #c0c0c0;font-size: 12px;"/>				
			</div>
			<div>
				상세주소 : <input id="map_addrDetail" type="text" style="width: 400px;padding-left:10px;height: 25px;border: 1px solid #c0c0c0;font-size: 12px;"/>
			</div>
			<input type="hidden" id="map_lon" />
			<input type="hidden" id="map_lat" />
		</div>
		<br><br>
		<div class="btn_type1"><a href="#none" class="btn_ok">확인</a></div>
	</div>
</div>