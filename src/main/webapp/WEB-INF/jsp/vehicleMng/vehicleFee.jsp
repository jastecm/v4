<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script type="text/javascript">
var lazy;
var lazyManage;
$(document).ready(function(){
	
	initMenuSel("M2002");
    
	$(".btn_search").on("click",function(){
		$("#searchFrm input[name=searchDate]").val($("#searchDate").val()?replaceAll($("#searchDate").val(),"-","")+"01":"");
		$.lazyLoader.search(lazy);
		feeSummary();
	});
	
	$("#searchOrder, #searchState").on("change",function(){
		$.lazyLoader.search(lazy);
		feeSummary();
	});

	if(!lazy){
		
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
				searchOrder : $("#searchOrder")
				,searchState : $("#searchState")
			}
			,scrollRow : 4
			,rowHeight : 95
			,limit : 5
			,loadUrl : "/allocate/getFeeList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.result;
				var strHtml = "";
				console.log(data);
				var strHtml = "";
				for(var i = 0 ; i < data.length; i++){
					var vo = data[i];
					strHtml += '<tr>';
					strHtml += '<td>';
					if(vo.state == '1')
						strHtml += '<span class="ico5">미승인</span><br />';
					if(vo.state == '2')
						strHtml += '<span class="ico1">승인</span><br /><span class="t_date">'+vo.stateDate+'</span>';
					if(vo.state == '3')
						strHtml += '<span class="ico6">반려</span><br /><span class="t_date">'+vo.stateDate+'</span>';
					strHtml += '</td>';
					strHtml += '<td class="align_left">';
					strHtml += '<b>시작</b> '+vo.startDate+'<br />';
					strHtml += '<b>종료</b> '+vo.endDate+'<br />';
					strHtml += '<b>사용일</b> '+vo.allocateDate;
					strHtml += '</td>';
					strHtml += '<td class="align_left">';
					strHtml += '<b>업무명</b> '+convertNullString(vo.title)+'<br />';
					strHtml += '<b>사용차량</b> '+convertNullString(vo.modelMaster)+'/'+vo.plateNum;
					strHtml += '</td>';
					strHtml += '<td class="align_left">';
					$VDAS.http_post("/com/getCode.do",{code:"C018", val:vo.expenseType},{
						sync : true
						,success : function(r){
							var code = r.rtvList[0];
							strHtml += '<b>항목</b> <em class="color_red">'+code.codeName+'</em><br />';
						}
					});
					strHtml += '<b>금액</b> <em class="color_red">'+number_format(vo.price)+'원</em>';
					strHtml += '</td>';
					if(vo.state == '2')
						strHtml += '<td><a href="#none" class="a_check" data-key="'+vo.expenseKey+'"><img src="${pathCommon}/images/btn3.jpg" alt=""></a></td>';
					else
						strHtml += '<td><a href="#none" class="a_edit" data-key="'+vo.expenseKey+'"><img src="${pathCommon}/images/btn4.jpg" alt=""></a></td>';
					strHtml += '</tr>';
				}
				
				return strHtml;
				
			}
		});
		
		feeSummary();
	}
	
	$("body").on("click",".a_check",function(){
		var key = $(this).data("key");
		$VDAS.instance_post("/vehicleMng/vehicleFeeDetail.do",{expenseKey:key,code:"C018"});
	});

	$("body").on("click",".a_reg",function(){
		$VDAS.instance_post("/vehicleMng/vehicleFeeRegister.do",{});
	});
	
	$("body").on("click",".a_edit",function(){
		var key = $(this).data("key");
		$VDAS.instance_post("/vehicleMng/vehicleFeeEdit.do",{expenseKey:key,code:"C018"});
	});
	
});


function feeSummary(){
			
	$VDAS.http_post("/allocate/getExpenseList.do",
		{	
			searchDate : $("#searchFrm input[name=searchDate]").val()
			,searchState: $("#searchFrm input[name=searchState]").val()
		},
		{success : function(r){
			var data = r.result;
			var totalMain = 0;
			var totalEtc = 0;
			$("#refuel").text(" 0원");
			$("#clinic").text(" 0원");
			$("#parking").text(" 0원");
			$("#toll").text(" 0원");
			$("#rental").text(" 0원");
			$("#wash").text(" 0원");
			$("#penalty").text(" 0원");
			$("#etc").text(" 0원");
			for(var i = 0;i < data.length;i++){
				var vo = data[i];
				console.log(vo);
				switch(vo.expenseType){
					case "1":		// 주유
						$("#refuel").text(" "+number_format(vo.price)+"원");
						totalMain += parseInt(vo.price);
						break;
					case "2":		// 정비
						$("#clinic").text(" "+number_format(vo.price)+"원");
						totalMain += parseInt(vo.price);
						break;
					case "3":		// 주차
						$("#parking").text(" "+number_format(vo.price)+"원");
						totalMain += parseInt(vo.price);
						break;
					case "4":		// 통행료
						$("#toll").text(" "+number_format(vo.price)+"원");
						totalMain += parseInt(vo.price);
						break;
					case "5":		// 렌탈료
						$("#rental").text(" "+number_format(vo.price)+"원");
						totalEtc += parseInt(vo.price);
						break;
					case "6":		// 세차
						$("#wash").text(" "+number_format(vo.price)+"원");
						totalEtc += parseInt(vo.price);
						break;
					case "7":		// 과태료
						$("#penalty").text(" "+number_format(vo.price)+"원");
						totalEtc += parseInt(vo.price);
						break;
					case "8":		// 기타
						$("#etc").text(" "+number_format(vo.price)+"원");
						totalEtc += parseInt(vo.price);
						break;
				}
			}
			$(".totalMain").text("= "+number_format(totalMain)+"원");
			$(".totalEtc").text("= "+number_format(totalEtc)+"원");
			$(".total").text("= "+number_format(totalMain+totalEtc)+"원");
		}
	});
	
}

</script>
<form id="searchFrm">
	<input type="hidden" name="searchDate" value="" />
	<input type="hidden" name="searchOrder" value="" />
	<input type="hidden" name="searchState" value="" />
</form>
<div class="right_layout">
	<h2 class="tit_sub">
		차량경비 조회
		<p>등록차량의 모든 경비내역을 관리합니다.</p>
	</h2>
	<div class="sub_top1">
		<strong id="nowDate"></strong>
	</div>
	<h3 class="h3tit">경비 등록</h3>
	<div class="box1">
		새로운 경비를 등록하시려면 우측 경비 등록하기 버튼을 눌려 신청서를 작성해주세요.
		<a href="#" class="a_reg">경비 등록</a>
	</div>
	<h3 class="h3tit">경비 신청 조회·수정</h3>
	<!-- 일반 -->
	<div class="search_box1">
		<input type="text" style="width:85px; height:36px; border:1px solid #c0c0c0;" height:36px;" id="searchDate" class="date datePickerMonth" name="vehicleRegDate" value="${reqVo.vehicleRegDate }"/>
		<!-- <div class="search_box1_2">
			<input type="text" id="searchText" class="search_input" />
		</div> -->
		<a href="#none" class="btn_search">조회</a>
	</div>
	<div class="txtbox">
		<p>
			<strong>①</strong> 
			<strong>주차</strong><b id="parking"> 0원</b>
			+ 
			<strong>통행료</strong><b id="toll"> 0원</b>
			+ 
			<strong>주유</strong><b id="refuel"> 0원</b>
			+ 
			<strong>정비</strong><b id="clinic"> 0원</b>
			
			<strong class="color_red totalMain"> 0원</strong>
		</p>
		<p>
			<strong>②</strong> 
			<strong>세차</strong> <b id="wash"> 0원</b>
			+ 
			<strong>렌탈료</strong> <b id="rental"> 0원</b>
			+ 
			<strong>과태료 </strong> <b id="penalty"> 0원</b>
			+ 
			<strong>기타</strong> <b id="etc"> 0원</b>
			
			<strong class="color_red totalEtc"> 0원</strong>
		</p>
	</div>
	<div class="txtbox2">
		<strong>①</strong>
		+ 
		<strong>②</strong> 
		<strong class="color_red total"> 0원</strong>
	</div>
	<div class="tab_layout">
		<div class="mgt30">
			<select id="searchOrder" class="select form" style="width:105px;">
				<option value="driving">최근 운행순</option>
				<option value="regDate">최근 등록순</option>
			</select>
			<table class="table1 mgt10" id="contentsTable">
				<colgroup>
					<col style="width:145px;" />
					<col />
					<col />
					<col style="width:140px;" />
					<col style="width:90px;" />
				</colgroup>
				<thead>
					<tr>
						<th>
							<select class='th_select' id="searchState">
								<option value="">상태</option>
								<option value="1">미결제</option>
								<option value="2">승인</option>
								<option value="3">반려</option>
							</select>
						</th>
						<th>배차 시간</th>
						<th>배차 정보</th>
						<th>경비</th>
						<th>수정</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>