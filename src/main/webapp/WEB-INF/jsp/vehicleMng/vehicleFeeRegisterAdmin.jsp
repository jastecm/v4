<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script type="text/javascript">
var lazy;
var lazyManage;

var lazyVehicle;
var lazyUser;
$(document).ready(function(){
	initMenuSel("M2002");
	
	$VDAS.ajaxCreateSelect("/com/getCode.do",{code:"C018"},$(".selectKind"),false,"선택","","${_LANG}",null,null);
	
	$("body").on("click",".a_accept",function(){
		var length = $(".gcbox").size();
		$("#registFrm input[name=allocateDate]").val("");
		$("#registFrm input[name=allocateKey]").val("");
		$("#registFrm input[name=selectKind]").val("");
		$("#registFrm input[name=inputCost]").val("");
		$("#registFrm input[name=inputNote]").val("");
		
		var objDate = $(".allocateDate");
		if(objDate.val().length == 0){
			alert("날짜를 선택해주세요.");
			objDate.focus();
			return false;
		}
		
		var objVehicle = $("#vehicle");
		if(objVehicle.val().length == 0){
			alert("차량을 선택해주세요.");
			$("#searchVehicle").trigger("click");
			return false;
		}
		
		var objAccount = $("#account");
		if(objAccount.val().length == 0){
			alert("사용자를 선택해주세요.");
			$("#searchUser").trigger("click");
			return false;
		}
		
		var objKind = $(".selectKind");
		if(objKind.val().length == 0){
			alert("경비 유형을 선택해주세요.");
			objKind.focus();
			return false;
		}
		
		$("#registFrm input[name=allocateDate]").val(replaceAll(objDate.val(),"/",""));
		$("#registFrm input[name=allocateKey]").val("|"+objVehicle.val()+"|"+objAccount.val());
		$("#registFrm input[name=selectKind]").val(objKind.val());
		
		var cost = 0;
		if($(".inputCost").val().length != 0) cost = $(".inputCost").val();
		
		$("#registFrm input[name=inputCost]").val(cost);
		$("#registFrm input[name=inputNote]").val($(".inputNote").val());
	
		//console.log($("#registFrm").serialize());
			
		$VDAS.http_post("/allocate/registFee.do",$("#registFrm").serialize(),{
			success:function(r){
				$VDAS.instance_post("/vehicleMng/vehicleFee.do",{});
			}
		});
			
			
	});
	
	$("body").on("click",".btn_cancle",function(){
		$VDAS.instance_post("/vehicleMng/vehicleFee.do",{});
	});
	
	/* $("body").on("click",".gc_close",function(){
		$(this).parentsUntil(".detail_view").remove();
		calCost();
	}); */
	
	$("body").on("change",".inputCost,.selectKind",function(){
		calCost();
	});
	
	
	
	
	
	
	$("body").on("click","#searchUser",function(){
		
		if(lazyUser)
			$.lazyLoader.search(lazyUser);
		
		$( "#dialogUser" ).dialog({
	        width:800,
	        dialogClass:'alert',
	        draggable: false,
	        modal:true,
	        height:"auto",
	        resizable:false,
	        closeOnEscape:true,
	        buttons:{
	        	'취소':function(){
         			$(this).dialog("close");
         		}
	            ,'확인':function(){
	            	var selObj = $('.selPopObj:checked',$(this));
	            	if(selObj.length == 0)
	            		alert("사용자가 선택되지 않았습니다.");
	            	else{
	            		var strKeys = "";
	            		var strName = "";
	            		selObj.each(function(){
	        				var key = $(this).data("accountkey");
	        				if(strKeys.length == 0 ) strKeys = key;
	        				else strKeys += ","+key;
	        				
	        				var name = $(this).data("name");
	        				if(strName.length == 0 ) strName = name;
	        				else strName += ","+name;
	        				
	        			});
	            		
	            		$("#searchUser").val(strName);
	            		$("#account").val(strKeys);
	        			
	            		$(this).dialog("close");
	            	}
	            	
	            }
	        }
	    });
	
	if(!lazyUser)
		lazyUser = $("#dialogUser table").lazyLoader({
			searchFrmObj : $("#popupSearchFormUser")
			,searchFrmVal : {searchOrder:$("#popupSearchFormUser input[name=searchOrder]")
							,searchUserNm:$("#popupSearchFormUser input[name=searchUserNm]")
			}
			,scrollRow : 4
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/com/getUserList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.rtvList;
				var strHtml = "";
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += "<tr>";
					strHtml += '<td><input type="radio" name="selPopObj" class="selPopObj" data-accountnm="'+vo.accountNm
					+'" data-accountkey="'+vo.accountKey
					+'" data-corpgroupnm="'+vo.corpGroupNm
					+'" data-name="'+vo.name
					+'"/></td>';
					strHtml += '<th>';
					if(vo.imgId&&vo.imgId.length > 0)
						strHtml += '<strong class="car_img"><img class="svg-clipped" style="width:49px;height:49px;" src="${defaultPath}/com/getUserImg.do?uuid='+vo.imgId+'" alt="" /></strong>';
					else strHtml += '<strong class="car_img"><img class="svg-clipped" src="${pathCommon}/images/user_img7.jpg" alt="" style="width:49px;height:49px;"/></strong>';
					strHtml += '<a href="#none">';
					strHtml += '<span>'+vo.accountNm+'</span>';
					strHtml += vo.name;
					strHtml += '</a>';
					strHtml += '</th>';
					strHtml += '<td>';
					strHtml += '<p>'+vo.corpGroupNm+'/'+vo.corpPosition+'</p>';
					strHtml += '</td>';
					strHtml += '<td>';
					strHtml += '<p>'+vo.phone+'</p>';
					strHtml += '<p>'+vo.mobilePhone+'</p>';
					strHtml += '</td>';
					strHtml += '<td>'+vo.descript+'</td>';
					strHtml += '</tr>';	
				}
				
				return strHtml;
			}
		});
	
	});
	
	
	
	$("body").on("click","#searchVehicle",function(){
		if(lazyVehicle)
			$.lazyLoader.search(lazyVehicle);
		
		$( "#dialogVehicle" ).dialog({
	        width:800,
	        dialogClass:'alert',
	        draggable: false,
	        modal:true,
	        height:"auto",
	        resizable:false,
	        closeOnEscape:true,
	        buttons:{
	        	'취소':function(){
         			$(this).dialog("close");
         		}
	            ,'확인':function(){
	            	var selObj = $('.selPopObj:checked',$(this));
	            	if(selObj.length == 0)
	            		alert("차량이 선택되지 않았습니다.");
	            	else{
	            		var strKeys = "";
	            		var strName = "";
	            		selObj.each(function(){
	        				var key = $(this).data("vehiclekey");	  
	        				if(strKeys.length == 0 ) strKeys = key;
	        				else strKeys += ","+key;
	        				
	        				var name = $(this).data("platenum");
	        				if(strName.length == 0 ) strName = name;
	        				else strName += ","+name;
	        				
	        			});
	            		
	            		$("#searchVehicle").val(strName);
	            		$("#vehicle").val(strKeys);
	        			
	            		$(this).dialog("close");
	            		
	        				
	            	}
	            	
	            }
	        }
	    });
		
		if(!lazyVehicle)
			lazyVehicle = $("#dialogVehicle table").lazyLoader({
				searchFrmObj : $("#popupSearchFormVehicle")
				,searchFrmVal : {searchOrder:$("#popupSearchFormVehicle input[name=searchOrder]")
								,searchType:$("#popupSearchFormVehicle input[name=searchType]")
								,searchText:$("#popupSearchFormVehicle input[name=searchText]")
				}
				,scrollRow : 4
				,rowHeight : 80
				,limit : 5
				,loadUrl : "/com/getVehicleList.do"
				,initSearch : true
				,createDataHtml : function(r){
					var data = r.rtvList;
					var strHtml = "";
					
					for(var i = 0 ; i < data.length ; i++){
						var vo = data[i];
						
						var used = "";
						if(vo.deviceId.length>0)
							used = '1';
						else
							used = '0';
						
						strHtml += "<tr>";
						strHtml += '<td><input type="radio" name="selVehicle" class="selPopObj" data-vehiclekey="'+vo.vehicleKey
						+'" data-modelmaster="'+vo.modelMaster
						+'" data-platenum="'+vo.plateNum
						+'" data-used="'+used
						+'"/></td>';
						strHtml += '<th>';
						strHtml += '<strong class="car_img"><img src="${path}/common/images/vehicle/'+vo.imgS+'.png" alt="" /></strong>';
						strHtml += '<a href="#none">';
						strHtml += '<span>'+vo.modelMaster+'</span>';
						strHtml += vo.plateNum;
						strHtml += '</a>';
						strHtml += '</th>';
						strHtml += '<td><div class="start_end txt_left">';
						strHtml += '<p>'+vo.year+'</p>';
						strHtml += '<p>'+number_format((vo.totDistance/1000).toFixed(1))+'</p>';
						strHtml += '</div></td>';
						strHtml += '<td><div class="start_end txt_left">';
						strHtml += '<p>'+vo.deviceSeries+'</p>';
						strHtml += '<p>'+vo.deviceSn+'</p>';
						strHtml += '</div></td>';
						if(vo.deviceId.length>0)
							strHtml += '<td>사용중</td>';
						else
							strHtml += '<td>사용중지</td>';
						strHtml += '</tr>';	
					}
					
					return strHtml;
				}
			});
	});	
	
	$("#searchVehicle").on("focus",function(){
		$("searchVehicle").trigger("click");
	});
	$("#searchUser").on("focus",function(){
		$("searchUser").trigger("click");
	});
	
	$(".btn_searchUserPopText").on("keyup",function(e){
		if(e.keyCode == 13) {
			$(this).closest("form").find(".btn_searchUserPop").trigger("click");
		}
	});
	
	$(".btn_searchUserPop").on("click",function(e){
		var e = $(this).data("e");
		
		var target = "";
		if(e == 'user') target = lazyUser;
		else if(e == 'vehicle') target = lazyVehicle;
		
		$.lazyLoader.search(target);
		
	});
	
	$(".btn_close").on("click",function(){
		$("#"+$(this).closest("div").attr("id")).dialog("close");
	});
});

function calCost(){
	var length = $(".gcbox").size();
	var tot = 0;
	var strHtml = '';
	console.log(length);
	$(".gcbox").each(function(index){
		var kind = $(this).find(".selectKind :selected").text();
		var val = $(this).find(".selectKind :selected").val();
		var cost = $(this).find(".inputCost").val();
		
		strHtml += '' + (val?kind:"") + ' ' + number_format(cost?cost:"0") + '원';
		tot += parseInt(cost?cost:"0");
		
		if(index != length - 1) strHtml += " + ";
	})
	$(".costList").text(strHtml+" = 총");
	$(".totalCost").text(number_format(tot) + "원");
}

</script>
<form id="registFrm">
	<input type="hidden" name="allocateDate" value="" />
	<input type="hidden" name="allocateKey" value="" />
	<input type="hidden" name="selectKind" value="" />
	<input type="hidden" name="inputCost" value="" />
	<input type="hidden" name="inputNote" value="" />
</form>
<div class="right_layout">
	<div class="detail_view">
		<h2 class="h2tit">유지비 등록</h2>
		<div class="gcbox">
			<%-- <a href="#none" class="gc_close"><img src="${pathCommon}/images/close.jpg" alt="닫기" /></a> --%>
			<div class="gct">
				<input type="text" class="date datePicker allocateDate" style="width:160px; height:32px; border:1px solid #c0c0c0;"/>
				<input type="text" class='btn_ajaxPop' id="searchVehicle" readonly style="padding-left:5px; height:36px;width:100px; height:32px; border:1px solid #c0c0c0;" placeholder="차량선택"/>
				<input type="text" class='btn_ajaxPop' id="searchUser" readonly style="padding-left:5px; height:36px;width:100px; height:32px; border:1px solid #c0c0c0;" placeholder="사용자선택"/>
				<input type="hidden" id="vehicle">
				<input type="hidden" id="account">
			</div>
			<div class="gcb">
				<div class="gcb_sel">
					<select class="select form selectKind" style="width: 160px;">
					</select>
					<div class="gc_input">
						<input type="text" class="text numberOnly inputCost" style="width:198px;" />
						<span>원</span>
					</div>
				</div>
				<div class="gcb_area">
					<textarea class="inputNote" cols="30" rows="10" placeholder="메모"></textarea>
				</div>
			</div>
		</div>
		<!-- 합계 -->
		<div class="gc_total">
			<dl>
				<dt>차량경비 합계</dt>
				<dd><span class="costList"></span> <strong class="color_red totalCost"></strong></dd>
			</dl>
		</div>
		<!-- 버튼 -->
		<div class="btn_area2 mgt20">
		<a href="#" class="btn_cancle">취소</a>
		<a href="#none" class="a_accept">등록</a></div>
	</div>
</div>
<div id="dialogUser" title="사용자 선택" class="myPopup" style="display: none">
	<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt=""></a>		
	<p class="pop_txt">
		사용자 선택
	</p>
	<div class="p_relative">
		<form id='popupSearchFormUser' onSubmit="return false;">
			<div class="tab_select">
				<select class="select form" name="searchOrder" style="height:22px;">
					<option value="regDate">최근 등록순</option>
				</select>
			</div>
			<strong class="search_area">
				<input type="text" placeholder="직접검색" id="input_searchUserPop" name="searchUserNm" class='btn_searchUserPopText'/><a href="#none" class="btn_searchUserPop" data-e="user"><img src="${pathCommon}/images/btn_search2.jpg" alt=""></a>
			</strong>
		</form>
	</div>
	<table class="table1 table1_1">
		<colgroup>
			<col width="60">
			<col width="170">
			<col width="150">
			<col width="140">
			<col width="150">
		</colgroup>
		<thead>
			<tr>
				<th>선택</th>
				<th>사용자 정보</th>
				<th>부서/직급</th>
				<th>연락처</th>
				<th>비고</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<div id="dialogVehicle" title="차량 선택" class="myPopup" style="display: none">
	<a href="#none" class="btn_close"><img src="${pathCommon}/images/close3.jpg" alt=""></a>		
	<p class="pop_txt">
		차량 선택
	</p>
	<div class="p_relative">
		<form id='popupSearchFormVehicle' onSubmit="return false;">
			<input type="hidden" name="searchType" value="plateNum"/>
			<div class="tab_select">
				<select class="select form" name="searchOrder" style="height:22px;">
					<option value="regDate">최근 등록순</option>
				</select>
			</div>
			<strong class="search_area">
				<input type="text" placeholder="직접검색" id="input_searchUserPop" name="searchText" class='btn_searchUserPopText'/><a href="#none" class="btn_searchUserPop" data-e="vehicle"><img src="${pathCommon}/images/btn_search2.jpg" alt=""></a>
			</strong>
		</form>
	</div>
	<table class="table1 table1_1">
		<colgroup>
			<col width="60">
			<col width="170">
			<col width="150">
			<col width="*">
			<col width="80">
		</colgroup>
		<thead>
			<tr>
				<th>선택</th>
				<th>차량정보</th>
				<th>년식/주행거리</th>
				<th>단말</th>
				<th>사용여부</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>