<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script type="text/javascript">
var lazy;
var lazyUser;
$(document).ready(function(){
	initMenuSel("M2001");

	$(".btn_search").on("click",function(){
		$.lazyLoader.search(lazy);
	});
	
	$("#searchText").on("keyup",function(e){
		if(e.keyCode == 13) $(".btn_search").trigger("click");
	});
	
	if(!lazy)
		lazy = $("#contentsTable").lazyLoader({
			searchFrmObj : $("#searchFrm")
			,searchFrmVal : {
				searchType : $("#searchType")
				,searchText : $("#searchText")
				,searchOrder : $("#searchOrder")
				,selectReport : $("#selectReport")
				,searchFlag : $("#searchFlag")
			}
			,scrollRow : 5
			,rowHeight : 80
			,limit : 5
			,loadUrl : "/allocate/getAllocateList.do"
			,initSearch : true
			,createDataHtml : function(r){
				var data = r.result;
				console.log(data);			
				var strHtml = '';
				
				for(var i = 0 ; i < data.length ; i++){
					var vo = data[i];
					strHtml += '<tr>';
					strHtml += '<th class="">';
					strHtml += '<strong class="car_img"><img src="${path}/common/images/vehicle/'+vo.imgS+'.png" alt="" /></strong>';
					strHtml += '<a href="#" class="btn_vehicleDetail" data-key='+vo.vehicleKey+'>';
					strHtml += '<span>'+vo.modelMaster+'</span>';
					strHtml += ''+vo.plateNum;
					strHtml += '</a>';
					strHtml += '</th>';
					strHtml += '<th>';
					strHtml += '<img src='+userImg(vo.driverImg)+' alt="" />';
					strHtml += '<a href="#" class="btn_userDetail" data-key='+vo.accountKey+'>';
					strHtml += '<span>'+convertNullString(vo.corpGroupNm)+'</span>';
					strHtml += ''+convertNullString(vo.corpPosition)+'/'+vo.name+'';
					strHtml += '</a>';
					strHtml += '</th>';
					strHtml += '<th>';
					strHtml += '<p><span>시작/종료 </span>'+vo.startDate+' ~ '+vo.endDate+'</p>';
					strHtml += '<p><span>업무명 </span>'+vo.title+'</p>';
					strHtml += '</th>';
					strHtml += '<td>';
					if(vo.returnFlag == '1'){
						strHtml += '<b class="b1">';
						strHtml += '제출완료';
						strHtml += '<span class="submit_ok"><a href="#none" class="a_flag" data-key='+vo.allocateKey+'>상태변경</a></span>';	
					}else{
						strHtml += '<b class="b2">';
						strHtml += '미제출';
						strHtml += '<span class="submit_ok"></span>';	
					}
					strHtml += '</b><a href="#" class="a_send" data-key='+vo.allocateKey+'><img src="${pathCommon}/images/btn3.jpg" alt=""></a>';
					strHtml += '</td></tr>';
				}

				//<span class="ico1">운행중</span>
				//<span class="ico2"><span>주차중</span><a href="#">위치확인</a></span>
				//<span class="ico3">미배차</span>
				//<span class="ico4">운행대기</span>
				return strHtml;
				
			}
		});
	
	$("body").on("click",".a_send",function(){
		var allocateKey = $(this).data("key");
		$VDAS.instance_post("/vehicleMng/vehicleReportDetail.do",{allocateKey:allocateKey});
	});
	
	$("body").on("click",".btn_vehicleDetail",function(){
		var vehicleKey = $(this).data("key");
		$VDAS.instance_post("/drivingMng/searchVehicleDetail.do",{vehicleKey:vehicleKey});
	});

	$("body").on("click",".btn_userDetail",function(){
		var accountKey = $(this).data("key");
		$VDAS.instance_post("/drivingMng/searchUserDetail.do",{accountKey:accountKey});
	});
	
	$("body").on("click",".tab_select_btn1 a",function(){
		$(".tab_select_btn1 a").removeClass("on");
		$(this).addClass("on");
	});
	
	$("body").on("click",".a_flag",function(){
		var allocateKey = $(this).data("key");
		$VDAS.http_post("/vehicleMng/sendReport.do",{allocateKey:allocateKey,flag:"1"},{
			success : function(r){
				alert("보고서 제출이 취소되었습니다.");
				$.lazyLoader.search(lazy);
				getCnts();
			}
		});
	});
	
	$("#searchFlag,#searchOrder").on("change",function(){
		$.lazyLoader.search(lazy);
	});
	
	getCnts();
});


function userImg(a){
	if(a==null) return '${pathCommon}/images/user_none.png'
	else return '${pathCommon}/images/'+a+'.png'
}

function getCnts(){
	$VDAS.http_post("/vehicleMng/getReportCnt.do",{},{
		success : function(r){
			var data = r.result;
			
			$("#titleCnt1").text(data.total+"건");
			$("#titleCnt2").text(data.submit+"건");
			$("#titleCnt3").text(data.unsubmit+"건");
		}
	});
}

</script>
<form id="searchFrm" style="display:none">
	<input type="hidden" name="searchType" value="" />
	<input type="hidden" name="searchText" value="" />
	<input type="hidden" name="searchOrder" value="" />
	<input type="hidden" name="selectReport" value="" />
	<input type="hidden" name="searchFlag" value="" />
</form>
<div class="right_layout">
	<h2 class="tit_sub">
		차량보고서 조회·제출
		<p>사용차량의 운행내역ㆍ경비보고 또는 반납을 위한 보고서를제출합니다.</p>
	</h2>
	<div class="sub_top1">
		<span>총보고서<b id="titleCnt1"></b></span><span>제출<b id="titleCnt2"></b></span><span>미제출<b id="titleCnt3"></b></span>
		<strong id="nowDate"></strong>
	</div>
	<div class="search_box1">
		<form id="frmSearch" method="post" onSubmit="return false;">
			<div class="search_box1_1">
				<div class="select_type1">
					<div class="select_type1_1">
						<select class="sel" id="searchType">
							<option value="">전체</option>
							<option value="plateNum">차량번호</option>
							<option value="modelMaster">차종</option>
							<option value="accountNm">사용자명</option>
							<!-- <option value="drivingLocaiton">운행지역</option> -->
						</select>
					</div>
				</div><input type="text" id="searchText" class="search_input" />
			</div><a href="#none" class="btn_search">조회</a>
		</form>
	</div>
	<div class="tab_layout">
	<!-- 과거버전 
		<div class="pageTab">
			<ul class="tab_nav">
				<li class="tabItem on" data-tab='whole'><a href="#none" >전체차량</a></li>
				<li class='tabItem' data-tab='using'><a href="#none" >이용중인 차량</a></li>
			</ul>
		</div>
	 -->
		<div class="div1 whole" id="contentsDiv">
			<div style="margin-bottom: 10px;">
				<select id="searchOrder" class="select form" style="width:105px;">
					<option value="driving">최근 운행순</option>
					<option value="regDate">최근 등록순</option>
				</select>
			</div>
			<table class="table1" id="contentsTable">
				<colgroup>
					<col style="width:200px;" />
					<col style="width:180px;" />
					<col />
					<col style="width:120px;" />
				</colgroup>
				<thead>
					<tr>
						<th>차량정보</th>
						<th>최근 사용자</th>
						<th>운행정보</th>
						<th>
							<select class='th_select' id="searchFlag">
								<option value="">보고서</option>
								<option value="0">미제출</option>
								<option value="1">제출완료</option>
							</select>
						</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
		<!-- 과거버전 
		<div class="div1 using" id="contentsDiv" style="display:none">
			<div style="margin-bottom: 10px;">
				<select id="searchOrder" class="select form" style="width:105px;">
					<option value="driving">최근 운행순</option>
					<option value="regDate">최근 등록순</option>
				</select>
			</div>
			<table class="table1" id="contentsTable2">
				<colgroup>
					<col style="width:239px;" />
					<col style="width:100px;" />
					<col style="width:100px;" />
					<col />
					<col style="width:110px;" />
				</colgroup>
				<thead>
					<tr>
						<th>차량정보</th>
						<th>사용자</th>
						<th>
							<select class='th_select' id="selectVehicleState">
								<option value="">운행유무</option>
								<option value="1">운행중</option>
								<option value="2">주차중</option>
								<option value="3">미배차</option>
								<option value="4">운행대기</option>
							</select>
						</th>
						<th>사용(예정)일시/업무명</th>
						<th>
							<select class='th_select' id="selectReport">
								<option value="">보고서</option>
								<option value="1">1</option>
								<option value="2">2</option>
							</select>
						</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
		-->
		<div class="txt1">
			<h1>운행유무 표기 안내</h1>
			<p><b>운행대기 </b>배차등록/승인된 사용자가 있으나 아직 운행을 시작하지 않은 차량입니다.</p>
			<p><b>운행중 </b>차량이 현재 운행중으로 엔진종료 후 현재 운행정보 시작/종료일시, 이동거리가 시스템에 기록됩니다.</p>
			<p><b>주차중 </b>배차등록된 사용자가 현재 차량을 주차중입니다.</p>
			<p><b>미배차 </b>배차 등록된 사용자 없는 차량입니다.</p>
			<h2>주차위치 안내</h2>
			<p>위치확인을 클릭하시면 사용자 휴대폰 App을 통해 최종 등록된 주차위치가 아래 지도에 표시됩니다.<br>(사용자의 GPS 활용 미동의, GPS 오동작, 지하주차장 이동 등 GPS 수신상태로 고르지 못할 경우 위치조회가 정확하지 않을 수 있습니다.)</p>
			<h2>보고서제출</h2>
			<p>표 우측에 있는 보고서 버튼을 누르면 보고서를 작성하거나 조회할 수 있습니다. 이미 제출 된 보고서는 수정 할 수 없습니다.</p>
		</div>
	</div>
</div>