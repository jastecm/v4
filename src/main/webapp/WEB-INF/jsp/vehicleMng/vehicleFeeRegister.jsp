<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script type="text/javascript">
var lazy;
var lazyManage;
$(document).ready(function(){
	
	
	initMenuSel("M2002");
	
	$("body").on("change",".allocateDate",function(){
		var date = replaceAll($(this).val(),"/","");
		$VDAS.ajaxCreateSelect("/com/getAllocateList.do",{searchDate:date},$(this).next(),false,"선택","","${_LANG}",null,null);
	});
    
	$VDAS.ajaxCreateSelect("/com/getCode.do",{code:"C018"},$(".selectKind"),false,"선택","","${_LANG}",null,null);
	$("body").on("click",".gc_add",function(){
		$(".selectKind").removeClass("last");
		$(".gc_add").before(function(){
			var strHtml = '';
			strHtml += '<div class="gcbox mgt10">';
			strHtml += '<a href="#none" class="gc_close"><img src="${pathCommon}/images/close.jpg" alt="닫기" /></a>';
			strHtml += '<div class="gct">';
			strHtml += '<input type="text" class="date datePicker allocateDate" style="width:160px; height:32px; border:1px solid #c0c0c0;"/>';
			strHtml += '<select class="select form selectAllocate" style="width: 228px;">';
			strHtml += '<option value="">선택</option>';
			strHtml += '</select>';
			strHtml += '</div>';
			strHtml += '<div class="gcb">';
			strHtml += '<div class="gcb_sel">';
			strHtml += '<select class="select form selectKind last" style="width: 160px;">';
			strHtml += '</select>';
			strHtml += '<div class="gc_input">';
			strHtml += '<input type="text" class="text numberOnly inputCost" style="width:198px;" />';
			strHtml += '<span>원</span>';
			strHtml += '</div>';
			strHtml += '</div>';
			strHtml += '<div class="gcb_area">';
			strHtml += '<textarea class="inputNote" cols="30" rows="10" placeholder="메모"></textarea>';
			strHtml += '</div>';
			strHtml += '</div>';
			strHtml += '</div>';
			return strHtml;
		});
		$VDAS.ajaxCreateSelect("/com/getCode.do",{code:"C018"},$(".last"),false,"선택","","${_LANG}",null,null);
        $( ".datePicker" ).datepicker({});
	});
	
	$("body").on("click",".a_accept",function(){
		var length = $(".gcbox").size();
		var vaild = 0;
		$("#registFrm input[name=allocateDate]").val("");
		$("#registFrm input[name=allocateKey]").val("");
		$("#registFrm input[name=selectKind]").val("");
		$("#registFrm input[name=inputCost]").val("");
		$("#registFrm input[name=inputNote]").val("");
		$(".gcbox").each(function(index){
			var obj = $(this).find(".allocateDate");
			var input = $("#registFrm input[name=allocateDate]");
			if(obj.val() != null && obj.val() != '') input.val(input.val() + replaceAll(obj.val(),"/",""));
			if(index != length - 1) input.val(input.val()+",");
			
			var obj = $(this).find(".selectAllocate");
			var input = $("#registFrm input[name=allocateKey]");
			if(obj.val() != null && obj.val() != '') input.val(input.val() + obj.val());
			else {
				vaild = 1;
				obj.prev().focus();
				return false;
			}
			if(index != length - 1) input.val(input.val()+",");

			
			obj = $(this).find(".selectKind");
			input = $("#registFrm input[name=selectKind]");
			if(obj.val() != null && obj.val() != '') input.val(input.val() + obj.val());
			else {
				vaild = 2;
				obj.focus();
				return false;
			}
			if(index != length - 1) input.val(input.val()+",");
			
			obj = $(this).find(".inputCost");
			input = $("#registFrm input[name=inputCost]");
			if(obj.val() != null && obj.val() != '') input.val(input.val() + obj.val());
			else {
				vaild = 3;
				obj.focus();
				return false;
			}
			if(index != length - 1) input.val(input.val()+",");
			
			obj = $(this).find(".inputNote");
			input = $("#registFrm input[name=inputNote]");
			input.val(input.val() + obj.val());
			if(index != length - 1) input.val(input.val()+",");
		});
		console.log($("#registFrm").serialize());
		
		if(vaild == 0){
			$VDAS.http_post("/allocate/registFee.do",$("#registFrm").serialize(),{
				success:function(r){
					$VDAS.instance_post("/vehicleMng/vehicleFee.do",{});
				}
			});
		}
		else if(vaild == 1) alert("배차를 선택해주세요.");
		else if(vaild == 2) alert("경비 유형을 선택해주세요.");
		else if(vaild == 3) alert("경비를 입력해주세요.");
			
	});
	
	$("body").on("click",".btn_cancle",function(){
		$VDAS.instance_post("/vehicleMng/vehicleFee.do",{});
	});
	
	$("body").on("click",".gc_close",function(){
		$(this).parentsUntil(".detail_view").remove();
		calCost();
	});
	$("body").on("change",".inputCost,.selectKind",function(){
		calCost();
	});
});


function calCost(){
	var length = $(".gcbox").size();
	var tot = 0;
	var strHtml = '';
	console.log(length);
	$(".gcbox").each(function(index){
		var kind = $(this).find(".selectKind :selected").text();
		var val = $(this).find(".selectKind :selected").val();
		var cost = $(this).find(".inputCost").val();
		
		strHtml += '' + (val?kind:"") + ' ' + number_format(cost?cost:"0") + '원';
		tot += parseInt(cost?cost:"0");
		
		if(index != length - 1) strHtml += " + ";
	})
	$(".costList").text(strHtml+" = 총");
	$(".totalCost").text(number_format(tot) + "원");
}

</script>
<form id="registFrm">
	<input type="hidden" name="allocateDate" value="" />
	<input type="hidden" name="allocateKey" value="" />
	<input type="hidden" name="selectKind" value="" />
	<input type="hidden" name="inputCost" value="" />
	<input type="hidden" name="inputNote" value="" />
</form>
<div class="right_layout">
	<div class="detail_view">
		<h2 class="h2tit">차량경비 등록</h2>
		<div class="gcbox">
			<a href="#none" class="gc_close"><img src="${pathCommon}/images/close.jpg" alt="닫기" /></a>
			<div class="gct">
				<input type="text" class="date datePicker allocateDate" style="width:160px; height:32px; border:1px solid #c0c0c0;"/>
				<select class="select form selectAllocate" style="width: 228px;">
					<option value="">선택</option>
				</select>
			</div>
			<div class="gcb">
				<div class="gcb_sel">
					<select class="select form selectKind" style="width: 160px;">
					</select>
					<div class="gc_input">
						<input type="text" class="text numberOnly inputCost" style="width:198px;" />
						<span>원</span>
					</div>
				</div>
				<div class="gcb_area">
					<textarea class="inputNote" cols="30" rows="10" placeholder="메모"></textarea>
				</div>
			</div>
		</div>
		<!-- 추가 버튼 -->
		<div class="gc_add"></div>
		<!-- 합계 -->
		<div class="gc_total">
			<dl>
				<dt>차량경비 합계</dt>
				<dd><span class="costList"></span> <strong class="color_red totalCost"></strong></dd>
			</dl>
		</div>
		<!-- 버튼 -->
		<div class="btn_area2 mgt20">
		<a href="#" class="btn_cancle">취소</a>
		<a href="#none" class="a_accept">등록</a></div>
	</div>
</div>