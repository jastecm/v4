<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>  
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta http-equiv="Content-Script-Type" content="text/javascript"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <title><tiles:insertAttribute name="title" ignore="true" /></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/content.css"/>
	<%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/js/jquery-ui.css"/> --%>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/common/css/style.css">
	
    <script src="${pageContext.request.contextPath}/common/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/common/js/common.js"></script>
    <script src="${pageContext.request.contextPath}/common/js/topup.js"></script>
    
<%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/js/jquery-ui.min.css"/> --%>
<%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/jsDatePick_ltr.min.css" media="all" /> --%>
	
	<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery-ui.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jsDatePick.jquery.min.1.3.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/util.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/message/${_LANG}.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/commonDev.js"></script>
	<script type="text/javascript" src="https://www.gstatic.com/firebasejs/4.1.3/firebase.js"></script>

</head>

<body>
<script type="text/javascript">

var $sw;
var fcm_config = {
		apiKey : "AAAAoqqBUIs:APA91bGw5qx4h2WwkzfziY_EjKZa2FMGvPIHgIJQgV7MninxALQcFb1iYCGtQrWjUKn92N3o3l1YJlB0TLTHmL_WLJOjKP7h_Z_8WhaIHR9kO1AQxFSRNUKC297RLq8TeOXv-hwGQ7Fl",
		authDomain: "vdaspro-76df5.firebaseapp.com",
		databaseURL: "https://vdaspro-76df5.firebaseio.com",
		projectId: "vdaspro-76df5",
		storageBucket: "",
		messagingSenderId: "698645303435"
	};
var messaging;

$(document).ready(function(){
	jQuery(function($){
		 $.datepicker.regional['ko'] = {
		  closeText: '닫기',
		  prevText: '이전',
		  nextText: '다음',
		  currentText: '오늘',
		  monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
		  monthNamesShort: ['1','2','3','4','5','6','7','8','9','10','11','12'],
		  dayNames: ['일','월','화','수','목','금','토'],
		  dayNamesShort: ['일','월','화','수','목','금','토'],
		  dayNamesMin: ['일','월','화','수','목','금','토'],
		  weekHeader: 'Wk',
		  dateFormat: 'yy/mm/dd',
		  firstDay: 0,
		  isRTL: false,
		  showMonthAfterYear: true,
		  yearSuffix: ''};
		 
		 $.datepicker.sep = "/";		 
		 $.datepicker.setDefaults($.datepicker.regional['ko']);

		 $('.datePicker').datepicker({
			 showButtonPanel: true // 하단 today, done  버튼기능 추가 표시 (기본은 false)		 	
		 });
		 
	});
	
	$(".datePickerImg").on("click",function(){
		$("#"+$(this).data("target")).trigger("focus");	
	});
	
	
	firebase.initializeApp(fcm_config);

	messaging = firebase.messaging();	
});
</script>

<!-- body 컨텐츠부분시작 -->
	<tiles:insertAttribute name="gnb"/>
	<tiles:insertAttribute name="lnb"/>
	<tiles:insertAttribute name="bookmark"/>
	
	<tiles:insertAttribute name="content"/>
	
	<tiles:insertAttribute name="footer"/>			
<!-- body 컨텐츠부분끝 -->	

</body>
</html>