<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="title" content="차량운행 관리 서비스 VIEW CAR" />
    <meta name="description" content="" />
    <title><tiles:insertAttribute name="title" ignore="true" /></title>

	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/earlyaccess/notosanskr.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/common/new/css/jquery-ui.min.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/common/new/css/style.css" />
    
    <link rel="stylesheet" href="${pageContext.request.contextPath}/common/css/jquery.monthpicker.css" />
    
    <style type="text/css">
    /* user pop style*/
	div.scroll-table tbody {
    	height: 319px;
	}
	
	#commonUserDialog .scroll-table .table thead th:nth-of-type(1),
	#commonUserDialog .scroll-table .table tbody td:nth-of-type(1) {
	    width: 81px;
	}
	
	#commonUserDialog .scroll-table .table thead th:nth-of-type(2),
	#commonUserDialog .scroll-table .table tbody td:nth-of-type(2),
	#commonUserDialog .scroll-table .table thead th:nth-of-type(3),
	#commonUserDialog .scroll-table .table tbody td:nth-of-type(3) {
	    width: 130px;
	}
	
	#commonUserDialog .scroll-table .table thead th:nth-of-type(4),
	#commonUserDialog .scroll-table .table tbody td:nth-of-type(4),
	#commonUserDialog .scroll-table .table thead th:nth-of-type(5),
	#commonUserDialog .scroll-table .table tbody td:nth-of-type(5) {
	    width: 210px;
	}
	
	#commonUserDialog .scroll-table .table thead th:nth-of-type(6) {
	    width: 137px;
	}
	
	#commonUserDialog .scroll-table .table tbody td:nth-of-type(6) {
	    width: 120px;
	}
	
	/* vehicle pop style*/
	
	
	#commonVehicleDialog .scroll-table .table thead th,
	#commonVehicleDialog .scroll-table .table tbody td {
	    width: 110px;
	}
	
	#commonVehicleDialog .scroll-table .table thead th:nth-of-type(1),
	#commonVehicleDialog .scroll-table .table tbody td:nth-of-type(1) {
	    width: 81px;
	}
	
	#commonVehicleDialog .scroll-table .table thead th:nth-of-type(2),
	#commonVehicleDialog .scroll-table .table tbody td:nth-of-type(2),
	#commonVehicleDialog .scroll-table .table thead th:nth-of-type(3),
	#commonVehicleDialog .scroll-table .table tbody td:nth-of-type(3) {
	    width: 150px;
	}
	
	#commonVehicleDialog .scroll-table .table thead th:nth-of-type(4),
	#commonVehicleDialog .scroll-table .table tbody td:nth-of-type(4) {
	    width: 170px;
	}
	
	#commonVehicleDialog .scroll-table .table thead th:last-child {
	    width: 127px;
	}
	
	/* group pop style*/
	
	
	#commonGroupDialog .scroll-table .table thead th,
	#commonGroupDialog .scroll-table .table tbody td {
	    width: 77px;
	}
	
	#commonGroupDialog .scroll-table .table thead th:nth-of-type(2),
	#commonGroupDialog .scroll-table .table tbody td:nth-of-type(2),
	#commonGroupDialog .scroll-table .table thead th:nth-of-type(3),
	#commonGroupDialog .scroll-table .table tbody td:nth-of-type(3),
	#commonGroupDialog .scroll-table .table thead th:nth-of-type(4),
	#commonGroupDialog .scroll-table .table tbody td:nth-of-type(4)  {
	    width: 180px;
	}
	
	
	#commonGroupDialog .scroll-table .table thead th:nth-of-type(5),
	#commonGroupDialog .scroll-table .table tbody td:nth-of-type(5),
	#commonGroupDialog .scroll-table .table thead th:nth-of-type(6),
	#commonGroupDialog .scroll-table .table tbody td:nth-of-type(6) {
	    width: 70px;
	}
	
	#commonGroupDialog .scroll-table .table thead th:last-child {
	    width: 140px;
	}
	#commonGroupDialog .scroll-table .table tbody td:last-child {
	    width: 130px;
	}
	</style>    
    <%@include file="/WEB-INF/jsp/common/common.jsp"%>
    
    <script src="${pageContext.request.contextPath}/common/new/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/common/new/js/jquery-ui.min.js"></script>
    <script src="${pageContext.request.contextPath}/common/new/js/jquery-debounce.js"></script>
    <script src="${pageContext.request.contextPath}/common/new/js/base64.js"></script>
    <script src="${pageContext.request.contextPath}/common/new/js/chart.js"></script>
    <script src="${pageContext.request.contextPath}/common/new/js/common.js"></script>
    
    <script src="${pageContext.request.contextPath}/common/js/xlsx/xlsx.full.min.js" type="text/javascript"></script>
    
    <script src="${pageContext.request.contextPath}/common/js/jquery.form.min.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/common/js/underscore-min.js" type="text/javascript"></script>
    
    <script src="${pageContext.request.contextPath}/common/js/jquery.transit.min.js" type="text/javascript"></script>
    
    <script src="${pageContext.request.contextPath}/common/js/util.js"></script>
    <script src="${pageContext.request.contextPath}/common/new/js/v4.js"></script>
    <script src="${pageContext.request.contextPath}/common/new/js/commonV4.js"></script>
    
    <script src="${pageContext.request.contextPath}/common/js/jquery.monthpicker.js"></script>
    
    
    
	<script type="text/javascript">
	$(document).ready(function(){
		$.datepicker.regional['ko'] = {
			  closeText: '확인',
			  prevText: '이전',
			  nextText: '다음',
			  currentText: '오늘',
			  monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
			  monthNamesShort: ['1','2','3','4','5','6','7','8','9','10','11','12'],
			  dayNames: ['일','월','화','수','목','금','토'],
			  dayNamesShort: ['일','월','화','수','목','금','토'],
			  dayNamesMin: ['일','월','화','수','목','금','토'],
			  weekHeader: 'Wk',
			  dateFormat: 'yy/mm/dd', //TODO - used unitDate
			  firstDay: 0,
			  isRTL: false,
			  showMonthAfterYear: true,
			  yearSuffix: '',
			  showButtonPanel: true
			};
			 
			 
			$.datepicker.sep = "/";		 
			$.datepicker.setDefaults($.datepicker.regional['${_LANG}']);
			
			$('.datePicker').datepicker({
				 showButtonPanel: true // 하단 today, done  버튼기능 추가 표시 (기본은 false)		 	
			 });
		$(".commonDialog").dialog({autoOpen:false});
	});
	</script>
	
	
	
</head>

<body>
	<h1 class="hidden">차량운행 관리 서비스 VIEW CAR</h1>

    <!-- 바로가기 메뉴 -->
    <dl id="skip-navigation">
        <dt>바로가기메뉴</dt>
        <dd>
            <a href="#gnb" class="skip">사이트 메뉴 바로가기</a>
        </dd>
        <dd>
            <a href="#container" class="skip">본문 바로가기</a>
        </dd>
    </dl>
    <!--/ 바로가기 메뉴 -->
    
    <div id="wrap"> <!-- wrap 시작 -->
	
	<tiles:insertAttribute name="header"/>
	<tiles:insertAttribute name="section"/>
	<tiles:insertAttribute name="footer"/>
	<tiles:insertAttribute name="aside"/>
	
	<!-- 사용자 선택 팝업 -->
    <div id="commonUserDialog" class="commonDialog" title="사용자 선택">
        <div class="top-info clr">
            <span class="left"></span>
            <span class="right car">
				검색결과 <strong class="commonCounter">0</strong>명
			</span>
        </div>
        	
        <div class="top-search">
            <select name="" class="commonSearchType">
				<option value="">전체</option>
				<option value="groupNm">부서</option>
				<option value="name">이름</option>
				<!-- <option value="accountId">아이디</option> -->
				<option value="corpPosition">직급</option>
			</select>
            <input type="text" name="" class="commonSearchText"/>
            <button class="btn btn03 btn_commonSearch">조회</button>
        </div>
	
		<div class="scroll-table mgt50">
			<table class="table list"></table>
		</div>
        

        <!-- 하단 버튼 -->
        <div class="btn-bottom">
            <button class="btn btn02 btn_commonPopCancle">취소</button>
            <button class="btn btn03 btn_commonPopOk">선택</button>
        </div>
        <!--/ 하단 버튼 -->
    </div>
    <!--/ 사용자 선택 팝업 -->
    
    <!-- 차량 선택 팝업 -->
    <div id="commonVehicleDialog" class="commonDialog" title="차량 선택">
        <div class="top-info clr">
            <span class="left">등록된 모든 차량을 표시합니다.</span>
            <span class="right car">검색결과<strong class="commonCounter">0</strong>대
            </span>
        </div>
        <!-- 상단 검색 -->
        <div class="top-search">
            <select name="" class="commonSearchType">
				<option value="">전체</option>
				<option value="plateNum">차량번호</option>
				<option value="modelMaster">차종</option>
				<option value="deviceSn">단말 시리얼</option>
			</select>
            <input type="text" name="" class="commonSearchText"/>
            <button class="btn btn03 btn_commonSearch" >조회</button>
        </div>
        <!--/ 상단 검색 -->
        <div class="scroll-table mgt50">
			<table class="table list"></table>
		</div>


        <!-- 하단 버튼 -->
        <div class="btn-bottom">
            <button class="btn btn02 btn_commonPopCancle">취소</button>
            <button class="btn btn03 btn_commonPopOk">선택</button>
        </div>
        <!--/ 하단 버튼 -->
    </div>
    <!--/ 차량 선택 팝업 -->
    
    <!-- 차량 선택 팝업 -->
    <div id="commonGroupDialog" class="commonDialog" title="부서 선택">
        <div class="top-info clr">
            <span class="left">등록된 모든 부서를 표시합니다.</span>
            <span class="right car">검색결과<strong class="commonCounter">0</strong>부서
            </span>
        </div>
        <!-- 상단 검색 -->
        <div class="top-search">
            <select name="" class="commonSearchType">
				<option value="">전체</option>
				<option value="groupNm">부서명</option>
				<option value="parentGroupNm">상위부서명</option>
				<option value="managerName">부서장명</option>
			</select>
            <input type="text" name="" class="commonSearchText"/>
            <button class="btn btn03 btn_commonSearch" >조회</button>
        </div>
        <!--/ 상단 검색 -->
        <div class="scroll-table mgt50">
			<table class="table list"></table>
		</div>


        <!-- 하단 버튼 -->
        <div class="btn-bottom">
            <button class="btn btn02 btn_commonPopCancle">취소</button>
            <button class="btn btn03 btn_commonPopOk">선택</button>
        </div>
        <!--/ 하단 버튼 -->
    </div>
    <!--/ 차량 선택 팝업 -->
	</div> <!-- wrap 종료 -->

</body>
</html>