<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>  
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta name="description" content="VIEWCAR - For Your Smart Driving">
<meta property="og:title" content="VIEWCAR">
<meta property="og:description" content="VIEWCAR - For Your Smart Driving">

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/js/jqueryui/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/content.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/content_costom.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/style.css" />
<link rel="manifest" href="${pageContext.request.contextPath}/common/json/manifest.json">
<%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/jsDatePick_ltr.min.css" media="all" /> --%>

<%@include file="/WEB-INF/jsp/common/common.jsp"%>

<script src="${pageContext.request.contextPath}/common/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/common.js"></script>
<script src="${pageContext.request.contextPath}/common/js/topup.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jqueryui/jquery-ui.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.stylish-select.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jsDatePick.jquery.min.1.3.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/util.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.form.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.canvasjs.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.MultiFile.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/message/${_LANG}.js"></script>

<script type="text/javascript" src="https://www.gstatic.com/firebasejs/4.1.3/firebase.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/commonDev.js"></script>


<script>
  
</script>


	
<title><tiles:insertAttribute name="title" ignore="true" /></title>
</head>
<body>
<script type="text/javascript">
$(document).ready(function(){
	jQuery(function($){
		 $.datepicker.regional['ko'] = {
		  closeText: '확인',
		  prevText: '이전',
		  nextText: '다음',
		  currentText: '오늘',
		  monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
		  monthNamesShort: ['1','2','3','4','5','6','7','8','9','10','11','12'],
		  dayNames: ['일','월','화','수','목','금','토'],
		  dayNamesShort: ['일','월','화','수','목','금','토'],
		  dayNamesMin: ['일','월','화','수','목','금','토'],
		  weekHeader: 'Wk',
		  dateFormat: 'yy/mm/dd',
		  firstDay: 0,
		  isRTL: false,
		  showMonthAfterYear: true,
		  yearSuffix: '',
		  showButtonPanel: true
		};
		 
		 
		$.datepicker.sep = "/";		 
		$.datepicker.setDefaults($.datepicker.regional['${_LANG}']);

		$('.datePicker').datepicker({
			showButtonPanel: true // 하단 today, done  버튼기능 추가 표시 (기본은 false)		 	
		});
		 
		$('.datePickerMonth').datepicker({
			changeMonth: true,
	        changeYear: true,
	        showButtonPanel: true,
	        dateFormat: 'yy-mm',
	        showMonthAfterYear: false,
	        calenderHide : true,
	        onClose: function(dateText, inst) { 
	            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
	        },beforeShow : function(input, inst) {
	            
                if ((datestr = $(this).val()).length > 0) {
                    year = datestr.substring(0,4);
                    month = datestr.substring(5,7);
                    $(this).datepicker('option', 'defaultDate', new Date(year, month-1, 1));
                    $(this).datepicker('setDate', new Date(year, month-1, 1));
                    
                }
            }
		});
	    
		 
		 
	});
	
	$(".datePickerImg").on("click",function(){
		$("#"+$(this).data("target")).trigger("focus");	
	});
	
	<c:if test="${VDAS ne null}">
	var $sw;
	var fcm_config = {
		apiKey : "AAAAoqqBUIs:APA91bGw5qx4h2WwkzfziY_EjKZa2FMGvPIHgIJQgV7MninxALQcFb1iYCGtQrWjUKn92N3o3l1YJlB0TLTHmL_WLJOjKP7h_Z_8WhaIHR9kO1AQxFSRNUKC297RLq8TeOXv-hwGQ7Fl",
		authDomain: "vdaspro-76df5.firebaseapp.com",
		databaseURL: "https://vdaspro-76df5.firebaseio.com",
		projectId: "vdaspro-76df5",
		storageBucket: "",
		messagingSenderId: "698645303435",
		token : "${VDAS.fcmKey}"
	};
	
	firebase.initializeApp(fcm_config);

	const messaging = firebase.messaging();

	var fcmToken;
	if ('serviceWorker' in navigator) {  
		navigator.serviceWorker.register($VDAS.path+'/common/js/sw.js').then(function(registration){
			console.log('ServiceWorker registration successful with scope: ',    registration);
			$sw = registration;
		    
			//Initialize Firebase
			messaging.useServiceWorker($sw);
		    
			messaging.requestPermission()
				.then(function() {
					console.log('Notification permission granted.');

		    		messaging.getToken()
		    		.then(function(currentToken) {
						if (currentToken) {
		    		    	sendTokenToServer(currentToken);
		    		  	} else {
							console.log('No Instance ID token available. Request permission to generate one.');
		    		  	}
		    		}).catch(function(err) {
		    		console.log('An error occurred while retrieving token. ', err);
		    	});
		    })
		    .catch(function(err) {
		    	
			});
		}).catch(function(err) {
	    	console.log('ServiceWorker registration failed: ', err);
	  	});
	} else {  
		console.warn('Service workers aren\'t supported in this browser.');  
	}  
	  
	  
	messaging.onMessage(function(payload) {
		console.log('Received foground message ', payload);
		const notificationTitle = payload.notification.title;
		const notificationOptions = {
		    body: payload.notification.body
		    ,icon: payload.notification.icon
		};
	  	return $sw.showNotification(notificationTitle,notificationOptions);
  	});


	function sendTokenToServer(t){
		if(fcm_config.fcmKey != t){
			$VDAS.http_post("/push/refresh.do",{key:t},{
				success : function(r){
					
					$VDAS.http_post("/push/subscribe.do",{topic:"noti",accept:"1"},{
						success : function(r){
							console.log(r);
						}
					});
					$VDAS.http_post("/push/subscribe.do",{topic:"event",accept:"1"},{
						success : function(r){
							console.log(r);
						}
					});
					$VDAS.http_post("/push/subscribe.do",{topic:"test",accept:"1"},{
						success : function(r){
							console.log(r);
						}
					});
				}
			});
		}
	}
	</c:if>
	
	
});


</script>

<div id="wrap">
	<tiles:insertAttribute name="gnb"/>
	<tiles:insertAttribute name="lnb"/>
	<tiles:insertAttribute name="bookmark"/>
	
	<tiles:insertAttribute name="left"/>
	
	<tiles:insertAttribute name="content"/>
	
	<tiles:insertAttribute name="footer"/>				
</div>
</body>
</html>