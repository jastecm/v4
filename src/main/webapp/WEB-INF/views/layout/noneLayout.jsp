<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>  
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta name="description" content="VIEWCAR - For Your Smart Driving">
<meta property="og:title" content="VIEWCAR">
<meta property="og:description" content="VIEWCAR - For Your Smart Driving">

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/content.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/style.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/js/jqueryui/jquery-ui.css"/>
<%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/common/css/jsDatePick_ltr.min.css" media="all" /> --%>

<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script src="${pageContext.request.contextPath}/common/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/common.js"></script>
<script src="${pageContext.request.contextPath}/common/js/topup.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jqueryui/jquery-ui.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.stylish-select.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jsDatePick.jquery.min.1.3.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.form.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/util.js"></script>
<c:set var="_LANG" value="ko"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/message/${_LANG}.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/commonDev.js"></script>
	
<title><tiles:insertAttribute name="title" ignore="true" /></title>
</head>
<body>
<script type="text/javascript">
$(document).ready(function(){
});
</script>

<div id="wrap">
	<tiles:insertAttribute name="content"/>
</div>
</body>
</html>