<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="title" content="차량운행 관리 서비스 VIEW CAR" />
    <meta name="description" content="" />
    <title><tiles:insertAttribute name="title" ignore="true" /></title>

	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/earlyaccess/notosanskr.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/common/new/css/jquery-ui.min.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/common/new/css/style.css" />
    
     <%@include file="/WEB-INF/jsp/common/common.jsp"%>
     
	<script src="${pageContext.request.contextPath}/common/new/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/common/new/js/jquery-ui.min.js"></script>
    <script src="${pageContext.request.contextPath}/common/new/js/jquery-debounce.js"></script>
    <script src="${pageContext.request.contextPath}/common/new/js/base64.js"></script>
    <script src="${pageContext.request.contextPath}/common/new/js/common.js"></script>
    
    <script src="${pageContext.request.contextPath}/common/js/util.js"></script>
    <script src="${pageContext.request.contextPath}/common/new/js/v4.js"></script>
    <script src="${pageContext.request.contextPath}/common/new/js/commonV4.js"></script>
    
</head>

<body>
	<h1 class="hidden">차량운행 관리 서비스 VIEW CAR</h1>

    <!-- 바로가기 메뉴 -->
    <dl id="skip-navigation">
        <dt>바로가기메뉴</dt>
        <dd>
            <a href="#gnb" class="skip">사이트 메뉴 바로가기</a>
        </dd>
        <dd>
            <a href="#container" class="skip">본문 바로가기</a>
        </dd>
    </dl>
    <!--/ 바로가기 메뉴 -->
    
    <div id="wrap"> <!-- wrap 시작 -->
	
	<tiles:insertAttribute name="header"/>
	<tiles:insertAttribute name="section"/>
	<tiles:insertAttribute name="footer"/>
	<tiles:insertAttribute name="aside"/>
	
	</div> <!-- wrap 종료 -->

</body>
</html>