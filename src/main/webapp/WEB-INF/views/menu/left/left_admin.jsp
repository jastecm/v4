<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script type="text/javascript">
function initMenuSel(m){
	$("#"+m).addClass("on");
	$(".left_menu li").mouseover(function(){
		$(this).addClass("on");
	});
	$(".left_menu li").mouseleave(function(){
		if($(this).text() != $("#"+m).text())
			$(this).removeClass("on");
	})
}
</script>
<div class="sub_layout">
	<div class="left_menu sidebar">
		<div>
			<h1>관리자메뉴</h1>
			<ul>
				<li id="M9001"><a href="${defaultPath}/admin/corpInfo.do">법인정보</a></li>
				<li id="M9001"><a href="${defaultPath}/admin/noticeRequest.do">공지사항등록</a></li>
				<li id="M9002"><a href="${defaultPath}/admin/faqRequest.do">FAQ등록</a></li>
				<li id="M9003"><a href="${defaultPath}/admin/festivalRequest.do">이벤트등록</a></li>
				<li id="M9004"><a href="${defaultPath}/admin/serviceComplainReple.do">서비스불편답변</a></li>
				<li id="M9005"><a href="${defaultPath}/admin/afterServiceReple.do">A/S답변</a></li>
				<li id="M9003"><a href="${defaultPath}/admin/push.do">푸쉬</a></li>
				<li id="M9003"><a href="${defaultPath}/admin/firmUpload.do">펌웨어업로드</a></li>
				<li id="M9004"><a href="${defaultPath}/admin/deviceManagement.do">*OBD관리</a></li>
				<li id="M9005"><a href="${defaultPath}/admin/sendPushMessage.do">*푸쉬보내기</a></li>
				<li id="M9006"><a href="${defaultPath}/admin/bizHpBoardRequest.do">*BIZ HP 글쓰기</a></li>
				<li id="M9007"><a href="${defaultPath}/admin/resetDevice.do">*OBD 리셋</a></li>
				<li id="M9008"><a href="${defaultPath}/admin/issueRequest.do">*이슈보드</a></li>
				<li id="M9009"><a href="${defaultPath}/admin/appVer.do">*앱버전</a></li>
				<li id="M9010"><a href="${defaultPath}/admin/deviceVehicleSearch.do">*단말차량검색</a></li>
				<li id="M9011"><a href="${defaultPath}/admin/user.do">*관리자검색</a></li>
			</ul>
		</div>
	</div>
	<div class="right_con">
			