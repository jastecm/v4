<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script type="text/javascript">
function initMenuSel(m){
	$("#"+m).addClass("on");
	$(".left_menu li").mouseover(function(){
		$(this).addClass("on");
	});
	$(".left_menu li").mouseleave(function(){
		if($(this).text() != $("#"+m).text())
			$(this).removeClass("on");
	})
}
</script>
<!-- 중앙 정렬을 위해서 padding-left을 0으로 지정한다. -->
<div class="sub_layout" style="padding-left: 0px;">
	<div class="left_menu sidebar">
		
	</div>
	<div class="right_con">
			