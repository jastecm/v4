<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script type="text/javascript">
function initMenuSel(m){
	$("#"+m).addClass("on");
	$(".left_menu li").mouseover(function(){
		$(this).addClass("on");
	});
	$(".left_menu li").mouseleave(function(){
		if($(this).text() != $("#"+m).text())
			$(this).removeClass("on");
	})
}
</script>
<div class="sub_layout">
	<div class="left_menu sidebar">
		<div>
			<h1><a href="${defaultPath}/menual/index.do">메뉴얼</a></h1>
			<ul>
				<li id="M10001"><a href="${defaultPath}/menual/user.do">회원정보관리</a></li>
				<li id="M10002"><a href="${defaultPath}/menual/config.do">환경설정</a></li>
                <li id="M10003"><a href="${defaultPath}/menual/driving.do">운행관리</a></li>
				<li id="M10004"><a href="${defaultPath}/menual/vehicle.do">차량관리</a></li>
				<li id="M10005"><a href="${defaultPath}/menual/clinic.do">차량정비</a></li>
				<li id="M10006"><a href="${defaultPath}/menual/analysis.do">드라이빙 분석</a></li>
				<li id="M10007"><a href="${defaultPath}/menual/helpdesk.do">고객센터</a></li>

			</ul>
		</div>
	</div>
	<div class="right_con">
			