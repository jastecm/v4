<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script type="text/javascript">
function initMenuSel(m){
	$("#"+m).addClass("on");
	$(".left_menu li").mouseover(function(){
		$(this).addClass("on");
	});
	$(".left_menu li").mouseleave(function(){
		if($(this).text() != $("#"+m).text())
			$(this).removeClass("on");
	})
}
</script>
<div class="sub_layout">
	<div class="left_menu">
		<div>
			<h1><img src="${pageContext.request.contextPath}/common/images/${_LANG}/lnb_h1_2.png" alt="${_L.label1}" /></h1>
			<ul><c:if test="${VDAS.allocationUsed eq '1' }">
				<li id="M2001"><a href="${defaultPath}/vehicle/vehicleInterval.do"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/lnb2_m1.png" alt="${_L.label2}" class="m_off" /><img src="${pageContext.request.contextPath}/common/images/${_LANG}/lnb2_m1_on.png" alt="${_L.label2}" class="m_on" /></a></li>	
				</c:if>
				<li id="M2003"><a href="${defaultPath}/vehicle/vehicleReport.do"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/lnb2_m3.png" alt="${_L.label3}" class="m_off" /><img src="${pageContext.request.contextPath}/common/images/${_LANG}/lnb2_m3_on.png" alt="${_L.label3}" class="m_on" /></a></li>
				<li id="M2004"><a href="${defaultPath}/vehicle/vehicleCost.do"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/lnb2_m4.png" alt="${_L.label4}" class="m_off" /><img src="${pageContext.request.contextPath}/common/images/${_LANG}/lnb2_m4_on.png" alt="${_L.label4}" class="m_on" /></a></li>
				<li id="M2005"><a href="${defaultPath}/vehicle/vehiclePenalty.do"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/lnb2_m5.png" alt="${_L.label5}" class="m_off" /><img src="${pageContext.request.contextPath}/common/images/${_LANG}/lnb2_m5_on.png" alt="${_L.label5}" class="m_on" /></a></li>
			</ul>
		</div>
	</div>
	<div class="right_con">
			