<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<fmt:setLocale value="${_LANG}" scope="request" />
<fmt:setBundle basename="properties.messages.rest.message" />

<script type="text/javascript">
function initMenuSel(m){
	$("#"+m).addClass("on");
	$(".left_menu li").mouseover(function(){
		$(this).addClass("on");
	});
	$(".left_menu li").mouseleave(function(){
		if($(this).text() != $("#"+m).text())
			$(this).removeClass("on");
	})
}
</script>
<div class="sub_layout">
	<div class="left_menu sidebar">
		<div>
			<h1><fmt:message key='leftconfig.setting'/></h1>
			<ul><c:if test="${VDAS.rule eq '0' or VDAS.rule eq '1' }">
				<li id="M0001"><a href="${defaultPath}/config/registedVehicle.do"><fmt:message key='leftconfig.registedVehicle'/></a></li>
				<li id="M0002"><a href="${defaultPath}/config/registedAccount.do"><fmt:message key='leftconfig.registedAccount'/></a></li>
				<li id="M0003"><a href="${defaultPath}/config/registedGroup.do"><fmt:message key='leftconfig.registedGroup'/></a></li>
				<li id="M0007"><a href="${defaultPath}/config/registedGeofence.do"><fmt:message key='leftconfig.registedGeofence'/></a></li>
				<li id="M0004"><a href="${defaultPath}/config/registedWorkingArea.do"><fmt:message key='leftconfig.registedWorkingArea'/></a></li>
				<li id="M0005"><a href="${defaultPath}/config/registedWorkingTime.do"><fmt:message key='leftconfig.registedWorkingTime'/></a></li>
				</c:if>
				<li id="M0006"><a href="${defaultPath}/config/pcMngUpdate.do"><fmt:message key='leftconfig.pcMngUpdate'/></a></li>
			</ul>
		</div>
	</div>
	<div class="right_con">	
			