<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>    
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<fmt:setLocale value="${_LANG}" scope="request" />
<fmt:setBundle basename="properties.messages.rest.message" />

<script type="text/javascript">
function initMenuSel(m){
	$("#"+m).addClass("on");
	$(".left_menu li").mouseover(function(){
		$(this).addClass("on");
	});
	$(".left_menu li").mouseleave(function(){
		if($(this).text() != $("#"+m).text())
			$(this).removeClass("on");
	})
}
</script>
<div class="sub_layout">
	<div class="left_menu sidebar">
		<div>
			<h1><fmt:message key='leftReport.leftMenu'/></h1>
			<ul>
				<li id="M4002"><a href="${defaultPath}/report/personalReport.do"><fmt:message key='leftReport.leftMenu1'/></a></li>
				<li id="M4001"><a href="${defaultPath}/report/integrationReport.do"><fmt:message key='leftReport.leftMenu2'/></a></li>
				<%-- <li id="M4003"><a href="${defaultPath}/report/compareVehicle.do">기간별 차량 비교</a></li> --%>
				<li id="M4003"><a href="${defaultPath}/report/drivingReport.do"><fmt:message key='leftReport.leftMenu3'/></a></li>
			</ul>
		</div>
	</div>
	<div class="right_con">
			