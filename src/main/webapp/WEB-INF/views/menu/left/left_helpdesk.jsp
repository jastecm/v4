<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script type="text/javascript">
function initMenuSel(m){
	$("#"+m).addClass("on");
	$(".left_menu li").mouseover(function(){
		$(this).addClass("on");
	});
	$(".left_menu li").mouseleave(function(){
		if($(this).text() != $("#"+m).text())
			$(this).removeClass("on");
	})
}
</script>
<div class="sub_layout">
	<div class="left_menu sidebar">
		<div>
			<h1>고객센터</h1>
			<ul>
				<li id="M6001"><a href="${defaultPath}/helpdesk/notice.do">공지사항</a></li>
				<li id="M6002"><a href="${defaultPath}/helpdesk/faq.do">FAQ</a></li>
				<li id="M6005"><a href="${defaultPath}/helpdesk/festival.do">이벤트·할인정보</a></li>
				<li id="M6003"><a href="${defaultPath}/helpdesk/serviceComplain.do">서비스 불편신고</a></li>
				<li id="M6004"><a href="${defaultPath}/helpdesk/afterService.do">제품 A/S접수</a></li>
			</ul>
		</div>
	</div>
	<div class="right_con">
			