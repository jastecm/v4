<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script type="text/javascript">
function initMenuSel(m){
	$("#"+m).addClass("on");
	$(".left_menu li").mouseover(function(){
		$(this).addClass("on");
	});
	$(".left_menu li").mouseleave(function(){
		if($(this).text() != $("#"+m).text())
			$(this).removeClass("on");
	})
}
</script>
<div class="sub_layout">
	<div class="left_menu sidebar" >
		<div>
			<h1>회원정보 관리</h1>			
			<ul>
				<c:if test="${VDAS.rule eq '2' or VDAS.rule eq '1'}">
				<li id="M5001"><a href="${defaultPath}/user/userConfig.do">회원정보 설정</a></li>
				</c:if>
				<li id="M5002"><a href="${defaultPath}/user/changePassword.do">비밀번호 변경</a></li>
				<li id="M5003"><a href="${defaultPath}/user/myFavoriteMenu.do">자주사용하는메뉴</a></li>
				<%-- <li id="M5004"><a href="${defaultPath}/user/myFavoriteVehicle.do">즐겨찾기 차량</a></li> 메뉴 삭제--%>
				<li id="M5005"><a href="${defaultPath}/user/pay.do">이용요금 조회·결제</a></li>
			</ul>
		</div>
	</div>
	<div class="right_con">
			