<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<div class="sub_layout">
	<div class="left_menu">
		<div>
			<h1><img src="${pageContext.request.contextPath}/common/images/${_LANG}/lnb_h1_1.png" alt="운행관리" /></h1>
			<ul>
				<li class=""><a href="${defaultPath}/driving/searchVehicle.do"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/lnb_m1.png" alt="등록차량 조회" class="m_off" /><img src="${pageContext.request.contextPath}/common/images/${_LANG}/lnb_m1_on.png" alt="등록차량 조회" class="m_on" /></a></li>
				<li class=""><a href="${defaultPath}/driving/searchUser.do"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/lnb_m2.png" alt="사용자 조회" class="m_off" /><img src="${pageContext.request.contextPath}/common/images/${_LANG}/lnb_m2_on.png" alt="사용자 조회" class="m_on" /></a></li>
				<li class=""><a href="${defaultPath}/driving/searchLocation.do"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/lnb_m3.png" alt="차량위치 조회" class="m_off" /><img src="${pageContext.request.contextPath}/common/images/${_LANG}/lnb_m3_on.png" alt="차량위치 조회" class="m_on" /></a></li>
				<li class=""><a href="${defaultPath}/driving/searchDriving.do"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/lnb_m4.png" alt="운행기록 조회" class="m_off" /><img src="${pageContext.request.contextPath}/common/images/${_LANG}/lnb_m4_on.png" alt="운행기록 조회" class="m_on" /></a></li>
				<li class=""><a href="${defaultPath}/driving/searchAccident.do"><img src="${pageContext.request.contextPath}/common/images/${_LANG}/lnb_m5.png" alt="사고차량 조회" class="m_off" /><img src="${pageContext.request.contextPath}/common/images/${_LANG}/lnb_m5_on.png" alt="사고차량 조회" class="m_on" /></a></li>
			</ul>
		</div>
	</div>
	<div class="right_con">
			