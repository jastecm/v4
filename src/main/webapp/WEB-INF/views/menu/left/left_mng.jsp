<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script type="text/javascript">
function initMenuSel(m){
	$("#"+m).addClass("on");
	$(".left_menu li").mouseover(function(){
		$(this).addClass("on");
	});
	$(".left_menu li").mouseleave(function(){
		if($(this).text() != $("#"+m).text())
			$(this).removeClass("on");
	})
}
</script>
<div class="sub_layout">
	<div class="left_menu sidebar">
		<div>
			<h1>단말관리</h1>
			<ul>
				<li id="M9001"><a href="${defaultPath}/mng/corp.do">법인관리</a></li>
				<li id="M9001"><a href="${defaultPath}/mng/device.do">단말관리</a></li>
			</ul>
		</div>
	</div>
	<div class="right_con">
			