<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>    
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<fmt:setLocale value="${_LANG}" scope="request" />
<fmt:setBundle basename="properties.messages.rest.message" />
<script type="text/javascript">
function initMenuSel(m){
	$("#"+m).addClass("on");
	$(".left_menu li").mouseover(function(){
		$(this).addClass("on");
	});
	$(".left_menu li").mouseleave(function(){
		if($(this).text() != $("#"+m).text())
			$(this).removeClass("on");
	})
}
</script>
<div class="sub_layout">
	<div class="left_menu sidebar">
		<div>
			<h1><fmt:message key='leftClinic.menu'/></h1>
			<ul>
				<li id="M3001"><a href="${defaultPath}/clinic/vehicleMaintenance.do"><fmt:message key='leftClinic.leftMenu1'/></a></li>
				<li id="M3002"><a href="${defaultPath}/clinic/vehicleClinic.do"><fmt:message key='leftClinic.leftMenu2'/></a></li>
			</ul>
		</div>
	</div>
	<div class="right_con">
			