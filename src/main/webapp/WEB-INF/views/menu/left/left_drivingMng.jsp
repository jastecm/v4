<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fmt:setLocale value="${_LANG}" scope="request" />
<fmt:setBundle basename="properties.messages.rest.message" />
    
<%@include file="/WEB-INF/jsp/common/common.jsp"%>
<script type="text/javascript">
function initMenuSel(m){
	$("#"+m).addClass("on");
	$(".left_menu li").mouseover(function(){
		$(this).addClass("on");
	});
	$(".left_menu li").mouseleave(function(){
		if($(this).text() != $("#"+m).text())
			$(this).removeClass("on");
	})
}
</script>
<div class="sub_layout">
	<div class="left_menu sidebar">
		<div>
			<h1><fmt:message key='leftDrivingMng.drivingMng'/></h1>
			<ul>
				<li id="M1001"><a href="${defaultPath}/drivingMng/searchInfo.do"><fmt:message key='leftDrivingMng.searchInfo'/></a></li>
				<li id="M1002"><a href="${defaultPath}/drivingMng/searchDriving.do"><fmt:message key='leftDrivingMng.sarchDriving'/></a></li>
				<li id="M1003"><a href="${defaultPath}/drivingMng/searchLocation.do"><fmt:message key='leftDrivingMng.searchLocation'/></a></li>
				<li id="M1004"><a href="${defaultPath}/drivingMng/searchAcceptLocation.do"><fmt:message key='leftDrivingMng.searchAcceptLocation'/></a></li>
				<li id="M1005"><a href="${defaultPath}/drivingMng/alarmList.do"><fmt:message key='leftDrivingMng.alarmList'/></a></li>
				<%-- <li id="M1005"><a href="${defaultPath}/drivingMng/searchAccident.do">사고차량</a></li> --%>
			</ul>
		</div>
	</div>
	<div class="right_con">
			