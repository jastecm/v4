<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${_LANG}" scope="request" />
<fmt:setBundle basename="properties.messages.rest.message" />

<script type="text/javascript">
$(function(){
	
})
</script>
<header id="header">
	<div class="section01">
		<!-- 로고 -->
		<div class="logo">
			<a href="#" class="btn_main"> 
			<img src="${pageContext.request.contextPath}/common/new/img/common/top-logo.png" alt="VIEW CAR" />
			</a>
		</div>
		<!--/ 로고 -->
		<!-- 상단메뉴 -->
		<nav id="gnb">
			<h2 class="invisible">메인메뉴</h2>
			<ul>
				<li class="depth1 gnb01"><a href="#">운행정보</a>
					<ul>
						<li><a href="javascript:void(0);" onclick="$V4.move('/drivingInfo/vehicleUsers');">차량ㆍ사용자</a></li>
						<li><a href="javascript:void(0);" onclick="$V4.move('/drivingInfo/vehicleAllocate/list');">차량 배차</a></li>
						<li><a href="javascript:void(0);" onclick="$V4.move('/drivingInfo/availableDriving/list');">미배차 운행</a></li>
						<li><a href="javascript:void(0);" onclick="$V4.move('/drivingInfo/drivingLog');">운행 일지</a></li>
						<li><a href="javascript:void(0);" onclick="$V4.move('/drivingInfo/busRouteInfo/list');">버스 노선정보</a></li>
						<li><a href="javascript:void(0);" onclick="$V4.move('/drivingInfo/detailDrivingRecord');">상세 운행기록</a></li>
						<li><a href="javascript:void(0);" onclick="$V4.move('/drivingInfo/vehicleLocation');">차량 위치</a></li>
						<li><a href="javascript:void(0);" onclick="$V4.move('/drivingInfo/vehicleAlarm');">차량 주요알림</a></li>
						<li><a href="javascript:void(0);" onclick="$V4.move('/drivingInfo/vehicleAccident');">사고 차량조회</a></li>
					</ul></li>
				<li class="depth1 gnb02"><a href="#">차량관리</a>
					<ul>
						<li><a href="javascript:void(0);" onclick="$V4.move('/vehicleMng/drivingSchedule');">운행 스케쥴</a></li>
						<li><a href="javascript:void(0);" onclick="$V4.move('/vehicleMng/realTimeSelfDiagnosis');">실시간 자가진단</a></li>
						<li><a href="javascript:void(0);" onclick="$V4.move('/vehicleMng/troubleAndConsumables');">고장ㆍ소모품 현황</a></li>
						<li><a href="javascript:void(0);" onclick="$V4.move('/vehicleMng/penalty');">벌점ㆍ과태료</a></li>
						<li><a href="javascript:void(0);" onclick="$V4.move('/vehicleMng/vehicleMaintenance');">정비현황 조회ㆍ등록</a></li>
						<li><a href="javascript:void(0);" onclick="$V4.move('/vehicleMng/vehicleExpense');">차량경비</a></li>
						<li><a href="javascript:void(0);" onclick="$V4.move('/vehicleMng/subcontract');">도급비</a></li>
						<li><a href="javascript:void(0);" onclick="$V4.move('/vehicleMng/rentCarMng');">렌트카 관리</a></li>
					</ul></li>
				<li class="depth1 gnb03"><a href="#">보고서ㆍ결재</a>
					<ul>
						<li><a href="javascript:void(0);" onclick="$V4.move('/reportApproval/vehicleReport');">차량보고서 조회ㆍ제출</a></li>
						<li><a href="javascript:void(0);" onclick="$V4.move('/reportApproval/vehicleAccident');">차량 사고 보고</a></li>
						<li><a href="javascript:void(0);" onclick="$V4.move('/reportApproval/userApprovalMng');">결재 관리</a></li>
						<li><a href="javascript:void(0);" onclick="$V4.move('/reportApproval/report');">리포트</a></li>
						<li><a href="javascript:void(0);" onclick="$V4.move('/reportApproval/drivingStatistics');">운행 통계</a></li>
						<li><a href="javascript:void(0);" onclick="$V4.move('/reportApproval/taxServiceVehicleLog');">국세청 운행일지</a></li>
						<!-- <li><a href="javascript:void(0);">렌터카 관리</a></li> -->
					</ul></li>
				<li class="depth1 gnb04"><a href="#">ViewCAR API</a>
					<ul>
						<li><a href="javascript:void(0);" onclick="$V4.move('/viewCarApi/developerSupport');">개발자 지원</a></li>
						<li><a href="javascript:void(0);" onclick="$V4.move('/viewCarApi/apiDocument');">API 문서</a></li>
						<li><a href="javascript:void(0);">Web Document</a></li>
						<li><a href="javascript:void(0);" onclick="$V4.move('/viewCarApi/faq');">FAQ</a></li>
					</ul></li>
			</ul>
		</nav>
		<div class="bg-gnb"></div>
		<!--/ 상단메뉴 -->

		<!-- 언어 선택 -->
		<select name="" onchange="window.open(value,'_blank');" class="language">
			<option value="https://vdaspro.viewcar.co.kr/">한국어</option>
			<option value="https://vdaspro.viewcar.co.kr/">English</option>
		</select>
		<!--/ 언어 선택 -->
	</div>
</header>