<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${_LANG}" scope="request" />
<fmt:setBundle basename="properties.messages.rest.message" />
<script type="text/javascript">
$(function(){
	$("#btn_login").on("click",function(){
		$V4.move("/login");	
	});
	$("#btn_regist").on("click",function(){
		$V4.move("/regist/step0");
	});
	$(".btn_logOut").on("click",function(){
		$V4.http_post("/login/removeToken",null,{
			success : function(t){
				$V4.move("/main");
			}
		});
	});
	
});
</script>
<!-- 우측 사이드 바-->
<aside id="quick-menu">
	<h2 class="hidden">퀵메뉴</h2>
	<div class="inner">
	<!-- 로그인 전 -->
	<c:if test="${empty _KEY}">
		<ul class="login-before">
			<li class="login">
				<a href="javascript:void(0);" class="icon-login" id="btn_login">
					<span>로그인</span>
				</a>
			</li>
			<li class="join">
				<a href="javascript:void(0);" class="icon-join" id="btn_regist">
					<span>회원가입</span>
				</a>
			</li>
			<li class="manual">
				<a href="javascript:void(0);" class="icon-manual">
					<span>사용설명서</span>
				</a>
			</li>
			<li class="cs">
				<a href="javascript:void(0);" class="icon-cs">
					<span>고객센터</span>
				</a>
			</li>
			<li class="store">
				<a href="javascript:void(0);" class="icon-store">
					<span>스토어</span>
				</a>
			</li>
		</ul>
	</c:if>
	<!-- 로그인 전 -->

	<c:if test="${not empty _KEY}">
	<!-- 로그인 후 -->
                <!-- 사용자 정보 (공통) -->
                <div class="user-info">
                    <div class="top">
                        환영합니다. 홍길동님
                        <a href="javascript:void(0);" class="btn_logOut">로그아웃</a>
                    </div>
                    <div class="vehicle">
                        <h2>등록차량</h2>
                        <div class="name">
                            <img src="${pageContext.request.contextPath}/common/new/img/vehicle/S103.png" alt="" />
                            <span>티구안 49보 4022</span>
                        </div>
                        <span class="state">운행중</span>
                        <ul>
                            <li class="grade01">연비
                                <span>B</span>
                            </li>
                            <li class="grade02">안전점수
                                <span>A</span>
                            </li>
                            <li class="grade03">에코점수
                                <span>B</span>
                            </li>
                        </ul>
                        <a href="" class="btn-next">NEXT</a>
                    </div>
                </div>
                <!--/ 사용자 정보 (공통) -->
                <ul class="login-after">
                    <!-- 1차메뉴 사용자 -->
                    <li class="depth1 user">
                        <a href="javascript:;" class="icon-user">
                            <span>사용자</span>
                        </a>
                        <div class="cont">
                            <div class="vehicle-condition">
                                <div class="top">
                                    <a href="" class="icon-spr prev">이전</a>
                                    <span class="date">2018년 4월</span>
                                    <a href="" class="icon-spr next">다음</a>
                                </div>

                                <div class="detail">
                                    <dl>
                                        <dt>주행거리</dt>
                                        <dd>899.9
                                            <span>km</span>
                                        </dd>
                                    </dl>
                                    <dl>
                                        <dt>주행시간</dt>
                                        <dd>38
                                            <span>시간</span> 6
                                            <span>분</span>
                                        </dd>
                                    </dl>
                                    <dl>
                                        <dt>연료소모량</dt>
                                        <dd>7.7
                                            <span>L</span>
                                        </dd>
                                    </dl>
                                    <dl>
                                        <dt>유류비</dt>
                                        <dd>104.007
                                            <span>원</span>
                                        </dd>
                                    </dl>
                                </div>
                            </div>

                            <h2 class="tit border">QUICK MENU</h2>
                            <ul class="favorites-menu">
                                <li>
                                    <a href="javascript:void(0);" class="icon-user01">마이페이지</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-user02">환경설정</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-user03">고객센터</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-user04">사용설명서</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-user05">스토어</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-user06">가족관리</a>
                                </li>
                            </ul>

                            <h2 class="tit">즐겨찾기</h2>
                            <ul class="favorites-menu">
                                <li>
                                    <a href="javascript:void(0);" class="icon-driving05">버스노선정보</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-driving07">차량위치</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-vehicle08">렌터카</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!--/ 1차메뉴 사용자 -->

                    <!-- 1차메뉴 마이페이지 -->
                    <li class="depth1 mypage">
                        <a href="javascript:;" class="icon-mypage">
                            <span>마이페이지</span>
                        </a>
                        <div class="cont">
                            <h2 class="tit2">마이페이지</h2>
                            <ul class="sub-menu">
                                <li>
                                    <a href="javascript:void(0);" class="icon-mypage01" onclick="$V4.move('/mypage/userIdentify');">회원정보 설정</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-mypage02" onclick="$V4.move('/mypage/passwordChange');">비밀번호 변경</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-mypage03" onclick="$V4.move('/mypage/familyMng');">가족관리</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-mypage04" onclick="$V4.move('/mypage/serviceFee');">이용요금 조회ㆍ결제</a>
                                </li>
                            </ul>

                            <h2 class="tit border">즐겨찾기</h2>
                            <ul class="favorites-menu">
                                <li>
                                    <a href="javascript:void(0);" class="icon-driving05">버스노선정보</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-driving07">차량위치</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-vehicle08">렌터카</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!--/ 1차메뉴 마이페이지 -->

                    <!-- 1차메뉴 운행정보 -->
                    <li class="depth1 driving">
                        <a href="javascript:;" class="icon-driving">
                            <span>운행정보</span>
                        </a>
                        <div class="cont">
                            <h2 class="tit2">운행정보</h2>
                            <ul class="sub-menu">
                                <li>
                                    <a href="javascript:void(0);" class="icon-driving01" onclick="$V4.move('/drivingInfo/vehicleUsers');">차량ㆍ사용자</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-driving02" onclick="$V4.move('/drivingInfo/vehicleAllocate/list');">차량배차</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-driving03" onclick="$V4.move('/drivingInfo/availableDriving/list');">미배차 운행</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-driving04" onclick="$V4.move('/drivingInfo/drivingLog');">운행일지</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-driving05" onclick="$V4.move('/drivingInfo/busRouteInfo/list');">버스 노선정보</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-driving06" onclick="$V4.move('/drivingInfo/detailDrivingRecord');">상세운행기록</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-driving07" onclick="$V4.move('/drivingInfo/vehicleLocation');">차량위치</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-driving08" onclick="$V4.move('/drivingInfo/vehicleAlarm');">차량 주요알림</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-driving09" onclick="$V4.move('/drivingInfo/vehicleAccident');">사고차량 조회</a>
                                </li>
                            </ul>

                            <h2 class="tit border">즐겨찾기</h2>
                            <ul class="favorites-menu">
                                <li>
                                    <a href="javascript:void(0);" class="icon-driving05">버스노선정보</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-driving07">차량위치</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-vehicle08">렌터카</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!--/ 1차메뉴 운행정보 -->

                    <!-- 1차메뉴 차량관리 -->
                    <li class="depth1 vehicle">
                        <a href="javascript:;" class="icon-vehicle">
                            <span>차량관리</span>
                        </a>
                        <div class="cont">
                            <h2 class="tit2">차량관리</h2>
                            <ul class="sub-menu">
                                <li>
                                    <a href="javascript:void(0);" class="icon-vehicle01" onclick="$V4.move('/vehicleMng/drivingSchedule');">운행스케쥴</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-vehicle02" onclick="$V4.move('/vehicleMng/realTimeSelfDiagnosis');">실시간 자가진단</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-vehicle03" onclick="$V4.move('/vehicleMng/troubleAndConsumables');">
										고장ㆍ소모품
										<br />현황
									</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-vehicle04" onclick="$V4.move('/vehicleMng/penalty');">별점ㆍ과태료</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-vehicle05" onclick="$V4.move('/vehicleMng/vehicleMaintenance');">
										정비현황
										<br />조회ㆍ제출
									</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-vehicle06" onclick="$V4.move('/vehicleMng/vehicleExpense');">차량경비</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-vehicle07" onclick="$V4.move('/vehicleMng/subcontract');">도급비</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-vehicle08" onclick="$V4.move('/vehicleMng/rentCarMng');">렌트카 관리</a>
                                </li>
                            </ul>

                            <h2 class="tit border">즐겨찾기</h2>
                            <ul class="favorites-menu">
                                <li>
                                    <a href="javascript:void(0);" class="icon-driving05">버스노선정보</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-driving07">차량위치</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-vehicle08">렌터카</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!--/ 1차메뉴 차량관리 -->

                    <!-- 1차메뉴 보고서 결재 -->
                    <li class="depth1 report">
                        <a href="javascript:;" class="icon-report">
                            <span>보고서ㆍ결재</span>
                        </a>
                        <div class="cont">
                            <h2 class="tit2">보고서ㆍ결재</h2>
                            <ul class="sub-menu">
                                <li>
                                    <a href="javascript:void(0);" class="icon-report01" onclick="$V4.move('/reportApproval/vehicleReport');">
										차량보고서
										<br />조회ㆍ제출
									</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-report02" onclick="$V4.move('/reportApproval/vehicleAccident');">차량사고 보고</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-report03" onclick="$V4.move('/reportApproval/userApprovalMng');">결재 관리</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-report04" onclick="$V4.move('/reportApproval/report');">리포트</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-report05" onclick="$V4.move('/reportApproval/drivingStatistics');">운행통계</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-report06" onclick="$V4.move('/reportApproval/taxServiceVehicleLog');">국세청 운행일지</a>
                                </li>
                            </ul>

                            <h2 class="tit border">즐겨찾기</h2>
                            <ul class="favorites-menu">
                                <li>
                                    <a href="javascript:void(0);" class="icon-driving05">버스노선정보</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-driving07">차량위치</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-vehicle08">렌터카</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!--/ 1차메뉴 보고서 결재 -->

                    <!-- 1차메뉴 API -->
                    <li class="depth1 api">
                        <a href="javascript:;" class="icon-api">
                            <span>Open API</span>
                        </a>
                        <div class="cont">
                            <h2 class="tit2">Open API</h2>
                            <ul class="sub-menu">
                                <li>
                                    <a href="javascript:void(0);" class="icon-api01" onclick="$V4.move('/viewCarApi/developerSupport');">개발자 지원</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-api02" onclick="$V4.move('/viewCarApi/apiDocument');">API 문서</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-api03">Web Document</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-api04">FAQ</a>
                                </li>
                            </ul>

                            <h2 class="tit border">즐겨찾기</h2>
                            <ul class="favorites-menu">
                                <li>
                                    <a href="javascript:void(0);" class="icon-driving05">버스노선정보</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-driving07">차량위치</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-vehicle08">렌터카</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!--/ 1차메뉴 API -->

                    <!-- 1차메뉴 환경설정 -->
                    <li class="depth1 setting">
                        <a href="javascript:;" class="icon-setting">
                            <span>환경설정</span>
                        </a>
                        <div class="cont">
                            <h2 class="tit2">환경설정</h2>
                            <ul class="sub-menu">
                                <li>
                                    <a href="javascript:void(0);" class="icon-setting01" onclick="$V4.move('/config/serviceStore');">서비스 스토어</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-setting02" onclick="$V4.move('/config/vehicleRegister/list');">차량등록</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-setting03" onclick="$V4.move('/config/groupRegister');">부서(차량) 등록</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-setting04" onclick="$V4.move('/config/userRegister');">사용자 등록</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-setting05" onclick="$V4.move('/config/approvalMng');">결재 관리</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-setting06" onclick="$V4.move('/config/unitSetting');">단위 선택</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-setting07" onclick="$V4.move('/config/customerRegister/list');">거래처 등록</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-setting08">이수지역 설정</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-setting09" onclick="$V4.move('/config/workingArea');">업무시간 설정</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-setting10" onclick="$V4.move('/config/myServiceStore');">MY서비스</br> 스토어</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-driving05" onclick="$V4.move('/config/busManager/list');">버스 노선관리</a>
                                </li>
                            </ul>

                            <h2 class="tit border">즐겨찾기</h2>
                            <ul class="favorites-menu">
                                <li>
                                    <a href="javascript:void(0);" class="icon-driving05">버스노선정보</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-driving07">차량위치</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-vehicle08">렌터카</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!--/ 1차메뉴 환경설정 -->

                    <!-- 1차메뉴 사용설명서 -->
                    <li class="manual">
                        <a href="#" class="icon-manual">
                            <span>사용설명서</span>
                        </a>
                    </li>
                    <!--/ 1차메뉴 사용설명서 -->

                    <!-- 1차메뉴 고객센터 -->
                    <li class="depth1 cs">
                        <a href="javascript:;" class="icon-cs">
                            <span>고객센터</span>
                        </a>
                        <div class="cont">
                            <h2 class="tit2">고객센터</h2>
                            <ul class="sub-menu">
                                <li>
                                    <a href="javascript:void(0);" class="icon-cs01" onclick="$V4.move('/customerCenter/notice');">공지사항</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-cs02" onclick="$V4.move('/customerCenter/faq');">FAQ</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-cs03" onclick="$V4.move('/customerCenter/event');">이벤트, 할인정보</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-cs04" onclick="$V4.move('/customerCenter/reportInconvenience');">서비스 불편신고</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-cs05" onclick="$V4.move('/customerCenter/productAs');">제품 A/S 접수</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-cs06" onclick="$V4.move('/customerCenter/deviceUpdate');">단말기 업데이트</a>
                                </li>
                            </ul>

                            <h2 class="tit border">즐겨찾기</h2>
                            <ul class="favorites-menu">
                                <li>
                                    <a href="javascript:void(0);" class="icon-driving05">버스노선정보</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-driving07">차량위치</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-vehicle08">렌터카</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!--/ 1차메뉴 고객센터 -->

                    <!-- 1차메뉴 스토어 -->
                    <li class="store">
                        <a href="javascript:void(0);" class="icon-store" onclick="$V4.move('/config/serviceStore');">
                            <span>스토어</span>
                        </a>
                        <div class="cont">
                            <h2 class="tit2">스토어</h2>
                            <ul class="sub-menu">
                                <li>
                                    <a href="javascript:void(0);" class="icon-store01"></a>
                                </li>
                            </ul>

                            <h2 class="tit border">즐겨찾기</h2>
                            <ul class="favorites-menu">
                                <li>
                                    <a href="javascript:void(0);" class="icon-driving05">버스노선정보</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-driving07">차량위치</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-vehicle08">렌터카</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!--/ 1차메뉴 스토어 -->
                </ul>
                <!--/ 로그인 후 -->

	</c:if>
	

	<!-- 상단으로 가기-->
	<div id="page-up">
		<a href="javascript:;" class="icon-spr">TOP</a>
	</div>
	<!--/ 상단으로 가기-->

	<a href="javascript:;" class="btn-closed icon-spr">사이드 메뉴 닫기</a>
</aside>
<!-- 모달 bg -->
<div class="bg-modal"></div>
<!--/ 모달 bg -->
<!--/ 우측 사이드 바-->