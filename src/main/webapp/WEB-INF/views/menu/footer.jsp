<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${_LANG}" scope="request" />
<fmt:setBundle basename="properties.messages.rest.message" />

<!-- 하단 -->
        <footer id="footer">
            <div class="section01">
                <div class="logo">
                    <img src="${pageContext.request.contextPath}/common/new/img/common/footer-logo.gif" alt="" />
                </div>
                <ul>
                    <li>
                        <a href="">회사소개</a>
                    </li>
                    <li>
                        <a href="">이용약관</a>
                    </li>
                    <li>
                        <a href="">개인정보처리방침</a>
                    </li>
                    <li>
                        <a href="">위치정보서비스</a>
                    </li>
                    <li>
                        <a href="">고객센터</a>
                    </li>
                    <li>
                        <a href="">매뉴얼</a>
                    </li>
                    <li>
                        <a href="">Contact us</a>
                    </li>
                </ul>
                <address>
					<span>(주) 자스텍엠</span>
					<span>대표 : 백용범</span>
					<span>사업자번호 : 389-88-00371</span>
					<span>소재지 : 기도 성남시 분당구 판교로 242, C동 402-1호(판교 디지털센터)</span>
					<br />
					<span>고객센터 : 1599-8439</span>
					<span>Fax : 031-716-0379</span>
					<span>Week : 10:00~19:00 (토요일 및 공휴일 휴무)</span>
					<div class="copyright">Copyright 2018&copy; JastecM All Right Reserved.</div>
				</address>
            </div>
        </footer>
        <!--/ 하단 -->