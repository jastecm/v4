package v4.ex.exc;


public class ResultDataNotFoundException  extends Exception{

    public ResultDataNotFoundException () {
        super();
    }

    public ResultDataNotFoundException (String s) {
        super ("result data is none");
    }

}
