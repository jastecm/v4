package v4.ex.exc.extents;

import v4.ex.exc.DefaultInitCheckException;


public class BadParameterException extends DefaultInitCheckException{

    public BadParameterException () {
        super();
    }

    public BadParameterException (String s) {
        super (s);
    }

}
