package v4.ex.exc.extents;

import v4.ex.exc.NotFoundException;


public class UserNotFoundException  extends NotFoundException{

    public UserNotFoundException () {
        super("can't found user");
    }

    public UserNotFoundException (String s) {
        super (s);
    }

}
