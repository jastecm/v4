package v4.ex.exc.extents;

import v4.ex.exc.NotFoundException;


public class VehicleNotFoundException  extends NotFoundException{

    public VehicleNotFoundException () {
        super("can't found vehicle");
    }

    public VehicleNotFoundException (String s) {
        super (s);
    }

}
