package v4.ex.exc.extents;

import v4.ex.exc.CreateResponseException;


public class CreateResponseBodyException extends CreateResponseException{

    public CreateResponseBodyException () {
        super();
    }

    public CreateResponseBodyException (String s) {
        super (s);
    }

}
