package v4.ex.exc.extents;

import v4.ex.exc.DefaultInitCheckException;


public class UnauthorizedException  extends DefaultInitCheckException{

    public UnauthorizedException () {
        super("Unauthorized");
    }

    public UnauthorizedException (String s) {
        super (s);
    }

}
