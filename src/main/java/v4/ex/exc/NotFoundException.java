package v4.ex.exc;

public class NotFoundException extends Exception {

	public NotFoundException(){
		super();
	};
	
	public NotFoundException(String s){
		super(s);
	};
}
