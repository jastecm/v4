package v4.ex.exc;

import java.io.IOException;

//500 Internal
public class RestConnectException extends IOException{

	public RestConnectException(){
		super();
	};
	
	public RestConnectException(String s){
		super(s);
	};
}
