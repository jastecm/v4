package v4.ex.exc;

public class CreateResponseException extends Exception {

	public CreateResponseException(){
		super();
	};
	
	public CreateResponseException(String s){
		super(s);
	};
}
