package v4.ex.exc;

public class DefaultInitCheckException extends Exception{

	public DefaultInitCheckException(){
		super("check parameter fail");
	};
	
	public DefaultInitCheckException(String s){
		super(s);
	};
}
