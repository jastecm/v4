package v4.ex.exc;

//403 Forbidden
public class ResouceDeninedException extends Exception{

	public ResouceDeninedException(){
		super("used system reserved field");
	};
	
	public ResouceDeninedException(String s){
		super("can't used " + s + " - System reserved field");
	};
	
	public ResouceDeninedException(String s,String t){
		super(t + "is can not update");
	};
}
