package v4.ex.exc;

//403 Forbidden
public class ResouceRequiredException extends Exception{

	public ResouceRequiredException(){
		super("required field is not found");
	};
	
	public ResouceRequiredException(String s){
		super(s+" is required");
	};
}
