package v4.ex.exc;

//500 Internal
public class BizException extends Exception{

	public BizException(){
		super();
	};
	
	public BizException(String s){
		super(s);
	};
	
	public BizException(String s,Throwable e){
		super(s,e);
	};
}
