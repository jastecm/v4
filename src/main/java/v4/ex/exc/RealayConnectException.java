package v4.ex.exc;

import java.io.IOException;

//500 Internal
public class RealayConnectException extends IOException{

	public RealayConnectException(){
		super();
	};
	
	public RealayConnectException(String s){
		super(s);
	};
}
