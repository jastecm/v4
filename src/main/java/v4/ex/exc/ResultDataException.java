package v4.ex.exc;

//403 Forbidden
public class ResultDataException extends Exception{

	public ResultDataException(){
		super();
	};
	
	public ResultDataException(String s){
		super(s);
	};
}
