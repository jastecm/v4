package v4.ex.resolver;
  
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import net.sf.json.JSONObject;
import v4.ex.exc.BizException;
import v4.ex.exc.CreateResponseException;
import v4.ex.exc.NotFoundException;
import v4.ex.exc.RestConnectException;
import v4.ex.exc.extents.UnauthorizedException;



@ControllerAdvice(basePackages={"v4.web.rest.controller"})
public class DefaultRestHandlerExceptionResolver {
  
	
    @ExceptionHandler(Exception.class)
    public ResponseEntity exceptionHandler(Exception ex) {
    	ex.printStackTrace();
    	
    	JSONObject jObj = new JSONObject();
    	jObj.put("result", ex.getMessage());
    	
	    HttpHeaders responseHeaders = new HttpHeaders();
	    responseHeaders.set("Content-Type", MediaType.APPLICATION_JSON + ";charset=utf-8");
        return new ResponseEntity(jObj.toString(),responseHeaders,HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    @ExceptionHandler(CreateResponseException.class)
    public ResponseEntity createResponseExceptionHandler(CreateResponseException ex) {
    	ex.printStackTrace();
    	
    	JSONObject jObj = new JSONObject();
    	jObj.put("result", ex.getMessage());
    	
	    HttpHeaders responseHeaders = new HttpHeaders();
	    responseHeaders.set("Content-Type", MediaType.APPLICATION_JSON + ";charset=utf-8");
        return new ResponseEntity(jObj.toString(),responseHeaders,HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    
    @ExceptionHandler(UnauthorizedException.class)
    public ResponseEntity exceptionHandler(UnauthorizedException ex) {
    	
    	JSONObject jObj = new JSONObject();
    	jObj.put("result", ex.getMessage());
    	
    	HttpHeaders responseHeaders = new HttpHeaders();
    	responseHeaders.set("Content-Type", MediaType.APPLICATION_JSON + ";charset=utf-8");
    	return new ResponseEntity(jObj.toString(),responseHeaders,HttpStatus.UNAUTHORIZED);
    }
    
    @ExceptionHandler(BizException.class)
    public ResponseEntity exceptionHandler(BizException ex) {
    	if(!(ex.getCause() instanceof DataAccessException))
    		ex.printStackTrace();
    	
    	JSONObject jObj = new JSONObject();
    	jObj.put("result", ex.getMessage());
    	
    	HttpHeaders responseHeaders = new HttpHeaders();
    	responseHeaders.set("Content-Type", MediaType.APPLICATION_JSON + ";charset=utf-8");
    	return new ResponseEntity(jObj.toString(),responseHeaders,HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    @ExceptionHandler(RestConnectException.class)
    public ResponseEntity exceptionHandler(RestConnectException ex) {
    	
    	JSONObject jObj = new JSONObject();
    	jObj.put("result", ex.getMessage());
    	
    	HttpHeaders responseHeaders = new HttpHeaders();
    	responseHeaders.set("Content-Type", MediaType.APPLICATION_JSON + ";charset=utf-8");
    	return new ResponseEntity(jObj.toString(),responseHeaders,HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity exceptionHandler(NotFoundException ex) {
    	
    	JSONObject jObj = new JSONObject();
    	jObj.put("result", ex.getMessage());
    	
    	HttpHeaders responseHeaders = new HttpHeaders();
    	responseHeaders.set("Content-Type", MediaType.APPLICATION_JSON + ";charset=utf-8");
    	return new ResponseEntity(jObj.toString(),responseHeaders,HttpStatus.NOT_FOUND);
    }
}
