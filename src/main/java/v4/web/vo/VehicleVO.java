package v4.web.vo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;

public class VehicleVO {
		
	private String plateNum;
	private String deviceSn;
	private String convCode;
	private double totDistance;
	private String vehicleType;
	private String corpGroupKey;
	private String fixed;
	private String fixedAccountKey;
	private String fixedDriverKey;
	private String fixedContents;
	private String autoReport;
	private String autoReportPurpose;
	private String autoReportContents;
	private String locationSetting;
	private String color;
	private String vehicleBizType;
	private String vehicleRegDate;
	private String holidayPass;
	private String approvalType;
	private String approvalGroupKey;
	private String approvalAccountKey;
	private String vehicleBodyNum;
	private String vehicleMaintenanceDate;
	private int vehiclePayment;
	private String vehicleInsureNm;
	private String vehicleInsurePhone;
	private String vehicleDescript;
	private String connectionKey;
	private String vehicleKey;
	private String corpKey;
	private String defaultAccountKey;
	private String deviceId;
	private String lastestLocation;
	private String lastestLocationTime;
	private String lastestLocationAddr;
	private String lastestLocationAddrEn;
	private String lastestLocationAddrDetail;
	private String lastestTripKey;
	private String lastestStartTripKey;
	private int accureDistance;
	private String regDate;
	private String geofenceMonitor;
	private int totDistanceCollectionValue;
	private String drivingState;
	private String latestDriverKey;
	private String latestDriverName;
	
	private String modelMaster;
	
	private String groupNm;
	
	private String vehicleManager; //차량 관리자
	
	private String corpType;
	
	private String fuel;
	private String compoundFuelEfficiency;
	private String dtcManufacture;
	private String deviceKey;
	
	private String lang;

	private String corpGroupMasterAccount;
	
	
	private String vehicleMasterFlag = "1";
	private String deviceSound;
	private String autoPurpose;
	
	private String imgS,imgL;
	
	private String vehicleManagerName;
	private String vehicleManagerGroupName;
	private String corpPosition;
	
	
	
	public String getCorpPosition() {
		return corpPosition;
	}
	public void setCorpPosition(String corpPosition) {
		this.corpPosition = corpPosition;
	}
	public String getVehicleManagerName() {
		return vehicleManagerName;
	}
	public void setVehicleManagerName(String vehicleManagerName) {
		this.vehicleManagerName = vehicleManagerName;
	}
	public String getVehicleManagerGroupName() {
		return vehicleManagerGroupName;
	}
	public void setVehicleManagerGroupName(String vehicleManagerGroupName) {
		this.vehicleManagerGroupName = vehicleManagerGroupName;
	}
	public String getImgS() {
		return imgS;
	}
	public void setImgS(String imgS) {
		this.imgS = imgS;
	}
	public String getImgL() {
		return imgL;
	}
	public void setImgL(String imgL) {
		this.imgL = imgL;
	}
	public String getLatestDriverKey() {
		return latestDriverKey;
	}
	public void setLatestDriverKey(String latestDriverKey) {
		this.latestDriverKey = latestDriverKey;
	}
	public String getLatestDriverName() {
		return latestDriverName;
	}
	public void setLatestDriverName(String latestDriverName) {
		this.latestDriverName = latestDriverName;
	}
	public String getDrivingState() {
		return drivingState;
	}
	public void setDrivingState(String drivingState) {
		this.drivingState = drivingState;
	}
	public String getAutoPurpose() {
		return autoPurpose;
	}
	public void setAutoPurpose(String autoPurpose) {
		this.autoPurpose = autoPurpose;
	}
	public String getModelMaster() {
		return modelMaster;
	}
	public void setModelMaster(String modelMaster) {
		this.modelMaster = modelMaster;
	}
	public String getDeviceSound() {
		return deviceSound;
	}
	public void setDeviceSound(String deviceSound) {
		this.deviceSound = deviceSound;
	}
	
	public String getCorpGroupMasterAccount() {
		return corpGroupMasterAccount;
	}
	public void setCorpGroupMasterAccount(String corpGroupMasterAccount) {
		this.corpGroupMasterAccount = corpGroupMasterAccount;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public String getVehicleMasterFlag() {
		return vehicleMasterFlag;
	}
	public void setVehicleMasterFlag(String vehicleMasterFlag) {
		this.vehicleMasterFlag = vehicleMasterFlag;
	}
	public String getDeviceKey() {
		return deviceKey;
	}
	public void setDeviceKey(String deviceKey) {
		this.deviceKey = deviceKey;
	}
	public String getDtcManufacture() {
		return dtcManufacture;
	}
	public void setDtcManufacture(String dtcManufacture) {
		this.dtcManufacture = dtcManufacture;
	}
	public String getCompoundFuelEfficiency() {
		return compoundFuelEfficiency;
	}
	public void setCompoundFuelEfficiency(String compoundFuelEfficiency) {
		this.compoundFuelEfficiency = compoundFuelEfficiency;
	}
	public String getFuel() {
		return fuel;
	}
	public void setFuel(String fuel) {
		this.fuel = fuel;
	}
	public String getCorpType() {
		return corpType;
	}
	public void setCorpType(String corpType) {
		this.corpType = corpType;
	}
	public String getVehicleManager() {
		return vehicleManager;
	}
	public void setVehicleManager(String vehicleManager) {
		this.vehicleManager = vehicleManager;
	}
	public String getGroupNm() {
		return groupNm;
	}
	public void setGroupNm(String groupNm) {
		this.groupNm = groupNm;
	}
	public String getPlateNum() {
		return plateNum;
	}
	public void setPlateNum(String plateNum) {
		this.plateNum = plateNum;
	}
	public String getDeviceSn() {
		return deviceSn;
	}
	public void setDeviceSn(String deviceSn) {
		this.deviceSn = deviceSn;
	}
	public String getConvCode() {
		return convCode;
	}
	public void setConvCode(String convCode) {
		this.convCode = convCode;
	}
	public double getTotDistance() {
		return totDistance;
	}
	public void setTotDistance(double totDistance) {
		this.totDistance = totDistance;
	}
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getCorpGroupKey() {
		return corpGroupKey;
	}
	public void setCorpGroupKey(String corpGroupKey) {
		this.corpGroupKey = corpGroupKey;
	}
	public String getFixed() {
		return fixed;
	}
	public void setFixed(String fixed) {
		this.fixed = fixed;
	}
	public String getFixedAccountKey() {
		return fixedAccountKey;
	}
	public void setFixedAccountKey(String fixedAccountKey) {
		this.fixedAccountKey = fixedAccountKey;
	}
	public String getFixedDriverKey() {
		return fixedDriverKey;
	}
	public void setFixedDriverKey(String fixedDriverKey) {
		this.fixedDriverKey = fixedDriverKey;
	}
	public String getFixedContents() {
		return fixedContents;
	}
	public void setFixedContents(String fixedContents) {
		this.fixedContents = fixedContents;
	}
	public String getAutoReport() {
		return autoReport;
	}
	public void setAutoReport(String autoReport) {
		this.autoReport = autoReport;
	}
	public String getAutoReportPurpose() {
		return autoReportPurpose;
	}
	public void setAutoReportPurpose(String autoReportPurpose) {
		this.autoReportPurpose = autoReportPurpose;
	}
	public String getAutoReportContents() {
		return autoReportContents;
	}
	public void setAutoReportContents(String autoReportContents) {
		this.autoReportContents = autoReportContents;
	}
	public String getLocationSetting() {
		return locationSetting;
	}
	public void setLocationSetting(String locationSetting) {
		this.locationSetting = locationSetting;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getVehicleBizType() {
		return vehicleBizType;
	}
	public void setVehicleBizType(String vehicleBizType) {
		this.vehicleBizType = vehicleBizType;
	}
	public String getVehicleRegDate() {
		return vehicleRegDate;
	}
	public void setVehicleRegDate(String vehicleRegDate) {
		this.vehicleRegDate = vehicleRegDate;
	}
	public String getHolidayPass() {
		return holidayPass;
	}
	public void setHolidayPass(String holidayPass) {
		this.holidayPass = holidayPass;
	}
	public String getApprovalType() {
		return approvalType;
	}
	public void setApprovalType(String approvalType) {
		this.approvalType = approvalType;
	}
	public String getApprovalGroupKey() {
		return approvalGroupKey;
	}
	public void setApprovalGroupKey(String approvalGroupKey) {
		this.approvalGroupKey = approvalGroupKey;
	}
	public String getApprovalAccountKey() {
		return approvalAccountKey;
	}
	public void setApprovalAccountKey(String approvalAccountKey) {
		this.approvalAccountKey = approvalAccountKey;
	}
	public String getVehicleBodyNum() {
		return vehicleBodyNum;
	}
	public void setVehicleBodyNum(String vehicleBodyNum) {
		this.vehicleBodyNum = vehicleBodyNum;
	}
	public String getVehicleMaintenanceDate() {
		return vehicleMaintenanceDate;
	}
	public void setVehicleMaintenanceDate(String vehicleMaintenanceDate) {
		this.vehicleMaintenanceDate = vehicleMaintenanceDate;
	}
	public int getVehiclePayment() {
		return vehiclePayment;
	}
	public void setVehiclePayment(int vehiclePayment) {
		this.vehiclePayment = vehiclePayment;
	}
	public String getVehicleInsureNm() {
		return vehicleInsureNm;
	}
	public void setVehicleInsureNm(String vehicleInsureNm) {
		this.vehicleInsureNm = vehicleInsureNm;
	}
	public String getVehicleInsurePhone() {
		return vehicleInsurePhone;
	}
	public void setVehicleInsurePhone(String vehicleInsurePhone) {
		this.vehicleInsurePhone = vehicleInsurePhone;
	}
	public String getVehicleDescript() {
		return vehicleDescript;
	}
	public void setVehicleDescript(String vehicleDescript) {
		this.vehicleDescript = vehicleDescript;
	}
	public String getConnectionKey() {
		return connectionKey;
	}
	public void setConnectionKey(String connectionKey) {
		this.connectionKey = connectionKey;
	}
	public String getVehicleKey() {
		return vehicleKey;
	}
	public void setVehicleKey(String vehicleKey) {
		this.vehicleKey = vehicleKey;
	}
	public String getCorpKey() {
		return corpKey;
	}
	public void setCorpKey(String corpKey) {
		this.corpKey = corpKey;
	}
	public String getDefaultAccountKey() {
		return defaultAccountKey;
	}
	public void setDefaultAccountKey(String defaultAccountKey) {
		this.defaultAccountKey = defaultAccountKey;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getLastestLocation() {
		return lastestLocation;
	}
	public void setLastestLocation(String lastestLocation) {
		this.lastestLocation = lastestLocation;
	}
	public String getLastestLocationTime() {
		return lastestLocationTime;
	}
	public void setLastestLocationTime(String lastestLocationTime) {
		this.lastestLocationTime = lastestLocationTime;
	}
	public String getLastestLocationAddr() {
		return lastestLocationAddr;
	}
	public void setLastestLocationAddr(String lastestLocationAddr) {
		this.lastestLocationAddr = lastestLocationAddr;
	}
	public String getLastestLocationAddrEn() {
		return lastestLocationAddrEn;
	}
	public void setLastestLocationAddrEn(String lastestLocationAddrEn) {
		this.lastestLocationAddrEn = lastestLocationAddrEn;
	}
	public String getLastestLocationAddrDetail() {
		return lastestLocationAddrDetail;
	}
	public void setLastestLocationAddrDetail(String lastestLocationAddrDetail) {
		this.lastestLocationAddrDetail = lastestLocationAddrDetail;
	}
	public String getLastestTripKey() {
		return lastestTripKey;
	}
	public void setLastestTripKey(String lastestTripKey) {
		this.lastestTripKey = lastestTripKey;
	}
	public String getLastestStartTripKey() {
		return lastestStartTripKey;
	}
	public void setLastestStartTripKey(String lastestStartTripKey) {
		this.lastestStartTripKey = lastestStartTripKey;
	}
	public int getAccureDistance() {
		return accureDistance;
	}
	public void setAccureDistance(int accureDistance) {
		this.accureDistance = accureDistance;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getGeofenceMonitor() {
		return geofenceMonitor;
	}
	public void setGeofenceMonitor(String geofenceMonitor) {
		this.geofenceMonitor = geofenceMonitor;
	}
	public int getTotDistanceCollectionValue() {
		return totDistanceCollectionValue;
	}
	public void setTotDistanceCollectionValue(int totDistanceCollectionValue) {
		this.totDistanceCollectionValue = totDistanceCollectionValue;
	}
	public VehicleVO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
}
