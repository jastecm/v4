package v4.web.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ItemVO{
	
	private int val;
	private String max;
	private String name;
	private String itemKey;
	private String partsKey;
	private String sort;
	
	@JsonIgnore
	private String vehicleKey;
	
	
	public String getPartsKey() {
		return partsKey;
	}
	public void setPartsKey(String partsKey) {
		this.partsKey = partsKey;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public String getItemKey() {
		return itemKey;
	}
	public void setItemKey(String itemKey) {
		this.itemKey = itemKey;
	}
	public String getVehicleKey() {
		return vehicleKey;
	}
	public void setVehicleKey(String vehicleKey) {
		this.vehicleKey = vehicleKey;
	}
	public int getVal() {
		return val;
	}
	public void setVal(int val) {
		this.val = val;
	}
	public String getMax() {
		return max;
	}
	public void setMax(String max) {
		this.max = max;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
		
		
}
