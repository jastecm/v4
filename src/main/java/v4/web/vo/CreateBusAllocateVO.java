package v4.web.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value="CreateBusAllocateVO" , description="template CreateBusRouteStationVO value object")
@JsonIgnoreProperties(ignoreUnknown = true)
public @Data class CreateBusAllocateVO {

	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String corp;
	
	@ApiModelProperty(value = "busRouteKey",required = true)
	private String busRouteKey;
	
	@ApiModelProperty(value = "allocateDate",required = true)
	private String allocateDate;
	
	@ApiModelProperty(value = "vehicleKey",required = true)
	private String vehicleKey;
	
	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String workingStartDate;
	
	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String workingEndDate;
	
}


