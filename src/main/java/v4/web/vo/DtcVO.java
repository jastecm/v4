package v4.web.vo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DtcVO  {
	
	private int tripId;
	private long curTime;
	private int dtcKind;
	private int dtcType;
	
	private List<String> dtcCode = new ArrayList<String>();
	private List<Map<String,String>> dtcDesc = new ArrayList<Map<String,String>>();
	
	
	public List<Map<String, String>> getDtcDesc() {
		return dtcDesc;
	}

	public void setDtcDesc(List<Map<String, String>> dtcDesc) {
		this.dtcDesc = dtcDesc;
	}

	public int getDtcKind() {
		return dtcKind;
	}

	public void setDtcKind(int dtcKind) {
		this.dtcKind = dtcKind;
	}

	public int getDtcType() {
		return dtcType;
	}

	public void setDtcType(int dtcType) {
		this.dtcType = dtcType;
	}

	public int getTripId() {
		return tripId;
	}

	public void setTripId(int tripId) {
		this.tripId = tripId;
	}

	public long getCurTime() {
		return curTime;
	}

	public void setCurTime(long curTime) {
		this.curTime = curTime;
	}

	public List<String> getDtcCode() {
		return dtcCode;
	}

	public void setDtcCode(List<String> dtcCode) {
		this.dtcCode = dtcCode;
	}
	
		
}
