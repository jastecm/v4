package v4.web.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.fasterxml.jackson.annotation.JsonIgnore;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AttachVO  {
	
	private long dettachTime;
	private int dettachType;
	private long attachTime;
	private int attachType;
	

	public long getDettachTime() {
		return dettachTime;
	}
	public void setDettachTime(long dettachTime) {
		this.dettachTime = dettachTime;
	}
	public int getDettachType() {
		return dettachType;
	}
	public void setDettachType(int dettachType) {
		this.dettachType = dettachType;
	}
	public long getAttachTime() {
		return attachTime;
	}
	public void setAttachTime(long attachTime) {
		this.attachTime = attachTime;
	}
	public int getAttachType() {
		return attachType;
	}
	public void setAttachType(int attachType) {
		this.attachType = attachType;
	}
	

		
}
