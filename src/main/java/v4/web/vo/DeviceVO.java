package v4.web.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DeviceVO {
	
	private int seq;
	private String corpKey;
	private String deviceSn, deviceId, appEui, deviceAppKey, deviceSeries, adminCorp, latestPacket, descript, expireDate, numberOfTimes, modelCode, manufacturingDate, networkOpeningDate, plan, monthlyCharge, classType, periodOfContract, nameOfBank, accountHolder, accountNumber, contractor, telecommunicationsOperator, adminDeviceState, adminDeviceStateModDate, deviceWorkingState, deviceWorkingStateDate, beaconMaj, beaconMin, latestPaymentType, shiftDay, delivery, deliveryNum;
	private String vehicleKey,convCode;	
	private String deviceKey;
	private String defaultAccountKey;
	
	private String connectionKey;
    private String firmNm;
    private String dbNm;
    
    @JsonIgnore
    private String adminCorpNm,adminCorpDeviceSound,android,genelicVer,genelicBusVer,s31,f31;
    
    @JsonIgnore
    private String corpType;
    
    
	public String getConvCode() {
		return convCode;
	}
	public void setConvCode(String convCode) {
		this.convCode = convCode;
	}
	public String getCorpType() {
		return corpType;
	}
	public void setCorpType(String corpType) {
		this.corpType = corpType;
	}
	public String getAdminCorpNm() {
		return adminCorpNm;
	}
	public void setAdminCorpNm(String adminCorpNm) {
		this.adminCorpNm = adminCorpNm;
	}
	public String getAdminCorpDeviceSound() {
		return adminCorpDeviceSound;
	}
	public void setAdminCorpDeviceSound(String adminCorpDeviceSound) {
		this.adminCorpDeviceSound = adminCorpDeviceSound;
	}
	public String getAndroid() {
		return android;
	}
	public void setAndroid(String android) {
		this.android = android;
	}
	public String getGenelicVer() {
		return genelicVer;
	}
	public void setGenelicVer(String genelicVer) {
		this.genelicVer = genelicVer;
	}
	public String getGenelicBusVer() {
		return genelicBusVer;
	}
	public void setGenelicBusVer(String genelicBusVer) {
		this.genelicBusVer = genelicBusVer;
	}
	public String getS31() {
		return s31;
	}
	public void setS31(String s31) {
		this.s31 = s31;
	}
	public String getF31() {
		return f31;
	}
	public void setF31(String f31) {
		this.f31 = f31;
	}
	public String getDbNm() {
		return dbNm;
	}
	public void setDbNm(String dbNm) {
		this.dbNm = dbNm;
	}
	public String getDefaultAccountKey() {
		return defaultAccountKey;
	}
	public String getConnectionKey() {
		return connectionKey;
	}
	public void setConnectionKey(String connectionKey) {
		this.connectionKey = connectionKey;
	}
	public String getFirmNm() {
		return firmNm;
	}
	public void setFirmNm(String firmNm) {
		this.firmNm = firmNm;
	}
	public void setDefaultAccountKey(String defaultAccountKey) {
		this.defaultAccountKey = defaultAccountKey;
	}
	public String getDeviceKey() {
		return deviceKey;
	}
	public void setDeviceKey(String deviceKey) {
		this.deviceKey = deviceKey;
	}
	public String getCorpKey() {
		return corpKey;
	}
	public void setCorpKey(String corpKey) {
		this.corpKey = corpKey;
	}
	public String getVehicleKey() {
		return vehicleKey;
	}
	public void setVehicleKey(String vehicleKey) {
		this.vehicleKey = vehicleKey;
	}
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	public String getDeviceSn() {
		return deviceSn;
	}
	public void setDeviceSn(String deviceSn) {
		this.deviceSn = deviceSn;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getAppEui() {
		return appEui;
	}
	public void setAppEui(String appEui) {
		this.appEui = appEui;
	}
	public String getDeviceAppKey() {
		return deviceAppKey;
	}
	public void setDeviceAppKey(String deviceAppKey) {
		this.deviceAppKey = deviceAppKey;
	}
	public String getDeviceSeries() {
		return deviceSeries;
	}
	public void setDeviceSeries(String deviceSeries) {
		this.deviceSeries = deviceSeries;
	}
	public String getAdminCorp() {
		return adminCorp;
	}
	public void setAdminCorp(String adminCorp) {
		this.adminCorp = adminCorp;
	}
	
	public String getDescript() {
		return descript;
	}
	public void setDescript(String descript) {
		this.descript = descript;
	}
	public String getExpireDate() {
		return expireDate;
	}
	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}
	public String getNumberOfTimes() {
		return numberOfTimes;
	}
	public void setNumberOfTimes(String numberOfTimes) {
		this.numberOfTimes = numberOfTimes;
	}
	public String getModelCode() {
		return modelCode;
	}
	public void setModelCode(String modelCode) {
		this.modelCode = modelCode;
	}
	public String getManufacturingDate() {
		return manufacturingDate;
	}
	public void setManufacturingDate(String manufacturingDate) {
		this.manufacturingDate = manufacturingDate;
	}
	public String getNetworkOpeningDate() {
		return networkOpeningDate;
	}
	public void setNetworkOpeningDate(String networkOpeningDate) {
		this.networkOpeningDate = networkOpeningDate;
	}
	public String getPlan() {
		return plan;
	}
	public void setPlan(String plan) {
		this.plan = plan;
	}
	public String getMonthlyCharge() {
		return monthlyCharge;
	}
	public void setMonthlyCharge(String monthlyCharge) {
		this.monthlyCharge = monthlyCharge;
	}
	public String getClassType() {
		return classType;
	}
	public void setClassType(String classType) {
		this.classType = classType;
	}
	public String getPeriodOfContract() {
		return periodOfContract;
	}
	public void setPeriodOfContract(String periodOfContract) {
		this.periodOfContract = periodOfContract;
	}
	public String getNameOfBank() {
		return nameOfBank;
	}
	public void setNameOfBank(String nameOfBank) {
		this.nameOfBank = nameOfBank;
	}
	public String getAccountHolder() {
		return accountHolder;
	}
	public void setAccountHolder(String accountHolder) {
		this.accountHolder = accountHolder;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getContractor() {
		return contractor;
	}
	public void setContractor(String contractor) {
		this.contractor = contractor;
	}
	public String getTelecommunicationsOperator() {
		return telecommunicationsOperator;
	}
	public void setTelecommunicationsOperator(String telecommunicationsOperator) {
		this.telecommunicationsOperator = telecommunicationsOperator;
	}
	public String getAdminDeviceState() {
		return adminDeviceState;
	}
	public void setAdminDeviceState(String adminDeviceState) {
		this.adminDeviceState = adminDeviceState;
	}
	public String getAdminDeviceStateModDate() {
		return adminDeviceStateModDate;
	}
	public void setAdminDeviceStateModDate(String adminDeviceStateModDate) {
		this.adminDeviceStateModDate = adminDeviceStateModDate;
	}
	public String getDeviceWorkingState() {
		return deviceWorkingState;
	}
	public void setDeviceWorkingState(String deviceWorkingState) {
		this.deviceWorkingState = deviceWorkingState;
	}
	public String getDeviceWorkingStateDate() {
		return deviceWorkingStateDate;
	}
	public void setDeviceWorkingStateDate(String deviceWorkingStateDate) {
		this.deviceWorkingStateDate = deviceWorkingStateDate;
	}
	public String getBeaconMaj() {
		return beaconMaj;
	}
	public void setBeaconMaj(String beaconMaj) {
		this.beaconMaj = beaconMaj;
	}
	public String getBeaconMin() {
		return beaconMin;
	}
	public void setBeaconMin(String beaconMin) {
		this.beaconMin = beaconMin;
	}
	
	public String getLatestPacket() {
		return latestPacket;
	}
	public void setLatestPacket(String latestPacket) {
		this.latestPacket = latestPacket;
	}
	public String getLatestPaymentType() {
		return latestPaymentType;
	}
	public void setLatestPaymentType(String latestPaymentType) {
		this.latestPaymentType = latestPaymentType;
	}
	public String getShiftDay() {
		return shiftDay;
	}
	public void setShiftDay(String shiftDay) {
		this.shiftDay = shiftDay;
	}
	public String getDelivery() {
		return delivery;
	}
	public void setDelivery(String delivery) {
		this.delivery = delivery;
	}
	public String getDeliveryNum() {
		return deliveryNum;
	}
	public void setDeliveryNum(String deliveryNum) {
		this.deliveryNum = deliveryNum;
	}

		
}
