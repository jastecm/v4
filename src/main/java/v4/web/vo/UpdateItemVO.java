package v4.web.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;



public class UpdateItemVO{
	
	@ApiModelProperty(required = true ,value = "now value")
	private int val;
	@ApiModelProperty(required = true , value = "max value" )
	private String max;	
	@ApiModelProperty(required = true , value = "item index" )
	private int sort;
	@ApiModelProperty(required = true , value = "partsKey" )
	private String partsKey;
	@ApiModelProperty(required = true , value = "itemKey" )
	private String itemKey;	
	
	@JsonIgnore
	@ApiModelProperty(hidden = true)
	private String vehicleKey;
	@JsonIgnore
	@ApiModelProperty(hidden = true)
	private String lang;
	public int getVal() {
		return val;
	}
	public void setVal(int val) {
		this.val = val;
	}
	public String getMax() {
		return max;
	}
	public void setMax(String max) {
		this.max = max;
	}
	public int getSort() {
		return sort;
	}
	public void setSort(int sort) {
		this.sort = sort;
	}
	public String getPartsKey() {
		return partsKey;
	}
	public void setPartsKey(String partsKey) {
		this.partsKey = partsKey;
	}
	public String getItemKey() {
		return itemKey;
	}
	public void setItemKey(String itemKey) {
		this.itemKey = itemKey;
	}
	public String getVehicleKey() {
		return vehicleKey;
	}
	public void setVehicleKey(String vehicleKey) {
		this.vehicleKey = vehicleKey;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	
	
}
