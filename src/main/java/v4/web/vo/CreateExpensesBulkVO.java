package v4.web.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value="CreateExpensesBulkVO" , description="template create expensesbulk value object")
@JsonIgnoreProperties(ignoreUnknown = true)
public @Data class CreateExpensesBulkVO {
	
	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String corp;
	
	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String accountKey;
	
	@ApiModelProperty(value = "plateNum",required = true)
	private String plateNum;
	
	@ApiModelProperty(value = "expensesDate",required = true)
	private String expensesDate;
	
	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String vehicleKey;
	
	@ApiModelProperty(value = "oilPrice",required = true)
	private int oilPrice;
	
	@ApiModelProperty(value = "oilLiquid",required = true)
	private double oilLiquid;
	
	@ApiModelProperty(value = "passageMoney",required = true)
	private int passageMoney;
	
	@ApiModelProperty(value = "parking",required = true)
	private int parking;
	
	@ApiModelProperty(value = "carWash",required = true)
	private int carWash;
	
	@ApiModelProperty(value = "accessories",required = true)
	private int accessories;
	
	@ApiModelProperty(value = "rental",required = true)
	private int rental;
	
	@ApiModelProperty(value = "insurance",required = true)
	private int insurance;
	
	@ApiModelProperty(value = "etc",required = true)
	private int etc;

	@ApiModelProperty(value = "memo")
	private String memo;
}
