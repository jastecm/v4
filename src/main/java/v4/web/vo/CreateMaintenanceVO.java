package v4.web.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value="CreateMaintenanceVO" , description="template create maintenance value object")
@JsonIgnoreProperties(ignoreUnknown = true)
public @Data class CreateMaintenanceVO {
	
	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String corp;
	
	@ApiModelProperty(value = "vehicleKey",required = true)
	private String vehicleKey;
	
	@ApiModelProperty(value = "periodicInspection")
	private String periodicInspection;
	
	@ApiModelProperty(value = "nextPeriodicInspection")
	private String nextPeriodicInspection;
	
	@ApiModelProperty(value = "accountKey",required = true)
	private String accountKey;
	
	@ApiModelProperty(value = "groupKey",required = true)
	private String groupKey;
	
	@ApiModelProperty(value = "maintenanceReqDate",required = true)
	private String maintenanceReqDate;
	
	@ApiModelProperty(value = "completeReqDate",required = true)
	private String completeReqDate;
	
	@ApiModelProperty(value = "troubleDesc")
	private String troubleDesc;
	
	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String regAccount;
	
	
	//나중에 insert할때 채워 넣어야 한다. 나중에 한번에 처리 한다.
	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String approvalState;
	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String approvalStep1Account;
	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private long approvalStep1Date;
	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String approvalStep2Account;
	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private long approvalStep2Date;
	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String maxStep;
	
}
