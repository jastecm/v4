package v4.web.vo;


public class TokenVO{
	private long expired;
	private String userKey;
	private String msg;
	private String corpKey;
	private String corpType;
	private String serviceGrade;
	private String lang;
	
	
	
	public TokenVO(long expired, String userKey,String corpKey,String corpType,String serviceGrade) {
		super();
		this.expired = expired;
		this.userKey = userKey;
		this.corpKey = corpKey;
		this.corpType = corpType;
		this.serviceGrade = serviceGrade;
		this.lang = "ko"; //TODO - 구현
	}
	public TokenVO(String msg) {
		super();
		this.msg = msg;
	}
	
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public long getExpired() {
		return expired;
	}
	public void setExpired(long expired) {
		this.expired = expired;
	}
	public String getUserKey() {
		return userKey;
	}
	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCorpKey() {
		return corpKey;
	}
	public void setCorpKey(String corpKey) {
		this.corpKey = corpKey;
	}
	public String getCorpType() {
		return corpType;
	}
	public void setCorpType(String corpType) {
		this.corpType = corpType;
	}
	public String getServiceGrade() {
		return serviceGrade;
	}
	public void setServiceGrade(String serviceGrade) {
		this.serviceGrade = serviceGrade;
	}

		
}
