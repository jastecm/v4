package v4.web.vo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value="CreateMaintenanceDetailVO" , description="template create maintenanceDetail value object")
@JsonIgnoreProperties(ignoreUnknown = true)
public @Data class CreateMaintenanceDetailVO {
	
	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String corp;
	
	@ApiModelProperty(value = "maintenanceKey",required = true)
	private String maintenanceKey;
	
	@ApiModelProperty(value = "garage",required = true)
	private String garage;
	@ApiModelProperty(value = "distance",required = true)
	private int distance;
	@ApiModelProperty(value = "repairDate",required = true)
	private String repairDate;
	@ApiModelProperty(value = "maintenanceHistory",required = true)
	private String maintenanceHistory;
	
	@ApiModelProperty(value = "partsKey")
	private List<String> partsKey;
	@ApiModelProperty(value = "memo")
	private List<String> memo;
	@ApiModelProperty(value = "partsQuality")
	private List<String> partsQuality;
	@ApiModelProperty(value = "partsCnt")
	private List<Integer> partsCnt;
	@ApiModelProperty(value = "partsPay")
	private List<Integer> partsPay;
	@ApiModelProperty(value = "wagePay")
	private List<Integer> wagePay;
	
}
