package v4.web.vo;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value="CreateDrivingPurposeVO" , description="template DrivingPurpose value object")
@JsonIgnoreProperties(ignoreUnknown = true)
public @Data class CreateDrivingPurposeVO {
	
	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String corp;
	
	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private int seq;
	
	@ApiModelProperty(value = "drivingPurposeName",required = true)
	@NotEmpty(message = "drivingPurposeName is required")
	private String drivingPurposeName;
	
	
}
