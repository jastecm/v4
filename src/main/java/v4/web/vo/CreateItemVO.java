package v4.web.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;



public class CreateItemVO{
	
	@ApiModelProperty(required = true ,value = "now value")
	private int val;
	@ApiModelProperty(required = true , value = "max value" )
	private int max;	
	@ApiModelProperty(required = true , value = "partsKey" )
	private String partsKey;
	
	@JsonIgnore
	@ApiModelProperty(hidden = true)
	private String vehicleKey;
	@JsonIgnore
	@ApiModelProperty(hidden = true)
	private String itemKey;
	@JsonIgnore
	@ApiModelProperty(hidden = true)
	private String sort;
	@JsonIgnore
	@ApiModelProperty(hidden = true)
	private String lang;
	
	public int getVal() {
		return val;
	}
	public void setVal(int val) {
		this.val = val;
	}
	public int getMax() {
		return max;
	}
	public void setMax(int max) {
		this.max = max;
	}
	public String getPartsKey() {
		return partsKey;
	}
	public void setPartsKey(String partsKey) {
		this.partsKey = partsKey;
	}
	public String getVehicleKey() {
		return vehicleKey;
	}
	public void setVehicleKey(String vehicleKey) {
		this.vehicleKey = vehicleKey;
	}
	public String getItemKey() {
		return itemKey;
	}
	public void setItemKey(String itemKey) {
		this.itemKey = itemKey;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	
			
}
