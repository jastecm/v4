package v4.web.vo;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="CreateApprovalConfigVO" , description="template create approvalConfig value object")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateApprovalConfigVO {
	
	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String corpKey;
	
	@ApiModelProperty(value = "allocateApproval" , allowableValues = "1,0" , required = true)
	@NotEmpty(message = "allocateApproval.required")
	@Pattern(regexp ="^1$|^0$" , message = "Invalid accept type (1,0)")
	private String allocateApproval;
	
	private String allocateApprovalStep1;
	private String allocateApprovalStep2;
	
	@ApiModelProperty(value = "autoAllocateApprovalStep1" , allowableValues = "1,0" , required = true)
	@NotEmpty(message = "autoAllocateApprovalStep1.required")
	@Pattern(regexp ="^1$|^0$" , message = "Invalid accept type (1,0)")
	private String autoAllocateApprovalStep1;
	
	@ApiModelProperty(value = "autoAllocateApprovalStep2" , allowableValues = "1,0" , required = true)
	@NotEmpty(message = "autoAllocateApprovalStep2.required")
	@Pattern(regexp ="^1$|^0$" , message = "Invalid accept type (1,0)")
	private String autoAllocateApprovalStep2;
	
	@ApiModelProperty(value = "maintenanceApproval" , allowableValues = "1,0" , required = true)
	@NotEmpty(message = "maintenanceApproval.required")
	@Pattern(regexp ="^1$|^0$" , message = "Invalid accept type (1,0)")
	private String maintenanceApproval;
	
	private String maintenanceApprovalStep1;
	private String maintenanceApprovalStep2;
	
	@ApiModelProperty(value = "autoMaintenanceApprovalStep1" , allowableValues = "1,0" , required = true)
	@NotEmpty(message = "autoMaintenanceApprovalStep1.required")
	@Pattern(regexp ="^1$|^0$" , message = "Invalid accept type (1,0)")
	private String autoMaintenanceApprovalStep1;
	@ApiModelProperty(value = "autoMaintenanceApprovalStep2" , allowableValues = "1,0" , required = true)
	@NotEmpty(message = "autoMaintenanceApprovalStep2.required")
	@Pattern(regexp ="^1$|^0$" , message = "Invalid accept type (1,0)")
	private String autoMaintenanceApprovalStep2;
	
	@ApiModelProperty(value = "expensesApproval" , allowableValues = "1,0" , required = true)
	@NotEmpty(message = "expensesApproval.required")
	@Pattern(regexp ="^1$|^0$" , message = "Invalid accept type (1,0)")
	private String expensesApproval;
	
	private String expensesApprovalStep1;
	private String expensesApprovalStep2;
	
	@ApiModelProperty(value = "autoExpensesApprovalStep1" , allowableValues = "1,0" , required = true)
	@NotEmpty(message = "autoExpensesApprovalStep1.required")
	@Pattern(regexp ="^1$|^0$" , message = "Invalid accept type (1,0)")
	private String autoExpensesApprovalStep1;
	@ApiModelProperty(value = "autoExpensesApprovalStep2" , allowableValues = "1,0" , required = true)
	@NotEmpty(message = "autoExpensesApprovalStep2.required")
	@Pattern(regexp ="^1$|^0$" , message = "Invalid accept type (1,0)")
	private String autoExpensesApprovalStep2;
	
	@ApiModelProperty(value = "accidentApproval" , allowableValues = "1,0" , required = true)
	@NotEmpty(message = "accidentApproval.required")
	@Pattern(regexp ="^1$|^0$" , message = "Invalid accept type (1,0)")
	private String accidentApproval;
	
	private String accidentApprovalStep1;
	private String accidentApprovalStep2;
	private String accidentApprovalStep3;
	public String getCorpKey() {
		return corpKey;
	}
	public void setCorpKey(String corpKey) {
		this.corpKey = corpKey;
	}
	public String getAllocateApproval() {
		return allocateApproval;
	}
	public void setAllocateApproval(String allocateApproval) {
		this.allocateApproval = allocateApproval;
	}
	public String getAllocateApprovalStep1() {
		return allocateApprovalStep1;
	}
	public void setAllocateApprovalStep1(String allocateApprovalStep1) {
		this.allocateApprovalStep1 = allocateApprovalStep1;
	}
	public String getAllocateApprovalStep2() {
		return allocateApprovalStep2;
	}
	public void setAllocateApprovalStep2(String allocateApprovalStep2) {
		this.allocateApprovalStep2 = allocateApprovalStep2;
	}
	public String getAutoAllocateApprovalStep1() {
		return autoAllocateApprovalStep1;
	}
	public void setAutoAllocateApprovalStep1(String autoAllocateApprovalStep1) {
		this.autoAllocateApprovalStep1 = autoAllocateApprovalStep1;
	}
	public String getAutoAllocateApprovalStep2() {
		return autoAllocateApprovalStep2;
	}
	public void setAutoAllocateApprovalStep2(String autoAllocateApprovalStep2) {
		this.autoAllocateApprovalStep2 = autoAllocateApprovalStep2;
	}
	public String getMaintenanceApproval() {
		return maintenanceApproval;
	}
	public void setMaintenanceApproval(String maintenanceApproval) {
		this.maintenanceApproval = maintenanceApproval;
	}
	public String getMaintenanceApprovalStep1() {
		return maintenanceApprovalStep1;
	}
	public void setMaintenanceApprovalStep1(String maintenanceApprovalStep1) {
		this.maintenanceApprovalStep1 = maintenanceApprovalStep1;
	}
	public String getMaintenanceApprovalStep2() {
		return maintenanceApprovalStep2;
	}
	public void setMaintenanceApprovalStep2(String maintenanceApprovalStep2) {
		this.maintenanceApprovalStep2 = maintenanceApprovalStep2;
	}
	public String getAutoMaintenanceApprovalStep1() {
		return autoMaintenanceApprovalStep1;
	}
	public void setAutoMaintenanceApprovalStep1(String autoMaintenanceApprovalStep1) {
		this.autoMaintenanceApprovalStep1 = autoMaintenanceApprovalStep1;
	}
	public String getAutoMaintenanceApprovalStep2() {
		return autoMaintenanceApprovalStep2;
	}
	public void setAutoMaintenanceApprovalStep2(String autoMaintenanceApprovalStep2) {
		this.autoMaintenanceApprovalStep2 = autoMaintenanceApprovalStep2;
	}
	public String getExpensesApproval() {
		return expensesApproval;
	}
	public void setExpensesApproval(String expensesApproval) {
		this.expensesApproval = expensesApproval;
	}
	public String getExpensesApprovalStep1() {
		return expensesApprovalStep1;
	}
	public void setExpensesApprovalStep1(String expensesApprovalStep1) {
		this.expensesApprovalStep1 = expensesApprovalStep1;
	}
	public String getExpensesApprovalStep2() {
		return expensesApprovalStep2;
	}
	public void setExpensesApprovalStep2(String expensesApprovalStep2) {
		this.expensesApprovalStep2 = expensesApprovalStep2;
	}
	public String getAutoExpensesApprovalStep1() {
		return autoExpensesApprovalStep1;
	}
	public void setAutoExpensesApprovalStep1(String autoExpensesApprovalStep1) {
		this.autoExpensesApprovalStep1 = autoExpensesApprovalStep1;
	}
	public String getAutoExpensesApprovalStep2() {
		return autoExpensesApprovalStep2;
	}
	public void setAutoExpensesApprovalStep2(String autoExpensesApprovalStep2) {
		this.autoExpensesApprovalStep2 = autoExpensesApprovalStep2;
	}
	public String getAccidentApproval() {
		return accidentApproval;
	}
	public void setAccidentApproval(String accidentApproval) {
		this.accidentApproval = accidentApproval;
	}
	public String getAccidentApprovalStep1() {
		return accidentApprovalStep1;
	}
	public void setAccidentApprovalStep1(String accidentApprovalStep1) {
		this.accidentApprovalStep1 = accidentApprovalStep1;
	}
	public String getAccidentApprovalStep2() {
		return accidentApprovalStep2;
	}
	public void setAccidentApprovalStep2(String accidentApprovalStep2) {
		this.accidentApprovalStep2 = accidentApprovalStep2;
	}
	public String getAccidentApprovalStep3() {
		return accidentApprovalStep3;
	}
	public void setAccidentApprovalStep3(String accidentApprovalStep3) {
		this.accidentApprovalStep3 = accidentApprovalStep3;
	}
	
	
	
	
	 
}
