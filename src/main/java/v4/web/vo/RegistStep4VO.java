package v4.web.vo;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RegistStep4VO {
	
	@ApiModelProperty(value = "accountId type email",required = true)
	@NotEmpty(message = "accountId is required")
	@Pattern(regexp = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$" , message = "userId is email type")
	private String accountId;
	
	@ApiModelProperty(required = true ,value = "accountPw")
	//@Pattern(regexp = "^(?=.*[a-zA-Z])(?=.*[!@#$%^\\&*\\(\\)\\_\\+\\|\\-\\=\\\\\\[\\];'?\\.\\,])(?=.*[0-9]).{8,20}$" , message = "bad userPw(size 8~20 , en , )")
	@Pattern(regexp = "^.{8,20}$" , message = "bad accountPw(size 8~20)")
	private String accountPw;

	@ApiModelProperty(required = true ,value = "corp type")
	@Pattern(regexp = "^1$|^2$|^3$|^4$" , message = "corpType only 1(person),2(corp),3(rent),4(partner)")
	private String corpType;
	
	
	
	
	@ApiModelProperty(value = "smsAccepted" , allowableValues = "1,0" , required = true)
	@Pattern(regexp ="^1$|^0$" , message = "Invalid accept type (1,0)")
	@NotEmpty(message = "smsAccepted.required")
	private String smsAccepted = "1";
	
	@ApiModelProperty(value = "locationAccepted" , allowableValues = "1,0" , required = true)
	@Pattern(regexp ="^1$|^0$" , message = "Invalid accept type (1,0)")
	@NotEmpty(message = "locationAccepted.required")
	private String locationAccepted;
	
	@ApiModelProperty(value = "privateAccepted" , allowableValues = "1,0" , required = true)
	@Pattern(regexp ="^1$|^0$" , message = "Invalid accept type (1,0)")
	@NotEmpty(message = "privateAccepted.required")
	private String privateAccepted;

	@ApiModelProperty(value = "marketingAccepted" , allowableValues = "1,0" , required = true)
	@Pattern(regexp ="^1$|^0$" , message = "Invalid accept type (1,0)")
	@NotEmpty(message = "marketingAccepted.required")
	private String marketingAccepted;
	
	@ApiModelProperty(value = "serviceAccepted" , allowableValues = "1,0" , required = true)
	@Pattern(regexp ="^1$|^0$" , message = "Invalid accept type (1,0)")
	@NotEmpty(message = "serviceAccepted.required")
	private String serviceAccepted;
	
	@ApiModelProperty(value = "display languege" , example = "ko,en" , allowableValues = "ko,en")
	@Pattern(regexp ="^ko$|^en$|^$" , message = "can only ko,en")	
	private String lang = "ko";
	
	@ApiModelProperty(value = "timezoneOffset" , example = "-720 ~ 840")
	@Range(min=-720,max=840)
	private int timezoneOffset = 540;
	
	@ApiModelProperty(required = true ,value = "countryCode" , example = "82")
	@NotEmpty(message = "ctyCode is required")
	@Range(min=1 , max=999 , message = "countryCode's range 1~999")
	private String ctyCode;
	
	@ApiModelProperty(value = "unitLength" , example = "km,mi")
	@Pattern(regexp ="^km$|^mi$" , message = "can only km,mi(mile)")
	private String unitLength = "km";
	
	@ApiModelProperty(value = "unitVolume" , example = "l(liter),gal(gallon)")
	@Pattern(regexp ="^l$|^gal$" , message = "can only l,gal")
	private String unitVolume = "l";
	
	@ApiModelProperty(value = "unitTemperature" , example = "C,F")
	@Pattern(regexp ="^C$|^F$" , message = "can only C,F")	
	private String unitTemperature = "C";
	
	@ApiModelProperty(value = "unitWeight" , example = "g,oz")
	@Pattern(regexp ="^g$|^oz$" , message = "can only g(gram),oz(ounce)")
	private String unitWeight = "g";
	
	@ApiModelProperty(value = "unitDate" , example = "YMD,MDY,DMY")
	@Pattern(regexp ="^YMD$|^MDY$|^DMY&" , message = "can only YMD,MDY,DMY")
	private String unitDate = "YMD";

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getAccountPw() {
		return accountPw;
	}

	public void setAccountPw(String accountPw) {
		this.accountPw = accountPw;
	}

	public String getCorpType() {
		return corpType;
	}

	public void setCorpType(String corpType) {
		this.corpType = corpType;
	}

	public String getSmsAccepted() {
		return smsAccepted;
	}

	public void setSmsAccepted(String smsAccepted) {
		this.smsAccepted = smsAccepted;
	}

	public String getLocationAccepted() {
		return locationAccepted;
	}

	public void setLocationAccepted(String locationAccepted) {
		this.locationAccepted = locationAccepted;
	}

	public String getPrivateAccepted() {
		return privateAccepted;
	}

	public void setPrivateAccepted(String privateAccepted) {
		this.privateAccepted = privateAccepted;
	}

	public String getMarketingAccepted() {
		return marketingAccepted;
	}

	public void setMarketingAccepted(String marketingAccepted) {
		this.marketingAccepted = marketingAccepted;
	}

	public String getServiceAccepted() {
		return serviceAccepted;
	}

	public void setServiceAccepted(String serviceAccepted) {
		this.serviceAccepted = serviceAccepted;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public int getTimezoneOffset() {
		return timezoneOffset;
	}

	public void setTimezoneOffset(int timezoneOffset) {
		this.timezoneOffset = timezoneOffset;
	}

	public String getCtyCode() {
		return ctyCode;
	}

	public void setCtyCode(String ctyCode) {
		this.ctyCode = ctyCode;
	}

	public String getUnitLength() {
		return unitLength;
	}

	public void setUnitLength(String unitLength) {
		this.unitLength = unitLength;
	}

	public String getUnitVolume() {
		return unitVolume;
	}

	public void setUnitVolume(String unitVolume) {
		this.unitVolume = unitVolume;
	}

	public String getUnitTemperature() {
		return unitTemperature;
	}

	public void setUnitTemperature(String unitTemperature) {
		this.unitTemperature = unitTemperature;
	}

	public String getUnitWeight() {
		return unitWeight;
	}

	public void setUnitWeight(String unitWeight) {
		this.unitWeight = unitWeight;
	}

	public String getUnitDate() {
		return unitDate;
	}

	public void setUnitDate(String unitDate) {
		this.unitDate = unitDate;
	}
	
	
	
				
}
