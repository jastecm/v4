package v4.web.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CorpVO {
	
	private int seq;
	private String corpKey, corpType, serviceGrade, partnerType, corpName, corpNum, ceoName, corpPhone, corpPhoneCtyCode, corpBusiness, corpItem, allocationUsed, apiKey, apiExpired, geoMaxCnt;
	private long regDate;
	
	private String vehicleManager;
	private String topManager;
	
	//배차 결재
	private String allocateApproval;
	private String allocateApprovalStep1;
	private String allocateApprovalStep2;
	private String autoAllocateApprovalStep1;
	private String autoAllocateApprovalStep2;
	//정비결재
	private String maintenanceApproval;
	private String maintenanceApprovalStep1;
	private String maintenanceApprovalStep2;
	private String autoMaintenanceApprovalStep1;
	private String autoMaintenanceApprovalStep2;
	//경비결재
	private String expensesApproval;
	private String expensesApprovalStep1;
	private String expensesApprovalStep2;
	private String autoExpensesApprovalStep1;
	private String autoExpensesApprovalStep2;
	//사고보고서
	private String accidentApproval;
	private String accidentApprovalStep1;
	private String accidentApprovalStep2;
	private String accidentApprovalStep3;
	
	
	
	public String getVehicleManager() {
		return vehicleManager;
	}
	public void setVehicleManager(String vehicleManager) {
		this.vehicleManager = vehicleManager;
	}
	public String getTopManager() {
		return topManager;
	}
	public void setTopManager(String topManager) {
		this.topManager = topManager;
	}
	public String getAllocateApproval() {
		return allocateApproval;
	}
	public void setAllocateApproval(String allocateApproval) {
		this.allocateApproval = allocateApproval;
	}
	public String getAllocateApprovalStep1() {
		return allocateApprovalStep1;
	}
	public void setAllocateApprovalStep1(String allocateApprovalStep1) {
		this.allocateApprovalStep1 = allocateApprovalStep1;
	}
	public String getAllocateApprovalStep2() {
		return allocateApprovalStep2;
	}
	public void setAllocateApprovalStep2(String allocateApprovalStep2) {
		this.allocateApprovalStep2 = allocateApprovalStep2;
	}
	public String getAutoAllocateApprovalStep1() {
		return autoAllocateApprovalStep1;
	}
	public void setAutoAllocateApprovalStep1(String autoAllocateApprovalStep1) {
		this.autoAllocateApprovalStep1 = autoAllocateApprovalStep1;
	}
	public String getAutoAllocateApprovalStep2() {
		return autoAllocateApprovalStep2;
	}
	public void setAutoAllocateApprovalStep2(String autoAllocateApprovalStep2) {
		this.autoAllocateApprovalStep2 = autoAllocateApprovalStep2;
	}
	public String getMaintenanceApproval() {
		return maintenanceApproval;
	}
	public void setMaintenanceApproval(String maintenanceApproval) {
		this.maintenanceApproval = maintenanceApproval;
	}
	public String getMaintenanceApprovalStep1() {
		return maintenanceApprovalStep1;
	}
	public void setMaintenanceApprovalStep1(String maintenanceApprovalStep1) {
		this.maintenanceApprovalStep1 = maintenanceApprovalStep1;
	}
	public String getMaintenanceApprovalStep2() {
		return maintenanceApprovalStep2;
	}
	public void setMaintenanceApprovalStep2(String maintenanceApprovalStep2) {
		this.maintenanceApprovalStep2 = maintenanceApprovalStep2;
	}
	public String getAutoMaintenanceApprovalStep1() {
		return autoMaintenanceApprovalStep1;
	}
	public void setAutoMaintenanceApprovalStep1(String autoMaintenanceApprovalStep1) {
		this.autoMaintenanceApprovalStep1 = autoMaintenanceApprovalStep1;
	}
	public String getAutoMaintenanceApprovalStep2() {
		return autoMaintenanceApprovalStep2;
	}
	public void setAutoMaintenanceApprovalStep2(String autoMaintenanceApprovalStep2) {
		this.autoMaintenanceApprovalStep2 = autoMaintenanceApprovalStep2;
	}
	public String getExpensesApproval() {
		return expensesApproval;
	}
	public void setExpensesApproval(String expensesApproval) {
		this.expensesApproval = expensesApproval;
	}
	public String getExpensesApprovalStep1() {
		return expensesApprovalStep1;
	}
	public void setExpensesApprovalStep1(String expensesApprovalStep1) {
		this.expensesApprovalStep1 = expensesApprovalStep1;
	}
	public String getExpensesApprovalStep2() {
		return expensesApprovalStep2;
	}
	public void setExpensesApprovalStep2(String expensesApprovalStep2) {
		this.expensesApprovalStep2 = expensesApprovalStep2;
	}
	public String getAutoExpensesApprovalStep1() {
		return autoExpensesApprovalStep1;
	}
	public void setAutoExpensesApprovalStep1(String autoExpensesApprovalStep1) {
		this.autoExpensesApprovalStep1 = autoExpensesApprovalStep1;
	}
	public String getAutoExpensesApprovalStep2() {
		return autoExpensesApprovalStep2;
	}
	public void setAutoExpensesApprovalStep2(String autoExpensesApprovalStep2) {
		this.autoExpensesApprovalStep2 = autoExpensesApprovalStep2;
	}
	public String getAccidentApproval() {
		return accidentApproval;
	}
	public void setAccidentApproval(String accidentApproval) {
		this.accidentApproval = accidentApproval;
	}
	public String getAccidentApprovalStep1() {
		return accidentApprovalStep1;
	}
	public void setAccidentApprovalStep1(String accidentApprovalStep1) {
		this.accidentApprovalStep1 = accidentApprovalStep1;
	}
	public String getAccidentApprovalStep2() {
		return accidentApprovalStep2;
	}
	public void setAccidentApprovalStep2(String accidentApprovalStep2) {
		this.accidentApprovalStep2 = accidentApprovalStep2;
	}
	public String getAccidentApprovalStep3() {
		return accidentApprovalStep3;
	}
	public void setAccidentApprovalStep3(String accidentApprovalStep3) {
		this.accidentApprovalStep3 = accidentApprovalStep3;
	}
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	public String getCorpKey() {
		return corpKey;
	}
	public void setCorpKey(String corpKey) {
		this.corpKey = corpKey;
	}
	public String getCorpType() {
		return corpType;
	}
	public void setCorpType(String corpType) {
		this.corpType = corpType;
	}
	public String getServiceGrade() {
		return serviceGrade;
	}
	public void setServiceGrade(String serviceGrade) {
		this.serviceGrade = serviceGrade;
	}
	public String getPartnerType() {
		return partnerType;
	}
	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}
	public String getCorpName() {
		return corpName;
	}
	public void setCorpName(String corpName) {
		this.corpName = corpName;
	}
	public String getCorpNum() {
		return corpNum;
	}
	public void setCorpNum(String corpNum) {
		this.corpNum = corpNum;
	}
	public String getCeoName() {
		return ceoName;
	}
	public void setCeoName(String ceoName) {
		this.ceoName = ceoName;
	}
	public String getCorpPhone() {
		return corpPhone;
	}
	public void setCorpPhone(String corpPhone) {
		this.corpPhone = corpPhone;
	}
	public String getCorpPhoneCtyCode() {
		return corpPhoneCtyCode;
	}
	public void setCorpPhoneCtyCode(String corpPhoneCtyCode) {
		this.corpPhoneCtyCode = corpPhoneCtyCode;
	}
	public String getCorpBusiness() {
		return corpBusiness;
	}
	public void setCorpBusiness(String corpBusiness) {
		this.corpBusiness = corpBusiness;
	}
	public String getCorpItem() {
		return corpItem;
	}
	public void setCorpItem(String corpItem) {
		this.corpItem = corpItem;
	}
	public String getAllocationUsed() {
		return allocationUsed;
	}
	public void setAllocationUsed(String allocationUsed) {
		this.allocationUsed = allocationUsed;
	}
	public String getApiKey() {
		return apiKey;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	public String getApiExpired() {
		return apiExpired;
	}
	public void setApiExpired(String apiExpired) {
		this.apiExpired = apiExpired;
	}
	public String getGeoMaxCnt() {
		return geoMaxCnt;
	}
	public void setGeoMaxCnt(String geoMaxCnt) {
		this.geoMaxCnt = geoMaxCnt;
	}
	public long getRegDate() {
		return regDate;
	}
	public void setRegDate(long regDate) {
		this.regDate = regDate;
	}
	
	
}
