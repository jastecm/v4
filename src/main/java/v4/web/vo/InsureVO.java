package v4.web.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;

public class InsureVO{
	
	@JsonIgnore
	@ApiModelProperty(hidden = true)	
	private String vehicleKey;
	
	@ApiModelProperty(required = true ,value = "insure No.")
	private int corpInsure;
	
	@ApiModelProperty(required = true )
	private long startDate;
	
	@ApiModelProperty(required = true )
	private long endDate;
	
	@ApiModelProperty(required = true ,value = "only 1,0 accept")
	private String insureState;
	
	private int insureDistatnce;
	private int startDistance;
	
	@JsonIgnore
	@ApiModelProperty(hidden = true)	
	private int insureDistanceMax;
	            
	
	public int getInsureDistanceMax() {
		return insureDistanceMax;
	}
	public void setInsureDistanceMax(int insureDistanceMax) {
		this.insureDistanceMax = insureDistanceMax;
	}
	public String getVehicleKey() {
		return vehicleKey;
	}
	public void setVehicleKey(String vehicleKey) {
		this.vehicleKey = vehicleKey;
	}
	public int getCorpInsure() {
		return corpInsure;
	}
	public void setCorpInsure(int corpInsure) {
		this.corpInsure = corpInsure;
	}
	public long getStartDate() {
		return startDate;
	}
	public void setStartDate(long startDate) {
		this.startDate = startDate;
	}
	public long getEndDate() {
		return endDate;
	}
	public void setEndDate(long endDate) {
		this.endDate = endDate;
	}
	public String getInsureState() {
		return insureState;
	}
	public void setInsureState(String insureState) {
		this.insureState = insureState;
	}
	public int getInsureDistatnce() {
		return insureDistatnce;
	}
	public void setInsureDistatnce(int insureDistatnce) {
		this.insureDistatnce = insureDistatnce;
	}
	public int getStartDistance() {
		return startDistance;
	}
	public void setStartDistance(int startDistance) {
		this.startDistance = startDistance;
	}
	
	
	
	
}
