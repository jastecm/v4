package v4.web.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;

public class UpdateClinicVO {
	
	@JsonIgnore
	private String maintenanceKey;

	@ApiModelProperty(value = "vehicleKey")
	private String vehicleKey;
	
	@ApiModelProperty(value = "periodicInspection")
	private String periodicInspection;
	
	@ApiModelProperty(value = "nextPeriodicInspection")
	private String nextPeriodicInspection;
	
	@ApiModelProperty(value = "reqAccountKey")
	private String reqAccountKey;
	
	@ApiModelProperty(value = "groupKey")
	private String groupKey;
	
	@ApiModelProperty(value = "maintenanceReqDate")
	private String maintenanceReqDate;
	
	@ApiModelProperty(value = "completeReqDate")
	private String completeReqDate;
	
	@ApiModelProperty(value = "troubleDesc")
	private String troubleDesc;
	
	@ApiModelProperty(value = "clinicNm")
	private String clinicNm;
	
	@ApiModelProperty(value = "distance")
	private String distance;
	
	@ApiModelProperty(value = "clinicDate")
	private String clinicDate;
	
	@ApiModelProperty(value = "repairContent")
	private String repairContent;
	
	
	public String getMaintenanceKey() {
		return maintenanceKey;
	}
	public void setMaintenanceKey(String maintenanceKey) {
		this.maintenanceKey = maintenanceKey;
	}
	public String getVehicleKey() {
		return vehicleKey;
	}
	public void setVehicleKey(String vehicleKey) {
		this.vehicleKey = vehicleKey;
	}
	public String getPeriodicInspection() {
		return periodicInspection;
	}
	public void setPeriodicInspection(String periodicInspection) {
		this.periodicInspection = periodicInspection;
	}
	public String getNextPeriodicInspection() {
		return nextPeriodicInspection;
	}
	public void setNextPeriodicInspection(String nextPeriodicInspection) {
		this.nextPeriodicInspection = nextPeriodicInspection;
	}
	public String getReqAccountKey() {
		return reqAccountKey;
	}
	public void setReqAccountKey(String reqAccountKey) {
		this.reqAccountKey = reqAccountKey;
	}
	public String getGroupKey() {
		return groupKey;
	}
	public void setGroupKey(String groupKey) {
		this.groupKey = groupKey;
	}
	public String getMaintenanceReqDate() {
		return maintenanceReqDate;
	}
	public void setMaintenanceReqDate(String maintenanceReqDate) {
		this.maintenanceReqDate = maintenanceReqDate;
	}
	public String getCompleteReqDate() {
		return completeReqDate;
	}
	public void setCompleteReqDate(String completeReqDate) {
		this.completeReqDate = completeReqDate;
	}
	public String getTroubleDesc() {
		return troubleDesc;
	}
	public void setTroubleDesc(String troubleDesc) {
		this.troubleDesc = troubleDesc;
	}
	public String getClinicNm() {
		return clinicNm;
	}
	public void setClinicNm(String clinicNm) {
		this.clinicNm = clinicNm;
	}
	public String getDistance() {
		return distance;
	}
	public void setDistance(String distance) {
		this.distance = distance;
	}
	public String getClinicDate() {
		return clinicDate;
	}
	public void setClinicDate(String clinicDate) {
		this.clinicDate = clinicDate;
	}
	public String getRepairContent() {
		return repairContent;
	}
	public void setRepairContent(String repairContent) {
		this.repairContent = repairContent;
	}
	
	
	
}
