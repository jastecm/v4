package v4.web.vo;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;

public class CreateAllocateVO {
	
	@JsonIgnore
	@ApiModelProperty(hidden = true)
	private String allocateKey;
	
	@JsonIgnore
	@ApiModelProperty(hidden = true)
	private String vehicleKey;
	
	@NotEmpty(message = "accountKey is required")
	private String accountKey;
	
	@JsonIgnore
	@ApiModelProperty(hidden = true)
	private String regAccountKey;

	@JsonIgnore
	@ApiModelProperty(hidden = true)
	private String corpKey;
	
	
	private long startDate;
	private long endDate;
	
	@ApiModelProperty(value = "purpose" , allowableValues = "1,2,3" , required = true)
	@Pattern(regexp ="^1$|^2$|^3$" , message = "Invalid accept type (1,2,3)")
	private String purpose;
	
	@ApiModelProperty(required = true ,value = "title")
	private String title;
	
	private String passenger;
	private String destination;
	private String contents;
	
	
	@ApiModelProperty(value = "isHolyDay" , allowableValues = "1,0" , required = true)
	@Pattern(regexp ="^1$|^0$" , message = "Invalid accept type (1,0)")
	private String isHolyDay;
	
	
	@JsonIgnore
	@ApiModelProperty(hidden = true)
	private String approvalStep1Account;
	@JsonIgnore
	@ApiModelProperty(hidden = true)
	private String approvalStep2Account;
	@JsonIgnore
	@ApiModelProperty(hidden = true)
	private String approvalState;
	@JsonIgnore
	@ApiModelProperty(hidden = true)
	private String maxStep;
	
	@JsonIgnore
	@ApiModelProperty(hidden = true)
	private Long approvalStep1Date;
	@JsonIgnore
	@ApiModelProperty(hidden = true)
	private Long approvalStep2Date;
	
	
	
	
	public String getVehicleKey() {
		return vehicleKey;
	}
	public void setVehicleKey(String vehicleKey) {
		this.vehicleKey = vehicleKey;
	}
	public String getRegAccountKey() {
		return regAccountKey;
	}
	public void setRegAccountKey(String regAccountKey) {
		this.regAccountKey = regAccountKey;
	}
	
	
	
	public Long getApprovalStep1Date() {
		return approvalStep1Date;
	}
	public void setApprovalStep1Date(Long approvalStep1Date) {
		this.approvalStep1Date = approvalStep1Date;
	}
	public Long getApprovalStep2Date() {
		return approvalStep2Date;
	}
	public void setApprovalStep2Date(Long approvalStep2Date) {
		this.approvalStep2Date = approvalStep2Date;
	}
	public String getMaxStep() {
		return maxStep;
	}
	public void setMaxStep(String maxStep) {
		this.maxStep = maxStep;
	}
	public String getApprovalStep1Account() {
		return approvalStep1Account;
	}
	public void setApprovalStep1Account(String approvalStep1Account) {
		this.approvalStep1Account = approvalStep1Account;
	}
	public String getApprovalStep2Account() {
		return approvalStep2Account;
	}
	public void setApprovalStep2Account(String approvalStep2Account) {
		this.approvalStep2Account = approvalStep2Account;
	}
	public String getApprovalState() {
		return approvalState;
	}
	public void setApprovalState(String approvalState) {
		this.approvalState = approvalState;
	}
	public String getAllocateKey() {
		return allocateKey;
	}
	public void setAllocateKey(String allocateKey) {
		this.allocateKey = allocateKey;
	}
	public String getAccountKey() {
		return accountKey;
	}
	public void setAccountKey(String accountKey) {
		this.accountKey = accountKey;
	}
	
	public String getCorpKey() {
		return corpKey;
	}
	public void setCorpKey(String corpKey) {
		this.corpKey = corpKey;
	}
	public long getStartDate() {
		return startDate;
	}
	public void setStartDate(long startDate) {
		this.startDate = startDate;
	}
	public long getEndDate() {
		return endDate;
	}
	public void setEndDate(long endDate) {
		this.endDate = endDate;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPassenger() {
		return passenger;
	}
	public void setPassenger(String passenger) {
		this.passenger = passenger;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	public String getIsHolyDay() {
		return isHolyDay;
	}
	public void setIsHolyDay(String isHolyDay) {
		this.isHolyDay = isHolyDay;
	}
	
	
}
