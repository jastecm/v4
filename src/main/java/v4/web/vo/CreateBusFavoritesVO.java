package v4.web.vo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@ApiModel(value="CreateBusFavoritesVO" , description="template CreateBusFavoritesVO value object")
@JsonIgnoreProperties(ignoreUnknown = true)
public @Data class CreateBusFavoritesVO {

	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String corp;
	
	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String userKey;
	
	@ApiModelProperty(value = "busRouteKey",required = true)
	private String busRouteKey;
	
}
