package v4.web.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class PushControllVO{
	@JsonIgnore
	private String accountKey;
	
	private String pushType;
	private String state;
	
	@JsonIgnore
	private String lastestKey;
	
	public String getAccountKey() {
		return accountKey;
	}
	public void setAccountKey(String accountKey) {
		this.accountKey = accountKey;
	}
	public String getPushType() {
		return pushType;
	}
	public void setPushType(String pushType) {
		this.pushType = pushType;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getLastestKey() {
		return lastestKey;
	}
	public void setLastestKey(String lastestKey) {
		this.lastestKey = lastestKey;
	}
	
		
}
