package v4.web.vo;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;

public class CreateMaintenanceCompanyVO {
	@JsonIgnore
	private String seq;
	@JsonIgnore
	private String corpKey;
	
	@JsonIgnore
	private String companyKey;
	
	@ApiModelProperty(value = "companyNm is required", required = true)
	@NotEmpty(message = "companyNm is required")
	private String companyNm;

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getCorpKey() {
		return corpKey;
	}

	public void setCorpKey(String corpKey) {
		this.corpKey = corpKey;
	}

	public String getCompanyKey() {
		return companyKey;
	}

	public void setCompanyKey(String companyKey) {
		this.companyKey = companyKey;
	}

	public String getCompanyNm() {
		return companyNm;
	}

	public void setCompanyNm(String companyNm) {
		this.companyNm = companyNm;
	}
	
	
}
