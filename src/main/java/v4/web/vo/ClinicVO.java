package v4.web.vo;

public class ClinicVO {
	private String maintenanceKey;
	private String accountKey;
	private String corpKey;
	private String regDate;
	private String vehicleKey;
	private String periodicInspection;
	private String nextPeriodicInspection;
	private String reqAccountKey;
	private String groupKey;
	private String maintenanceReqDate;
	private String completeReqDate;
	private String troubleDesc;
	private String clinicNm;
	private String distance;
	private String clinicDate;
	private String repairContent;
	private String approvalState;
	private String approvalStep1Account;
	private String approvalStep1Date;
	private String approvalStep2Account;
	private String approvalStep2Date;
	private String approvalReserveAccount;
	private String approvalreserveDate;
	
	
	
	public String getMaintenanceKey() {
		return maintenanceKey;
	}
	public void setMaintenanceKey(String maintenanceKey) {
		this.maintenanceKey = maintenanceKey;
	}
	public String getAccountKey() {
		return accountKey;
	}
	public void setAccountKey(String accountKey) {
		this.accountKey = accountKey;
	}
	public String getCorpKey() {
		return corpKey;
	}
	public void setCorpKey(String corpKey) {
		this.corpKey = corpKey;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getVehicleKey() {
		return vehicleKey;
	}
	public void setVehicleKey(String vehicleKey) {
		this.vehicleKey = vehicleKey;
	}
	public String getPeriodicInspection() {
		return periodicInspection;
	}
	public void setPeriodicInspection(String periodicInspection) {
		this.periodicInspection = periodicInspection;
	}
	public String getNextPeriodicInspection() {
		return nextPeriodicInspection;
	}
	public void setNextPeriodicInspection(String nextPeriodicInspection) {
		this.nextPeriodicInspection = nextPeriodicInspection;
	}
	public String getReqAccountKey() {
		return reqAccountKey;
	}
	public void setReqAccountKey(String reqAccountKey) {
		this.reqAccountKey = reqAccountKey;
	}
	public String getGroupKey() {
		return groupKey;
	}
	public void setGroupKey(String groupKey) {
		this.groupKey = groupKey;
	}
	public String getMaintenanceReqDate() {
		return maintenanceReqDate;
	}
	public void setMaintenanceReqDate(String maintenanceReqDate) {
		this.maintenanceReqDate = maintenanceReqDate;
	}
	public String getCompleteReqDate() {
		return completeReqDate;
	}
	public void setCompleteReqDate(String completeReqDate) {
		this.completeReqDate = completeReqDate;
	}
	public String getTroubleDesc() {
		return troubleDesc;
	}
	public void setTroubleDesc(String troubleDesc) {
		this.troubleDesc = troubleDesc;
	}
	public String getClinicNm() {
		return clinicNm;
	}
	public void setClinicNm(String clinicNm) {
		this.clinicNm = clinicNm;
	}
	public String getDistance() {
		return distance;
	}
	public void setDistance(String distance) {
		this.distance = distance;
	}
	public String getClinicDate() {
		return clinicDate;
	}
	public void setClinicDate(String clinicDate) {
		this.clinicDate = clinicDate;
	}
	public String getRepairContent() {
		return repairContent;
	}
	public void setRepairContent(String repairContent) {
		this.repairContent = repairContent;
	}
	public String getApprovalState() {
		return approvalState;
	}
	public void setApprovalState(String approvalState) {
		this.approvalState = approvalState;
	}
	public String getApprovalStep1Account() {
		return approvalStep1Account;
	}
	public void setApprovalStep1Account(String approvalStep1Account) {
		this.approvalStep1Account = approvalStep1Account;
	}
	public String getApprovalStep1Date() {
		return approvalStep1Date;
	}
	public void setApprovalStep1Date(String approvalStep1Date) {
		this.approvalStep1Date = approvalStep1Date;
	}
	public String getApprovalStep2Account() {
		return approvalStep2Account;
	}
	public void setApprovalStep2Account(String approvalStep2Account) {
		this.approvalStep2Account = approvalStep2Account;
	}
	public String getApprovalStep2Date() {
		return approvalStep2Date;
	}
	public void setApprovalStep2Date(String approvalStep2Date) {
		this.approvalStep2Date = approvalStep2Date;
	}
	public String getApprovalReserveAccount() {
		return approvalReserveAccount;
	}
	public void setApprovalReserveAccount(String approvalReserveAccount) {
		this.approvalReserveAccount = approvalReserveAccount;
	}
	public String getApprovalreserveDate() {
		return approvalreserveDate;
	}
	public void setApprovalreserveDate(String approvalreserveDate) {
		this.approvalreserveDate = approvalreserveDate;
	}
	
	
	
}
