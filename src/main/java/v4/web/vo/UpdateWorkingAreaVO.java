package v4.web.vo;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class UpdateWorkingAreaVO {
	
	private String groupKey;
	
	private String accountKey;
	
	private String cityLev1;

	private String cityLev2;
	
	public String getGroupKey() {
		return groupKey;
	}
	public void setGroupKey(String groupKey) {
		this.groupKey = groupKey;
	}
	public String getAccountKey() {
		return accountKey;
	}
	public void setAccountKey(String accountKey) {
		this.accountKey = accountKey;
	}
	public String getCityLev1() {
		return cityLev1;
	}
	public void setCityLev1(String cityLev1) {
		this.cityLev1 = cityLev1;
	}
	public String getCityLev2() {
		return cityLev2;
	}
	public void setCityLev2(String cityLev2) {
		this.cityLev2 = cityLev2;
	}
	

}
