package v4.web.vo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author woong
 *
 */
@ApiModel(value="UpdateVehicleVO" , description="template update Vehicle value object")
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateVehicleVO {
	
	@ApiModelProperty(value = "plateNum",required = true)
	@NotEmpty(message = "plateNum is required")
	private String plateNum;
	
	@ApiModelProperty(value = "device S/N",required = true)
	@NotEmpty(message = "deiceSn is required")
	private String deviceSn;
	
	@ApiModelProperty(value = "convCode",required = true)
	@NotEmpty(message = "convCode is required")
	private String convCode;
	
	@ApiModelProperty(value = "totDistance(unit. km)",required = true)
	@NotNull(message = "totDistance is required")
	private int totDistance = 0;
	
	///////////필수항목 끝/////////////
	
	//수정불가
	@ApiModelProperty(value = "vehicleType", hidden = true)
	@Pattern(regexp = "^1$|^2$|^3$|^4$" , message = "vehicleType only 1(vehicle),2(SUV),3(trak),4(ETC)")
	private String vehicleType = "1";
	
	@ApiModelProperty(value = "corpGroupKey")
	private String corpGroupKey;
	
	@ApiModelProperty(value = "fixed")
	@Pattern(regexp = "^0$|^1$|^$" , message = "fixed only 1(ON),0(OFF)")
	private String fixed = "0";
	
	@ApiModelProperty(value = "fixedOnOff 1 on account")
	private String fixedAccountKey;
	
	@ApiModelProperty(value = "fixedOnOff 1 on driver account")
	private String fixedDriverKey;
	
	@ApiModelProperty(value = "fixedOnOff 1 on account")	
	private String fixedContents;
	
	@ApiModelProperty(value = "autoReport")
	@Pattern(regexp = "^0$|^1$" , message = "autoReport only 1(ON),0(OFF)")
	private String autoReport = "0";
	
	@ApiModelProperty(value = "autoReport 1 on purpose")
	private String autoReportPurpose = "1";
	
	@ApiModelProperty(value = "autoReport 1 on contents")
	private String autoReportContents;
	
	@ApiModelProperty(value = "locationSetting")
	@Pattern(regexp = "^0$|^1$" , message = "locationSetting only 1(ON),0(OFF)")
	private String locationSetting = "1";
	
	@ApiModelProperty(value = "vehicle color")
	private String color;
	
	@ApiModelProperty(value = "vehicleBizType")
	@Pattern(regexp = "^1$|^2$|^3$|^4$|^$" , message = "vehicleBizType only 1(personal),2(ris),3(rent),4(ETC),''")
	private String vehicleBizType = "1";
	
	@ApiModelProperty(value = "vehicleRegDate - YYYY-MM-DD")
	@Pattern(regexp ="^\\d{4}-\\d{2}-\\d{2}$" , message = "Invalid vehicleRegDate")
	private String vehicleRegDate = "1970-01-01";
	
	@ApiModelProperty(value = "holidayPass")
	@Pattern(regexp = "^0$|^1$|^$" , message = "holidayPass only 1(ON),0(OFF)")
	private String holidayPass = "0";
	
	@ApiModelProperty(value = "approvalType")
	@Pattern(regexp = "^0$|^1$|^$" , message = "approvalType only 1(ON),0(OFF)")
	private String approvalType = "0";
	
	@ApiModelProperty(value = "approvalTypeKey 1 on group")
	private String approvalGroupKey;
	@ApiModelProperty(value = "approvalType 1 on account")
	private String approvalAccountKey;
	
	
	
	
	
	
	@ApiModelProperty(value = "vehicleBodyNum")
	private String vehicleBodyNum;
	
	@ApiModelProperty(value = "vehicleMaintenanceDate  - YYYY-MM-DD")
	@Pattern(regexp ="^\\d{4}-\\d{2}-\\d{2}$" , message = "Invalid vehicleMaintenanceDate")	
	private String vehicleMaintenanceDate = "1970-01-01";
	
	@ApiModelProperty(value = "vehiclePayment")	
	private int vehiclePayment = 0;
	
	@ApiModelProperty(value = "vehicleInsureNm")
	private String vehicleInsureNm;
	
	@ApiModelProperty(value = "vehicleInsurePhone")
	private String vehicleInsurePhone;
	
	@ApiModelProperty(value = "vehicleDescript")
	private String vehicleDescript;
	
	
	@ApiModelProperty(value = "connectionKey")
	private String connectionKey;

	@ApiModelProperty(value = "vehicleKey", notes="device change service")
	private String vehicleKey;
	
	@ApiModelProperty(hidden = true)
	@JsonIgnore
	private String corpKey;
	
	@ApiModelProperty(hidden = true)
	@JsonIgnore
	private String defaultAccountKey;
	
	@ApiModelProperty(hidden = true)
	@JsonIgnore
	private String deviceId;
	
	@ApiModelProperty(value = "lastestLocation")
	private String lastestLocation;
	
	@ApiModelProperty(value = "lastestLocationTime")
	private String lastestLocationTime;
	
	@ApiModelProperty(value = "lastestLocationAddr")
	private String lastestLocationAddr;
	
	@ApiModelProperty(value = "lastestLocationAddrEn")
	private String lastestLocationAddrEn;
	
	@ApiModelProperty(value = "lastestLocationAddrDetail")
	private String lastestLocationAddrDetail;
	
	@ApiModelProperty(value = "lastestTripKey")
	private String lastestTripKey;
	
	@ApiModelProperty(value = "lastestStartTripKey")
	private String lastestStartTripKey;
	
	@ApiModelProperty(value = "accureDistance")
	private int accureDistance;
	
	@ApiModelProperty(value = "regDate")
	private String regDate;
	
	@ApiModelProperty(value = "geofenceMonitor")
	private String geofenceMonitor;
	
	@ApiModelProperty(value = "totDistanceCollectionValue")
	private int totDistanceCollectionValue;

	public String getPlateNum() {
		return plateNum;
	}

	public void setPlateNum(String plateNum) {
		this.plateNum = plateNum;
	}

	public String getDeviceSn() {
		return deviceSn;
	}

	public void setDeviceSn(String deviceSn) {
		this.deviceSn = deviceSn;
	}

	public String getConvCode() {
		return convCode;
	}

	public void setConvCode(String convCode) {
		this.convCode = convCode;
	}

	public int getTotDistance() {
		return totDistance;
	}

	public void setTotDistance(int totDistance) {
		this.totDistance = totDistance;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getCorpGroupKey() {
		return corpGroupKey;
	}

	public void setCorpGroupKey(String corpGroupKey) {
		this.corpGroupKey = corpGroupKey;
	}

	public String getFixed() {
		return fixed;
	}

	public void setFixed(String fixed) {
		this.fixed = fixed;
	}

	public String getFixedAccountKey() {
		return fixedAccountKey;
	}

	public void setFixedAccountKey(String fixedAccountKey) {
		this.fixedAccountKey = fixedAccountKey;
	}

	public String getFixedDriverKey() {
		return fixedDriverKey;
	}

	public void setFixedDriverKey(String fixedDriverKey) {
		this.fixedDriverKey = fixedDriverKey;
	}

	public String getFixedContents() {
		return fixedContents;
	}

	public void setFixedContents(String fixedContents) {
		this.fixedContents = fixedContents;
	}

	public String getAutoReport() {
		return autoReport;
	}

	public void setAutoReport(String autoReport) {
		this.autoReport = autoReport;
	}

	public String getAutoReportPurpose() {
		return autoReportPurpose;
	}

	public void setAutoReportPurpose(String autoReportPurpose) {
		this.autoReportPurpose = autoReportPurpose;
	}

	public String getAutoReportContents() {
		return autoReportContents;
	}

	public void setAutoReportContents(String autoReportContents) {
		this.autoReportContents = autoReportContents;
	}

	public String getLocationSetting() {
		return locationSetting;
	}

	public void setLocationSetting(String locationSetting) {
		this.locationSetting = locationSetting;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getVehicleBizType() {
		return vehicleBizType;
	}

	public void setVehicleBizType(String vehicleBizType) {
		this.vehicleBizType = vehicleBizType;
	}

	public String getVehicleRegDate() {
		return vehicleRegDate;
	}

	public void setVehicleRegDate(String vehicleRegDate) {
		this.vehicleRegDate = vehicleRegDate;
	}

	public String getHolidayPass() {
		return holidayPass;
	}

	public void setHolidayPass(String holidayPass) {
		this.holidayPass = holidayPass;
	}

	public String getApprovalType() {
		return approvalType;
	}

	public void setApprovalType(String approvalType) {
		this.approvalType = approvalType;
	}

	public String getApprovalGroupKey() {
		return approvalGroupKey;
	}

	public void setApprovalGroupKey(String approvalGroupKey) {
		this.approvalGroupKey = approvalGroupKey;
	}

	public String getApprovalAccountKey() {
		return approvalAccountKey;
	}

	public void setApprovalAccountKey(String approvalAccountKey) {
		this.approvalAccountKey = approvalAccountKey;
	}

	public String getVehicleBodyNum() {
		return vehicleBodyNum;
	}

	public void setVehicleBodyNum(String vehicleBodyNum) {
		this.vehicleBodyNum = vehicleBodyNum;
	}

	public String getVehicleMaintenanceDate() {
		return vehicleMaintenanceDate;
	}

	public void setVehicleMaintenanceDate(String vehicleMaintenanceDate) {
		this.vehicleMaintenanceDate = vehicleMaintenanceDate;
	}

	public int getVehiclePayment() {
		return vehiclePayment;
	}

	public void setVehiclePayment(int vehiclePayment) {
		this.vehiclePayment = vehiclePayment;
	}

	public String getVehicleInsureNm() {
		return vehicleInsureNm;
	}

	public void setVehicleInsureNm(String vehicleInsureNm) {
		this.vehicleInsureNm = vehicleInsureNm;
	}

	public String getVehicleInsurePhone() {
		return vehicleInsurePhone;
	}

	public void setVehicleInsurePhone(String vehicleInsurePhone) {
		this.vehicleInsurePhone = vehicleInsurePhone;
	}

	public String getVehicleDescript() {
		return vehicleDescript;
	}

	public void setVehicleDescript(String vehicleDescript) {
		this.vehicleDescript = vehicleDescript;
	}

	public String getConnectionKey() {
		return connectionKey;
	}

	public void setConnectionKey(String connectionKey) {
		this.connectionKey = connectionKey;
	}

	public String getVehicleKey() {
		return vehicleKey;
	}

	public void setVehicleKey(String vehicleKey) {
		this.vehicleKey = vehicleKey;
	}

	public String getCorpKey() {
		return corpKey;
	}

	public void setCorpKey(String corpKey) {
		this.corpKey = corpKey;
	}

	public String getDefaultAccountKey() {
		return defaultAccountKey;
	}

	public void setDefaultAccountKey(String defaultAccountKey) {
		this.defaultAccountKey = defaultAccountKey;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

			
}
