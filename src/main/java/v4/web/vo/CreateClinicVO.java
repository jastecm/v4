package v4.web.vo;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;

public class CreateClinicVO {
	
	@JsonIgnore
	private String maintenanceKey;
	@JsonIgnore
	private String accountKey;
	@JsonIgnore
	private String corpKey;
	@JsonIgnore
	private String regDate;

	@ApiModelProperty(value = "vehicleKey", required = true)
	@NotEmpty(message = "vehicleKey is required")
	private String vehicleKey;
	
	@ApiModelProperty(value = "periodicInspection")
	private String periodicInspection;
	
	@ApiModelProperty(value = "nextPeriodicInspection")
	private String nextPeriodicInspection;
	
	@ApiModelProperty(value = "nextPeriodicInspection",required = true)
	@NotEmpty(message = "reqAccountKey is required")
	private String reqAccountKey;
	
	@ApiModelProperty(value = "groupKey",required = true)
	@NotEmpty(message = "groupKey is required")
	private String groupKey;
	
	@ApiModelProperty(value = "maintenanceReqDate")
	private String maintenanceReqDate;
	
	@ApiModelProperty(value = "completeReqDate")
	private String completeReqDate;
	
	@ApiModelProperty(value = "troubleDesc")
	private String troubleDesc;
	
	@ApiModelProperty(value = "clinicNm")
	private String clinicNm;
	
	@ApiModelProperty(value = "distance")
	private String distance;
	
	@ApiModelProperty(value = "clinicDate")
	private String clinicDate;
	
	@ApiModelProperty(value = "repairContent")
	private String repairContent;

	
	@JsonIgnore
	private String approvalState;
	@JsonIgnore
	private String approvalStep1Account;
	@JsonIgnore
	private String approvalStep1Date;
	@JsonIgnore
	private String approvalStep2Account;
	@JsonIgnore
	private String approvalStep2Date;
	
	@JsonIgnore
	private String maxStep;
	
	
	//detail
	private String partsKey;
	private String partsQuality;
	private String partsCnt;
	private String partsPay;
	private String wage;
	private String partsNm;
	
	
	
	public String getPartsNm() {
		return partsNm;
	}
	public void setPartsNm(String partsNm) {
		this.partsNm = partsNm;
	}
	public String getPartsKey() {
		return partsKey;
	}
	public void setPartsKey(String partsKey) {
		this.partsKey = partsKey;
	}
	public String getPartsQuality() {
		return partsQuality;
	}
	public void setPartsQuality(String partsQuality) {
		this.partsQuality = partsQuality;
	}
	public String getPartsCnt() {
		return partsCnt;
	}
	public void setPartsCnt(String partsCnt) {
		this.partsCnt = partsCnt;
	}
	public String getPartsPay() {
		return partsPay;
	}
	public void setPartsPay(String partsPay) {
		this.partsPay = partsPay;
	}
	public String getWage() {
		return wage;
	}
	public void setWage(String wage) {
		this.wage = wage;
	}
	public String getMaxStep() {
		return maxStep;
	}
	public void setMaxStep(String maxStep) {
		this.maxStep = maxStep;
	}
	public String getApprovalState() {
		return approvalState;
	}
	public void setApprovalState(String approvalState) {
		this.approvalState = approvalState;
	}
	public String getApprovalStep1Account() {
		return approvalStep1Account;
	}
	public void setApprovalStep1Account(String approvalStep1Account) {
		this.approvalStep1Account = approvalStep1Account;
	}
	public String getApprovalStep1Date() {
		return approvalStep1Date;
	}
	public void setApprovalStep1Date(String approvalStep1Date) {
		this.approvalStep1Date = approvalStep1Date;
	}
	public String getApprovalStep2Account() {
		return approvalStep2Account;
	}
	public void setApprovalStep2Account(String approvalStep2Account) {
		this.approvalStep2Account = approvalStep2Account;
	}
	public String getApprovalStep2Date() {
		return approvalStep2Date;
	}
	public void setApprovalStep2Date(String approvalStep2Date) {
		this.approvalStep2Date = approvalStep2Date;
	}
	public String getAccountKey() {
		return accountKey;
	}
	public void setAccountKey(String accountKey) {
		this.accountKey = accountKey;
	}
	public String getCorpKey() {
		return corpKey;
	}
	public void setCorpKey(String corpKey) {
		this.corpKey = corpKey;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getVehicleKey() {
		return vehicleKey;
	}
	public void setVehicleKey(String vehicleKey) {
		this.vehicleKey = vehicleKey;
	}
	public String getPeriodicInspection() {
		return periodicInspection;
	}
	public void setPeriodicInspection(String periodicInspection) {
		this.periodicInspection = periodicInspection;
	}
	public String getNextPeriodicInspection() {
		return nextPeriodicInspection;
	}
	public void setNextPeriodicInspection(String nextPeriodicInspection) {
		this.nextPeriodicInspection = nextPeriodicInspection;
	}
	public String getReqAccountKey() {
		return reqAccountKey;
	}
	public void setReqAccountKey(String reqAccountKey) {
		this.reqAccountKey = reqAccountKey;
	}
	public String getGroupKey() {
		return groupKey;
	}
	public void setGroupKey(String groupKey) {
		this.groupKey = groupKey;
	}
	public String getMaintenanceReqDate() {
		return maintenanceReqDate;
	}
	public void setMaintenanceReqDate(String maintenanceReqDate) {
		this.maintenanceReqDate = maintenanceReqDate;
	}
	public String getCompleteReqDate() {
		return completeReqDate;
	}
	public void setCompleteReqDate(String completeReqDate) {
		this.completeReqDate = completeReqDate;
	}
	public String getTroubleDesc() {
		return troubleDesc;
	}
	public void setTroubleDesc(String troubleDesc) {
		this.troubleDesc = troubleDesc;
	}
	public String getClinicNm() {
		return clinicNm;
	}
	public void setClinicNm(String clinicNm) {
		this.clinicNm = clinicNm;
	}
	public String getDistance() {
		return distance;
	}
	public void setDistance(String distance) {
		this.distance = distance;
	}
	public String getClinicDate() {
		return clinicDate;
	}
	public void setClinicDate(String clinicDate) {
		this.clinicDate = clinicDate;
	}
	public String getRepairContent() {
		return repairContent;
	}
	public void setRepairContent(String repairContent) {
		this.repairContent = repairContent;
	}
	public String getMaintenanceKey() {
		return maintenanceKey;
	}
	public void setMaintenanceKey(String maintenanceKey) {
		this.maintenanceKey = maintenanceKey;
	}
	
	
	
}
