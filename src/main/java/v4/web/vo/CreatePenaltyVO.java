package v4.web.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value="CreatePenaltyVO" , description="template create penalty value object")
@JsonIgnoreProperties(ignoreUnknown = true)
public @Data class CreatePenaltyVO {

	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String corpKey;
	
	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String accountKey;
	
	@ApiModelProperty(value = "vehicleKey" ,required = true)
	private String vehicleKey;
	
	@ApiModelProperty(value = "business")
	private String business;
	
	@ApiModelProperty(value = "crackDownDate")
	private String crackDownDate;
	
	@ApiModelProperty(value = "penaltyType")
	private String penaltyType;
	
	@ApiModelProperty(value = "violationType")
	private String violationType;
	
	@ApiModelProperty(value = "reason")
	private String reason;
	
	@ApiModelProperty(value = "location")
	private String location;
	
	@ApiModelProperty(value = "jurisdiction")
	private String jurisdiction;
	
	@ApiModelProperty(value = "billImg")
	private String billImg;
	
	@ApiModelProperty(value = "paymentType")
	private String paymentType;
	
	@ApiModelProperty(value = "deadLineDate")
	private String deadLineDate;
	
	
	
}
