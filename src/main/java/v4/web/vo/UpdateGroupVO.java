package v4.web.vo;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(value="UpdateGroupVO" , description="template create Group value object")
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateGroupVO {
	
	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String groupKey;
	
	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String corpKey;
	
	@ApiModelProperty(value = "group name",required = true)
	@NotEmpty(message = "groupNm.required")
	private String groupNm;
	
	@ApiModelProperty(value = "parent group key - if null or empty is lev 1")
	@NotEmpty(message = "parentGroupKey.required")
	private String parentGroupKey;
	
	@ApiModelProperty(value = "manager accountKey")
	@NotEmpty(message = "managerAccountKey.required")
	private String managerAccountKey;

	public String getGroupKey() {
		return groupKey;
	}

	public void setGroupKey(String groupKey) {
		this.groupKey = groupKey;
	}

	public String getCorpKey() {
		return corpKey;
	}

	public void setCorpKey(String corpKey) {
		this.corpKey = corpKey;
	}

	public String getGroupNm() {
		return groupNm;
	}

	public void setGroupNm(String groupNm) {
		this.groupNm = groupNm;
	}

	public String getParentGroupKey() {
		return parentGroupKey;
	}

	public void setParentGroupKey(String parentGroupKey) {
		this.parentGroupKey = parentGroupKey;
	}

	public String getManagerAccountKey() {
		return managerAccountKey;
	}

	public void setManagerAccountKey(String managerAccountKey) {
		this.managerAccountKey = managerAccountKey;
	}
	
	
	
}
