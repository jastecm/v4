package v4.web.vo;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RegistStep1VO {
	
	@ApiModelProperty(value = "accountId type email",required = true)
	@NotEmpty(message = "accountId is required")
	@Pattern(regexp = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$" , message = "userId is email type")
	private String accountId;
	
	@ApiModelProperty(required = true ,value = "accountPw")
	//@Pattern(regexp = "^(?=.*[a-zA-Z])(?=.*[!@#$%^\\&*\\(\\)\\_\\+\\|\\-\\=\\\\\\[\\];'?\\.\\,])(?=.*[0-9]).{8,20}$" , message = "bad userPw(size 8~20 , en , )")
	@Pattern(regexp = "^.{8,20}$" , message = "bad accountPw(size 8~20)")
	private String accountPw;

	
	public RegistStep1VO() {
		super();
	}

	

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}



	public String getAccountPw() {
		return accountPw;
	}



	public void setAccountPw(String accountPw) {
		this.accountPw = accountPw;
	}



	public RegistStep1VO(String accountId, String accountPw) {
		super();
		this.accountId = accountId;
		this.accountPw = accountPw;
	}


	


	

			
}
