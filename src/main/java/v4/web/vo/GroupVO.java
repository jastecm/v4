package v4.web.vo;

public class GroupVO {
	private String groupKey;
	private String corpKey;
	private String groupNm;
	private String parentGroupNm;
	private String managerAccountKey;
	private String parentGroupKey;
	private String regDate;
	
	private String managerName;
	
	private int userCount;
	private int vehicleCount;
	
	private String groupNmList;
	
	
	
	public String getGroupNmList() {
		return groupNmList;
	}
	public void setGroupNmList(String groupNmList) {
		this.groupNmList = groupNmList;
	}
	public String getParentGroupNm() {
		return parentGroupNm;
	}
	public void setParentGroupNm(String parentGroupNm) {
		this.parentGroupNm = parentGroupNm;
	}
	public String getManagerName() {
		return managerName;
	}
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
	public int getUserCount() {
		return userCount;
	}
	public void setUserCount(int userCount) {
		this.userCount = userCount;
	}
	public int getVehicleCount() {
		return vehicleCount;
	}
	public void setVehicleCount(int vehicleCount) {
		this.vehicleCount = vehicleCount;
	}
	public String getGroupKey() {
		return groupKey;
	}
	public void setGroupKey(String groupKey) {
		this.groupKey = groupKey;
	}
	public String getCorpKey() {
		return corpKey;
	}
	public void setCorpKey(String corpKey) {
		this.corpKey = corpKey;
	}
	public String getGroupNm() {
		return groupNm;
	}
	public void setGroupNm(String groupNm) {
		this.groupNm = groupNm;
	}
	public String getManagerAccountKey() {
		return managerAccountKey;
	}
	public void setManagerAccountKey(String managerAccountKey) {
		this.managerAccountKey = managerAccountKey;
	}
	public String getParentGroupKey() {
		return parentGroupKey;
	}
	public void setParentGroupKey(String parentGroupKey) {
		this.parentGroupKey = parentGroupKey;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	
	
}
