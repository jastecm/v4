package v4.web.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.jastecm.lora.vo.extention.LoraMsg_TDR;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TripRecordVO {
	
	private String recordId;
	private String deviceKey;
	private String vehicleKey;
	private long tripId;
	private int rpm;
	private int speed;
	private double lon;
	private double lat;
	private long gpsTime;
	private int suddenMark;
	private int distance;
	private int fco;
		
	private int rapidStart,rapidStop,rapidAccel,rapidDeaccel,rapidTurn,rapidUtern,overSpeed,overSpeedLong;
	
	private int encrypt;
	private String ver;
	
	private long shiftDay;
	
	
	public TripRecordVO() {super();}


	public TripRecordVO(String deviceKey, String vehicleKey, long tripId, int rpm, int speed,
			double lon, double lat, long gpsTime, int suddenMark, int distance, int fco, int rapidStart,
			int rapidStop, int rapidAccel, int rapidDeaccel, int rapidTurn, int rapidUtern, int overSpeed,
			int overSpeedLong, int encrypt, String ver, long shiftDay) {
		super();
		this.deviceKey = deviceKey;
		this.vehicleKey = vehicleKey;
		this.tripId = tripId;
		this.rpm = rpm;
		this.speed = speed;
		this.lon = lon;
		this.lat = lat;
		this.gpsTime = gpsTime;
		this.suddenMark = suddenMark;
		this.distance = distance;
		this.fco = fco;
		this.rapidStart = rapidStart;
		this.rapidStop = rapidStop;
		this.rapidAccel = rapidAccel;
		this.rapidDeaccel = rapidDeaccel;
		this.rapidTurn = rapidTurn;
		this.rapidUtern = rapidUtern;
		this.overSpeed = overSpeed;
		this.overSpeedLong = overSpeedLong;
		this.encrypt = encrypt;
		this.ver = ver;
		this.shiftDay = shiftDay;
	}


	@Override
	public String toString() {
		return "TripRecordVO [recordId=" + recordId + ", deviceKey=" + deviceKey + ", vehicleKey=" + vehicleKey
				+ ", tripId=" + tripId + ", rpm=" + rpm + ", speed=" + speed + ", lon=" + lon + ", lat=" + lat
				+ ", gpsTime=" + gpsTime + ", suddenMark=" + suddenMark + ", distance=" + distance + ", fco=" + fco
				+ ", rapidStart=" + rapidStart + ", rapidStop=" + rapidStop + ", rapidAccel=" + rapidAccel
				+ ", rapidDeaccel=" + rapidDeaccel + ", rapidTurn=" + rapidTurn + ", rapidUtern=" + rapidUtern
				+ ", overSpeed=" + overSpeed + ", overSpeedLong=" + overSpeedLong + ", encrypt=" + encrypt + ", ver="
				+ ver + ", shiftDay=" + shiftDay + "]";
	}


	public String getRecordId() {
		return recordId;
	}


	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}


	public String getDeviceKey() {
		return deviceKey;
	}


	public void setDeviceKey(String deviceKey) {
		this.deviceKey = deviceKey;
	}


	public String getVehicleKey() {
		return vehicleKey;
	}


	public void setVehicleKey(String vehicleKey) {
		this.vehicleKey = vehicleKey;
	}


	public long getTripId() {
		return tripId;
	}


	public void setTripId(long tripId) {
		this.tripId = tripId;
	}



	public int getRpm() {
		return rpm;
	}


	public void setRpm(int rpm) {
		this.rpm = rpm;
	}


	public int getSpeed() {
		return speed;
	}


	public void setSpeed(int speed) {
		this.speed = speed;
	}


	public double getLon() {
		return lon;
	}


	public void setLon(double lon) {
		this.lon = lon;
	}


	public double getLat() {
		return lat;
	}


	public void setLat(double lat) {
		this.lat = lat;
	}


	
	public long getGpsTime() {
		return gpsTime;
	}


	public void setGpsTime(long gpsTime) {
		this.gpsTime = gpsTime;
	}


	public int getSuddenMark() {
		return suddenMark;
	}


	public void setSuddenMark(int suddenMark) {
		this.suddenMark = suddenMark;
	}


	public int getDistance() {
		return distance;
	}


	public void setDistance(int distance) {
		this.distance = distance;
	}


	public int getFco() {
		return fco;
	}


	public void setFco(int fco) {
		this.fco = fco;
	}


	public int getRapidStart() {
		return rapidStart;
	}


	public void setRapidStart(int rapidStart) {
		this.rapidStart = rapidStart;
	}


	public int getRapidStop() {
		return rapidStop;
	}


	public void setRapidStop(int rapidStop) {
		this.rapidStop = rapidStop;
	}


	public int getRapidAccel() {
		return rapidAccel;
	}


	public void setRapidAccel(int rapidAccel) {
		this.rapidAccel = rapidAccel;
	}


	public int getRapidDeaccel() {
		return rapidDeaccel;
	}


	public void setRapidDeaccel(int rapidDeaccel) {
		this.rapidDeaccel = rapidDeaccel;
	}


	public int getRapidTurn() {
		return rapidTurn;
	}


	public void setRapidTurn(int rapidTurn) {
		this.rapidTurn = rapidTurn;
	}


	public int getRapidUtern() {
		return rapidUtern;
	}


	public void setRapidUtern(int rapidUtern) {
		this.rapidUtern = rapidUtern;
	}


	public int getOverSpeed() {
		return overSpeed;
	}


	public void setOverSpeed(int overSpeed) {
		this.overSpeed = overSpeed;
	}


	public int getOverSpeedLong() {
		return overSpeedLong;
	}


	public void setOverSpeedLong(int overSpeedLong) {
		this.overSpeedLong = overSpeedLong;
	}


	public int getEncrypt() {
		return encrypt;
	}


	public void setEncrypt(int encrypt) {
		this.encrypt = encrypt;
	}


	public String getVer() {
		return ver;
	}


	public void setVer(String ver) {
		this.ver = ver;
	}


	public long getShiftDay() {
		return shiftDay;
	}


	public void setShiftDay(long shiftDay) {
		this.shiftDay = shiftDay;
	}


	
		
}
