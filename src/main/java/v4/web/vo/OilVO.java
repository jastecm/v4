package v4.web.vo;


/**
 * @Class Name : FileVO.java
 * @Description : 파일정보 처리를 위한 VO 클래스
 * @Modification Information
 *
 *    수정일       수정자         수정내용
 *    -------        -------     -------------------
 *    2009. 3. 25.     이삼섭
 *
 * @author 공통 서비스 개발팀 이삼섭
 * @since 2009. 3. 25.
 * @version
 * @see
 *
 */
public class OilVO {

    private String tradeDt;
    private String gasolineHigh;
    private String gasoline;
    private String diesel;
    private String lpg;
    
    
	public String getLpg() {
		return lpg;
	}
	public void setLpg(String lpg) {
		this.lpg = lpg;
	}
	public String getTradeDt() {
		return tradeDt;
	}
	public void setTradeDt(String tradeDt) {
		this.tradeDt = tradeDt;
	}
	public String getGasolineHigh() {
		return gasolineHigh;
	}
	public void setGasolineHigh(String gasolineHigh) {
		this.gasolineHigh = gasolineHigh;
	}
	public String getGasoline() {
		return gasoline;
	}
	public void setGasoline(String gasoline) {
		this.gasoline = gasoline;
	}
	public String getDiesel() {
		return diesel;
	}
	public void setDiesel(String diesel) {
		this.diesel = diesel;
	}
    
    
}

