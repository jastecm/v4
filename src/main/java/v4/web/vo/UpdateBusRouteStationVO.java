package v4.web.vo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value="UpdateBusRouteStationVO" , description="template UpdateBusRouteStationVO value object")
@JsonIgnoreProperties(ignoreUnknown = true)
public @Data class UpdateBusRouteStationVO {

	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String corp;
	
	@ApiModelProperty(value = "busRouteKey",required = true)
	private String busRouteKey;
	
	@ApiModelProperty(value = "drivingPurpose",required = true)
	private String drivingPurpose;
	
	@ApiModelProperty(value = "busRouteName ",required = true)
	private String busRouteName;
	
	@ApiModelProperty(value = "busRouteImg")
	private String busRouteImg;
	
	@ApiModelProperty(value = "busRouteKeyword")
	private String busRouteKeyword;
	
	@ApiModelProperty(value = "stationName",required = true)
	private List<String> stationName;
	
	@ApiModelProperty(value = "arrivalTime",required = true)
	private List<String> arrivalTime;
	
	@ApiModelProperty(value = "stationImg")
	private List<String> stationImg;
	
	@ApiModelProperty(value = "address",required = true)
	private List<String> address;
	
	@ApiModelProperty(value = "lon",required = true)
	private List<String> lon;
	
	@ApiModelProperty(value = "lat",required = true)
	private List<String> lat;
}
