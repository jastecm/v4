package v4.web.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class DownloadVO {
	@JsonIgnore
	private String osType;
	@JsonIgnore
	private String deviceSeries;
	@JsonIgnore
	private String model;
	@JsonIgnore
	private String bcmSupport;
	@JsonIgnore
	private String ios, android, genelicVer, genelicBusVer;
	@JsonIgnore
	private String s31,f31;
	
	private String firmware;
	private String appVer;
	private String vehicleVer;
	private String dbType;
	
	
	public String getF31() {
		return f31;
	}
	public void setF31(String f31) {
		this.f31 = f31;
	}
	public String getGenelicBusVer() {
		return genelicBusVer;
	}
	public void setGenelicBusVer(String genelicBusVer) {
		this.genelicBusVer = genelicBusVer;
	}
	public String getDbType() {
		return dbType;
	}
	public void setDbType(String dbType) {
		this.dbType = dbType;
	}
	public String getOsType() {
		return osType;
	}
	public void setOsType(String osType) {
		this.osType = osType;
	}
	public String getDeviceSeries() {
		return deviceSeries;
	}
	public void setDeviceSeries(String deviceSeries) {
		this.deviceSeries = deviceSeries;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getBcmSupport() {
		return bcmSupport;
	}
	public void setBcmSupport(String bcmSupport) {
		this.bcmSupport = bcmSupport;
	}
	public String getIos() {
		return ios;
	}
	public void setIos(String ios) {
		this.ios = ios;
	}
	public String getAndroid() {
		return android;
	}
	public void setAndroid(String android) {
		this.android = android;
	}
	public String getGenelicVer() {
		return genelicVer;
	}
	public void setGenelicVer(String genelicVer) {
		this.genelicVer = genelicVer;
	}
	public String getS31() {
		return s31;
	}
	public void setS31(String s31) {
		this.s31 = s31;
	}
	public String getFirmware() {
		return firmware;
	}
	public void setFirmware(String firmware) {
		this.firmware = firmware;
	}
	public String getAppVer() {
		return appVer;
	}
	public void setAppVer(String appVer) {
		this.appVer = appVer;
	}
	public String getVehicleVer() {
		return vehicleVer;
	}
	public void setVehicleVer(String vehicleVer) {
		this.vehicleVer = vehicleVer;
	}
	
		
}
