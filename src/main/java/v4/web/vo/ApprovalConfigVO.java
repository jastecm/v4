package v4.web.vo;

public class ApprovalConfigVO {
	private String allocateApproval;
	private String allocateApprovalStep1;
	private String allocateApprovalStep1Name;
	private String allocateApprovalStep1GroupName;
	private String allocateApprovalStep2;
	private String allocateApprovalStep2Name;
	private String allocateApprovalStep2GroupName;
	private String autoAllocateApprovalStep1;
	private String autoAllocateApprovalStep2;
	
	private String maintenanceApproval;
	private String maintenanceApprovalStep1;
	private String maintenanceApprovalStep1Name;
	private String maintenanceApprovalStep1GroupName;
	private String maintenanceApprovalStep2;
	private String maintenanceApprovalStep2Name;
	private String maintenanceApprovalStep2GroupName;
	private String autoMaintenanceApprovalStep1;
	private String autoMaintenanceApprovalStep2;
	
	private String expensesApproval;
	private String expensesApprovalStep1;
	private String expensesApprovalStep1Name;
	private String expensesApprovalStep1GroupName;
	private String expensesApprovalStep2;
	private String expensesApprovalStep2Name;
	private String expensesApprovalStep2GroupName;
	private String autoExpensesApprovalStep1;
	private String autoExpensesApprovalStep2;
	
	private String accidentApproval;
	private String accidentApprovalStep1;
	private String accidentApprovalStep1Name;
	private String accidentApprovalStep1GroupName;
	private String accidentApprovalStep2;
	private String accidentApprovalStep2Name;
	private String accidentApprovalStep2GroupName;
	private String accidentApprovalStep3;
	private String accidentApprovalStep3Name;
	private String accidentApprovalStep3GroupName;
	
	
	public String getAllocateApprovalStep1GroupName() {
		return allocateApprovalStep1GroupName;
	}
	public void setAllocateApprovalStep1GroupName(String allocateApprovalStep1GroupName) {
		this.allocateApprovalStep1GroupName = allocateApprovalStep1GroupName;
	}
	public String getAllocateApprovalStep2GroupName() {
		return allocateApprovalStep2GroupName;
	}
	public void setAllocateApprovalStep2GroupName(String allocateApprovalStep2GroupName) {
		this.allocateApprovalStep2GroupName = allocateApprovalStep2GroupName;
	}
	public String getMaintenanceApprovalStep1GroupName() {
		return maintenanceApprovalStep1GroupName;
	}
	public void setMaintenanceApprovalStep1GroupName(String maintenanceApprovalStep1GroupName) {
		this.maintenanceApprovalStep1GroupName = maintenanceApprovalStep1GroupName;
	}
	public String getMaintenanceApprovalStep2GroupName() {
		return maintenanceApprovalStep2GroupName;
	}
	public void setMaintenanceApprovalStep2GroupName(String maintenanceApprovalStep2GroupName) {
		this.maintenanceApprovalStep2GroupName = maintenanceApprovalStep2GroupName;
	}
	public String getExpensesApprovalStep1GroupName() {
		return expensesApprovalStep1GroupName;
	}
	public void setExpensesApprovalStep1GroupName(String expensesApprovalStep1GroupName) {
		this.expensesApprovalStep1GroupName = expensesApprovalStep1GroupName;
	}
	public String getExpensesApprovalStep2GroupName() {
		return expensesApprovalStep2GroupName;
	}
	public void setExpensesApprovalStep2GroupName(String expensesApprovalStep2GroupName) {
		this.expensesApprovalStep2GroupName = expensesApprovalStep2GroupName;
	}
	public String getAccidentApprovalStep1GroupName() {
		return accidentApprovalStep1GroupName;
	}
	public void setAccidentApprovalStep1GroupName(String accidentApprovalStep1GroupName) {
		this.accidentApprovalStep1GroupName = accidentApprovalStep1GroupName;
	}
	public String getAccidentApprovalStep2GroupName() {
		return accidentApprovalStep2GroupName;
	}
	public void setAccidentApprovalStep2GroupName(String accidentApprovalStep2GroupName) {
		this.accidentApprovalStep2GroupName = accidentApprovalStep2GroupName;
	}
	public String getAccidentApprovalStep3GroupName() {
		return accidentApprovalStep3GroupName;
	}
	public void setAccidentApprovalStep3GroupName(String accidentApprovalStep3GroupName) {
		this.accidentApprovalStep3GroupName = accidentApprovalStep3GroupName;
	}
	public String getAllocateApproval() {
		return allocateApproval;
	}
	public void setAllocateApproval(String allocateApproval) {
		this.allocateApproval = allocateApproval;
	}
	public String getAllocateApprovalStep1() {
		return allocateApprovalStep1;
	}
	public void setAllocateApprovalStep1(String allocateApprovalStep1) {
		this.allocateApprovalStep1 = allocateApprovalStep1;
	}
	public String getAllocateApprovalStep1Name() {
		return allocateApprovalStep1Name;
	}
	public void setAllocateApprovalStep1Name(String allocateApprovalStep1Name) {
		this.allocateApprovalStep1Name = allocateApprovalStep1Name;
	}
	public String getAllocateApprovalStep2() {
		return allocateApprovalStep2;
	}
	public void setAllocateApprovalStep2(String allocateApprovalStep2) {
		this.allocateApprovalStep2 = allocateApprovalStep2;
	}
	public String getAllocateApprovalStep2Name() {
		return allocateApprovalStep2Name;
	}
	public void setAllocateApprovalStep2Name(String allocateApprovalStep2Name) {
		this.allocateApprovalStep2Name = allocateApprovalStep2Name;
	}
	public String getAutoAllocateApprovalStep1() {
		return autoAllocateApprovalStep1;
	}
	public void setAutoAllocateApprovalStep1(String autoAllocateApprovalStep1) {
		this.autoAllocateApprovalStep1 = autoAllocateApprovalStep1;
	}
	public String getAutoAllocateApprovalStep2() {
		return autoAllocateApprovalStep2;
	}
	public void setAutoAllocateApprovalStep2(String autoAllocateApprovalStep2) {
		this.autoAllocateApprovalStep2 = autoAllocateApprovalStep2;
	}
	public String getMaintenanceApproval() {
		return maintenanceApproval;
	}
	public void setMaintenanceApproval(String maintenanceApproval) {
		this.maintenanceApproval = maintenanceApproval;
	}
	public String getMaintenanceApprovalStep1() {
		return maintenanceApprovalStep1;
	}
	public void setMaintenanceApprovalStep1(String maintenanceApprovalStep1) {
		this.maintenanceApprovalStep1 = maintenanceApprovalStep1;
	}
	public String getMaintenanceApprovalStep1Name() {
		return maintenanceApprovalStep1Name;
	}
	public void setMaintenanceApprovalStep1Name(String maintenanceApprovalStep1Name) {
		this.maintenanceApprovalStep1Name = maintenanceApprovalStep1Name;
	}
	public String getMaintenanceApprovalStep2() {
		return maintenanceApprovalStep2;
	}
	public void setMaintenanceApprovalStep2(String maintenanceApprovalStep2) {
		this.maintenanceApprovalStep2 = maintenanceApprovalStep2;
	}
	public String getMaintenanceApprovalStep2Name() {
		return maintenanceApprovalStep2Name;
	}
	public void setMaintenanceApprovalStep2Name(String maintenanceApprovalStep2Name) {
		this.maintenanceApprovalStep2Name = maintenanceApprovalStep2Name;
	}
	public String getAutoMaintenanceApprovalStep1() {
		return autoMaintenanceApprovalStep1;
	}
	public void setAutoMaintenanceApprovalStep1(String autoMaintenanceApprovalStep1) {
		this.autoMaintenanceApprovalStep1 = autoMaintenanceApprovalStep1;
	}
	public String getAutoMaintenanceApprovalStep2() {
		return autoMaintenanceApprovalStep2;
	}
	public void setAutoMaintenanceApprovalStep2(String autoMaintenanceApprovalStep2) {
		this.autoMaintenanceApprovalStep2 = autoMaintenanceApprovalStep2;
	}
	public String getExpensesApproval() {
		return expensesApproval;
	}
	public void setExpensesApproval(String expensesApproval) {
		this.expensesApproval = expensesApproval;
	}
	public String getExpensesApprovalStep1() {
		return expensesApprovalStep1;
	}
	public void setExpensesApprovalStep1(String expensesApprovalStep1) {
		this.expensesApprovalStep1 = expensesApprovalStep1;
	}
	public String getExpensesApprovalStep1Name() {
		return expensesApprovalStep1Name;
	}
	public void setExpensesApprovalStep1Name(String expensesApprovalStep1Name) {
		this.expensesApprovalStep1Name = expensesApprovalStep1Name;
	}
	public String getExpensesApprovalStep2() {
		return expensesApprovalStep2;
	}
	public void setExpensesApprovalStep2(String expensesApprovalStep2) {
		this.expensesApprovalStep2 = expensesApprovalStep2;
	}
	public String getExpensesApprovalStep2Name() {
		return expensesApprovalStep2Name;
	}
	public void setExpensesApprovalStep2Name(String expensesApprovalStep2Name) {
		this.expensesApprovalStep2Name = expensesApprovalStep2Name;
	}
	public String getAutoExpensesApprovalStep1() {
		return autoExpensesApprovalStep1;
	}
	public void setAutoExpensesApprovalStep1(String autoExpensesApprovalStep1) {
		this.autoExpensesApprovalStep1 = autoExpensesApprovalStep1;
	}
	public String getAutoExpensesApprovalStep2() {
		return autoExpensesApprovalStep2;
	}
	public void setAutoExpensesApprovalStep2(String autoExpensesApprovalStep2) {
		this.autoExpensesApprovalStep2 = autoExpensesApprovalStep2;
	}
	public String getAccidentApproval() {
		return accidentApproval;
	}
	public void setAccidentApproval(String accidentApproval) {
		this.accidentApproval = accidentApproval;
	}
	public String getAccidentApprovalStep1() {
		return accidentApprovalStep1;
	}
	public void setAccidentApprovalStep1(String accidentApprovalStep1) {
		this.accidentApprovalStep1 = accidentApprovalStep1;
	}
	public String getAccidentApprovalStep1Name() {
		return accidentApprovalStep1Name;
	}
	public void setAccidentApprovalStep1Name(String accidentApprovalStep1Name) {
		this.accidentApprovalStep1Name = accidentApprovalStep1Name;
	}
	public String getAccidentApprovalStep2() {
		return accidentApprovalStep2;
	}
	public void setAccidentApprovalStep2(String accidentApprovalStep2) {
		this.accidentApprovalStep2 = accidentApprovalStep2;
	}
	public String getAccidentApprovalStep2Name() {
		return accidentApprovalStep2Name;
	}
	public void setAccidentApprovalStep2Name(String accidentApprovalStep2Name) {
		this.accidentApprovalStep2Name = accidentApprovalStep2Name;
	}
	public String getAccidentApprovalStep3() {
		return accidentApprovalStep3;
	}
	public void setAccidentApprovalStep3(String accidentApprovalStep3) {
		this.accidentApprovalStep3 = accidentApprovalStep3;
	}
	public String getAccidentApprovalStep3Name() {
		return accidentApprovalStep3Name;
	}
	public void setAccidentApprovalStep3Name(String accidentApprovalStep3Name) {
		this.accidentApprovalStep3Name = accidentApprovalStep3Name;
	}
	
	
	
}
