package v4.web.vo;

import io.swagger.annotations.ApiModelProperty;

public class CreateWorkingAreaVO {
	
	@ApiModelProperty(value = "groupkey")
	private String groupkey;
	
	@ApiModelProperty(value = "accountKey")
	private String accountKey;
	
	@ApiModelProperty(value = "cityLev1")
	private String cityLev1;
	
	@ApiModelProperty(value = "cityLev2")
	private String cityLev2;

	public String getGroupkey() {
		return groupkey;
	}

	public void setGroupkey(String groupkey) {
		this.groupkey = groupkey;
	}

	public String getAccountKey() {
		return accountKey;
	}

	public void setAccountKey(String accountKey) {
		this.accountKey = accountKey;
	}

	public String getCityLev1() {
		return cityLev1;
	}

	public void setCityLev1(String cityLev1) {
		this.cityLev1 = cityLev1;
	}

	public String getCityLev2() {
		return cityLev2;
	}

	public void setCityLev2(String cityLev2) {
		this.cityLev2 = cityLev2;
	}
	
	
	
}
