package v4.web.vo;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;

public class InviteStep1VO {
	@ApiModelProperty(value = "accountId type email",required = true)
	@NotEmpty(message = "accountId is required")
	@Pattern(regexp = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$" , message = "userId is email type")
	private String accountId;
	
	@ApiModelProperty(hidden=true)
	private String corpKey;

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getCorpKey() {
		return corpKey;
	}

	public void setCorpKey(String corpKey) {
		this.corpKey = corpKey;
	}

	public InviteStep1VO(String accountId, String corpKey) {
		super();
		this.accountId = accountId;
		this.corpKey = corpKey;
	}

	public InviteStep1VO() {
		super();
	}

	
}
