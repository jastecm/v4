package v4.web.vo;

public class CreateWorkingTimeVO {
	private String groupKey;
	private String accountKey;
	private String startH;
	private String startM;
	private String endH;
	private String endM;
	private String day;
	public String getGroupKey() {
		return groupKey;
	}
	public void setGroupKey(String groupKey) {
		this.groupKey = groupKey;
	}
	public String getAccountKey() {
		return accountKey;
	}
	public void setAccountKey(String accountKey) {
		this.accountKey = accountKey;
	}
	public String getStartH() {
		return startH;
	}
	public void setStartH(String startH) {
		this.startH = startH;
	}
	public String getStartM() {
		return startM;
	}
	public void setStartM(String startM) {
		this.startM = startM;
	}
	public String getEndH() {
		return endH;
	}
	public void setEndH(String endH) {
		this.endH = endH;
	}
	public String getEndM() {
		return endM;
	}
	public void setEndM(String endM) {
		this.endM = endM;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	
	
}
