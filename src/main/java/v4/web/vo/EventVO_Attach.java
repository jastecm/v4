package v4.web.vo;

public class EventVO_Attach extends EventVO_Abstract implements EventVO{
	
	public EventVO_Attach(){
		super.setEventType("attach");
	}
	 
	private String eventKey;
	private String eventType;
	private String eventTypeSub;
	private String eventTime;
	private String eventValue;
	private String eventNm;
	
	
	public String getEventTypeSub() {
		return eventTypeSub;
	}
	public void setEventTypeSub(String eventTypeSub) {
		this.eventTypeSub = eventTypeSub;
	}
	public String getEventKey() {
		return eventKey;
	}
	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public String getEventTime() {
		return eventTime;
	}
	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}
	public String getEventValue() {
		return eventValue;
	}
	public void setEventValue(String eventValue) {
		this.eventValue = eventValue;
	}
	public String getEventNm() {
		return eventNm;
	}
	public void setEventNm(String eventNm) {
		this.eventNm = eventNm;
	}
	
	public long getSortDate() {
		return sortDate;
	}
	public void setSortDate(long sortDate) {
		this.sortDate = sortDate;
	}	
	
	
	
}
