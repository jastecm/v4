package v4.web.vo;

public class VehiclePartsEtcVO {
	private String seq;
	private String corpKey;
	private String partsKey;
	private String partsNm;
	
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getCorpKey() {
		return corpKey;
	}
	public void setCorpKey(String corpKey) {
		this.corpKey = corpKey;
	}
	public String getPartsKey() {
		return partsKey;
	}
	public void setPartsKey(String partsKey) {
		this.partsKey = partsKey;
	}
	public String getPartsNm() {
		return partsNm;
	}
	public void setPartsNm(String partsNm) {
		this.partsNm = partsNm;
	}
	
	
}
