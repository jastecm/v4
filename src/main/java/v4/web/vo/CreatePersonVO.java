package v4.web.vo;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(value="CreatePersonVO" , description="template create Persnal user value object")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreatePersonVO {
	
	////////////////////////step1 회원 분류 선택///////////////////////////////////
	@ApiModelProperty(value = "accountId type email",required = true)
	@NotEmpty(message = "userId is required")
	@Pattern(regexp = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$" , message = "userId is email type")
	private String accountId;
	
	@ApiModelProperty(required = true ,value = "accountPw")
	//@Pattern(regexp = "^(?=.*[a-zA-Z])(?=.*[!@#$%^\\&*\\(\\)\\_\\+\\|\\-\\=\\\\\\[\\];'?\\.\\,])(?=.*[0-9]).{8,20}$" , message = "bad userPw(size 8~20 , en , )")
	@Pattern(regexp = "^.{8,20}$" , message = "bad accountPw(size 8~20)")
	private String accountPw;
	
	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String corpType = "1";
	//////////////////////step1 회원 분류 선택///////////////////////////////////
	
	
	//////////////////////step2 개인정보 활용 동의///////////////////////////////////	
	@ApiModelProperty(value = "smsAccepted" , allowableValues = "1,0" , required = true)
	@Pattern(regexp ="^1$|^0$" , message = "Invalid accept type (1,0)")
	@NotEmpty(message = "smsAccepted.required")
	private String smsAccepted = "1";
	
	@ApiModelProperty(value = "locationAccepted" , allowableValues = "1,0" , required = true)
	@Pattern(regexp ="^1$|^0$" , message = "Invalid accept type (1,0)")
	@NotEmpty(message = "locationAccepted.required")
	private String locationAccepted;
	
	@ApiModelProperty(value = "privateAccepted" , allowableValues = "1,0" , required = true)
	@Pattern(regexp ="^1$|^0$" , message = "Invalid accept type (1,0)")
	@NotEmpty(message = "privateAccepted.required")
	private String privateAccepted;

	@ApiModelProperty(value = "marketingAccepted" , allowableValues = "1,0" , required = true)
	@Pattern(regexp ="^1$|^0$" , message = "Invalid accept type (1,0)")
	@NotEmpty(message = "marketingAccepted.required")
	private String marketingAccepted;
	
	@ApiModelProperty(value = "serviceAccepted" , allowableValues = "1,0" , required = true)
	@Pattern(regexp ="^1$|^0$" , message = "Invalid accept type (1,0)")
	@NotEmpty(message = "serviceAccepted.required")
	private String serviceAccepted;
	//////////////////////step2 개인정보 활용 동의///////////////////////////////////	
	
	//////////////////////step3 국가 타임존 선택화면///////////////////////////////////
	@ApiModelProperty(value = "display languege" , example = "ko,en" , allowableValues = "ko,en")
	@Pattern(regexp ="^ko$|^en$|^$" , message = "can only ko,en")	
	private String lang = "ko";
	
	@ApiModelProperty(value = "timezoneOffset" , example = "-720 ~ 840")
	@Range(min=-720,max=840)
	private int timezoneOffset = 540;
	
	@ApiModelProperty(required = true ,value = "countryCode" , example = "82")
	@NotEmpty(message = "ctyCode is required")
	@Range(min=1 , max=999 , message = "ctyCode's range 1~999")
	private String ctyCode;
	
	@ApiModelProperty(value = "unitLength" , example = "km,mi")
	@Pattern(regexp ="^km$|^mi$" , message = "can only km,mi(mile)")
	private String unitLength = "km";
	
	@ApiModelProperty(value = "unitVolume" , example = "l(liter),gal(gallon)")
	@Pattern(regexp ="^l$|^gal$" , message = "can only l,gal")
	private String unitVolume = "l";
	
	@ApiModelProperty(value = "unitTemperature" , example = "C,F")
	@Pattern(regexp ="^C$|^F$" , message = "can only C,F")	
	private String unitTemperature = "C";
	
	@ApiModelProperty(value = "unitCurrency" , example = "WON,USD")
	@Pattern(regexp ="^WON$|^USD$" , message = "can only WON,USD")
	private String unitCurrency = "WON";
	
	@ApiModelProperty(value = "unitWeight" , example = "g,oz")
	@Pattern(regexp ="^g$|^oz$" , message = "can only g(gram),oz(ounce)")
	private String unitWeight = "g";
	
	@ApiModelProperty(value = "unitDate" , example = "YMD,MDY,DMY")
	@Pattern(regexp ="^YMD$|^MDY$|^DMY&" , message = "can only YMD,MDY,DMY")
	private String unitDate = "YMD";
	//////////////////////step3 국가 타임존 선택화면///////////////////////////////////
	
	
	//////////////////////step4 개인정보 기본정보, 부가정보///////////////////////////////////
	@ApiModelProperty(required = true , value = "user name" )
	@NotEmpty(message = "name is required")
	private String name;
	
	//addr 주소
	@ApiModelProperty(value = "address")
	private String addr;
		
	@ApiModelProperty(required = true ,value = "mobile phone number")
	@NotEmpty(message = "mobilePhone is required")
	@Pattern(regexp ="^01[0-9]{8,9}$" , message = "Invalid mobilePhone")
	private String mobilePhone;
	
	//국가번호(+82) +1 ~ +999
	@ApiModelProperty(required = true ,value = "international dialling codes - mobilePhone")
	@NotEmpty(message = "mobilePhoneCtyCode is required")
	@Pattern(regexp ="^\\+[0-9]{1,3}" , message = "Invalid mobilePhoneCtyCode")
	private String mobilePhoneCtyCode;
	
	//일반연락처
	@ApiModelProperty(value = "landline number")
	@Pattern(regexp ="^0[0-9]{8,10}$|^$" , message = "Invalid phone")
	private String phone;
	
	//국가번호(+82) +1 ~ +999
	@ApiModelProperty(value = "international dialling codes - phone")
	@Pattern(regexp ="^\\+[0-9]{1,3}" , message = "Invalid phoneCtyCode")
	private String phoneCtyCode;
	
	//생년월일
	@ApiModelProperty(value = "birthday" , example = "unixTimestamp")	
	private Long birth;
	
	//혈액형
	@ApiModelProperty(value = "blood type" , example = "A,B,O,AB")
	@Pattern(regexp ="^A$|^B$|^O$|^AB$|^$" , message = "Invalid blood type (A,B,O,AB)")
	private String blood;
	
	//혈액타입?
	@ApiModelProperty(value = "blood rh type" , example = "rh+,rh-")
	@Pattern(regexp ="^rh\\+$|^rh\\-$|^$" , message = "Invalid blood type (rh+,rh-)")
	private String bloodRh;
	
	//성별
	@ApiModelProperty(value = "gender" , example = "M,F")
	@Pattern(regexp ="^M$|^F$" , message = "Invalid gender type (M,F)")
	@NotEmpty(message = "gender is required")
	private String gender;

	//직급
	@ApiModelProperty(value = "Corporation Position")
	private String corpPosition;
	//////////////////////step4 개인정보 기본정보, 부가정보///////////////////////////////////

	//이하 히든정보
	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String corpKey;
	
	@ApiModelProperty(hidden=true)
	private String corpName = "개인";
	
	@ApiModelProperty(hidden=true)
	private String allocateionUsed = "0";
	
	@ApiModelProperty(hidden=true)
	private String partnerType;
			
	@ApiModelProperty(hidden=true)
	private String serviceGrade = "1";

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getAccountPw() {
		return accountPw;
	}

	public void setAccountPw(String accountPw) {
		this.accountPw = accountPw;
	}

	public String getCorpType() {
		return corpType;
	}

	public void setCorpType(String corpType) {
		this.corpType = corpType;
	}

	public String getSmsAccepted() {
		return smsAccepted;
	}

	public void setSmsAccepted(String smsAccepted) {
		this.smsAccepted = smsAccepted;
	}

	public String getLocationAccepted() {
		return locationAccepted;
	}

	public void setLocationAccepted(String locationAccepted) {
		this.locationAccepted = locationAccepted;
	}

	public String getPrivateAccepted() {
		return privateAccepted;
	}

	public void setPrivateAccepted(String privateAccepted) {
		this.privateAccepted = privateAccepted;
	}

	public String getMarketingAccepted() {
		return marketingAccepted;
	}

	public void setMarketingAccepted(String marketingAccepted) {
		this.marketingAccepted = marketingAccepted;
	}

	public String getServiceAccepted() {
		return serviceAccepted;
	}

	public void setServiceAccepted(String serviceAccepted) {
		this.serviceAccepted = serviceAccepted;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public int getTimezoneOffset() {
		return timezoneOffset;
	}

	public void setTimezoneOffset(int timezoneOffset) {
		this.timezoneOffset = timezoneOffset;
	}

	public String getCtyCode() {
		return ctyCode;
	}

	public void setCtyCode(String ctyCode) {
		this.ctyCode = ctyCode;
	}

	public String getUnitLength() {
		return unitLength;
	}

	public void setUnitLength(String unitLength) {
		this.unitLength = unitLength;
	}

	public String getUnitVolume() {
		return unitVolume;
	}

	public void setUnitVolume(String unitVolume) {
		this.unitVolume = unitVolume;
	}

	public String getUnitTemperature() {
		return unitTemperature;
	}

	public void setUnitTemperature(String unitTemperature) {
		this.unitTemperature = unitTemperature;
	}

	public String getUnitCurrency() {
		return unitCurrency;
	}

	public void setUnitCurrency(String unitCurrency) {
		this.unitCurrency = unitCurrency;
	}

	public String getUnitWeight() {
		return unitWeight;
	}

	public void setUnitWeight(String unitWeight) {
		this.unitWeight = unitWeight;
	}

	public String getUnitDate() {
		return unitDate;
	}

	public void setUnitDate(String unitDate) {
		this.unitDate = unitDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getMobilePhoneCtyCode() {
		return mobilePhoneCtyCode;
	}

	public void setMobilePhoneCtyCode(String mobilePhoneCtyCode) {
		this.mobilePhoneCtyCode = mobilePhoneCtyCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhoneCtyCode() {
		return phoneCtyCode;
	}

	public void setPhoneCtyCode(String phoneCtyCode) {
		this.phoneCtyCode = phoneCtyCode;
	}

	public Long getBirth() {
		return birth;
	}

	public void setBirth(Long birth) {
		this.birth = birth;
	}

	public String getBlood() {
		return blood;
	}

	public void setBlood(String blood) {
		this.blood = blood;
	}

	public String getBloodRh() {
		return bloodRh;
	}

	public void setBloodRh(String bloodRh) {
		this.bloodRh = bloodRh;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCorpPosition() {
		return corpPosition;
	}

	public void setCorpPosition(String corpPosition) {
		this.corpPosition = corpPosition;
	}

	public String getCorpKey() {
		return corpKey;
	}

	public void setCorpKey(String corpKey) {
		this.corpKey = corpKey;
	}

	public String getCorpName() {
		return corpName;
	}

	public void setCorpName(String corpName) {
		this.corpName = corpName;
	}

	public String getAllocateionUsed() {
		return allocateionUsed;
	}

	public void setAllocateionUsed(String allocateionUsed) {
		this.allocateionUsed = allocateionUsed;
	}

	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}

	public String getServiceGrade() {
		return serviceGrade;
	}

	public void setServiceGrade(String serviceGrade) {
		this.serviceGrade = serviceGrade;
	}

			
}
