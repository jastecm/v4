package v4.web.vo;


public class EventVO_Trip extends EventVO_Abstract implements EventVO{
	
	public EventVO_Trip(){
		super.setEventType("trip");
	}
	
		
	private String startDate;
	private String endDate;
	private double startLat;
	private double startLon;
	private double endLat;
	private double endLon;
	private String startAddr;
	private String endAddr;
	
	private String tripId;
	private String tripKey;
	private String tripIndenty;
	
	private String purpose;
	
	private int distance;
	private int drivingTime;
	private int fco;
	private int oilPrice;
	
	private double avgFco;
	
	private String accountKey;
	
	private double officialAvgFco;
	private int safeScore;
	private int ecoScore;
	private int fuelScore;
	
	private String tripState;
	private String tripDetailState;
	
	
	public String getTripDetailState() {
		return tripDetailState;
	}
	public void setTripDetailState(String tripDetailState) {
		this.tripDetailState = tripDetailState;
	}
	public String getTripState() {
		return tripState;
	}
	public void setTripState(String tripState) {
		this.tripState = tripState;
	}
	public int getFuelScore() {
		return fuelScore;
	}
	public void setFuelScore(int fuelScore) {
		this.fuelScore = fuelScore;
	}
	public String getTripIndenty() {
		return tripIndenty;
	}
	public void setTripIndenty(String tripIndenty) {
		this.tripIndenty = tripIndenty;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public double getStartLat() {
		return startLat;
	}
	public void setStartLat(double startLat) {
		this.startLat = startLat;
	}
	public double getStartLon() {
		return startLon;
	}
	public void setStartLon(double startLon) {
		this.startLon = startLon;
	}
	public double getEndLat() {
		return endLat;
	}
	public void setEndLat(double endLat) {
		this.endLat = endLat;
	}
	public double getEndLon() {
		return endLon;
	}
	public void setEndLon(double endLon) {
		this.endLon = endLon;
	}
	public String getStartAddr() {
		return startAddr;
	}
	public void setStartAddr(String startAddr) {
		this.startAddr = startAddr;
	}
	public String getEndAddr() {
		return endAddr;
	}
	public void setEndAddr(String endAddr) {
		this.endAddr = endAddr;
	}
	public String getTripId() {
		return tripId;
	}
	public void setTripId(String tripId) {
		this.tripId = tripId;
	}
	public String getTripKey() {
		return tripKey;
	}
	public void setTripKey(String tripKey) {
		this.tripKey = tripKey;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	public int getDistance() {
		return distance;
	}
	public void setDistance(int distance) {
		this.distance = distance;
	}
	public int getDrivingTime() {
		return drivingTime;
	}
	public void setDrivingTime(int drivingTime) {
		this.drivingTime = drivingTime;
	}
	public int getFco() {
		return fco;
	}
	public void setFco(int fco) {
		this.fco = fco;
	}
	public int getOilPrice() {
		return oilPrice;
	}
	public void setOilPrice(int oilPrice) {
		this.oilPrice = oilPrice;
	}
	public double getAvgFco() {
		return avgFco;
	}
	public void setAvgFco(double avgFco) {
		this.avgFco = avgFco;
	}
	public String getAccountKey() {
		return accountKey;
	}
	public void setAccountKey(String accountKey) {
		this.accountKey = accountKey;
	}
	public double getOfficialAvgFco() {
		return officialAvgFco;
	}
	public void setOfficialAvgFco(double officialAvgFco) {
		this.officialAvgFco = officialAvgFco;
	}
	public int getSafeScore() {
		return safeScore;
	}
	public void setSafeScore(int safeScore) {
		this.safeScore = safeScore;
	}
	public int getEcoScore() {
		return ecoScore;
	}
	public void setEcoScore(int ecoScore) {
		this.ecoScore = ecoScore;
	}
	
	public long getSortDate() {
		return sortDate;
	}
	public void setSortDate(long sortDate) {
		this.sortDate = sortDate;
	}	
}
