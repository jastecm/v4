package v4.web.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class AllocateVO {
	private String allocateKey;
	private String accountKey;
	private String vehicleKey;
	private String startDate;
	private String endDate;
	private String purpose;
	private String title;
	private String contents;
	private String regDate;
	private String modDate;
	private String returnFlag;
	
	private String passenger;
	private String destination;
	
	private String check;
	private String tripKey;
	
	@JsonIgnore
	private String lang;
	@JsonIgnore
	private String corpKey;
	/*@JsonIgnore
	private String group;
	@JsonIgnore
	private String model;*/
	@JsonIgnore
	private int limit;
	@JsonIgnore
	private int offset;
	
	
	private String approvalState;
	private String approvalStep1Account;
	private String approvalStep1Date;
	private String approvalStep2Account;
	private String approvalStep2Date;
	private String approvalReserveAccount;
	private String approvalreserveDate;
	private String userAccount;
	private String affterAllocate;
	private String maxStep;
	
	private String fixedType;
	private String instantAllocate;
	
	private String reportFlag;

	private String isHolyDay;
	private String name;
	private String accountId;
	private String corpPosition;
	private String groupNm;
	
	private String approvalStep1Nm;
	private String approvalStep1Id;
	private String approvalStep1CorpPosition;
	private String approvalStep1GroupNm;
	private String approvalStep2Nm;
	private String approvalStep2Id;
	private String approvalStep2CorpPosition;
	private String approvalStep2GroupNm;
	private String plateNum;
	private String img_S;
	private String img_L;
	
	
	public String getIsHolyDay() {
		return isHolyDay;
	}
	public void setIsHolyDay(String isHolyDay) {
		this.isHolyDay = isHolyDay;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getCorpPosition() {
		return corpPosition;
	}
	public void setCorpPosition(String corpPosition) {
		this.corpPosition = corpPosition;
	}
	public String getGroupNm() {
		return groupNm;
	}
	public void setGroupNm(String groupNm) {
		this.groupNm = groupNm;
	}
	public String getApprovalStep1Nm() {
		return approvalStep1Nm;
	}
	public void setApprovalStep1Nm(String approvalStep1Nm) {
		this.approvalStep1Nm = approvalStep1Nm;
	}
	public String getApprovalStep1Id() {
		return approvalStep1Id;
	}
	public void setApprovalStep1Id(String approvalStep1Id) {
		this.approvalStep1Id = approvalStep1Id;
	}
	public String getApprovalStep1CorpPosition() {
		return approvalStep1CorpPosition;
	}
	public void setApprovalStep1CorpPosition(String approvalStep1CorpPosition) {
		this.approvalStep1CorpPosition = approvalStep1CorpPosition;
	}
	public String getApprovalStep1GroupNm() {
		return approvalStep1GroupNm;
	}
	public void setApprovalStep1GroupNm(String approvalStep1GroupNm) {
		this.approvalStep1GroupNm = approvalStep1GroupNm;
	}
	public String getApprovalStep2Nm() {
		return approvalStep2Nm;
	}
	public void setApprovalStep2Nm(String approvalStep2Nm) {
		this.approvalStep2Nm = approvalStep2Nm;
	}
	public String getApprovalStep2Id() {
		return approvalStep2Id;
	}
	public void setApprovalStep2Id(String approvalStep2Id) {
		this.approvalStep2Id = approvalStep2Id;
	}
	public String getApprovalStep2CorpPosition() {
		return approvalStep2CorpPosition;
	}
	public void setApprovalStep2CorpPosition(String approvalStep2CorpPosition) {
		this.approvalStep2CorpPosition = approvalStep2CorpPosition;
	}
	public String getApprovalStep2GroupNm() {
		return approvalStep2GroupNm;
	}
	public void setApprovalStep2GroupNm(String approvalStep2GroupNm) {
		this.approvalStep2GroupNm = approvalStep2GroupNm;
	}
	public String getPlateNum() {
		return plateNum;
	}
	public void setPlateNum(String plateNum) {
		this.plateNum = plateNum;
	}
	public String getImg_S() {
		return img_S;
	}
	public void setImg_S(String img_S) {
		this.img_S = img_S;
	}
	public String getImg_L() {
		return img_L;
	}
	public void setImg_L(String img_L) {
		this.img_L = img_L;
	}
	public String getReportFlag() {
		return reportFlag;
	}
	public void setReportFlag(String reportFlag) {
		this.reportFlag = reportFlag;
	}
	public String getInstantAllocate() {
		return instantAllocate;
	}
	public void setInstantAllocate(String instantAllocate) {
		this.instantAllocate = instantAllocate;
	}
	public String getFixedType() {
		return fixedType;
	}
	public void setFixedType(String fixedType) {
		this.fixedType = fixedType;
	}
	public String getApprovalState() {
		return approvalState;
	}
	public void setApprovalState(String approvalState) {
		this.approvalState = approvalState;
	}
	public String getApprovalStep1Account() {
		return approvalStep1Account;
	}
	public void setApprovalStep1Account(String approvalStep1Account) {
		this.approvalStep1Account = approvalStep1Account;
	}
	public String getApprovalStep1Date() {
		return approvalStep1Date;
	}
	public void setApprovalStep1Date(String approvalStep1Date) {
		this.approvalStep1Date = approvalStep1Date;
	}
	public String getApprovalStep2Account() {
		return approvalStep2Account;
	}
	public void setApprovalStep2Account(String approvalStep2Account) {
		this.approvalStep2Account = approvalStep2Account;
	}
	public String getApprovalStep2Date() {
		return approvalStep2Date;
	}
	public void setApprovalStep2Date(String approvalStep2Date) {
		this.approvalStep2Date = approvalStep2Date;
	}
	public String getApprovalReserveAccount() {
		return approvalReserveAccount;
	}
	public void setApprovalReserveAccount(String approvalReserveAccount) {
		this.approvalReserveAccount = approvalReserveAccount;
	}
	public String getApprovalreserveDate() {
		return approvalreserveDate;
	}
	public void setApprovalreserveDate(String approvalreserveDate) {
		this.approvalreserveDate = approvalreserveDate;
	}
	public String getUserAccount() {
		return userAccount;
	}
	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}
	public String getAffterAllocate() {
		return affterAllocate;
	}
	public void setAffterAllocate(String affterAllocate) {
		this.affterAllocate = affterAllocate;
	}
	public String getMaxStep() {
		return maxStep;
	}
	public void setMaxStep(String maxStep) {
		this.maxStep = maxStep;
	}
	public String getAllocateKey() {
		return allocateKey;
	}
	public void setAllocateKey(String allocateKey) {
		this.allocateKey = allocateKey;
	}
	public String getAccountKey() {
		return accountKey;
	}
	public void setAccountKey(String accountKey) {
		this.accountKey = accountKey;
	}
	public String getVehicleKey() {
		return vehicleKey;
	}
	public void setVehicleKey(String vehicleKey) {
		this.vehicleKey = vehicleKey;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getModDate() {
		return modDate;
	}
	public void setModDate(String modDate) {
		this.modDate = modDate;
	}
	public String getReturnFlag() {
		return returnFlag;
	}
	public void setReturnFlag(String returnFlag) {
		this.returnFlag = returnFlag;
	}
	public String getPassenger() {
		return passenger;
	}
	public void setPassenger(String passenger) {
		this.passenger = passenger;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getCheck() {
		return check;
	}
	public void setCheck(String check) {
		this.check = check;
	}
	public String getTripKey() {
		return tripKey;
	}
	public void setTripKey(String tripKey) {
		this.tripKey = tripKey;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public String getCorpKey() {
		return corpKey;
	}
	public void setCorpKey(String corpKey) {
		this.corpKey = corpKey;
	}
	/*public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}*/
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	
	
}
