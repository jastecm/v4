package v4.web.vo;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author woong
 *
 */
@ApiModel(value="CreateReActiveVehicleVO" , description="template create ReVehicle value object")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateReActiveVehicleVO {
	
	@ApiModelProperty(value = "device S/N",required = true)
	@NotEmpty(message = "deiceSn is required")
	private String deviceSn;
	
	@ApiModelProperty(value = "vehicleKey",required = true)
	@NotEmpty(message = "vehicleKey is required")
	private String vehicleKey;

	public String getDeviceSn() {
		return deviceSn;
	}

	public void setDeviceSn(String deviceSn) {
		this.deviceSn = deviceSn;
	}

	public String getVehicleKey() {
		return vehicleKey;
	}

	public void setVehicleKey(String vehicleKey) {
		this.vehicleKey = vehicleKey;
	}
	
	
			
}
