package v4.web.vo;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@ApiModel(value="UpdateDrivingPurposeVO" , description="template update DrivingPurpose value object")
@JsonIgnoreProperties(ignoreUnknown = true)
public @Data class UpdateDrivingPurposeVO {
	
	@ApiModelProperty(required = true ,value = "seq value")
	@NotEmpty(message = "seq.required")
	private String seq;
	
	@ApiModelProperty(required = true ,value = "drivingPurposeName value")
	@NotEmpty(message = "drivingPurposeName.required")
	private String drivingPurposeName;
	
	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String corpKey;
}
