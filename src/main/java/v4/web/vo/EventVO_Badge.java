package v4.web.vo;


import java.util.List;

public class EventVO_Badge extends EventVO_Abstract implements EventVO{
	
	public EventVO_Badge(){
		super.setEventType("badge");
	}
	 
	private List<BadgeVO> badgeList;

	public List<BadgeVO> getBadgeList() {
		return badgeList;
	}

	public void setBadgeList(List<BadgeVO> badgeList) {
		this.badgeList = badgeList;
	}

	@Override
	public long getSortDate() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
}
