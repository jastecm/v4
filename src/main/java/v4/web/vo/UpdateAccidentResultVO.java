package v4.web.vo;

import org.codehaus.jackson.annotate.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value="UpdateAccidentResultVO" , description="template update accidentresult value object")
public @Data class UpdateAccidentResultVO {

	@JsonIgnore
	@ApiModelProperty(hidden = true)	
	private String corpKey;
	
	@ApiModelProperty(value = "accidentKey",required = true)
	private String accidentKey;
	
	@ApiModelProperty(value = "insuranceResult",required = true)
	private String insuranceResult;
	
	@ApiModelProperty(value = "repairDesc")
	private String repairDesc;
	
	@ApiModelProperty(value = "cost")
	private int cost;
	
	@ApiModelProperty(value = "costType")
	private String costType;
	
}
