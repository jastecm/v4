package v4.web.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value="CreateAccidentVO" , description="template create accident value object")
@JsonIgnoreProperties(ignoreUnknown = true)
public @Data class CreateAccidentVO {
	
	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String corpKey;
	
	@ApiModelProperty(value = "accountKey",required = true)
	private String accountKey;
	
	@ApiModelProperty(value = "vehicleKey",required = true)
	private String vehicleKey;
	
	@ApiModelProperty(value = "accidentDate",required = true)
	private String accidentDate;
	
	@ApiModelProperty(value = "location")
	private String location;
	
	@ApiModelProperty(value = "outline")
	private String outline;
	
	@ApiModelProperty(value = "accidentImg1")
	private String accidentImg1;
	
	@ApiModelProperty(value = "accidentImg2")
	private String accidentImg2;
	
	@ApiModelProperty(value = "accidentImg3")
	private String accidentImg3;
	
	@ApiModelProperty(value = "opponentPlateNum")
	private String opponentPlateNum;
	
	@ApiModelProperty(value = "opponentName")
	private String opponentName;
	
	@ApiModelProperty(value = "opponentPhone")
	private String opponentPhone;
	
	@ApiModelProperty(value = "insuranceCompany")
	private String insuranceCompany;
	
	@ApiModelProperty(value = "insuranceReceiptNumber")
	private String insuranceReceiptNumber;
	
	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String regUser;
	
	private String approvalState;
	private String approvalStep1Account;
	private String approvalStep1Date;
	private String approvalStep2Account;
	private String approvalStep2Date;
	private String approvalStep3Account;
	private String approvalStep3Date;
	private String approvalStep4Account;
	private String approvalStep4Date;
	private String approvalStep5Account;
	private String approvalStep5Date;
	private String approvalStep6Account;
	private String approvalStep6Date;
	private String maxStep;
	
	
}
