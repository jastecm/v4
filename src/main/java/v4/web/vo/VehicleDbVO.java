package v4.web.vo;


public class VehicleDbVO {
	private String lang;	
	private String manufacture;
	private String modelMaster;
	private String year;
	private String modelHeader;
	private String volume;
	private String transmission;
	private String fuelType;
	
	
	public VehicleDbVO() {
		super();
	}
	
	public VehicleDbVO(String lang, String manufacture, String modelMaster,
			String year, String modelHeader, String volume,
			String transmission, String fuelType) {
		super();
		this.lang = lang;
		this.manufacture = manufacture;
		this.modelMaster = modelMaster;
		this.year = year;
		this.modelHeader = modelHeader;
		this.volume = volume;
		this.transmission = transmission;
		this.fuelType = fuelType;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public String getManufacture() {
		return manufacture;
	}
	public void setManufacture(String manufacture) {
		this.manufacture = manufacture;
	}
	public String getModelMaster() {
		return modelMaster;
	}
	public void setModelMaster(String modelMaster) {
		this.modelMaster = modelMaster;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getModelHeader() {
		return modelHeader;
	}
	public void setModelHeader(String modelHeader) {
		this.modelHeader = modelHeader;
	}
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	public String getTransmission() {
		return transmission;
	}
	public void setTransmission(String transmission) {
		this.transmission = transmission;
	}
	public String getFuelType() {
		return fuelType;
	}
	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}
	
	
		
}
