package v4.web.vo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author woong
 *
 */
@ApiModel(value="CreateBusVO" , description="template create Bus value object")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateBusVO {
	
	@ApiModelProperty(value = "plateNum",required = true)
	@NotEmpty(message = "plateNum is required")
	private String plateNum;
	
	@ApiModelProperty(value = "totDistance(unit. km)",required = true)
	@NotNull(message = "totDistance is required")
	private double totDistance = 0;
	///////////필수항목 끝/////////////
	
	@ApiModelProperty(value = "manufacture")
	private String manufacture;
	
	@ApiModelProperty(value = "modelMaster")
	private String modelMaster;
	
	@ApiModelProperty(value = "year")
	private String year;
	
	@ApiModelProperty(value = "modelHeader")
	private String modelHeader;
	
	@ApiModelProperty(value = "volume")
	private String volume;
	
	@ApiModelProperty(value = "transmission")
	private String transmission;
	
	@ApiModelProperty(value = "fuelEfficiency")
	private double fuelEfficiency = 0;
	
	@ApiModelProperty(value = "fuelType")
	private String fuelType;

	@ApiModelProperty(value = "specifications")
	private String specifications;
	
	
	@ApiModelProperty(value = "defaultAccountKey - driver")
	private String defaultAccountKey;
	
	//단말 필수아님
	@ApiModelProperty(value = "device S/N",required = false)
	private String deviceSn;
	
	@ApiModelProperty(value = "fixed" , hidden=true)
	private String fixed = "0";
	
	@ApiModelProperty(value = "fixedOnOff 1 on account" , hidden = true)
	private String fixedAccountKey;
	
	@ApiModelProperty(value = "fixedOnOff 1 on driver account" , hidden = true)
	private String fixedDriverKey;
	///////////권장필드 끝//////////
	
	@ApiModelProperty(value = "vehicleType" , hidden=true)
	@Pattern(regexp = "^1$|^2$|^3$|^4$" , message = "vehicleType only 1(vehicle),2(Bus),3(Truck),4(ETC)")
	private String vehicleType = "2";
	
	//서브타입이 2종류이상 있다면 히든제거후 required
	@ApiModelProperty(value = "vehicleSubType" , hidden=true)
	private String vehicleSubType = "1";
	
	/*@ApiModelProperty(value = "corpGroupKey")
	private String corpGroupKey;*/
	
	@ApiModelProperty(value = "locationSetting")
	@Pattern(regexp = "^0$|^1$" , message = "locationSetting only 1(ON),0(OFF)")
	private String locationSetting = "1";
	
	
	////////////////////////////////////////
	
	
	@ApiModelProperty(value = "vehicle color")
	private String color;
	
	
	//개인,리스,랜탈,기타 외 다른타입을 쓴다면 5번부터 추가할것(5 : 지입?)
	@ApiModelProperty(value = "vehicleBizType")
	@Pattern(regexp = "^1$|^2$|^3$|^4$|^$" , message = "vehicleBizType only 1(personal),2(ris),3(rent),4(ETC),''")
	private String vehicleBizType = "1";
	
	@ApiModelProperty(value = "vehicleRegDate")
	private Long vehicleRegDate;
	
	@ApiModelProperty(value = "holidayPass" , hidden = true)
	private String holidayPass = "0";
	
	@ApiModelProperty(value = "approvalType" , hidden = true)
	private String approvalType = "0";
	
	@ApiModelProperty(value = "vehicleBodyNum")
	private String vehicleBodyNum;
	
	@ApiModelProperty(value = "vehicleMaintenanceDate")
	private Long vehicleMaintenanceDate;
	
	@ApiModelProperty(value = "vehiclePayment")	
	private int vehiclePayment = 0;
	
	@ApiModelProperty(value = "vehicleInsureNm")
	private String vehicleInsureNm;
	
	@ApiModelProperty(value = "vehicleInsurePhone")
	private String vehicleInsurePhone;
	
	@ApiModelProperty(value = "vehicleDescript")
	private String vehicleDescript;
	
	
	@ApiModelProperty(value = "vehicleKey", hidden = true)
	private String vehicleKey;
	
	@ApiModelProperty(hidden = true)
	@JsonIgnore
	private String corpKey;
	
	@ApiModelProperty(hidden = true)
	@JsonIgnore
	private String deviceId;
	
	@ApiModelProperty(hidden = true)
	@JsonIgnore
	private String deviceSound;
	
	@ApiModelProperty(hidden = true)
	@JsonIgnore
	private String regUser;

	
	public String getRegUser() {
		return regUser;
	}

	public void setRegUser(String regUser) {
		this.regUser = regUser;
	}

	public String getPlateNum() {
		return plateNum;
	}

	public void setPlateNum(String plateNum) {
		this.plateNum = plateNum;
	}

	public double getTotDistance() {
		return totDistance;
	}

	public void setTotDistance(double totDistance) {
		this.totDistance = totDistance;
	}

	

	public String getManufacture() {
		return manufacture;
	}

	public void setManufacture(String manufacture) {
		this.manufacture = manufacture;
	}

	public String getModelMaster() {
		return modelMaster;
	}

	public void setModelMaster(String modelMaster) {
		this.modelMaster = modelMaster;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getModelHeader() {
		return modelHeader;
	}

	public void setModelHeader(String modelHeader) {
		this.modelHeader = modelHeader;
	}

	public String getSpecifications() {
		return specifications;
	}

	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}

	public String getDefaultAccountKey() {
		return defaultAccountKey;
	}

	public void setDefaultAccountKey(String defaultAccountKey) {
		this.defaultAccountKey = defaultAccountKey;
	}

	public String getDeviceSn() {
		return deviceSn;
	}

	public void setDeviceSn(String deviceSn) {
		this.deviceSn = deviceSn;
	}

	public String getFixed() {
		return fixed;
	}

	public void setFixed(String fixed) {
		this.fixed = fixed;
	}

	public String getFixedAccountKey() {
		return fixedAccountKey;
	}

	public void setFixedAccountKey(String fixedAccountKey) {
		this.fixedAccountKey = fixedAccountKey;
	}

	public String getFixedDriverKey() {
		return fixedDriverKey;
	}

	public void setFixedDriverKey(String fixedDriverKey) {
		this.fixedDriverKey = fixedDriverKey;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getVehicleSubType() {
		return vehicleSubType;
	}

	public void setVehicleSubType(String vehicleSubType) {
		this.vehicleSubType = vehicleSubType;
	}

/*	public String getCorpGroupKey() {
		return corpGroupKey;
	}

	public void setCorpGroupKey(String corpGroupKey) {
		this.corpGroupKey = corpGroupKey;
	}*/

	public String getLocationSetting() {
		return locationSetting;
	}

	public void setLocationSetting(String locationSetting) {
		this.locationSetting = locationSetting;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getVehicleBizType() {
		return vehicleBizType;
	}

	public void setVehicleBizType(String vehicleBizType) {
		this.vehicleBizType = vehicleBizType;
	}

	public Long getVehicleRegDate() {
		return vehicleRegDate;
	}

	public void setVehicleRegDate(Long vehicleRegDate) {
		this.vehicleRegDate = vehicleRegDate;
	}

	public String getHolidayPass() {
		return holidayPass;
	}

	public void setHolidayPass(String holidayPass) {
		this.holidayPass = holidayPass;
	}

	public String getApprovalType() {
		return approvalType;
	}

	public void setApprovalType(String approvalType) {
		this.approvalType = approvalType;
	}

	public String getVehicleBodyNum() {
		return vehicleBodyNum;
	}

	public void setVehicleBodyNum(String vehicleBodyNum) {
		this.vehicleBodyNum = vehicleBodyNum;
	}

	public Long getVehicleMaintenanceDate() {
		return vehicleMaintenanceDate;
	}

	public void setVehicleMaintenanceDate(Long vehicleMaintenanceDate) {
		this.vehicleMaintenanceDate = vehicleMaintenanceDate;
	}

	public int getVehiclePayment() {
		return vehiclePayment;
	}

	public void setVehiclePayment(int vehiclePayment) {
		this.vehiclePayment = vehiclePayment;
	}

	public String getVehicleInsureNm() {
		return vehicleInsureNm;
	}

	public void setVehicleInsureNm(String vehicleInsureNm) {
		this.vehicleInsureNm = vehicleInsureNm;
	}

	public String getVehicleInsurePhone() {
		return vehicleInsurePhone;
	}

	public void setVehicleInsurePhone(String vehicleInsurePhone) {
		this.vehicleInsurePhone = vehicleInsurePhone;
	}

	public String getVehicleDescript() {
		return vehicleDescript;
	}

	public void setVehicleDescript(String vehicleDescript) {
		this.vehicleDescript = vehicleDescript;
	}

	public String getVehicleKey() {
		return vehicleKey;
	}

	public void setVehicleKey(String vehicleKey) {
		this.vehicleKey = vehicleKey;
	}

	public String getCorpKey() {
		return corpKey;
	}

	public void setCorpKey(String corpKey) {
		this.corpKey = corpKey;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceSound() {
		return deviceSound;
	}

	public void setDeviceSound(String deviceSound) {
		this.deviceSound = deviceSound;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getTransmission() {
		return transmission;
	}

	public void setTransmission(String transmission) {
		this.transmission = transmission;
	}

	public String getFuelType() {
		return fuelType;
	}

	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}

	public double getFuelEfficiency() {
		return fuelEfficiency;
	}

	public void setFuelEfficiency(double fuelEfficiency) {
		this.fuelEfficiency = fuelEfficiency;
	}

	
	
}
