package v4.web.vo;

import javax.validation.constraints.Pattern;

import io.swagger.annotations.ApiModelProperty;

public class LocationVO {
	@ApiModelProperty(value = "위치(gps lon,lat)")
	@Pattern(regexp ="^\\d+\\.\\d+,\\d+\\.\\d+$" , message = "Invalid location (pattern : 'lon,lat')")
	private String location;
	
	@ApiModelProperty(value = "위치주소")
	private String locationAddr;
	
	@ApiModelProperty(value = "위치주소상세")
	private String locationAddrDetail;
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getLocationAddr() {
		return locationAddr;
	}
	public void setLocationAddr(String locationAddr) {
		this.locationAddr = locationAddr;
	}
	public String getLocationAddrDetail() {
		return locationAddrDetail;
	}
	public void setLocationAddrDetail(String locationAddrDetail) {
		this.locationAddrDetail = locationAddrDetail;
	}
}
