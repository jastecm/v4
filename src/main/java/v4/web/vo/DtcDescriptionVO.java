package v4.web.vo;

public class DtcDescriptionVO {
	private String typeName;
	private String appName;
	private String emegencyRepair;
	private String manufacture;
	private String descript;
	private String code;
	
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public String getEmegencyRepair() {
		return emegencyRepair;
	}
	public void setEmegencyRepair(String emegencyRepair) {
		this.emegencyRepair = emegencyRepair;
	}
	public String getManufacture() {
		return manufacture;
	}
	public void setManufacture(String manufacture) {
		this.manufacture = manufacture;
	}
	public String getDescript() {
		return descript;
	}
	public void setDescript(String descript) {
		this.descript = descript;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	@Override
	public String toString() {
		return "DtcDescriptionVO [typeName=" + typeName + ", appName=" + appName + ", emegencyRepair=" + emegencyRepair
				+ ", manufacture=" + manufacture + ", descript=" + descript + ", code=" + code + "]";
	}
	
	
}
