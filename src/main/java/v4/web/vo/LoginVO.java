package v4.web.vo;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginVO {
	
	@ApiModelProperty(value = "id" , required = true)
	@NotEmpty(message = "id.required")
	private String id;
	
	@ApiModelProperty(value = "pw" , required = true)
	@NotEmpty(message = "pw.required")
	private String pw;
	
	@ApiModelProperty(value = "token expired" , required = true)
	//@NotEmpty(message = "expire.required")
	private Long expire;
	
	public Long getExpire() {
		return expire;
	}
	public void setExpire(Long expire) {
		this.expire = expire;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPw() {
		return pw;
	}
	public void setPw(String pw) {
		this.pw = pw;
	}
	
	
		
}
