package v4.web.vo;

public class AdminCorpVO {
	
	private String corp;
	private String corpNm;
	private String deviceSound;
	
	
	public String getCorp() {
		return corp;
	}
	public void setCorp(String corp) {
		this.corp = corp;
	}
	public String getCorpNm() {
		return corpNm;
	}
	public void setCorpNm(String corpNm) {
		this.corpNm = corpNm;
	}
	public String getDeviceSound() {
		return deviceSound;
	}
	public void setDeviceSound(String deviceSound) {
		this.deviceSound = deviceSound;
	}
}
