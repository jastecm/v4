package v4.web.vo;

import org.jastecm.string.StrUtil;

public class WorkingTimeAreaVO {
	private String corpKey;
	private String groupKey;
	private String accountKey;
	private String cityLev1;
	private String cityLev2;
	private String workAreaKey;
	
	private String groupNm;
	private String accountNm;
	private String corpPosition;
	private String lev1Cd;
	private String lev1Nm;
	private String lev2Cd;
	private String lev2Nm;
	
	private String startH;
	private String startM;
	private String endH;
	private String endM;
	private String[] dayArr;

	private String workTimeKey;
	private String dayMon;
	private String dayTue;
	private String dayWed;
	private String dayThu;
	private String dayFri;
	private String daySat;
	private String daySun;
	
	
	public WorkingTimeAreaVO() {}
	
	
	public WorkingTimeAreaVO(String corpKey, String groupKey, String accountKey, String cityLev1, String cityLev2) {
		super();
		this.corpKey = corpKey;
		
		if(!StrUtil.isNullToEmpty(groupKey))
			this.groupKey = groupKey;
		
		if(!StrUtil.isNullToEmpty(accountKey))
			this.accountKey = accountKey;
		
		this.cityLev1 = cityLev1;
		
		if(!StrUtil.isNullToEmpty(cityLev2))
			this.cityLev2 = cityLev2;
	}

	public WorkingTimeAreaVO(String corpkey, String workAreaKey,String groupKey, String accountKey, String cityLev1, String cityLev2) {
		super();
		this.corpKey = corpkey;
		this.workAreaKey = workAreaKey;
		
		if(!StrUtil.isNullToEmpty(groupKey))
			this.groupKey = groupKey;
		
		if(!StrUtil.isNullToEmpty(accountKey))
			this.accountKey = accountKey;
		
		this.cityLev1 = cityLev1;
		
		if(!StrUtil.isNullToEmpty(cityLev2))
			this.cityLev2 = cityLev2;
	}
	
	public WorkingTimeAreaVO(String corpKey, String groupKey, String accountKey, String startH, String startM,
			String endH, String endM, String[] dayArr) {
		super();
		this.corpKey = corpKey;
		
		if(!StrUtil.isNullToEmpty(groupKey))
			this.groupKey = groupKey;

		if(!StrUtil.isNullToEmpty(accountKey))
			this.accountKey = accountKey;

		this.startM = startM;
		this.startH = startH;
		this.endH = endH;
		this.endM = endM;
		
		this.dayArr = new String[dayArr.length];
		for(int i = 0;i < dayArr.length;i++){
			if(!StrUtil.isNullToEmpty(dayArr[i]))
				this.dayArr[i] = dayArr[i];
			else
				this.dayArr[i] = null;
		}
	}
	
	public WorkingTimeAreaVO(String corpKey, String workTimeKey, String groupKey, String accountKey, String startH, String startM,
			String endH, String endM, String[] dayArr) {
		super();
		this.corpKey = corpKey;
		this.workTimeKey = workTimeKey;
		
		if(!StrUtil.isNullToEmpty(groupKey))
			this.groupKey = groupKey;

		if(!StrUtil.isNullToEmpty(accountKey))
			this.accountKey = accountKey;

		this.startM = startM;
		this.startH = startH;
		this.endH = endH;
		this.endM = endM;
		
		this.dayArr = new String[dayArr.length];
		for(int i = 0;i < dayArr.length;i++){
			if(!StrUtil.isNullToEmpty(dayArr[i]))
				this.dayArr[i] = dayArr[i];
			else
				this.dayArr[i] = null;
		}
	}

	public String getWorkAreaKey() {
		return workAreaKey;
	}


	public String getCorpGroupNm() {
		return groupNm;
	}


	public void setCorpGroupNm(String corpGroupNm) {
		this.groupNm = corpGroupNm;
	}


	public String getAccountNm() {
		return accountNm;
	}


	public void setAccountNm(String accountNm) {
		this.accountNm = accountNm;
	}


	public String getCorpPosition() {
		return corpPosition;
	}


	public void setCorpPosition(String corpPosition) {
		this.corpPosition = corpPosition;
	}


	public String getLev1Cd() {
		return lev1Cd;
	}


	public void setLev1Cd(String lev1Cd) {
		this.lev1Cd = lev1Cd;
	}


	public String getLev1Nm() {
		return lev1Nm;
	}


	public void setLev1Nm(String lev1Nm) {
		this.lev1Nm = lev1Nm;
	}


	public String getLev2Cd() {
		return lev2Cd;
	}


	public void setLev2Cd(String lev2Cd) {
		this.lev2Cd = lev2Cd;
	}


	public String getLev2Nm() {
		return lev2Nm;
	}


	public void setLev2Nm(String lev2Nm) {
		this.lev2Nm = lev2Nm;
	}


	public void setWorkAreaKey(String workAreaKey) {
		this.workAreaKey = workAreaKey;
	}


	public String getCorp() {
		return corpKey;
	}
	
	
	public void setCorp(String corp) {
		this.corpKey = corp;
	}
	
	
	public String getGroupKey() {
		return groupKey;
	}
	
	
	public void setGroupKey(String groupKey) {
		this.groupKey = groupKey;
	}
	
	
	public String getAccountKey() {
		return accountKey;
	}
	
	
	public void setAccountKey(String accountKey) {
		this.accountKey = accountKey;
	}
	
	
	public String getCityLev1() {
		return cityLev1;
	}
	
	
	public void setCityLev1(String cityLev1) {
		this.cityLev1 = cityLev1;
	}
	
	
	public String getCityLev2() {
		return cityLev2;
	}
	
	
	public void setCityLev2(String cityLev2) {
		this.cityLev2 = cityLev2;
	}

	
	public String getStartH() {
		return startH;
	}
	
	
	public void setStartH(String startH) {
		this.startH = startH;
	}
	
	
	public String getStartM() {
		return startM;
	}
	
	
	public void setStartM(String startM) {
		this.startM = startM;
	}
	
	
	public String getEndH() {
		return endH;
	}
	
	
	public void setEndH(String endH) {
		this.endH = endH;
	}
	
	
	public String getEndM() {
		return endM;
	}
	
	
	public void setEndM(String endM) {
		this.endM = endM;
	}
	
	
	public String[] getDayArr() {
		return dayArr;
	}
	
	
	public void setDayArr(String[] dayArr) {
		this.dayArr = dayArr;
	}
	
	
	public String getWorkTimeKey() {
		return workTimeKey;
	}


	public void setWorkTimeKey(String workTimeKey) {
		this.workTimeKey = workTimeKey;
	}


	public String getDayMon() {
		return dayMon;
	}
	
	
	public void setDayMon(String dayMon) {
		this.dayMon = dayMon;
	}
	
	
	public String getDayTue() {
		return dayTue;
	}
	
	
	public void setDayTue(String dayTue) {
		this.dayTue = dayTue;
	}
	
	
	public String getDayWed() {
		return dayWed;
	}
	
	
	public void setDayWed(String dayWed) {
		this.dayWed = dayWed;
	}
	
	
	public String getDayThu() {
		return dayThu;
	}
	
	
	public void setDayThu(String dayThu) {
		this.dayThu = dayThu;
	}
	
	
	public String getDayFri() {
		return dayFri;
	}
	
	
	public void setDayFri(String dayFri) {
		this.dayFri = dayFri;
	}
	
	
	public String getDaySat() {
		return daySat;
	}
	
	
	public void setDaySat(String daySat) {
		this.daySat = daySat;
	}
	
	
	public String getDaySun() {
		return daySun;
	}
	
	
	public void setDaySun(String daySun) {
		this.daySun = daySun;
	}
}
