package v4.web.vo;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author woong
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountVO {
	
	private int seq;
	
	private String corpKey;
	
	private String accountId;
	
	private String accountKey;
	
	private String fcmKey;
	
	private String accountPw;
	
	private String mobilePhone;
	
	private String mobilePhoneCtyCode;
	
	private String phone;
	
	private String phoneCtyCode;
	
	private String groupKey;
	
	private int rule;
	
	private int subRule;
	
	private String addr;
	
	private String lang;
	
	private String ctyCode;
	
	private int timezoneOffset;
	
	private String name;
	
	private Long birth;
	
	private String blood;
	
	private String bloodRh;
	
	private String gender;
	
	private String serviceAccepted;
	
	private String privateAccepted;
	
	private String locationAccepted;
	
	private String marketingAccepted;
	
	private String smsAccepted;
	
	private String joinDate;
	
	private String leaveDate;
	
	private String corpPosition;
	
	private String favo1;
	
	private String favo2;
	
	private String favo3;
	
	private String favo4;
	
	private String favo5;
	
	private String favo6;
	
	private String descript;
	
	private String pcmngMonitor;
	
	private String pushMonitor;

	private String corpType,serviceGrade,partnerType,corpName,corpNum,ceoName,corpPhone,corpPhoneCtyCode;
	private String corpBusiness,corpItem,allocationUsed,apiKey;
	private long apiExpired;
	private int geoMaxCnt;
	
	private String mailAuth;
	
	private String groupNm;
	
	private String unitLength, unitVolume, unitTemperature,unitWeight,unitDate,unitCurrency;
	
	private String latestVehicleKey,latestLocationTime,latestStartDate,latestEndDate,latestDistance,drivingState,latestModelMaster,latestPlateNum;
	
	public AccountVO() {
		super();
	}
	
	
	//회원가입용
	public AccountVO(String accountId, String accountPw,String corpKey,
			String mobilePhone, String mobilePhoneCtyCode, String phone, String phoneCtyCode, int rule,
			int subRule, String addr, String lang, String ctyCode, int timezoneOffset, String name, Long birth,
			String blood, String bloodRh, String gender, String serviceAccepted, String privateAccepted,
			String locationAccepted, String marketingAccepted, String smsAccepted,
			String corpPosition,String mailAuth,String unitLength, String unitVolume,String unitTemperature , String unitWeight , String unitDate) {
		super();
		this.accountId = accountId;
		this.accountPw = accountPw;
		this.corpKey = corpKey;
		this.mobilePhone = mobilePhone;
		this.mobilePhoneCtyCode = mobilePhoneCtyCode;
		this.phone = phone;
		this.phoneCtyCode = phoneCtyCode;
		this.rule = rule;
		this.subRule = subRule;
		this.addr = addr;
		this.lang = lang;
		this.ctyCode = ctyCode;
		this.timezoneOffset = timezoneOffset;
		this.name = name;
		this.birth = birth;
		this.blood = blood;
		this.bloodRh = bloodRh;
		this.gender = gender;
		this.serviceAccepted = serviceAccepted;
		this.privateAccepted = privateAccepted;
		this.locationAccepted = locationAccepted;
		this.marketingAccepted = marketingAccepted;
		this.smsAccepted = smsAccepted;
		this.corpPosition = corpPosition;
		this.mailAuth = mailAuth;
		this.unitLength = unitLength;
		this.unitVolume = unitVolume;
		this.unitTemperature = unitTemperature;
		this.unitWeight = unitWeight;
		if(unitDate.equals("YMD")) this.unitDate = "%Y/%m/%d";
		else if(unitDate.equals("MDY")) this.unitDate = "%m/%d/%Y";
		else if(unitDate.equals("DMY")) this.unitDate = "%d/%m/%Y";
		else this.unitDate = "%Y/%m/%d";
	}
	
	

	public String getLatestVehicleKey() {
		return latestVehicleKey;
	}


	public void setLatestVehicleKey(String latestVehicleKey) {
		this.latestVehicleKey = latestVehicleKey;
	}


	public String getLatestLocationTime() {
		return latestLocationTime;
	}


	public void setLatestLocationTime(String latestLocationTime) {
		this.latestLocationTime = latestLocationTime;
	}


	public String getLatestStartDate() {
		return latestStartDate;
	}


	public void setLatestStartDate(String latestStartDate) {
		this.latestStartDate = latestStartDate;
	}


	public String getLatestEndDate() {
		return latestEndDate;
	}


	public void setLatestEndDate(String latestEndDate) {
		this.latestEndDate = latestEndDate;
	}


	public String getLatestDistance() {
		return latestDistance;
	}


	public void setLatestDistance(String latestDistance) {
		this.latestDistance = latestDistance;
	}


	public String getDrivingState() {
		return drivingState;
	}


	public void setDrivingState(String drivingState) {
		this.drivingState = drivingState;
	}


	public String getLatestModelMaster() {
		return latestModelMaster;
	}


	public void setLatestModelMaster(String latestModelMaster) {
		this.latestModelMaster = latestModelMaster;
	}


	public String getLatestPlateNum() {
		return latestPlateNum;
	}


	public void setLatestPlateNum(String latestPlateNum) {
		this.latestPlateNum = latestPlateNum;
	}


	public String getDescript() {
		return descript;
	}


	public void setDescript(String descript) {
		this.descript = descript;
	}


	public String getUnitCurrency() {
		return unitCurrency;
	}


	public void setUnitCurrency(String unitCurrency) {
		this.unitCurrency = unitCurrency;
	}


	public int getSeq() {
		return seq;
	}


	public void setSeq(int seq) {
		this.seq = seq;
	}


	public String getCorpKey() {
		return corpKey;
	}


	public void setCorpKey(String corpKey) {
		this.corpKey = corpKey;
	}


	public String getAccountId() {
		return accountId;
	}


	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}


	public String getAccountKey() {
		return accountKey;
	}


	public void setAccountKey(String accountKey) {
		this.accountKey = accountKey;
	}


	public String getFcmKey() {
		return fcmKey;
	}


	public void setFcmKey(String fcmKey) {
		this.fcmKey = fcmKey;
	}


	public String getAccountPw() {
		return accountPw;
	}


	public void setAccountPw(String accountPw) {
		this.accountPw = accountPw;
	}


	public String getMobilePhone() {
		return mobilePhone;
	}


	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}


	public String getMobilePhoneCtyCode() {
		return mobilePhoneCtyCode;
	}


	public void setMobilePhoneCtyCode(String mobilePhoneCtyCode) {
		this.mobilePhoneCtyCode = mobilePhoneCtyCode;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getPhoneCtyCode() {
		return phoneCtyCode;
	}


	public void setPhoneCtyCode(String phoneCtyCode) {
		this.phoneCtyCode = phoneCtyCode;
	}


	public String getGroupKey() {
		return groupKey;
	}


	public void setGroupKey(String groupKey) {
		this.groupKey = groupKey;
	}


	public int getRule() {
		return rule;
	}


	public void setRule(int rule) {
		this.rule = rule;
	}


	public int getSubRule() {
		return subRule;
	}


	public void setSubRule(int subRule) {
		this.subRule = subRule;
	}


	public String getAddr() {
		return addr;
	}


	public void setAddr(String addr) {
		this.addr = addr;
	}


	public String getLang() {
		return lang;
	}


	public void setLang(String lang) {
		this.lang = lang;
	}


	public String getCtyCode() {
		return ctyCode;
	}


	public void setCtyCode(String ctyCode) {
		this.ctyCode = ctyCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTimezoneOffset() {
		return timezoneOffset;
	}


	public void setTimezoneOffset(int timezoneOffset) {
		this.timezoneOffset = timezoneOffset;
	}


	public Long getBirth() {
		return birth;
	}


	public void setBirth(Long birth) {
		this.birth = birth;
	}


	public String getBlood() {
		return blood;
	}


	public void setBlood(String blood) {
		this.blood = blood;
	}


	public String getBloodRh() {
		return bloodRh;
	}


	public void setBloodRh(String bloodRh) {
		this.bloodRh = bloodRh;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public String getServiceAccepted() {
		return serviceAccepted;
	}


	public void setServiceAccepted(String serviceAccepted) {
		this.serviceAccepted = serviceAccepted;
	}


	public String getPrivateAccepted() {
		return privateAccepted;
	}


	public void setPrivateAccepted(String privateAccepted) {
		this.privateAccepted = privateAccepted;
	}


	public String getLocationAccepted() {
		return locationAccepted;
	}


	public void setLocationAccepted(String locationAccepted) {
		this.locationAccepted = locationAccepted;
	}


	public String getMarketingAccepted() {
		return marketingAccepted;
	}


	public void setMarketingAccepted(String marketingAccepted) {
		this.marketingAccepted = marketingAccepted;
	}


	public String getSmsAccepted() {
		return smsAccepted;
	}


	public void setSmsAccepted(String smsAccepted) {
		this.smsAccepted = smsAccepted;
	}


	public String getJoinDate() {
		return joinDate;
	}


	public void setJoinDate(String joinDate) {
		this.joinDate = joinDate;
	}


	public String getLeaveDate() {
		return leaveDate;
	}


	public void setLeaveDate(String leaveDate) {
		this.leaveDate = leaveDate;
	}


	public String getCorpPosition() {
		return corpPosition;
	}


	public void setCorpPosition(String corpPosition) {
		this.corpPosition = corpPosition;
	}


	public String getFavo1() {
		return favo1;
	}


	public void setFavo1(String favo1) {
		this.favo1 = favo1;
	}


	public String getFavo2() {
		return favo2;
	}


	public void setFavo2(String favo2) {
		this.favo2 = favo2;
	}


	public String getFavo3() {
		return favo3;
	}


	public void setFavo3(String favo3) {
		this.favo3 = favo3;
	}


	public String getFavo4() {
		return favo4;
	}


	public void setFavo4(String favo4) {
		this.favo4 = favo4;
	}


	public String getFavo5() {
		return favo5;
	}


	public void setFavo5(String favo5) {
		this.favo5 = favo5;
	}


	public String getFavo6() {
		return favo6;
	}


	public void setFavo6(String favo6) {
		this.favo6 = favo6;
	}


	public String getPcmngMonitor() {
		return pcmngMonitor;
	}


	public void setPcmngMonitor(String pcmngMonitor) {
		this.pcmngMonitor = pcmngMonitor;
	}


	public String getPushMonitor() {
		return pushMonitor;
	}


	public void setPushMonitor(String pushMonitor) {
		this.pushMonitor = pushMonitor;
	}


	public String getCorpType() {
		return corpType;
	}


	public void setCorpType(String corpType) {
		this.corpType = corpType;
	}


	public String getServiceGrade() {
		return serviceGrade;
	}


	public void setServiceGrade(String serviceGrade) {
		this.serviceGrade = serviceGrade;
	}


	public String getPartnerType() {
		return partnerType;
	}


	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}


	public String getCorpName() {
		return corpName;
	}


	public void setCorpName(String corpName) {
		this.corpName = corpName;
	}


	public String getCorpNum() {
		return corpNum;
	}


	public void setCorpNum(String corpNum) {
		this.corpNum = corpNum;
	}


	public String getCeoName() {
		return ceoName;
	}


	public void setCeoName(String ceoName) {
		this.ceoName = ceoName;
	}


	public String getCorpPhone() {
		return corpPhone;
	}


	public void setCorpPhone(String corpPhone) {
		this.corpPhone = corpPhone;
	}


	public String getCorpPhoneCtyCode() {
		return corpPhoneCtyCode;
	}


	public void setCorpPhoneCtyCode(String corpPhoneCtyCode) {
		this.corpPhoneCtyCode = corpPhoneCtyCode;
	}


	public String getCorpBusiness() {
		return corpBusiness;
	}


	public void setCorpBusiness(String corpBusiness) {
		this.corpBusiness = corpBusiness;
	}


	public String getCorpItem() {
		return corpItem;
	}


	public void setCorpItem(String corpItem) {
		this.corpItem = corpItem;
	}


	public String getAllocationUsed() {
		return allocationUsed;
	}


	public void setAllocationUsed(String allocationUsed) {
		this.allocationUsed = allocationUsed;
	}


	public String getApiKey() {
		return apiKey;
	}


	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}


	public long getApiExpired() {
		return apiExpired;
	}


	public void setApiExpired(long apiExpired) {
		this.apiExpired = apiExpired;
	}


	public int getGeoMaxCnt() {
		return geoMaxCnt;
	}


	public void setGeoMaxCnt(int geoMaxCnt) {
		this.geoMaxCnt = geoMaxCnt;
	}


	public String getMailAuth() {
		return mailAuth;
	}


	public void setMailAuth(String mailAuth) {
		this.mailAuth = mailAuth;
	}


	public String getGroupNm() {
		return groupNm;
	}


	public void setGroupNm(String groupNm) {
		this.groupNm = groupNm;
	}


	public String getUnitLength() {
		return unitLength;
	}


	public void setUnitLength(String unitLength) {
		this.unitLength = unitLength;
	}


	public String getUnitVolume() {
		return unitVolume;
	}


	public void setUnitVolume(String unitVolume) {
		this.unitVolume = unitVolume;
	}


	public String getUnitTemperature() {
		return unitTemperature;
	}


	public void setUnitTemperature(String unitTemperature) {
		this.unitTemperature = unitTemperature;
	}

	public String getUnitWeight() {
		return unitWeight;
	}


	public void setUnitWeight(String unitWeight) {
		this.unitWeight = unitWeight;
	}


	public String getUnitDate() {
		return unitDate;
	}


	public void setUnitDate(String unitDate) {
		this.unitDate = unitDate;
	}
			
	
}
