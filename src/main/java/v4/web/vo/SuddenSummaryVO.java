package v4.web.vo;

public class SuddenSummaryVO  {
	
	private int rapidStart;
	private int rapidStop;
	private int rapidAccel;
	private int rapidDeaccel;
	private int rapidTurn;
	private int rapidUtern;
	private int overSpeed;
	private int overSpeedLong;
	public int getRapidStart() {
		return rapidStart;
	}
	public void setRapidStart(int rapidStart) {
		this.rapidStart = rapidStart;
	}
	public int getRapidStop() {
		return rapidStop;
	}
	public void setRapidStop(int rapidStop) {
		this.rapidStop = rapidStop;
	}
	public int getRapidAccel() {
		return rapidAccel;
	}
	public void setRapidAccel(int rapidAccel) {
		this.rapidAccel = rapidAccel;
	}
	public int getRapidDeaccel() {
		return rapidDeaccel;
	}
	public void setRapidDeaccel(int rapidDeaccel) {
		this.rapidDeaccel = rapidDeaccel;
	}
	public int getRapidTurn() {
		return rapidTurn;
	}
	public void setRapidTurn(int rapidTurn) {
		this.rapidTurn = rapidTurn;
	}
	public int getRapidUtern() {
		return rapidUtern;
	}
	public void setRapidUtern(int rapidUtern) {
		this.rapidUtern = rapidUtern;
	}
	public int getOverSpeed() {
		return overSpeed;
	}
	public void setOverSpeed(int overSpeed) {
		this.overSpeed = overSpeed;
	}
	public int getOverSpeedLong() {
		return overSpeedLong;
	}
	public void setOverSpeedLong(int overSpeedLong) {
		this.overSpeedLong = overSpeedLong;
	}
	
	
	
}
