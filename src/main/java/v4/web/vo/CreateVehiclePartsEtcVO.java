package v4.web.vo;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;

public class CreateVehiclePartsEtcVO {
	
	@JsonIgnore
	private String corpKey;
	
	@JsonIgnore
	private String partsKey;
	
	
	@ApiModelProperty(value = "partsName",required = true)
	@NotEmpty(message = "partsNm is required")
	private String partsNm;
	
	
	public String getPartsKey() {
		return partsKey;
	}

	public void setPartsKey(String partsKey) {
		this.partsKey = partsKey;
	}

	public String getCorpKey() {
		return corpKey;
	}

	public void setCorpKey(String corpKey) {
		this.corpKey = corpKey;
	}

	public String getPartsNm() {
		return partsNm;
	}

	public void setPartsNm(String partsNm) {
		this.partsNm = partsNm;
	}
	
	
}
