package v4.web.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TripVO {
	
	private String deviceKey; //
	private long tripId; //
	private String tripKey; //
	private int tripSeq; //
	private String vehicleKey; //
	private String accountKey; //
	
	private int fco; 
	private double oilUnitPrice;
	private double oilPrice;
	private int distance;
	private int totDistance;
	private String phone;
	
	private int partTotal;
	private String part;
	private int tripState;
	
	private String startDate;
	private String endDate;
	private long startGpsTime;
	private long endGpsTime;
	private double startLat;
	private double startLon;
	private double endLat;
	private double endLon;
	
	private String startGisCode;
	private String endGisCode;
	
	private String startAddr;
	private String startAddrEn;
	private String endAddr;
	private String endAddrEn;
	
	
	private int coolantTemp;
	private int co2Emission;
	private int fuelCutTime;
	private int highSpeed;
	private int meanSpeed;
	private int idleTime;
	private double ecuVolt;
	private double deviceVolt;
	private int warmupTime;
	private int engineOilTemp;
	private int ecoTime;
	private int ecoSpeedTime;
	private String version;
	
	private String allocateKey;
	private double avgFco;
	private String purpose;
	
	private float ecoScore;
	private float safeScore;
	private float fuelScore;
	
	private float score1;
	private float score2;
	private float score3;
	private float score4;
	
	private String itemEmpty;
	private String dtcOn;
	
	private String workingAreaWarning;
	private String workingTimeWarning;
	
	private int under20;
	private int under40;
	private int under60;
	private int under80;
	private int under100;
	private int under120;
	private int under140;
	private int over140;
	
	private String tripMatched;
	
	private String shiftDay;
	private String fwVer,needAllocate,needReport,contents;
	
	public TripVO() {super();}
	public TripVO(String tripKey) {super(); this.tripKey = tripKey;}
	
	public TripVO(String deviceKey , long tripId , String accountKey , String vehicleKey ,long startGpsTime, int partTotal , String part , int tripState, String fwVer
			,String needAllocate , String allocateKey , String needReport , String purpose , String contents){
		super();
		this.deviceKey = deviceKey;
		this.tripId = tripId;
		this.accountKey = accountKey;
		this.vehicleKey = vehicleKey;
		this.partTotal = partTotal;
		this.part = part;
		this.tripState = tripState;
		this.startGpsTime = startGpsTime;
		this.fwVer = fwVer;
		this.needAllocate = needAllocate;
		this.allocateKey = allocateKey;
		this.needReport = needReport;
		this.purpose = purpose;
		this.contents = contents;
	}
/*
	public TripVO(String deviceKey, long identy, String tripId, String driverKey, String vehicleKey, int coolantTemp,
			int co2Emission, int fuelCutTime, int partTotal, String part,int highSpeed,int meanSpeed, int idleTime, double ecuVolt,
			double deviceVolt,int warmupTime,int engineOilTemp,int ecoTime,String version,int tripState) {
		super();
		this.deviceKey = deviceKey;
		this.identy = identy;
		this.tripId = tripId;
		this.driverKey = driverKey;
		this.vehicleKey = vehicleKey;
		this.coolantTemp = coolantTemp;
		this.co2Emission = co2Emission;
		this.fuelCutTime = fuelCutTime;
		this.partTotal = partTotal;
		this.part = part;
		this.highSpeed = highSpeed;
		this.meanSpeed = meanSpeed;
		this.idleTime = idleTime;
		this.ecuVolt = ecuVolt;
		this.deviceVolt = deviceVolt;
		this.warmupTime = warmupTime;
		this.engineOilTemp = engineOilTemp;
		this.ecoTime = ecoTime;
		this.version = version;
		this.tripState = tripState;
	}
	
	public TripVO(String deviceKey, int identy, String tripId, String driverKey, String vehicleKey, int fco,
			int distance, int totDistance, int partTotal, String part, String startDate, String endDate, double startLat,
			double startLon, double endLat, double endLon,int tripState) {
		super();
		this.deviceKey = deviceKey;
		this.identy = identy;
		this.tripId = tripId;
		this.driverKey = driverKey;
		this.vehicleKey = vehicleKey;
		this.fco = fco;
		this.distance = distance;
		this.totDistance = totDistance;
		this.partTotal = partTotal;
		this.part = part;
		this.startDate = startDate;
		this.endDate = endDate;
		this.startLat = startLat;
		this.startLon = startLon;
		this.endLat = endLat;
		this.endLon = endLon;
		this.tripState = tripState;
	}
	
	
	*/

	
	
	
	public long getStartGpsTime() {
		return startGpsTime;
	}
	public String getNeedAllocate() {
		return needAllocate;
	}
	public void setNeedAllocate(String needAllocate) {
		this.needAllocate = needAllocate;
	}
	public String getNeedReport() {
		return needReport;
	}
	public void setNeedReport(String needReport) {
		this.needReport = needReport;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	public String getFwVer() {
		return fwVer;
	}
	public void setFwVer(String fwVer) {
		this.fwVer = fwVer;
	}
	public void setStartGpsTime(long startGpsTime) {
		this.startGpsTime = startGpsTime;
	}
	public long getEndGpsTime() {
		return endGpsTime;
	}
	public void setEndGpsTime(long endGpsTime) {
		this.endGpsTime = endGpsTime;
	}
	public String getTripKey() {
		return tripKey;
	}

	public void setTripKey(String tripKey) {
		this.tripKey = tripKey;
	}

	public String getTripMatched() {
		return tripMatched;
	}

	public void setTripMatched(String tripMatched) {
		this.tripMatched = tripMatched;
	}

	public String getShiftDay() {
		return shiftDay;
	}

	public void setShiftDay(String shiftDay) {
		this.shiftDay = shiftDay;
	}

	public int getUnder20() {
		return under20;
	}

	public void setUnder20(int under20) {
		this.under20 = under20;
	}

	public int getUnder40() {
		return under40;
	}

	public void setUnder40(int under40) {
		this.under40 = under40;
	}

	public int getUnder60() {
		return under60;
	}

	public void setUnder60(int under60) {
		this.under60 = under60;
	}

	public int getUnder80() {
		return under80;
	}

	public void setUnder80(int under80) {
		this.under80 = under80;
	}

	public int getUnder100() {
		return under100;
	}

	public void setUnder100(int under100) {
		this.under100 = under100;
	}

	public int getUnder120() {
		return under120;
	}

	public void setUnder120(int under120) {
		this.under120 = under120;
	}

	public int getUnder140() {
		return under140;
	}

	public void setUnder140(int under140) {
		this.under140 = under140;
	}

	public int getOver140() {
		return over140;
	}

	public void setOver140(int over140) {
		this.over140 = over140;
	}

	public int getTripSeq() {
		return tripSeq;
	}

	public void setTripSeq(int tripSeq) {
		this.tripSeq = tripSeq;
	}

	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getStartGisCode() {
		return startGisCode;
	}
	public void setStartGisCode(String startGisCode) {
		this.startGisCode = startGisCode;
	}
	public String getEndGisCode() {
		return endGisCode;
	}
	public void setEndGisCode(String endGisCode) {
		this.endGisCode = endGisCode;
	}
	public int getEcoSpeedTime() {
		return ecoSpeedTime;
	}
	public void setEcoSpeedTime(int ecoSpeedTime) {
		this.ecoSpeedTime = ecoSpeedTime;
	}
	public String getWorkingAreaWarning() {
		return workingAreaWarning;
	}
	public void setWorkingAreaWarning(String workingAreaWarning) {
		this.workingAreaWarning = workingAreaWarning;
	}
	public String getWorkingTimeWarning() {
		return workingTimeWarning;
	}
	public void setWorkingTimeWarning(String workingTimeWarning) {
		this.workingTimeWarning = workingTimeWarning;
	}
	public String getDtcOn() {
		return dtcOn;
	}
	public void setDtcOn(String dtcOn) {
		this.dtcOn = dtcOn;
	}
	
	public String getItemEmpty() {
		return itemEmpty;
	}
	public void setItemEmpty(String itemEmpty) {
		this.itemEmpty = itemEmpty;
	}
	public float getScore1() {
		return score1;
	}
	public void setScore1(float score1) {
		this.score1 = score1;
	}
	public float getScore2() {
		return score2;
	}
	public void setScore2(float score2) {
		this.score2 = score2;
	}
	public float getScore3() {
		return score3;
	}
	public void setScore3(float score3) {
		this.score3 = score3;
	}
	public float getScore4() {
		return score4;
	}
	public void setScore4(float score4) {
		this.score4 = score4;
	}
	public float getEcoScore() {
		return ecoScore;
	}
	public void setEcoScore(float ecoScore) {
		this.ecoScore = ecoScore;
	}
	public float getSafeScore() {
		return safeScore;
	}
	public void setSafeScore(float safeScore) {
		this.safeScore = safeScore;
	}
	public float getFuelScore() {
		return fuelScore;
	}
	public void setFuelScore(float fuelScore) {
		this.fuelScore = fuelScore;
	}
	public int getEcoTime() {
		return ecoTime;
	}
	public void setEcoTime(int ecoTime) {
		this.ecoTime = ecoTime;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	public String getAccountKey() {
		return accountKey;
	}
	public void setAccountKey(String accountKey) {
		this.accountKey = accountKey;
	}
	public double getAvgFco() {
		return avgFco;
	}
	public void setAvgFco(double avgFco) {
		this.avgFco = avgFco;
	}
	public String getAllocateKey() {
		return allocateKey;
	}

	public void setAllocateKey(String allocateKey) {
		this.allocateKey = allocateKey;
	}

	public double getOilUnitPrice() {
		return oilUnitPrice;
	}

	public void setOilUnitPrice(double oilUnitPrice) {
		this.oilUnitPrice = oilUnitPrice;
	}

	public double getOilPrice() {
		return oilPrice;
	}

	public void setOilPrice(double oilPrice) {
		this.oilPrice = oilPrice;
	}

	public String getStartAddr() {
		return startAddr;
	}

	public void setStartAddr(String startAddr) {
		this.startAddr = startAddr;
	}

	public String getStartAddrEn() {
		return startAddrEn;
	}

	public void setStartAddrEn(String startAddrEn) {
		this.startAddrEn = startAddrEn;
	}

	public String getEndAddr() {
		return endAddr;
	}

	public void setEndAddr(String endAddr) {
		this.endAddr = endAddr;
	}

	public String getEndAddrEn() {
		return endAddrEn;
	}

	public void setEndAddrEn(String endAddrEn) {
		this.endAddrEn = endAddrEn;
	}

	public int getEngineOilTemp() {
		return engineOilTemp;
	}

	public void setEngineOilTemp(int engineOilTemp) {
		this.engineOilTemp = engineOilTemp;
	}

	public int getTripState() {
		return tripState;
	}

	public void setTripState(int tripState) {
		this.tripState = tripState;
	}

	public int getCoolantTemp() {
		return coolantTemp;
	}

	public void setCoolantTemp(int coolantTemp) {
		this.coolantTemp = coolantTemp;
	}

	public int getCo2Emission() {
		return co2Emission;
	}

	public void setCo2Emission(int co2Emission) {
		this.co2Emission = co2Emission;
	}

	public int getFuelCutTime() {
		return fuelCutTime;
	}

	public void setFuelCutTime(int fuelCutTime) {
		this.fuelCutTime = fuelCutTime;
	}

	public int getHighSpeed() {
		return highSpeed;
	}

	public void setHighSpeed(int highSpeed) {
		this.highSpeed = highSpeed;
	}

	public int getMeanSpeed() {
		return meanSpeed;
	}

	public void setMeanSpeed(int meanSpeed) {
		this.meanSpeed = meanSpeed;
	}

	public int getIdleTime() {
		return idleTime;
	}

	public void setIdleTime(int idleTime) {
		this.idleTime = idleTime;
	}

	public double getEcuVolt() {
		return ecuVolt;
	}
	public void setEcuVolt(double ecuVolt) {
		this.ecuVolt = ecuVolt;
	}
	public double getDeviceVolt() {
		return deviceVolt;
	}
	public void setDeviceVolt(double deviceVolt) {
		this.deviceVolt = deviceVolt;
	}
	public int getWarmupTime() {
		return warmupTime;
	}

	public void setWarmupTime(int warmupTime) {
		this.warmupTime = warmupTime;
	}

	public String getDeviceKey() {
		return deviceKey;
	}
	public void setDeviceKey(String deviceKey) {
		this.deviceKey = deviceKey;
	}
	
	public long getTripId() {
		return tripId;
	}
	public void setTripId(long tripId) {
		this.tripId = tripId;
	}
	public String getVehicleKey() {
		return vehicleKey;
	}
	public void setVehicleKey(String vehicleKey) {
		this.vehicleKey = vehicleKey;
	}
	public int getFco() {
		return fco;
	}
	public void setFco(int fco) {
		this.fco = fco;
	}
	public int getDistance() {
		return distance;
	}
	public void setDistance(int distance) {
		this.distance = distance;
	}
	public int getTotDistance() {
		return totDistance;
	}
	public void setTotDistance(int totDistance) {
		this.totDistance = totDistance;
	}
	public int getPartTotal() {
		return partTotal;
	}
	public void setPartTotal(int partTotal) {
		this.partTotal = partTotal;
	}
	
	public String getPart() {
		return part;
	}
	public void setPart(String part) {
		this.part = part;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public double getStartLat() {
		return startLat;
	}
	public void setStartLat(double startLat) {
		this.startLat = startLat;
	}
	public double getStartLon() {
		return startLon;
	}
	public void setStartLon(double startLon) {
		this.startLon = startLon;
	}
	public double getEndLat() {
		return endLat;
	}
	public void setEndLat(double endLat) {
		this.endLat = endLat;
	}
	public double getEndLon() {
		return endLon;
	}
	public void setEndLon(double endLon) {
		this.endLon = endLon;
	}
	
	
}
