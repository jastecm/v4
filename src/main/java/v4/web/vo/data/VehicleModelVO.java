package v4.web.vo.data;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@SuppressWarnings("unused")
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@Data
//@Accessors(fluent = true)
@NoArgsConstructor
@AllArgsConstructor
public class VehicleModelVO {
	private String listVersion;
	private String convCode;
	private String manufacture;
	private String dtcManufacture;
	private String modelMaster;
	private String modelHeader;
	private String year;		
	private String transmission;
	private String specifications;
	private String fuelType;
	private String fuel;
	private String cylinder;
	private String volume;
	private String fuelEfficiency;
	private String dbType;
	private String bcmSupport;
	private String srsSupport;
	private String dbVersion;
	private String usable;
	private String jastecmdDbIndex;
	private String fuelTankSize;
	private String tireSize;
	private String compoundFuelEfficiency;
	private String fuelEfficiencyRating;
	private String co2emissionQuantity;
	private String mileageAuth;
	private String batterySize;
	private String vinPattern;
	private String calIdPattern;
	private String carFreeDayAuth;
	private String fuelLevel;
	private String throttlePosition;
	private String brakePosition;
	private String imgL;
	private String imgS;
	private String country;
	private String appBcm;
}