package v4.web.vo.data;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import v4.web.vo.data.VehicleVO.LatestLocation;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("unused")
public class AllocateVO {
	private String allocateKey;
	
	private CorpVO corp;
	private AccountVO account;
	private AccountVO regAccount;
	private VehicleVO vehicle;
	private Date startDate;
	private Date endDate;
	private String purpose;
	private String title;
	private String passenger;
	private String destination;
	private String contents;
	private Date regDate;
	private Date modDate;
	
	@JsonAutoDetect(fieldVisibility = Visibility.ANY)
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Approval {
		private String state;
		private String maxStep;
		private String afterAllocate;
		private Date step1Date;
		private Date step2Date;
		private Date reserveDate;
		private AccountVO step1;
		private AccountVO step2;
		private AccountVO reserve;
	}
	private Approval approval;
	
	
	//사용여부확인
	private String returnFlag;
	private Date returnDate;
	private String returnLocation;
	private String returnLocationAddr;
	private String returnLocationAddrEn;
	private String returnLocationAddrDetail;
	private String instantAllocate;
	private String fixedType;
	private String reportFlag;
}
