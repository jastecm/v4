package v4.web.vo.data;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("unused")
public class MaintenanceDetailVO {
	private String maintenanceDetailKey;
	private String partsKey;
	private String memo;
	private String partsQuality;
	private int partsCnt;
	private int partsPay;
	private int wagePay;
	private int partsOrder;
	private String partsNm;
}
