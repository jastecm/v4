package v4.web.vo.data;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import v4.web.vo.data.AllocateVO.Approval;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MaintenanceVO {
	
	private String maintenanceKey;
	private String vehicleKey;
	private Date periodicInspection;
	private Date nextPeriodicInspection;
	private String accountKey;
	private String groupKey;
	private Date maintenanceReqDate;
	private Date completeReqDate;
	private String troubleDesc;
	private String garage;
	private int distance;
	private Date repairDate;
	private String maintenanceHistory;
	
	private CorpVO corp;
	private AccountVO account;
	private VehicleVO vehicle;
	private Approval approval;
	private GroupVO group;
	
	private List<MaintenanceDetailVO> detail;
}
