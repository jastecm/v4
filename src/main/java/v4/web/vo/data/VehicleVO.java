package v4.web.vo.data;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@Data
// @Accessors(fluent = true)
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("unused")
public class VehicleVO {

	private String vehicleKey;
	private String plateNum;
	private String vehicleType;
	private String locationSetting;
	private String holidayPass;
	private String color;
	private String vehicleBizType;
	private String vehicleBodyNum;
	private String vehicleInsureNm;
	private String vehicleInsurePhone;
	private String vehicleDescript;
	private String drivingState;
	private String lang;
	private String vehicleMasterFlag = "1";
	private String geofenceMonitor;
	private Integer vehiclePayment;
	private Integer accureDistance;
	private Integer totDistanceCollectionValue;
	private Double totDistance;
	private Date vehicleRegDate;
	private Date vehicleMaintenanceDate;
	private Date regDate;
	private String latestDtcKey;
	private int latestDtcCnt;

	@JsonAutoDetect(fieldVisibility = Visibility.ANY)
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class LatestLocation {
		private String location;
		private Date locationTime;
		private String locationAddr;
		private String locationAddrDetail;
	}

	private LatestLocation latestLocation;

	private DeviceVO device;
	private VehicleModelVO vehicleModel;
	private CorpVO corp;
	private AccountVO defaultDriver;
	private AccountVO vehicleManager;

	@JsonAutoDetect(fieldVisibility = Visibility.ANY)
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class FixedInfo {
		private AccountVO fixedAccount;
		private AccountVO fixedDriver;
	}

	private FixedInfo fixedInfo;
	private String fixed;

	private TripVO latestTrip;
	private TripVO latestStartTrip;
	private List<GroupVO> groups;

	private String autoReport, autoReportPurpose, autoReportContents;

	public String getDefaultAccountKey() {
		return (null != defaultDriver) ? defaultDriver.getAccountKey() : null;
	}

	public String getFixedAccountKey() {
		return (null != fixedInfo && null != fixedInfo.getFixedAccount()) ? fixedInfo.getFixedAccount().getAccountKey()
				: null;
	}

	public String getFixedDriverKey() {
		return (null != fixedInfo && null != fixedInfo.getFixedDriver()) ? fixedInfo.getFixedDriver().getAccountKey()
				: null;
	}

	public String getManagerKey() {
		if (vehicleManager != null)
			return vehicleManager.getAccountKey();
		else
			return (null != corp && null != corp.getManager() && null != corp.getManager().getVehicleManager())
					? corp.getManager().getVehicleManager().getAccountKey() : null;
	}

	public String getDeviceKey() {
		return (null != device && null != device.getDeviceKey()) ? device.getDeviceKey() : null;
	}

	public String getDeviceId() {
		return (null != device && null != device.getDeviceId()) ? device.getDeviceId() : null;
	}

	public String getDeviceSn() {
		return (null != device && null != device.getDeviceSn()) ? device.getDeviceSn() : null;
	}

}
