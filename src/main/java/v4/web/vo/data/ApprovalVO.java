package v4.web.vo.data;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import v4.web.vo.data.CorpVO.ApprovalLine;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("unused")
public class ApprovalVO {
	private String allocateApproval,maintenanceApproval,expensesApproval,accidentApproval;
	
	@JsonAutoDetect(fieldVisibility = Visibility.ANY)
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Allocate {
		private AccountVO allocateApprovalStep1; //
		private AccountVO allocateApprovalStep2; //
		private String autoAllocateApprovalStep1; //
		private String autoAllocateApprovalStep2; //
	}
	private Allocate allocate;
	
	@JsonAutoDetect(fieldVisibility = Visibility.ANY)
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Maintenance {
		private AccountVO maintenanceApprovalStep1; //
		private AccountVO maintenanceApprovalStep2; //
		private String autoMaintenanceApprovalStep1; //
		private String autoMaintenanceApprovalStep2; //
	}
	private Maintenance maintenance;
	
	@JsonAutoDetect(fieldVisibility = Visibility.ANY)
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Expenses {
		private AccountVO expensesApprovalStep1; //
		private AccountVO expensesApprovalStep2; //
		private String autoExpensesApprovalStep1; //
		private String autoExpensesApprovalStep2; //
	}
	private Expenses expenses;
	
	@JsonAutoDetect(fieldVisibility = Visibility.ANY)
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Accident {
		private AccountVO accidentApprovalStep1; //
		private AccountVO accidentApprovalStep2; //
		private AccountVO accidentApprovalStep3; //
	}
	private Accident accident;
}