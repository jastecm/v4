package v4.web.vo.data;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@Data
//@Accessors(fluent = true)
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("unused")
public class TripAnalysisVO{
	
	@JsonAutoDetect(fieldVisibility = Visibility.ANY)
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Summary {
		private int tripCnt;
		private int distance;
		private int fco;
		private int oilPrice;
		private double officialFuelEff;
		private double fuelEff;
		
		private int safeScore;
		private int ecoScore;
		private int score1;
		private int score2;
		private int score3;
		private int score4;
		private int score5;
		
		private int drivingTime;
		private int stopTime;
		private int parkingTime;
		private int co2Emission;
		
		private int purposeWorkingCnt;
		private int purposeNoneWorkingCnt;
		private int purposeCommutingCnt;
	}
	private Summary summary;
	private Summary summaryPrev;
	
	@JsonAutoDetect(fieldVisibility = Visibility.ANY)
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class SummaryWeek extends Summary{
		private int weekNumIdx;
	}
	private List<SummaryWeek> summaryWeek;
	
	@JsonAutoDetect(fieldVisibility = Visibility.ANY)
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class SummaryHour {
		
		private int hour0,hour1,hour2,hour3,hour4,hour5,hour6,hour7,hour8,hour9,hour10,hour11,hour12,hour13,hour14,hour15,hour16,hour17,hour18,hour19,hour20,hour21,hour22,hour23;
		
	}
	private SummaryHour summaryHourCnt;
	
	@JsonAutoDetect(fieldVisibility = Visibility.ANY)
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class SummaryDay {
		
		private int sun,mon,tue,wed,thu,fri,sat;
		
	}
	private SummaryDay summaryDayCnt;
	
	@JsonAutoDetect(fieldVisibility = Visibility.ANY)
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class SummaryGeofence {
		
		private int cnt;
		private String alies;
		
	}
	private List<SummaryGeofence> summaryGeofenceCnt;
	
	@JsonAutoDetect(fieldVisibility = Visibility.ANY)
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class SummaryCity {
		
		private int cnt;
		private String cityNm;
		
	}
	private List<SummaryCity> summaryCityCnt;
}
