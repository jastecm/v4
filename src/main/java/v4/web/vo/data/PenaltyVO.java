package v4.web.vo.data;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import v4.web.vo.FileVO;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PenaltyVO {

	private String corpKey;
	private String penaltyKey;
	private String accountKey;
	private String vehicleKey;
	private String business;
	private Date crackDownDate;
	private String penaltyType;
	private String violationType;
	private String reason;
	private String location;
	private String jurisdiction;
	private String billImg;
	private String paymentType;
	private Date deadLineDate;
	private String plateNum;
	
	private CorpVO corp;
	private AccountVO account;
	private VehicleVO vehicle;
	private FileVO file;
}
