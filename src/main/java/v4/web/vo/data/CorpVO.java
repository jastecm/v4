package v4.web.vo.data;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@Data
//@Accessors(fluent = true)
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("unused")
public class CorpVO {
	private String corpKey;
	private String corpType;
	private String serviceGrade;
	private String partnerType;
	private String corpName;
	private String corpNum;
	private String ceoName;
	private String corpPhone;
	private String corpPhoneCtyCode;
	private String corpBusiness;
	private String corpItem;
	private String allocationUsed;
	private String apiKey;
	private String apiExpired;
	private Integer geoMaxCnt;
	private Date regDate;
	private String unitCurrency;
	
	@JsonAutoDetect(fieldVisibility = Visibility.ANY)
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Manager {
		private AccountVO vehicleManager;
		private AccountVO topManager;
	}
	private Manager manager;
	
	@JsonAutoDetect(fieldVisibility = Visibility.ANY)
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class ApprovalLine {
		private String allocateApproval;
		private String allocateApprovalStep1; //
		private String allocateApprovalStep2; //
		private String autoAllocateApprovalStep1; //
		private String autoAllocateApprovalStep2; //
		private String maintenanceApproval;
		private String maintenanceApprovalStep1; //
		private String maintenanceApprovalStep2; //
		private String autoMaintenanceApprovalStep1; //
		private String autoMaintenanceApprovalStep2; //
		private String expensesApproval;
		private String expensesApprovalStep1; //
		private String expensesApprovalStep2; //
		private String autoExpensesApprovalStep1; //
		private String autoExpensesApprovalStep2; //
		private String accidentApproval;
		private String accidentApprovalStep1; //
		private String accidentApprovalStep2; //
		private String accidentApprovalStep3; //
	}
	private ApprovalLine approval;
	
}