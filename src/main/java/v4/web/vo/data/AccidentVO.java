package v4.web.vo.data;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import v4.web.vo.FileVO;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccidentVO {
	
	private String corpKey;
	private String accidentKey;
	private String accountKey;
	private String vehicleKey;
	private Date accidentDate;
	private String location;
	private String outline;
	private String accidentImg1;
	private String accidentImg2;
	private String accidentImg3;
	private String opponentPlateNum;
	private String opponentName;
	private String opponentPhone;
	private String insuranceCompany;
	private String insuranceReceiptNumber;
	private String insuranceResult;
	private String repairDesc;
	private String cost;
	private String costType;
	private String insureNm;
	private Date regDate;
	
	@JsonAutoDetect(fieldVisibility = Visibility.ANY)
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class AccidentApproval {
		private String state;
		private String maxStep;
		private Date step1Date;
		private Date step2Date;
		private Date step3Date;
		private Date step4Date;
		private Date step5Date;
		private Date step6Date;
		private AccountVO step1;
		private AccountVO step2;
		private AccountVO step3;
		private AccountVO step4;
		private AccountVO step5;
		private AccountVO step6;
		private AccountVO reserve1;
		private Date reserve1Date;
		private AccountVO reserve2;
		private Date reserve2Date;
	}
	private AccidentApproval approval;
	
	private CorpVO corp;
	private AccountVO account;
	private VehicleVO vehicle;
	
	private FileVO file1;
	private FileVO file2;
	private FileVO file3;

}
