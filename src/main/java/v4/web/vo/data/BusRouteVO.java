package v4.web.vo.data;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BusRouteVO {
	private String corp;
	private String busRouteKey;
	private String drivingPurpose;
	private String busRouteName;
	private String busRouteImg;
	private String busRouteImgName;
	private String busRouteKeyword;
	private String regUser;
	private Date regDate;
	private String stationNameGroup;
	private String firstArrivalTime;
	private String lastArrivalTime;
	private String drivingPurposeName;
	private int favoritesCnt;
	
	private DrivingPurposeVO drivingPurposeInfo;
	
	private List<BusStationVO> busStation;
	private List<BusMappingVO> busMapping;
	
	
}
