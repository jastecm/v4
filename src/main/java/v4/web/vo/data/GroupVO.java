package v4.web.vo.data;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@Data
//@Accessors(fluent = true)
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("unused")
public class GroupVO {
	private String groupKey;
	private String corpKey;
	private String groupNm;
	private String parentGroupNm;
	
	private String parentGroupKey;
	private Date regDate;
	private Integer userCount;
	private Integer vehicleCount;
	
	private AccountVO manager;
}
