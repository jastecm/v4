package v4.web.vo.data;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("unused")
public class ItemVO{
	
	private int val;
	private int max;
	private String name;
	private String itemKey;
	private String partsKey;
	private PartsVO parts;
	private String sort;
	
	private double per;
	private Date regDate;
	private int warningRemainDay,emptyRemainDay;
	
}
