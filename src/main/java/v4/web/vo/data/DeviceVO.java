package v4.web.vo.data;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@Data
//@Accessors(fluent = true)
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("unused")
public class DeviceVO {
	private String deviceSn;
	private String deviceId;
	private String appEui;
	private String deviceSeries;
	private String corpKey;
	private String adminCorp;
	private String connectionKey;
	private String firmNm;
	private String dbNm;
	
	private Date latestPacket;
	private Date expireDate;
	
	private String helloFwVer;
	private String helloDbVer;
	private String helloCylinder;
	private String helloVolume;
	private String helloFuelType;
	private String helloFuel;
	private String helloEngType;
	private String helloEUI;
	private String helloVIN;
	
	private Date helloCurTime;
	private Date helloDate;
	
	private String provisionDevEui;
	private String provisionAppEui;
	private String provisionAppKey;
	private String provisionSN;
	private String provisionConverSN;
	
	private Date provisionDate;
	
	private String faultCode;
	private String faultType;
	private Date faultTime;
	private Date faultDate;
	
	private String beaconMaj;
	private String beaconMin;
	private String latestPaymentType;
	private Integer shiftDay;
	
	private String downloadConvCode;
	private String downloadDb;
	private String downloadDeviceSound;
	private String downloadFirm;
	private Integer downloadTotDistance;
	
	private String deviceKey;
	private VehicleVO vehicle;
}