package v4.web.vo.data;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@Data
//@Accessors(fluent = true)
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("unused")
public class AccountVO {
	private String accountId;
	private String accountKey;
	private String fcmKey;
	private String mailAuth;
	private String smsAuth;
	private String mobilePhone;
	private String mobilePhoneCtyCode;
	private String phone;
	private String phoneCtyCode;
	private String rule;
	private String subRule;
	private String addr;
	private String lang;
	private String ctyCode;
	private String timezoneOffset;
	private String name;
	private Date birth;
	private String blood;
	private String bloodRh;
	private String gender;
	private String serviceAccepted;
	private String privateAccepted;
	private String locationAccepted;
	private String marketingAccepted;
	private String smsAccepted;
	private Date joinDate;
	private Date leaveDate;
	private String imgId;
	private String descript;
	private String corpPosition;
	private String favo1;
	private String favo2;
	private String favo3;
	private String favo4;
	private String favo5;
	private String favo6;
	private String pcmngMonitor;
	private String pushMonitor;
	private String unitLength;
	private String unitVolume;
	private String unitTemperature;
	private String unitWeight;
	private String unitDate;
	
	private GroupVO group;
	private CorpVO corp;
	/*private TripVO latestTrip;*/
	private VehicleVO latestVehicle;
	private int accureDistance,accureDrivingTime;
}
