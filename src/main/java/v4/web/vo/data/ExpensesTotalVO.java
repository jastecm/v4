package v4.web.vo.data;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExpensesTotalVO {
	
	private Integer oilPrice;
	private Double oilLiquid;
	private Integer passageMoney;
	private Integer parking;
	private Integer carWash;
	private Integer accessories;
	private Integer rental;
	private Integer insurance;
	private Integer etc;
	private Integer totalPrice;
	

}
