package v4.web.vo.data;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import v4.web.vo.data.VehicleVO.LatestLocation;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@Data
//@Accessors(fluent = true)
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("unused")
public class TrackInfoVO{
	private String tripKey;	
	private String tripId;
	private String phone;
	private Date regDate;
	private Date modDate;
	private Date startDate;
	private Date endDate;
	private Integer distance;
	private Integer totDistance;
	private Integer fco;
	private Double avgFco;
	private Integer oilUnitPrice;
	private Integer oilPrice;
	private Double startLat;
	private Double startLon;
	private String startAddr;
	private String startAddrEn;
	private Double endLat;
	private Double endLon;
	private String endAddr;
	private String endAddrEn;
	private String startGisCode;
	private String endGisCode;
	private String workingAreaWarning;
	private String workingTimeWarning;
	private Integer partTotal;
	private String part;
	private Integer tripState;
	private Integer coolantTemp;
	private Integer co2Emission;
	private Integer fuelCutTime;
	private Integer highSpeed;
	private Integer meanSpeed;
	private Integer idleTime;
	private Double ecuVolt;
	private Double deviceVolt;
	private Integer warmupTime;
	private Integer engineOilTemp;
	private Integer ecoTime;
	private Integer ecoSpeedTime;
	private String purpose;
	private Float safeScore;
	private Float ecoScore;
	private Float fuelScore;
	private String itemEmpty;
	private String dtcOn;
	private Float score1, score2, score3, score4;
	private String encrypt;
	private String version;
	private Integer under20, under40, under60, under80, under100, under120, under140, over140;
	private String shiftDay, tripMatched , holyday;
	
	@JsonAutoDetect(fieldVisibility = Visibility.ANY)
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class TrackRecord {
		private double lat,lon;
		private int rapidStart,rapidStop,rapidAccel,rapidDeaccel,rapidTurn,rapidUtern,overSpeed,overSpeedLong,seq;
		private Date gpsTime;
		private Date regTime;
		private int rpm,speed,fco,distance;
		private String ver;
		private boolean gpsAccuracyOut = false;
	}
	private List<TrackRecord> trackRecord;
		
	@JsonAutoDetect(fieldVisibility = Visibility.ANY)
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class MatchedTrackRecord {
		private double lat,lon;
		private int rapidStart,rapidStop,rapidAccel,rapidDeaccel,rapidTurn,rapidUtern,overSpeed,overSpeedLong,seq,orgSeq;
		private Date regTime;
		private int matchedSpeed,matchedRoadCategory;
	}
	private List<MatchedTrackRecord> matchedTrackRecord;
	
}
