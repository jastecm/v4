package v4.web.vo.data;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("unused")
public class PartsVO{
	
	private String partsKey;
	private String partsType;
	private String partsTypeNm;
	private String partsNum;
	private String partsNm;
	private int partsPay;
	private int partsWage;
	private int defaultMax;
	
}
