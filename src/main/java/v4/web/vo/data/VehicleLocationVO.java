package v4.web.vo.data;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import v4.web.vo.data.VehicleVO.LatestLocation;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@Data
//@Accessors(fluent = true)
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("unused")
public class VehicleLocationVO {
	
	private String vehicleKey;
	private String corpKey;
	private String plateNum;	
	private String vehicleType;	
	private String drivingState;
	private String allocateState;
	
	@JsonAutoDetect(fieldVisibility = Visibility.ANY)
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class LatestLocation {
		private String location;
		private Date locationTime;
		private String locationAddr;
		private String locationAddrDetail;
	}
	private LatestLocation latestLocation;
	
	private AccountVO driver;	
	
}
