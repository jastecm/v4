package v4.web.vo.data;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import v4.web.vo.data.AllocateVO.Approval;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("unused")
public class ExpensesVO {
	
	private String type;
	private String expensesKey;
	private Date expensesDate;
	private String vehicleKey;
	private int oilPrice;
	private double oilLiquid;
	private int passageMoney;
	private int parking;
	private int carWash;
	private int accessories;
	private int rental;
	private int insurance;
	private int etc;
	private int totalSum;
	private String oilImg;
	private String passageMoneyImg;
	private String carWashImg;
	private String accessoriesImg;
	private String rentalImg;
	private String insuranceImg;
	private String etcImg;
	private String memo;
	
	
	private CorpVO corp;
	private AccountVO account;
	private VehicleVO vehicle;
	
	private Approval approval;
}
