package v4.web.vo.data;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BusStationVO {
	private String busRouteKey;
	private String stationName;
	private String arrivalTime;
	private String stationImg;
	private String stationImgName;
	private String address;
	private String lon;
	private String lat;
	private String busStationOrder;
	private String regDate;
	
}
