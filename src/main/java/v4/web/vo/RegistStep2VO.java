package v4.web.vo;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RegistStep2VO {
	
	@ApiModelProperty(value = "accountId type email",required = true)
	@NotEmpty(message = "accountId is required")
	@Pattern(regexp = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$" , message = "userId is email type")
	private String accountId;
	
	@ApiModelProperty(required = true ,value = "accountPw")
	//@Pattern(regexp = "^(?=.*[a-zA-Z])(?=.*[!@#$%^\\&*\\(\\)\\_\\+\\|\\-\\=\\\\\\[\\];'?\\.\\,])(?=.*[0-9]).{8,20}$" , message = "bad userPw(size 8~20 , en , )")
	@Pattern(regexp = "^.{8,20}$" , message = "bad accountPw(size 8~20)")
	private String accountPw;

	@ApiModelProperty(required = true ,value = "corp type")
	@Pattern(regexp = "^1$|^2$|^3$|^4$" , message = "corpType only 1(person),2(corp),3(rent),4(partner)")
	private String corpType;
	
	
	

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	

	public String getAccountPw() {
		return accountPw;
	}

	public void setAccountPw(String accountPw) {
		this.accountPw = accountPw;
	}

	public String getCorpType() {
		return corpType;
	}

	public void setCorpType(String corpType) {
		this.corpType = corpType;
	}

			
}
