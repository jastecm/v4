package v4.web.vo;

public class VehiclePartsVO {
	private String partsKey;
	private String partsType;
	private String partsTypeNm;
	private String partsNum;
	private String partsNm;
	private String itemFlag;
	private String partsPay;
	private String partsWage;
	private String defaultMax;
	
	public String getPartsKey() {
		return partsKey;
	}
	public void setPartsKey(String partsKey) {
		this.partsKey = partsKey;
	}
	public String getPartsType() {
		return partsType;
	}
	public void setPartsType(String partsType) {
		this.partsType = partsType;
	}
	public String getPartsTypeNm() {
		return partsTypeNm;
	}
	public void setPartsTypeNm(String partsTypeNm) {
		this.partsTypeNm = partsTypeNm;
	}
	public String getPartsNum() {
		return partsNum;
	}
	public void setPartsNum(String partsNum) {
		this.partsNum = partsNum;
	}
	public String getPartsNm() {
		return partsNm;
	}
	public void setPartsNm(String partsNm) {
		this.partsNm = partsNm;
	}
	public String getItemFlag() {
		return itemFlag;
	}
	public void setItemFlag(String itemFlag) {
		this.itemFlag = itemFlag;
	}
	public String getPartsPay() {
		return partsPay;
	}
	public void setPartsPay(String partsPay) {
		this.partsPay = partsPay;
	}
	public String getPartsWage() {
		return partsWage;
	}
	public void setPartsWage(String partsWage) {
		this.partsWage = partsWage;
	}
	public String getDefaultMax() {
		return defaultMax;
	}
	public void setDefaultMax(String defaultMax) {
		this.defaultMax = defaultMax;
	}
			
}
