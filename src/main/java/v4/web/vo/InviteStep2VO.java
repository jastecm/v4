package v4.web.vo;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;

public class InviteStep2VO {
	@ApiModelProperty(value = "accountId type email",required = true)
	@NotEmpty(message = "accountId is required")
	@Pattern(regexp = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$" , message = "userId is email type")
	private String accountId;
	
	@ApiModelProperty(value = "corpKey",required = true)
	@NotEmpty(message = "corpKey is required")
	private String corpKey;
	
	@ApiModelProperty(value = "smsAccepted" , allowableValues = "1,0" , required = true)
	@Pattern(regexp ="^1$|^0$" , message = "Invalid accept type (1,0)")
	@NotEmpty(message = "smsAccepted.required")
	private String smsAccepted = "1";
	
	@ApiModelProperty(value = "locationAccepted" , allowableValues = "1,0" , required = true)
	@Pattern(regexp ="^1$|^0$" , message = "Invalid accept type (1,0)")
	@NotEmpty(message = "locationAccepted.required")
	private String locationAccepted;
	
	@ApiModelProperty(value = "privateAccepted" , allowableValues = "1,0" , required = true)
	@Pattern(regexp ="^1$|^0$" , message = "Invalid accept type (1,0)")
	@NotEmpty(message = "privateAccepted.required")
	private String privateAccepted;

	@ApiModelProperty(value = "marketingAccepted" , allowableValues = "1,0" , required = true)
	@Pattern(regexp ="^1$|^0$" , message = "Invalid accept type (1,0)")
	@NotEmpty(message = "marketingAccepted.required")
	private String marketingAccepted;
	
	@ApiModelProperty(value = "serviceAccepted" , allowableValues = "1,0" , required = true)
	@Pattern(regexp ="^1$|^0$" , message = "Invalid accept type (1,0)")
	@NotEmpty(message = "serviceAccepted.required")
	private String serviceAccepted;

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getCorpKey() {
		return corpKey;
	}

	public void setCorpKey(String corpKey) {
		this.corpKey = corpKey;
	}

	public String getSmsAccepted() {
		return smsAccepted;
	}

	public void setSmsAccepted(String smsAccepted) {
		this.smsAccepted = smsAccepted;
	}

	public String getLocationAccepted() {
		return locationAccepted;
	}

	public void setLocationAccepted(String locationAccepted) {
		this.locationAccepted = locationAccepted;
	}

	public String getPrivateAccepted() {
		return privateAccepted;
	}

	public void setPrivateAccepted(String privateAccepted) {
		this.privateAccepted = privateAccepted;
	}

	public String getMarketingAccepted() {
		return marketingAccepted;
	}

	public void setMarketingAccepted(String marketingAccepted) {
		this.marketingAccepted = marketingAccepted;
	}

	public String getServiceAccepted() {
		return serviceAccepted;
	}

	public void setServiceAccepted(String serviceAccepted) {
		this.serviceAccepted = serviceAccepted;
	}
	
	
}
