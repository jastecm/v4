package v4.web.vo;

import java.util.List;

import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value="CreateExpensesVO" , description="template create expenses value object")
@JsonIgnoreProperties(ignoreUnknown = true)
public @Data class CreateExpensesVO {
	
	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String corp;
	
	@ApiModelProperty(hidden=true)
	@JsonIgnore
	private String accountKey;
	
	@ApiModelProperty(value = "type",required = true)
	@Pattern(regexp ="^1$|^2$" , message = "Invalid accept type (1,2)")
	private String type;
	
	@ApiModelProperty(value = "expensesDate",required = true)
	private long expensesDate;
	
	@ApiModelProperty(value = "vehicleKey",required = true)
	private String vehicleKey;
	
	@ApiModelProperty(value = "oilPrice",required = true)
	private int oilPrice;
	
	@ApiModelProperty(value = "oilLiquid",required = true)
	private double oilLiquid;
	
	@ApiModelProperty(value = "oilImg")
	private String oilImg;
	
	@ApiModelProperty(value = "passageMoney",required = true)
	private int passageMoney;
	
	@ApiModelProperty(value = "passageMoneyImg")
	private String passageMoneyImg;
	
	@ApiModelProperty(value = "parking",required = true)
	private int parking;
	
	@ApiModelProperty(value = "parkingImg")
	private String parkingImg;
	
	@ApiModelProperty(value = "carWash",required = true)
	private int carWash;
	
	@ApiModelProperty(value = "carWashImg")
	private String carWashImg;
	
	@ApiModelProperty(value = "accessories",required = true)
	private int accessories;
	
	@ApiModelProperty(value = "accessoriesImg")
	private String accessoriesImg;
	
	@ApiModelProperty(value = "rental",required = true)
	private int rental;
	
	@ApiModelProperty(value = "rentalImg")
	private String rentalImg;
	
	@ApiModelProperty(value = "insurance",required = true)
	private int insurance;
	
	@ApiModelProperty(value = "insuranceImg")
	private String insuranceImg;
	
	@ApiModelProperty(value = "etc",required = true)
	private int etc;
	
	@ApiModelProperty(value = "etcImg")
	private String etcImg;
	
	@ApiModelProperty(value = "etcImg")
	private String memo;
	
	//private List<CreateExpensesListVO> expensesList;
	
	/*@ApiModelProperty(value = "expensesDate",required = true)
	private List<String> expensesDate;
	
	@ApiModelProperty(value = "vehicleKey",required = true)
	private List<String> vehicleKey;
	
	@ApiModelProperty(value = "oilPrice",required = true)
	private List<Integer> oilPrice;
	
	@ApiModelProperty(value = "oilLiquid",required = true)
	private List<Double> oilLiquid;
	
	@ApiModelProperty(value = "oilImg")
	private List<String> oilImg;
	
	@ApiModelProperty(value = "passageMoney",required = true)
	private List<Integer> passageMoney;
	
	@ApiModelProperty(value = "passageMoneyImg")
	private List<String> passageMoneyImg;
	
	@ApiModelProperty(value = "parking",required = true)
	private List<Integer> parking;
	
	@ApiModelProperty(value = "parkingImg")
	private List<String> parkingImg;
	
	@ApiModelProperty(value = "carWash",required = true)
	private List<Integer> carWash;
	
	@ApiModelProperty(value = "carWashImg")
	private List<String> carWashImg;
	
	@ApiModelProperty(value = "accessories",required = true)
	private List<Integer> accessories;
	
	@ApiModelProperty(value = "accessoriesImg")
	private List<String> accessoriesImg;
	
	@ApiModelProperty(value = "rental",required = true)
	private List<Integer> rental;
	
	@ApiModelProperty(value = "rentalImg")
	private List<String> rentalImg;
	
	@ApiModelProperty(value = "insurance",required = true)
	private List<Integer> insurance;
	
	@ApiModelProperty(value = "insuranceImg")
	private List<String> insuranceImg;
	
	@ApiModelProperty(value = "etc",required = true)
	private List<Integer> etc;
	
	@ApiModelProperty(value = "etcImg")
	private List<String> etcImg;
	
	@ApiModelProperty(value = "etcImg")
	private List<String> memo;*/
	
	
}
