package v4.web.vo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author woong
 *
 */

@ApiModel(value="CreateEtcVO" , description="template create ETC value object")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateEtcVO {
	
	@ApiModelProperty(value = "plateNum",required = true)
	@NotEmpty(message = "plateNum is required")
	private String plateNum;
	
	
	///////////필수항목 끝/////////////
	@ApiModelProperty(value = "totDistance(unit. km)")
	private double totDistance = 0;
	
	@ApiModelProperty(value = "modelMaster")
	private String modelMaster;
	
	@ApiModelProperty(value = "modelHeader")
	private String modelHeader;
	
	@ApiModelProperty(value = "manufacture")
	private String manufacture;
	
	@ApiModelProperty(value = "year")
	private String year;
	
	@ApiModelProperty(value = "volume")
	private String volume;
	
	@ApiModelProperty(value = "transmission")
	private String transmission;
	
	@ApiModelProperty(value = "fuelType")
	private String fuelType;
	
	
	@ApiModelProperty(value = "specifications")
	private String specifications;
	
	///////////권장필드 끝//////////
	@ApiModelProperty(value = "vehicleType" , hidden=true)
	@Pattern(regexp = "^1$|^2$|^3$|^4$" , message = "vehicleType only 1(vehicle),2(Bus),3(Truck),4(ETC)")
	private String vehicleType = "4";
	
	//서브타입이 2종류이상 있다면 히든제거후 required
	@ApiModelProperty(value = "vehicleSubType" , hidden=true)
	private String vehicleSubType = "1";
	
	@ApiModelProperty(value = "corpGroupKey")
	private String corpGroupKey;
	
	////////////////////////////////////////
	
	
	@ApiModelProperty(value = "vehicle color")
	private String color;
	
	
	//개인,리스,랜탈,기타 외 다른타입을 쓴다면 5번부터 추가할것(5 : 지입?)
	@ApiModelProperty(value = "vehicleBizType")
	@Pattern(regexp = "^1$|^2$|^3$|^4$|^$" , message = "vehicleBizType only 1(personal),2(ris),3(rent),4(ETC),''")
	private String vehicleBizType = "1";
	
	@ApiModelProperty(value = "vehicleRegDate")
	private Long vehicleRegDate;
	
	@ApiModelProperty(value = "approvalType" , hidden = true)
	private String approvalType = "0";
	
	@ApiModelProperty(value = "vehicleBodyNum")
	private String vehicleBodyNum;
	
	@ApiModelProperty(value = "vehicleMaintenanceDate")
	private Long vehicleMaintenanceDate;
	
	@ApiModelProperty(value = "vehiclePayment")	
	private int vehiclePayment = 0;
	
	@ApiModelProperty(value = "vehicleInsureNm")
	private String vehicleInsureNm;
	
	@ApiModelProperty(value = "vehicleInsurePhone")
	private String vehicleInsurePhone;
	
	@ApiModelProperty(value = "vehicleDescript")
	private String vehicleDescript;
	
	
	@ApiModelProperty(value = "vehicleKey", hidden = true)
	private String vehicleKey;
	
	@ApiModelProperty(hidden = true)
	@JsonIgnore
	private String corpKey;
	
	@ApiModelProperty(hidden = true)
	@JsonIgnore
	private String defaultAccountKey;

	@ApiModelProperty(hidden = true)
	@JsonIgnore
	private String regUser;

	public String getPlateNum() {
		return plateNum;
	}

	public void setPlateNum(String plateNum) {
		this.plateNum = plateNum;
	}

	public double getTotDistance() {
		return totDistance;
	}

	public void setTotDistance(double totDistance) {
		this.totDistance = totDistance;
	}

	public String getModelMaster() {
		return modelMaster;
	}

	public void setModelMaster(String modelMaster) {
		this.modelMaster = modelMaster;
	}

	public String getModelHeader() {
		return modelHeader;
	}

	public void setModelHeader(String modelHeader) {
		this.modelHeader = modelHeader;
	}

	

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getSpecifications() {
		return specifications;
	}

	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getVehicleSubType() {
		return vehicleSubType;
	}

	public void setVehicleSubType(String vehicleSubType) {
		this.vehicleSubType = vehicleSubType;
	}

	public String getCorpGroupKey() {
		return corpGroupKey;
	}

	public void setCorpGroupKey(String corpGroupKey) {
		this.corpGroupKey = corpGroupKey;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getVehicleBizType() {
		return vehicleBizType;
	}

	public void setVehicleBizType(String vehicleBizType) {
		this.vehicleBizType = vehicleBizType;
	}

	public Long getVehicleRegDate() {
		return vehicleRegDate;
	}

	public void setVehicleRegDate(Long vehicleRegDate) {
		this.vehicleRegDate = vehicleRegDate;
	}

	public String getApprovalType() {
		return approvalType;
	}

	public void setApprovalType(String approvalType) {
		this.approvalType = approvalType;
	}

	public String getVehicleBodyNum() {
		return vehicleBodyNum;
	}

	public void setVehicleBodyNum(String vehicleBodyNum) {
		this.vehicleBodyNum = vehicleBodyNum;
	}

	public Long getVehicleMaintenanceDate() {
		return vehicleMaintenanceDate;
	}

	public void setVehicleMaintenanceDate(Long vehicleMaintenanceDate) {
		this.vehicleMaintenanceDate = vehicleMaintenanceDate;
	}

	public int getVehiclePayment() {
		return vehiclePayment;
	}

	public void setVehiclePayment(int vehiclePayment) {
		this.vehiclePayment = vehiclePayment;
	}

	public String getVehicleInsureNm() {
		return vehicleInsureNm;
	}

	public void setVehicleInsureNm(String vehicleInsureNm) {
		this.vehicleInsureNm = vehicleInsureNm;
	}

	public String getVehicleInsurePhone() {
		return vehicleInsurePhone;
	}

	public void setVehicleInsurePhone(String vehicleInsurePhone) {
		this.vehicleInsurePhone = vehicleInsurePhone;
	}

	public String getVehicleDescript() {
		return vehicleDescript;
	}

	public void setVehicleDescript(String vehicleDescript) {
		this.vehicleDescript = vehicleDescript;
	}

	public String getVehicleKey() {
		return vehicleKey;
	}

	public void setVehicleKey(String vehicleKey) {
		this.vehicleKey = vehicleKey;
	}

	public String getCorpKey() {
		return corpKey;
	}

	public void setCorpKey(String corpKey) {
		this.corpKey = corpKey;
	}

	public String getDefaultAccountKey() {
		return defaultAccountKey;
	}

	public void setDefaultAccountKey(String defaultAccountKey) {
		this.defaultAccountKey = defaultAccountKey;
	}

	public String getRegUser() {
		return regUser;
	}

	public void setRegUser(String regUser) {
		this.regUser = regUser;
	}

	public String getManufacture() {
		return manufacture;
	}

	public void setManufacture(String manufacture) {
		this.manufacture = manufacture;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getTransmission() {
		return transmission;
	}

	public void setTransmission(String transmission) {
		this.transmission = transmission;
	}

	public String getFuelType() {
		return fuelType;
	}

	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}
		
	
}
