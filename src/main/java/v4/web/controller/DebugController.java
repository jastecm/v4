package v4.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DebugController extends BasicWebViewController {
	
	@RequestMapping(value="/loraEcho")
	public String socket(HttpServletRequest request,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/v4/sock/socket";
		
		try {
			model.addAttribute("target", request.getParameter("target"));
			
			
		} catch (Exception e) {
			return "/error";
		}
		return rtvPage;
	}

}
