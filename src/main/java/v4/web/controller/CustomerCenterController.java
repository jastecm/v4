package v4.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CustomerCenterController extends BasicWebViewController {
	
	private Log logger = LogFactory.getLog(getClass());
	
	/**
	 * 공지사항
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/customerCenter/notice")
	public String notice(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/customerCenter/notice"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * faq
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/customerCenter/faq")
	public String faq(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/customerCenter/faq"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 이벤트, 할인정보
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/customerCenter/event")
	public String event(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/customerCenter/event"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 서비스 불편신고 - 리스트
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/customerCenter/reportInconvenience")
	public String reportInconvenienceList(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/customerCenter/reportInconvenience"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 서비스 불편신고 - 등록
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/customerCenter/reportInconvenience/reg")
	public String reportInconvenienceReg(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/customerCenter/reportInconvenience/reg"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 제품 AS 접수 - 리스트
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/customerCenter/productAs")
	public String productAsList(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/customerCenter/productAs"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 제품 AS 접수 - 등록
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/customerCenter/productAs/reg")
	public String productAsReg(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/customerCenter/productAs/reg"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 단말기 업데이트
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/customerCenter/deviceUpdate")
	public String deviceUpdate(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/customerCenter/deviceUpdate"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}

}
