package v4.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import org.jastecm.string.StrUtil;

@Controller
public class MapController extends BasicWebViewController {
	
	private Log logger = LogFactory.getLog(getClass());	
	
	@RequestMapping(value="/map/openMap")
	public String mapTest(HttpServletRequest req,ModelMap model) throws Exception  {
		
		String rtvPage = "/map/map_sk"; //none tiles
		
		try {
			
			String searchType = (String)req.getParameter("searchType");//trip,time,date,vehicle
			String vehicleKey = (String)req.getParameter("vehicleKey");
			String vehicleKeys = (String)req.getParameter("vehicleKeys");
			String searchDriverKey = (String)req.getParameter("searchDriverKey");
			String searchDrivingState = (String)req.getParameter("searchDrivingState");
			String searchAllocateState = (String)req.getParameter("searchAllocateState");
			String lastestTime = (String)req.getParameter("lastestTime");
			String alwaysPopup = (String)req.getParameter("alwaysPopup");
			
			String searchAreaLev1 = (String)req.getParameter("searchAreaLev1");
			String searchAreaLev2 = (String)req.getParameter("searchAreaLev1");
			
			
			String searchTime = (String)req.getParameter("searchTime");
			String searchDate = (String)req.getParameter("searchDate");
			if(!StrUtil.isNullToEmpty(searchDate)) searchDate = searchDate.replaceAll("\\/", "").replaceAll("\\-", "");
			String tripKey = (String)req.getParameter("tripKey");
			
			model.addAttribute("searchType",searchType);
			model.addAttribute("vehicleKey",vehicleKey);
			model.addAttribute("vehicleKeys",vehicleKeys);
			
			model.addAttribute("searchAreaLev1",searchAreaLev1);
			model.addAttribute("searchAreaLev2",searchAreaLev2);
			model.addAttribute("lastestTime",lastestTime);
			model.addAttribute("alwaysPopup",alwaysPopup);
			
			
			model.addAttribute("searchDriverKey",searchDriverKey);
			model.addAttribute("searchDrivingState",searchDrivingState);
			model.addAttribute("searchAllocateState",searchAllocateState);
			
			model.addAttribute("searchTime",searchTime);
			model.addAttribute("searchDate",searchDate);
			model.addAttribute("tripKey",tripKey);
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}

}
