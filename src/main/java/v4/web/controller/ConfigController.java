package v4.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ConfigController extends BasicWebViewController {
	
	private Log logger = LogFactory.getLog(getClass());	
	/**
	 * 결재 관리
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/config/approvalMng")
	public String approvalMng(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/config/approvalMng"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 단위선택
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/config/unitSetting")
	public String unitSetting(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/config/unitSetting"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	/**
	 * 거래등록 - 리스트
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/config/customerRegister/list")
	public String customerRegisterList(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/config/customerRegister/list"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 거래등록 - 등록
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/config/customerRegister/reg")
	public String customerRegisterReg(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/config/customerRegister/reg"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
}
