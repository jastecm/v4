package v4.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.util.JwtUtil;

import v4.web.rest.service.CommonService;
import v4.web.vo.CreateCorpVO;
import v4.web.vo.CreateInviteVO;
import v4.web.vo.CreatePartnerVO;
import v4.web.vo.CreatePersonVO;
import v4.web.vo.InviteStep1VO;
import v4.web.vo.InviteStep2VO;
import v4.web.vo.InviteStep3VO;
import v4.web.vo.RegistStep1VO;
import v4.web.vo.RegistStep2VO;
import v4.web.vo.RegistStep3VO;
import v4.web.vo.RegistStep4VO;




/*
 * @Class name : LoginController
 * @Description : 
 * @Modification Information
 * 
 * @author 
 * @since 
 * @version 
 * @see
 *
 */
@Controller
public class RegistController {
		
	private Log logger = LogFactory.getLog(getClass());	
	
	@Autowired
	protected JwtUtil jwt;
	
	@Autowired
	private CommonService commonService;
	/*
	@Autowired
    private AccountService accountService;
		
	@Autowired
	private RestBoardService restBoardService;
    */
	
	//가입 이메일 발송 페이지
	@RequestMapping(value="/regist/step0")
	public String userJoinSendMail(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/v4/regist/step0"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 초대 회원 가입 step1 동의화면
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/inviteRegist/step1")
	public String inviteStep1(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/v4/inviteRegist/step1";
		
		try {
			
			String key = req.getParameter("key");
			
			if(StrUtil.isNullToEmpty(key))
				return "/error";
			
			InviteStep1VO vo = jwt.parseAccountInviteToken(key);
			
			model.addAttribute("accountId", vo.getAccountId());
			model.addAttribute("corpKey", vo.getCorpKey());
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 초대 회원 가입 step2 지역선택
	 * @param req
	 * @param res
	 * @param model
	 * @param vo
	 * @param br
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/inviteRegist/step2")
	public String inviteStep2(HttpServletRequest req,HttpServletResponse res,
    		ModelMap model , @ModelAttribute @Valid InviteStep2VO vo , BindingResult br) throws Exception  {
		
		String rtvPage = "/v4/inviteRegist/step2"; //동의화면
		
		try {
			
			if(br.hasErrors())
				return "/error";
			
			
			model.addAttribute("rtv", vo);
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 초대 회원 가입 step3 회원정보 입력
	 * @param req
	 * @param res
	 * @param model
	 * @param vo
	 * @param br
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/inviteRegist/step3")
	public String inviteStep3(HttpServletRequest req,HttpServletResponse res,
    		ModelMap model , @ModelAttribute @Valid InviteStep3VO vo , BindingResult br) throws Exception  {
		
		String rtvPage = "/v4/inviteRegist/step3";
		
		try {
			
			if(br.hasErrors())
				return "/error";
			
			model.addAttribute("rtv", vo);
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 초대 회원 가입 회원정보 입력 확인화면
	 * @param req
	 * @param res
	 * @param model
	 * @param vo
	 * @param br
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/inviteRegist/step4")
	public String inviteStep4(HttpServletRequest req,HttpServletResponse res,
    		ModelMap model , @ModelAttribute @Valid CreateInviteVO vo , BindingResult br) throws Exception  {
		
		String rtvPage = "/v4/inviteRegist/step4";
		
		try {
			
			if(br.hasErrors())
				return "/error";
			
			model.addAttribute("rtv", vo);
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}	
	
	/**
	 * 회원 분류 선택화면 (개인,법인,파트너) (공통)
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/regist/step1")
	public String step1(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/v4/regist/step1"; //타입선택화면
		
		try {
			
			String key = req.getParameter("key");
			
			if(StrUtil.isNullToEmpty(key))
				return "/error";
			
			RegistStep1VO vo = jwt.parseEmailAuthToken(key);
			
			model.addAttribute("accountId", vo.getAccountId());
			model.addAttribute("accountPw", vo.getAccountPw());
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	/**
	 * 동의화면 (공통)
	 * @param req
	 * @param res
	 * @param model
	 * @param vo
	 * @param br
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/regist/step2")
	public String step2(HttpServletRequest req,HttpServletResponse res,
    		ModelMap model , @ModelAttribute @Valid RegistStep2VO vo , BindingResult br) throws Exception  {
		
		String rtvPage = "/v4/regist/step2"; //동의화면
		
		try {
			
			if(br.hasErrors())
				return "/error";
			
			
			model.addAttribute("rtv", vo);
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 국가 타임존 선택화면 (공통)
	 * @param req
	 * @param res
	 * @param model
	 * @param vo
	 * @param br
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/regist/step3")
	public String step3(HttpServletRequest req,HttpServletResponse res,
    		ModelMap model , @ModelAttribute @Valid RegistStep3VO vo , BindingResult br) throws Exception  {
		
		String rtvPage = "/v4/regist/step3";
		
		try {
			
			if(br.hasErrors())
				return "/error";
			
			model.addAttribute("rtv", vo);
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 개인회원 개인정보 입력화면
	 * @param req
	 * @param res
	 * @param model
	 * @param vo
	 * @param br
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/regist/person/step1")
	public String personStep1(HttpServletRequest req,HttpServletResponse res,
    		ModelMap model , @ModelAttribute @Valid RegistStep4VO vo , BindingResult br) throws Exception  {
		
		String rtvPage = "/v4/regist/person/step1"; 
		
		try {
			
			if(br.hasErrors())
				return "/error";
			
			model.addAttribute("rtv", vo);
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 개인회원 가입정보 확인화면 (개인) -> next restapi
	 * @param req
	 * @param res
	 * @param model
	 * @param vo
	 * @param br
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/regist/person/step2")
	public String personStep2(HttpServletRequest req,HttpServletResponse res,
    		ModelMap model , @ModelAttribute @Valid CreatePersonVO vo , BindingResult br) throws Exception  {
		
		String rtvPage = "/v4/regist/person/step2"; 
		
		try {
			
			if(br.hasErrors())
				return "/error";
			
			model.addAttribute("rtv", vo);
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 법인회원 추가정보 입력화면
	 * @param req
	 * @param res
	 * @param model
	 * @param vo
	 * @param br
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/regist/corp/step1")
	public String corpStep1(HttpServletRequest req,HttpServletResponse res,
    		ModelMap model , @ModelAttribute @Valid RegistStep4VO vo , BindingResult br) throws Exception  {
		
		String rtvPage = "/v4/regist/corp/step1";  //->rest api 가입
		
		try {
			
			if(br.hasErrors())
				return "/error";
			
			model.addAttribute("rtv", vo);
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 법인회원 입력확인 화면 (법인)
	 * @param req
	 * @param res
	 * @param model
	 * @param vo
	 * @param br
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/regist/corp/step2")
	public String corpStep2(HttpServletRequest req,HttpServletResponse res,
    		ModelMap model , @ModelAttribute @Valid CreateCorpVO vo , BindingResult br) throws Exception  {
		
		String rtvPage = "/v4/regist/corp/step2"; 
		
		try {
			
			if(br.hasErrors()){
				System.out.println(br);
				return "/error";
			}
			model.addAttribute("rtv", vo);
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 법인회원 서비스 등급 선택화면 (법인) -> next restapi
	 * @param req
	 * @param res
	 * @param model
	 * @param vo
	 * @param br
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/regist/corp/step3")
	public String corpStep3(HttpServletRequest req,HttpServletResponse res,
    		ModelMap model , @ModelAttribute @Valid CreateCorpVO vo , BindingResult br) throws Exception  {
		
		String rtvPage = "/v4/regist/corp/step3"; 
		
		try {
			
			if(br.hasErrors())
				return "/error";
			
			model.addAttribute("rtv", vo);
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 파트너 회원 추가 정보 입력화면 (파트너)
	 * @param req
	 * @param res
	 * @param model
	 * @param vo
	 * @param br
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/regist/partner/step1")
	public String partnerStep1(HttpServletRequest req,HttpServletResponse res,
    		ModelMap model , @ModelAttribute @Valid RegistStep4VO vo , BindingResult br) throws Exception  {
		
		String rtvPage = "/v4/regist/partner/step1";
		
		try {
			
		if(br.hasErrors())
				return "/error";
			
			model.addAttribute("rtv", vo);
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 파트너 회원 파트너 타입 선택화면 (파트너)
	 * @param req
	 * @param res
	 * @param model
	 * @param vo
	 * @param br
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/regist/partner/step2")
	public String partnerStep2(HttpServletRequest req,HttpServletResponse res,
    		ModelMap model , @ModelAttribute @Valid CreatePartnerVO vo , BindingResult br) throws Exception  {
		
		String rtvPage = "/v4/regist/partner/step2";
		
		try {
			
			if(br.hasErrors())
				return "/error";
			
			model.addAttribute("rtv", vo);
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 파트너회원 입력확인 화면 (파트너) -> next restapi
	 * @param req
	 * @param res
	 * @param model
	 * @param vo
	 * @param br
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/regist/partner/step3")
	public String partnerStep3(HttpServletRequest req,HttpServletResponse res,
    		ModelMap model , @ModelAttribute @Valid CreatePartnerVO vo , BindingResult br) throws Exception  {
		
		String rtvPage = "/v4/regist/partner/step3";
		
		try {
			
			/*if(br.hasErrors())
				return "/error";
			
			model.addAttribute("rtv", vo);*/
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/regist/step7")
	public String step7(HttpServletRequest req,HttpServletResponse res,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/v4/regist/step7"; //가입 완료
		
		try {
			
			String corpType = req.getParameter("corpType");
			String name = req.getParameter("name");
			String accountId = req.getParameter("accountId");
			
			model.addAttribute("corpType", corpType);
			model.addAttribute("name", name);
			model.addAttribute("accountId", accountId);
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
}


