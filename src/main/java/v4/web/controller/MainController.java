package v4.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import v4.web.rest.service.CommonService;





/*
 * @Class name : LoginController
 * @Description : 
 * @Modification Information
 * 
 * @author 
 * @since 
 * @version 
 * @see
 *
 */
@Controller
public class MainController extends BasicWebViewController {
		
	private Log logger = LogFactory.getLog(getClass());	
	
	@Autowired
	private CommonService commonService;
	/*
	@Autowired
    private AccountService accountService;
		
	@Autowired
	private RestBoardService restBoardService;
    */
	
	@RequestMapping(value="/index")
	public String index(HttpServletRequest req) throws Exception  {
		
		String rtvPage = "/main/page";
		
		try {
			
			String cmd = req.getParameter("cmd");
			if(!StrUtil.isNullToEmpty(cmd)) {
				rtvPage = "forward:"+cmd;
			}
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/main")
	public String mainPage(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/main/page";
		
		try {
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/404")
	public String _404(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/common/404.jsp";
		System.out.println("404");
		try {
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/test")
	public String testPage(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/v4/test";
		
		try {
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/test2")
	public String testPage2(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/v4/test2";
		
		try {
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/tree")
	public String tree(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/v4/tree";
		
		try {
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/group")
	public String testGroupPage(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/v4/group";
		
		try {
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/allocate")
	public String testAllocatePage(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/v4/allocate";
		
		try {
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/maintenance")
	public String testmaintenance(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/v4/maintenance";
		
		try {
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/*@RequestMapping(value="/map/test")
	public String mapTest(HttpServletRequest req,ModelMap model) throws Exception  {
		
		String rtvPage = "/map/map_sk"; //none tiles
		
		try {
			
			String searchType = (String)req.getParameter("searchType");//trip,time,date,vehicle
			String vehicleKey = (String)req.getParameter("vehicleKey");
			String vehicleKeys = (String)req.getParameter("vehicleKeys");
			String searchDriverKey = (String)req.getParameter("searchDriverKey");
			String searchDrivingState = (String)req.getParameter("searchDrivingState");
			String searchAllocateState = (String)req.getParameter("searchAllocateState");
			String lastestTime = (String)req.getParameter("lastestTime");
			String alwaysPopup = (String)req.getParameter("alwaysPopup");
			
			String searchAreaLev1 = (String)req.getParameter("searchAreaLev1");
			String searchAreaLev2 = (String)req.getParameter("searchAreaLev1");
			
			
			String searchTime = (String)req.getParameter("searchTime");
			String searchDate = (String)req.getParameter("searchDate");
			if(!StrUtil.isNullToEmpty(searchDate)) searchDate = searchDate.replaceAll("\\/", "").replaceAll("\\-", "");
			String tripKey = (String)req.getParameter("tripKey");
			
			model.addAttribute("searchType",searchType);
			model.addAttribute("vehicleKey",vehicleKey);
			model.addAttribute("vehicleKeys",vehicleKeys);
			
			model.addAttribute("searchAreaLev1",searchAreaLev1);
			model.addAttribute("searchAreaLev2",searchAreaLev2);
			model.addAttribute("lastestTime",lastestTime);
			model.addAttribute("alwaysPopup",alwaysPopup);
			
			
			model.addAttribute("searchDriverKey",searchDriverKey);
			model.addAttribute("searchDrivingState",searchDrivingState);
			model.addAttribute("searchAllocateState",searchAllocateState);
			
			model.addAttribute("searchTime",searchTime);
			model.addAttribute("searchDate",searchDate);
			model.addAttribute("tripKey",tripKey);
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}*/
		
}
