package v4.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ViewCarApiController extends BasicWebViewController {
	
	private Log logger = LogFactory.getLog(getClass());
	
	/**
	 * 개발자 지원
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/viewCarApi/developerSupport")
	public String developerSupport(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/viewCarApi/developerSupport"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * api 문서
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/viewCarApi/apiDocument")
	public String apiDocument(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/viewCarApi/apiDocument"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/viewCarApi/faq")
	public String faq(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/viewCarApi/faq"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}

}
