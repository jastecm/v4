package v4.web.controller;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.util.JwtUtil;
import com.util.TimeUtil;
import org.jastecm.json.JSONException;
import org.jastecm.json.JSONObject;
import com.xebia.jacksonlombok.JacksonLombokAnnotationIntrospector;

import v4.ex.exc.BizException;
import v4.ex.exc.CreateResponseException;
import v4.web.rest.service.TokenService;
import v4.web.vo.TokenVO;





/*
 * @Class name : LoginController
 * @Description : 
 * @Modification Information
 * 
 * @author 
 * @since 
 * @version 
 * @see
 *
 */
public class BasicWebViewController {
	
	public ObjectMapper mapper = new ObjectMapper();
	
	@Autowired
	protected JwtUtil jwt;
	
	
	//@Value("129600") //36h
	//@Value("43200") //12h
	@Value("30000000") //1y
	protected long expired;
	
	//@Value("43200") //remain 12h : refrash
	//@Value("129540") //remain 35h 59m : refrash
	@Value("1800") //remain 30m : refrash
	protected long expireRefresh;
	
	@Value("300") //5분
	protected long expireAccept;
	
	@Autowired
	private MessageSource messageSource;
	
	public ResponseEntity createValidErrorResponse(HttpServletResponse response , String[] args , BindingResult br , Object... value) throws CreateResponseException {
		
		if(value == null) value = new Object[]{};
        
        if(args.length-1 == value.length){
        	
        	Map<String,String> errList= new HashMap<String, String>();
        	
            for (Object object : br.getAllErrors()) {
                if(object instanceof FieldError) {
                    FieldError fieldError = (FieldError) object;
                    errList.put(fieldError.getField(),messageSource.getMessage(fieldError,  response.getLocale()));
                }
            }
            
            MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		    params.set("Content-Type", MediaType.APPLICATION_JSON + ";charset=utf-8");
			
		    Map<String,Object> body = new HashMap<String,Object>();
		    
		    body.put(args[0], errList);
		    
		    for(int i = 1; i < args.length ; i++){
		    	body.put(args[i], value[i-1]);
		    }
		    
		    String strBody;
			try {
				strBody = mapper.writeValueAsString(body);
			} catch (JsonGenerationException e) {
				e.printStackTrace();
				throw new CreateResponseException(e.getMessage());
			} catch (JsonMappingException e) {
				e.printStackTrace();
				throw new CreateResponseException(e.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
				throw new CreateResponseException(e.getMessage());
			}
		    
		    return new ResponseEntity(strBody, params, HttpStatus.BAD_REQUEST);
        }else{
			throw new CreateResponseException("unmatched key,value");
		}
		
	}
	public ResponseEntity createResponse(HttpStatus state) throws CreateResponseException {
			MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		    params.set("Content-Type", MediaType.APPLICATION_JSON + ";charset=utf-8");
			
		    return new ResponseEntity(null, params, state);
	}
	
	public ResponseEntity createResponse(HttpStatus state,String[] args,Object... value) throws CreateResponseException {
		
		if(args.length == value.length){
			MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		    params.set("Content-Type", MediaType.APPLICATION_JSON + ";charset=utf-8");
			
		    Map<String,Object> body = new HashMap<String,Object>();
		    
		    for(int i = 0; i < args.length ; i++){
		    	body.put(args[i], value[i]);
		    }
		    
		    
		    String strBody;
		    try {
				strBody = mapper.writeValueAsString(body);
			} catch (JsonGenerationException e) {
				e.printStackTrace();
				throw new CreateResponseException(e.getMessage());
			} catch (JsonMappingException e) {
				e.printStackTrace();
				throw new CreateResponseException(e.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
				throw new CreateResponseException(e.getMessage());
			}
			
		    return new ResponseEntity(strBody, params, state);
		}else{
			throw new CreateResponseException("unmatched key,value");
		}
	}

	
	public ResponseEntity createResponse(HttpStatus state,Map<String,Object> args) throws CreateResponseException {
		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
	    params.set("Content-Type", MediaType.APPLICATION_JSON + ";charset=utf-8");
		
	    String strBody;
	    try {
			strBody = mapper.writeValueAsString(args);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			throw new CreateResponseException(e.getMessage());
		} catch (JsonMappingException e) {
			e.printStackTrace();
			throw new CreateResponseException(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			throw new CreateResponseException(e.getMessage());
		}
		
	    return new ResponseEntity(strBody, params, state);
}
	
	public ResponseEntity createResponse(HttpStatus state,boolean nombok,Map<String,Object> args) throws CreateResponseException {
			MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		    params.set("Content-Type", MediaType.APPLICATION_JSON + ";charset=utf-8");
			
		    String strBody;
		    try {
		    	if(nombok){
		    		ObjectMapper mapperNombok = new ObjectMapper().setAnnotationIntrospector(new JacksonLombokAnnotationIntrospector());
					strBody = mapperNombok.writeValueAsString(args);
		    	}else{
		    		return createResponse(state,args);
		    	}
			} catch (JsonGenerationException e) {
				e.printStackTrace();
				throw new CreateResponseException(e.getMessage());
			} catch (JsonMappingException e) {
				e.printStackTrace();
				throw new CreateResponseException(e.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
				throw new CreateResponseException(e.getMessage());
			}
			
		    return new ResponseEntity(strBody, params, state);
	}
	
	public ResponseEntity createResponse(HttpStatus state,JSONObject jObj) throws CreateResponseException {
			MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		    params.set("Content-Type", MediaType.APPLICATION_JSON + ";charset=utf-8");
			
		    String strBody;
			try {
				strBody = jObj.toString(4);
				return new ResponseEntity(strBody, params, state);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return new ResponseEntity("", params, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
		    
	}
	
	public TokenVO authentication(TokenService service , String key) throws BizException{
		
		
		if(key == null) return null;
		
		else{
			
			TokenVO t = jwt.parseToken(key);
			
			if(t == null) {
				return new TokenVO("Unauthorized");
			}
			if(t.getExpired() < TimeUtil.now()) {
				return new TokenVO("token.tokenExpired");
			}
			
			if(!service.getUserToken(t.getUserKey()).contains(t.getExpired()))
				return new TokenVO("token.tokenExpired");
			
			return t;
			
		}
	}
	
	//삭제예정
	public Map<String,String> authenticationGetDeviceAuth(String key) {
		
		
		if(key == null) return null;
		
		else{
			
			Map<String,String> t = jwt.parseTokenGetDeviceAuth(key);
			
			if(t == null) {
				return null;
			}else return t;
						
		}
	}
	
	//삭제예정
	public String authenticationGetDeviceId(String key) {
		
		
		if(key == null) return null;
		
		else{
			
			String t = jwt.parseTokenGetDeviceId(key);
			
			if(t == null) {
				return null;
			}else return t;
						
		}
	}
	
	//삭제예정
	public String authenticationGetDeviceSn(String key) {
		
		
		if(key == null) return null;
		
		else{
			
			String t = jwt.parseTokenGetDeviceSn(key);
			
			if(t == null) {
				return null;
			}else return t;
						
		}
	}
	
	//변경예정
	public TokenVO authenticationChangePw(String key) {
		
		
		if(key == null) return null;
		
		else{
			
			TokenVO t = jwt.parseChangePwToken(key);
			
			if(t == null) {
				return null;
			}
			if(t.getExpired() < TimeUtil.now()) {
				return null;
			}
			
			return t;
			
		}
	}

	
	public byte[] randomByte(int len){
		Random r = new Random();
		
		byte[] ba = new byte[len/2];
		
		for(int i = 0 ; i < ba.length ; i++){
			int v = r.nextInt(254)+1; // 1~255
			ba[i] = intToByteArray(v,1,BIG_EDIAN)[0] ;
		}
		return ba;
	}
	
	public String randomHex(int len){
		return byteArrayToHex(randomByte(len));
	}
	
	public final byte[] intToByteArray(int integer , int offset , int order){
		byte[] bytes = new byte[offset];
		for (int i = 0; i < offset; i++) {
			bytes[offset-1-i] = (byte)(integer >>> (i * 8));
		}
		return changeByteOrder(bytes, order);
	}
	
	private final byte[] changeByteOrder(byte[] value,int Order){
    	int idx = value.length;
    	byte[]Temp = new byte[idx];
     
    	if(Order == BIG_EDIAN){
    		Temp = value;
    	}else if(Order ==LITTLE_EDIAN){
    		for(int i = 0 ; i < idx ; i ++){
    			Temp[i] = value[idx - (i+1)];
    		}
    	}
    	return Temp;
    }
	
	protected final int BIG_EDIAN = 1;
    protected final int LITTLE_EDIAN = 2;
    
    public final String byteArrayToHex(byte[] ba) {
        if (ba == null || ba.length == 0) {
            return null;
        }

        StringBuffer sb = new StringBuffer(ba.length * 2);
        String hexNumber;
        for (int x = 0; x < ba.length; x++) {
            hexNumber = " 0" + Integer.toHexString(0xff & ba[x]);

            sb.append(hexNumber.substring(hexNumber.length() - 2));
        }
        return sb.toString();
    } 
    
    public String SHA512(String passwordToHash, String   salt) throws Exception{
    	String generatedPassword = null;
    	    try {
    	         MessageDigest md = MessageDigest.getInstance("SHA-512");
    	         md.update(passwordToHash.getBytes("UTF-8"));
    	         byte[] bytes = md.digest(salt.getBytes("UTF-8"));
    	         StringBuilder sb = new StringBuilder();
    	         for(int i=0; i< bytes.length ;i++){
    	            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
    	         }
    	         generatedPassword = sb.toString();
    	        } 
    	       catch (NoSuchAlgorithmException e){
    	        e.printStackTrace();
    	       }
    	    return generatedPassword;
    	}
}
