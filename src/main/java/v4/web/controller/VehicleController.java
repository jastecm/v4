package v4.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import v4.web.rest.service.CommonService;
import v4.web.vo.data.AccountVO;




/*
 * @Class name : LoginController
 * @Description : 
 * @Modification Information
 * 
 * @author 
 * @since 
 * @version 
 * @see
 *
 */
@Controller
public class VehicleController extends BasicWebViewController {
		
	private Log logger = LogFactory.getLog(getClass());	
	
	@Autowired
	private CommonService commonService;
	
	@RequestMapping(value="/config/vehicle")
	public String configVehicle(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/config/vehicle";
		
		try {
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/config/vehicleRegister/list")
	public String vehicleRegisterList(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		//개인이냐 법인이냐
		AccountVO account = (AccountVO) req.getSession().getAttribute("V4");
		String rtvPage = "";
		
		try {
			
			//1개인 2법인 3렌트 4파트너
			if(account.getCorp().getCorpKey().equals("1")){
				rtvPage = "/config/vehicleRegister/list/person";
			}else{
				rtvPage = "/config/vehicleRegister/list/corp";
			}
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 차량 등록 페이지로 이동
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/config/vehicleRegister/reg/person")
	public String vehicleRegisterPerson(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		

		String rtvPage = "/config/vehicleRegister/reg/person";
		
		try {
			

		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	
	/**
	 * 차량등록 페이지 이동 (법인) - 승용
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/config/vehicleRegister/reg/corp/vehicle")
	public String vehicleRegisterRegCorpVehicle(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/config/vehicleRegister/reg/corp/vehicle";
		
		try {
			

		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 차량등록 페이지 이동 (법인) - 버스
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/config/vehicleRegister/reg/corp/bus")
	public String vehicleRegisterRegCorpBus(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/config/vehicleRegister/reg/corp/bus";
		
		try {
			

		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 차량등록 페이지 이동 (법인) - 트럭
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/config/vehicleRegister/reg/corp/truck")
	public String vehicleRegisterRegCorpTruck(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/config/vehicleRegister/reg/corp/truck";
		
		try {
			

		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	

	/**
	 * 차량등록 페이지 이동 (법인) - 특수차량
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/config/vehicleRegister/reg/corp/etcVehicle")
	public String vehicleRegisterRegCorpEtcVehicle(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/config/vehicleRegister/reg/corp/etcVehicle";
		
		try {
			

		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	
	@RequestMapping(value="/config/vehicleRegister/common/pcManager")
	public String vehicleRegisterPcManager(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/config/vehicleRegister/common/pcManager";
		
		try {
			

		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
}
