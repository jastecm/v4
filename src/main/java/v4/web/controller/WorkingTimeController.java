package v4.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import v4.web.rest.service.WorkingAreaTimeService;
import v4.web.vo.AccountVO;
import v4.web.vo.WorkingTimeAreaVO;


@Controller
public class WorkingTimeController extends BasicWebViewController {
	private Log logger = LogFactory.getLog(getClass());	
	
	@Autowired
    private WorkingAreaTimeService workingAreaTimeService;
	
	/**
	 * 업무시간 설정 리스트
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/config/workingArea")
	public String workingAreaList(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/config/workingArea"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	
	@RequestMapping(value="/config/workingArea/reg")
	public String workingAreaListRegister(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/config/workingArea/reg"; 
		
		AccountVO accountVo = (AccountVO) req.getSession().getAttribute("V4");
		try {
			
			String workTimeKey = req.getParameter("workTimeKey");
			
			if(!StrUtil.isNullToEmpty(workTimeKey)){
				Map<String,Object> param = new HashMap<String,Object>();
				param.put("corpKey", accountVo.getCorpKey());
	        	param.put("workTimeKey", workTimeKey);
				
	        	List<WorkingTimeAreaVO> rtvList = workingAreaTimeService.getWorkingTime(param);
	        	WorkingTimeAreaVO vo = null;
				if(rtvList.size() == 1) 
					 vo = rtvList.get(0);
				else throw new Exception();
				
				model.addAttribute("rtv",vo);
			}
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
}
