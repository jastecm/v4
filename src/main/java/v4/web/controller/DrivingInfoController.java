package v4.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jastecm.string.StrUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DrivingInfoController extends BasicWebViewController {
	private Log logger = LogFactory.getLog(getClass());	
	
	/**
	 * 차량-사용자
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/drivingInfo/vehicleUsers")
	public String vehicleUsers(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/drivingInfo/vehicleUsers"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 차량 상세
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/drivingInfo/vehicleUsersDetail/vehicle")
	public String vehicleDetail(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/drivingInfo/vehicleUsersDetail/vehicle"; 
		
		try {
			
			String vehicleKey = req.getParameter("vehicleKey");
			
			if(StrUtil.isNullToEmpty(vehicleKey)){
				return "/error";
			}
			
			model.addAttribute("vehicleKey", vehicleKey);
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/drivingInfo/vehicleUsersDetail/user")
	public String userDetail(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/drivingInfo/vehicleUsersDetail/user"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 차량 배차 - 리스트
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/drivingInfo/vehicleAllocate/list")
	public String vehicleAllocateList(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/drivingInfo/vehicleAllocate/list"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 차량 배차 - 등록
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/drivingInfo/vehicleAllocate/reg")
	public String vehicleAllocateReg(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/drivingInfo/vehicleAllocate/reg"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 미배차 운행 - 리스트
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/drivingInfo/availableDriving/list")
	public String availableDrivingList(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/drivingInfo/availableDriving/list"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 미배차 운행 - 등록
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/drivingInfo/availableDriving/reg")
	public String availableDrivingReg(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/drivingInfo/availableDriving/reg"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 운행일지 - 리스트
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/drivingInfo/drivingLog")
	public String drivingLog(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/drivingInfo/drivingLog"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 버스노선 정보 - 리스트
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/drivingInfo/busRouteInfo/list")
	public String busRouteInfoList(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/drivingInfo/busRouteInfo/list"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	/**
	 * 버스노선 정보 - 상세
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/drivingInfo/busRouteInfo/detail")
	public String busRouteInfoDetail(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/drivingInfo/busRouteInfo/detail"; 
		
		try {
			
			String busRouteKey = req.getParameter("busRouteKey");
			
			if(StrUtil.isNullToEmpty(busRouteKey)){
				return "/error";
			}
			
			model.addAttribute("busRouteKey", busRouteKey);
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 상세운행기록
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/drivingInfo/detailDrivingRecord")
	public String detailDrivingRecord(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/drivingInfo/detailDrivingRecord"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 차량 위치
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/drivingInfo/vehicleLocation")
	public String vehicleLocation(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/drivingInfo/vehicleLocation"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 차량주요알림
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/drivingInfo/vehicleAlarm")
	public String vehicleAlarm(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/drivingInfo/vehicleAlarm"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 차량사고조회
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/drivingInfo/vehicleAccident")
	public String vehicleAccident(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/drivingInfo/vehicleAccident"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
}
