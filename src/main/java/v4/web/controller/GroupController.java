package v4.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
public class GroupController extends BasicWebViewController {
	private Log logger = LogFactory.getLog(getClass());	
	
	@RequestMapping(value="/config/groupRegister")
	public String groupRegister(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/config/groupRegister"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
}
