package v4.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jastecm.string.StrUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class VehicleMngController extends BasicWebViewController {
	
	private Log logger = LogFactory.getLog(getClass());
	
	/**
	 * 차량관리 - 운행스케쥴
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/vehicleMng/drivingSchedule")
	public String drivingSchedule(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/vehicleMng/drivingSchedule"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 차량관리 - 운행스케쥴 상세
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/vehicleMng/drivingSchedule/detail")
	public String drivingScheduleDetail(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/vehicleMng/drivingSchedule/detail"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 실시간 자가 진단
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/vehicleMng/realTimeSelfDiagnosis")
	public String realTimeSelfDiagnosis(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/vehicleMng/realTimeSelfDiagnosis"; 
		
		try {
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 고장 소모품 현황
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/vehicleMng/troubleAndConsumables")
	public String troubleAndConsumables(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/vehicleMng/troubleAndConsumables"; 
		
		try {
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 고장 소모품 현황 - 상세
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/vehicleMng/troubleAndConsumables/detail")
	public String troubleAndConsumablesDetail(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/vehicleMng/troubleAndConsumables/detail"; 
		
		try {
			
			String vehicleKey = req.getParameter("vehicleKey");
			
			if(StrUtil.isNullToEmpty(vehicleKey)){
				return "/error";
			}
			
			model.addAttribute("vehicleKey", vehicleKey);
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 벌점 과태료 - 리스트
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/vehicleMng/penalty")
	public String penaltyList(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/vehicleMng/penalty"; 
		
		try {
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 벌점 과태료 - 등록
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/vehicleMng/penalty/reg")
	public String penaltyReg(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/vehicleMng/penalty/reg"; 
		
		try {
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 정비현황 조회 등록 - 리스트
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/vehicleMng/vehicleMaintenance")
	public String vehicleMaintenance(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/vehicleMng/vehicleMaintenance"; 
		
		try {
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	/**
	 * 정비현황 조회 등록 - 요청
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/vehicleMng/vehicleMaintenance/reg")
	public String vehicleMaintenanceReg(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/vehicleMng/vehicleMaintenance/reg"; 
		
		try {
			String maintenanceKey = req.getParameter("maintenanceKey");
			String vehicleKey = req.getParameter("vehicleKey");
			
			model.addAttribute("maintenanceKey", maintenanceKey);
			model.addAttribute("vehicleKey", vehicleKey);
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	/**
	 * 정비현황 조회 등록 - 부품 등록
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/vehicleMng/vehicleMaintenance/parts")
	public String vehicleMaintenanceRegEtc(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/vehicleMng/vehicleMaintenance/parts"; 
		
		try {
			String maintenanceKey = req.getParameter("maintenanceKey");
			String vehicleKey = req.getParameter("vehicleKey");
			String mode = req.getParameter("mode");
			
			if(StrUtil.isNullToEmpty(maintenanceKey) || StrUtil.isNullToEmpty(vehicleKey)){
				return "/error";
			}
			
			model.addAttribute("maintenanceKey", maintenanceKey);
			model.addAttribute("vehicleKey", vehicleKey);
			model.addAttribute("mode", mode);
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 차량 경비 리스트
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/vehicleMng/vehicleExpense")
	public String vehicleExpense(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/vehicleMng/vehicleExpense"; 
		
		try {
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 차량경비 등록
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/vehicleMng/vehicleExpense/reg")
	public String vehicleExpenseReg(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/vehicleMng/vehicleExpense/reg"; 
		
		try {
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 도급비
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/vehicleMng/subcontract")
	public String subcontract(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/vehicleMng/subcontract"; 
		
		try {
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 렌트가 관리 - 리스트
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/vehicleMng/rentCarMng")
	public String rentCarMngList(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/vehicleMng/rentCarMng"; 
		
		try {
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 렌트카 관리 - 상세
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/vehicleMng/rentCarMng/detail")
	public String rentCarMngDetail(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/vehicleMng/rentCarMng/detail"; 
		
		try {
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	

}
