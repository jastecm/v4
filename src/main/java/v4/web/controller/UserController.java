package v4.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserController {
	private Log logger = LogFactory.getLog(getClass());	
	
	/**
	 * 사용자 등록 리스트
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/config/userRegister")
	public String userRegister(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/config/userRegister"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 사용자 초대
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/config/userInvite")
	public String userInvite(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/config/userInvite"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 사용자 개별등록
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/config/userOneRegister")
	public String userOneRegister(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/config/userOneRegister"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 사용자 대량등록
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/config/userManyRegister")
	public String userManyRegister(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/config/userManyRegister"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
}
