package v4.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import v4.ex.exc.extents.UserNotFoundException;
import v4.web.rest.service.TokenService;
import v4.web.rest.service.data.AccountDataService;
import v4.web.vo.TokenVO;
import v4.web.vo.data.AccountVO;

@Controller
public class LoginController extends BasicWebViewController {
	private Log logger = LogFactory.getLog(getClass());	
	
	@Autowired
	AccountDataService accountDataService;
	
	@Autowired
	TokenService tokenService;
	
	//로그인 페이지로 이동
	@RequestMapping(value="/login")
	public String groupRegister(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/login"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	
	@RequestMapping(value="/login/registToken" , method=RequestMethod.POST)
	public ResponseEntity registToken(HttpServletRequest req,HttpServletResponse res
			,@RequestHeader(required=true)
			String key
	)  throws Exception  {
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
			return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
		
		AccountVO account = accountDataService.getAccountFromKey(t.getUserKey(),false);
		
		if(account == null)
			throw new UserNotFoundException();
		//여기까지 거의공통//
		
		
		req.getSession(true).setAttribute("V4",account);
		req.getSession(true).setAttribute("_KEY",key);
		req.getSession(true).setAttribute("_LANG",account.getLang());
		
		
		return createResponse(HttpStatus.OK);
		
	}
	
	@RequestMapping(value="/login/removeToken" , method=RequestMethod.POST)
	public ResponseEntity removeToken(HttpServletRequest req,HttpServletResponse res)  throws Exception  {
		
		
		req.getSession(true).invalidate();
		
		
		return createResponse(HttpStatus.OK);
		
	}
}
