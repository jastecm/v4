package v4.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jastecm.string.StrUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ReportAndApprovalController extends BasicWebViewController {
	
	private Log logger = LogFactory.getLog(getClass());	
	
	/**
	 * 차량보고서 조회 제출 - 리스트
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/reportApproval/vehicleReport")
	public String vehicleReport(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/reportApproval/vehicleReport"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 차량보고서 조회 제출 - 등록
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/reportApproval/vehicleReport/reg")
	public String vehicleReportReg(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/reportApproval/vehicleReport/reg"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 차량 사고 보고 - 리스트
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/reportApproval/vehicleAccident")
	public String vehicleAccidentList(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/reportApproval/vehicleAccident"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	/**
	 * 차량 사고 보고 - 등록
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/reportApproval/vehicleAccident/reg")
	public String vehicleAccidentReg(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/reportApproval/vehicleAccident/reg"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 차량 사고 보고 - 상세
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/reportApproval/vehicleAccident/detail")
	public String vehicleAccidentDetail(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/reportApproval/vehicleAccident/detail"; 
		
		try {
			
			String accidentKey = req.getParameter("accidentKey");
			
			if(StrUtil.isNullToEmpty(accidentKey)){
				return "/error";
			}
			
			model.addAttribute("accidentKey", accidentKey);
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 결재 관리 - 사용자
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/reportApproval/userApprovalMng")
	public String userApprovalMng(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/reportApproval/userApprovalMng"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 리포트 
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/reportApproval/report")
	public String report(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/reportApproval/report"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 운행 통계
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/reportApproval/drivingStatistics")
	public String drivingStatistics(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/reportApproval/drivingStatistics"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 국세청 운행 일지
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 * 
	 */
	@RequestMapping(value="/reportApproval/taxServiceVehicleLog")
	public String taxServiceVehicleLog(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/reportApproval/taxServiceVehicleLog"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}

}
