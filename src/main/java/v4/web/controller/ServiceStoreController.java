package v4.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ServiceStoreController extends BasicWebViewController {
	
	private Log logger = LogFactory.getLog(getClass());
	
	
	/**
	 * 서비스 스토어
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/config/serviceStore")
	public String serviceStore(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/config/serviceStore"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 서비스 스토어 - 상세 (월)
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/config/serviceStore/month")
	public String serviceStoreBuyMonth(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/config/serviceStore/month"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 서비스 스토어 - 상세 (갯수)
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/config/serviceStore/number")
	public String serviceStoreBuyNumber(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/config/serviceStore/number"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	
	
	/**
	 * MY서비스 스토어 - 리스트
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/config/myServiceStore")
	public String myServiceStoreList(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/config/myServiceStore"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * MY서비스 스토어 - 사용자 초대 step1
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/config/myServiceStore/invite/step1")
	public String myServiceStoreInviteStep1(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/config/myServiceStore/invite/step1"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * MY서비스 스토어 - 사용자 초대 step2
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/config/myServiceStore/invite/step2")
	public String myServiceStoreInviteStep2(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/config/myServiceStore/invite/step2"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * MY서비스 스토어 - 등록
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/config/myServiceStore/reg")
	public String myServiceStoreReg(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/config/myServiceStore/reg"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
}
