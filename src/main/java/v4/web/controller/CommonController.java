package v4.web.controller;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.common.Globals;
import org.jastecm.string.StrUtil;

import v4.web.rest.service.CommonService;
import v4.web.vo.UserVO;




/*
 * @Class name : LoginController
 * @Description : 
 * @Modification Information
 * 
 * @author 
 * @since 
 * @version 
 * @see
 *
 */
@Controller
public class CommonController extends BasicWebViewController{
		
	private Log logger = LogFactory.getLog(getClass());	
	
	@Autowired
	CommonService commonService;
	
	@RequestMapping(value="/api/doc" , method=RequestMethod.GET)
	public String doc(HttpServletRequest req,HttpServletResponse response) throws Exception  {
		
		String b = "/swagger-ui/index";
		try {
			
			return b;
	        
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			return b;
		}
	}
	
	/*
	@RequestMapping(value="/com/getDeviceSeries.do")
	public String getDeviceSeries(HttpServletRequest req
			,HttpServletResponse res
			,ModelMap model) throws Exception  {
		
		String rtvPage = "jsonView";
		UserVO user = (UserVO)req.getSession().getAttribute("VDAS");
		res.setContentType("application/x-json; charset=UTF-8");
		
		try {
			
			List<String> rtvList = commonService.getDeviceSeries(user);
			
			model.addAttribute("rtvList", rtvList);
			
			return rtvPage;
	        
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			return rtvPage;
		}
	}
	
	@RequestMapping(value="/com/getDeviceList.do")
	public String getDeviceList(HttpServletRequest req
			,HttpServletResponse res
			,ModelMap model) throws Exception  {
		
		String rtvPage = "jsonView";
		res.setContentType("application/x-json; charset=UTF-8");
		
		UserVO user = (UserVO)req.getSession().getAttribute("VDAS");
		
		
		try {
			
			String deviceSeries = (String)req.getParameter("deviceSeries");
			String free = (String)req.getParameter("free");
			
			List<Map<String,String>> rtvList = commonService.getDeviceList(user,deviceSeries,free);
			
			model.addAttribute("rtvList", rtvList);
			
			return rtvPage;
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			return rtvPage;
		}
	}
	
	@RequestMapping(value="/com/getUserList.do")
	public String getUserList(HttpServletRequest req
			,HttpServletResponse res
			,ModelMap model) throws Exception  {
		
		String rtvPage = "jsonView";
		res.setContentType("application/x-json; charset=UTF-8");
		
		UserVO user = (UserVO)req.getSession().getAttribute("VDAS");
		
		
		try {
			String offset = (String)req.getParameter("offset");
			String limit = (String)req.getParameter("limit");
			String searchOrder = (String)req.getParameter("searchOrder");
			String searchType = (String)req.getParameter("searchType");
			String searchText = (String)req.getParameter("searchText");
			String searchState = (String)req.getParameter("searchState");
			String searchUserNm = (String)req.getParameter("searchUserNm");
			String searchRemoveGroup = (String)req.getParameter("searchRemoveGroup");
			String searchGroup = (String)req.getParameter("searchGroup");
			
			String searchCorp = (String)req.getParameter("searchCorp");
			
			if(!StrUtil.isNullToEmpty(searchCorp) && !user.getRule().equals("0")){
				return rtvPage;
			}
				
			
			Map<String,String> param = new HashMap<String,String>();
			param.put("offset", offset);
			param.put("limit", limit);
			param.put("searchOrder", searchOrder);
			param.put("searchType", searchType);
			param.put("searchText", searchText);
			param.put("searchState", searchState);
			param.put("searchUserNm", searchUserNm);
			param.put("searchRemoveGroup", searchRemoveGroup);
			param.put("searchGroup", searchGroup);
			param.put("searchCorp", searchCorp);
			
			
			model.addAttribute("rtvList", commonService.getUserList(user,param));
			
			return rtvPage;
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			return rtvPage;
		}
	}
	
	@RequestMapping(value="/com/getVehicleList.do")					
	public String getVehicleList(HttpServletRequest req,HttpServletResponse res,
    		ModelMap model ,@ModelAttribute("vehicleVo") @Valid VehicleVO reqVo,BindingResult br) throws Exception  {
		
		String rtvPage = "jsonView";
		res.setContentType("application/x-json; charset=UTF-8");
		
		UserVO user = (UserVO)req.getSession().getAttribute("VDAS");
		
		try {
			String offset = (String)req.getParameter("offset");
			String limit = (String)req.getParameter("limit");
			String searchType = (String)req.getParameter("searchType");
			String searchText = (String)req.getParameter("searchText");
			String searchOrder = (String)req.getParameter("searchOrder");
			String searchDevice = (String)req.getParameter("searchDevice");
			String searchUsed = (String)req.getParameter("searchUsed");
			String searchState = (String)req.getParameter("searchState");
			String searchdrivingState = (String)req.getParameter("searchdrivingState");
			String searchRemoveGroup = (String)req.getParameter("searchRemoveGroup");
			String searchGroup = (String)req.getParameter("searchGroup");
			String searchDtc = (String)req.getParameter("searchDtc");
			String searchWarningItem = (String)req.getParameter("searchWarningItem");
			String searchExpireState = (String)req.getParameter("searchExpireState");
			String searchDeviceSn = (String)req.getParameter("searchDeviceSn");
			
			
			
			Map<String,String> param = new HashMap<String,String>();
			param.put("offset", offset);
			param.put("limit", limit);
			param.put("searchType", searchType);
			param.put("searchText", searchText);
			param.put("searchOrder", searchOrder);
			param.put("searchDevice", searchDevice);
			param.put("searchDeviceSn", searchDeviceSn);
			param.put("searchUsed", searchUsed);
			param.put("searchState", searchState);
			param.put("searchdrivingState", searchdrivingState);
			param.put("searchRemoveGroup", searchRemoveGroup);
			param.put("searchGroup", searchGroup);
			param.put("searchDtc", searchDtc);
			param.put("searchWarningItem", searchWarningItem);
			param.put("searchExpireState", searchExpireState);
			
			model.addAttribute("rtvList", commonService.getVehicleList(user,param));
			
			return rtvPage;
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			return rtvPage;
		}
		
	}	
	
	@RequestMapping(value="/com/getDeviceVehicleList.do")					
	public String getDeviceVehicleList(HttpServletRequest req,HttpServletResponse res,
    		ModelMap model ,@ModelAttribute("vehicleVo") @Valid VehicleVO reqVo,BindingResult br) throws Exception  {
		
		String rtvPage = "jsonView";
		res.setContentType("application/x-json; charset=UTF-8");
		
		UserVO user = (UserVO)req.getSession().getAttribute("VDAS");
		
		try {
			String offset = (String)req.getParameter("offset");
			String limit = (String)req.getParameter("limit");
			String searchType = (String)req.getParameter("searchType");
			String searchText = (String)req.getParameter("searchText");
			String searchOrder = (String)req.getParameter("searchOrder");
			String searchDevice = (String)req.getParameter("searchDevice");
			String searchUsed = (String)req.getParameter("searchUsed");
			String searchState = (String)req.getParameter("searchState");
			String searchdrivingState = (String)req.getParameter("searchdrivingState");
			String searchRemoveGroup = (String)req.getParameter("searchRemoveGroup");
			String searchGroup = (String)req.getParameter("searchGroup");
			String searchDtc = (String)req.getParameter("searchDtc");
			String searchWarningItem = (String)req.getParameter("searchWarningItem");
			String searchExpireState = (String)req.getParameter("searchExpireState");
			
			
			
			Map<String,String> param = new HashMap<String,String>();
			param.put("offset", offset);
			param.put("limit", limit);
			param.put("searchType", searchType);
			param.put("searchText", searchText);
			param.put("searchOrder", searchOrder);
			param.put("searchDevice", searchDevice);
			param.put("searchUsed", searchUsed);
			param.put("searchState", searchState);
			param.put("searchdrivingState", searchdrivingState);
			param.put("searchRemoveGroup", searchRemoveGroup);
			param.put("searchGroup", searchGroup);
			param.put("searchDtc", searchDtc);
			param.put("searchWarningItem", searchWarningItem);
			param.put("searchExpireState", searchExpireState);
			
			model.addAttribute("rtvList", commonService.getDeviceVehicleList(user,param));
			
			return rtvPage;
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			return rtvPage;
		}
		
	}	
	
	@RequestMapping(value="/com/fileUpload.do")
	public String fileUpload(HttpServletRequest req, HttpServletResponse response ,ModelMap model) throws Exception {
		long sTime = System.currentTimeMillis();
		HashMap map = new HashMap();
		String rtvPage = "jsonView";
		UserVO user = (UserVO)req.getSession().getAttribute("VDAS");
		String corpDomain = user.getCorpDomain();
		
		try{
			
			//utf-8면 실질적으로jsp에서는 화면이 깨짐 서블릿에서는 euc-kr 로 생성
			response.setCharacterEncoding("euc-kr");
			boolean file_result;
			//default upload path
			File f_midir = new File(Globals.FILE_PATH+System.getProperty("file.separator")+corpDomain);
			
			file_result =  f_midir.mkdirs();
   
			//----------------------------파일 저장에 필요한 객체 생성 부분------------------------------------
			//디스크 파일 아이템 factory
			DiskFileItemFactory  factory = new DiskFileItemFactory();
			//업로드시 사용할 임시 메모리
			factory.setSizeThreshold(4096);
			//업로드시 사용할 temp path
			factory.setRepository(f_midir);
			
			ServletFileUpload upload = new ServletFileUpload(factory);
			//사이즈 제한
			upload.setSizeMax(Integer.parseInt(Globals.FILE_SIZE));
			
			//멀티파트로 넘어온지 여부
			boolean isMultipart = ServletFileUpload.isMultipartContent(req);
			//System.out.println("멀티파트로 넘어온지 여부=" + isMultipart);
			
			//이 시점에서 temp path에 파일이 업로드 (파일뿐만아니라 필드값들도 일단 파일형태로 저장된다.)
			List items = upload.parseRequest(req);
			//System.out.println("list size : " + items.size());

			String uploadType = "";
			Iterator iter = items.iterator();
			while(iter.hasNext()){
				FileItem item = (FileItem)iter.next();
				if(item.isFormField()){
					String key = item.getFieldName();    					
					String value = item.getString("utf-8");
					if(key.equals("uploadType")){
						uploadType = value;
						break;
					}
				}
			}
			
			if(StrUtil.isNullToEmpty(uploadType)) throw new Exception("upload type is undefinded");
			
			File rtvDir = new File(f_midir.getPath()+System.getProperty("file.separator")+uploadType);
			rtvDir.mkdirs();
			
			//업로드된 파일 파싱
			iter = items.iterator();
			map = commonService.getFileUpload(iter,rtvDir);
			logger.debug(map.toString());
   
			model.addAttribute("rtv","S");
			model.addAttribute("rtvMap",map);
			
			if(uploadType.equals("excelUploadUser")) rtvPage = "forward:/account/addUserExcelUpload.do";
			if(uploadType.equals("excelTempVehicle")) rtvPage = "forward:/upload/addVehicleExcelUpload.do";
			
		}catch(Exception e){  
			logger.error("FileUpload Exception : " + e.getMessage());
			model.addAttribute("rtv","F");
		}finally{
			long endTime = System.currentTimeMillis();
			logger.debug("FileUpload Loading Time... : " + (endTime - sTime));
		}
		
		return rtvPage;
    }
	
	@RequestMapping(value={"/com/getUserImg.do","/com/getFile.do"})
	public String getUserImg(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		UserVO user = (UserVO)req.getSession().getAttribute("VDAS");
		String corpDomain = user.getCorpDomain();
		
		String uuid = StrUtil.nvl(req.getParameter("uuid"));
		if(!uuid.equals("")){
			//userId로 img경로 겟
			FileVO file = commonService.getFile(uuid);
			if(file == null){
				model.addAttribute("MSG", "");
				return "/error";
			}else{
				String imgPath = "";
				imgPath = file.getFilePath()+System.getProperty("file.separator")+file.getSaveFileNm();
				
				try {		
					File img = new File(imgPath);
					
					model.addAttribute("downloadFile",img);
					model.addAttribute("downloadFileNm",file.getOrgFileNm());
					
				} catch (Exception e) {
					model.addAttribute("MSG", "");
					logger.error(e);
					return "/error";
				}
			}		
			
			
		}
		
		
		return "download";
	}
	
	@RequestMapping(value="/com/getGroupList.do")
	public String getGroupList(HttpServletRequest req
			,HttpServletResponse res
			,ModelMap model) throws Exception  {
		
		String rtvPage = "jsonView";
		res.setContentType("application/x-json; charset=UTF-8");
		
		UserVO user = (UserVO)req.getSession().getAttribute("VDAS");
		
		
		try {
			String offset = (String)req.getParameter("offset");
			String limit = (String)req.getParameter("limit");
			String searchOrder = (String)req.getParameter("searchOrder");
			
			Map<String,String> param = new HashMap<String,String>();
			param.put("offset", offset);
			param.put("limit", limit);
			param.put("searchOrder", searchOrder);
			
			
			model.addAttribute("rtvList", commonService.getGroupList(user,param));
			
			return rtvPage;
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			return rtvPage;
		}
	}
	@RequestMapping(value="/com/getGroupCode.do")
	public String getGroupCode(HttpServletRequest req
			,HttpServletResponse res
			,ModelMap model) throws Exception  {
		
		String rtvPage = "jsonView";
		res.setContentType("application/x-json; charset=UTF-8");
		
		UserVO user = (UserVO)req.getSession().getAttribute("VDAS");
		
		
		try {
			
			model.addAttribute("rtvList", commonService.getGroupCode(user));
			
			return rtvPage;
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			return rtvPage;
		}
	}
	@RequestMapping(value="/com/getGroupUserList.do")
	public String getGroupUserList(HttpServletRequest req
			,HttpServletResponse res
			,ModelMap model) throws Exception  {
		
		String rtvPage = "jsonView";
		res.setContentType("application/x-json; charset=UTF-8");
		
		UserVO user = (UserVO)req.getSession().getAttribute("VDAS");
		
		
		try {
			
			String corpGroup = (String)req.getParameter("corpGroup");
			
			model.addAttribute("rtvList", commonService.getGroupUserList(user,corpGroup));
			
			return rtvPage;
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			return rtvPage;
		}
	}
	
	@RequestMapping(value="/com/getGroupVehicleList.do")
	public String getGroupVehicleList(HttpServletRequest req
			,HttpServletResponse res
			,ModelMap model) throws Exception  {
		
		String rtvPage = "jsonView";
		res.setContentType("application/x-json; charset=UTF-8");
		
		UserVO user = (UserVO)req.getSession().getAttribute("VDAS");
		String lang = (String)req.getSession().getAttribute("_LANG");
		
		
		try {
			
			String corpGroup = (String)req.getParameter("corpGroup");
			
			model.addAttribute("rtvList", commonService.getGroupVehicleList(user,user.getLang(),corpGroup));
			
			return rtvPage;
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			return rtvPage;
		}
	}
	
	@RequestMapping(value="/com/getImportUserTemplate.do")
	public String getImportUserTemplate(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		UserVO user = (UserVO)req.getSession().getAttribute("VDAS");
		
		String path = CommonController.class.getResource("").getPath().substring(0, CommonController.class.getResource("").getPath().lastIndexOf("WEB-INF"))
	    		+ "common" + System.getProperty("file.separator") + "ref/";
		String file = "importUserTemplate.xlsx";
		
		File f = new File(path+file);
		
		model.addAttribute("downloadFile",f);
		model.addAttribute("downloadFileNm","viewcarPro_사용자등록양식"+".xlsx");
		
		return "download";
	}
	
	@RequestMapping(value="/com/getCityLev1.do")
	public String getCityLev1(HttpServletRequest req
			,HttpServletResponse res
			,ModelMap model) throws Exception  {
		
		String rtvPage = "jsonView";
		String lang = (String)req.getSession().getAttribute("_LANG");
		res.setContentType("application/x-json; charset=UTF-8");
		
		try {
			
			List<Map<String,String>> rtvList = commonService.getCityLev1(lang);
			
			model.addAttribute("rtvList", rtvList);
			
			return rtvPage;
	        
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			return rtvPage;
		}
	}
	@RequestMapping(value="/com/getCityLev2.do")
	public String getCityLev2(HttpServletRequest req
			,HttpServletResponse res
			,ModelMap model) throws Exception  {
		
		String rtvPage = "jsonView";
		String lang = (String)req.getSession().getAttribute("_LANG");
		res.setContentType("application/x-json; charset=UTF-8");
		
		try {
			
			String city = (String)req.getParameter("city");
			
			List<Map<String,String>> rtvList = commonService.getCityLev2(lang,city);
			
			model.addAttribute("rtvList", rtvList);
			
			return rtvPage;
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			return rtvPage;
		}
	}
	
	@RequestMapping(value="/com/getCorpCode.do")
	public String getCorpCode(HttpServletRequest req,HttpServletResponse res,ModelMap model) throws Exception  {
		
		String rtvPage = "jsonView";
		res.setContentType("application/x-json; charset=UTF-8");
		
		try {
			
			
			Map<String, String> param = new HashMap<String,String>();
			
			
			List<Map<String,Object>> rtvList = commonService.getCorpList(param);
			
			model.addAttribute("rtvList", rtvList);
			
			return rtvPage;
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			return rtvPage;
		}
	}
	
	@RequestMapping(value="/com/getAllocateList.do")
	public String getAllocateList(HttpServletRequest req
			,HttpServletResponse res
			,ModelMap model) throws Exception  {
		
		String rtvPage = "jsonView";
		res.setContentType("application/x-json; charset=UTF-8");
		
		UserVO user = (UserVO)req.getSession().getAttribute("VDAS");
		
		
		try {
			
			String searchDate = (String)req.getParameter("searchDate");
			
			model.addAttribute("rtvList", commonService.getAllocateList(user,searchDate));
			
			return rtvPage;
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			return rtvPage;
		}
	}
	
	@RequestMapping(value="/com/getCode.do")
	public String getCode(HttpServletRequest req
			,HttpServletResponse res
			,ModelMap model) throws Exception  {
		
		String rtvPage = "jsonView";
		res.setContentType("application/x-json; charset=UTF-8");
		
		UserVO user = (UserVO)req.getSession().getAttribute("VDAS");
		
		
		try {
			
			String code = (String)req.getParameter("code");
			String val = (String)req.getParameter("val");
			
			model.addAttribute("rtvList", commonService.getCode(user,code,val));
			
			return rtvPage;
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			return rtvPage;
		}
	}
	
	@RequestMapping(value="/com/addrSearch.do")
	public String addrSearch(HttpServletRequest req
			,HttpServletResponse res
			,ModelMap model) throws Exception  {
		
		String rtvPage = "/addr/addrFrame";
		
		try {
			
			return rtvPage;
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			return rtvPage;
		}
	}
	
	@RequestMapping(value="/com/addrPointSearch.do")
	public ResponseEntity addrPointSearch(HttpServletRequest req
			,HttpServletResponse res
			,ModelMap model) throws Exception  {
		
		
		try {
			
			String addr = (String)req.getParameter("addr");
			
			String json=MqttUtil.getGPSAdressByAddressVerSK(addr);
			
			return createResponse(HttpStatus.OK,new String[]{"result"}, json);
		} catch (Exception e) {
			e.printStackTrace();
			return createResponse(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value="/serviceAgreement/policy.do" , method=RequestMethod.GET)
	public String policy2(HttpServletRequest req,HttpServletResponse response) throws Exception  {
		
		String b = "/serviceAgreement/policy";
		try {
			
			return b;
	        
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			return b;
		}
	}	
	
	@RequestMapping(value="/serviceAgreement/location.do" , method=RequestMethod.GET)
	public String policy3(HttpServletRequest req,HttpServletResponse response) throws Exception  {
		
		String b = "/serviceAgreement/location";
		try {
			
			return b;
	        
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			return b;
		}
	}	
	
	@RequestMapping(value="/serviceAgreement/userInfomation.do" , method=RequestMethod.GET)
	public String policy(HttpServletRequest req,HttpServletResponse response) throws Exception  {
		
		String b = "/serviceAgreement/userInfomation";
		try {
			
			return b;
	        
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			return b;
		}
	}	
	*/
}
