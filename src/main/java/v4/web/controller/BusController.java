package v4.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import org.jastecm.string.StrUtil;

@Controller
public class BusController extends BasicWebViewController {

	private Log logger = LogFactory.getLog(getClass());	
	
	/**
	 * 버스 노선관리 - 리스트
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/config/busManager/list")
	public String busManagerList(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/config/busManager/list"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	/**
	 * 버스 노선관리 - 버스 배차 등록
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/config/busManager/allocate/reg")
	public String busManagerAllocateReg(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/config/busManager/allocate/reg"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 버스 노선관리 - 버스 노선 등록
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/config/busManager/serviceRoute/reg")
	public String busManagerServiceRouteReg(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/config/busManager/serviceRoute/reg"; 
		
		try {
			
			String busRouteKey = req.getParameter("busRouteKey");
			
			model.addAttribute("busRouteKey", busRouteKey);
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	@RequestMapping(value="/map/tripBus.do")					
	public String tripBus(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/map/map_sk_bus"; 

		try {
			String busRouteKey = req.getParameter("busRouteKey");
			model.addAttribute("busRouteKey",busRouteKey);
		} catch (Exception e) {
			model.addAttribute("MSG", "");
			logger.error(e);
			return "/error";
		}
		
		return rtvPage;
	}
}
