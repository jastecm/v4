package v4.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import v4.web.vo.data.AccountVO;


@Controller
public class MypageController extends BasicWebViewController {
	private Log logger = LogFactory.getLog(getClass());	
	
	/**
	 * 회원정보 수정 비밀번호 확인
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/mypage/userIdentify")
	public String userIdentify(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/mypage/userIdentify"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 회원정보 수정
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/mypage/userInfo")
	public String userInfo(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		//개인이냐 법인이냐
		AccountVO account = (AccountVO) req.getSession().getAttribute("V4");
				
		String rtvPage = ""; 
		
		try {
			//1개인 2법인 3렌트 4파트너
			if(account.getCorp().getCorpKey().equals("1")){
				 rtvPage = "/mypage/userInfo/person"; 
			}else{
				//여기서 회원 룰에 따라서 마스터냐 일반이냐 해서 놔뉘여야한다.
				
				if(true){
					rtvPage = "/mypage/userInfo/corp";
				}else{
					rtvPage = "/mypage/userInfo/corp/master";
				}
				  
			}
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 비밀번호 변경
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/mypage/passwordChange")
	public String passwordChange(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/mypage/passwordChange"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 가족 관리
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/mypage/familyMng")
	public String familyMng(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/mypage/familyMng"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 가족관리 - 초대
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/mypage/familyMng/invite")
	public String familyMngInvite(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/mypage/familyMng/invite"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 이용요금 조회결재 
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/mypage/serviceFee")
	public String serviceFee(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/mypage/serviceFee"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
	
	/**
	 * 이용요금 조회결재 상세
	 * @param req
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/mypage/serviceFee/detail")
	public String serviceFeeDetail(HttpServletRequest req,
    		ModelMap model) throws Exception  {
		
		String rtvPage = "/mypage/serviceFee/detail"; 
		
		try {
			
			
		} catch (Exception e) {
			logger.error(e);
			return "/error";
		}
		return rtvPage;
	}
}
