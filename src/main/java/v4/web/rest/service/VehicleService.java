package v4.web.rest.service;

import java.util.List;
import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.AllocateVO;
import v4.web.vo.CreateBusVO;
import v4.web.vo.CreateEtcVO;
import v4.web.vo.CreateTruckVO;
import v4.web.vo.CreateVehicleVO;
import v4.web.vo.VehicleVO;
import v4.web.vo.data.TripVO;

public interface VehicleService {

	Map<String,Object> createVehicle(v4.web.vo.data.AccountVO account, CreateVehicleVO vehicleVo)   throws BizException ;
	Map<String,Object> createBus(v4.web.vo.data.AccountVO account, CreateBusVO vehicleVo)   throws BizException ;
	Map<String, Object> createTruck(v4.web.vo.data.AccountVO account, CreateTruckVO vehicleVo)   throws BizException ;
	Map<String, Object> createEtc(v4.web.vo.data.AccountVO account, CreateEtcVO vehicleVo)   throws BizException ;

	Map<String, Object> createVehicleTemp(v4.web.vo.data.AccountVO account, CreateVehicleVO vehicleVo) throws BizException;
	
	void updateVehicle(String vehicleKey, String convCode, String totDistance, String deviceSound)throws BizException;
	
	VehicleVO getVehicle(Map<String,Object> param,Map<String,Object> auth) throws BizException;
	
	List<VehicleVO> getVehicleList(Map<String, Object> param,Map<String,Object> auth) throws BizException;
	
	int getVehicleListCnt(Map<String, Object> param,Map<String,Object> auth) throws BizException;
	
	int duplicatePlateNumChk(String plateNum) throws BizException;
	
	void inactiveVehicle(String vehicleKey) throws BizException;

	void deleteVehicle(String vehicleKey) throws BizException;

	List<VehicleVO> getAllocateListBeCan(AllocateVO vo) throws BizException;

	public void vehicleLastestInfoUpdate(String vehicleKey,int distance, String location, String endAddr, String endAddrEn,
			long endDate, String tripId, String driver) throws BizException;
	public void vehicleLastestInfoUpdate(String vehicleKey,int distance, String location, String endAddr, String detail,String endAddrEn,
			long endDate, String tripId, String driver) throws BizException;

	void updateLastTripStart(String vehicleKey, String tripKey) throws BizException;
	
	void updateTotDistance(TripVO vo) throws BizException;

	VehicleVO getVehicleInAllocate(VehicleVO vvo) throws BizException;

	void updateLastestLoc(VehicleVO vvo)  throws BizException;

	CreateVehicleVO getTempVehicle(String deviceSn) throws BizException;

	void updateLastestDbInfoTemp(String deviceSn, String strDbNm, String strFirmNm, String convCode, int totDistance,String deviceSound) throws BizException;
	

	
}
	

