package v4.web.rest.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import v4.ex.exc.BizException;
import v4.web.rest.dao.PenaltyDAO;
import v4.web.rest.service.PenaltyService;
import v4.web.vo.CreatePenaltyVO;

@Service("penaltyService")
public class PenaltyServiceImpl implements PenaltyService {

	@Autowired
	PenaltyDAO penaltyDAO;

	@Override
	public void createPenalty(CreatePenaltyVO createPenaltyVO) throws BizException {
		try {
			penaltyDAO.createPenalty(createPenaltyVO);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public void deletePenalty(Map<String, Object> param) throws BizException {
		try {
			penaltyDAO.deletePenalty(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}
	
	
}
