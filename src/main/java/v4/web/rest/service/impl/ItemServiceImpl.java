package v4.web.rest.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import v4.ex.exc.BizException;
import v4.web.rest.dao.ItemDAO;
import v4.web.rest.service.ItemService;
import v4.web.rest.service.MailBoxService;
import v4.web.rest.service.PushService;
import v4.web.rest.service.PushType;
import v4.web.rest.service.TimeLineService;
import v4.web.rest.service.data.ItemDataService;
import v4.web.vo.CreateItemVO;
import v4.web.vo.ItemVO;
import v4.web.vo.UpdateItemVO;
import v4.web.vo.data.TripVO;

@Service("itemService")
public class ItemServiceImpl implements ItemService {
	
	@Autowired
	ItemDAO itemDao;
	
	@Autowired
	ItemDataService itemDataService;
	
	@Autowired
	PushService pushService;
	
	@Autowired
	TimeLineService timeLineService;
	
	@Autowired
	MailBoxService mailBoxService;


	@Override
	@Transactional(rollbackFor=Exception.class)
	public String createItem(CreateItemVO itemVo) throws BizException {
		try {
			
			itemDao.createVehicleItem(itemVo);
			return itemVo.getItemKey();
			
		} catch (Exception e) {
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public void updateItem(UpdateItemVO itemVo) throws BizException {
		try {
			
			itemDao.updateVehicleItem(itemVo);
			
		} catch (Exception e) {
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public void deleteVehicleItem(String vehicleKey, String itemKey) throws BizException {
		try {
			Map<String,String> param = new HashMap<String,String>();
			param.put("vehicleKey",vehicleKey);
			param.put("itemKey",itemKey);
			itemDao.deleteVehicleItem(param);
			
		} catch (Exception e) {
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}
	
	@Override
	public int updateItemByTrip(TripVO vo) throws BizException {
		int rtv = 0;
		List<v4.web.vo.data.ItemVO> itemList = itemDataService.getVehicleItems(vo.getVehicle().getVehicleKey(), "");
		for(v4.web.vo.data.ItemVO ivo : itemList){
			if(ivo.getVal() > 0){
				
				String timeLineMainType =  "item";
				String timeLineSubType = "empty";
				
				String accountKey = vo.getVehicle().getCorp().getCorpType().equals("1")?vo.getVehicle().getDefaultAccountKey():vo.getVehicle().getManagerKey();
				String pushMsg = "%s 차량의 %s 교환 주기가 얼마남지 않았습니다.";
				String title = "소모품 교환 주기";
				boolean checker = false;
				Map<String,Object> param = new HashMap<String,Object>();
				
				param.put("vehicleKey", vo.getVehicle().getVehicleKey());
				param.put("eventTime", vo.getEndDate().getTime());
				param.put("eventValue", ivo.getPartsKey());
				
				int chkPointPer = 10; //10%
				
				if(ivo.getVal() - (vo.getDistance()/1000) < 0){
					checker = true;
					rtv++;
				}else if(ivo.getVal() > (ivo.getMax()/chkPointPer)){ //checkPoint 라인을 지나가는 순간 1번
					if(ivo.getVal()*1000 - vo.getDistance() < (ivo.getMax()/chkPointPer)*1000){
						checker = true;
						timeLineSubType = "warning";
						pushMsg = "%s 차량의 %s 교환 주기가 되었습니다.";
						
					}
				}
				
				if(checker){
					pushMsg = String.format(pushMsg, vo.getVehicle().getPlateNum(),ivo.getName());
					
					timeLineService.insertTimeLineEvent(timeLineMainType,timeLineSubType, param);
					pushService.pushChkAccept(PushType.PUSHTYPE_ITEM, accountKey, "", title, pushMsg);
					
					pushService.insertPushHstr(vo.getVehicle().getCorp().getCorpKey(),PushType.PUSHTYPE_ITEM,title,pushMsg);
					
					long mailExpired = 1000*60*60*24*7L;						
					mailBoxService.simgleUserMailBoxInsert(accountKey, title, pushMsg, mailExpired);
				}
			}else{
				rtv++;
			}
		}
		
		itemDao.updateItemByTrip(vo);
		
		return rtv;
		
	}
}
