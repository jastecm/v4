package v4.web.rest.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import v4.ex.exc.BizException;
import v4.web.rest.dao.GeoFenceDAO;
import v4.web.rest.service.AccountService;
import v4.web.rest.service.GeoFenceService;
import v4.web.rest.service.MailBoxService;
import v4.web.rest.service.PushService;
import v4.web.rest.service.PushType;
import v4.web.rest.service.TimeLineService;
import v4.web.vo.CreateGeoFenceVO;
import v4.web.vo.GeoFenceVO;
import v4.web.vo.data.VehicleVO;


@Service("geoFenceService")
public class GeoFenceServiceImpl implements GeoFenceService {

	@Autowired
	GeoFenceDAO geoFenceDao;
	
	@Autowired
	AccountService accountService;
	
	@Autowired
	PushService pushService;
	
	@Autowired
	MailBoxService mailBoxService;
	
	@Autowired
	TimeLineService timeLineService;
	
	@Override
	public void geoFence(VehicleVO vvo,String driverAccountKey,String tripKey,double lat, double lon) throws BizException {
		try {
			
			if(lat == 0 && lon == 0) return;
			
			String accountKey = driverAccountKey;
			
			String fenceMonitor = StrUtil.isNullToEmpty(vvo.getGeofenceMonitor(), "");
			
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("corpKey", vvo.getCorp().getCorpKey());
			param.put("vehicleKey", vvo.getVehicleKey());
			param.put("lat", lat);
			param.put("lon", lon);
			
			
			GeoFenceVO vo = geoFenceDao.getGeoFenceChk(param);
			
			if(vo == null) {
				vo = new GeoFenceVO();
				vo.setFenceKey("");
			}
			
			if(!fenceMonitor.equals(vo.getFenceKey())){
				String inoutChk = "1"; //in
				if(StrUtil.isNullToEmpty(vo.getFenceKey())) inoutChk = "0"; //out
				
				String alies = "";
				String addr = "";
				double fence_lat = 0;
				double fence_lon = 0;
				String title = "";
				String msg = "";
				
				if(inoutChk.equals("1")){
					alies = vo.getAlies();
					addr = vo.getAddr() +" "+vo.getAddrDetail();	
					fence_lat = lat;
					fence_lat = lon;
					title = vo.getAlies()+" 진입";
					msg = "["+vvo.getPlateNum()+"] "+vo.getAlies()+"에 진입했습니다.";
				}else{
					
					GeoFenceVO prevVo = geoFenceDao.getGeoFence(fenceMonitor);
					alies = prevVo.getAlies();
					addr = prevVo.getAddr() +" "+prevVo.getAddrDetail();	
					fence_lat = lat;
					fence_lat = lon;
					title = prevVo.getAlies()+" 이탈";
					msg = "["+vvo.getPlateNum()+"] "+prevVo.getAlies()+"에 이탈했습니다.";
				}
				
				//update
				param.clear();
				param.put("vehicleKey", vvo.getVehicleKey());
				param.put("fenceMonitor", vo.getFenceKey());
				geoFenceDao.updateFenceMonitor(param);
				
				/*
				param.put("driverKey", driverKey);
				param.put("chk", chk);
				param.put("alies", alies);
				param.put("addr", addr);
				param.put("vehicleKey", vvo.getVehicleKey());
				param.put("corp", vvo.getCorp());
				param.put("lat", fence_lat);
				param.put("lon", fence_lon);
				restGeoFenceDao.saveGeofenceHstr(param);
				*/
				
				
				param.clear();						
				param.put("vehicleKey", vvo.getVehicleKey());
				param.put("eventTime", new Date().getTime());
				param.put("eventValue", msg);
				
				timeLineService.insertTimeLineEvent("geoFence",inoutChk.equals("1")?"IN":"OUT", param);
												
				pushService.pushChkAccept(PushType.PUSHTYPE_GEOFENCE, accountKey, "", title, msg);
				pushService.insertPushHstr(vvo.getCorp().getCorpKey(),PushType.PUSHTYPE_GEOFENCE,title,msg);
				long mailExpired = 1000*60*60*24*7L;						
				mailBoxService.simgleUserMailBoxInsert(accountKey, title, msg, mailExpired);
				
				if(!vvo.getCorp().getCorpType().equals("1") && !accountKey.equals(vvo.getManagerKey())){
					accountKey = vvo.getManagerKey();
					pushService.pushChkAccept(PushType.PUSHTYPE_GEOFENCE, accountKey, "", title, msg);
					pushService.insertPushHstr(vvo.getCorp().getCorpKey(),PushType.PUSHTYPE_GEOFENCE,title,msg);
					mailExpired = 1000*60*60*24*7L;						
					mailBoxService.simgleUserMailBoxInsert(accountKey, title, msg, mailExpired);
				}
				
			}
			
			
		}catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}
	
	/*
	@Override
	public GeoFenceVO getGeoFenceAllChk(CreateGeoFenceVO vo){
		
		return geoFenceDao.getGeoFenceAllChk(vo);
	}
	
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void addGeoFence(CreateGeoFenceVO vo,int max) throws BizException {
		try {
			
			int chk = geoFenceDao.getGeoFenceCnt(vo);
			
			if(chk >= max)
				throw new BizException("geoFence overflow");
			
			geoFenceDao.addGeoFence(vo);
			
		}catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
		
	}

	@Override
	public List<GeoFenceVO> getGeoFenceList(String vehicleKey,String corpKey,String all) throws BizException {
		try {
			
			if(!StrUtil.isNullToEmpty(vehicleKey) && !StrUtil.isNullToEmpty(corpKey)) new BizException("only used vehicleKey or corpKey");
			
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("vehicleKey", vehicleKey);
			param.put("corpKey", corpKey);
			param.put("all", all);
			
			return geoFenceDao.getGeoFenceList(param);
						
		}catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public GeoFenceVO getGeoFence(String fenceKey) throws BizException {
		try {
			
			return geoFenceDao.getGeoFence(fenceKey);
						
		}catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public void updateGeoFence(CreateGeoFenceVO vo) throws BizException {
		try {
			
			geoFenceDao.updateGeoFence(vo);
						
		}catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}
	
	@Override
	public void deleteGeoFence(String fenceKey) throws BizException {
		try {
			geoFenceDao.deleteGeoFence(fenceKey);
						
		}catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}
*/
	
	/************************************************/
	/*
	@Override
	public void updateGeoFencePush(String fenceKey , String push) throws BizException {
		try {
			GeoFenceVO vo = new GeoFenceVO();
			vo.setFenceKey(fenceKey);
			vo.setPush(push);
			
			geoFenceDao.updateGeoFencePush(vo);
						
		}catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}
	
	
	
	
	

	@Override
	public void updateGeofenceOnOff(String key, String val) throws BizException {
		try {
			Map<String, String> param = new HashMap<String, String>();
			param.put("fenceKey", key);
			param.put("push", val);
			geoFenceDao.updateGeofenceOnOff(param);
						
		}catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public List<Map<String, String>> getGeoFenceUserList(viewcar.pro.vo.UserVO user, Map<String, String> param) {
		param.put("corp", user.getCorp());
		param.put("rule", user.getRule());
		param.put("ruleAccountKey", user.getAccountKey());
		
		return geoFenceDao.getGeoFenceUserList(param);
	}

	@Override
	public List<Map<String, String>> getGeoFenceUserDetail(viewcar.pro.vo.UserVO user, Map<String, String> param) {
		param.put("corp", user.getCorp());
		param.put("rule", user.getRule());
		param.put("ruleAccountKey", user.getAccountKey());
		
		return geoFenceDao.getGeoFenceUserDetail(param);
	}

	@Override
	public void deleteGeoFenceHstr(String fenceKey) {
		geoFenceDao.deleteGeoFenceHstr(fenceKey);
		
	}

	@Override
	public int getGeofenceCnt(viewcar.pro.vo.UserVO user, Map<String, String> param) {
		param.put("vehicleKey", user.getCorp());
		
		return geoFenceDao.getGeoFenceCnt(user.getCorp());
		
	}
	*/
	
	
}

