package v4.web.rest.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import v4.ex.exc.BizException;
import v4.web.rest.dao.TimeLineDAO;
import v4.web.rest.service.TimeLineService;
import v4.web.vo.EventVO;
import v4.web.vo.EventVO_Attach;
import v4.web.vo.EventVO_Item;
import v4.web.vo.EventVO_Milige;
import v4.web.vo.EventVO_Trip;
import v4.web.vo.EventVO_Trouble;

@Service("timeLineService")
public class TimeLineServiceImpl implements TimeLineService {
	
	@Autowired
	TimeLineDAO timeLineDao;

	@Override
	public void insertTimeLineEvent(String type, String sub, Map<String, Object> vo) throws BizException {
		vo.put("eventType", type);
		vo.put("eventTypeSub", sub);
		
		timeLineDao.insertTimeLineEvent(vo);
	}

	@Override
	public List<EventVO> getTimeLine(String userKey, String vehicleKey, String searchYm, String fillter, String offset,
			int limit, String scope, String lang) throws BizException {
		Map<String,String> param = new HashMap<String,String>();
		param.put("userKey", userKey);
		param.put("vehicleKey", vehicleKey);
		param.put("searchYm", searchYm);
		param.put("offset", offset);
		param.put("lang", lang);
		param.put("scope", scope);
		
		List<EventVO_Trip> tripList = new ArrayList<EventVO_Trip>();
		if(StrUtil.isNullToEmpty(fillter) || fillter.contains("trip")){
			tripList = timeLineDao.getTripList(param);
		}
		
		List<EventVO_Item> itemList = new ArrayList<EventVO_Item>();
		if(StrUtil.isNullToEmpty(fillter) || fillter.contains("item")){
			itemList = timeLineDao.getItemList(param);
		}
		
		List<EventVO_Milige> miligeList = new ArrayList<EventVO_Milige>();
		if(StrUtil.isNullToEmpty(fillter) || fillter.contains("milige")){
			miligeList = timeLineDao.getMiligeList(param);
		}
		
		List<EventVO_Attach> attachList = new ArrayList<EventVO_Attach>();
		if(StrUtil.isNullToEmpty(fillter) || fillter.contains("attach")){
			attachList = timeLineDao.getAttachList(param);
		}
		
		List<EventVO_Trouble> troubleList = new ArrayList<EventVO_Trouble>();
		if(StrUtil.isNullToEmpty(fillter) || fillter.contains("trouble")){
			troubleList = timeLineDao.getTroubleList(param);
		}
		
		List<EventVO> sortList = sort(tripList,itemList,miligeList,attachList,troubleList);
		sortList = limit(sortList,limit);
		
		return sortList;
	}

	@Override
	public int getTimeLineChk(String vehicleKey, String searchYm, String scope, String accountKey) throws BizException {
		Map<String,String> param = new HashMap<String,String>();
		param.put("vehicleKey", vehicleKey);
		param.put("searchYm", searchYm);
		param.put("scope", scope);
		param.put("userKey", accountKey);
		int rtv = 0;
		rtv += timeLineDao.getTripListCnt(param);
		rtv += timeLineDao.getItemListCnt(param);
		rtv += timeLineDao.getMiligeListCnt(param);	
		rtv += timeLineDao.getAttachListCnt(param);	
		rtv += timeLineDao.getTroubleListCnt(param);	
		return rtv;
	}
	
	private List<EventVO> limit(List<EventVO> sortList,int limit) {
		if(sortList.size() == 0) return  new ArrayList<EventVO>();
		
		List<EventVO> rtvList = new ArrayList<EventVO>();
		
		long prevPoint = sortList.get(0).getSortDate();
		//limit--;
		for(int i = 0,j = 1 ; i < sortList.size() && limit >= j ; i++){
			if(prevPoint > sortList.get(i).getSortDate()){
				prevPoint = sortList.get(i).getSortDate();
				j++;
				if(limit >= j) rtvList.add(sortList.get(i));
			}else if(prevPoint == sortList.get(i).getSortDate()) {
				rtvList.add(sortList.get(i));
			}
		}
		
		return rtvList;
	}

	private List<EventVO> sort(List<? extends EventVO> tripList, List<? extends EventVO> itemList, List<? extends EventVO> miligeList
			,List<? extends EventVO> attachList,List<? extends EventVO> troubleList) {
		
		List<EventVO> tempList = new ArrayList<EventVO>();
		
		tempList.addAll(tripList);
		tempList.addAll(itemList);
		tempList.addAll(miligeList);
		tempList.addAll(attachList);
		tempList.addAll(troubleList);
		
		List<EventVO> rtvList = new ArrayList<EventVO>();
		
		while(tempList.size()>0){
			long pointVal = 0;
			int pointIdx = 0;
			
			for(int i = 0 ; i < tempList.size() ; i++){
				if(pointVal < tempList.get(i).getSortDate()) {
					pointVal = tempList.get(i).getSortDate();
					pointIdx = i;
				}
			}
			rtvList.add(tempList.get(pointIdx));
			tempList.remove(pointIdx);
		}
		
		return rtvList;
	}

}
