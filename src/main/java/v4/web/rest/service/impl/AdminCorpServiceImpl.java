package v4.web.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import v4.ex.exc.BizException;
import v4.web.rest.dao.AdminCorpDAO;
import v4.web.rest.service.AdminCorpService;
import v4.web.vo.AdminCorpVO;


@Service("adminCorpService")
public class AdminCorpServiceImpl implements AdminCorpService {

	@Autowired
	AdminCorpDAO adminCorpDao;
	
	
	@Override
	public AdminCorpVO getAdminCorpInfo(String adminCorp) throws BizException {
		try {
			return adminCorpDao.getAdminCorpInfo(adminCorp);
		}catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}
}

