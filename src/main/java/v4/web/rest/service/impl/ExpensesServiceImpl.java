package v4.web.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import v4.ex.exc.BizException;
import v4.web.rest.dao.ExpensesDAO;
import v4.web.rest.service.ExpensesService;
import v4.web.vo.CreateExpensesBulkVO;
import v4.web.vo.CreateExpensesVO;

@Service("expensesService")
public class ExpensesServiceImpl implements ExpensesService {

	
	@Autowired
	ExpensesDAO expensesDAO;
	
	@Override
	public void createExpenses(CreateExpensesVO vo) throws BizException {
		try {
			expensesDAO.createExpenses(vo);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public void createExpensesBulk(CreateExpensesBulkVO vo) throws BizException {
		
		try {
			expensesDAO.createExpensesBulk(vo);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

}
