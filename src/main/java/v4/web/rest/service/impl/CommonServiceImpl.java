package v4.web.rest.service.impl;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.fileupload.FileItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import v4.web.rest.dao.CommonDAO;
import v4.web.rest.service.CommonService;
import v4.web.vo.FileVO;


@Service("commonService")
public class CommonServiceImpl implements CommonService {
	
	@Autowired
	private CommonDAO commonDao;
	
	public HashMap<String,String> getFileUpload(Iterator iter, File fMidir,String accountKey) {
		HashMap file_map = new HashMap();
		try{
			while(iter.hasNext()){
				FileItem item = (FileItem)iter.next();
				//파일이 아닌경우 (필드 파라메터)
				if(item.isFormField()){
					String key = item.getFieldName();    
					//한글 꺠지는거 방지 utf-8로 재설정
					String value = item.getString("utf-8");
					//System.out.println("key=" + key + " value=" + value);
					file_map.put(key, value);
				
				//파일처리 + 중복처리	
				}else{
					FileVO vo = new FileVO();
					
					String key = item.getFieldName();
					
					//한글 계속 깨짐 
					String tempfileName = new String(item.getName().getBytes("euc-kr"),"KSC5601");
					String orgFileName = "";
					String saveFileName = "";
					
					//파일이름이 존재하고 업로드할 파일이 존재한다면 업로드 시작
					if(!tempfileName.equals("") && tempfileName != null){
						vo.setAccountKey(accountKey);
						//파일명 추출
						int idx = tempfileName.lastIndexOf(System.getProperty("file.separator"));
						
						String fileSize = String.valueOf(item.getSize());
						vo.setFileSize(fileSize);
						
						//파일 풀 name(확장자포함)
						orgFileName = tempfileName.substring(idx + 1);
						vo.setOrgFileNm(orgFileName);
						
						//확장자 추출
						String fileExt = orgFileName.substring(orgFileName.lastIndexOf("."));
						vo.setFileExt(fileExt);
						
						//파일명 추출
						orgFileName = orgFileName.substring(0,orgFileName.lastIndexOf("."));
						
						//저장될 파일명
						saveFileName = orgFileName+fileExt;
						
						//중복 파일 처리
						File save_file = new File(fMidir.getPath(),saveFileName);
						//파일이 존재한다면
						int i = 0;
						while(save_file.exists()){
							i++;
							//fileName_i
							saveFileName = orgFileName+"_"+i+fileExt;
							save_file = new File(fMidir.getPath(),saveFileName);
						}
							
						vo.setFilePath(fMidir.getPath());
						vo.setSaveFileNm(saveFileName);						
						item.write(save_file);
						//중복 파일 처리 End
						
						String uuid = insertFileUpload(vo);
						file_map.put(key, uuid);
						file_map.put("fileName", tempfileName);
						
					}else{
						file_map.put(key, null);
					}//End Of FileName if
				}                           
			}                
		}catch(Exception e){
		}finally{}
		
		return file_map;
	}
	
	public String insertFileUpload(FileVO vo) throws Exception{
		commonDao.insertFileUpload(vo);
		return vo.getFileKey();
	}
	
	public FileVO getFile(String vo) throws Exception{
		return commonDao.getFile(vo);
	}
	/*
	@Autowired
	private VehicleService vehicleService;
	
	public String getAuthId(String table) throws BizException{
		try {
			int chk = 1;
			String authId = "";
			
			while(chk>0){
				authId = commonDao.getAuthId();
				HashMap<String,String> param = new HashMap<String, String>();
				param.put("table", table);
				param.put("authId", authId);
				chk = commonDao.chkAuthId(param);
			}
			
			return authId;
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException(e.getMessage());
		}
		
		
	}
	
	public List<CodeVO> getCodeList(CodeVO vo) throws BizException{
		return commonDao.getCodeList(vo);
	}

	@Override
	public List<String> getDeviceSeries(UserVO user) throws BizException {
		Map<String,String> param = new HashMap<String,String>();
		param.put("lang", user.getLang());
		param.put("corp", user.getCorp());
		return commonDao.getDeviceSeries(param);
	}

	@Override
	public List<Map<String,String>> getDeviceList(UserVO user, String deviceSeries, String free) throws BizException {
		Map<String,String> param = new HashMap<String,String>();
		param.put("lang", user.getLang());
		param.put("corp", user.getCorp());
		param.put("deviceSeries", deviceSeries);
		
		if(StrUtil.isNullToEmpty(free))
			return commonDao.getDeviceList(param);
		else return commonDao.getFreeDeviceList(param);
	}

	@Override
	public List<UserVO> getUserList(UserVO user, Map<String, String> param) {
		param.put("lang",user.getLang());
		param.put("corp", user.getCorp());
		param.put("corpDomain", user.getCorpDomain());
		return commonDao.getUserList(param);
	}

	@Override
	public List<VehicleVO> getVehicleList(UserVO user, Map<String, String> param) {
		param.put("lang",user.getLang());
		param.put("corp", user.getCorp());
		return commonDao.getVehicleList(param);
	}
	
	@Override
	public VehicleVO getVehicle(UserVO user, Map<String, String> param) {
		param.put("lang",user.getLang());
		param.put("corp", user.getCorp());
		return commonDao.getVehicle(param);
	}

	@Override
	public UserVO getUser(UserVO user, Map<String, String> param) {
		param.put("lang",user.getLang());
		param.put("corp", user.getCorp());
		param.put("corpDomain", user.getCorpDomain());
		return commonDao.getUser(param);
	}

	public String insertFileUpload(FileVO vo) throws Exception{
		commonDao.insertFileUpload(vo);
		return vo.getFileKey();
	}
	
	public FileVO getFile(String vo) throws Exception{
		return commonDao.getFile(vo);
	}
	
	
	public HashMap<String,String> getFileUpload(Iterator iter, File fMidir) {
		HashMap file_map = new HashMap();
		String file_name = "";
		try{
			while(iter.hasNext()){
				FileItem item = (FileItem)iter.next();
				//파일이 아닌경우 (필드 파라메터)
				if(item.isFormField()){
					String key = item.getFieldName();    
					//한글 꺠지는거 방지 utf-8로 재설정
					String value = item.getString("utf-8");
					//System.out.println("key=" + key + " value=" + value);
					file_map.put(key, value);
				
				//파일처리 + 중복처리	
				}else{
					FileVO vo = new FileVO();
					
					String key = item.getFieldName();
					
					//한글 계속 깨짐 
					String tempfileName = new String(item.getName().getBytes("euc-kr"),"KSC5601");
					String orgFileName = "";
					String saveFileName = "";
					
					//파일이름이 존재하고 업로드할 파일이 존재한다면 업로드 시작
					if(!tempfileName.equals("") && tempfileName != null){
						//System.out.println("filefieldname=" + key + " fileName=" + tempfileName);
						//파일명 추출
						int idx = tempfileName.lastIndexOf(System.getProperty("file.separator"));
						
						String fileSize = String.valueOf(item.getSize());
						vo.setFileSize(fileSize);
						
						//파일 풀 name(확장자포함)
						orgFileName = tempfileName.substring(idx + 1);
						vo.setOrgFileNm(orgFileName);
						
						//확장자 추출
						String fileExt = orgFileName.substring(orgFileName.lastIndexOf("."));
						vo.setFileExt(fileExt);
						
						//파일명 추출
						orgFileName = orgFileName.substring(0,orgFileName.lastIndexOf("."));
						
						//저장될 파일명
						saveFileName = orgFileName+fileExt;
						
						//중복 파일 처리
						File save_file = new File(fMidir.getPath(),saveFileName);
						//파일이 존재한다면
						int i = 0;
						while(save_file.exists()){
							i++;
							//fileName_i
							saveFileName = orgFileName+"_"+i+fileExt;
							save_file = new File(fMidir.getPath(),saveFileName);
						}
							
						vo.setFilePath(fMidir.getPath());
						vo.setSaveFileNm(saveFileName);						
						item.write(save_file);
						//중복 파일 처리 End
						
						String uuid = insertFileUpload(vo);
						file_map.put(key, uuid);
						
					}else{
						file_map.put(key, null);
					}//End Of FileName if
				}                           
			}                
		}catch(Exception e){
		}finally{}
		
		return file_map;
	}
	
	@Override
	public List<GroupVO> getGroupList(UserVO user, Map<String, String> param) {
		param.put("searchCorp", user.getCorp());
		param.put("lang", user.getLang());
		return commonDao.getGroupList(param);
	}

	@Override
	public String getDeivceIdFromSn(String deviceSn) throws BizException {
		return commonDao.getDeivceIdFromSn(deviceSn);
	}

	@Override
	public Map<String, Integer> getVehicleCnt(UserVO user) throws BizException {
		Map<String,String> param = new HashMap<String,String>();
		param.put("corp", user.getCorp());
		
		Map<String,Integer> rtvMap = new HashMap<String,Integer>();
		rtvMap.put("total", commonDao.getVehicleCnt(param));
		param.put("used","1");
		rtvMap.put("used", commonDao.getVehicleCnt(param));
		param.put("used","0");
		rtvMap.put("stop", commonDao.getVehicleCnt(param));
		param.put("state","1");
		rtvMap.put("using", commonDao.getVehicleCnt(param));
		param.put("state","2");
		rtvMap.put("allocate", commonDao.getVehicleCnt(param));
		return rtvMap;
	}

	@Override
	public Map<String, Integer> getUserCnt(UserVO user) throws BizException {
		Map<String,String> param = new HashMap<String,String>();
		param.put("corp", user.getCorp());
		
		Map<String,Integer> rtvMap = new HashMap<String,Integer>();
		rtvMap.put("total", commonDao.getUserCnt(param));
		return rtvMap;
	}

	@Override
	public List<Map<String, String>> getCityLev1(String lang) throws BizException {
		Map<String,String> param = new HashMap<String,String>();
		param.put("lang", lang);
		
		return commonDao.getCityLev1(param); 
	}
	@Override
	public List<Map<String, String>> getCityLev2(String lang ,String city) throws BizException {
		Map<String,String> param = new HashMap<String,String>();
		param.put("lang", lang);
		param.put("city", city);
		
		return commonDao.getCityLev2(param); 
	}
	
	@Override
	public List<Map<String,String>> getGroupCode(UserVO user) throws BizException{
		Map<String,String> param = new HashMap<String,String>();
		param.put("corp", user.getCorp());
		
		return commonDao.getGroupCode(param); 
	}

	@Override
	public List<Map<String,String>> getGroupUserList(UserVO user,String corpGroup) throws BizException{
		Map<String,String> param = new HashMap<String,String>();
		param.put("corp", user.getCorp());
		param.put("corpGroup", corpGroup);
		param.put("rule", user.getRule());
		param.put("ruleAccount",user.getAccountKey());
		
		return commonDao.getGroupUserList(param); 
	}

	@Override
	public List<Map<String, Object>> getCorpList(Map<String, String> param) throws BizException {
		
		return commonDao.getCorpList(param);
	}

	@Override
	public List<Map<String,String>> getGroupVehicleList(UserVO user, String lang, String corpGroup) throws BizException {
		Map<String,String> param = new HashMap<String,String>();
		param.put("corp", user.getCorp());
		param.put("lang", lang);
		param.put("corpGroup", corpGroup);
		
		return commonDao.getGroupVehicleList(param);
	}

	@Override
	public Map<String, Integer> getDrivingCnt(UserVO user) throws BizException {
		Map<String,String> param = new HashMap<String,String>();
		param.put("corp", user.getCorp());
		
		Map<String,Integer> rtvMap = new HashMap<String,Integer>();
		rtvMap.put("total", commonDao.getDrivingCnt(param));
		
		return rtvMap;
	}

	@Override
	public Map<String, Integer> getSearchDrivingCnt(UserVO user, Map<String, Object> param) throws BizException {
		param.put("corp", user.getCorp());
		param.put("lang", user.getLang());
		
		Map<String,Integer> rtvMap = new HashMap<String,Integer>();
		rtvMap.put("total", commonDao.getSearchDrivingCnt(param));
		
		return rtvMap;
	}

	@Override
	public int sendReport(Map<String,String> param) throws BizException {
		int rtv = commonDao.sendReport(param);
		
		if(param.get("flag") != null && param.get("flag").equals("1")) return rtv;
		
		String allocateKey = param.get("allocateKey");
		
		String vehicleKey = vehicleService.getAllocateVehicle(allocateKey);
		VehicleVO vo = new VehicleVO();
		vo.setVehicleKey(vehicleKey);
		vo = vehicleService.getVehicle(vo);
		
		String lastestLoc = vo.getLastestLocation();
		
		int dis = getDistance(lastestLoc , param.get("lon")+","+param.get("lat"));
		
		if(dis < 1000)
			vehicleService.vehicleLastestInfoUpdate(vehicleKey,0,param.get("lon")+","+param.get("lat"),param.get("addr"),param.get("addr"),param.get("addrDetail"),new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()),null);
		
		
		return rtv;
	}

	@Override
	public Map<String, Integer> getReportCnt(UserVO user) throws BizException {
		Map<String, String> param = new HashMap<String,String>();
		param.put("corp", user.getCorp());
		

		Map<String,Integer> rtvMap = new HashMap<String,Integer>();
		rtvMap.put("total", commonDao.getReportCnt(param));
		param.put("flag","1");
		rtvMap.put("submit", commonDao.getReportCnt(param));
		param.put("flag","0");
		rtvMap.put("unsubmit", commonDao.getReportCnt(param));

		return rtvMap;
	}

	@Override
	public List<Map<String,String>> getAllocateList(UserVO user, String searchDate) throws BizException {
		Map<String,String> param = new HashMap<String,String>();
		param.put("corp", user.getCorp());
		param.put("accountKey", user.getAccountKey());
		param.put("rule", user.getRule());
		param.put("searchDate", searchDate);
		
		return commonDao.getAllocateList(param);
	}

	@Override
	public List<Map<String, String>> getCode(UserVO user, String code, String val) throws BizException {
		Map<String,String> param = new HashMap<String,String>();
		param.put("lang", user.getLang());
		param.put("code", code);
		param.put("val", val);
		
		return commonDao.getCode(param);
	}

	@Override
	public List<VehicleVO> getDeviceVehicleList(UserVO user, Map<String, String> param) throws BizException {
		param.put("lang",user.getLang());
		param.put("corp", user.getCorp());
		return commonDao.getDeviceVehicleList(param);
	}

	@Override
	public int getDistance(String from, String to) throws BizException {
		from = from.replace(",", " ");
		to = to.replace(",", " ");
		
		Map<String, String> param = new HashMap<String, String>();
		param.put("from", from);
		param.put("to", to);
		
		return commonDao.getDistance(param);
	}

	@Override
	public List<Map<String, Object>> getMasterList(Map<String, String> param) throws BizException {
		return commonDao.getMasterList(param);
	}
	*/

	@Override
	public List<HashMap<String, Object>> getTree() {

		return commonDao.getTree();
	}

	@Override
	public Map<String, Object> getCorp(Map<String, Object> param) {

		return commonDao.getCorp(param);
	}

	@Override
	public String getGroupManagerAccountKey(String groupKey) {
		// TODO Auto-generated method stub
		return commonDao.getGroupManagerAccountKey(groupKey);
	}
}

