package v4.web.rest.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import v4.ex.exc.BizException;
import v4.web.rest.dao.DownloadDAO;
import v4.web.rest.dao.VehicleDbDAO;
import v4.web.rest.service.VehicleDBService;
import v4.web.vo.VehicleDbVO;


@Service("vehicleDBService")
public class VehicleDBServiceImpl implements VehicleDBService {

	@Autowired
	VehicleDbDAO vehicleDbDao;
	
	@Autowired
	DownloadDAO downloadDao;
	
	@Override
	public List<String> getManufacture(String lang) throws BizException{
		try {
			return vehicleDbDao.getManufacture(lang);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}

	@Override
	public List<String> getModelMaster(String lang, String manufacture)
			throws BizException {
		try {
			Map<String,String> param = new HashMap<String,String>();
			param.put("lang", lang);
			param.put("manufacture", manufacture);
			
			return vehicleDbDao.getModelMaster(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public List<String> getYear(String lang, String manufacture,
			String modelMaster) throws BizException {
		try {
			Map<String,String> param = new HashMap<String,String>();
			param.put("lang", lang);
			param.put("manufacture", manufacture);
			param.put("modelMaster", modelMaster);
			
			return vehicleDbDao.getYear(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public List<String> getModelHeader(String lang, String manufacture,
			String modelMaster, String year) throws BizException {
		try {
			Map<String,String> param = new HashMap<String,String>();
			param.put("lang", lang);
			param.put("manufacture", manufacture);
			param.put("modelMaster", modelMaster);
			param.put("year", year);
			
			return vehicleDbDao.getModelHeader(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public List<String> getTransmission(String lang, String manufacture,
			String modelMaster, String year, String modelHeader)
			throws BizException {
		try {
			Map<String,String> param = new HashMap<String,String>();
			param.put("lang", lang);
			param.put("manufacture", manufacture);
			param.put("modelMaster", modelMaster);
			param.put("year", year);
			param.put("modelHeader", modelHeader);
			
			return vehicleDbDao.getTransmission(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public List<String> getFuelType(String lang, String manufacture,
			String modelMaster, String year, String modelHeader)
			throws BizException {
		try {
			Map<String,String> param = new HashMap<String,String>();
			param.put("lang", lang);
			param.put("manufacture", manufacture);
			param.put("modelMaster", modelMaster);
			param.put("year", year);
			param.put("modelHeader", modelHeader);
			
			return vehicleDbDao.getFuelType(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public List<String> getVolume(String lang, String manufacture,
			String modelMaster, String year, String modelHeader)
			throws BizException {
		try {
			Map<String,String> param = new HashMap<String,String>();
			param.put("lang", lang);
			param.put("manufacture", manufacture);
			param.put("modelMaster", modelMaster);
			param.put("year", year);
			param.put("modelHeader", modelHeader);
			
			return vehicleDbDao.getVolume(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public Map<String,String> getConvCode(VehicleDbVO vo) throws BizException {
		try {
			Map<String,String> rtv = vehicleDbDao.getConvCode(vo);
			
			String convCode = rtv.get("convCode");
			String path = downloadDao.getVehicleDbPathByConvCode(convCode);
			
			if(StrUtil.isNullToEmpty(path)) rtv.put("fileDb", "G");
			else rtv.put("fileDb","M");
			
			return rtv;
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public Map<String, String> getVehicleDbInfo(String lang, String convCode) throws BizException {
		try {
			Map<String,String> param = new HashMap<String,String>();
			param.put("lang", lang);
			param.put("convCode", convCode);
			return vehicleDbDao.getVehicleDbInfo(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
}

