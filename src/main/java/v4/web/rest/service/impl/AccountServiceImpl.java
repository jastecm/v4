package v4.web.rest.service.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import v4.ex.exc.BizException;
import v4.web.rest.dao.AccountDAO;
import v4.web.rest.service.AccountService;
import v4.web.vo.AccountVO;
import v4.web.vo.CreateCorpVO;
import v4.web.vo.CreateInviteVO;
import v4.web.vo.CreatePartnerVO;
import v4.web.vo.CreatePersonVO;
import v4.web.vo.CreateRentVO;
import v4.web.vo.CreateUserVO;
import v4.web.vo.LoginVO;


@Service("accountService")
public class AccountServiceImpl implements AccountService {

	@Autowired
	AccountDAO accountDao;

	@Override
	public AccountVO getAccountFromIdPw(LoginVO vo) throws BizException {
		
		try {
			
			return accountDao.getAccountFromIdPw(vo);
			
		}catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}

	@Override
	public AccountVO getAccount(String userKey) throws BizException {
		try {
			return accountDao.getAccountFromKey(userKey);
		}catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public AccountVO getAccountSession(String userKey) throws BizException {
		try {
			return accountDao.getAccountFromKeySession(userKey);
		}catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public AccountVO getAccountFromId(String userId) throws BizException {
		try {
			return accountDao.getAccountFromId(userId);
		}catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public List<AccountVO> getAccountList(Map<String,Object> param , Map<String,Object> auth) throws BizException {
		try {
			
			return accountDao.getAccountList(param);
			
		}catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	@Override
	public int getAccountListCnt(Map<String,Object> param , Map<String,Object> auth) throws BizException {
		try {
			
			return accountDao.getAccountListCnt(param);
			
		}catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
				System.err.println(e.getMessage());
				throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public boolean createCorpUser(CreateCorpVO vo,int rule,int subRule) throws BizException {
		try {
			
			AccountVO avo = new AccountVO(vo.getAccountId(),vo.getAccountPw(),vo.getCorpKey()
					,vo.getMobilePhone(),vo.getMobilePhoneCtyCode(),vo.getPhone(),vo.getPhoneCtyCode(),rule,subRule					
					,vo.getAddr(),vo.getLang(),vo.getCtyCode(),vo.getTimezoneOffset(),vo.getName(),vo.getBirth(),vo.getBlood(),vo.getBloodRh()
					,vo.getGender(),vo.getServiceAccepted() , vo.getPrivateAccepted() , vo.getLocationAccepted() , vo.getMarketingAccepted() , vo.getSmsAccepted()
					, vo.getCorpPosition(),"1", vo.getUnitLength(), vo.getUnitVolume(), vo.getUnitTemperature() , vo.getUnitWeight() , vo.getUnitDate()
					);
			
			//기존 들어온 유저를 만들고
			accountDao.createUser(avo);
			// 기존 계정으로 corp에 vehicleManager와 topManager에 update
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("vehicleManager", avo.getAccountKey());
			param.put("topManager", avo.getAccountKey());
			param.put("corpKey", vo.getCorpKey());

			//null이 아니고 accountid와 ceoid가 다르면 새로 가입시킨다. 
			if (!StrUtil.isNullToEmpty(vo.getCeoId())) {
				if (!vo.getAccountId().equals(vo.getCeoId())) {

					AccountVO topAvo = new AccountVO(vo.getCeoId(), vo.getCeoPw(), vo.getCorpKey()
							, null, null, vo.getCorpPhone(), vo.getCorpPhoneCtyCode(), rule
							, subRule, null, vo.getLang(), null, vo.getTimezoneOffset(), vo.getCeoName(), null
							, null, null, null, vo.getServiceAccepted(), vo.getPrivateAccepted()
							, vo.getLocationAccepted(), vo.getMarketingAccepted(), vo.getSmsAccepted()
							, null, "0", vo.getUnitLength(), vo.getUnitVolume(), vo.getUnitTemperature() , vo.getUnitWeight() , vo.getUnitDate());

					accountDao.createUser(topAvo);
					
					param.put("topManager", topAvo.getAccountKey());
					// 해당키를 corp에 topManager에 업데이트
				}

			}
			
			//최종적으로 corp에 insert
			accountDao.updateCorpManager(param);
			
			
			return true;
		}catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public boolean createPartnerUser(CreatePartnerVO vo, int rule, int subRule) throws BizException {

		AccountVO avo = new AccountVO(vo.getAccountId(),vo.getAccountPw(),vo.getCorpKey()
				,vo.getMobilePhone(),vo.getMobilePhoneCtyCode(),vo.getPhone(),vo.getPhoneCtyCode(),rule,subRule					
				,vo.getAddr(),vo.getLang(),vo.getCtyCode(),vo.getTimezoneOffset(),vo.getName(),vo.getBirth(),vo.getBlood(),vo.getBloodRh()
				,vo.getGender(),vo.getServiceAccepted() , vo.getPrivateAccepted() , vo.getLocationAccepted() , vo.getMarketingAccepted() , vo.getSmsAccepted()
				, vo.getCorpPosition(),"1", vo.getUnitLength(), vo.getUnitVolume(), vo.getUnitTemperature() , vo.getUnitWeight() , vo.getUnitDate()
				);
		
		//기존 들어온 유저를 만들고
		accountDao.createUser(avo);
		// 기존 계정으로 corp에 vehicleManager와 topManager에 update
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("vehicleManager", avo.getAccountKey());
		param.put("topManager", avo.getAccountKey());
		param.put("corpKey", vo.getCorpKey());

		//null이 아니고 accountid와 ceoid가 다르면 새로 가입시킨다. 
		if (!StrUtil.isNullToEmpty(vo.getCeoId())) {
			if (!vo.getAccountId().equals(vo.getCeoId())) {

				AccountVO topAvo = new AccountVO(vo.getCeoId(), vo.getCeoPw(), vo.getCorpKey()
						, null, null, vo.getCorpPhone(), vo.getCorpPhoneCtyCode(), rule
						, subRule, null, vo.getLang(), null, vo.getTimezoneOffset(), vo.getCeoName(), null
						, null, null, null, vo.getServiceAccepted(), vo.getPrivateAccepted()
						, vo.getLocationAccepted(), vo.getMarketingAccepted(), vo.getSmsAccepted()
						, null, "0", vo.getUnitLength(), vo.getUnitVolume(), vo.getUnitTemperature() , vo.getUnitWeight() , vo.getUnitDate());

				accountDao.createUser(topAvo);
				
				param.put("topManager", topAvo.getAccountKey());
				// 해당키를 corp에 topManager에 업데이트
			}

		}
		
		//최종적으로 corp에 insert
		accountDao.updateCorpManager(param);
		
		
		return true;
	}
	@Override
	public boolean createUser(CreateUserVO vo, int rule, int subRule) throws BizException {
		try {
			
			AccountVO avo = new AccountVO(vo.getAccountId(),vo.getAccountPw(),vo.getCorpKey()
					,vo.getMobilePhone(),vo.getMobilePhoneCtyCode(),vo.getPhone(),vo.getPhoneCtyCode(),rule,subRule					
					,"",vo.getLang(),vo.getCtyCode(),vo.getTimezoneOffset(),vo.getName(),vo.getBirth(),vo.getBlood(),vo.getBloodRh()
					,vo.getGender(),vo.getServiceAccepted() , vo.getPrivateAccepted() , vo.getLocationAccepted() , vo.getMarketingAccepted() , vo.getSmsAccepted()
					, vo.getCorpPosition(), "0", vo.getUnitLength(), vo.getUnitVolume(), vo.getUnitTemperature() , vo.getUnitWeight() , vo.getUnitDate()
					);
			
			accountDao.createUser(avo);
			
			return true;
		}catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public boolean createUser(List<CreateUserVO> voList, int rule, int subRule) throws BizException {
		for(int i = 0 ; i < voList.size();i++){
			CreateUserVO vo = voList.get(i);
			
			//insert하기전에 중복체크 해야된다.
			AccountVO av = accountDao.getAccountFromId(vo.getAccountId());
			
			if(av == null){
				AccountVO avo = new AccountVO(vo.getAccountId(),vo.getAccountPw(),vo.getCorpKey()
						,vo.getMobilePhone(),vo.getMobilePhoneCtyCode(),vo.getPhone(),vo.getPhoneCtyCode(),rule,subRule					
						,"",vo.getLang(),vo.getCtyCode(),vo.getTimezoneOffset(),vo.getName(),vo.getBirth(),vo.getBlood(),vo.getBloodRh()
						,vo.getGender(),vo.getServiceAccepted() , vo.getPrivateAccepted() , vo.getLocationAccepted() , vo.getMarketingAccepted() , vo.getSmsAccepted()
						, vo.getCorpPosition(), "0", vo.getUnitLength(), vo.getUnitVolume(), vo.getUnitTemperature() , vo.getUnitWeight() , vo.getUnitDate()
						);
				
				accountDao.createUser(avo);
			}else{
				throw new BizException("userId.duplicate");
			}
		}
		return true;
	}

	@Override
	public boolean createUser(CreatePersonVO vo, int rule, int subRule) throws BizException {
		try {
			
			AccountVO avo = new AccountVO(vo.getAccountId(),vo.getAccountPw(),vo.getCorpKey()
					,vo.getMobilePhone(),vo.getMobilePhoneCtyCode(),vo.getPhone(),vo.getPhoneCtyCode(),rule,subRule					
					,vo.getAddr(),vo.getLang(),vo.getCtyCode(),vo.getTimezoneOffset(),vo.getName(),vo.getBirth(),vo.getBlood(),vo.getBloodRh()
					,vo.getGender(),vo.getServiceAccepted() , vo.getPrivateAccepted() , vo.getLocationAccepted() , vo.getMarketingAccepted() , vo.getSmsAccepted()
					, vo.getCorpPosition(), "1", vo.getUnitLength(), vo.getUnitVolume(), vo.getUnitTemperature() , vo.getUnitWeight() , vo.getUnitDate()
					);
			
			accountDao.createUser(avo);
			
			return true;
		}catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public boolean createUser(CreateInviteVO vo, int rule, int subRule) throws BizException {
		try {
			
			AccountVO avo = new AccountVO(vo.getAccountId(),vo.getAccountPw(),vo.getCorpKey()
					,vo.getMobilePhone(),vo.getMobilePhoneCtyCode(),vo.getPhone(),vo.getPhoneCtyCode(),rule,subRule					
					,vo.getAddr(),vo.getLang(),vo.getCtyCode(),vo.getTimezoneOffset(),vo.getName(),vo.getBirth(),vo.getBlood(),vo.getBloodRh()
					,vo.getGender(),vo.getServiceAccepted() , vo.getPrivateAccepted() , vo.getLocationAccepted() , vo.getMarketingAccepted() , vo.getSmsAccepted()
					, vo.getCorpPosition(), "1", vo.getUnitLength(), vo.getUnitVolume(), vo.getUnitTemperature() , vo.getUnitWeight() , vo.getUnitDate()
					);
			
			accountDao.createUser(avo);
			
			return true;
		}catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public boolean createRentUser(CreateRentVO vo, int rule, int subRule) throws BizException {
try {
			
			AccountVO avo = new AccountVO(vo.getAccountId(),vo.getAccountPw(),vo.getCorpKey()
					,vo.getMobilePhone(),vo.getMobilePhoneCtyCode(),vo.getPhone(),vo.getPhoneCtyCode(),rule,subRule					
					,vo.getAddr(),vo.getLang(),vo.getCtyCode(),vo.getTimezoneOffset(),vo.getName(),vo.getBirth(),vo.getBlood(),vo.getBloodRh()
					,vo.getGender(),vo.getServiceAccepted() , vo.getPrivateAccepted() , vo.getLocationAccepted() , vo.getMarketingAccepted() , vo.getSmsAccepted()
					, vo.getCorpPosition(),"1", vo.getUnitLength(), vo.getUnitVolume(), vo.getUnitTemperature() , vo.getUnitWeight() , vo.getUnitDate()
					);
			
			//기존 들어온 유저를 만들고
			accountDao.createUser(avo);
			// 기존 계정으로 corp에 vehicleManager와 topManager에 update
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("vehicleManager", avo.getAccountKey());
			param.put("topManager", avo.getAccountKey());
			param.put("corpKey", vo.getCorpKey());

			//null이 아니고 accountid와 ceoid가 다르면 새로 가입시킨다. 
			if (!StrUtil.isNullToEmpty(vo.getCeoId())) {
				if (!vo.getAccountId().equals(vo.getCeoId())) {

					AccountVO topAvo = new AccountVO(vo.getCeoId(), vo.getCeoPw(), vo.getCorpKey()
							, null, null, vo.getCorpPhone(), vo.getCorpPhoneCtyCode(), rule
							, subRule, null, vo.getLang(), null, vo.getTimezoneOffset(), vo.getCeoName(), null
							, null, null, null, vo.getServiceAccepted(), vo.getPrivateAccepted()
							, vo.getLocationAccepted(), vo.getMarketingAccepted(), vo.getSmsAccepted()
							, null, "0", vo.getUnitLength(), vo.getUnitVolume(), vo.getUnitTemperature() , vo.getUnitWeight() , vo.getUnitDate());
					
					
					accountDao.createUser(topAvo);
					
					param.put("topManager", topAvo.getAccountKey());
					// 해당키를 corp에 topManager에 업데이트
				}

			}
			
			//최종적으로 corp에 insert
			accountDao.updateCorpManager(param);
			
			
			return true;
		}catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public void accountUseStop(String accountKey) {
		
		List<String> list = Arrays.asList(accountKey.split(","));
		
		for(int i = 0 ; i < list.size() ; i++){
			accountDao.accountUseStop(list.get(i));
		}
		
	}

	@Override
	public int getAllAccountCount(Map<String, Object> param) {
		// TODO Auto-generated method stub
		return accountDao.getAllAccountCount(param);
	}

	@Override
	public void updateAccureData(String driver, Integer distance, Date startDate, Date endDate) throws BizException {
		try {
			
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("accountKey", driver);
			param.put("distance", distance);
			param.put("startDate", startDate);
			param.put("endDate", endDate);
			
			accountDao.updateAccureData(param);
		}catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}
	
	@Override
	public void updateAccureDataReverse(String driver,String tripKey) throws BizException {
		try {
			
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("accountKey", driver);
			param.put("tripKey", tripKey);
			
			accountDao.updateAccureDataReverse(param);
		}catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}
}

