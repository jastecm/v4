package v4.web.rest.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import v4.ex.exc.BizException;
import v4.web.rest.dao.AccidentDAO;
import v4.web.rest.service.AccidentService;
import v4.web.vo.CreateAccidentVO;
import v4.web.vo.UpdateAccidentResultVO;


@Service("accidentService")
public class AccidentServiceImpl implements AccidentService {

	@Autowired
	AccidentDAO accidentDAO;

	@Override
	public void createAccident(CreateAccidentVO createAccidentVO) throws BizException {
		try {
			
			accidentDAO.createAccident(createAccidentVO);
			
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}

	@Override
	public List<Map<String, Object>> getInsureList() throws BizException {
		try {
			
			return accidentDAO.getInsureList();
			
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public void updateAccidentResult(UpdateAccidentResultVO updateAccidentResultVO) throws BizException {
		try {
			accidentDAO.updateAccidentResult(updateAccidentResultVO);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}
	
	
	
}
