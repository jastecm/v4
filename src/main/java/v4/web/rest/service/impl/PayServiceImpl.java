package v4.web.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import v4.web.rest.dao.PayDAO;
import v4.web.rest.service.PayService;



@Service("payService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class PayServiceImpl implements PayService {
	
	@Autowired
	private PayDAO payDao;

	@Override
	public boolean chkPayment(String deviceKey) {
		int i = payDao.chkPayment(deviceKey);
		return i>0;
	}
	
	/*
	@Override
	public String getPaymentUid() {
		return payDao.getPaymentUid();
	}

	@Override
	public void createPayRequest(String uid, String accountKey, String corp, String[] item, String[] itemProduct,String itemNm, int pay) throws BizException {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("paymentId", uid);
		param.put("reqPay", pay);
		param.put("accountKey", accountKey);
		param.put("itemNm", itemNm);
		param.put("itemType", "device");
		param.put("corp", corp);
		param.put("state", -1);
		
		payDao.createPayRequestHeader(param);
		
		for(int i = 0 ; i < item.length ; i++){
			String product = itemProduct[i];
			String deviceSn = item[i];
			
			int itemPay = 0;
			String productSeries = product.split("\\.")[0];
			String productMonth = product.split("\\.")[1];
			if(productSeries.equals("von-S31")) itemPay = 5500*Integer.parseInt(productMonth);
			else itemPay = 0;
			
			param.clear();
			param.put("paymentId", uid);
			param.put("paymentType", "device");
			param.put("paymentValue", deviceSn);
			param.put("paymentValueNote", productMonth);
			param.put("paymentValuePrice", itemPay);
		
			payDao.createPayRequestDevcieDetail(param);
		}
		
		
	}

	@Override
	public void savePaymentResult(EasyPayVO reqVo) throws BizException {
		payDao.savePaymentResult(reqVo);
		
		
		if(reqVo.getRes_cd().equals("0000")){
			
			Map<String,String> header = getPaymentHeader("",reqVo.getOrder_no());
			header.put("state",reqVo.getRes_cd());
			payDao.updatePaymentResult(header);
			
			if(header.get("reqPay").equals(reqVo.getAmount())){
				
				List<Map<String,String>> payment = payDao.getPaymentType(reqVo.getOrder_no());
				
				for(Map<String,String> pay : payment){
					String payType = pay.get("paymentType");
					if(payType.equals("device")){
						String product = pay.get("paymentValueNote");
						String productId = "";
						if(product.equals("1"))
							productId = "lora1monthpayment";
						else if(product.equals("3"))
							productId = "lora3monthspayment";
						else if(product.equals("12"))
							productId = "loraoneyearpayment";
						
						pay.put("productId", productId);
						payDao.updateExpire(pay);
					}else if(payType.equals("geoFence")){
						payDao.updateGeofence(pay);
					}
				}
			}else{
				//요청금액과 결제금액이 다른 부정 케이스
			}
			
		}else{
			//실패케이스
			//결제 지울까?
		}
	}

	@Override
	public List<Map<String, Object>> getPaymentHeaderList(String accountKey) {
		return payDao.getPaymentHeaderList(accountKey);
	}

	@Override
	public String getPaymentHeaderLastDate(String accountKey, String productType) {
		Map<String, String> param = new HashMap<String, String>();
		param.put("accountKey",accountKey);
		param.put("productType", productType);
		return  payDao.getPaymentHeaderLastDate(param);
	}

	@Override
	public void createPayRequest(String uid, String accountKey, String corp, int cnt, String productType, String itemNm,
			int pay) throws BizException {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("paymentId", uid);
		param.put("reqPay", pay);
		param.put("accountKey", accountKey);
		param.put("corp", corp);
		param.put("itemType", productType);
		param.put("itemNm", itemNm);
		param.put("state", -1);
		
		payDao.createPayRequestHeader(param);
		
		if(productType.equals("geoFence")){
			
			param.clear();
			param.put("paymentId", uid);
			param.put("paymentType", productType);
			param.put("paymentValue", corp);
			param.put("paymentValueNote", cnt);
			param.put("paymentValuePrice", pay);
			
			payDao.createPayRequestDevcieDetail(param);
		}
	}

	@Override
	public Map<String, String> getPaymentHeader(String accountKey, String paymentKey) {
		return payDao.getpaymentHeader(paymentKey);
	}

	@Override
	public List<Map<String, String>> getPaymentDetail(String accountKey, String paymentKey) {
		return payDao.getPaymentTypeDevice(paymentKey);
	}
*/
}

