package v4.web.rest.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import v4.ex.exc.BizException;
import v4.web.rest.dao.GroupDAO;
import v4.web.rest.service.GroupService;
import v4.web.vo.AccountVO;
import v4.web.vo.CreateGroupVO;
import v4.web.vo.GroupVO;
import v4.web.vo.UpdateGroupVO;

@Service("groupService")
public class GroupServiceImpl implements GroupService {
	
	@Autowired
	GroupDAO groupDao;

	@Override
	public String addGroup(CreateGroupVO reqVo, String corpKey) throws BizException {
		//lev0을 셀렉트 해서 없으면 생성해준다.
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("corpKey", corpKey);
		param.put("groupNm", reqVo.getGroupNm());
		
		param.put("managerAccountKey", reqVo.getManagerAccountKey());
		param.put("parentGroupKey", reqVo.getParentGroupKey());
		
		//여기서 한가지 체크를 해줘야 한다 parentGroupKey를 가지고 있으면 2댑스가
		//parent키가 2댑스면 안된다.
		
		groupDao.addGroup(param);
		String groupKey = (String) param.get("groupKey");
		
		return groupKey;
	}

	@Override
	public List<GroupVO> getGroupList(Map<String, Object> param) {
		return groupDao.getGroupList(param);
	}

	@Override
	public List<GroupVO> getGroupListDesending(Map<String, Object> param) {
		return groupDao.getGroupListDesending(param);
	}

	@Override
	public GroupVO getGroup(Map<String, Object> param) {
		return groupDao.getGroup(param);
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public void deleteGroup(Map<String, Object> param) {
		//삭제할려는 키를 가져와야 한다 본인 포함 하위키
		List<GroupVO> delGroupKeyList = groupDao.getGroupListDesending(param);
		for(int i = 0 ; i < delGroupKeyList.size(); i++){
			GroupVO vo = delGroupKeyList.get(i);
			System.out.println(vo.getGroupKey());
			
			//하위 그룹까지 다 삭제한다.
			groupDao.delGroup(vo);
			//해당 유저들 그룹키를 null로 변경 account에 groupKey
			groupDao.updateAccountGroupKeyToNull(vo);
			//차량도 삭제 vehicle_group table 삭제
			//groupDao.delVehicleGroup(vo);
			
		}
	}

	@Override
	public void sortGroup(AccountVO account) {
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("corpKey", account.getCorpKey());
		//해당 corpKey temp 삭제
		groupDao.delGroupTemp(param);
		
		//임시로 최상위 temp를 넣는다.
		groupDao.initGroupTemp(param);
		
		//temp 삭제하고 temp에 전부다 insert
		int lev = 1;
		while(lev < 10){
			param.put("lev", lev);
			lev++;
			groupDao.insertGroupTemp(param);
		}
		
		groupDao.updateTemp();
		
	}

	@Override
	public void updateGroup(UpdateGroupVO groupVo, Map<String, Object> param) {
		
		//업데이트 해야된다 업데치트 항목, 이름,
		param.put("groupNm", groupVo.getGroupNm());
		param.put("parentGroupKey", groupVo.getParentGroupKey());
		param.put("managerAccountKey", groupVo.getManagerAccountKey());
		
		groupDao.updateCorpGroup(param);
		
	}

	@Override
	public List<GroupVO> getParentGroupTree(Map<String, Object> param) {

		return groupDao.getParentGroupTree(param);
	}

	@Override
	public int getGroupListCnt(Map<String, Object> param, Object object) {
		return groupDao.getGroupListCnt(param);
	}

	@Override
	public void groupAddUser(Map<String, Object> param) {
		groupDao.groupAddUser(param);
		
	}

	@Override
	public void groupDeleteUser(Map<String, Object> param) {
		groupDao.groupDeleteUser(param);
		
	}

	@Override
	public void groupAddVehicle(Map<String, Object> param) {
		groupDao.groupAddVehicle(param);
	}

	@Override
	public void groupDeleteVehicle(Map<String, Object> param) {
		groupDao.groupDeleteVehicle(param);
	}

}
