package v4.web.rest.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import v4.ex.exc.BizException;
import v4.web.rest.dao.TokenDAO;
import v4.web.rest.service.TokenService;
import v4.web.vo.TokenVO;


@Service("tokenService")
public class TokenServiceImpl implements TokenService {

	@Autowired
	TokenDAO tokenDao;

	@Override
	public void clearUserToken(String userKey) throws BizException{
		try {
			tokenDao.clearUserToken(userKey);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
		
	}
	@Override
	public void semiClearUserToken(String userKey) throws BizException {
		try {
			tokenDao.semiClearUserToken(userKey);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
		
	}

	@Override
	public void addUserToken(TokenVO t) throws BizException {
		try {
			tokenDao.addUserToken(t);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
		
	}

	@Override
	public List<Long> getUserToken(String userKey) throws BizException {
		try {
			return tokenDao.getUserToken(userKey);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
		
	}

	
	
	
}

