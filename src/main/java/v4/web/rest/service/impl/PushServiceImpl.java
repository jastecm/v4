package v4.web.rest.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.common.Globals;
import org.jastecm.string.StrUtil;

import v4.ex.exc.BizException;
import v4.web.rest.dao.PushDAO;
import v4.web.rest.service.AccountService;
import v4.web.rest.service.PushService;
import v4.web.vo.AccountVO;
import v4.web.vo.PushControllVO;


@Service("pushService")
public class PushServiceImpl implements PushService {
	
	@Autowired
	AccountService accountService;
	
	@Autowired
	PushDAO pushDao;
	
	
	@Value("AAAAoqqBUIs:APA91bGw5qx4h2WwkzfziY_EjKZa2FMGvPIHgIJQgV7MninxALQcFb1iYCGtQrWjUKn92N3o3l1YJlB0TLTHmL_WLJOjKP7h_Z_8WhaIHR9kO1AQxFSRNUKC297RLq8TeOXv-hwGQ7Fl")	        
	private String serverKey;

	@Override
	public void pushKeyRefresh(String accountKey, String key) throws BizException {
		try {
					
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("accountKey", accountKey);
			param.put("key", key);
			
			pushDao.pushKeyRefresh(param);
			
		}catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}

	@Override
	public void pushSubscribe(String accountKey, String topic, String accept) throws BizException {
		
		try {
			
			AccountVO account =accountService.getAccount(accountKey);;
			
			try {
				String url = "";
				Map<String, Object> body = new HashMap<String, Object>();
				if(accept.equals("1")){
					body.put("to", "/topics/"+topic);
					body.put("registration_tokens", new String[]{account.getFcmKey()});
					url = "https://iid.googleapis.com/iid/v1:batchAdd";
				}else{
					body.put("to", "/topics/"+topic);
					body.put("registration_tokens", new String[]{account.getFcmKey()});
					url = "https://iid.googleapis.com/iid/v1:batchRemove";
					
				}
				Map<String,String> rtv = call(url,body,"POST");
				
				System.out.println(rtv.toString());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
		
	}
	
	
	
	

	@Override
	public void push(String accountKey, String topic, String title,String msg) throws BizException {
		
		try {
			
			Map<String, String> message = new HashMap<String, String>();
			message.put("title", title);
			message.put("body", msg);
			message.put("icon", "https://vdaspro.viewcar.co.kr/vdasPro/common/images/gnb_logo.png");
			message.put("sound", "default");
			message.put("priority", "high");
			
			Map<String, String> aps = new HashMap<String, String>();
			aps.put("sound", "defualt");
			
			try {
				String url = "";
				Map<String, Object> body = new HashMap<String, Object>();
				if(!StrUtil.isNullToEmpty(topic)){
					body.put("to","/topics/"+topic);
					body.put("aps", aps);
					body.put("priority", "high");
					body.put("notification", message);
					body.put("data", message);
					body.put("sound", "default");
					
					url = "https://fcm.googleapis.com/fcm/send";
					
				}else{
					AccountVO account = accountService.getAccount(accountKey);
					
					body.put("to", account.getFcmKey());
					body.put("aps", aps);
					body.put("priority", "high");
					body.put("notification", message);
					body.put("data", message);
					body.put("sound", "default");
					
					url = "https://fcm.googleapis.com/fcm/send";
					
				}
				
				Map<String,String> rtv = call(url,body,"POST");
				
				System.out.println(rtv.toString());
				
				long mailExpired = 1000*60*60*24*30L;			                   
				
				/*
				if(!StrUtil.isNullToEmpty(topic)){
					//all user mailBox insert
					restMailBoxService.allUserMailBoxInsert(title,msg,mailExpired);
				}else{
					//accountKey mailBox Insert
					restMailBoxService.simgleUserMailBoxInsert(accountKey,title,msg,mailExpired);
				}
				*/
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
		
	}
	
	@Override
	public void pushDenyUpdate(String userKey, String denyType, String accept) throws BizException {
		
		try {
			
			List<String> denyList = pushDao.getDenyList(userKey);
			
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("accountKey", userKey);
			param.put("denyType", denyType);
			
			if(denyList.contains(denyType) && accept.equals("1"))
				pushDao.removeDeny(param);
			else if(!denyList.contains(denyType) && accept.equals("0"))
				pushDao.addDeny(param);
			
		}catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
		
	}
	
	@Override
	public Map<String,Object> getDenyList(String userKey) throws BizException {
		
		try {
			
			Map<String, Object> rtv = new HashMap<String, Object>();
			
			List<String> denyList = pushDao.getDenyList(userKey);
					
			rtv.put("denyList", denyList);
			
			AccountVO ruvo = accountService.getAccount(userKey);	
			String url = "https://iid.googleapis.com/iid/info/"+ruvo.getFcmKey()+"?details=true";
			
			Map<String, String> param;
			try {
				param = call(url,null,"GET");
				rtv.put("fcmTopicInfo", param.get("resMsg"));
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			
			return rtv;
			
		}catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}

		
		
		
	}
	
	@Override
	public List<Map<String, String>> getPushList(Map<String, Object> param) throws BizException {
		try {
				
			return pushDao.getPushList(param);
			
		}catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}
	
	@Override
	public void delPushList(String pushKey) throws BizException{
		
		try {
			
			pushDao.delPushList(pushKey);
			
		}catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public void pushChkAccept(String pushType , String accountKey, String topic, String title,String msg) throws BizException {
		try {
			
			if(StrUtil.isNullToEmpty(accountKey)) return;
			
			AccountVO uvo = accountService.getAccount(accountKey);
			
			if(StrUtil.isNullToEmpty(uvo.getFcmKey())) return;
			
			String pushMonitor = StrUtil.isNullToEmpty(uvo.getPushMonitor(), "0");
			
			if(pushMonitor.equals("1")){
				PushControllVO pushVo = getPushControll(accountKey,pushType);
				
				if(pushVo == null){
					pushVo = new PushControllVO();
					pushVo.setAccountKey(accountKey);
					pushVo.setPushType(pushType);
					pushVo.setState("1");
					updatePushControllState(pushVo);
				}
				
				String state = pushVo.getState();
				if(state.equals("1")){
					Map<String, String> message = new HashMap<String, String>();
					message.put("title", title);
					message.put("body", msg);
					message.put("icon", Globals.VDAS_URL_PREFIX+"/common/images/gnb_logo.png");
					message.put("priority", "high");
					
					Map<String, String> aps = new HashMap<String, String>();
					aps.put("sound", "default");
					
					try {
						String url = "";
						Map<String, Object> body = new HashMap<String, Object>();
						if(!StrUtil.isNullToEmpty(topic)){
							body.put("to","/topics/"+topic);
							body.put("notification", message);
							body.put("data", message);
							body.put("priority", "high");
							body.put("sound", "default");
							body.put("aps", aps);
							
							url = "https://fcm.googleapis.com/fcm/send";
							
						}else{
							body.put("to", uvo.getFcmKey());
							body.put("notification", message);
							body.put("data", message);
							body.put("priority", "high");
							body.put("sound", "default");
							body.put("aps", aps);
							
							url = "https://fcm.googleapis.com/fcm/send";
							
						}
						
						Map<String,String> rtv = call(url,body,"POST");
						
						System.out.println(rtv.toString());
						
						/*if(hstrFlag) //corp master push //추적해서 vehicle의 배차가 있을시 배차 사용자에게 메세지박스
							restPushControllService.insertPushHstr(corpKey,pushType,title,msg);
						
						if(!StrUtil.isNullToEmpty(mailAccountKey)){
							long mailExpired = 1000*60*60*24*7L;
							restMailBoxService.simgleUserMailBoxInsert(mailAccountKey, title, msg, mailExpired);
						}*/
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}

	@Override
	public void updatePushControllState(PushControllVO vo) throws BizException {
		try {
			
			pushDao.updatePushControllState(vo);
			
		}catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}

	@Override
	public List<PushControllVO> getPushControllList(String accountKey) throws BizException {
		try {
			
			return pushDao.getPushControllList(accountKey);
			
		}catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public PushControllVO getPushControll(String accountKey,String pushType) throws BizException {
		try {
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("accountKey",accountKey);
			param.put("pushType",pushType);
			return pushDao.getPushControll(param);
			
		}catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public void updatePushControllLastest(String accountKey,String pushType,String val) throws BizException {
		try {
			PushControllVO vo = new PushControllVO();
			vo.setAccountKey(accountKey);
			vo.setPushType(pushType);
			vo.setLastestKey(val);
			
			pushDao.updatePushControllLastest(vo);
			
		}catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public void insertPushHstr(String corpKey , String pushType , String title , String msg) throws BizException{
		try {
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("corpKey", corpKey);
			param.put("pushType", pushType);
			param.put("title", title);
			param.put("msg", msg);
			pushDao.insertPushHstr(param);
			
		}catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public void updatePushControllAll(String userKey, String state) throws BizException{
		try {
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("accountKey", userKey);
			param.put("state", state);
			pushDao.updatePushControllAll(param);
			
		}catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}
	
	private Map<String,String> call(String url,Object body,String method) throws IOException{
		
		ObjectMapper mapper = new ObjectMapper();
		
		
		Map<String,String> rtvObj = new HashMap<String,String>();
		rtvObj.put("resCode",null);
		rtvObj.put("resMsg",null);
		
		URL obj = new URL(url);
		
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setDoInput(true);
		con.setDoOutput(true);
		
		// optional default is GET
		con.setRequestMethod(method);
		
		
		//add request header			
		con.setRequestProperty("Content-Type","application/json;charset=UTF-8");  
		con.setRequestProperty("Accept-Charset", "UTF-8");
		
		con.setRequestProperty("Authorization", "key="+serverKey);
		
		
		if(body != null){
			String bodyStr = "";
			if(!String.class.isInstance(body))
				bodyStr = mapper.writeValueAsString(body);
			else bodyStr = (String) body;
			
			byte[] outputInBytes = bodyStr.getBytes("UTF-8");
			OutputStream os = con.getOutputStream();
			os.write( outputInBytes );    
			os.close();
			
			System.out.println("body contents : " + bodyStr);
		}
		

		
		System.out.println("Sending "+method+" request to URL : " + url);
		
		int responseCode = con.getResponseCode();
		
		System.out.println("Response Code : " + responseCode);
		rtvObj.put("resCode",String.valueOf(responseCode));
		
		if(responseCode < 300){
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream(),"UTF-8"));
			String inputLine;
			StringBuffer response = new StringBuffer();
	
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			System.out.println(response.toString());
			rtvObj.put("resMsg",response.toString());
		}else{
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getErrorStream(),"UTF-8"));
			String inputLine;
			StringBuffer response = new StringBuffer();
	
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			System.out.println(response.toString());
			rtvObj.put("resMsg",response.toString());
		}
		
		return rtvObj;
	}
	
	
}

