package v4.web.rest.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.util.Util;

import v4.ex.exc.BizException;
import v4.web.rest.dao.WorkingAreaTimeDAO;
import v4.web.rest.service.WorkingAreaTimeService;
import v4.web.vo.AccountVO;
import v4.web.vo.WorkingTimeAreaVO;

@Service("workingAreaTimeService")
public class WorkingAreaTimeServiceImpl implements WorkingAreaTimeService {
	
	@Autowired
	private WorkingAreaTimeDAO wokringAreaTimeDao;
/*
	@Override
	public void addWorkingArea(String corp, String corpGroup, String accountKey, String cityLev1, String cityLev2)
			throws BizException {
		
		String[] cityLev1Arr = cityLev1.split(",");
		String[] cityLev2Arr = cityLev2.split(",",cityLev1Arr.length);
		
		if(cityLev1Arr.length != cityLev2Arr.length)
			throw new BizException("addWorkingArea.badRequest");
		
		for(String city1 : cityLev1Arr){
			if(StrUtil.isNullToEmpty(city1))
				throw new BizException("addWorkingArea.badRequest");
		}
			
		for(int i = 0 ; i < cityLev1Arr.length ; i++){
			String city1 = cityLev1Arr[i];
			String city2 = cityLev2Arr[i];
			
			WorkingTimeAreaVO vo = new WorkingTimeAreaVO(corp,corpGroup,accountKey,city1,city2);
			
			wokringAreaTimeDao.addWorkingArea(vo);
			
		}
		
		
		
		
	}

	@Override
	public List<WorkingTimeAreaVO> getWorkingArea(UserVO user, Map<String, String> param) throws BizException {
		param.put("corp", user.getCorp());
		param.put("lang", user.getLang());
		
		return wokringAreaTimeDao.getWorkingArea(param);
		
	}

	@Override
	public void deleteWorkingArea(UserVO user, String workAreaKey) throws BizException {
		
		String[] workAreaKeyArr = workAreaKey.split(",");
		
		for(String key : workAreaKeyArr){
			wokringAreaTimeDao.deleteWorkingArea(key);
		}
	}

	@Override
	public int getWorkingAreaTotCnt(UserVO user) throws BizException {
		
		return wokringAreaTimeDao.getWorkingAreaTotCnt(user.getCorp());
	}

	@Override
	public void updateWorkingArea(String corp ,String workAreaKey, String corpGroup, String accountKey, String cityLev1,
			String cityLev2) throws BizException {
		WorkingTimeAreaVO vo = new WorkingTimeAreaVO(corp ,workAreaKey,corpGroup,accountKey,cityLev1,cityLev2);
		
		wokringAreaTimeDao.updateWorkingArea(vo);
		
	}

	@Override
	public void addWorkingTime(String corp, String corpGroup, String accountKey, String startH, String startM,
			String endH, String endM, String day) throws BizException {
		String[] dayArr = day.split(",",-1);
		boolean validChk = false;

		if(dayArr.length != 7)
			throw new BizException();
		
		for(String days : dayArr){
			if(!StrUtil.isNullToEmpty(days)){
				validChk = true;
				break;
			}
		}
		if(!validChk)
			throw new BizException();
		
		WorkingTimeAreaVO vo = new WorkingTimeAreaVO(corp,corpGroup,accountKey,startH,startM,endH,endM,dayArr);
		
		wokringAreaTimeDao.addWorkingTime(vo);
	}

	@Override
	public List<WorkingTimeAreaVO> getWorkingTime(UserVO user, Map<String, String> param) throws BizException {
		param.put("corp", user.getCorp());
		
		return wokringAreaTimeDao.getWorkingTime(param);
	}

	@Override
	public int getWorkingTimeTotCnt(UserVO user) throws BizException {

		return wokringAreaTimeDao.getWorkingTimeTotCnt(user.getCorp());
	}

	@Override
	public void deleteWorkingTime(UserVO user, String workTimeKey) throws BizException {
		String[] workTimeKeyArr = workTimeKey.split(",");
		
		for(String key : workTimeKeyArr){
			wokringAreaTimeDao.deleteWorkingTime(key);
		}
	}

	@Override
	public void updateWorkingTime(String corp, String workTimeKey, String corpGroup, String accountKey,
			String startH, String startM, String endH, String endM, String day)
					throws BizException {
		String[] dayArr = day.split(",",-1);	// 0~6 : Mon~Sun
		boolean validChk = false;

		if(dayArr.length != 7)
			throw new BizException();
		
		for(String days : dayArr){
			if(!StrUtil.isNullToEmpty(days)){
				validChk = true;
				break;
			}
		}
		if(!validChk)
			throw new BizException();
		
		WorkingTimeAreaVO vo = new WorkingTimeAreaVO(corp,workTimeKey,corpGroup,accountKey,startH,startM,endH,endM,dayArr);
		
		wokringAreaTimeDao.updateWorkingTime(vo);
	}

	@Override
	public List<WorkingAreaDrivingVO> workingAreaDrivingResult(UserVO user, Map<String, String> param)
			throws BizException {
		param.put("lang",user.getLang());
		param.put("rule",user.getRule());
		param.put("ruleAccountKey",user.getAccountKey());
		
		
		return wokringAreaTimeDao.workingAreaDrivingResult(param);
	}

	
	
	public static void main(String args[]){
		String[] p = {"11","41135","41131","41133"};
		String s = "41135";
		String e = "11215";
		
		boolean chkStart = false;
		boolean chkEnd = false;
		
		for(String acceptArea : p){
			if(chkStart && chkEnd) break;
			if(acceptArea.length() == 2){
				if(!chkStart) chkStart = acceptArea.equals( s.substring(0, 2) );
				if(!chkEnd) chkEnd = acceptArea.equals( e.substring(0, 2) );
				
			}else if(acceptArea.length() == 5){
				if(!chkStart) chkStart = acceptArea.equals( s );
				if(!chkEnd) chkEnd = acceptArea.equals( e );
			}
			
		}
		
	}

	

	
*/
	
	/**
	 * 업무 시간 삭제
	 * @param user
	 * @param workTimeKey
	 * @throws BizException
	 */
	@Override
	public void deleteWorkingTime(AccountVO user, String workTimeKey) throws BizException {
		String[] workTimeKeyArr = workTimeKey.split(",");
		
		for(String key : workTimeKeyArr){
			wokringAreaTimeDao.deleteWorkingTime(key);
		}
	}
	
	/**
	 * 업무 시간 리스트
	 * @param user
	 * @param param
	 * @return
	 * @throws BizException
	 */
	@Override
	public List<WorkingTimeAreaVO> getWorkingTime(Map<String, Object> param) throws BizException {
		
		return wokringAreaTimeDao.getWorkingTime(param);
	}
	
	/**
	 * 업무시간 수정
	 */
	@Override
	public void updateWorkingTime(String corpKey, String workTimeKey, String groupKey, String accountKey,
			String startH, String startM, String endH, String endM, String day)
					throws BizException {
		String[] dayArr = day.split(",",-1);	// 0~6 : Mon~Sun
		boolean validChk = false;

		if(dayArr.length != 7)
			throw new BizException();
		
		for(String days : dayArr){
			if(!StrUtil.isNullToEmpty(days)){
				validChk = true;
				break;
			}
		}
		if(!validChk)
			throw new BizException();
		
		WorkingTimeAreaVO vo = new WorkingTimeAreaVO(corpKey,workTimeKey,groupKey,accountKey,startH,startM,endH,endM,dayArr);
		
		wokringAreaTimeDao.updateWorkingTime(vo);
	}
	
	/**
	 * 업무시간 등록
	 */
	@Override
	public void addWorkingTime(String corpkey, String groupKey, String accountKey, String startH, String startM,
			String endH, String endM, String day) throws BizException {
		String[] dayArr = day.split(",",-1);
		boolean validChk = false;

		if(dayArr.length != 7)
			throw new BizException();
		
		for(String days : dayArr){
			if(!StrUtil.isNullToEmpty(days)){
				validChk = true;
				break;
			}
		}
		if(!validChk)
			throw new BizException();
		
		WorkingTimeAreaVO vo = new WorkingTimeAreaVO(corpkey,groupKey,accountKey,startH,startM,endH,endM,dayArr);
		
		wokringAreaTimeDao.addWorkingTime(vo);
	}
	
	/**
	 * 이수지역 수정
	 */
	@Override
	public void updateWorkingArea(String corpKey ,String workAreaKey, String groupKey, String accountKey, String cityLev1,
			String cityLev2) throws BizException {
		
		WorkingTimeAreaVO vo = new WorkingTimeAreaVO(corpKey ,workAreaKey,groupKey,accountKey,cityLev1,cityLev2);
		
		wokringAreaTimeDao.updateWorkingArea(vo);
		
	}
	
	/**
	 * 이수지역 삭제
	 * @param user
	 * @param workAreaKey
	 * @throws BizException
	 */
	@Override
	public void deleteWorkingArea(AccountVO user, String workAreaKey) throws BizException {
		
		String[] workAreaKeyArr = workAreaKey.split(",");
		
		for(String key : workAreaKeyArr){
			wokringAreaTimeDao.deleteWorkingArea(key);
		}
	}
	
	
	/**
	 * 이수지역을 등록한다.
	 */
	@Override
	public void addWorkingArea(String corpKey, String groupKey, String accountKey, String cityLev1, String cityLev2)
			throws BizException {
		
		String[] cityLev1Arr = cityLev1.split(",");
		String[] cityLev2Arr = cityLev2.split(",",cityLev1Arr.length);
		
		if(cityLev1Arr.length != cityLev2Arr.length)
			throw new BizException("addWorkingArea.badRequest");
		
		for(String city1 : cityLev1Arr){
			if(StrUtil.isNullToEmpty(city1))
				throw new BizException("addWorkingArea.badRequest");
		}
			
		for(int i = 0 ; i < cityLev1Arr.length ; i++){
			String city1 = cityLev1Arr[i];
			String city2 = cityLev2Arr[i];
			
			WorkingTimeAreaVO vo = new WorkingTimeAreaVO(corpKey,groupKey,accountKey,city1,city2);
			
			wokringAreaTimeDao.addWorkingArea(vo);
			
		}
	}
	
	/**
	 * 이수지역 리스트를 가져 온다.
	 */
	@Override
	public List<WorkingTimeAreaVO> getWorkingArea(AccountVO user, Map<String, Object> param) throws BizException {
		
		Util.setCurrentInfoToMap(user,param);
		
		return wokringAreaTimeDao.getWorkingArea(param);
		
	}
	
	@Override
	public String getCityByPoint(String point) throws BizException {
		try {
			return wokringAreaTimeDao.getCityByPoint(point);
		}catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}

	@Override
	public String chkWorkingTime(String accountKey, long startDate, long endDate) throws BizException {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("accountKey", accountKey);
		param.put("startDate", startDate);
		param.put("rtv", "");
		wokringAreaTimeDao.getAccountWorkingTime(param);
		if(((String)param.get("rtv")).equals("0")) return null;
		else return (String)param.get("rtv"); 
	}
	
	@Override
	public String chkWorkingArea(String accountKey, double startLon, double startLat, double endLon, double endLat)
			throws BizException {
		
		if(StrUtil.isNullToEmpty(accountKey)) return null;
		
		String areaCd = getAccountWorkingArea(accountKey);
		if(StrUtil.isNullToEmpty(areaCd)) return null;
		else{
			String startAreaPoint = startLon+" "+startLat;
			String startArea = getCityByPoint(startAreaPoint);
			String endAreaPoint = endLon+" "+endLat;
			String endArea = getCityByPoint(endAreaPoint);
			
			if(StrUtil.isNullToEmpty(startArea) || StrUtil.isNullToEmpty(endArea) ) return null;
			
			String[] areaCds = areaCd.split(",");
						
			boolean chkStart = false;
			boolean chkEnd = false;
			
			for(String acceptArea : areaCds){
				if(chkStart && chkEnd) break;
				
				if(acceptArea.length() == 2){
					chkStart = acceptArea.equals( startArea.substring(0, 2) );
					chkEnd = acceptArea.equals( endArea.substring(0, 2) );
					
				}else if(acceptArea.length() == 5){
					chkStart = acceptArea.equals( startArea );
					chkEnd = acceptArea.equals( endArea );
				}
				
			}
			
			if(chkStart && chkEnd) return null;
			else return  "1";
			
		}
		
	}
	
	@Override
	public String getAccountWorkingArea(String accountKey) throws BizException {
		return wokringAreaTimeDao.getAccountWorkingArea(accountKey);
	}

	@Override
	public int getWorkingTimeCnt(Map<String, Object> param, Object object) {
		// TODO Auto-generated method stub
		return wokringAreaTimeDao.getWorkingTimeCnt(param);
	}

	@Override
	public int getWorkingTimeTotalCnt(Map<String, Object> param) {
		
		return wokringAreaTimeDao.getWorkingTimeTotalCnt(param);
	}

	
}

