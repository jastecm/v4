package v4.web.rest.service.impl;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import v4.ex.exc.BizException;
import v4.web.rest.dao.AttachDAO;
import v4.web.rest.service.AttachService;
import v4.web.rest.service.MailBoxService;
import v4.web.rest.service.PushService;
import v4.web.rest.service.PushType;
import v4.web.rest.service.TimeLineService;
import v4.web.vo.AttachVO;
import v4.web.vo.data.DeviceVO;
import v4.web.vo.data.VehicleVO;


@Service("attachService")
public class AttachServiceImpl implements AttachService {
	
	@Autowired
	AttachDAO attachDao;
	
	@Autowired
	TimeLineService timeLineService;
	
	@Autowired
	PushService pushService;
	
	@Autowired
	MailBoxService mailBoxService;
	/*
	
	
	@Autowired
	RestDeviceService restDeviceService;
	@Autowired
	RestVehicleService restVehicleService;
	
	
	
	@Autowired
	VehicleService vehicleService;
	@Autowired
	RestAllocateService restAllocateService;
*/
	@Override
	public void saveAttachInfo(AttachVO vo,VehicleVO vehicle,DeviceVO device) throws BizException {
		
		Integer deviceShiftDay = device.getShiftDay();
		if(deviceShiftDay == null) deviceShiftDay = 0;
		
		long dTime = vo.getDettachTime();
		long aTime = vo.getAttachTime();
		long hour = 60*60*1000;
		
		dTime -= deviceShiftDay* (24*hour);
		aTime -= deviceShiftDay* (24*hour);
		
		vo.setDettachTime(dTime);
		vo.setAttachTime(aTime);
		/*day shift issue end*/
		
		Map<String, Object> paramEvent = new HashMap<String, Object>();
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("deviceKey", device.getDeviceKey());
		param.put("vehicleKey", vehicle.getVehicleKey());
		param.put("curTime", vo.getDettachTime());
		param.put("type", vo.getDettachType());
		
		String typeNm = "";
		if(vo.getDettachType() == 1) typeNm = "OBD";
		else if(vo.getDettachType() == 0) typeNm = "USB";
		else if(vo.getDettachType() == 2) typeNm = "NONE";
		
		param.put("typeNm", typeNm);
		
		attachDao.saveAttachInfo(param);
		
		
		paramEvent.put("vehicleKey", vehicle.getVehicleKey());
		paramEvent.put("eventTime", vo.getDettachTime());
		paramEvent.put("eventValue", String.valueOf(vo.getDettachType()));
		paramEvent.put("eventName", typeNm);
		
		timeLineService.insertTimeLineEvent("attach","dettach", paramEvent);
		
		
		String pushAccount = "";
		
		if(vehicle.getCorp().getCorpType().equals("1")){
			pushAccount = vehicle.getDefaultAccountKey();
		}else{
			pushAccount = vehicle.getManagerKey();
		}
		
		
		String pushMsg = "%s 차량의 단말기가 %s 탈착되었습니다.";
		String pushType = PushType.PUSHTYPE_DETTACH;
		String title = "단말 탈부착";
		pushMsg = String.format(pushMsg, vehicle.getPlateNum(),new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(vo.getDettachTime()));
		pushService.pushChkAccept(pushType, pushAccount, "", title, pushMsg);
		pushService.insertPushHstr(vehicle.getCorp().getCorpKey(), pushType, title, pushMsg);
		long mailExpired = 1000*60*60*24*7L;						
		mailBoxService.simgleUserMailBoxInsert(pushAccount, title, pushMsg, mailExpired);
		
		
		param.put("curTime", vo.getAttachTime());
		param.put("type", vo.getAttachType());
		typeNm = "";
		if(vo.getAttachType() == 1) typeNm = "OBD";
		else if(vo.getAttachType() == 0) typeNm = "USB";
		else if(vo.getAttachType() == 2) typeNm = "NONE";
		attachDao.saveAttachInfo(param);
		
		
		paramEvent.put("vehicleKey", vehicle.getVehicleKey());
		paramEvent.put("eventTime", vo.getAttachTime());
		paramEvent.put("eventValue", String.valueOf(vo.getAttachType()));
		paramEvent.put("eventName", typeNm);
		
		timeLineService.insertTimeLineEvent("attach","attach", paramEvent);
		
		
		pushMsg = "%s 차량의 단말기가 %s 부착되었습니다.";
		pushMsg = String.format(pushMsg, vehicle.getPlateNum(),new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(vo.getAttachTime()));
		pushService.pushChkAccept(pushType, pushAccount, "", title, pushMsg);
		pushService.insertPushHstr(vehicle.getCorp().getCorpKey(), pushType, title, pushMsg);
		mailExpired = 1000*60*60*24*7L;						
		mailBoxService.simgleUserMailBoxInsert(pushAccount, title, pushMsg, mailExpired);
		
	}
	
	
	
}
