package v4.web.rest.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import v4.ex.exc.BizException;
import v4.web.rest.dao.VehicleDAO;
import v4.web.rest.service.DeviceService;
import v4.web.rest.service.VehicleDBService;
import v4.web.rest.service.VehicleService;
import v4.web.rest.service.data.DeviceDataService;
import v4.web.rest.service.data.VehicleDataService;
import v4.web.vo.AllocateVO;
import v4.web.vo.CreateBusVO;
import v4.web.vo.CreateEtcVO;
import v4.web.vo.CreateTruckVO;
import v4.web.vo.CreateVehicleVO;
import v4.web.vo.VehicleVO;
import v4.web.vo.data.DeviceVO;
import v4.web.vo.data.TripVO;



@Service("vehicleService")
public class VehicleServiceImpl implements VehicleService {

	@Autowired
	VehicleDAO vehicleDao;
	
	@Autowired
	VehicleDataService vehicleDataService;
	
	@Autowired
	DeviceService deviceService;
	
	@Autowired
	DeviceDataService deviceDataService;
	
	@Autowired
	VehicleDBService vehicleDBService;
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public Map<String,Object> createVehicle(v4.web.vo.data.AccountVO account,CreateVehicleVO vo) throws BizException {
		
		try {
			
			Map<String,Object> rtv = new HashMap<String,Object>();
			
			//차량 신규등록
			if(StrUtil.isNullToEmpty(vo.getVehicleKey())){
				//차량번호 중복체크
				int chk = this.duplicatePlateNumChk(vo.getPlateNum());
				if(chk > 0){
					rtv.put("code",HttpStatus.CONFLICT);
					rtv.put("msg",vo.getPlateNum()+" - 이미 사용중인 차량번호 입니다.");
					return rtv;
				}
				
				if(vehicleDBService.getVehicleDbInfo("ko", vo.getConvCode())==null){
					rtv.put("code",HttpStatus.NOT_FOUND);
					rtv.put("msg","convCode - "+vo.getConvCode()+" - 잘못된 convCode입니다.");
					return rtv;
				}
				
				//active되어있으면 사용불가
				Map<String,Object> param = new HashMap<String,Object>();
				param.put("searchDeviceId", vo.getDeviceId());
				param.put("searchActiveState", "1");
				DeviceVO dvo = deviceDataService.getDevice(param,null);
				
				if(dvo != null){
					rtv.put("code",HttpStatus.FORBIDDEN);
					rtv.put("msg",vo.getDeviceId()+" - 단말접근거부(사용중인 단말입니다.)");
					return rtv;
				}else{
					param.clear();
					param.put("searchDeviceId", vo.getDeviceId());
					dvo = deviceDataService.getDevice(param,null);
				}
				
				
				//단말 법인 권한 체크 및 active
				if(!account.getCorp().getCorpKey().equals(dvo.getCorpKey()) && !dvo.getCorpKey().equals("0")){
					rtv.put("code",HttpStatus.FORBIDDEN);
					rtv.put("msg",vo.getDeviceId()+" - 단말접근거부(해당 법인 단말이 아닙니다.)");
					return rtv;
				}else{
					v4.web.vo.DeviceVO device = new v4.web.vo.DeviceVO();
					device.setDefaultAccountKey(account.getAccountKey());
					device.setCorpKey(account.getCorp().getCorpKey());
					device.setDeviceSn(vo.getDeviceSn());
					device.setDeviceId(vo.getDeviceId());
					
					device = deviceService.activeDevice(device);
				}
				
				//단말 등록끝
				
				//vehicle insert
				
				vo.setCorpKey(account.getCorp().getCorpKey());
				vo.setDeviceId(dvo.getDeviceId());
				
				vehicleDao.createVehicle(vo);
				deviceService.updateConnectionKey(vo.getDeviceId(),vo.getConnectionKey());
				
				param.clear();
				param.put("deviceSn", dvo.getDeviceSn());
				
				param.put("downloadProperty", "downloadConvCode");
				param.put("downloadPropertyValue", vo.getDownloadConvCode());
				deviceService.updateDownloadInfo(param);

				param.put("downloadProperty", "downloadDb");
				param.put("downloadPropertyValue", vo.getDownloadDb());
				deviceService.updateDownloadInfo(param);

				param.put("downloadProperty", "downloadDeviceSound");
				param.put("downloadPropertyValue", vo.getDownloadDeviceSound());
				deviceService.updateDownloadInfo(param);
				
				param.put("downloadProperty", "downloadFirm");
				param.put("downloadPropertyValue", vo.getDownloadFirm());
				deviceService.updateDownloadInfo(param);
				
				param.put("downloadProperty", "downloadTotDistance");
				param.put("downloadPropertyValue", vo.getDownloadTotDistance());
				deviceService.updateDownloadInfo(param);
				
				vehicleDao.deleteVehicleTemp(vo.getDeviceSn());
				
				rtv.put("code",HttpStatus.CREATED);
				rtv.put("msg", vo.getVehicleKey());
				
				return rtv;
			}else{				
				//re active
				v4.web.vo.data.VehicleVO oldVehicle = vehicleDataService.getVehicleFromKey(vo.getVehicleKey());
				/* TODO - 차량 수정 권한으로 변경예정
				if(!StrUtil.isNullToEmpty(oldVehicle.getCorp().getCorpKey())){
					if(!deviceService.isDeviceMaster(oldVehicle.getDevice().getDeviceKey(),vo.getDefaultAccountKey())){
						rtv.put("code",HttpStatus.FORBIDDEN);
						rtv.put("msg",vo.getDeviceId()+" - 단말접근거부(단말 마스터 권한이 없습니다. - old device)");
						return rtv;
					}
				}
				*/
				
				//차량수정권한 통과후
				inactiveVehicle(oldVehicle.getVehicleKey());
				
				Map<String,Object> param = new HashMap<String,Object>();
				param.put("searchDeviceId", vo.getDeviceId());
				DeviceVO dvo = deviceDataService.getDevice(param,null);
				vo.setDeviceId(dvo.getDeviceId());
				
				if(null == dvo.getVehicle()){
					
					if(account.getCorp().getCorpKey().equals(dvo.getCorpKey()) || dvo.getCorpKey().equals("0")){
						v4.web.vo.DeviceVO device = new v4.web.vo.DeviceVO();
						device.setDefaultAccountKey(account.getAccountKey());
						device.setCorpKey(account.getCorp().getCorpKey());
						device.setDeviceSn(vo.getDeviceSn());
						device.setDeviceId(vo.getDeviceId());						
						device = deviceService.activeDevice(device);
						
						vehicleDao.reactiveVehicle(vo);
						deviceService.updateConnectionKey(vo.getDeviceId(),vo.getConnectionKey());
						vehicleDao.deleteVehicleTemp(vo.getDeviceSn());
						
						param.clear();
						param.put("deviceSn", dvo.getDeviceSn());
						
						param.put("downloadProperty", "downloadConvCode");
						param.put("downloadPropertyValue", vo.getDownloadConvCode());
						deviceService.updateDownloadInfo(param);

						param.put("downloadProperty", "downloadDb");
						param.put("downloadPropertyValue", vo.getDownloadDb());
						deviceService.updateDownloadInfo(param);

						param.put("downloadProperty", "downloadDeviceSound");
						param.put("downloadPropertyValue", vo.getDownloadDeviceSound());
						deviceService.updateDownloadInfo(param);
						
						param.put("downloadProperty", "downloadFirm");
						param.put("downloadPropertyValue", vo.getDownloadFirm());
						deviceService.updateDownloadInfo(param);
						
						param.put("downloadProperty", "downloadTotDistance");
						param.put("downloadPropertyValue", vo.getDownloadTotDistance());
						deviceService.updateDownloadInfo(param);
						
						/*
						if(vo.getCorp() == 0){
							int chk = getAccountVehicleChk(vo);
							if(chk == 0) createAccountVehicle(vo);
						}
						*/
						rtv.put("code",HttpStatus.CREATED);
						rtv.put("msg", vo.getVehicleKey());
						return rtv;
					}else{
						rtv.put("code",HttpStatus.FORBIDDEN);
						rtv.put("msg",vo.getDeviceId()+" - 단말접근거부(해당 법인 단말이 아닙니다.)");
						return rtv;
					}
						
				}else{
					rtv.put("code",HttpStatus.FORBIDDEN);
					rtv.put("msg",vo.getDeviceId()+" - 이미 사용중인 단말입니다. - new device");
					return rtv;
				}
			}
			
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public Map<String,Object> createBus(v4.web.vo.data.AccountVO account,CreateBusVO vo) throws BizException {
		
		try {
			
			Map<String,Object> rtv = new HashMap<String,Object>();
			
			//차량 신규등록
			if(StrUtil.isNullToEmpty(vo.getVehicleKey())){
				//차량번호 중복체크
				int chk = this.duplicatePlateNumChk(vo.getPlateNum());
				if(chk > 0){
					rtv.put("code",HttpStatus.CONFLICT);
					rtv.put("msg",vo.getPlateNum()+" - 이미 사용중인 차량번호 입니다.");
					return rtv;
				}
				
				//버스는 단말을 등록하지 않아도 등록이 되어야 한다.
				//null일경우 차량중복만 체크 하고 넘어간다.
				String deviceSn = vo.getDeviceSn();
				if(deviceSn == null){
					vo.setCorpKey(account.getCorp().getCorpKey());
					vehicleDao.createBus(vo);
					
					rtv.put("code",HttpStatus.CREATED);
					rtv.put("msg", vo.getVehicleKey());
					return rtv;
				}
				
				//active되어있으면 사용불가
				Map<String,Object> param = new HashMap<String,Object>();
				param.put("searchDeviceSn", vo.getDeviceSn());
				param.put("searchActiveState", "1");
				DeviceVO dvo = deviceDataService.getDevice(param,null);
				
				if(dvo != null){
					rtv.put("code",HttpStatus.FORBIDDEN);
					rtv.put("msg",vo.getDeviceId()+" - 단말접근거부(사용중인 단말입니다.)");
					return rtv;
				}else{
					param.clear();
					param.put("searchDeviceSn", vo.getDeviceSn());
					dvo = deviceDataService.getDevice(param,null);
				}
				
				
				//단말 법인 권한 체크 및 active
				if(!account.getCorp().getCorpKey().equals(dvo.getCorpKey()) && !dvo.getCorpKey().equals("0")){
					rtv.put("code",HttpStatus.FORBIDDEN);
					rtv.put("msg",vo.getDeviceId()+" - 단말접근거부(해당 법인 단말이 아닙니다.)");
					return rtv;
				}else{
					v4.web.vo.DeviceVO device = new v4.web.vo.DeviceVO();
					device.setDefaultAccountKey(account.getAccountKey());
					device.setCorpKey(account.getCorp().getCorpKey());
					device.setDeviceSn(dvo.getDeviceSn());
					device.setDeviceId(dvo.getDeviceId());
					
					device = deviceService.activeDevice(device);
				}
				
				//단말 등록끝
				
				vo.setCorpKey(account.getCorp().getCorpKey());
				vo.setDeviceId(dvo.getDeviceId());
				if(StrUtil.isNullToEmpty(vo.getDeviceSound())) vo.setDeviceSound("0");
				
				vehicleDao.createBus(vo);
				
				
				rtv.put("code",HttpStatus.CREATED);
				rtv.put("msg", vo.getVehicleKey());
				
				return rtv;
			}else{
				//re active
				v4.web.vo.data.VehicleVO oldVehicle = vehicleDataService.getVehicleFromKey(vo.getVehicleKey());
				
				inactiveVehicle(oldVehicle.getVehicleKey());
				
				Map<String,Object> param = new HashMap<String,Object>();
				param.put("searchDeviceId", vo.getDeviceId());
				DeviceVO dvo = deviceDataService.getDevice(param,null);
				vo.setDeviceId(dvo.getDeviceId());
				
				if(null == dvo.getVehicle()){
					
					if(account.getCorp().getCorpKey().equals(dvo.getCorpKey()) || dvo.getCorpKey().equals("0")){
						v4.web.vo.DeviceVO device = new v4.web.vo.DeviceVO();
						device.setDefaultAccountKey(account.getAccountKey());
						device.setCorpKey(account.getCorp().getCorpKey());
						device.setDeviceSn(vo.getDeviceSn());
						device.setDeviceId(vo.getDeviceId());						
						device = deviceService.activeDevice(device);
						
						vehicleDao.reactiveBus(vo);
						
						rtv.put("code",HttpStatus.CREATED);
						rtv.put("msg", vo.getVehicleKey());
						return rtv;
					}else{
						rtv.put("code",HttpStatus.FORBIDDEN);
						rtv.put("msg",vo.getDeviceId()+" - 단말접근거부(해당 법인 단말이 아닙니다.)");
						return rtv;
					}
						
				}else{
					rtv.put("code",HttpStatus.FORBIDDEN);
					rtv.put("msg",vo.getDeviceId()+" - 이미 사용중인 단말입니다.");
					return rtv;
				}
				
			}
			
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public Map<String,Object> createTruck(v4.web.vo.data.AccountVO account,CreateTruckVO vo) throws BizException {
		
		try {
			
			Map<String,Object> rtv = new HashMap<String,Object>();
			
			//차량 신규등록
			if(StrUtil.isNullToEmpty(vo.getVehicleKey())){
				//차량번호 중복체크
				int chk = this.duplicatePlateNumChk(vo.getPlateNum());
				if(chk > 0){
					rtv.put("code",HttpStatus.CONFLICT);
					rtv.put("msg",vo.getPlateNum()+" - 이미 사용중인 차량번호 입니다.");
					return rtv;
				}
				
				String deviceSn = vo.getDeviceSn();
				if(deviceSn == null){
					vo.setCorpKey(account.getCorp().getCorpKey());
					vehicleDao.createTruck(vo);
					
					rtv.put("code",HttpStatus.CREATED);
					rtv.put("msg", vo.getVehicleKey());
					return rtv;
				}
				
				//active되어있으면 사용불가
				Map<String,Object> param = new HashMap<String,Object>();
				param.put("searchDeviceSn", vo.getDeviceSn());
				param.put("searchActiveState", "1");
				DeviceVO dvo = deviceDataService.getDevice(param,null);
				
				if(dvo != null){
					rtv.put("code",HttpStatus.FORBIDDEN);
					rtv.put("msg",vo.getDeviceId()+" - 단말접근거부(사용중인 단말입니다.)");
					return rtv;
				}else{
					param.clear();
					param.put("searchDeviceSn", vo.getDeviceSn());
					dvo = deviceDataService.getDevice(param,null);
				}
				
				
				//단말 법인 권한 체크 및 active
				if(!account.getCorp().getCorpKey().equals(dvo.getCorpKey()) && !dvo.getCorpKey().equals("0")){
					rtv.put("code",HttpStatus.FORBIDDEN);
					rtv.put("msg",vo.getDeviceId()+" - 단말접근거부(해당 법인 단말이 아닙니다.)");
					return rtv;
				}else{
					v4.web.vo.DeviceVO device = new v4.web.vo.DeviceVO();
					device.setDefaultAccountKey(account.getAccountKey());
					device.setCorpKey(account.getCorp().getCorpKey());
					device.setDeviceSn(dvo.getDeviceSn());
					device.setDeviceId(dvo.getDeviceId());
					
					device = deviceService.activeDevice(device);
				}
				
				//단말 등록끝
				
				vo.setCorpKey(account.getCorp().getCorpKey());
				vo.setDeviceId(dvo.getDeviceId());
				if(StrUtil.isNullToEmpty(vo.getDeviceSound())) vo.setDeviceSound("0");
				
				vehicleDao.createTruck(vo);
				
				
				rtv.put("code",HttpStatus.CREATED);
				rtv.put("msg", vo.getVehicleKey());
				
				return rtv;
			}else{				
				//re active
				v4.web.vo.data.VehicleVO oldVehicle = vehicleDataService.getVehicleFromKey(vo.getVehicleKey());
				
				inactiveVehicle(oldVehicle.getVehicleKey());
				
				Map<String,Object> param = new HashMap<String,Object>();
				param.put("searchDeviceId", vo.getDeviceId());
				DeviceVO dvo = deviceDataService.getDevice(param,null);
				vo.setDeviceId(dvo.getDeviceId());
				
				if(null == dvo.getVehicle()){
					
					if(account.getCorp().getCorpKey().equals(dvo.getCorpKey()) || dvo.getCorpKey().equals("0")){
						v4.web.vo.DeviceVO device = new v4.web.vo.DeviceVO();
						device.setDefaultAccountKey(account.getAccountKey());
						device.setCorpKey(account.getCorp().getCorpKey());
						device.setDeviceSn(vo.getDeviceSn());
						device.setDeviceId(vo.getDeviceId());						
						device = deviceService.activeDevice(device);
						
						vehicleDao.reactiveTruck(vo);
						
						rtv.put("code",HttpStatus.CREATED);
						rtv.put("msg", vo.getVehicleKey());
						return rtv;
					}else{
						rtv.put("code",HttpStatus.FORBIDDEN);
						rtv.put("msg",vo.getDeviceId()+" - 단말접근거부(해당 법인 단말이 아닙니다.)");
						return rtv;
					}
						
				}else{
					rtv.put("code",HttpStatus.FORBIDDEN);
					rtv.put("msg",vo.getDeviceId()+" - 이미 사용중인 단말입니다.");
					return rtv;
				}
				
			}
			
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public Map<String,Object> createEtc(v4.web.vo.data.AccountVO account,CreateEtcVO vo) throws BizException {
		
		try {
			
			Map<String,Object> rtv = new HashMap<String,Object>();
			
			//차량 신규등록
			if(StrUtil.isNullToEmpty(vo.getVehicleKey())){
				//차량번호 중복체크
				int chk = this.duplicatePlateNumChk(vo.getPlateNum());
				if(chk > 0){
					rtv.put("code",HttpStatus.CONFLICT);
					rtv.put("msg",vo.getPlateNum()+" - 이미 사용중인 차량번호 입니다.");
					return rtv;
				}
				
				vo.setCorpKey(account.getCorp().getCorpKey());
				
				vehicleDao.createEtc(vo);
				
				
				rtv.put("code",HttpStatus.CREATED);
				rtv.put("msg", vo.getVehicleKey());
				
				return rtv;
			}else{
				//단말이 없으므로 reactive 프로세스 없음
				rtv.put("code",HttpStatus.CREATED);
				rtv.put("msg", vo.getVehicleKey());
				
				return rtv;
			}
			
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}
	@Override
	public Map<String, Object> createVehicleTemp(v4.web.vo.data.AccountVO account, CreateVehicleVO vo) throws BizException {
		try {
			
			Map<String,Object> rtv = new HashMap<String,Object>();
			
			//차량 신규등록
			if(StrUtil.isNullToEmpty(vo.getVehicleKey())){
				//차량번호 중복체크
				int chk = this.duplicatePlateNumChk(vo.getPlateNum());
				if(chk > 0){
					rtv.put("code",HttpStatus.CONFLICT);
					rtv.put("msg",vo.getPlateNum()+" - 이미 사용중인 차량번호 입니다.");
					return rtv;
				}
				
				if(vehicleDBService.getVehicleDbInfo("ko", vo.getConvCode())==null){
					rtv.put("code",HttpStatus.NOT_FOUND);
					rtv.put("msg","convCode - "+vo.getConvCode()+" - 잘못된 convCode입니다.");
					return rtv;
				}
				
				//active되어있으면 사용불가
				Map<String,Object> param = new HashMap<String,Object>();
				param.put("searchDeviceId", vo.getDeviceId());
				param.put("searchActiveState", "1");
				DeviceVO dvo = deviceDataService.getDevice(param,null);
				
				if(dvo != null){
					rtv.put("code",HttpStatus.FORBIDDEN);
					rtv.put("msg",vo.getDeviceId()+" - 단말접근거부(사용중인 단말입니다.)");
					return rtv;
				}else{
					param.clear();
					param.put("searchDeviceId", vo.getDeviceId());
					dvo = deviceDataService.getDevice(param,null);
				}
				
				
				//단말 법인 권한 체크 및 active
				if(!account.getCorp().getCorpKey().equals(dvo.getCorpKey()) && !dvo.getCorpKey().equals("0")){
					rtv.put("code",HttpStatus.FORBIDDEN);
					rtv.put("msg",vo.getDeviceId()+" - 단말접근거부(해당 법인 단말이 아닙니다.)");
					return rtv;
				}				
				
				
				
				vehicleDao.deleteVehicleTemp(vo.getDeviceSn());
				vehicleDao.createVehicleTemp(vo);
				
				rtv.put("code",HttpStatus.CREATED);
				rtv.put("msg", vo.getVehicleKey());
				
				return rtv;
			}else{
				//re active
				v4.web.vo.data.VehicleVO oldVehicle = vehicleDataService.getVehicleFromKey(vo.getVehicleKey());
				
				/* TODO - 차량 수정 권한으로 변경예정
				if(!StrUtil.isNullToEmpty(oldVehicle.getCorp().getCorpKey())){
					if(!deviceService.isDeviceMaster(oldVehicle.getDevice().getDeviceKey(),vo.getDefaultAccountKey())){
						rtv.put("code",HttpStatus.FORBIDDEN);
						rtv.put("msg",vo.getDeviceId()+" - 단말접근거부(단말 마스터 권한이 없습니다. - old device)");
						return rtv;
					}
				}
				*/
				
				Map<String,Object> param = new HashMap<String,Object>();
				param.put("searchDeviceId", vo.getDeviceId());
				DeviceVO dvo = deviceDataService.getDevice(param,null);
				
				if(null == dvo.getVehicle()){
					
					vo.setRegUser(account.getAccountKey());
					vo.setConvCode(oldVehicle.getVehicleModel().getConvCode());
					vo.setTotDistance(oldVehicle.getTotDistance());
					
					if(account.getCorp().getCorpKey().equals(dvo.getCorpKey()) || dvo.getCorpKey().equals("0")){
						
						vehicleDao.deleteVehicleTemp(vo.getDeviceSn());
						vehicleDao.createVehicleTemp(vo);
						/*
						if(vo.getCorp() == 0){
							int chk = getAccountVehicleChk(vo);
							if(chk == 0) createAccountVehicle(vo);
						}
						*/
						rtv.put("code",HttpStatus.CREATED);
						rtv.put("msg", vo.getVehicleKey());
						return rtv;
					}else{
						rtv.put("code",HttpStatus.FORBIDDEN);
						rtv.put("msg",vo.getDeviceId()+" - 단말접근거부(해당 법인 단말이 아닙니다.)");
						return rtv;
					}
						
				}else{
					rtv.put("code",HttpStatus.FORBIDDEN);
					rtv.put("msg",vo.getDeviceId()+" - 이미 사용중인 단말입니다. - new device");
					return rtv;
				}
			}
			
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}	

	@Override
	public int duplicatePlateNumChk(String plateNum)  throws BizException {
		try {
			return vehicleDao.duplicatePlateNumChk(plateNum);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public VehicleVO getVehicle(Map<String,Object> param,Map<String,Object> auth) throws BizException {
		try {
			param.put("authTable", auth);
			return vehicleDao.getVehicle(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	

	@Override
	public List<VehicleVO> getVehicleList(Map<String,Object> param,Map<String,Object> auth) throws BizException {
		try {
			param.put("authTable", auth);
			return vehicleDao.getVehicleList(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	
	@Override
	public int getVehicleListCnt(Map<String,Object> param,Map<String,Object> auth) throws BizException {
		try {
			param.put("authTable", auth);
			return vehicleDao.getVehicleListCnt(param);
		} catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
				System.err.println(e.getMessage());
				throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	
	@Override
	public void inactiveVehicle(String vehicleKey) throws BizException {
		try {
			vehicleDao.inactiveVehicle(vehicleKey);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}

	@Override
	public void deleteVehicle(String vehicleKey) throws BizException {
		try {
			//자식사용자 unlinkAll
			//vehicleDao.unLinkVehicleAll(vehicleKey);
			
			//관련데이터삭제?(allocate , approval , trip , tdr 등)
			
			vehicleDao.deleteVehicle(vehicleKey);
			
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}

	/**
	 * 배차가 가능한 차량리스트를 뽑아 온다.
	 */
	@Override
	public List<VehicleVO> getAllocateListBeCan(AllocateVO vo) throws BizException {
		
		try {
			List<VehicleVO> rtv = vehicleDao.getAllocateListBeCan(vo);
			
			return rtv;
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void vehicleLastestInfoUpdate(String vehicleKey, int distance, String location, String endAddr,
			String endAddrEn, long endDate, String tripId,String driver) throws BizException {
		vehicleLastestInfoUpdate(vehicleKey,distance,location,endAddr,endAddrEn,null,endDate,tripId,driver);
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void vehicleLastestInfoUpdate(String vehicleKey,int distance , String location, String endAddr, String endAddrEn,
			String endAddrDetail, long endDate, String tripId,String driver) throws BizException {
		
		try {
			Map<String,Object> param = new HashMap<String, Object>();
			param.put("vehicleKey", vehicleKey);
			param.put("location", location);
			param.put("endDate", endDate);
			param.put("endAddr", endAddr);
			param.put("endAddrEn", endAddrEn);
			param.put("endAddrDetail", endAddrDetail);
			param.put("accureDistance", String.valueOf(distance));
			param.put("tripKey", tripId);
			param.put("driver", driver);
			
			if(!location.equals("0,0") && !location.equals("0.0,0.0"))
				vehicleDao.updateLastestLocation(param);
			if(tripId != null) vehicleDao.updateLastestTrip(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
		
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void updateLastTripStart(String vehicleKey, String tripKey) throws BizException {
		
		try {
			Map<String,Object> param = new HashMap<String, Object>();
			param.put("vehicleKey", vehicleKey);
			param.put("tripKey", tripKey);
			
			vehicleDao.updateLastestTripStart(param);
			
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public void updateTotDistance(TripVO vo) throws BizException{
		try {
						
			vehicleDao.updateTotDistance(vo);
			
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public VehicleVO getVehicleInAllocate(VehicleVO vvo) throws BizException{
		try {
						
			return vehicleDao.getVehicleInAllocate(vvo);	
			
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
			
	

	@Override
	@Transactional(rollbackFor=Exception.class)
	public void updateLastestLoc(VehicleVO vvo)  throws BizException{
		try {
			
			vehicleDao.updateLastestLoc(vvo);	
			
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public CreateVehicleVO getTempVehicle(String deviceSn) throws BizException {
		try {
			
			return vehicleDao.getTempVehicle(deviceSn);	
			
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public void updateLastestDbInfoTemp(String deviceSn, String strDbNm, String strFirmNm, String convCode, int totDistance,String deviceSound) throws BizException{
		try {
			
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("deviceSn", deviceSn);
			param.put("downloadDb", strDbNm);
			param.put("downloadFirm", strFirmNm);
			param.put("downloadTotDistance", totDistance);
			param.put("downloadConvCode", convCode);
			param.put("downloadDeviceSound", deviceSound);
			
			vehicleDao.updateLastestDbInfoTemp(param);	
			
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public void updateVehicle(String vehicleKey, String convCode, String totDistance, String deviceSound) throws BizException {
		
		try {
			
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("vehicleKey", vehicleKey);
			param.put("convCode", convCode);
			param.put("totDistance", totDistance);
			param.put("deviceSound", deviceSound);
			
			vehicleDao.updateVehicle(param);	
			
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
		
	}
	
}

