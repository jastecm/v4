package v4.web.rest.service.impl;

import java.nio.ByteOrder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.jastecm.convert.ConvertUtil;

import v4.ex.exc.BizException;
import v4.web.rest.dao.DtcDAO;
import v4.web.rest.service.DtcService;
import v4.web.vo.DtcVO;
import v4.web.vo.data.VehicleVO;


@Service("dtcService")
public class DtcServiceImpl implements DtcService {

	@Autowired
	DtcDAO dtcDao;
	/*
	@Autowired
	RestDrivingService restDrivingService;
	
	*/
	
	@Override
	public String saveDtc(VehicleVO vehicle, DtcVO dtcVo) throws BizException {
		
		String tripKey = null;
		
		String dtcKey = getTripDtcKey(tripKey);
		if(!StrUtil.isNullToEmpty(dtcKey)){
			dtcKey = appendDtc(dtcKey,vehicle.getVehicleModel().getDtcManufacture(),dtcVo);
		}else{
			String vehicleKey = vehicle.getVehicleKey();
			
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("vehicleKey",vehicleKey);
			param.put("curTime",new Date().getTime());
			dtcKey = dtcDao.dtcKeyFromTime(param);
			
			if(!StrUtil.isNullToEmpty(dtcKey)){
				dtcKey = appendDtc(dtcKey,vehicle.getVehicleModel().getDtcManufacture(),dtcVo);
			}else{
				dtcKey = createDtc(vehicleKey,tripKey,vehicle.getVehicleModel().getDtcManufacture(),dtcVo);
			}
		}
		
		return dtcKey;
	}
	
	@Override
	public void dtcTripChk(String vehicleKey, long startTime, long endTime) throws BizException {
		
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			long nStartDate = startTime-(1000*60*5);
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("vehicleKey", vehicleKey);
			param.put("startDate", nStartDate);
			param.put("endDate", endTime);
			
			int chk = dtcDao.chkTripDtc(param);
			if(chk == 0){
				param.clear();
				param.put("vehicleKey", vehicleKey);
				param.put("tripKey", null);
				param.put("curTime", String.valueOf(nStartDate+(1000*60*5)));
				
				dtcDao.insertDtcHstr(param);
			}
		}catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
		
	}
	
	private String createDtc(String vehicleKey, String tripKey, String dtcManufacture,DtcVO vo) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("vehicleKey", vehicleKey);
		param.put("tripKey", tripKey);
		param.put("curTime", String.valueOf(new Date().getTime()));
		
		dtcDao.insertDtcHstr(param);
		
		String dtcKey = (String)param.get("dtcKey");
		appendDtc(dtcKey,dtcManufacture,vo);
		
		return dtcKey;
	}
	
	String appendDtc(String dtcKey , String dtcManufacture, DtcVO vo){		 	
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("dtcKey", dtcKey);
		param.put("manufacture", dtcManufacture);
		param.put("dtcType", String.valueOf(vo.getDtcType()));
		param.put("dtcKind", String.valueOf(vo.getDtcKind()));
		for(String dtcCode : vo.getDtcCode()){
			if(dtcCode.equals("P0000") || dtcCode.equals("0000000")) continue;
			param.put("dtcCode", dtcCode);
			dtcDao.appendDtc(param);
		}
		
		return dtcKey;
	}
	
	@Override
	public DtcVO parseDtcPacket(String packet,String convCode,String dtcManufacture) throws BizException {
		int tripId = 0;
		long curTime = 0;
		
		int dtcKind = 0;
		int dtcType = 0;
		int dtcSize = 0;
		int dtcCnt = 0;
		List<String> dtcCode = new ArrayList<String>();
		
		int pointer = 0;
		
		String hex_id = packet.substring(pointer,pointer+=8);
		//tripId = byteArrayToInt(hexToByteArray(hex_id), LITTLE_EDIAN);
		String hex_curTime = packet.substring(pointer,pointer+=8);
		curTime = ConvertUtil.deviceTimeToServerTime(ConvertUtil.byteArrayToInt(ConvertUtil.hexToByteArray(hex_curTime), ByteOrder.LITTLE_ENDIAN));
		String hex_dtcKind = packet.substring(pointer,pointer+=2); 
		dtcKind = ConvertUtil.byteArrayToInt(ConvertUtil.hexToByteArray(hex_dtcKind), ByteOrder.LITTLE_ENDIAN);
		String hex_dtcType = packet.substring(pointer,pointer+=4);
		dtcType = ConvertUtil.byteArrayToInt(ConvertUtil.hexToByteArray(hex_dtcType), ByteOrder.LITTLE_ENDIAN);
		String hex_dtcSize = packet.substring(pointer,pointer+=2);
		dtcSize = ConvertUtil.byteArrayToInt(ConvertUtil.hexToByteArray(hex_dtcSize), ByteOrder.LITTLE_ENDIAN);
		String hex_dtcCnt = packet.substring(pointer,pointer+=2);
		dtcCnt = ConvertUtil.byteArrayToInt(ConvertUtil.hexToByteArray(hex_dtcCnt), ByteOrder.LITTLE_ENDIAN);
		
		if(dtcCnt != 0){
			int unit = dtcSize/dtcCnt;
			
			for(int i = 0 ; i < dtcCnt ; i++){
				String hex_dtcCodeUnit = packet.substring(pointer,pointer+=(unit*2));
				int dtcCodeUnit = ConvertUtil.byteArrayToInt(ConvertUtil.hexToByteArray(hex_dtcCodeUnit), ByteOrder.LITTLE_ENDIAN);
				
				dtcCode.add(parseDtc(dtcCodeUnit,unit));
			}
			 
		}
		
		DtcVO rtv = new DtcVO();
		rtv.setCurTime(curTime);
		rtv.setDtcCode(dtcCode);
		rtv.setDtcKind(dtcKind);
		rtv.setDtcType(dtcType);
		rtv.setTripId(tripId);
		
		return rtv;
	}
	
	String parseDtc(int dtcInt,int unit){
		String rtv ="";
		
		if(dtcInt == 0) return rtv;
		int head = 0;
		int code1 = 0;
		int code2 = 0;
		int code3 = 0;
		int code4 = 0;
		
		int code5 = 0;
		int code6 = 0;
		
		if(unit == 2){
			code4 = dtcInt & 0x0f;
			code3 = (dtcInt>>4) & 0x0f;
			code2 = (dtcInt>>8) & 0x0f;
			code1 = (dtcInt>>12) & 0x03;
			head = (dtcInt>>14) & 0x03;
		}else if(unit == 3){
			code6 = dtcInt & 0x0f;
			code5 = (dtcInt>>4) & 0x0f;
			code4 = (dtcInt>>8) & 0x0f;
			code3 = (dtcInt>>12) & 0x0f;
			code2 = (dtcInt>>16) & 0x0f;
			code1 = (dtcInt>>20) & 0x03;
			head = (dtcInt>>22) & 0x03;
		}
		
		if(head == 0) rtv = "P";
		if(head == 1) rtv = "C";
		if(head == 2) rtv = "B";
		if(head == 3) rtv = "U";
		if(head == 4) rtv = "0";
		
		rtv += Integer.toHexString(code1);
		rtv += Integer.toHexString(code2);
		rtv += Integer.toHexString(code3);
		rtv += Integer.toHexString(code4);
		
		if(unit == 3){
			rtv += Integer.toHexString(code5);
			rtv += Integer.toHexString(code6);
		}
		
		return rtv.toUpperCase();
		
	}
	
	String getTripDtcKey(String tripKey){
		return dtcDao.getTripDtcKey(tripKey);
	}
	
	@Override
	public List<Map<String, String>> getDtcList(String vehicleKey, String dtcKey,String lang) throws BizException {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("vehicleKey", vehicleKey);
		param.put("dtcKey", dtcKey);
		param.put("lang", lang);
		return dtcDao.getDtcList(param);
	}
	
	@Override
	public Map<String, String> getDtcDesc(String dtcCode, String dtcManufacture, String lang) {
		Map<String, String> param = new HashMap<String, String>();
		param.put("dtcCode", dtcCode);
		param.put("dtcManufacture", dtcManufacture);
		param.put("lang", lang);
		return dtcDao.getDtcDesc(param);
	}
	/////////////////////////////////////
/*	
	//used?
	DTCCodeVO getDtcDetail(DTCCodeVO vo){
		DTCCodeVO rtv = restDtcDao.getDtcManufactureDetail(vo);
		if(rtv == null) rtv = restDtcDao.getDtcGenaralDetail(vo);
		if(rtv == null) {
			rtv = new DTCCodeVO();
			rtv.setCode(vo.getCode());
		}
		return rtv;
	}
	
	

	
	
	@Override
	public List<Map<String, String>> getDtcHeaderList(String vehicleKey,int limit , int offset,String lang) throws BizException {
		Map<String, String> param = new HashMap<String, String>();
		param.put("vehicleKey", vehicleKey);
		param.put("limit", String.valueOf(limit));
		param.put("offset", String.valueOf(offset));
		param.put("lang", lang);
		return restDtcDao.getDtcHeaderList(param);
	}


	


	@Override
	public void removeAction(String vehicleKey) throws BizException {
		
		restDtcDao.removeAction(vehicleKey);
	}
		    
	@Override
	public void removeHstr(String dtcKey) throws BizException {
		
		restDtcDao.removeHstrRecord(dtcKey);
		restDtcDao.removeHstr(dtcKey);
	}


	
*/

}

