package v4.web.rest.service.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jastecm.string.StrUtil;
import org.springframework.stereotype.Service;

import v4.web.rest.service.ScoringService;
import v4.web.vo.data.TripVO;
import v4.web.vo.data.TripVO.TrackWarningEvent;


@Service("scoringService")
public class ScoringServiceImpl implements ScoringService {
	
	private Log logger = LogFactory.getLog(getClass());

	@Override
	public TripVO calcScoring(TripVO vo) {
		
		TrackWarningEvent suddenSummary = vo.getTrackWarningEvent();
		
		String compoundfuelEff = vo.getVehicle().getVehicleModel().getCompoundFuelEfficiency();
		String fuelEff = vo.getVehicle().getVehicleModel().getFuelEfficiency();
		
		if(!StrUtil.isNullToEmpty(compoundfuelEff)) fuelEff = compoundfuelEff;
		
		try {
			vo.setEcoScore(calcEcoScore(vo,suddenSummary,fuelEff));
			vo.setSafeScore(calcSafeScore(vo,suddenSummary));
			vo.setFuelScore(calcFuelScore(vo,suddenSummary,fuelEff));
			
			vo.setScore1(calcScore1(vo,suddenSummary,fuelEff));
			vo.setScore2(calcScore2(vo,suddenSummary,fuelEff));
			vo.setScore3(calcScore3(vo,suddenSummary,fuelEff));
			vo.setScore4(vo.getFuelScore());
			
			
		} catch (ParseException e) {
			vo.setEcoScore(-1f);
			vo.setEcoScore(-1f);
			vo.setFuelScore(-1f);
			e.printStackTrace();
		} catch (Exception e){
			vo.setEcoScore(-1f);
			vo.setSafeScore(-1f);
			vo.setFuelScore(-1f);
			e.printStackTrace();
		}
		
		return vo;
	}
	
	private float calcScore1(TripVO vo, TrackWarningEvent suddenSummary,String fuelEff) throws ParseException{
		float distanceKm = vo.getDistance()* (float)0.001;
		
		float weightSuddenStop = 50f;
		float pointSuddenStop = 3;

		float weightSuddenDecc = 50f;
		float pointSuddenDecc = 3;
		
		
		float b = suddenSummary.getRapidStop();
		float d = suddenSummary.getRapidDeaccel();
		
		
		b = calcScoreTemp(b,pointSuddenStop,distanceKm);
		d = calcScoreTemp(d,pointSuddenDecc,distanceKm);
		
		List<Float> arrOver = new ArrayList<Float>();
		List<Float> arrUnder = new ArrayList<Float>();
		List<Float> arrOverWeight = new ArrayList<Float>();
		List<Float> arrUnderWeight = new ArrayList<Float>();
			
		if(b >= 1){ 
			arrOver.add(b-1);
			arrOverWeight.add(weightSuddenStop);
		}else{
			arrUnder.add(b);
			arrUnderWeight.add(weightSuddenStop);
		}
		if(d >= 1){ 
			arrOver.add(d-1);
			arrOverWeight.add(weightSuddenDecc);
		}else{
			arrUnder.add(d);
			arrUnderWeight.add(weightSuddenDecc);
		}
		
		float totOver = 0;
		float totOverWeight = 0;
		float totOverPoint = 0;
		for(int i = 0 ; i < arrOver.size() ; i++){
			totOver += arrOver.get(i);
			totOverWeight += arrOverWeight.get(i);
			totOverPoint += arrOver.get(i)*arrOverWeight.get(i);			
		}
		
		
		float totUnderWeight = 0;
		for(int i = 0 ; i < arrUnder.size() ; i++){
			
			totUnderWeight += arrUnderWeight.get(i);
			
		}
		
		float totOverRatio = 1;
		if(totOverWeight != 0f){
			totOverRatio += (totOverPoint/totOverWeight);	
		}
		
		
		float removePointRatio = (100 - (100/totOverRatio))/100;
		
		
		float pulsPoint = totUnderWeight*removePointRatio;
		if(arrUnder.size()==0) pulsPoint = 0;
		else pulsPoint = pulsPoint / arrUnder.size();
		
		
		float finalMinusScore = 0;
		for(int i = 0 ; i < arrOver.size() ; i++){
			if(totOverPoint == 0f){
				finalMinusScore += arrOverWeight.get(i);
			}else{
				float eachPointPulsRatio = (arrOver.get(i)*arrOverWeight.get(i))/totOverPoint;
				arrOverWeight.set(i, arrOverWeight.get(i)+(pulsPoint*eachPointPulsRatio));
				finalMinusScore += arrOverWeight.get(i);
			}
		}
		
		float minusPoint = totUnderWeight-pulsPoint;
		
		for(int i = 0 ; i < arrUnder.size() ; i++){
			
			float eachPointUnderRatio = arrUnderWeight.get(i)/totUnderWeight;
			
			float fixedUnderPoint = minusPoint*eachPointUnderRatio;
			arrUnder.set(i, fixedUnderPoint*arrUnder.get(i)); 
			
			finalMinusScore += arrUnder.get(i);
		}
		
		
		return (100 -finalMinusScore);
	}
	private float calcScore2(TripVO vo, TrackWarningEvent suddenSummary,String fuelEff) throws ParseException{
		float distanceKm = vo.getDistance()* (float)0.001;
		
		float weightSuddenStart = 25f;
		float pointSuddenStart = 3;
		float weightSuddenAcc = 25f;
		float pointSuddenAcc = 3;
		float weightSuddenTurn = 25f;
		float pointSuddenTurn = 1;
		float weightSuddenUturn = 25f;
		float pointSuddenUturn = 1;
		
		float a = suddenSummary.getRapidStart();
		float c = suddenSummary.getRapidAccel();
		float e = suddenSummary.getRapidTurn();
		float f = suddenSummary.getRapidUtern();
		
		
		a = calcScoreTemp(a,pointSuddenStart,distanceKm);
		c = calcScoreTemp(c,pointSuddenAcc,distanceKm);
		e = calcScoreTemp(e,pointSuddenTurn,distanceKm);
		f = calcScoreTemp(f,pointSuddenUturn,distanceKm);
		
		List<Float> arrOver = new ArrayList<Float>();
		List<Float> arrUnder = new ArrayList<Float>();
		List<Float> arrOverWeight = new ArrayList<Float>();
		List<Float> arrUnderWeight = new ArrayList<Float>();
			
		if(a >= 1){ 
			arrOver.add(a-1);
			arrOverWeight.add(weightSuddenStart);
		}else{
			arrUnder.add(a);
			arrUnderWeight.add(weightSuddenStart);
		}
		if(c >= 1){ 
			arrOver.add(c-1);
			arrOverWeight.add(weightSuddenAcc);
		}else{
			arrUnder.add(c);
			arrUnderWeight.add(weightSuddenAcc);
		}
		if(e >= 1){ 
			arrOver.add(e-1);
			arrOverWeight.add(weightSuddenTurn);
		}else{
			arrUnder.add(e);
			arrUnderWeight.add(weightSuddenTurn);
		}
		if(f >= 1){ 
			arrOver.add(f-1);
			arrOverWeight.add(weightSuddenUturn);
		}else{
			arrUnder.add(f);
			arrUnderWeight.add(weightSuddenUturn);
		}
		
		float totOver = 0;
		float totOverWeight = 0;
		float totOverPoint = 0;
		for(int i = 0 ; i < arrOver.size() ; i++){
			totOver += arrOver.get(i);
			totOverWeight += arrOverWeight.get(i);
			totOverPoint += arrOver.get(i)*arrOverWeight.get(i);			
		}
		
		
		float totUnderWeight = 0;
		for(int i = 0 ; i < arrUnder.size() ; i++){
			
			totUnderWeight += arrUnderWeight.get(i);
			
		}
		
		float totOverRatio = 1;
		if(totOverWeight != 0f){
			totOverRatio += (totOverPoint/totOverWeight);	
		}
		
		
		float removePointRatio = (100 - (100/totOverRatio))/100;
		
		
		float pulsPoint = totUnderWeight*removePointRatio;
		if(arrUnder.size()==0) pulsPoint = 0;
		else pulsPoint = pulsPoint / arrUnder.size();
		
		
		float finalMinusScore = 0;
		for(int i = 0 ; i < arrOver.size() ; i++){
			if(totOverPoint == 0f){
				finalMinusScore += arrOverWeight.get(i);
			}else{
				float eachPointPulsRatio = (arrOver.get(i)*arrOverWeight.get(i))/totOverPoint;
				arrOverWeight.set(i, arrOverWeight.get(i)+(pulsPoint*eachPointPulsRatio));
				finalMinusScore += arrOverWeight.get(i);
			}
		}
		
		float minusPoint = totUnderWeight-pulsPoint;
		
		for(int i = 0 ; i < arrUnder.size() ; i++){
			
			float eachPointUnderRatio = arrUnderWeight.get(i)/totUnderWeight;
			
			float fixedUnderPoint = minusPoint*eachPointUnderRatio;
			arrUnder.set(i, fixedUnderPoint*arrUnder.get(i)); 
			
			finalMinusScore += arrUnder.get(i);
		}
		
		return (100 -finalMinusScore);
	}
	private float calcScore3(TripVO vo, TrackWarningEvent suddenSummary,String fuelEff) throws ParseException{
		float distanceKm = vo.getDistance()* (float)0.001;
		
		float weightSuddenSpeed = 50f;
		float pointSuddenSpeed = 1;
		float weightSuddenLongSpeed = 50f;
		float pointSuddenLongSpeed = 1;
		
		
		
		float a = 0f;
		float c = 0f;
		
		
		a = calcScoreTemp(a,pointSuddenSpeed,distanceKm);
		c = calcScoreTemp(c,pointSuddenLongSpeed,distanceKm);
		
		List<Float> arrOver = new ArrayList<Float>();
		List<Float> arrUnder = new ArrayList<Float>();
		List<Float> arrOverWeight = new ArrayList<Float>();
		List<Float> arrUnderWeight = new ArrayList<Float>();
			
		if(a >= 1){ 
			arrOver.add(a-1);
			arrOverWeight.add(weightSuddenSpeed);
		}else{
			arrUnder.add(a);
			arrUnderWeight.add(weightSuddenSpeed);
		}
		if(c >= 1){ 
			arrOver.add(c-1);
			arrOverWeight.add(weightSuddenLongSpeed);
		}else{
			arrUnder.add(c);
			arrUnderWeight.add(weightSuddenLongSpeed);
		}
		
		float totOver = 0;
		float totOverWeight = 0;
		float totOverPoint = 0;
		for(int i = 0 ; i < arrOver.size() ; i++){
			totOver += arrOver.get(i);
			totOverWeight += arrOverWeight.get(i);
			totOverPoint += arrOver.get(i)*arrOverWeight.get(i);			
		}
		
		
		float totUnderWeight = 0;
		for(int i = 0 ; i < arrUnder.size() ; i++){
			
			totUnderWeight += arrUnderWeight.get(i);
			
		}
		
		float totOverRatio = 1;
		if(totOverWeight != 0f){
			totOverRatio += (totOverPoint/totOverWeight);	
		}
		
		
		float removePointRatio = (100 - (100/totOverRatio))/100;
		
		
		float pulsPoint = totUnderWeight*removePointRatio;
		if(arrUnder.size()==0) pulsPoint = 0;
		else pulsPoint = pulsPoint / arrUnder.size();
		
		
		float finalMinusScore = 0;
		for(int i = 0 ; i < arrOver.size() ; i++){
			if(totOverPoint == 0f){
				finalMinusScore += arrOverWeight.get(i);
			}else{
				float eachPointPulsRatio = (arrOver.get(i)*arrOverWeight.get(i))/totOverPoint;
				arrOverWeight.set(i, arrOverWeight.get(i)+(pulsPoint*eachPointPulsRatio));
				finalMinusScore += arrOverWeight.get(i);
			}
		}
		
		float minusPoint = totUnderWeight-pulsPoint;
		
		for(int i = 0 ; i < arrUnder.size() ; i++){
			
			float eachPointUnderRatio = arrUnderWeight.get(i)/totUnderWeight;
			
			float fixedUnderPoint = minusPoint*eachPointUnderRatio;
			arrUnder.set(i, fixedUnderPoint*arrUnder.get(i)); 
			
			finalMinusScore += arrUnder.get(i);
		}
		
		return (100 -finalMinusScore);
	}
	private float calcScore4(TripVO vo, TrackWarningEvent suddenSummary,String fuelEff) throws ParseException{
		return 0;
	}
	
	private long calcTimeDiff(long start , long end) throws ParseException{
		return (end-start)/1000;
	}
	
	private float calcFuelScore(TripVO vo, TrackWarningEvent suddenSummary,String fuelEff) throws ParseException {
		float fcoScoreRatio = (float)(vo.getAvgFco()/new Float(fuelEff))*100;
		if(fcoScoreRatio>100) fcoScoreRatio = 100;
		return fcoScoreRatio;
	}
	
	private float calcEcoScore(TripVO vo, TrackWarningEvent suddenSummary,String fuelEff) throws ParseException {
		float distanceKm = vo.getDistance()* (float)0.001;
		
		long timeDiff = calcTimeDiff(vo.getStartDate().getTime(),vo.getEndDate().getTime());
		System.out.println("timeDiff : " + timeDiff);
		int idle = vo.getIdleTime();
		
		float timeDiffF = new Long(timeDiff).floatValue();
		float idleF = new Integer(idle).floatValue();
		
		float idleRatioScore = (idleF/timeDiffF)*100;
		if (idleRatioScore > 10) idleRatioScore = 10;
		if(timeDiffF <= 0) idleRatioScore = 0;
		
		System.out.println("idleRatioScore : " + idleRatioScore);
		//idleRatioScore
		int ecoTime = vo.getEcoTime();
		float ecoTimeF = new Integer(ecoTime).floatValue();
		
		float ecoTimeRatio = (ecoTimeF/timeDiffF)*100;
		
		if(ecoTimeRatio>80) ecoTimeRatio = 80;
		else if(ecoTimeRatio < 60) ecoTimeRatio = 60;
		
		System.out.println("ecoTimeRatio : " + ecoTimeRatio);
		ecoTimeRatio = 80 - ecoTimeRatio;
		
		float ecoTimeRatioScore = ecoTimeRatio*0.5f;
		System.out.println("ecoTimeRatioScore : " + ecoTimeRatioScore);
		//ecoTimeRatioScore
		
		
		float suddenStartScore = calcScoreTemp(new Float(suddenSummary.getRapidStart()), 5, distanceKm);
		if(suddenStartScore > 1) suddenStartScore = 1*10;
		else suddenStartScore = suddenStartScore*10;
		float suddenAccScore = calcScoreTemp(new Float(suddenSummary.getRapidAccel()), 5, distanceKm);
		if(suddenAccScore > 1) suddenAccScore = 1*10;
		else suddenAccScore = suddenAccScore*10;
		
		//suddenStartScore,suddenAccScore
		
		int fuelCutTime = vo.getFuelCutTime();
		float fuelCutTimeF = new Integer(fuelCutTime).floatValue();
		
		float fuelCutTimeRatio = (fuelCutTimeF/timeDiffF)*100;
		if(timeDiffF <= 0) fuelCutTimeRatio = 0;
		
		System.out.println("fuelCutTimeRatio : " + fuelCutTimeRatio);
		if(fuelCutTimeRatio>20) fuelCutTimeRatio = 20;
		
		float fuelCutTimeRatioScore = fuelCutTimeRatio*0.5f;
		System.out.println("fuelCutTimeRatioScore : " + fuelCutTimeRatioScore);
		//fuelCutTimeRatioScore
		
		float fcoScoreRatio = (float)(vo.getAvgFco()/new Float(fuelEff))*100;
		if(fcoScoreRatio>100) fcoScoreRatio = 100;
		float fcoScoreRatioScore = 50 - (fcoScoreRatio/2);
		System.out.println("fcoScoreRatio : " + fcoScoreRatio);
		System.out.println("fcoScoreRatioScore : " + fcoScoreRatioScore);
		//fcoScoreRatioScore
		
		
		float totalScore = 100 - idleRatioScore - ecoTimeRatioScore - suddenStartScore - suddenAccScore - fuelCutTimeRatioScore - fcoScoreRatioScore;
		 
		
		return totalScore;
	}
	
	private float calcSafeScore(TripVO vo, TrackWarningEvent suddenSummary){
		float distanceKm = vo.getDistance()* (float)0.001;
		
		float weightSuddenStart = 12.5f;
		float pointSuddenStart = 3;
		float weightSuddenStop = 12.5f;
		float pointSuddenStop = 3;
		float weightSuddenAcc = 12.5f;
		float pointSuddenAcc = 3;
		float weightSuddenDecc = 12.5f;
		float pointSuddenDecc = 3;
		float weightSuddenTurn = 12.5f;
		float pointSuddenTurn = 3;
		float weightSuddenUturn = 12.5f;
		float pointSuddenUturn = 3;
		float weightOverSpeed = 12.5f;
		float pointOverSpeed = 1;
		float weightOverSpeedLong = 12.5f;
		float pointOverSpeedLong = 1;
		
		float midnightDrivingPoint = 0;
		
		System.out.println("startDate : " + vo.getStartDate());
		System.out.println("endDate : " + vo.getEndDate());
		float scoreMidnightDriving = calcScoreMidnight(vo.getStartDate().getTime(),vo.getEndDate().getTime(),240*60*1000L);
		scoreMidnightDriving = scoreMidnightDriving*midnightDrivingPoint;
		System.out.println("midnightScore : " + scoreMidnightDriving);
		
		/*
		select sum(rapidStart) rapidStart
		,sum(rapidStop) rapidStop
		,sum(rapidAccel) rapidAccel
		,sum(rapidDeaccel) rapidDeaccel
		,sum(rapidTurn) rapidTurn
		,sum(rapidUtern) rapidUtern
		,sum(overSpeed) overSpeed
		,sum(overSpeedLong) overSpeedLong
		*/
		
		float a = new Float(suddenSummary.getRapidStart());
		float b = new Float(suddenSummary.getRapidStop());
		float c = new Float(suddenSummary.getRapidAccel());
		float d = new Float(suddenSummary.getRapidDeaccel());
		float e = new Float(suddenSummary.getRapidTurn());
		float f = new Float(suddenSummary.getRapidUtern());
		float g = new Float(suddenSummary.getOverSpeed());
		float h = new Float(suddenSummary.getOverSpeedLong());
		
		System.out.println("acc : " + a);
		System.out.println("decc : " + b);
		System.out.println("start : " + c);
		System.out.println("stop : " + d);
		System.out.println("turn : " + e);
		System.out.println("uTurn : " + f);
		System.out.println("overSpeed : " + g);
		System.out.println("overSpeedL : " + h);
		
		a = calcScoreTemp(a,pointSuddenStart,distanceKm);
		b = calcScoreTemp(b,pointSuddenStop,distanceKm);
		c = calcScoreTemp(c,pointSuddenAcc,distanceKm);
		d = calcScoreTemp(d,pointSuddenDecc,distanceKm);
		e = calcScoreTemp(e,pointSuddenTurn,distanceKm);
		f = calcScoreTemp(f,pointSuddenUturn,distanceKm);
		g = calcScoreTemp(g,pointOverSpeed,distanceKm);
		h = calcScoreTemp(h,pointOverSpeedLong,distanceKm);
		
		System.out.println("convert start : " + a);
		System.out.println("convert stop : " + b);
		System.out.println("convert acc : " + c);
		System.out.println("convert decc : " + d);
		System.out.println("convert turn : " + e);
		System.out.println("convert uturn : " + f);
		System.out.println("convert uturn : " + g);
		System.out.println("convert uturn : " + h);
		
		List<Float> arrOver = new ArrayList<Float>();
		List<Float> arrUnder = new ArrayList<Float>();
		List<Float> arrOverWeight = new ArrayList<Float>();
		List<Float> arrUnderWeight = new ArrayList<Float>();
			
		if(a >= 1){ 
			arrOver.add(a-1);
			arrOverWeight.add(weightSuddenStart);
		}else{
			arrUnder.add(a);
			arrUnderWeight.add(weightSuddenStart);
		}
		if(b >= 1){ 
			arrOver.add(b-1);
			arrOverWeight.add(weightSuddenStop);
		}else{
			arrUnder.add(b);
			arrUnderWeight.add(weightSuddenStop);
		}
		if(c >= 1){ 
			arrOver.add(c-1);
			arrOverWeight.add(weightSuddenAcc);
		}else{
			arrUnder.add(c);
			arrUnderWeight.add(weightSuddenAcc);
		}
		if(d >= 1){ 
			arrOver.add(d-1);
			arrOverWeight.add(weightSuddenDecc);
		}else{
			arrUnder.add(d);
			arrUnderWeight.add(weightSuddenDecc);
		}
		if(e >= 1){ 
			arrOver.add(e-1);
			arrOverWeight.add(weightSuddenTurn);
		}else{
			arrUnder.add(e);
			arrUnderWeight.add(weightSuddenTurn);
		}
		if(f >= 1){ 
			arrOver.add(f-1);
			arrOverWeight.add(weightSuddenUturn);
		}else{
			arrUnder.add(f);
			arrUnderWeight.add(weightSuddenUturn);
		}
		if(g >= 1){ 
			arrOver.add(g-1);
			arrOverWeight.add(weightOverSpeed);
		}else{
			arrUnder.add(g);
			arrUnderWeight.add(weightOverSpeed);
		}
		if(h >= 1){ 
			arrOver.add(h-1);
			arrOverWeight.add(weightOverSpeedLong);
		}else{
			arrUnder.add(h);
			arrUnderWeight.add(weightOverSpeedLong);
		}
		
		float totOver = 0;
		float totOverWeight = 0;
		float totOverPoint = 0;
		for(int i = 0 ; i < arrOver.size() ; i++){
			totOver += arrOver.get(i);
			totOverWeight += arrOverWeight.get(i);
			totOverPoint += arrOver.get(i)*arrOverWeight.get(i);			
		}
		
		System.out.println("totOver : " + totOver);
		System.out.println("totOverWeight : " + totOverWeight);
		System.out.println("totOverPoint : " + totOverPoint);
		
		float totUnderWeight = 0;
		for(int i = 0 ; i < arrUnder.size() ; i++){
			
			totUnderWeight += arrUnderWeight.get(i);
			
		}
		System.out.println("totUnderWeight : " + totUnderWeight);
		
		float totOverRatio = 1;
		if(totOverWeight != 0f){
			totOverRatio += (totOverPoint/totOverWeight);	
		}
		
		System.out.println("totOverRatio : " + totOverRatio);
		
		float removePointRatio = (100 - (100/totOverRatio))/100;
		
		System.out.println("100/totOverRatio : " + 100/totOverRatio);
		System.out.println("100 - (100/totOverRatio) : " + (100 - (100/totOverRatio)));
		System.out.println("removePointRatio : " + removePointRatio);
		
		float pulsPoint = totUnderWeight*removePointRatio;
		if(arrUnder.size()==0) pulsPoint = 0;
		else pulsPoint = pulsPoint / arrUnder.size();
		
		System.out.println("pulsPoint : "+pulsPoint);
		
		float finalMinusScore = 0;
		for(int i = 0 ; i < arrOver.size() ; i++){
			if(totOverPoint == 0f){
				finalMinusScore += arrOverWeight.get(i);
				System.out.println("arrOver"+i+" : " +arrOverWeight.get(i));
			}else{
				float eachPointPulsRatio = (arrOver.get(i)*arrOverWeight.get(i))/totOverPoint;
				arrOverWeight.set(i, arrOverWeight.get(i)+(pulsPoint*eachPointPulsRatio));
				finalMinusScore += arrOverWeight.get(i);
				System.out.println("arrOver"+i+" : " +arrOverWeight.get(i));
			}
		}
		
		float minusPoint = totUnderWeight-pulsPoint;
		System.out.println("minusPoint : " + minusPoint);
		
		for(int i = 0 ; i < arrUnder.size() ; i++){
			
			float eachPointUnderRatio = arrUnderWeight.get(i)/totUnderWeight;
			
			float fixedUnderPoint = minusPoint*eachPointUnderRatio;
			arrUnder.set(i, fixedUnderPoint*arrUnder.get(i)); 
			System.out.println("arrUnder"+i+" : " +arrUnder.get(i));
			
			finalMinusScore += arrUnder.get(i);
		}
		
		System.out.println("finalMinusScore : " + finalMinusScore);
		System.out.println("finalScore : " + (100 -finalMinusScore-scoreMidnightDriving));
		
		return (100 -finalMinusScore-scoreMidnightDriving);
	}

	private float calcScoreMidnight(long startDate,long endDate, long max) {
		
		Date sDate = new Date(startDate);
		Date eDate = new Date(endDate);
		
		long sTime = sDate.getTime();
		long eTime = eDate.getTime();
		
		long dsTime = new Date(eDate.getYear(),eDate.getMonth(),eDate.getDate()).getTime();
		long deTime = new Date(eDate.getYear(),eDate.getMonth(),eDate.getDate(),4,0).getTime();
		
		if(sTime <= deTime && eTime >= dsTime){
			if(sTime <= dsTime) sTime = dsTime;
			if(eTime >= deTime) eTime = deTime;
			
			long diff = eTime-sTime;
			return new Long(diff).floatValue()/new Long(max).floatValue();
			
		}else{
			return 0;	
		} 
		
	}
	
	private float calcScoreTemp(float point, float max, float distanceKm) {
		float sectionSize = distanceKm / 100;
		point /= sectionSize;
		float result = point / max;

		return result;
	}
}

