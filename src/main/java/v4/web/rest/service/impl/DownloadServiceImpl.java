package v4.web.rest.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.Map;

import org.jastecm.convert.ConvertUtil;
import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.common.Globals;
import com.util.CheckSumByteUtil;

import v4.ex.exc.BizException;
import v4.web.rest.dao.DownloadDAO;
import v4.web.rest.service.DeviceService;
import v4.web.rest.service.DownloadService;
import v4.web.rest.service.VehicleDBService;
import v4.web.vo.DeviceVO;
import v4.web.vo.DownloadVO;


@Service("downloadService")
public class DownloadServiceImpl implements DownloadService {

	public final static int BIG_EDIAN = 1;
	public final static int LITTLE_EDIAN = 2;
	
	@Autowired
	DownloadDAO downloadDao;
	
	@Autowired
	VehicleDBService vehicleDbService;
	
	@Autowired
	DeviceService deviceService;

	@Override
	public Map<Integer, byte[]> getFirmware(DeviceVO dvo) throws BizException {
		
		Map<Integer, byte[]> rtv = new HashMap<Integer,byte[]>();
		
		String fileNm = (StrUtil.isNullToEmpty(dvo.getAdminCorpNm())?"":dvo.getAdminCorpNm()+"_")+dvo.getDeviceSeries()+"-v";
		String rtvFileNm = dvo.getDeviceSeries()+"-v";
		
		if(dvo.getDeviceSeries().equals("von-S31")){
			fileNm += dvo.getS31()+".bin";
			rtvFileNm += dvo.getS31();
		}else if(dvo.getDeviceSeries().equals("von-F31")){
			fileNm += dvo.getF31()+".bin";
			rtvFileNm += dvo.getF31();
		}
		String strFile = "";
		
		strFile = Globals.FIRMWARE_LOCATION+fileNm;
		
		try {
			
			System.out.println("f/w path - " + strFile);
	
			File file = new File(strFile);
			FileInputStream fis = new FileInputStream(file);
	
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			byte[] buf = new byte[1024];
			try {
				for (int readNum; (readNum = fis.read(buf)) != -1;) {
					bos.write(buf, 0, readNum);
				}
			} catch (IOException ex) {
				System.out.println("read err");
	
			}
			byte[] bytes = bos.toByteArray();
	
			String version = new String(rtvFileNm);
			rtv.put(0, version.getBytes("UTF-8"));
			rtv.put(1, bytes);
			rtv.put(2, ConvertUtil.intToByteArray(CheckSumByteUtil.makeCheckSumFor4byte(bytes),Integer.SIZE/8,ByteOrder.LITTLE_ENDIAN));
																		
			return rtv;
	
		} catch (Exception e) {
			e.printStackTrace();
			return rtv;
		}
	}
	
	@Override
	public Map<Integer, byte[]> getVehicleDb(DeviceVO device) throws BizException {
		
		String path = downloadDao.getVehicleDbPathByConvCode(device.getConvCode());
		
		if(!StrUtil.isNullToEmpty(path)){
			//get mDb
			return getManupactureDb(path);
		}else{
			//get gDb
			return getGenelicDb(device);
		}
	}

	@Override
	public File getVehicleDbFile(DeviceVO vo) throws BizException {
		
		String path = downloadDao.getVehicleDbPathByConvCode(vo.getConvCode());
		
		if(!StrUtil.isNullToEmpty(path)){
			//get mDb
			return getManupactureDbFile(path);
		}else{
			//get gDb
			return getGenelicDbFile(vo);
		}
	}
	
	@Override
	public DownloadVO getDeviceSeriesVehicleDb(String deviceSn) throws BizException {
		return downloadDao.getDeviceSeriesVehicleDb(deviceSn);
	}
	
	@Override
	public DownloadVO getVer(DeviceVO device,String osType) throws BizException {
		
		DownloadVO rtv = new DownloadVO();
		
		
		DownloadVO vo2 = getDeviceSeriesVehicleDb(device.getDeviceSn());
		DownloadVO vo3 = getVerMng(device.getAdminCorp());
		
		String deviceSeries = vo2.getDeviceSeries();
		String convCode = vo2.getVehicleVer();
		
		String path = downloadDao.getVehicleDbPathByConvCode(convCode);
		
		if(!StrUtil.isNullToEmpty(path)){
			//get mDb
			String[] paths = path.split("\\\\");
			rtv.setVehicleVer(paths[paths.length-1].split("_")[1]);
			rtv.setDbType("M");
		}else{
			//get gDb
			if(deviceSeries.equals("von-S31")){
				rtv.setVehicleVer(vo3.getGenelicVer());
			}else if(deviceSeries.equals("von-F31")){
				rtv.setVehicleVer(vo3.getGenelicBusVer());
			}
			
			rtv.setDbType("G");
		}
		
		
		//appVer
		if(osType.toLowerCase().equals("ios")) rtv.setAppVer(vo3.getIos());
		else if(osType.toLowerCase().equals("android")) rtv.setAppVer(vo3.getAndroid());
		
		//firmVer
		if(deviceSeries.equals("von-S31")) rtv.setFirmware(deviceSeries+"-v"+vo3.getS31());
		else if(deviceSeries.equals("von-F31")) rtv.setFirmware(deviceSeries+"-v"+vo3.getF31());
		else rtv.setFirmware(null);
			
		
		return rtv;
	}
	
	@Override
	public String getWebViewVer() {
		return downloadDao.getWebViewVer();
	}
	@Override
	public void setWebViewVer(String ver) {
		downloadDao.setWebViewVer(ver);
	}
	
	private Map<Integer, byte[]> getManupactureDb(String path) {
		Map<Integer, byte[]> returnObject = new HashMap<Integer, byte[]>(0);
		
		try {
			path = path.replaceAll("\\\\", System.getProperty("file.separator"));
			String[] paths = path.split(System.getProperty("file.separator"));
			String strFileNm = paths[paths.length-1];
			
			File file = getManupactureDbFile(path);
			
			FileInputStream fis = new FileInputStream(file);
	
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			byte[] buf = new byte[1024];
			try {
				for (int readNum; (readNum = fis.read(buf)) != -1;) {
					bos.write(buf, 0, readNum);
				}
			} catch (IOException ex) {
				System.out.println("read err");
			}
			byte[] bytes = bos.toByteArray();
	
			String version = new String(strFileNm.split("_")[1]);
			returnObject = new HashMap<Integer, byte[]>(0);
			returnObject.put(0, version.getBytes("UTF-8"));
			returnObject.put(1, bytes);
			returnObject.put(2, ConvertUtil.intToByteArray(CheckSumByteUtil.makeCheckSumFor4byte(bytes) , Integer.SIZE/8 , ByteOrder.LITTLE_ENDIAN));
			
			
			
		}catch (Exception e) {
			e.printStackTrace();
			
		}
		
		
		return returnObject;
	}
	
	private File getManupactureDbFile(String path) {
		try {
			path = path.replaceAll("\\\\", System.getProperty("file.separator"));
			return new File(Globals.VEHICLE_DB_FILE_LOCATION+ path);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private Map<Integer, byte[]> getGenelicDb(DeviceVO device) throws BizException{
		
		Map<Integer, byte[]> returnObject = new HashMap<Integer, byte[]>(0);
		
		String rtvFileNm = "";
		
		if(device.getDeviceSeries().equals("von-S31"))
			rtvFileNm = device.getGenelicVer();
		else if(device.getDeviceSeries().equals("von-F31"))
			rtvFileNm = device.getGenelicBusVer();
		
		try {
			
			File file = getGenelicDbFile(device);
			FileInputStream fis = new FileInputStream(file);
	
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			byte[] buf = new byte[1024];
			try {
				for (int readNum; (readNum = fis.read(buf)) != -1;) {
					bos.write(buf, 0, readNum);
				}
			} catch (IOException ex) {
				System.out.println("read err");
			}
			byte[] bytes = bos.toByteArray();
	
			String version = new String(rtvFileNm);
			returnObject = new HashMap<Integer, byte[]>(0);
			returnObject.put(0, version.getBytes("UTF-8"));
			returnObject.put(1, bytes);
			returnObject.put(2, ConvertUtil.intToByteArray(CheckSumByteUtil.makeCheckSumFor4byte(bytes) , Integer.SIZE/8 , ByteOrder.LITTLE_ENDIAN));
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		
		return returnObject;
	}
	
	private File getGenelicDbFile(DeviceVO device) throws BizException{
		
		
		String fileNm = "";
		
		if(device.getDeviceSeries().equals("von-S31")){
			fileNm = "OBD2_"+device.getGenelicVer()+".dat";
		}else if(device.getDeviceSeries().equals("von-S31")){
			fileNm = device.getGenelicBusVer()+".dat";
		}
		
		try {
			String strFile = "";
			strFile = Globals.VEHICLE_DB_FILE_LOCATION+ "/vonsVehicleDb/GENERAL/"+fileNm;
			return new File(strFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	DownloadVO getVerMng(String adminCorp){
		return downloadDao.getVerMng(adminCorp);
	}
	
	
	
	//////////////////////////
	/*
	
	
	
	
	
	
	
	
	
	@Override
	public String getFirmwareVer(AdminCorpVO adminCorpVo,String deviceSeries) throws BizException {
		
		DownloadVO vo3 = getVerMng(adminCorpVo.getCorp());
		
		//firmVer
		if(deviceSeries.equals("von-S31")) return (deviceSeries+"-v"+vo3.getS31());
		else return null;
			
	}
	
	@Override
	public Map<Integer, byte[]> getVehicleDb(String deviceKey,AdminCorpVO adminCorpVo) throws BizException {
		DownloadVO vo = new DownloadVO();
		vo.setDeviceKey(deviceKey);
		DownloadVO vo2 = getDeviceSeriesVehicleDb(vo);
		DownloadVO vo3 = getVerMng(adminCorpVo.getCorp());
		
		String convCode = vo2.getVehicleVer();
		
		Map<Integer, byte[]> rtv = new HashMap<Integer,byte[]>();
		
		//vehicleVer
		String path = restDownloadDao.getVehicleDbPathByConvCode(convCode);
		
		if(!StrUtil.isNullToEmpty(path)){
			//get mDb
			String[] paths = path.split("\\\\");
			rtv = getManupactureDb(path,vo3);
		}else{
			//get gDb
			rtv = getGenelicDb(convCode,vo3);
		}
		return rtv;
	}
	



	@Override
	public Map<Integer, byte[]> getFirmware(String deviceKey,AdminCorpVO adminCorpVo) throws BizException {
		
		DownloadVO vo = new DownloadVO();
		vo.setDeviceKey(deviceKey);
		DownloadVO vo2 = getDeviceSeriesVehicleDb(vo);
		DownloadVO vo3 = getVerMng(adminCorpVo.getCorp());
		
		String deviceSeries = vo2.getDeviceSeries();
		
		return getFirmware(deviceSeries,vo3,adminCorpVo);
			
		
		
	}
	
	@Override
	public Map<Integer, byte[]> getFirmware(AdminCorpVO adminCorpVo,String deviceSeries) throws BizException {
		
		DownloadVO vo3 = getVerMng(adminCorpVo.getCorp());
		return getFirmware(deviceSeries,vo3,adminCorpVo);
			
	}
	
	@Override
	public Map<Integer, byte[]> getFirmwareBySeries(String series,AdminCorpVO adminCorpVo) throws BizException {
		
		DownloadVO vo3 = getVerMng(adminCorpVo.getCorp());
		
		return getFirmware(series,vo3,adminCorpVo);
			
		
		
	}
	
	
	
	@Override
	public Map<Integer, byte[]> getVehicleDbByConvCode(String convCode,AdminCorpVO adminCorpVo) throws BizException {
		
		DownloadVO vo3 = getVerMng(adminCorpVo);
		
		String path = restDownloadDao.getVehicleDbPathByConvCode(convCode);
		
		if(!StrUtil.isNullToEmpty(path)){
			//get mDb
			String[] paths = path.split("\\\\");
			return getManupactureDb(path,vo3);
		}else{
			//get gDb
			return getGenelicDb(convCode,vo3);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	
	
	public Map<Integer, byte[]> getFirmware(String deviceSeries,DownloadVO vo,AdminCorpVO adminCorpVo) throws BizException{
		Map<Integer, byte[]> rtv = new HashMap<Integer,byte[]>();
		
		
		String fileNm = (StrUtil.isNullToEmpty(adminCorpVo.getCorpNm())?"":adminCorpVo.getCorpNm()+"_")+deviceSeries+"-v";
		String rtvFileNm = deviceSeries+"-v";
		
		if(deviceSeries.equals("von-S31")){
			fileNm += vo.getS31()+".bin";
			rtvFileNm += vo.getS31();
		}
		String strFile = "";
		
		//von-S31-v1.12.bin
		
		strFile = Globals.FIRMWARE_LOCATION+fileNm;
		
		try {
			
			System.out.println("f/w path - " + strFile);
	
			File file = new File(strFile);
			FileInputStream fis = new FileInputStream(file);
	
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			byte[] buf = new byte[1024];
			try {
				for (int readNum; (readNum = fis.read(buf)) != -1;) {
					bos.write(buf, 0, readNum);
				}
			} catch (IOException ex) {
				System.out.println("read err");
	
			}
			byte[] bytes = bos.toByteArray();
	
			String version = new String(rtvFileNm);
			rtv.put(0, version.getBytes("UTF-8"));
			rtv.put(1, bytes);
			rtv.put(2, intToByteArray(util.makeCheckSumFor4byte(bytes),Integer.SIZE/8,LITTLE_EDIAN));
																		
			return rtv;
	
		} catch (Exception e) {
			e.printStackTrace();
			return rtv;
		}
		
	}
	

	final byte[] intToByteArray(int integer , int offset , int order){
		byte[] bytes = new byte[offset];
		for (int i = 0; i < offset; i++) {
			bytes[offset-1-i] = (byte)(integer >>> (i * 8));
		}
		return changeByteOrder(bytes, order);
	}
	protected final int BIG_EDIAN = 1;
    protected final int LITTLE_EDIAN = 2;
    
	final byte[] changeByteOrder(byte[] value,int Order){
    	int idx = value.length;
    	byte[]Temp = new byte[idx];
     
    	if(Order == BIG_EDIAN){
    		Temp = value;
    	}else if(Order ==LITTLE_EDIAN){
    		for(int i = 0 ; i < idx ; i ++){
    			Temp[i] = value[idx - (i+1)];
    		}
    	}
    	return Temp;
    }



	
	public void updateFirmVersion_S31(String version){
		restDownloadDao.updateFirmVersion_S31(version);
	}
	

	
	
	@Override
	public void updateVer(String adminCorp, String field, String val) {
		Map<String, String> param = new HashMap<String, String>();
		param.put("adminCorp", adminCorp);
		param.put("field", field);
		param.put("val", val);
		
		restDownloadDao.updateVer(param);
		
	}



	



	@Override
	public File getVehicleDb(String convCode) throws BizException {
		//vehicleVer
		String path = restDownloadDao.getVehicleDbPathByConvCode(convCode);
		
		if(!StrUtil.isNullToEmpty(path)){
			//get mDb
			String[] paths = path.split("\\\\");
			return getManupactureDb(path);
		}else{
			//get gDb
			return getGenelicDb();
		}
	}
*/
	
	
}

