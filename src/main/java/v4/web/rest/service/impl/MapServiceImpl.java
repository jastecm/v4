package v4.web.rest.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import v4.ex.exc.BizException;
import v4.web.rest.dao.MapDAO;
import v4.web.rest.service.MapService;
import v4.web.vo.SuddenSummaryVO;


@Service("mapService")
public class MapServiceImpl implements MapService {

	@Autowired
	MapDAO mapDao;
	/*
	@Autowired
	DrivingService drivingService;
	
	@Autowired
	WorkingAreaTimeService workingAreaTimeService;
	*/
	
	/*
	@Override
	public List<Map<String, Object>> getTripMapInfo_trip(UserVO user, String tripKey) throws BizException {
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("tripKey", tripKey);
		
		param.put("validTrip", "1");
		List<DrivingVO> tripInfo = drivingService.getDrivingList(user,param);
		
		Map<String,Object> rtvMap = new HashMap<String,Object>();
		List<MapVO> tdrList = null;
		if(tripInfo.size()>0){
			DrivingVO dvo = tripInfo.get(0);
			//if(!StrUtil.isNullToEmpty(dvo.getTripMatched()) && dvo.getTripMatched().equals("1")){
			if(true){
				tdrList = mapDao.getTdrListMatched(param);
				try {
					rtvMap = getTripMapInfo(tdrList,tripInfo.size()>0?tripInfo.get(0):null,false);
				} catch (JsonProcessingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{
				if(StrUtil.isNullToEmpty(user.getRule())) tdrList = mapDao.getTdrListRest(param);
				else tdrList = mapDao.getTdrList(param);
				try {
					rtvMap = getTripMapInfo(tdrList,tripInfo.size()>0?tripInfo.get(0):null,true);
				} catch (JsonProcessingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		List<MapVO> poiList = getFillterPoiList(tdrList);
			
		rtvMap.put("poi", poiList);
			
		rtvMap.remove("temp");
			
		rtvMap.put("tripKey",tripKey);
			
		List<Map<String, Object>> rtvList = new ArrayList<Map<String, Object>>();
		rtvList.add(rtvMap);
		
		return rtvList;
			
	}
	*/
	
	/*
	@Override
	public List<Map<String, Object>> getTripMapInfo_time(UserVO user, String vehicleKey, String accountKey, String time)
			throws BizException {
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("searchAccountKey", accountKey);
		param.put("vehicleKey", vehicleKey);
		param.put("searchRecentTime", time);
		param.put("searchOrderAsc", "startDate");
		param.put("validTrip", "1");
		
		List<DrivingVO> tripList = drivingService.getDrivingList(user,param);
		
		List<Map<String, Object>> tempList = new ArrayList<Map<String, Object>>();
		
		if(tripList.size() == 0){
			throw new BizException("운행내역이 없거나 GPS정보가 수집되지 않았습니다.");
		}
		
		
		for(DrivingVO vo : tripList){
			Map<String,Object> tempMap = new HashMap<String,Object>();
			
			param.clear();
			param.put("tripKey", vo.getTripKey());
			param.put("rule", user.getRule());
			param.put("ruleAccountKey", user.getAccountKey());
			
			DrivingVO tripInfo = vo;
			
			boolean fillter = !StrUtil.isNullToEmpty(tripInfo.getTripMatched()) && tripInfo.getTripMatched().equals("1");
			
			List<MapVO> tdrList = null;
			
			fillter = false;
			if(fillter) tdrList = mapDao.getTdrListMatched(param);
			else tdrList = mapDao.getTdrList(param);
			
			try {
				tempMap = getTripMapInfo(tdrList,tripInfo,fillter);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			tempMap.remove("temp");
			tempMap.put("tripKey" , vo.getTripKey());
			tempList.add(tempMap);
			
		}
		
		
		
		return tempList;
		
		//Map<String,Object> rtvMap = new HashMap<String,Object>();
		
	}
	
	*/
	
	/*
	@Override
	public List<Map<String, Object>> getTripMapInfo_date(UserVO user, String vehicleKey, String accountKey, String time)
			throws BizException {
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("searchAccountKey", accountKey);
		param.put("vehicleKey", vehicleKey);
		param.put("searchDate", time);
		param.put("searchOrderAsc", "startDate");
		param.put("validTrip", "1");
		
		List<DrivingVO> tripList = drivingService.getDrivingList(user,param);
		
		List<Map<String, Object>> tempList = new ArrayList<Map<String, Object>>();
		
		if(tripList.size() == 0){
			throw new BizException("운행내역이 없거나 GPS정보가 수집되지 않았습니다.");
		}
		
		for(DrivingVO vo : tripList){
			Map<String,Object> tempMap = new HashMap<String,Object>();
			
			param.clear();
			param.put("tripKey", vo.getTripKey());
			param.put("rule", user.getRule());
			param.put("ruleAccountKey", user.getAccountKey());
			
			boolean fillter = !StrUtil.isNullToEmpty(vo.getTripMatched()) && vo.getTripMatched().equals("1");
			
			List<MapVO> tdrList = null;
			
			//
			fillter = false;
			if(fillter) tdrList = mapDao.getTdrListMatched(param);
			else tdrList = mapDao.getTdrList(param);
			
			DrivingVO tripInfo = vo;
			
			try {
				tempMap = getTripMapInfo(tdrList,tripInfo,fillter);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			tempMap.remove("temp");
			tempMap.put("tripKey" , vo.getTripKey());
			tempList.add(tempMap);
			
		}
		
		return tempList;
		
	}
	
	*/
	
	/*
	private List<MapVO> getFillterPoiList(List<MapVO> tdrList) {
		List<MapVO> poiList = new ArrayList<MapVO>();
		
		if(tdrList==null||tdrList.size()==0) return poiList; 
		for(MapVO vo : tdrList){
			if(vo.getRapidAccel() + vo.getRapidDeaccel() + vo.getRapidStart() + vo.getRapidStop() + vo.getRapidTurn() + vo.getRapidUtern() + vo.getOverSpeed() + vo.getOverSpeedLong() > 0)
				poiList.add(vo);
		}
		
		return poiList;
	}

*/
	
	/*
	private Map<String,Object> getTripMapInfo(List<MapVO> rtvList,DrivingVO trip,boolean fillter) throws JsonProcessingException, BizException {
		Map<String,Object> rtv = new HashMap<String, Object>();
		
		if(trip == null){
			throw new BizException("운행내역이 없거나 GPS정보가 수집되지 않았습니다.");
		}else{
			
			List<Double[]> arrLonLat = getArrLonLatPoint(rtvList,trip);
			if(fillter)
				arrLonLat = filteringIrregularPoint(arrLonLat);
			
			return makeRtvLineMap(rtv,arrLonLat,trip);
		}
		
		
	}
	
	*/
	
	/*
	public List<Double[]> getArrLonLatPoint(List<MapVO> lvo,DrivingVO trip){
		List<Double[]> rtvVo = new ArrayList<Double[]>();
		
		//lvo = removeZeroPoint(lvo); 
		
		double prevLat = 0d;
		double prevLon = 0d;
		for(int i = 0 ; i < lvo.size() ; i++){
			MapVO vo = lvo.get(i);
			
			if(i == 0){
				prevLat = vo.getLat();
				prevLon = vo.getLon();
				
				rtvVo.add(new Double[]{prevLon,prevLat,(double)vo.getSeq()});
			}else{
				if(prevLat != vo.getLat() || prevLon != vo.getLon()) 
					rtvVo.add(new Double[]{vo.getLon(),vo.getLat(),(double)vo.getSeq()});
				prevLat = vo.getLat();
				prevLon = vo.getLon();
			}
		}
		
		if(rtvVo.size() > 0)
			rtvVo.set(0,new Double[]{Double.parseDouble(trip.getStartLon()),Double.parseDouble(trip.getStartLat()),(double)-1});
		else rtvVo.add(new Double[]{Double.parseDouble(trip.getStartLon()),Double.parseDouble(trip.getStartLat()),(double)-1});
		rtvVo.add(new Double[]{Double.parseDouble(trip.getEndLon()),Double.parseDouble(trip.getEndLat()),(double)-1});
		
		//if(encrypt)
		rtvVo = removeZeroPoint(rtvVo); 
		
		return rtvVo;
	}
	*/
	
	/*
	private List<Double[]> removeZeroPoint(List<Double[]> lvo) {
		List<Double[]> rtv = new ArrayList<Double[]>();
		for(Double[] vo : lvo){
			if(vo[0] != 0d || vo[1] != 0d)
				rtv.add(vo);
		}
			
		return rtv;
	}
*/
	
	/*
	public List<Double[]> filteringIrregularPoint(List<Double[]> lonLat){
		List<Double[]> rtv = new ArrayList<Double[]>();
		int gpxAccuracy = 7000;
		
		for(int i = 0 ; i < lonLat.size() ; i++){
			Double[] prevVo = null;
			Double[] currentVo = null;
			Double[] nextVo = null;
			double distance = 0;
			double distanceNext = 0;
			if(i == 0){
				currentVo = lonLat.get(i);
				if(i+1 < lonLat.size()){
					nextVo = lonLat.get(i+1);
				}
				if(nextVo != null){
					distance = GeoUtil.calcDist(
							currentVo[1]
							, currentVo[0]
							, nextVo[1]
							, nextVo[0]);
				}
				if(distance < gpxAccuracy){
					rtv.add(currentVo);
				}
			}else if(i == lonLat.size()-1){
				currentVo = lonLat.get(i);
				if(i-1 > 0){
					prevVo = lonLat.get(i-1);
				}
				if(prevVo != null){
					distance = GeoUtil.calcDist(
							currentVo[1]
							,currentVo[0]
							,prevVo[1]
							,prevVo[0]);
				}
				if(distance < gpxAccuracy){
					rtv.add(currentVo);
				}
			}else{
				currentVo = lonLat.get(i);
				prevVo = lonLat.get(i-1);
				nextVo = lonLat.get(i+1);
				distance = GeoUtil.calcDist(
						currentVo[1]
						,currentVo[0]
						,prevVo[1]
						,prevVo[0]);
				distanceNext = GeoUtil.calcDist(
						currentVo[1]
						, currentVo[0]
						, nextVo[1]
						, nextVo[0]);
				
				if(distance < gpxAccuracy || distanceNext < gpxAccuracy ){
					rtv.add(currentVo);
				}
			}
		}
		
		return rtv;
	}
	*/
	
	/*
	public Map<String,Object> makeRtvLineMap(Map<String,Object> rtv,List<Double[]> ld , DrivingVO trip) throws JsonProcessingException {
		LineString lLineString = new LineString() ;
		FeatureCollection featureCollection = new FeatureCollection();
		Feature theFeature = new Feature() ;
		featureCollection.add(theFeature);
		
		
		for(Double[] lnglat : ld){
			lLineString.add(new LngLatAlt(lnglat[0],lnglat[1]));
		}
		
		rtv.put("temp",ld);
		
		theFeature.setGeometry(lLineString);
		String json = new ObjectMapper().writeValueAsString(featureCollection);
		
		rtv.put("geoJson", json);
		
		return rtv;
	}
	
	*/
	
	/*

	@Override
	public List<Map<String, Object>> getVehicleLocation(UserVO user, Map<String, Object> param) throws BizException {
		param.put("corp", user.getCorp() );		
		return mapDao.getVehicleLocation(param);
	}
*/
	
	/*
	@Override
	public SuddenSummaryVO getTripSuddenSummary(List<String> tripKeys) throws BizException {
		return mapDao.getTripSuddenSummary(tripKeys);
	}
*/
	/*
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public boolean mapMatchedResult(String tripKey,String corp) throws Exception{

		Map<String,Object> param = new HashMap<String,Object>();
		param.put("tripKey", tripKey);
		
		Map<String,Object> rtvMap = new HashMap<String,Object>();
		List<MapVO> tdrList = mapDao.getTdrListRest(param);
		
		UserVO user = new UserVO();
		user.setCorp(corp);
		user.setLang("ko");
		List<DrivingVO> tripInfo = drivingService.getDrivingList(user,param);
		
		if((tripInfo.size()>0?tripInfo.get(0):null) != null && tripInfo.get(0).getTripMatched().equals("1")) return false;
				
		try {
			rtvMap = getTripMapInfo(tdrList,tripInfo.size()>0?tripInfo.get(0):null,true);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BizException e2){
			System.out.println("운행내역이 없거나 gps Off");
			return false;
		}
		
		List<Double[]> ld = (List<Double[]>)rtvMap.get("temp");
		System.out.println("ld size - "+ld.size());
		String[] splitPoint = new String[(int)(ld.size()/100)+1];
		String pointStr = "";
		for(int i = 0 ,j = 0; i <ld.size() ; i++){
			
			if(i%100==0 && i!=0) {
				System.out.println(pointStr);
				splitPoint[j] = pointStr;
				j++;
			}
			if(i%100!=0 && i==ld.size()-1) {
				System.out.println(pointStr);
				splitPoint[j] = pointStr;
				j++;
			}
			
			if(i%100==0)
				pointStr = ld.get(i)[0]+","+ld.get(i)[1];
			else pointStr += "|"+ld.get(i)[0]+","+ld.get(i)[1];
		}
		
		System.out.println(splitPoint);
		
		JSONObject[] matchedResult = null;
		try{
			matchedResult = callSkRoadApi(splitPoint);
		}catch(IOException e){
			matchedResult = null;
		}catch(JSONException e2){
			matchedResult = null;
		}
		
		if(matchedResult != null && matchedResult.length > 0){
			for(int i = 0 ; i < matchedResult.length ; i++){
				
				JSONObject jObj = matchedResult[i];
				if(jObj.has("matchedPoints")){
					JSONArray matchedPoints = jObj.getJSONArray("matchedPoints");
					
					for(int j = 0 ; j < matchedPoints.length() ; j++){
						
						JSONObject obj = matchedPoints.getJSONObject(j);
						
						int seq = obj.has("sourceIndex")?obj.getInt("sourceIndex"):-1;
						if(seq != -1){
							Double[] orgLd = ld.get(seq+(i*100));
							
							int orgSeq = orgLd[2].intValue();
							
							Map<String,Object> updateTdrParam = new HashMap<String,Object>();
							updateTdrParam.put("seq", orgSeq);
							updateTdrParam.put("tripKey", tripKey);
							updateTdrParam.put("matchedLat", obj.getJSONObject("matchedLocation").getDouble("latitude"));
							updateTdrParam.put("matchedLon", obj.getJSONObject("matchedLocation").getDouble("longitude"));
							updateTdrParam.put("matchedSpeed", obj.getInt("speed"));
							updateTdrParam.put("matchedRoadCategory", obj.getInt("roadCategory"));
							
							mapDao.insertMatchedTdr(updateTdrParam);
						}else{
							
							Map<String,Object> updateTdrParam = new HashMap<String,Object>();
							updateTdrParam.put("tripKey", tripKey);
							updateTdrParam.put("matchedLat", obj.getJSONObject("matchedLocation").getDouble("latitude"));
							updateTdrParam.put("matchedLon", obj.getJSONObject("matchedLocation").getDouble("longitude"));
							updateTdrParam.put("matchedSpeed", obj.getInt("speed"));
							updateTdrParam.put("matchedRoadCategory", obj.getInt("roadCategory"));
							
							mapDao.insertMatchedTdr(updateTdrParam);
						}
					}
	            }else{
	            	throw new Exception("matchedPoints not found - result index : "+i);
				}
			}
		}else{
			throw new Exception("matchedResult fail");
		}
		
		return true;
	}
*/
	
	/*
	JSONObject[] callSkRoadApi(String[] splitPoint) throws IOException, JSONException{
		
		String url = "https://api2.sktelecom.com/tmap/road/matchToRoads";
		String method = "POST";
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("appKey", "27f53640-e198-4550-a075-70f96a5fc34f");
		param.put("version", "1");
		param.put("responseType", "1");
		
		JSONObject[] matchedResult = new JSONObject[splitPoint.length];
		for(int i = 0 ; i < splitPoint.length ; i++){
			param.put("coords", splitPoint[i]);
			
			Map<String,String> rtv = call(url,method,param);
			System.out.println(rtv.toString());
			if(rtv.get("resCode").equals("200")){
				
				JSONObject jObj = new JSONObject(rtv.get("resMsg"));
				
				if(jObj.has("resultData")){
					jObj = jObj.getJSONObject("resultData");
					if(jObj.has("header")){
						JSONObject header = jObj.getJSONObject("header");
						if(header.length() == 0) return null;
						else matchedResult[i] = jObj;
					}else{
						return null;
					}
				}else{
					return null;
				}
			}else{
				return null;
			}
			
		}
		
		return matchedResult;
	}
	
	*/
	
	/*
	Map<String,String> call(String url , String method , Map<String,Object> param) throws IOException{
        
		Map<String,String> rtvObj = new HashMap<String,String>();
		rtvObj.put("resCode",null);
		rtvObj.put("resMsg",null);
		URL obj = new URL(url);
		
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setDoInput(true);
		con.setDoOutput(true);
		
		// optional default is GET
		con.setRequestMethod(method);
		
		
		//add request header			
		con.setRequestProperty("Content-Type","application/x-www-form-urlencoded");  
		con.setRequestProperty("Accept-Charset", "UTF-8");
		
		ObjectMapper mapper = new ObjectMapper();
		String bodyStr = "1=1";
		if(param != null){
			
			Iterator<String> paramKey = param.keySet().iterator();
            
			while (paramKey.hasNext()) {
				String key = (String) paramKey.next();
				String value = URLEncoder.encode((String)param.get(key), "UTF-8");
				bodyStr += "&"+key+"="+value;
			}
			
			byte[] outputInBytes = bodyStr.getBytes("UTF-8");
			OutputStream os = con.getOutputStream();
			os.write( outputInBytes );    
			os.close();
		}
		
		System.out.println("Sending \"+method+\" request to URL : " + url);
		System.out.println(bodyStr);
		
		int responseCode = con.getResponseCode();
		
		System.out.println("Response Code : " + responseCode);
		rtvObj.put("resCode",String.valueOf(responseCode));
		
		if(responseCode < 300){
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream(),"UTF-8"));
			String inputLine;
			StringBuffer response = new StringBuffer();
	
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			rtvObj.put("resMsg",response.toString());
		}else{
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getErrorStream(),"UTF-8"));
			String inputLine;
			StringBuffer response = new StringBuffer();
	
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			rtvObj.put("resMsg",response.toString());
		}
			
		
		return rtvObj;
	}
	*/
}

