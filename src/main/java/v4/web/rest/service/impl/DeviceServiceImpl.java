package v4.web.rest.service.impl;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.jastecm.convert.ConvertUtil;
import org.jastecm.lora.vo.extention.LoraMsg_FAULT;
import org.jastecm.lora.vo.extention.LoraMsg_HELLO;
import org.jastecm.lora.vo.extention.LoraMsg_PROVISION;
import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import v4.ex.exc.BizException;
import v4.web.rest.dao.DeviceDAO;
import v4.web.rest.service.DeviceService;
import v4.web.vo.DeviceVO;



@Service("deviceService")
public class DeviceServiceImpl implements DeviceService {

	@Autowired
	DeviceDAO deviceDao;

	@Override
	public DeviceVO getDeviceFromSn(String deviceSn) throws BizException {
		try {
			return deviceDao.getDeivceFromSn(deviceSn);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public DeviceVO getDeviceFromId(String deviceId) throws BizException {
		try {
			return deviceDao.getDeivceFromId(deviceId);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public DeviceVO getDeviceFromKey(String deviceKey) throws BizException {
		try {
			return deviceDao.getDeivceFromKey(deviceKey);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public DeviceVO getActiveDeviceChkFromId(String deviceId) throws BizException {
		try {
			return deviceDao.getActiveDeviceChkFromId(deviceId);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public DeviceVO getActiveDeviceChkFromSn(String deviceSn) throws BizException {
		try {
			return deviceDao.getActiveDeviceChkFromSn(deviceSn);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public DeviceVO activeDevice(DeviceVO vo) throws BizException {
		
		try {
			deviceDao.setDeviceCorpKey(vo);
			
			DeviceVO hvo = getHstrDeviceFromId(vo.getCorpKey(),vo.getDeviceId());
			if(hvo != null && !StrUtil.isNullToEmpty(hvo.getDeviceKey())){
				deviceDao.updateHstrDeviceAccount(vo);
				vo.setDeviceKey(hvo.getDeviceKey());
			}else{
				
				deviceDao.createHstrDevice(vo);				
			}
			
			
			
			return vo;
			
		} catch (Exception e) {
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override 
	public DeviceVO getHstrDeviceFromId(String corpKey, String deviceId)  throws BizException  {
		
		Map<String, Object> param = new HashMap<String, Object>();
		
		param.put("corpKey",corpKey);
		param.put("deviceId",deviceId);
		
		try {
		
			return deviceDao.getHstrDeviceFromId(param); 
		
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public void updateConnectionKey(String deviceId, String connectionKey) throws BizException {
		
		Map<String, Object> param = new HashMap<String, Object>();
		
		param.put("connectionKey",connectionKey);
		param.put("deviceId",deviceId);
		
		try {
		
			deviceDao.updateConnectionKey(param); 
		
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public void updateDeviceShiftDay(String deviceId, long shiftDay) throws BizException {
		Map<String, Object> param = new HashMap<String, Object>();
		
		param.put("deviceId",deviceId);
		param.put("shiftDay",shiftDay);
		
		try {
		
			deviceDao.updateDeviceShiftDay(param); 
		
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public String getDeviceShiftDay(String deviceId) throws BizException {
		
		try {
		
			return deviceDao.getDeviceShiftDay(deviceId); 
		
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public void setDeviceHello(LoraMsg_HELLO vo) throws BizException {
		try {
			Map<String,Object> param = new HashMap<String,Object>();
			
			param.put("deviceId", vo.getDevice());
			param.put("helloFwVer", vo.getFwVer());
			param.put("helloDbVer", vo.getDbVer());
			param.put("helloCylinder", vo.getCylinder());
			param.put("helloVolume", vo.getVolume());
			param.put("helloFuelType", vo.getEngFuelType());
			param.put("helloFuel", vo.getFuel()/100);
			param.put("helloEngType", vo.getEngType());
			param.put("helloEUI", vo.getEui());
			param.put("helloCurTime", vo.getCurTimeDate());
			
			
			String vin = vo.getVin(); 
			try {
				vin = new String(ConvertUtil.hexToByteArray(vin),"UTF-8");
			} catch (UnsupportedEncodingException e) {
				
			}
			
			param.put("helloVIN", vo.getVin());
			
			deviceDao.setDeviceHello(param); 
			
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}

	@Override
	public void setDeviceProvision(LoraMsg_PROVISION vo) throws BizException {
		try {
			Map<String,Object> param = new HashMap<String,Object>();
			
			param.put("deviceId", vo.getDevice());
			param.put("provisionDevEui", vo.getDeviceEui());
			param.put("provisionAppEui", vo.getApEui());
			param.put("provisionAppKey", vo.getApKey());
			param.put("provisionSN", vo.getSerialNumber());
			param.put("provisionConverSN", vo.getConvertedSn());
			
			deviceDao.setDeviceProvision(param); 
			
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
		
	}

	@Override
	public void setDeviceFault(LoraMsg_FAULT vo) throws BizException {
		try {
			Map<String,Object> param = new HashMap<String,Object>();
			
			param.put("deviceId", vo.getDevice());
			param.put("faultCode", vo.getFaultCode());
			param.put("faultType", vo.getFaultType());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			param.put("faultTime", sdf.format(new Date(vo.getTime())));
			
			deviceDao.setDeviceFault(param); 
			
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}
	
	@Override
	public boolean isDeviceMaster(String deviceKey, String accountKey) throws BizException {
		//TODO 개인 = 활성화시킨사람 or 관리권한있을경우
		//TODO 법인 = 관리자권한있을경우
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("deviceKey", deviceKey);
		param.put("accountKey", accountKey);
		int chk = deviceDao.isDeviceMaster(param);
		return chk==1;
	}

	@Override
	public void updateDownloadInfo(Map<String, Object> param) throws BizException {
		try {
			
			deviceDao.updateDownloadInfo(param); 
			
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}
	
	@Override
	public void updateLatestPacket(String deviceId) throws BizException {
		try {
			deviceDao.updateLatestPacket(deviceId);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
}

