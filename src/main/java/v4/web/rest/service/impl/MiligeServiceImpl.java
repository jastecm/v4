package v4.web.rest.service.impl;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import v4.ex.exc.BizException;
import v4.web.rest.service.InsureService;
import v4.web.rest.service.MailBoxService;
import v4.web.rest.service.MiligeService;
import v4.web.rest.service.PushService;
import v4.web.rest.service.PushType;
import v4.web.rest.service.TimeLineService;
import v4.web.rest.service.VehicleService;
import v4.web.vo.InsureVO;
import v4.web.vo.data.TripVO;


@Service("miligeService")
public class MiligeServiceImpl implements MiligeService {
	
	@Autowired
	private TimeLineService timeLineService;
	
	@Autowired
	private PushService pushService;
	
	@Autowired
	private VehicleService vehicleService;
	
	@Autowired
	private InsureService insureService;

	@Autowired
	MailBoxService mailBoxService;
	
	/*
	@Autowired
	private RestAllocateService restAllocateService;
	*/
	
	@Override
	public void updateTotDistance(TripVO vo) throws BizException {
		
		InsureVO invo = insureService.getInsure("", vo.getVehicle().getVehicleKey());
		
		if(invo != null && invo.getInsureState().equals("1")){
			long tripTime = vo.getEndDate().getTime();
			if(tripTime > invo.getStartDate() && tripTime < invo.getEndDate() ){
				
				double firstCut = (invo.getInsureDistanceMax() - (invo.getInsureDistatnce()*0.1))*1000d;
				double lastCut = invo.getInsureDistanceMax()*1000d;
				
				String pushMsg = "%s 차량의 보험 마일리지가 초과되었습니다.";
				String title = "보험 마일리지";
				String timeLineType = "milige";
				String timeLineSubType = "empty";
				String pushType = PushType.PUSHTYPE_INSURE_MILIGE;
				String accountKey = vo.getVehicle().getCorp().getCorpType().equals("1")?vo.getVehicle().getDefaultAccountKey():vo.getVehicle().getManagerKey();
				Map<String,Object> param = new HashMap<String,Object>();
				
				param.put("vehicleKey", vo.getVehicle().getVehicleKey());
				param.put("eventTime", vo.getEndDate().getTime());
				param.put("eventValue", "");
				
				boolean sendChk = false;
				
				if(lastCut > (double)vo.getVehicle().getTotDistance() && lastCut < (double)vo.getTotDistance()){

					pushMsg = String.format(pushMsg, vo.getVehicle().getPlateNum());
					sendChk = true;
				}else if(firstCut > (double)vo.getVehicle().getTotDistance() && firstCut < (double)vo.getTotDistance()){
					param.put("eventValue", String.valueOf(new Double(firstCut/1000).intValue()));
					timeLineSubType = "warning";
					int remain = invo.getInsureDistanceMax()-(vo.getTotDistance()/1000);
						
					pushMsg = "%s 차량의 보험 마일리지가 %s km 남았습니다.";
					pushMsg = String.format(pushMsg, vo.getVehicle().getPlateNum(),remain);
					sendChk = true;
				}
				
				if(sendChk){
					timeLineService.insertTimeLineEvent(timeLineType,timeLineSubType, param);
					pushService.pushChkAccept(pushType, accountKey, "", title, pushMsg);
					pushService.insertPushHstr(vo.getVehicle().getCorp().getCorpKey(), pushType, title, pushMsg);
					long mailExpired = 1000*60*60*24*7L;						
					mailBoxService.simgleUserMailBoxInsert(accountKey, title, pushMsg, mailExpired);
				}
				
			}
			  
		}
		
		vehicleService.updateTotDistance(vo);
		
	}

	

	
	
}

