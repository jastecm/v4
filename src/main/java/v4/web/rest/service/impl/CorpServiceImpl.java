package v4.web.rest.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import v4.ex.exc.BizException;
import v4.web.rest.dao.CorpDAO;
import v4.web.rest.service.CorpService;
import v4.web.vo.AccountVO;
import v4.web.vo.ApprovalConfigVO;
import v4.web.vo.CorpVO;
import v4.web.vo.CreateApprovalConfigVO;
import v4.web.vo.CreateCorpVO;
import v4.web.vo.CreatePartnerVO;
import v4.web.vo.CreatePersonVO;
import v4.web.vo.CreateRentVO;


@Service("corpService")
public class CorpServiceImpl implements CorpService {

	@Autowired
	CorpDAO corpDao;

	@Override
	public boolean createCorp(CreateCorpVO vo) throws BizException {
		try {
			corpDao.createCorp(vo);
			
			return true;
		}catch (Exception e) {
			
			if (e instanceof DataAccessException) {
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public boolean createCorp(CreatePersonVO vo) throws BizException {
		try {
			corpDao.createCorpPerson(vo);
			
			return true;
		}catch (Exception e) {
			
			if (e instanceof DataAccessException) {
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public boolean createCorp(CreateRentVO vo) throws BizException {
		try {
			corpDao.createCorpRent(vo);
			
			return true;
		}catch (Exception e) {
			
			if (e instanceof DataAccessException) {
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public boolean createPartner(CreatePartnerVO vo) throws BizException {
		try {
			corpDao.createPartner(vo);
			
			return true;
		}catch (Exception e) {
			
			if (e instanceof DataAccessException) {
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public CorpVO getCorp(String corpKey, String corpType) throws BizException {
		try {
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("searchCorpKey" , corpKey);
			param.put("searchCorpType", corpType);
			
			return corpDao.getCorp(param);
		}catch (Exception e) {
			
			if (e instanceof DataAccessException) {
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}

	@Override
	public void changeCorpVehicleManager(Map<String, Object> param) {
		// TODO Auto-generated method stub
		corpDao.changeCorpVehicleManager(param);
	}

	@Override
	public AccountVO getCorpVehicleManager(Map<String, Object> param) {
		// TODO Auto-generated method stub
		return corpDao.getCorpVehicleManager(param);
	}

	@Override
	public void updateApprovalConfig(CreateApprovalConfigVO createApprovalConfigVo) {
		corpDao.updateApprovalConfig(createApprovalConfigVo);
		
	}

	@Override
	public ApprovalConfigVO getApprovalConfig(Map<String, Object> param) {
		return corpDao.getApprovalConfig(param);
	}

	
}

