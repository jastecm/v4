package v4.web.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import v4.ex.exc.BizException;
import v4.web.rest.dao.AllocateDAO;
import v4.web.rest.dao.VehicleDAO;
import v4.web.rest.service.AllocateService;
import v4.web.vo.CreateAllocateVO;

@Service("allocateService")
public class AllocateServiceImpl implements AllocateService {
	
	@Autowired
	AllocateDAO allocateDao;
	
	@Autowired
	VehicleDAO vehicleDao;

	@Override
	public CreateAllocateVO createAlloccate(CreateAllocateVO vo) throws BizException {
		try {
			allocateDao.createAllocate(vo);
			return vo;
		}catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void deleteAllocate(String allocateKey) throws BizException {
		try {
			allocateDao.deleteAllocate(allocateKey);
		}catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}
	/*
	@Override
	public AllocateVO getMyAllocate(String userKey) throws BizException {
		try {
			return allocateDao.getMyAllocate(userKey);
		}catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public List<VehicleVO> getAllocateListBeCan(AllocateVO vo) throws BizException {
		List<VehicleVO> rtv = vehicleDao.getAllocateListBeCan(vo);
		
		return rtv;
	}

	

	@Override
	public CreateAllocateVO createAlloccateApproval(CreateAllocateVO vo) throws BizException {
		try {
			allocateDao.createAlloccateApproval(vo);
			return vo;
		}catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	

	@Override
	public void updateAllocateApprovalComplate(String allocateKey) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("allocateKey", allocateKey);
		
		allocateDao.updateAllocateApprovalComplate(param);
		
	}

	@Override
	public AllocateVO getAllocate(AllocateVO chker) {
		// TODO Auto-generated method stub
		return allocateDao.getAllocate(chker);
	}

	@Override
	public LocationVO allocateReleaseChk(AllocateVO chker) throws BizException {
		// TODO Auto-generated method stub
		try {
			return allocateDao.allocateReleaseChk(chker);
		}catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public void allocateReturn(String allocateKey, String location, String locationAddr, String locationAddrDetail) throws BizException {
		try {
			Map<String, String> param = new HashMap<String, String>();
			param.put("allocateKey", allocateKey);
			param.put("loc", location);
			param.put("addr", locationAddr);
			param.put("addrDetail", locationAddrDetail);
			allocateDao.allocateReturn(param);
		}catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public HashMap<String, String> getNextAllocate(Map<String, Object> param) {
		return allocateDao.getNextAllocate(param);
	}

	@Override
	public void updateAllocateExtend(Map<String, Object> param) {
		allocateDao.updateAllocateExtend(param);
	}

	@Override
	public AllocateVO getAllocateApproval(Map<String, Object> param) {
		// TODO Auto-generated method stub
		return allocateDao.getAllocateApproval(param);
	}

	@Override
	public void updateApprovalState(String allocateKey, String accountKey, String step) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("allocateKey", allocateKey);
		param.put("accountKey", accountKey);
		param.put("step", step);
		
		allocateDao.updateApprovalState(param);
		
	}

	@Override
	public void updateInstantAllocateApprovalComplate(String allocateKey) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("allocateKey", allocateKey);
		
		allocateDao.updateInstantAllocateApprovalComplate(param);
		
	}

	@Override
	public void updateApprovalReject(String allocateKey, String accountKey, String step) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("allocateKey", allocateKey);
		param.put("accountKey", accountKey);
		param.put("step", step);
		
		allocateDao.updateApprovalState(param);
	}

	@Override
	public void updateInstantAllocateApprovalReject(String allocateKey) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("allocateKey", allocateKey);
		
		allocateDao.updateInstantAllocateApprovalReject(param);
		
	}

	@Override
	public void updateApprovalRejectAllocateKeyUpdate(String allocateKey) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("allocateKey", allocateKey);
		
		allocateDao.updateApprovalRejectAllocateKeyUpdate(param);
		
	}

	@Override
	public void returnAllocate(String allocateKey) throws BizException {
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("searchAllocatekey", allocateKey);
		param.put("corp", "1");
		
		
		
		List<Map<String,String>> drivingList = allocateDao.getDrivingList(param);
		AllocateVO avo = new AllocateVO();
		avo.setAllocateKey(allocateKey);
		
		allocateDao.deleteAllocate(avo);
		
		for(int i = 0 ; i < drivingList.size() ; i++){
			Map<String,String> vo = drivingList.get(i);
			TripVO chker = allocateDao.getTrip(vo.get("tripKey"));
			String newAllocateKey = this.getAllocateMatchedResult(chker, true).getAllocateKey();
			//tripkey 을 가지고 trip테이블에 새로운 allocatekey로 업데이트
			Map<String,Object> param2 =new HashMap<String,Object>();
			param2.put("allocateKey", newAllocateKey);
			param2.put("tripKey", vo.get("tripKey"));
			
			allocateDao.updateTripAllocateKey(param2);
			
		}
		
	}
	@Override
	public AllocateVO getAllocateMatchedResult(TripVO vo,boolean insert) throws BizException {
		
		Map<String, Object> param = new HashMap<String,Object>();
		param.put("searchVehicleKey", vo.getVehicleKey());
		VehicleVO vvo = vehicleDao.getVehicle(param);
		
		AllocateVO avo = allocateDao.getAllocateMatchedResult(vo);
		
		if(avo != null) 
			return avo;
		else {
			avo = new AllocateVO();
			
			avo.setFixedType(vvo.getFixed());
			
			if(vvo.getFixed().equals("1")){
				avo.setInstantAllocate("0");
				avo.setAccountKey(vvo.getFixedAccountKey());
				avo.setReportFlag(vvo.getAutoReport());
				
				if(vvo.getAutoReport().equals("1")){
					avo.setPurpose(vvo.getAutoReportPurpose());
					avo.setTitle(vvo.getAutoReportContents());
				}
				
			}else {
				avo.setInstantAllocate("1");
				//TODO 공용차량이면 manager 부서있으면 팀장
				avo.setAccountKey(vvo.getVehicleManager());
				avo.setPurpose(vvo.getAutoPurpose());
				avo.setTitle("");
				avo.setReportFlag("0");
			}
			
			avo.setVehicleKey(vo.getVehicleKey());
			avo.setCorpKey(vvo.getCorpKey());
			avo.setStartDate(vo.getStartDate());
			avo.setEndDate(vo.getEndDate());
			avo.setContents("");
			
			if(insert) allocateDao.insertInstantAllocate(avo);
			
			return avo;
		}
		
	}

	@Override
	public List<AllocateVO> getAlocateList(AccountVO account, Map<String, Object> param) {
		Util.setCurrentInfoToMap(account,param);
		
		return allocateDao.getAlocateList(param);
	}
	
	public String getVehicleAllocateUser(String vehicleKey) throws BizException {
		try {
			return allocateDao.getVehicleAllocateUser(vehicleKey);
		}catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
*/
}
