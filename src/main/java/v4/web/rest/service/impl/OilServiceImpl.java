package v4.web.rest.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import v4.ex.exc.BizException;
import v4.web.rest.dao.OilDAO;
import v4.web.rest.service.OilService;
import v4.web.vo.OilVO;



@Service("oilService")
public class OilServiceImpl implements OilService {
	
	@Autowired
	private OilDAO oilDao;

	@Override
	public OilVO getUnitOilPrice(String date) throws BizException {
		OilVO dateOil = oilDao.getUnitOilPrice(date);
		if(dateOil == null){
			String today = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			today = "2017-06-12";
			dateOil = oilDao.getUnitOilPrice(today);
		}
		
		return dateOil;
	}
	public void insertOilPrice(OilVO vo) throws Exception{
		if(oilPriceChk(vo)) oilDao.insertOilPrice(vo);
	}
	
	public boolean oilPriceChk(OilVO vo) throws Exception{
		
		int chk = oilDao.oilPriceChk(vo);
		if(chk > 0) return false;
		else return true;
	}
	
}

