package v4.web.rest.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import v4.ex.exc.BizException;
import v4.web.rest.dao.TripDAO;
import v4.web.rest.service.TripService;


@Service("tripService")
public class TripServiceImpl implements TripService {

	@Autowired
	TripDAO tripDao;

	@Override
	public void updatePurpose(String tripKey, String purpose) throws BizException {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("tripKey", tripKey);
		param.put("purpose", purpose);
		tripDao.updatePurpose(param);
		
	}
	
	@Override
	public void updatePurposeCompareDay(String tripKey, String purpose) throws BizException {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("tripKey", tripKey);
		param.put("purpose", purpose);
		tripDao.updatePurposeCompareDay(param);
		
	}	
}

