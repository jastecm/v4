package v4.web.rest.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import v4.ex.exc.BizException;
import v4.web.rest.dao.InsureDAO;
import v4.web.rest.service.InsureService;
import v4.web.vo.InsureVO;


@Service("insureService")
public class InsureServiceImpl implements InsureService {
	
	@Autowired
	InsureDAO restInsureDao;

	@Override
	public InsureVO getInsure(String lang, String vehicleKey) throws BizException {
		Map<String, Object> param = new HashMap<String, Object>();
		
		param.put("lang",lang);
		param.put("vehicleKey", vehicleKey);
		
		return restInsureDao.getInsure(param);
	}

	@Override
	public void createInsure(InsureVO insureVo) throws BizException {
		restInsureDao.createInsure(insureVo);
		
	}

	@Override
	public void updateInsure(InsureVO insureVo) throws BizException {
		restInsureDao.updateInsure(insureVo);
		
	}

	@Override
	public void deleteInsure(String vehicleKey) throws BizException {
		restInsureDao.deleteInsure(vehicleKey);
		
	}

	@Override
	public List<Map<String,String>> getMiligeWaringList(int remainDay) throws BizException {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("remainDay", remainDay);
		
		return restInsureDao.getMiligeWaringList(param);
	}
}
