package v4.web.rest.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import v4.ex.exc.BizException;
import v4.web.rest.dao.MaintenanceDAO;
import v4.web.rest.service.MaintenanceService;
import v4.web.vo.CreateMaintenanceDetailVO;
import v4.web.vo.CreateMaintenanceVO;

@Service("maintenanceService")
public class MaintenanceServiceImpl implements MaintenanceService {

	@Autowired
	MaintenanceDAO maintenanceDAO;

	@Override
	public void createMaintenance(CreateMaintenanceVO createMaintenanceVO) throws BizException {
		try {
			maintenanceDAO.createMaintenance(createMaintenanceVO);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public void createMaintenanceDetil(CreateMaintenanceDetailVO createMaintenanceDetailVO) throws BizException {
		try {
			//maintenance table먼저 업데이트
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("corp", createMaintenanceDetailVO.getCorp());
			param.put("maintenanceKey", createMaintenanceDetailVO.getMaintenanceKey());
			
			param.put("garage", createMaintenanceDetailVO.getGarage());
			param.put("distance", createMaintenanceDetailVO.getDistance());
			param.put("repairDate", createMaintenanceDetailVO.getRepairDate());
			param.put("maintenanceHistory", createMaintenanceDetailVO.getMaintenanceHistory());

			maintenanceDAO.updateMaintenance(param);
			
			//업데이트 하고 partskey만큼 돌아서 insert
			List<String> partsKeys = createMaintenanceDetailVO.getPartsKey();
			List<String> memos = createMaintenanceDetailVO.getMemo();
			List<String> partsQualitys = createMaintenanceDetailVO.getPartsQuality(); 
			List<Integer> partsCnts = createMaintenanceDetailVO.getPartsCnt();
			List<Integer> partsPays = createMaintenanceDetailVO.getPartsPay();
			List<Integer> wagePays = createMaintenanceDetailVO.getWagePay();
			for(int i = 0 ; i < partsKeys.size();i++){
				String partsKey = partsKeys.get(i);
				String memo = memos.get(i);
				String partsQuality = partsQualitys.get(i);
				Integer partsCnt = partsCnts.get(i);
				Integer partsPay = partsPays.get(i);
				Integer wagePay = wagePays.get(i);
				param.put("partsKey", partsKey);
				param.put("memo", memo);
				param.put("partsQuality", partsQuality);
				param.put("partsCnt", partsCnt);
				param.put("partsPay", partsPay);
				param.put("wagePay", wagePay);
				param.put("partsOrder", i + 1);
				maintenanceDAO.insertMaintenanceDetail(param);
			}
			
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}
	
	
}
