package v4.web.rest.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import v4.web.rest.dao.ClinicDAO;
import v4.web.rest.service.ClinicService;
import v4.web.vo.AccountVO;
import v4.web.vo.ClinicVO;
import v4.web.vo.CreateClinicVO;
import v4.web.vo.CreateMaintenanceCompanyVO;
import v4.web.vo.CreateVehiclePartsEtcVO;
import v4.web.vo.MaintenanceCompanyVO;
import v4.web.vo.UpdateClinicVO;
import v4.web.vo.VehiclePartsEtcVO;
import v4.web.vo.VehiclePartsVO;

@Service("clinicService")
public class ClinicServiceImpl implements ClinicService {

	@Autowired
	ClinicDAO clinicDAO;

	/**
	 * 정비 내역을 입력한다.
	 * INSERT maintenance TABLE
	 */
	@Override
	public String addMaintenance(AccountVO accountVo, CreateClinicVO vo) {
		
		clinicDAO.addMaintenance(vo);
		System.out.println(vo);
		
		return vo.getMaintenanceKey();
	}
	
	/**
	 * 정비 상세 내역을 입력한다
	 * INSERT maintenance_detail TABLE
	 */
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void addMaintenanceDetail(String maintenanceKey, CreateClinicVO vo) {
		System.out.println(maintenanceKey);
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("maintenanceKey", maintenanceKey);
		
		String[] partsKey = vo.getPartsKey().split(",");
		String[] partsQuality = vo.getPartsQuality().split(",");
		String[] partsCnt = vo.getPartsCnt().split(",");
		String[] partsPay = vo.getPartsPay().split(",");
		String[] wage = vo.getWage().split(",");
		
		for(int i = 0 ; i < partsKey.length ; i++){
			param.put("partsKey", partsKey[i]);
			param.put("partsQuality", partsQuality[i]);
			param.put("partsCnt", partsCnt[i]);
			param.put("partsPay", partsPay[i]);
			param.put("wage", wage[i]);
			param.put("partsOrder", i);
			
			clinicDAO.addMaintenanceDetail(param);
		}
		
		
		//해당 부품에 itemFlag가 1이면
		
		
		//vehicle_items에 partskey로 검색해서 있으면 max을 val로 덮어씌우고
		//없으면 vehicle_parts에 커럼 defaultMax값을 vehicle_items의 val,max에 insert시킨다.
		//vehicle_items에 max(sort) + 1로 해야된다.
		
		
		
		
	}

	/**
	 * 정비내역을 삭제한다
	 * 
	 * maintenance TABLE
	 * maintenance_detail TABLE
	 * 
	 */
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void deleteMaintenance(Map<String, Object> param) {
		
		clinicDAO.deleteMaintenance(param);
		
		//상세도 삭제 해야 된다.
		clinicDAO.deleteMaintenanceDetail(param);
	}

	/**
	 * 정비내역을 수정한다
	 * UPDATE maintenance TABLE
	 * 
	 * 
	 * DELETE INSERT maintenance_detail TABEL
	 */
	@Override
	public void updateMainteance(UpdateClinicVO vo) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * 정비내역을 한개를 가져 온다.
	 * 
	 * SELECT 1 maintenance TABLE
	 */
	@Override
	public ClinicVO getMaintenance(Map<String, Object> param) {
		
		return clinicDAO.getMaintenance(param);
	}

	/**
	 * 정비 결재 상태를 업데이트 한다.
	 * 
	 * UPDATE maintenance TABLE
	 */
	@Override
	public void updateApprovalState(String maintenanceKey, String accountKey, String step) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("maintenanceKey", maintenanceKey);
		param.put("accountKey", accountKey);
		param.put("step", step);
		
		clinicDAO.updateApprovalState(param);
	}

	/**
	 * 정비 전체 결재 리스트를 가져 온다.
	 */
	@Override
	public List<ClinicVO> getMaintenanceApprovalList(Map<String, Object> param) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 전체 정비 업체 리스트를 가져 온다.
	 * 
	 * SELECT all maintenanceCompany TABLE
	 */
	@Override
	public List<MaintenanceCompanyVO> getMaintenanceCompanyList(Map<String, Object> param) {
		
		return clinicDAO.getMaintenanceCompanyList(param);
	}

	/**
	 * 정비 업체 리스트를 삭제한다.
	 * 
	 * DELETE maintenanceCompany TABLE
	 */
	@Override
	public void deleteMaintenanceCompany(Map<String, Object> param) {
		clinicDAO.deleteMaintenanceCompany(param);
	}

	/**
	 * 정비업체 리스트를 추가한다.
	 * 
	 * INSERT maintenanceCompany TABLE
	 */
	@Override
	public String insertMaintenanceCompany(CreateMaintenanceCompanyVO vo) {
		
		clinicDAO.insertMaintenanceCompany(vo);
		
		return vo.getCompanyKey();
	}

	/**
	 * 정비 부품 리스트를 가져 온다
	 * 
	 * SELECT vehicle_parts TABLE
	 */
	@Override
	public List<VehiclePartsVO> getVehiclePartsList(Map<String, Object> param) {
		// TODO Auto-generated method stub
		return clinicDAO.getVehiclePartsList(param);
	}


	/**
	 * 일반 정비 부품 리스트를 가져 온다.
	 * 
	 * SELECT vehicle_parts_etc TABLE  
	 */
	@Override
	public List<VehiclePartsEtcVO> getVehiclePartsEtcList(Map<String, Object> param) {
		
		return clinicDAO.getVehiclePartsEtcList(param);
	}

	/**
	 * 일반 정비 부품을 삭제한다.
	 * 
	 * DELETE vehicle_parts_etc TABLE 
	 */
	@Override
	public void deleteVehiclePartsEtc(Map<String, Object> param) {
		
		clinicDAO.deleteVehiclePartsEtc(param);
		
	}

	/**
	 * 일반 정비 부품 생성한다.
	 * 
	 * INSERT vehicle_parts_etc TABLE
	 */
	@Override
	public String createVehiclePartsEtc(CreateVehiclePartsEtcVO vo) {
		clinicDAO.createVehiclePartsEtc(vo);
		
		return vo.getPartsKey();
	}

	/**
	 * 일반 정비 내역을 입력한다.
	 * 
	 * INSERT maintenance_etc TABLE
	 * 
	 */
	@Override
	public String addMaintenanceEtc(AccountVO accountVo, CreateClinicVO vo) {
		clinicDAO.addMaintenanceEtc(vo);
		return vo.getMaintenanceKey();
	}

	/**
	 * 일반 정비 상세 내역을 입력한다.
	 * 
	 * INSERT maintenenace_etc_detail TABLE
	 */
	@Override
	public void addMaintenanceEtcDetail(String maintenanceKey, CreateClinicVO vo) {
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("maintenanceKey", maintenanceKey);
		
		String[] partsNm = vo.getPartsNm().split(",");
		String[] partsCnt = vo.getPartsCnt().split(",");
		String[] partsPay = vo.getPartsPay().split(",");
		String[] wage = vo.getWage().split(",");
		
		for(int i = 0 ; i < partsNm.length ; i++){
			param.put("partsNm", partsNm[i]);
			param.put("partsCnt", partsCnt[i]);
			param.put("partsPay", partsPay[i]);
			param.put("wage", wage[i]);
			param.put("partsOrder", i);
			
			clinicDAO.addMaintenanceEtcDetail(param);
		}
		
	}

	/**
	 * 일반 정비 삭제
	 * 
	 * DELETE maintenance_etc TABLE
	 * DELETE maintenance_etc_detail TABLE
	 */
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void deleteMaintenanceEtc(Map<String, Object> param) {
		clinicDAO.deleteMaintenanceEtc(param);
		clinicDAO.deleteMaintenanceEtcDetail(param);
	}

	@Override
	public void updateApprovalStateEtc(String maintenanceKey, String accountKey, String step) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("maintenanceKey", maintenanceKey);
		param.put("accountKey", accountKey);
		param.put("step", step);
		
		clinicDAO.updateApprovalStateEtc(param);
		
	}
	
    @Override
    public List<Map<String,Object>> getVehiclePartTypeList() {
        return clinicDAO.getVehiclePartTypeList();
    }
	
}
