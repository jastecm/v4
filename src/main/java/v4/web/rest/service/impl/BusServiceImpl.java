package v4.web.rest.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.scripting.bsh.BshScriptUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import v4.ex.exc.BizException;
import v4.web.rest.dao.BusDAO;
import v4.web.rest.service.BusService;
import v4.web.vo.CreateBusAllocateVO;
import v4.web.vo.CreateBusFavoritesVO;
import v4.web.vo.CreateBusRouteStationVO;
import v4.web.vo.CreateDrivingPurposeVO;
import v4.web.vo.UpdateBusRouteStationVO;
import v4.web.vo.UpdateDrivingPurposeVO;


@Service("busService")
public class BusServiceImpl implements BusService {

	@Autowired
	BusDAO busDao;
	
	@Override
	public int createDrivingPurpose(CreateDrivingPurposeVO drivingPurposeVo) throws BizException {
		try {
			busDao.createDrivingPurpose(drivingPurposeVo);
			return drivingPurposeVo.getSeq(); 
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public void deleteDrivingPurpose(Map<String, Object> param, Map<String, Object> auth) throws BizException {
		try {
			//운행목적을 삭제 하면 
			busDao.deleteDrivingPurpose(param);
			//매칭된 노선도 전부 삭제 한다.
			busDao.deleteBusServiceRoute(param);
			
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public void updateDrivingPurpose(UpdateDrivingPurposeVO updateDrivingPurposeVo) throws BizException {
		try {
			busDao.updateDrivingPurpose(updateDrivingPurposeVo);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public void createBusRouteStation(CreateBusRouteStationVO busRouteStationVO) throws BizException {
		try {
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("corp", busRouteStationVO.getCorp());
			param.put("drivingPurpose", busRouteStationVO.getDrivingPurpose());
			param.put("busRouteName", busRouteStationVO.getBusRouteName());
			param.put("busRouteImg", busRouteStationVO.getBusRouteImg());
			param.put("regUser", busRouteStationVO.getRegUser());
			param.put("busRouteKeyword", busRouteStationVO.getBusRouteKeyword());
			
			busDao.createBusRoute(param);
			
			Map<String,Object> param2 = new HashMap<String,Object>();
			
			List<String> stationName = busRouteStationVO.getStationName();
			List<String> arrivalTime = busRouteStationVO.getArrivalTime();
			List<String> stationImg = busRouteStationVO.getStationImg();
			List<String> address = busRouteStationVO.getAddress();
			List<String> lon = busRouteStationVO.getLon();
			List<String> lat = busRouteStationVO.getLat();
			
			for(int i = 0 ; i < stationName.size(); i++){
				param2.put("corp", param.get("corp"));
				param2.put("busRouteKey", param.get("busRouteKey"));
				param2.put("stationName", stationName.get(i));
				param2.put("arrivalTime", arrivalTime.get(i));
				if(stationImg.size() != 0){
					param2.put("stationImg", stationImg.get(i));
				}else{
					param2.put("stationImg", null);
				}
				
				param2.put("address", address.get(i));
				param2.put("lon", lon.get(i));
				param2.put("lat", lat.get(i));
				param2.put("busStationOrder", i + 1 );
				
				busDao.createBusRouteStation(param2);
			}
			
			
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public void deleteBusRouteStation(Map<String, Object> param) throws BizException {
		try {
			
			//노선삭제
			busDao.deleteBusRoute(param);
			//정류장 삭제
			busDao.deleteBusRouteStation(param);
			
			
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public void updateBusRouteStation(UpdateBusRouteStationVO busRouteStationVO) throws BizException {
		try {
			
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("corp", busRouteStationVO.getCorp());
			param.put("busRouteKey", busRouteStationVO.getBusRouteKey());
			param.put("drivingPurpose", busRouteStationVO.getDrivingPurpose());
			param.put("busRouteName", busRouteStationVO.getBusRouteName());
			param.put("busRouteImg", busRouteStationVO.getBusRouteImg());
			param.put("busRouteKeyword", busRouteStationVO.getBusRouteKeyword());
			
			busDao.updateBusRoute(param);
			busDao.deleteBusRouteStation(param);
			
			Map<String,Object> param2 = new HashMap<String,Object>();
			
			List<String> stationName = busRouteStationVO.getStationName();
			List<String> arrivalTime = busRouteStationVO.getArrivalTime();
			List<String> stationImg = busRouteStationVO.getStationImg();
			List<String> address = busRouteStationVO.getAddress();
			List<String> lon = busRouteStationVO.getLon();
			List<String> lat = busRouteStationVO.getLat();
			
			for(int i = 0 ; i < stationName.size(); i++){
				param2.put("corp", busRouteStationVO.getCorp());
				param2.put("busRouteKey", busRouteStationVO.getBusRouteKey());
				param2.put("stationName", stationName.get(i));
				param2.put("arrivalTime", arrivalTime.get(i));
				if(stationImg.size() != 0){
					param2.put("stationImg", stationImg.get(i));
				}else{
					param2.put("stationImg", null);
				}
				param2.put("address", address.get(i));
				param2.put("lon", lon.get(i));
				param2.put("lat", lat.get(i));
				param2.put("busStationOrder", i + 1 );
				busDao.createBusRouteStation(param2);
			}
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}

	@Override
	public boolean busAllocateCheck(Map<String, Object> param) throws BizException {
		try {
			
			Map<String,Object> arrivalTime = busDao.getBusArrivalTime(param);
			String firstArrivalTime = (String) arrivalTime.get("firstArrivalTime");
			String lastArrivalTime = (String) arrivalTime.get("lastArrivalTime");
			
			String allocateDate = (String) param.get("allocateDate");

			param.put("firstArrivalTime", allocateDate+" "+firstArrivalTime);
			param.put("lastArrivalTime", allocateDate+" "+lastArrivalTime);
			
			
			Map<String,Object> chk = busDao.busMappingCheck(param);
			
			if(chk == null){
				return true;
			}
			
			return false;
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public void createBusAllocate(CreateBusAllocateVO busAllocateVO) throws BizException {
		try {
			
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("corp", busAllocateVO.getCorp());
			param.put("busRouteKey", busAllocateVO.getBusRouteKey());
			
			Map<String,Object> arrivalTime = busDao.getBusArrivalTime(param);
			String firstArrivalTime = (String) arrivalTime.get("firstArrivalTime");
			String lastArrivalTime = (String) arrivalTime.get("lastArrivalTime");
			
			String allocateDate = busAllocateVO.getAllocateDate();
			
			busAllocateVO.setWorkingStartDate(allocateDate+" "+firstArrivalTime);
			busAllocateVO.setWorkingEndDate(allocateDate+" "+lastArrivalTime);
			
			busDao.createBusAllocate(busAllocateVO);
			
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}

	@Override
	public void deleteBusAllocate(Map<String, Object> param) throws BizException {
		try {
			
			busDao.deleteBusAllocate(param);
			
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}

	@Override
	public void createBusFavorites(CreateBusFavoritesVO busFavoritesVO) throws BizException {
		try {
			busDao.createBusFavorites(busFavoritesVO);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public void deleteBusFavorites(Map<String, Object> param) throws BizException {
		try {
			busDao.deleteBusFavorites(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}
}
