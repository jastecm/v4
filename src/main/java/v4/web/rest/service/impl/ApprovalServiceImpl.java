package v4.web.rest.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import v4.ex.exc.BizException;
import v4.web.rest.dao.ApprovalDAO;
import v4.web.rest.service.ApprovalService;
import v4.web.vo.CreateMaintenanceVO;

@Service("approvalService")
public class ApprovalServiceImpl implements ApprovalService {
	
	@Autowired
	ApprovalDAO approvalDAO;
	
	@Override
	public Map<String, Object> getMaintenanceApproval(CreateMaintenanceVO createMaintenanceVO) throws BizException {
		try {
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("corp", createMaintenanceVO.getCorp());
			param.put("accountKey", createMaintenanceVO.getAccountKey());
			
			return approvalDAO.getMaintenanceApproval(param);
			
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

}
