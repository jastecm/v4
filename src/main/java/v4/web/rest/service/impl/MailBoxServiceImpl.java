package v4.web.rest.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import v4.web.rest.dao.MailBoxDAO;
import v4.web.rest.service.MailBoxService;


@Service("mailBoxService")
public class MailBoxServiceImpl implements MailBoxService {

	@Autowired
	MailBoxDAO mailBoxDao;

	@Override
	public void allUserMailBoxInsert(String title, String msg, long mailExpired) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("title", title);
		param.put("msg", msg);
		param.put("mailExpired", mailExpired);
		
		mailBoxDao.allUserMailBoxInsert(param);
		
	}

	@Override
	public void simgleUserMailBoxInsert(String accountKey, String title, String msg, long mailExpired) {
		
		if(StrUtil.isNullToEmpty(accountKey)) return;
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("title", title);
		param.put("msg", msg);
		param.put("mailExpired", mailExpired);
		param.put("accountKey", accountKey);
		
		mailBoxDao.simgleUserMailBoxInsert(param);
	}

	@Override
	public List<Map<String, Object>> getMailBoxList(Map<String,Object> param , Map<String,Object> auth) {
		param.put("authTable", auth);
		return mailBoxDao.getMailBox(param);
	}
	@Override
	public int getMailBoxListCnt(Map<String,Object> param , Map<String,Object> auth) {
		param.put("authTable", auth);
		return mailBoxDao.getMailBoxCnt(param);
	}

	@Override
	public void deleteMailBox(String accountKey , String mailBoxKey) {
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("accountKey", accountKey);
		param.put("mailBoxKey", mailBoxKey);
		mailBoxDao.deleteMailBox(param);
	}
	
	@Override
	public void deleteMailBoxExpired() {
		mailBoxDao.deleteMailBoxExpired();
	}

	@Override
	public void deleteMailBoxAll(String userKey) {
		mailBoxDao.deleteMailBoxAll(userKey);
		
	}
	
}

