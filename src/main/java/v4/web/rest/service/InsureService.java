package v4.web.rest.service;

import java.util.List;
import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.InsureVO;








public interface InsureService {

	InsureVO getInsure(String lang, String vehicleKey) throws BizException;

	void createInsure(InsureVO insureVo)  throws BizException;

	void updateInsure(InsureVO insureVo)  throws BizException;

	void deleteInsure(String vehicleKey)  throws BizException;

	List<Map<String,String>> getMiligeWaringList(int remainDay) throws BizException;


	

}
	

