package v4.web.rest.service;

import java.util.List;
import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.AccountVO;
import v4.web.vo.CreateGroupVO;
import v4.web.vo.GroupVO;
import v4.web.vo.UpdateGroupVO;

public interface GroupService {
	
	String addGroup(CreateGroupVO reqVo, String corpKey) throws BizException;

	void updateGroup(UpdateGroupVO groupVo, Map<String, Object> param);

	void groupAddUser(Map<String, Object> param);

	void groupDeleteUser(Map<String, Object> param);

	void groupAddVehicle(Map<String, Object> param);

	void groupDeleteVehicle(Map<String, Object> param);
	
	void deleteGroup(Map<String, Object> param);
	
	
	
	/*group data service 구현 가능하면 삭제*/
	List<GroupVO> getGroupList(Map<String, Object> param);

	List<GroupVO> getGroupListDesending(Map<String, Object> param); //->getGroupParentList

	GroupVO getGroup(Map<String, Object> param); 

	void sortGroup(AccountVO account); //안쓰는듯? 
	
	List<GroupVO> getParentGroupTree(Map<String, Object> param); //->getGroupChildList
	
	int getGroupListCnt(Map<String, Object> param, Object object);
	/*group data service 구현 가능하면 삭제*/
}
