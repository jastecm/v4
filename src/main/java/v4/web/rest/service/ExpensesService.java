package v4.web.rest.service;

import v4.ex.exc.BizException;
import v4.web.vo.CreateExpensesBulkVO;
import v4.web.vo.CreateExpensesVO;

public interface ExpensesService {

	void createExpenses(CreateExpensesVO createExpensesVO) throws BizException;

	void createExpensesBulk(CreateExpensesBulkVO vo) throws BizException;

}
