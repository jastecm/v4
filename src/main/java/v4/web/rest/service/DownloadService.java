package v4.web.rest.service;

import java.io.File;
import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.AdminCorpVO;
import v4.web.vo.DeviceVO;
import v4.web.vo.DownloadVO;








public interface DownloadService {
	/*
	
	Map<Integer, byte[]> getVehicleDb(String convCode,AdminCorpVO adminCorpVo) throws BizException;
	Map<Integer, byte[]> getFirmware(String convCode,AdminCorpVO adminCorpVo) throws BizException;
	Map<Integer, byte[]> getFirmware(AdminCorpVO adminCorpVo,String deviceSeries) throws BizException;
	String getFirmwareVer(AdminCorpVO adminCorpVo,String deviceSeries) throws BizException;
	Map<Integer, byte[]> getFirmwareBySeries(String series,AdminCorpVO adminCorpVo) throws BizException;
	Map<Integer, byte[]> getVehicleDbByConvCode(String convCode,AdminCorpVO adminCorpVo) throws BizException;
	void updateFirmVersion_S31(String version);
	void updateVer(String adminCorp, String field, String val);
	
	File getVehicleDb(String convCode) throws BizException;
	*/
	//
	Map<Integer, byte[]> getFirmware(DeviceVO dvo) throws BizException;
	Map<Integer, byte[]> getVehicleDb(DeviceVO dvo) throws BizException;	
	File getVehicleDbFile(DeviceVO dvo)  throws BizException;
	
	DownloadVO getDeviceSeriesVehicleDb(String deviceSn) throws BizException;
	DownloadVO getVer(DeviceVO vo,String osType) throws BizException;
	
	String getWebViewVer();
	void setWebViewVer(String webViewVer);

}
	

