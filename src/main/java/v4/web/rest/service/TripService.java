package v4.web.rest.service;

import java.util.List;
import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.AccountVO;
import v4.web.vo.CreateCorpVO;
import v4.web.vo.CreateInviteVO;
import v4.web.vo.CreatePartnerVO;
import v4.web.vo.CreatePersonVO;
import v4.web.vo.CreateRentVO;
import v4.web.vo.CreateUserVO;
import v4.web.vo.LoginVO;







public interface TripService {

	void updatePurpose(String tripKey, String purpose) throws BizException;
	
	void updatePurposeCompareDay(String tripKey, String purpose) throws BizException;
}
	

