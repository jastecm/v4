package v4.web.rest.service;

import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.CreatePenaltyVO;

public interface PenaltyService {

	void createPenalty(CreatePenaltyVO createPenaltyVO) throws BizException;

	void deletePenalty(Map<String, Object> param) throws BizException;

}
