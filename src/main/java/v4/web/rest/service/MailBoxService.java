package v4.web.rest.service;

import java.util.List;
import java.util.Map;

public interface MailBoxService {

	void allUserMailBoxInsert(String title, String message,long mailExpired);

	void simgleUserMailBoxInsert(String accountKey, String title, String message,long mailExpired);

	List<Map<String,Object>> getMailBoxList(Map<String,Object> param , Map<String,Object> auth);
	
	int getMailBoxListCnt(Map<String,Object> param , Map<String,Object> auth);
	
	void deleteMailBox(String accountKey , String mailBoxKey);

	void deleteMailBoxExpired();

	void deleteMailBoxAll(String userKey);
}
	

