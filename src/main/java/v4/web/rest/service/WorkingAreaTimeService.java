package v4.web.rest.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.AccountVO;
import v4.web.vo.WorkingTimeAreaVO;

public interface WorkingAreaTimeService {

/*	void addWorkingArea(String corp, String corpGroup, String accountKey, String cityLev1, String cityLev2) throws BizException;

	List<WorkingTimeAreaVO> getWorkingArea(UserVO user, Map<String, String> param) throws BizException;

	void deleteWorkingArea(UserVO user, String workAreaKey)  throws BizException;

	int getWorkingAreaTotCnt(UserVO user) throws BizException;

	void updateWorkingArea(String corp , String workAreaKey, String corpGroup, String accountKey, String cityLev1, String cityLev2) throws BizException;

	void addWorkingTime(String corp, String corpGroup, String accountKey, String startH, String startM, String endH, String endM, String day) throws BizException;

	List<WorkingTimeAreaVO> getWorkingTime(UserVO user, Map<String, String> param) throws BizException;

	int getWorkingTimeTotCnt(UserVO user) throws BizException;

	void deleteWorkingTime(UserVO user, String workTimeKey) throws BizException;

	void updateWorkingTime(String corp, String workTimeKey, String corpGroup, String accountKey,
			String startH, String startM, String endH, String endM, String day) throws BizException;

	List<WorkingAreaDrivingVO> workingAreaDrivingResult(UserVO user, Map<String, String> param) throws BizException;
	
	
	

*/
	void deleteWorkingTime(AccountVO user, String workTimeKey) throws BizException;
	
	List<WorkingTimeAreaVO> getWorkingTime(Map<String, Object> param) throws BizException;
	
	void updateWorkingTime(String corpKey, String workTimeKey, String groupKey, String accountKey,
			String startH, String startM, String endH, String endM, String day) throws BizException;
	
	void addWorkingTime(String corpKey, String groupKey, String accountKey, String startH, String startM, String endH, String endM, String day) throws BizException;
	
	void updateWorkingArea(String corpKey , String workAreaKey, String groupKey, String accountKey, String cityLev1, String cityLev2) throws BizException;
	
	void deleteWorkingArea(AccountVO user, String workAreaKey)  throws BizException;
	
	void addWorkingArea(String corpKey, String groupKey, String accountKey, String cityLev1, String cityLev2) throws BizException;
	
	List<WorkingTimeAreaVO> getWorkingArea(AccountVO user, Map<String, Object> param) throws BizException;

	String getCityByPoint(String startAreaPoint) throws BizException;

	String chkWorkingArea(String accountKey, double startLon, double startLat, double endLon, double endLat) throws BizException;

	String chkWorkingTime(String accountKey, long startDate, long endDate) throws BizException;
	
	String getAccountWorkingArea(String accountKey)  throws BizException;

	int getWorkingTimeCnt(Map<String, Object> param, Object object);

	int getWorkingTimeTotalCnt(Map<String, Object> param);
}
	

