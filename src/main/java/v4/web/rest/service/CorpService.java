package v4.web.rest.service;

import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.AccountVO;
import v4.web.vo.ApprovalConfigVO;
import v4.web.vo.CorpVO;
import v4.web.vo.CreateApprovalConfigVO;
import v4.web.vo.CreateCorpVO;
import v4.web.vo.CreatePartnerVO;
import v4.web.vo.CreatePersonVO;
import v4.web.vo.CreateRentVO;







public interface CorpService {

	boolean createCorp(CreateCorpVO vo)  throws BizException ;
	
	boolean createCorp(CreatePersonVO vo)  throws BizException ;	

	boolean createCorp(CreateRentVO vo)  throws BizException ;
	
	boolean createPartner(CreatePartnerVO vo)  throws BizException ;
	

	CorpVO getCorp(String corpKey, String corpType)  throws BizException ;

	void changeCorpVehicleManager(Map<String, Object> param);

	AccountVO getCorpVehicleManager(Map<String, Object> param);

	void updateApprovalConfig(CreateApprovalConfigVO createApprovalConfigVo);

	ApprovalConfigVO getApprovalConfig(Map<String, Object> param);

}
	

