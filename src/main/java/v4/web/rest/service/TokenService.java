package v4.web.rest.service;

import java.util.List;

import v4.ex.exc.BizException;
import v4.web.vo.TokenVO;







public interface TokenService {
	
	public void clearUserToken(String userKey) throws BizException;
	public void addUserToken(TokenVO t) throws BizException;
	
	public void semiClearUserToken(String userKey) throws BizException;
	public List<Long> getUserToken(String userKey) throws BizException;

}
	

