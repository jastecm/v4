package v4.web.rest.service;

import java.util.List;
import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.DtcVO;
import v4.web.vo.data.VehicleVO;

public interface DtcService {

	void dtcTripChk(String vehicleKey, long startTime, long endTime) throws BizException;
	
	DtcVO parseDtcPacket(String packet,String convCode,String dtcManufacture) throws BizException;

	String saveDtc(VehicleVO vehicleKey, DtcVO dtcVo) throws BizException;
	
	List<Map<String, String>> getDtcList(String vehicleKey, String dtcKey,String lang) throws BizException;
	
	Map<String, String> getDtcDesc(String dtcCode, String dtcManufacture, String lang);
/*
	List<Map<String, String>> getDtcHeaderList(String vehicleKey,int limit , int offset,String lang) throws BizException;

	

	void removeAction(String vehicleKey) throws BizException;

	void removeHstr(String dtcKey)  throws BizException;

	
*/

}
	

