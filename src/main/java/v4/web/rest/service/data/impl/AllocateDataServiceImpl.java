package v4.web.rest.service.data.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import v4.ex.exc.BizException;
import v4.web.rest.dao.data.AllocateREPO;
import v4.web.rest.service.data.AllocateDataService;
import v4.web.vo.data.AllocateVO;



@Service("allocateDataService")
public class AllocateDataServiceImpl implements AllocateDataService {

	@Autowired
	AllocateREPO allocateRepo;
	
	
	@Override
	public List<AllocateVO> getAllocateList(Map<String, Object> param, Map<String, Object> auth) throws BizException {
		try {
			param.put("authTable", auth);
			return allocateRepo.getAllocateList(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public AllocateVO getAllocate(Map<String, Object> param, Map<String, Object> auth) throws BizException {
		try {
			param.put("authTable", auth);
			return allocateRepo.getAllocate(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public AllocateVO getAllocateFromKey(String key) throws BizException {
		try {
			return allocateRepo.getAllocateFromKey(key);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public int getAllocateListCnt(Map<String, Object> param, Map<String, Object> auth) throws BizException {
		try {
			param.put("authTable", auth);
			return allocateRepo.getAllocateListCnt(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
			
}

