package v4.web.rest.service.data;

import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.data.ApprovalVO;
import v4.web.vo.data.CorpVO;

public interface CorpDataService {

	CorpVO getCorp(Map<String,Object> param,Map<String,Object> auth) throws BizException;
	
	CorpVO getCorpFromKey(String key) throws BizException;
	
	ApprovalVO getApproval(String key) throws BizException;
}
	

