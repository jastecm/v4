package v4.web.rest.service.data.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import v4.ex.exc.BizException;
import v4.web.rest.dao.data.AccidentREPO;
import v4.web.rest.service.data.AccidentDataService;
import v4.web.vo.data.AccidentVO;


@Service("accidentDataService")
public class AccidentDataServiceImpl implements AccidentDataService {

	@Autowired
	AccidentREPO accidentREPO;
	
	@Override
	public List<AccidentVO> getAccidentList(Map<String, Object> param) throws BizException {
		try {
			return accidentREPO.getAccidentList(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public int getAccidentListCnt(Map<String, Object> param) throws BizException {
		try {
			return accidentREPO.getAccidentListCnt(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

}
