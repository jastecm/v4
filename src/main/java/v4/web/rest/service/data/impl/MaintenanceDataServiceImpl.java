package v4.web.rest.service.data.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import v4.ex.exc.BizException;
import v4.web.rest.dao.data.MaintenanceREPO;
import v4.web.rest.service.data.MaintenanceDataService;
import v4.web.vo.data.MaintenanceVO;

@Service("maintenanceDataService")
public class MaintenanceDataServiceImpl implements MaintenanceDataService {

	@Autowired
	MaintenanceREPO maintenanceREPO;
	
	
	@Override
	public List<MaintenanceVO> getMaintenanceList(Map<String, Object> param) throws BizException {
		try {
			return maintenanceREPO.getMaintenanceList(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}


	@Override
	public int getMaintenanceListCnt(Map<String, Object> param) throws BizException {
		try {
			return maintenanceREPO.getMaintenanceListCnt(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

}
