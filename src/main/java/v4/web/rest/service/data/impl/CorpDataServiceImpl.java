package v4.web.rest.service.data.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import v4.ex.exc.BizException;
import v4.web.rest.dao.data.CorpREPO;
import v4.web.rest.service.data.CorpDataService;
import v4.web.vo.data.ApprovalVO;
import v4.web.vo.data.CorpVO;



@Service("corpDataService")
public class CorpDataServiceImpl implements CorpDataService {

	@Autowired
	CorpREPO corpRepo;
	
	@Override
	public CorpVO getCorp(Map<String,Object> param,Map<String,Object> auth) throws BizException {
		try {
			param.put("authTable", auth);
			return corpRepo.getCorp(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public CorpVO getCorpFromKey(String key) throws BizException {
		try {
			return corpRepo.getCorpFromKey(key);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}	
	
	@Override
	public ApprovalVO getApproval(String key) throws BizException {
		try {
			return corpRepo.getApproval(key);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}	
}

