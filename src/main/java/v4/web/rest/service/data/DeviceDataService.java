package v4.web.rest.service.data;

import java.util.List;
import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.data.AccountVO;
import v4.web.vo.data.DeviceVO;
import v4.web.vo.data.GroupVO;
import v4.web.vo.data.TripVO;
import v4.web.vo.data.VehicleVO;

public interface DeviceDataService {

	DeviceVO getDevice(Map<String,Object> param,Map<String,Object> auth) throws BizException;

	DeviceVO getDeviceFromKey(String key) throws BizException;
	DeviceVO getDeviceFromSn(String key) throws BizException;
	DeviceVO getDeviceFromId(String key) throws BizException;
	
	List<DeviceVO> getDeviceList(Map<String, Object> param,Map<String,Object> auth) throws BizException;
	
	int getDeviceListCnt(Map<String, Object> param,Map<String,Object> auth) throws BizException;
}
	

