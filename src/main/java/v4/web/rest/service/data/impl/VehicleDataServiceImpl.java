package v4.web.rest.service.data.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import v4.ex.exc.BizException;
import v4.web.rest.dao.data.VehicleLocationREPO;
import v4.web.rest.dao.data.VehicleREPO;
import v4.web.rest.service.data.VehicleDataService;
import v4.web.vo.data.VehicleLocationVO;
import v4.web.vo.data.VehicleVO;



@Service("vehicleDataService")
public class VehicleDataServiceImpl implements VehicleDataService {

	@Autowired
	VehicleREPO vehicleRepo;
	
	@Autowired
	VehicleLocationREPO vehicleLocationRepo;
	
	@Override
	public VehicleVO getVehicle(Map<String,Object> param,Map<String,Object> auth) throws BizException {
		try {
			param.put("authTable", auth);
			return vehicleRepo.getVehicle(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public VehicleVO getVehicleFromKey(String key) throws BizException {
		try {
			return vehicleRepo.getVehicleFromKey(key);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public List<VehicleVO> getVehicleList(Map<String,Object> param,Map<String,Object> auth) throws BizException {
		try {
			param.put("authTable", auth);
			return vehicleRepo.getVehicleList(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	
	@Override
	public int getVehicleListCnt(Map<String,Object> param,Map<String,Object> auth) throws BizException {
		try {
			param.put("authTable", auth);
			return vehicleRepo.getVehicleListCnt(param);
		} catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
				System.err.println(e.getMessage());
				throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public List<VehicleLocationVO> getVehicleLocationList(Map<String, Object> param, Map<String, Object> auth)
			throws BizException {
		try {
			param.put("authTable", auth);
			return vehicleLocationRepo.getVehicleLocationList(param);
		} catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
				System.err.println(e.getMessage());
				throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public VehicleVO getVehicleFromPlateNum(String PlateNum) throws BizException {
		try {
			return vehicleRepo.getVehicleFromPlateNum(PlateNum);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
}

