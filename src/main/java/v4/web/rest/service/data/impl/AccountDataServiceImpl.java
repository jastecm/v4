package v4.web.rest.service.data.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import v4.ex.exc.BizException;
import v4.web.rest.dao.data.AccountREPO;
import v4.web.rest.service.data.AccountDataService;
import v4.web.vo.LoginVO;
import v4.web.vo.data.AccountVO;



@Service("accountDataService")
public class AccountDataServiceImpl implements AccountDataService {

	@Autowired
	AccountREPO accountRepo;
	
	@Override
	public AccountVO getAccount(Map<String,Object> param,Map<String,Object> auth) throws BizException {
		try {
			param.put("authTable", auth);
			return accountRepo.getAccount(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public AccountVO getAccountFromKey(String key,boolean simpleMode) throws BizException {
		try {
			if(simpleMode) return accountRepo.getAccountFromKeySimple(key);
			else return accountRepo.getAccountFromKey(key);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public AccountVO getAccountFromId(String id) throws BizException {
		try {
			return accountRepo.getAccountFromId(id);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public AccountVO getAccountFromIdPw(LoginVO vo) throws BizException {
		
		try {
			
			return accountRepo.getAccountFromIdPw(vo);
			
		}catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}
	
	@Override
	public AccountVO getAccountFromPhonePw(LoginVO vo) throws BizException {
		
		try {
			
			return accountRepo.getAccountFromPhonePw(vo);
			
		}catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
		
	}
	
	@Override
	public List<AccountVO> getAccountList(Map<String,Object> param,Map<String,Object> auth) throws BizException {
		try {
			param.put("authTable", auth);
			return accountRepo.getAccountList(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	
	@Override
	public int getAccountListCnt(Map<String,Object> param,Map<String,Object> auth) throws BizException {
		try {
			param.put("authTable", auth);
			return accountRepo.getAccountListCnt(param);
		} catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
				System.err.println(e.getMessage());
				throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
}

