package v4.web.rest.service.data.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.util.GeoUtil;

import v4.ex.exc.BizException;
import v4.web.rest.dao.data.TrackREPO;
import v4.web.rest.dao.data.TripAnalysisREPO;
import v4.web.rest.dao.data.TripREPO;
import v4.web.rest.service.data.TripDataService;
import v4.web.vo.data.TrackInfoVO;
import v4.web.vo.data.TrackInfoVO.TrackRecord;
import v4.web.vo.data.TripAnalysisVO;
import v4.web.vo.data.TripVO;



@Service("tripDataService")
public class TripDataServiceImpl implements TripDataService {

	@Autowired
	TripREPO tripRepo;
	
	@Autowired
	TripAnalysisREPO tripAnalysisRepo;
	
	@Autowired
	TrackREPO trackRepo;
	
	@Override
	public TripVO getTrip(Map<String,Object> param,Map<String,Object> auth) throws BizException {
		try {
			param.put("authTable", auth);
			return tripRepo.getTrip(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public TripVO getTripFromKey(String key) throws BizException {
		try {
			return tripRepo.getTripFromKey(key);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public TripVO getTripFromMsg(Map<String,Object> param) throws BizException {
		try {
			return tripRepo.getTripFromMsg(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public List<TripVO> getTripList(Map<String,Object> param,Map<String,Object> auth) throws BizException {
		try {
			param.put("authTable", auth);
			return tripRepo.getTripList(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	
	@Override
	public int getTripListCnt(Map<String,Object> param,Map<String,Object> auth) throws BizException {
		try {
			param.put("authTable", auth);
			return tripRepo.getTripListCnt(param);
		} catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
				System.err.println(e.getMessage());
				throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public TripAnalysisVO getTripAnalysis(Map<String,Object> param,Map<String,Object> auth) throws BizException {
		try {
			param.put("authTable", auth);
			return tripAnalysisRepo.getTripAnalysis(param);
		} catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
				System.err.println(e.getMessage());
				throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public TrackInfoVO getTrack_trip(String tripKey, int gpsAccuracy,Map<String, Object> param,Map<String,Object> auth) throws BizException{
		try {
			
			param.put("searchTripKey", tripKey);
			param.put("validTrip", "1");
			
			TrackInfoVO trip = trackRepo.getTrackInfo(param);
			
			if(trip.getTrackRecord() != null && trip.getTrackRecord().size() > 0 && gpsAccuracy > 0){
				fillterGpsAccuracyOut(trip.getTrackRecord(),gpsAccuracy);
			}
				
			return trip;
		} catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
				System.err.println(e.getMessage());
				throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public List<TrackInfoVO> getTrack_date(String vehicleKey, String accountKey , Date yyyymmdd, int gpsAccuracy, Map<String, Object> param,Map<String,Object> auth) throws BizException{
		try {
			
			param.put("searchVehiicleKey", vehicleKey);
			param.put("searchAccountKey", accountKey);
			param.put("searchDate", yyyymmdd);
			param.put("validTrip", "1");
			
			List<TrackInfoVO> trip = trackRepo.getTrackInfoList(param);
			
			if(trip != null && trip.size() > 0){
				for(TrackInfoVO vo : trip){
					if(vo.getTrackRecord() != null && vo.getTrackRecord().size() > 0 && gpsAccuracy > 0)
						fillterGpsAccuracyOut(vo.getTrackRecord(),gpsAccuracy);
				}
				
			}
				
			return trip;
		} catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
				System.err.println(e.getMessage());
				throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public List<TrackInfoVO> getTrack_time(String vehicleKey, String accountKey , Date sDate, Date eDate, int gpsAccuracy, Map<String, Object> param,Map<String,Object> auth) throws BizException{
		try {
			
			param.put("searchVehiicleKey", vehicleKey);
			param.put("searchAccountKey", accountKey);
			param.put("searchStartDate", sDate);
			param.put("searchEndDate", eDate);
			param.put("validTrip", "1");
			
			List<TrackInfoVO> trip = trackRepo.getTrackInfoList(param);
			
			if(trip != null && trip.size() > 0){
				for(TrackInfoVO vo : trip){
					if(vo.getTrackRecord() != null && vo.getTrackRecord().size() > 0 && gpsAccuracy > 0)
						fillterGpsAccuracyOut(vo.getTrackRecord(),gpsAccuracy);
				}
				
			}
				
			return trip;
		} catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
				System.err.println(e.getMessage());
				throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	public void fillterGpsAccuracyOut(List<TrackRecord> trr,int gpsAccuracy){
		for(int i = 0 ; i < trr.size() ;  i++){
			TrackRecord prevVo = null;
			TrackRecord currentVo = null;
			TrackRecord nextVo = null;
			double distance = 0;
			double distanceNext = 0;
			if(i == 0){
				currentVo = trr.get(i);
				if(i+1 < trr.size()){
					nextVo = trr.get(i+1);
				}
				if(nextVo != null){
					distance = GeoUtil.calcDist(
							currentVo.getLon()
							, currentVo.getLat()
							, nextVo.getLon()
							, nextVo.getLat());
				}
				if(distance >= gpsAccuracy){
					currentVo.setGpsAccuracyOut(true);
				}
			}else if(i == trr.size()-1){
				currentVo = trr.get(i);
				if(i-1 > 0){
					prevVo = trr.get(i-1);
				}
				if(prevVo != null){
					distance = GeoUtil.calcDist(
							currentVo.getLon()
							,currentVo.getLat()
							,prevVo.getLon()
							,prevVo.getLat());
				}
				if(distance >= gpsAccuracy){
					currentVo.setGpsAccuracyOut(true);
				}
			}else{
				currentVo = trr.get(i);
				prevVo = trr.get(i-1);
				nextVo = trr.get(i+1);
				distance = GeoUtil.calcDist(
						currentVo.getLon()
						,currentVo.getLat()
						,prevVo.getLon()
						,prevVo.getLat());
				distanceNext = GeoUtil.calcDist(
						currentVo.getLon()
						, currentVo.getLat()
						, nextVo.getLon()
						, nextVo.getLat());
				
				if(distance >= gpsAccuracy || distanceNext >= gpsAccuracy ){
					currentVo.setGpsAccuracyOut(true);
				}
			}
		}
	}
	
	
	
	
	
	
	
}

