package v4.web.rest.service.data.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import v4.ex.exc.BizException;
import v4.web.rest.dao.data.PenaltyREPO;
import v4.web.rest.service.data.PenaltyDataService;
import v4.web.vo.data.PenaltyVO;

@Service("penaltyDataService")
public class PenaltyDataServiceImpl implements PenaltyDataService {

	@Autowired
	PenaltyREPO penaltyREPO;

	@Override
	public List<PenaltyVO> getPenaltyList(Map<String, Object> param) throws BizException {
		try {
			return penaltyREPO.getPenaltyList(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public int getPenaltyListCnt(Map<String, Object> param) throws BizException {
		
		try {
			return penaltyREPO.getPenaltyListCnt(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public int getPenaltyPointCnt(Map<String, Object> param) throws BizException {
		try {
			return penaltyREPO.getPenaltyPointCnt(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public int getPenaltyPriceCnt(Map<String, Object> param) throws BizException {
		try {
			return penaltyREPO.getPenaltyPriceCnt(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
}
