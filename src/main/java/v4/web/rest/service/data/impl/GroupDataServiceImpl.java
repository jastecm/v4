package v4.web.rest.service.data.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import v4.ex.exc.BizException;
import v4.web.rest.dao.data.GroupREPO;
import v4.web.rest.service.data.GroupDataService;
import v4.web.vo.data.GroupVO;



@Service("groupDataService")
public class GroupDataServiceImpl implements GroupDataService {

	@Autowired
	GroupREPO groupRepo;
	@Override
	public GroupVO getGroup(Map<String,Object> param,Map<String,Object> auth) throws BizException {
		try {
			param.put("authTable", auth);
			return groupRepo.getGroup(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public GroupVO getGroupFromKey(String key) throws BizException {
		try {
			return groupRepo.getGroupFromKey(key);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public List<GroupVO> getGroupList(Map<String,Object> param,Map<String,Object> auth) throws BizException {
		try {
			param.put("authTable", auth);
			return groupRepo.getGroupList(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public List<GroupVO> getGroupParentList(Map<String,Object> param,Map<String,Object> auth) throws BizException {
		try {
			param.put("authTable", auth);
			return groupRepo.getGroupParentList(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public List<GroupVO> getGroupChildList(Map<String,Object> param,Map<String,Object> auth) throws BizException {
		try {
			param.put("authTable", auth);
			return groupRepo.getGroupChildList(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
	
	@Override
	public int getGroupListCnt(Map<String,Object> param,Map<String,Object> auth) throws BizException {
		try {
			param.put("authTable", auth);
			return groupRepo.getGroupListCnt(param);
		} catch (Exception e) {
			
			if (e instanceof DataAccessException) {
				
				System.err.println(e.getMessage());
				throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
		
}

