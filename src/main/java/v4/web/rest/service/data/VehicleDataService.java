package v4.web.rest.service.data;

import java.util.List;
import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.data.AccountVO;
import v4.web.vo.data.DeviceVO;
import v4.web.vo.data.GroupVO;
import v4.web.vo.data.TripVO;
import v4.web.vo.data.VehicleLocationVO;
import v4.web.vo.data.VehicleVO;

public interface VehicleDataService {

	VehicleVO getVehicle(Map<String,Object> param,Map<String,Object> auth) throws BizException;
	
	VehicleVO getVehicleFromKey(String key) throws BizException;
	
	List<VehicleVO> getVehicleList(Map<String, Object> param,Map<String,Object> auth) throws BizException;
	
	int getVehicleListCnt(Map<String, Object> param,Map<String,Object> auth) throws BizException;

	List<VehicleLocationVO> getVehicleLocationList(Map<String, Object> param,Map<String,Object> auth) throws BizException;
	
	VehicleVO getVehicleFromPlateNum(String plateNum) throws BizException;

}
	

