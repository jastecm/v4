package v4.web.rest.service.data;

import java.util.List;
import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.data.MaintenanceVO;

public interface MaintenanceDataService {

	List<MaintenanceVO> getMaintenanceList(Map<String, Object> param) throws BizException;

	int getMaintenanceListCnt(Map<String, Object> param) throws BizException;

}
