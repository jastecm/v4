package v4.web.rest.service.data;

import java.util.List;
import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.LoginVO;
import v4.web.vo.data.AccountVO;

public interface AccountDataService {

	
	AccountVO getAccount(Map<String,Object> param,Map<String,Object> auth) throws BizException;

	AccountVO getAccountFromKey(String key,boolean simpleMode) throws BizException;
	
	AccountVO getAccountFromId(String id) throws BizException;
	
	AccountVO getAccountFromIdPw(LoginVO loginVo) throws BizException;
	
	AccountVO getAccountFromPhonePw(LoginVO loginVo) throws BizException;
	
	List<AccountVO> getAccountList(Map<String, Object> param,Map<String,Object> auth) throws BizException;
	
	int getAccountListCnt(Map<String, Object> param,Map<String,Object> auth) throws BizException;
	
}
	

