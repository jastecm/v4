package v4.web.rest.service.data.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import v4.ex.exc.BizException;
import v4.web.rest.dao.data.ExpensesREPO;
import v4.web.rest.service.data.ExpensesDataService;
import v4.web.vo.data.ExpensesTotalVO;
import v4.web.vo.data.ExpensesVO;

@Service("expensesDataService")
public class ExpensesDataServiceImpl implements ExpensesDataService {

	
	@Autowired
	ExpensesREPO expensesRepo;
	
	@Override
	public ExpensesTotalVO getExpensesTotal(Map<String, Object> param) throws BizException {
		try {
			return expensesRepo.getExpensesTotal(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public List<ExpensesVO> getExpensesList(Map<String, Object> param) throws BizException {
		try {
			return expensesRepo.getExpensesList(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public int getExpensesListCnt(Map<String, Object> param) throws BizException {
		try {
			return expensesRepo.getExpensesListCnt(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

}
