package v4.web.rest.service.data.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import v4.ex.exc.BizException;
import v4.web.rest.dao.data.BusREPO;
import v4.web.rest.service.data.BusDataService;
import v4.web.vo.CreateDrivingPurposeVO;
import v4.web.vo.data.BusRouteVO;
import v4.web.vo.data.DrivingPurposeVO;

@Service("busDataService")
public class BusDataServiceImpl implements BusDataService {
	
	@Autowired
	BusREPO busRepo;
	
	@Override
	public List<DrivingPurposeVO> getDrivingPurpose(Map<String, Object> param, Map<String, Object> auth) throws BizException {
		try {
			param.put("authTable", auth);
			return busRepo.getDrivingPurpose(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public int getDrivingPurposeCnt(Map<String, Object> param, Map<String, Object> auth) throws BizException {
		try {
			param.put("authTable", auth);
			return busRepo.getDrivingPurposeCnt(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public List<BusRouteVO> getBusRouteList(Map<String, Object> param) throws BizException {
		try {
			return busRepo.getBusRouteList(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public int getBusRouteListCnt(Map<String, Object> param) throws BizException {
		try {
			return busRepo.getBusRouteListCnt(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public List<Map<String, Object>> getBusAllocateList(Map<String, Object> param) throws BizException {
		try {
			
			return busRepo.getBusAllocateList(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public int getBusAllocateListCnt(Map<String, Object> param) throws BizException {
		try {
			
			return busRepo.getBusAllocateListCnt(param);
		} catch (Exception e) {

			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

}
