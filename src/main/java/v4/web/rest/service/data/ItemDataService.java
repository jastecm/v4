package v4.web.rest.service.data;

import java.util.List;
import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.data.ItemVO;
import v4.web.vo.data.PartsVO;


public interface ItemDataService {
	
	List<ItemVO> getVehicleItems(String vehicleId,String lang) throws BizException;

	ItemVO getVehicleItem(String vehicleId, String itemKey, String lang)  throws BizException;
	
	List<PartsVO> getPartsList(Map<String, Object> param , Map<String, Object> auth) throws BizException;

	int getPartsListCnt(Map<String, Object> param , Map<String, Object> auth)  throws BizException;
	
	PartsVO getPart(String partsKey) throws BizException;

	List<PartsVO> getPartsDistinctList(Map<String, Object> param, Object object) throws BizException;

}
