package v4.web.rest.service.data;

import java.util.Date;
import java.util.List;
import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.data.TrackInfoVO;
import v4.web.vo.data.TripAnalysisVO;
import v4.web.vo.data.TripVO;

public interface TripDataService {

	TripVO getTrip(Map<String,Object> param,Map<String,Object> auth) throws BizException;

	TripVO getTripFromKey(String key) throws BizException;
	
	TripVO getTripFromMsg(Map<String,Object> param) throws BizException;
	
	
	List<TripVO> getTripList(Map<String, Object> param,Map<String,Object> auth) throws BizException;
	
	int getTripListCnt(Map<String, Object> param,Map<String,Object> auth) throws BizException;
	
	TripAnalysisVO getTripAnalysis(Map<String, Object> param,Map<String,Object> auth) throws BizException;
	
	TrackInfoVO getTrack_trip(String tripKey , int gpxAccuracy,Map<String, Object> param,Map<String,Object> auth) throws BizException;

	List<TrackInfoVO> getTrack_date(String vehicleKey, String accountKey ,Date yyyymmdd, int gpsAccuracy, Map<String, Object> param,Map<String,Object> auth) throws BizException;
	
	List<TrackInfoVO> getTrack_time(String vehicleKey, String accountKey , Date sDate , Date eDate, int gpsAccuracy, Map<String, Object> param,Map<String,Object> auth) throws BizException;
}
	

