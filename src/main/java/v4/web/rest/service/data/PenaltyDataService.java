package v4.web.rest.service.data;

import java.util.List;
import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.data.PenaltyVO;

public interface PenaltyDataService {

	List<PenaltyVO> getPenaltyList(Map<String, Object> param) throws BizException;

	int getPenaltyListCnt(Map<String, Object> param) throws BizException;

	int getPenaltyPointCnt(Map<String, Object> param) throws BizException;

	int getPenaltyPriceCnt(Map<String, Object> param) throws BizException;

}
