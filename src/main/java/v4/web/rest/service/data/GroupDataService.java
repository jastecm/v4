package v4.web.rest.service.data;

import java.util.List;
import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.data.AccountVO;
import v4.web.vo.data.DeviceVO;
import v4.web.vo.data.GroupVO;
import v4.web.vo.data.TripVO;
import v4.web.vo.data.VehicleVO;

public interface GroupDataService {

	GroupVO getGroup(Map<String,Object> param,Map<String,Object> auth) throws BizException;

	GroupVO getGroupFromKey(String key) throws BizException;
	
	List<GroupVO> getGroupList(Map<String, Object> param,Map<String,Object> auth) throws BizException;
	
	List<GroupVO> getGroupParentList(Map<String, Object> param,Map<String,Object> auth) throws BizException;
	
	List<GroupVO> getGroupChildList(Map<String, Object> param,Map<String,Object> auth) throws BizException;
	
	int getGroupListCnt(Map<String, Object> param,Map<String,Object> auth) throws BizException;
	
}
	

