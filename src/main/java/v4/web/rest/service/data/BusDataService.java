package v4.web.rest.service.data;

import java.util.List;
import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.CreateDrivingPurposeVO;
import v4.web.vo.data.BusRouteVO;
import v4.web.vo.data.DrivingPurposeVO;

public interface BusDataService {
	
	List<DrivingPurposeVO> getDrivingPurpose(Map<String,Object> param,Map<String,Object> auth) throws BizException;

	int getDrivingPurposeCnt(Map<String,Object> param,Map<String,Object> auth) throws BizException;

	List<BusRouteVO> getBusRouteList(Map<String, Object> param) throws BizException;

	int getBusRouteListCnt(Map<String, Object> param) throws BizException;

	List<Map<String, Object>> getBusAllocateList(Map<String, Object> param) throws BizException;

	int getBusAllocateListCnt(Map<String, Object> param) throws BizException;
	
}
