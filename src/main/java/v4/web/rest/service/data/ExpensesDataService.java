package v4.web.rest.service.data;

import java.util.List;
import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.data.ExpensesTotalVO;
import v4.web.vo.data.ExpensesVO;

public interface ExpensesDataService {

	ExpensesTotalVO getExpensesTotal(Map<String, Object> param) throws BizException;

	List<ExpensesVO> getExpensesList(Map<String, Object> param) throws BizException;

	int getExpensesListCnt(Map<String, Object> param) throws BizException;

}
