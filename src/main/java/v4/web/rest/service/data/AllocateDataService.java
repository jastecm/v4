package v4.web.rest.service.data;

import java.util.List;
import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.data.AllocateVO;

public interface AllocateDataService {

	
	List<AllocateVO> getAllocateList(Map<String,Object> param,Map<String,Object> auth) throws BizException;

	AllocateVO getAllocate(Map<String,Object> param,Map<String,Object> auth) throws BizException;
	
	AllocateVO getAllocateFromKey(String key) throws BizException;
	
	int getAllocateListCnt(Map<String, Object> param,Map<String,Object> auth) throws BizException;
	
}
	

