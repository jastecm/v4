package v4.web.rest.service.data.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import v4.ex.exc.BizException;
import v4.web.rest.dao.data.ItemREPO;
import v4.web.rest.service.data.ItemDataService;
import v4.web.vo.data.ItemVO;
import v4.web.vo.data.PartsVO;

@Service("itemDataService")
public class ItemDataServiceImpl implements ItemDataService {
	
	@Autowired
	ItemREPO itemRepo;


	@Override
	public List<ItemVO> getVehicleItems(String vehicleId, String lang) throws BizException {
		try {
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("searchVehicleKey", vehicleId);
			if(StrUtil.isNullToEmpty(lang)) lang = "ko";
			param.put("lang", lang);
			
			return itemRepo.getVehicleItems(param);
		} catch (Exception e) {
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}



	@Override
	public ItemVO getVehicleItem(String vehicleId, String itemKey, String lang) throws BizException {
		try {
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("searchVehicleKey", vehicleId);
			param.put("searchItemKey", itemKey);
			if(StrUtil.isNullToEmpty(lang)) lang = "ko";
			param.put("lang", lang);
			
			return itemRepo.getVehicleItem(param);
		} catch (Exception e) {
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}



	@Override
	public List<PartsVO> getPartsList(Map<String, Object> param, Map<String, Object> auth) throws BizException {
		try {
			param.put("authTable", auth);
			
			return itemRepo.getPartsList(param);
		} catch (Exception e) {
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}



	@Override
	public int getPartsListCnt(Map<String, Object> param, Map<String, Object> auth) throws BizException {
		try {
			param.put("authTable", auth);
			
			return itemRepo.getPartsListCnt(param);
		} catch (Exception e) {
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}

	@Override
	public PartsVO getPart(String partsKey) throws BizException {
		try {
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("searchPartsKey",partsKey);
			
			return itemRepo.getPart(param);
		} catch (Exception e) {
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}



	@Override
	public List<PartsVO> getPartsDistinctList(Map<String, Object> param, Object auth) throws BizException {
		try {
			param.put("authTable", auth);
			
			return itemRepo.getPartsDistinctList(param);
		} catch (Exception e) {
			if (e instanceof DataAccessException) {
				
			     System.err.println(e.getMessage());
			     throw new BizException("***** SQLException : "+e.getClass().getName(),(DataAccessException)e);
			}
			
			throw new BizException(e.getClass().getName() + " : " + e.getMessage(),e);
		}
	}
}
