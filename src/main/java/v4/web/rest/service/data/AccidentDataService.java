package v4.web.rest.service.data;

import java.util.List;
import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.data.AccidentVO;

public interface AccidentDataService {

	List<AccidentVO> getAccidentList(Map<String, Object> param) throws BizException;

	int getAccidentListCnt(Map<String, Object> param) throws BizException;

}
