package v4.web.rest.service;

import java.util.List;

import v4.ex.exc.BizException;
import v4.web.vo.CreateItemVO;
import v4.web.vo.ItemVO;
import v4.web.vo.UpdateItemVO;
import v4.web.vo.data.TripVO;
import v4.web.vo.data.VehicleVO;


public interface ItemService {
	
	String createItem(CreateItemVO itemVo)  throws BizException;

	void updateItem(UpdateItemVO itemVo)  throws BizException;

	void deleteVehicleItem(String vehicleKey, String itemKey)  throws BizException;
	
	int updateItemByTrip(TripVO vo) throws BizException;
}
