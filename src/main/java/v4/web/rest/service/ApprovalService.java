package v4.web.rest.service;

import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.CreateMaintenanceVO;

public interface ApprovalService {

	Map<String, Object> getMaintenanceApproval(CreateMaintenanceVO createMaintenanceVO) throws BizException;

}
