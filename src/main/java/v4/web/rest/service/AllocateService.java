package v4.web.rest.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.AccountVO;
import v4.web.vo.AllocateVO;
import v4.web.vo.CreateAllocateVO;
import v4.web.vo.LocationVO;
import v4.web.vo.TripVO;
import v4.web.vo.VehicleVO;

public interface AllocateService {
	
	void deleteAllocate(String allocateKey) throws BizException;
	
	CreateAllocateVO createAlloccate(CreateAllocateVO vo) throws BizException;
	
	//사용미확인
	/*
	AllocateVO getMyAllocate(String userKey) throws BizException;
	
	List<VehicleVO> getAllocateListBeCan(AllocateVO vo) throws BizException;

	

	CreateAllocateVO createAlloccateApproval(CreateAllocateVO vo) throws BizException;

	

	void updateAllocateApprovalComplate(String allocateKey);

	AllocateVO getAllocate(AllocateVO chker);

	LocationVO allocateReleaseChk(AllocateVO chker) throws BizException;

	void allocateReturn(String allocateKey, String location, String locationAddr, String locationAddrDetail) throws BizException;

	HashMap<String, String> getNextAllocate(Map<String, Object> param);

	void updateAllocateExtend(Map<String, Object> param);

	AllocateVO getAllocateApproval(Map<String, Object> param);

	void updateApprovalState(String allocateKey, String accountKey, String step);

	void updateInstantAllocateApprovalComplate(String allocateKey);

	void updateApprovalReject(String allocateKey, String accountKey, String step);

	void updateInstantAllocateApprovalReject(String allocateKey);

	void updateApprovalRejectAllocateKeyUpdate(String allocateKey);

	void returnAllocate(String allocateKey) throws BizException;

	AllocateVO getAllocateMatchedResult(TripVO vo, boolean insert) throws BizException;

	List<AllocateVO> getAlocateList(AccountVO account, Map<String, Object> param);
	
	String getVehicleAllocateUser(String vehicleKey) throws BizException;
	*/
}
