package v4.web.rest.service;

import java.util.List;
import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.AccountVO;
import v4.web.vo.PushControllVO;

public interface PushService {

	void pushKeyRefresh(String accountKey, String key) throws BizException;

	void pushSubscribe(String accountKey, String topic, String accept) throws BizException;

	void push(String accountKey, String topic, String title,String msg) throws BizException;

	void pushDenyUpdate(String userKey, String denyType, String accept) throws BizException;

	Map<String,Object> getDenyList(String userKey) throws BizException;
	
	List<Map<String, String>> getPushList(Map<String, Object> param) throws BizException;

	void delPushList(String pushKey) throws BizException;


	void updatePushControllState(PushControllVO vo) throws BizException;

	List<PushControllVO> getPushControllList(String accountKey) throws BizException;
	
	PushControllVO getPushControll(String accountKey,String pushType) throws BizException;

	void updatePushControllLastest(String accountKey,String pushType,String val) throws BizException;

	void insertPushHstr(String corpKey , String pushType , String title , String msg) throws BizException;

	void updatePushControllAll(String userKey, String state)throws BizException;
	
	void pushChkAccept(String pushType , String accountKey, String topic, String title,String msg) throws BizException;
}
	

