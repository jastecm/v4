package v4.web.rest.service;

import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.CreateBusAllocateVO;
import v4.web.vo.CreateBusFavoritesVO;
import v4.web.vo.CreateBusRouteStationVO;
import v4.web.vo.CreateDrivingPurposeVO;
import v4.web.vo.UpdateBusRouteStationVO;
import v4.web.vo.UpdateDrivingPurposeVO;

public interface BusService {
	
	int createDrivingPurpose(CreateDrivingPurposeVO drivingPurposeVo) throws BizException;

	void deleteDrivingPurpose(Map<String, Object> param, Map<String, Object> auth) throws BizException;

	void updateDrivingPurpose(UpdateDrivingPurposeVO updateDrivingPurposeVo) throws BizException;

	void createBusRouteStation(CreateBusRouteStationVO busRouteStationVO) throws BizException;

	void deleteBusRouteStation(Map<String, Object> param) throws BizException;

	void updateBusRouteStation(UpdateBusRouteStationVO busRouteStationVO) throws BizException;

	boolean busAllocateCheck(Map<String, Object> param) throws BizException;

	void createBusAllocate(CreateBusAllocateVO busAllocateVO) throws BizException;

	void deleteBusAllocate(Map<String, Object> param) throws BizException;

	void createBusFavorites(CreateBusFavoritesVO busFavoritesVO) throws BizException;

	void deleteBusFavorites(Map<String, Object> param) throws BizException;
	
}
