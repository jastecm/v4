package v4.web.rest.service;

import java.util.List;
import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.EventVO;


public interface TimeLineService {
	
	void insertTimeLineEvent(String type, String sub,Map<String,Object> vo) throws BizException;

	List<EventVO> getTimeLine(String userKey, String vehicleKey, String searchYm, String fillter, String offset,
			int limit,String scope, String lang) throws BizException;

	int getTimeLineChk(String vehicleKey, String searchYm, String scope, String accountKey) throws BizException;

}
