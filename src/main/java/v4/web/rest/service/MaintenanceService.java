package v4.web.rest.service;

import v4.ex.exc.BizException;
import v4.web.vo.CreateMaintenanceDetailVO;
import v4.web.vo.CreateMaintenanceVO;

public interface MaintenanceService {

	void createMaintenance(CreateMaintenanceVO createMaintenanceVO) throws BizException;

	void createMaintenanceDetil(CreateMaintenanceDetailVO createMaintenanceDetailVO) throws BizException;

}
