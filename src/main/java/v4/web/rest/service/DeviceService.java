package v4.web.rest.service;

import java.util.Map;

import org.jastecm.lora.vo.extention.LoraMsg_FAULT;
import org.jastecm.lora.vo.extention.LoraMsg_HELLO;
import org.jastecm.lora.vo.extention.LoraMsg_PROVISION;

import v4.ex.exc.BizException;
import v4.web.vo.DeviceVO;







public interface DeviceService {

	DeviceVO getDeviceFromSn(String deviceSn)  throws BizException ;

	DeviceVO getDeviceFromId(String deviceId)  throws BizException ;
	
	DeviceVO getDeviceFromKey(String deviceKey)  throws BizException ;
	
	DeviceVO getActiveDeviceChkFromId(String deviceId)  throws BizException ;
	
	DeviceVO getActiveDeviceChkFromSn(String deviceSn)  throws BizException ;

	DeviceVO activeDevice(DeviceVO dvo)  throws BizException ;

	DeviceVO getHstrDeviceFromId(String corpKey, String deviceId)  throws BizException ;

	void updateConnectionKey(String deviceId, String connectionKey)  throws BizException ;

	void updateDeviceShiftDay(String deviceId, long shiftDay)  throws BizException ;

	String getDeviceShiftDay(String deviceId)  throws BizException ;

	void setDeviceHello(LoraMsg_HELLO vo)  throws BizException ;

	void setDeviceProvision(LoraMsg_PROVISION vo)  throws BizException ;

	void setDeviceFault(LoraMsg_FAULT vo)  throws BizException ;

	boolean isDeviceMaster(String deviceKey, String accountKey)  throws BizException ;

	void updateDownloadInfo(Map<String, Object> param)  throws BizException ;

	void updateLatestPacket(String deviceId)  throws BizException ;

	

	



}
	

