package v4.web.rest.service;

import java.util.List;
import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.CreateAccidentVO;
import v4.web.vo.UpdateAccidentResultVO;

public interface AccidentService {

	void createAccident(CreateAccidentVO createAccidentVO) throws BizException;

	List<Map<String, Object>> getInsureList() throws BizException;

	void updateAccidentResult(UpdateAccidentResultVO updateAccidentResultVO) throws BizException;

}
