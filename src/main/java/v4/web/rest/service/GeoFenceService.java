package v4.web.rest.service;

import v4.ex.exc.BizException;
import v4.web.vo.data.VehicleVO;

public interface GeoFenceService {

	void geoFence(VehicleVO vvo,String driverAccountKey,String tripKey, double lat, double lon) throws BizException;
	
	/*
	GeoFenceVO getGeoFenceAllChk(CreateGeoFenceVO vo) throws BizException;
	
	void addGeoFence(CreateGeoFenceVO vo,int max) throws BizException;
	
	List<GeoFenceVO> getGeoFenceList(String vehicleKey,String corpKey,String all) throws BizException;
	
	GeoFenceVO getGeoFence(String fenceKey) throws BizException;
	
	void updateGeoFence(CreateGeoFenceVO vo) throws BizException;
	
	void deleteGeoFence(String fenceKey) throws BizException;
	*/
	
	/*
	void updateGeoFencePush(String fenceKey , String push) throws BizException;

	void updateGeofenceOnOff(String key, String val) throws BizException;

	List<Map<String, String>> getGeoFenceUserList(UserVO user, Map<String, String> param);

	List<Map<String, String>> getGeoFenceUserDetail(UserVO user, Map<String, String> param);

	void deleteGeoFenceHstr(String fenceKey);

	int getGeofenceCnt(UserVO user, Map<String, String> param);
	*/
	


	
}
	

