package v4.web.rest.service;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import v4.web.vo.FileVO;

public interface CommonService {

	List<HashMap<String, Object>> getTree();
	
	Map<String, Object> getCorp(Map<String,Object> param);
	
	//그룹키를 가지고 부서장을 뽑아온다.
	String getGroupManagerAccountKey(String groupKey);
	
	public HashMap<String,String> getFileUpload(Iterator iter, File fMidir, String accountKey);
	
	public FileVO getFile(String vo) throws Exception;
	/*
	public String getAuthId(String table) throws BizException;

	public List<CodeVO> getCodeList(CodeVO vo) throws BizException;

	public List<String> getDeviceSeries(UserVO user) throws BizException;

	public List<Map<String, String>> getDeviceList(UserVO user, String deviceSeries, String free) throws BizException;

	public List<UserVO> getUserList(UserVO user, Map<String, String> param);

	public List<VehicleVO> getVehicleList(UserVO user, Map<String, String> param);
	
	public VehicleVO getVehicle(UserVO user, Map<String, String> param);

	public UserVO getUser(UserVO user, Map<String, String> param);
	
	public String insertFileUpload(FileVO vo) throws Exception;
	
	public FileVO getFile(String vo) throws Exception;
	
	public HashMap<String,String> getFileUpload(Iterator iter, File fMidir);

	public List<GroupVO> getGroupList(UserVO user, Map<String, String> param);

	public String getDeivceIdFromSn(String deviceSn) throws BizException;

	public Map<String, Integer> getVehicleCnt(UserVO user) throws BizException;

	public Map<String, Integer> getUserCnt(UserVO user) throws BizException;

	public List<Map<String, String>> getCityLev1(String lang) throws BizException;

	public List<Map<String, String>> getCityLev2(String lang, String city) throws BizException;

	public List<Map<String,String>> getGroupCode(UserVO user) throws BizException;

	public List<Map<String,String>> getGroupUserList(UserVO user, String corpGorup) throws BizException;

	public List<Map<String, Object>> getCorpList(Map<String, String> param) throws BizException;

	public List<Map<String, Object>> getMasterList(Map<String, String> param) throws BizException;
	
	public Object getGroupVehicleList(UserVO user, String lang, String corpGroup) throws BizException;

	public Map<String, Integer> getDrivingCnt(UserVO user) throws BizException;

	public Map<String, Integer> getSearchDrivingCnt(UserVO user, Map<String, Object> param) throws BizException;

	public int sendReport(Map<String, String> param) throws BizException;

	public Map<String, Integer> getReportCnt(UserVO user) throws BizException;

	public List<Map<String,String>> getAllocateList(UserVO user, String searchDate) throws BizException;

	public List<Map<String,String>> getCode(UserVO user, String code, String val) throws BizException;

	public List<VehicleVO> getDeviceVehicleList(UserVO user, Map<String, String> param) throws BizException;
	
	public int getDistance(String from , String to) throws BizException;
	*/
}
	

