package v4.web.rest.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.AccountVO;
import v4.web.vo.CreateCorpVO;
import v4.web.vo.CreateInviteVO;
import v4.web.vo.CreatePartnerVO;
import v4.web.vo.CreatePersonVO;
import v4.web.vo.CreateRentVO;
import v4.web.vo.CreateUserVO;
import v4.web.vo.LoginVO;







public interface AccountService {

	public AccountVO getAccountFromIdPw(LoginVO vo) throws BizException;
	public AccountVO getAccountFromId(String userId) throws BizException;
	public AccountVO getAccount(String userKey) throws BizException;
	public AccountVO getAccountSession(String userKey) throws BizException;
	
	public List<AccountVO> getAccountList(Map<String,Object> param , Map<String,Object> auth) throws BizException;
	public int getAccountListCnt(Map<String,Object> param , Map<String,Object> auth) throws BizException;
	
	
	
	public boolean createUser(CreatePersonVO vo,int rule,int subRule) throws BizException;
	public boolean createUser(CreateInviteVO vo,int rule,int subRule) throws BizException;
	public boolean createCorpUser(CreateCorpVO vo,int rule,int subRule) throws BizException;
	public boolean createRentUser(CreateRentVO vo, int rule, int subRule) throws BizException;
	public boolean createPartnerUser(CreatePartnerVO vo,int rule,int subRule) throws BizException;
	
	public boolean createUser(CreateUserVO vo,int rule,int subRule) throws BizException;
	public boolean createUser(List<CreateUserVO> vo, int rule, int subRule) throws BizException;
	public void accountUseStop(String accountKey);
	public int getAllAccountCount(Map<String, Object> param);
	public void updateAccureData(String driver, Integer distance, Date startDate, Date endDate) throws BizException;
	public void updateAccureDataReverse(String driver,String tripKey) throws BizException;
}
	

