package v4.web.rest.service;

import java.util.List;
import java.util.Map;

import v4.ex.exc.BizException;
import v4.web.vo.VehicleDbVO;







public interface VehicleDBService {

	List<String> getManufacture(String lang) throws BizException;

	List<String> getModelMaster(String lang, String manufacture) throws BizException;

	List<String> getYear(String lang, String manufacture, String modelMaster) throws BizException;

	List<String> getModelHeader(String lang, String manufacture,
			String modelMaster, String year) throws BizException;

	List<String> getTransmission(String lang, String manufacture,
			String modelMaster, String year, String modelHeader) throws BizException;

	List<String> getFuelType(String lang, String manufacture,
			String modelMaster, String year, String modelHeader) throws BizException;

	List<String> getVolume(String lang, String manufacture, String modelMaster,
			String year, String modelHeader) throws BizException;

	Map<String, String> getConvCode(VehicleDbVO vo) throws BizException;

	Map<String, String> getVehicleDbInfo(String lang, String convCode) throws BizException;
}
	

