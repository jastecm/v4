package v4.web.rest.service;

import java.util.List;
import java.util.Map;

import v4.web.vo.AccountVO;
import v4.web.vo.ClinicVO;
import v4.web.vo.CreateClinicVO;
import v4.web.vo.CreateMaintenanceCompanyVO;
import v4.web.vo.CreateVehiclePartsEtcVO;
import v4.web.vo.MaintenanceCompanyVO;
import v4.web.vo.UpdateClinicVO;
import v4.web.vo.VehiclePartsEtcVO;
import v4.web.vo.VehiclePartsVO;

public interface ClinicService {

	String addMaintenance(AccountVO accountVo, CreateClinicVO vo);

	void deleteMaintenance(Map<String, Object> param);

	void updateMainteance(UpdateClinicVO vo);

	ClinicVO getMaintenance(Map<String, Object> param);

	void updateApprovalState(String maintenanceKey, String accountKey, String step);

	List<ClinicVO> getMaintenanceApprovalList(Map<String, Object> param);

	void addMaintenanceDetail(String maintenanceKey, CreateClinicVO vo);

	List<MaintenanceCompanyVO> getMaintenanceCompanyList(Map<String, Object> param);

	void deleteMaintenanceCompany(Map<String, Object> param);

	String insertMaintenanceCompany(CreateMaintenanceCompanyVO vo);

	List<VehiclePartsVO> getVehiclePartsList(Map<String, Object> param);

	List<Map<String,Object>> getVehiclePartTypeList();
	
	List<VehiclePartsEtcVO> getVehiclePartsEtcList(Map<String, Object> param);

	void deleteVehiclePartsEtc(Map<String, Object> param);

	String createVehiclePartsEtc(CreateVehiclePartsEtcVO vo);

	String addMaintenanceEtc(AccountVO accountVo, CreateClinicVO vo);

	void addMaintenanceEtcDetail(String maintenanceKey, CreateClinicVO vo);

	void deleteMaintenanceEtc(Map<String, Object> param);

	void updateApprovalStateEtc(String maintenanceKey, String accountKey, String step);

}
