package v4.web.rest.service;

import v4.ex.exc.BizException;
import v4.web.vo.OilVO;





public interface OilService {
	public void insertOilPrice(OilVO vo) throws Exception;
	public OilVO getUnitOilPrice(String date) throws BizException;

	
}
	

