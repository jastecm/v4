package v4.web.rest.api;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import org.jastecm.string.StrUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javassist.NotFoundException;
import v4.ex.exc.BizException;
import v4.ex.exc.CreateResponseException;
import v4.ex.exc.extents.UnauthorizedException;
import v4.ex.exc.extents.UserNotFoundException;
import v4.ex.exc.extents.VehicleNotFoundException;
import v4.web.controller.BasicWebViewController;
import v4.web.rest.service.BusService;
import v4.web.rest.service.TokenService;
import v4.web.rest.service.data.BusDataService;
import v4.web.rest.service.data.VehicleDataService;
import v4.web.vo.CreateBusAllocateVO;
import v4.web.vo.CreateBusFavoritesVO;
import v4.web.vo.CreateBusRouteStationVO;
import v4.web.vo.CreateDrivingPurposeVO;
import v4.web.vo.TokenVO;
import v4.web.vo.UpdateBusRouteStationVO;
import v4.web.vo.UpdateDrivingPurposeVO;
import v4.web.vo.UpdateItemVO;
import v4.web.vo.data.BusMappingVO;
import v4.web.vo.data.BusRouteVO;
import v4.web.vo.data.DrivingPurposeVO;
import v4.web.vo.data.ItemVO;
import v4.web.vo.data.VehicleVO;

@Api(value="busApi")
@RestController
public class Bus extends BasicWebViewController {
	
	
	ObjectMapper mapper = new ObjectMapper();
	
	@Autowired
	TokenService tokenService;
	
	@Autowired
	BusService busService;
	
	@Autowired
	BusDataService busDataService;
	
	@Autowired
	VehicleDataService vehicleDataService;
	
	@ApiOperation(value = "버스 노선 조회", notes = "버스 노선을 조회한다." , response = v4.web.vo.data.DrivingPurposeVO.class , responseContainer = "List")		
	@ApiResponses(value = {
			@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my vehicles"),
			@ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
			@ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
			@ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/busRouteStation" , method=RequestMethod.GET)
	public ResponseEntity busRouteStationList(HttpServletRequest req,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@RequestParam(value="limit" , defaultValue = "10")
			int limit
			,@RequestParam(value="offset" , defaultValue = "0")
			int offset
			,@RequestParam(value="searchType" , required = false)
			String searchType
			,@RequestParam(value="searchText" , required = false)
			String searchText
			,@RequestParam(value="searchKeyword" , required = false)
			String searchKeyword
			,@RequestParam(value="busRouteKey" , required = false)
			String busRouteKey
			,@RequestParam(value="drivingPurpose" , required = false)
			String drivingPurpose
			,@RequestParam(value="counting" , defaultValue = "false")
			boolean counting
			) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
			return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
		
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("searchCorpKey", t.getCorpKey());
		param.put("limit", limit);
		param.put("offset", offset);
		param.put("searchType", searchType);
		param.put("searchText", searchText);
		param.put("busRouteKey", busRouteKey);
		param.put("drivingPurpose", drivingPurpose);
		
		//키워드 검색
		param.put("searchKeyword", searchKeyword);
		
		String order = req.getParameter("searchOrder");
		param.put("searchOrder", order);
		
		String favorites = req.getParameter("favorites");
		param.put("favorites", favorites);
		param.put("userKey", t.getUserKey());
		
		List<BusRouteVO> rtvList = busDataService.getBusRouteList(param);
		
		Map<String,Object> rtv = new HashMap<String,Object>();
		rtv.put("result", rtvList);
		
		if(counting){
			int totCnt = busDataService.getBusRouteListCnt(param);
			rtv.put("totalCnt", totCnt);
		}
		
		return createResponse(HttpStatus.OK,true,rtv);
	}
	
	@ApiOperation(value = "버스 노선 등록", notes = "버스 노선을 등록한다." , response = CreateBusRouteStationVO.class)		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/busRouteStation" , method=RequestMethod.POST)
	public ResponseEntity createBusRouteStation(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@RequestBody(required = true)
			@Valid
			CreateBusRouteStationVO busRouteStationVO
			,BindingResult br
			) throws CreateResponseException, UnauthorizedException, BizException , NotFoundException, UserNotFoundException, VehicleNotFoundException
	{
		
		if(br.hasErrors())
			return createValidErrorResponse(response,new String[]{"result"}, br ,null);
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		busRouteStationVO.setCorp(t.getCorpKey());
		busRouteStationVO.setRegUser(t.getUserKey());
		
		busService.createBusRouteStation(busRouteStationVO);
		
		return createResponse(HttpStatus.OK);
	}
	
	@ApiOperation(value = "버스 노선 수정", notes = "버스 노선을 수정한다.")		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/busRouteStation" , method=RequestMethod.PUT)
	public ResponseEntity updateBusRouteStation(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@RequestBody(required = true)
			@Valid
			UpdateBusRouteStationVO busRouteStationVO
			,BindingResult br
			) throws CreateResponseException, UnauthorizedException, BizException , NotFoundException, UserNotFoundException, VehicleNotFoundException
	{
		
		if(br.hasErrors())
			return createValidErrorResponse(response,new String[]{"result"}, br ,null);
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null)
			throw new UnauthorizedException();
		
		busRouteStationVO.setCorp(t.getCorpKey());
		
		busService.updateBusRouteStation(busRouteStationVO);
		
		return createResponse(HttpStatus.OK);
	}
	
	@ApiOperation(value = "버스 노선 삭제", notes = "버스노선 정보를 삭제한다.")		
	@ApiResponses(value = {
			@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/busRouteStation/{busRouteKeys}" , method=RequestMethod.DELETE)
	public ResponseEntity deleteBusRouteStation(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@PathVariable(value="busRouteKeys") 
			@ApiParam(value = "target busRouteKeys arr - delimeter ',' ")
			String busRouteKeys
			) throws CreateResponseException, UnauthorizedException, BizException ,NotFoundException, UserNotFoundException, VehicleNotFoundException
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("corp", t.getCorpKey());
		
		List<String> busRouteKey = Arrays.asList(busRouteKeys.split(","));
		
		for(int i = 0 ; i < busRouteKey.size() ; i++){
			param.put("busRouteKey", busRouteKey.get(i));
			busService.deleteBusRouteStation(param);
		}
		
		return createResponse(HttpStatus.OK);
	}
	
	@ApiOperation(value = "버스 배차 조회", notes = "버스 배차를 조회한다." , response = v4.web.vo.data.DrivingPurposeVO.class , responseContainer = "List")		
	@ApiResponses(value = {
			@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my vehicles"),
			@ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
			@ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
			@ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/busAllocate" , method=RequestMethod.GET)
	public ResponseEntity busAllocateList(HttpServletRequest req,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@RequestParam(value="limit" , defaultValue = "10")
			int limit
			,@RequestParam(value="offset" , defaultValue = "0")
			int offset
			,@RequestParam(value="counting" , defaultValue = "false")
			boolean counting
			) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
			return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
		
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("searchCorpKey", t.getCorpKey());
		param.put("limit", limit);
		param.put("offset", offset);
		
		String order = req.getParameter("searchOrder");
		param.put("searchOrder", order);
		
		String searchStartDate = req.getParameter("searchStartDate");
		String searchEndDate = req.getParameter("searchEndDate");
		param.put("searchStartDate", searchStartDate);
		param.put("searchEndDate", searchEndDate);
		
		List<Map<String,Object>> rtvList = busDataService.getBusAllocateList(param);
		
		Map<String,Object> rtv = new HashMap<String,Object>();
		rtv.put("result", rtvList);
		
		if(counting){
			int totCnt = busDataService.getBusAllocateListCnt(param);
			rtv.put("totalCnt", totCnt);
		}
		
		return createResponse(HttpStatus.OK,true,rtv);
	}
	
	@ApiOperation(value = "버스 배차 검증 조회", notes = "버스 배차 검증 조회한다.")		
	@ApiResponses(value = {
			@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my vehicles"),
			@ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
			@ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
			@ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/busAllocateCheck" , method=RequestMethod.GET)
	public ResponseEntity busAllocateCheck(HttpServletRequest req,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
			return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
		
		
		String busRouteKey = req.getParameter("busRouteKey");
		String allocateDate = req.getParameter("allocateDate");
		String vehicleKey = req.getParameter("vehicleKey");
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("busRouteKey",busRouteKey);
		param.put("allocateDate",allocateDate);
		param.put("vehicleKey",vehicleKey);
		param.put("corp",t.getCorpKey());
		
		Map<String,Object> rtv = new HashMap<String,Object>();
		if(busService.busAllocateCheck(param)){
			rtv.put("result", "OK");
		}else{
			rtv.put("result", "FAIL");
		}
		
		return createResponse(HttpStatus.OK,true,rtv);
		
	}
	
	@ApiOperation(value = "버스 배차 등록", notes = "버스 배차을 등록한다.")		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/busAllocate" , method=RequestMethod.POST)
	public ResponseEntity createBusAllocate(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@RequestBody(required = true)
			@Valid
			CreateBusAllocateVO busAllocateVO
			,BindingResult br
			) throws CreateResponseException, UnauthorizedException, BizException , NotFoundException, UserNotFoundException, VehicleNotFoundException
	{
		
		if(br.hasErrors())
			return createValidErrorResponse(response,new String[]{"result"}, br ,null);
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		busAllocateVO.setCorp(t.getCorpKey());
		
		busService.createBusAllocate(busAllocateVO);
		
		
		return createResponse(HttpStatus.OK);
	}
	
	@ApiOperation(value = "버스 배차 삭제", notes = "버스 배차 정보를 삭제한다.")		
	@ApiResponses(value = {
			@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/busAllocate/{busMappingKeys}" , method=RequestMethod.DELETE)
	public ResponseEntity deleteBusAllocate(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@PathVariable(value="busMappingKeys") 
			@ApiParam(value = "target busMappingKeys arr - delimeter ',' ")
			String busMappingKeys
			) throws CreateResponseException, UnauthorizedException, BizException ,NotFoundException, UserNotFoundException, VehicleNotFoundException
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("corp", t.getCorpKey());
		
		List<String> busMappingKey = Arrays.asList(busMappingKeys.split(","));
		
		for(int i = 0 ; i < busMappingKey.size() ; i++){
			param.put("busMappingKey", busMappingKey.get(i));
			busService.deleteBusAllocate(param);
		}
		
		return createResponse(HttpStatus.OK);
	}
	
	@ApiOperation(value = "운행 목적 조회", notes = "자신이 접근 운행 목적을 조회한다." , response = v4.web.vo.data.DrivingPurposeVO.class , responseContainer = "List")		
	@ApiResponses(value = {
			@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my vehicles"),
			@ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
			@ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
			@ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/drivingPurpose" , method=RequestMethod.GET)
	public ResponseEntity getVehicleList(HttpServletRequest req,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@RequestParam(value="limit" , defaultValue = "10")
			int limit
			,@RequestParam(value="offset" , defaultValue = "0")
			int offset
			,@RequestParam(value="searchText" , required = false)
			String searchText
			,@RequestParam(value="counting" , defaultValue = "false")
			boolean counting
			) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
			return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
		
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("searchCorpKey", t.getCorpKey());
		param.put("limit", limit);
		param.put("offset", offset);
		param.put("drivingPurposeName", searchText);
		
		List<DrivingPurposeVO> rtvList = busDataService.getDrivingPurpose(param, null);
		
		Map<String,Object> rtv = new HashMap<String,Object>();
		rtv.put("result", rtvList);
		
		if(counting){
			int totCnt = busDataService.getDrivingPurposeCnt(param,null);
			rtv.put("totalCnt", totCnt);
		}
		
		
		return createResponse(HttpStatus.OK,true,rtv);
		
	}
	
	
	@ApiOperation(value = "운행 목적 등록", notes = "운행 목적을 등록한다." , response = DrivingPurposeVO.class)		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/drivingPurpose" , method=RequestMethod.POST)
	public ResponseEntity createDrivingPurpose(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@RequestBody(required = true)
			@Valid
			CreateDrivingPurposeVO drivingPurposeVo
			,BindingResult br
			) throws CreateResponseException, UnauthorizedException, BizException , NotFoundException, UserNotFoundException, VehicleNotFoundException
	{
		
		if(br.hasErrors())
			return createValidErrorResponse(response,new String[]{"result"}, br ,null);
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		drivingPurposeVo.setCorp(t.getCorpKey());
		
		int seq = busService.createDrivingPurpose(drivingPurposeVo);
		
		return createResponse(HttpStatus.OK, new String[]{"result"}, seq);
	}
	
	@ApiOperation(value = "운행목적 삭제", notes = "운행목적 정보를 삭제한다.")		
	@ApiResponses(value = {
			@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/drivingPurpose/{seq}" , method=RequestMethod.DELETE)
	public ResponseEntity deleteDrivingPurpose(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@PathVariable(value="seq") 
			@ApiParam(value = "target seq")
			int seq				
			) throws CreateResponseException, UnauthorizedException, BizException ,NotFoundException, UserNotFoundException, VehicleNotFoundException
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("corpKey", t.getCorpKey());
		param.put("seq", seq);
		
		busService.deleteDrivingPurpose(param,null);
		
		return createResponse(HttpStatus.OK);
	}
	
	@ApiOperation(value = "운행 목적 갱신", notes = "운행 목적을 갱신한다." )		
	@ApiResponses(value = {
			@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/drivingPurpose" , method=RequestMethod.PUT)
	public ResponseEntity updateDrivingPerpose(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@RequestBody(required = true)
			@Valid
			UpdateDrivingPurposeVO updateDrivingPurposeVo
			,BindingResult br
			) throws CreateResponseException, UnauthorizedException, BizException ,NotFoundException, VehicleNotFoundException, UserNotFoundException
	{
		
		if(br.hasErrors())
			return createValidErrorResponse(response,new String[]{"result"}, br ,null);
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		updateDrivingPurposeVo.setCorpKey(t.getCorpKey());
		
		busService.updateDrivingPurpose(updateDrivingPurposeVo);
		
		return createResponse(HttpStatus.OK);
	}
	
	
	@ApiOperation(value = "버스 노선 즐겨 찾기 등록", notes = "버스 노선 즐겨 찾기를 등록한다.")		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/busFavorites" , method=RequestMethod.POST)
	public ResponseEntity createBusFavorites(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@RequestBody(required = true)
			@Valid
			CreateBusFavoritesVO busFavoritesVO
			,BindingResult br
			) throws CreateResponseException, UnauthorizedException, BizException , NotFoundException, UserNotFoundException, VehicleNotFoundException
	{
		
		if(br.hasErrors())
			return createValidErrorResponse(response,new String[]{"result"}, br ,null);
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		busFavoritesVO.setCorp(t.getCorpKey());
		busFavoritesVO.setUserKey(t.getUserKey());
		
		busService.createBusFavorites(busFavoritesVO);
		
		return createResponse(HttpStatus.OK);
	}
	
	@ApiOperation(value = "버스 노선 즐겨 찾기 삭제", notes = "버스 노선 즐겨 찾기를 삭제한다.")		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/busFavorites/{busRouteKey}" , method=RequestMethod.DELETE)
	public ResponseEntity deleteBusFavorites(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@PathVariable(value="busRouteKey") 
			@ApiParam(value = "target busRouteKey")
			String busRouteKey	
			) throws CreateResponseException, UnauthorizedException, BizException , NotFoundException, UserNotFoundException, VehicleNotFoundException
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("corp", t.getCorpKey());
		param.put("userKey",t.getUserKey());
		param.put("busRouteKey", busRouteKey);
		
		busService.deleteBusFavorites(param);
		
		return createResponse(HttpStatus.OK);
	}
	
	
	
	@ApiOperation(value = "버스 정류장 map", notes = "버스 정류장 map" , response = BusRouteVO.class , responseContainer = "List")		
	@ApiResponses(value = {
			@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my vehicles"),
			@ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
			@ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
			@ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/map/bus" , method=RequestMethod.GET)
	public ResponseEntity getVehicleList(HttpServletRequest req,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@RequestParam(value="busRouteKey" , required = true)
			String busRouteKey
			) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
			return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
		
		if(busRouteKey.equals("") || busRouteKey == null){
			return createResponse(HttpStatus.BAD_REQUEST);
		}
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("searchCorpKey", t.getCorpKey());
		param.put("busRouteKey", busRouteKey);
		
		List<BusRouteVO> rtvList = busDataService.getBusRouteList(param);
		BusRouteVO busRouteVO = rtvList.get(0);
		List<BusMappingVO> busMappingVO = busRouteVO.getBusMapping();
		
		List<String> drivingVehicle = new ArrayList<String>();
		for(int i = 0 ; i < busMappingVO.size(); i++){
			//운행중인 vehiclekey
			drivingVehicle.add(busMappingVO.get(i).getVehicleKey());
		}
		param.put("vehicleKeys", drivingVehicle);
		List<VehicleVO> vehicleList = vehicleDataService.getVehicleList(param, null);
		
		Map<String,Object> rtv = new HashMap<String,Object>();
		rtv.put("routeInfo", busRouteVO);
		rtv.put("vehicleInfo", vehicleList);
		
		return createResponse(HttpStatus.OK,true,rtv);
		
	}
}
