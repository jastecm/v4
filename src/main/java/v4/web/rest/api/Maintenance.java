package v4.web.rest.api;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.codehaus.jackson.map.ObjectMapper;
import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javassist.NotFoundException;
import v4.ex.exc.BizException;
import v4.ex.exc.CreateResponseException;
import v4.ex.exc.extents.UnauthorizedException;
import v4.ex.exc.extents.UserNotFoundException;
import v4.ex.exc.extents.VehicleNotFoundException;
import v4.web.controller.BasicWebViewController;
import v4.web.rest.service.ApprovalService;
import v4.web.rest.service.MaintenanceService;
import v4.web.rest.service.TokenService;
import v4.web.rest.service.data.AccountDataService;
import v4.web.rest.service.data.MaintenanceDataService;
import v4.web.vo.CreateExpensesVO;
import v4.web.vo.CreateMaintenanceDetailVO;
import v4.web.vo.CreateMaintenanceVO;
import v4.web.vo.TokenVO;
import v4.web.vo.data.AccountVO;
import v4.web.vo.data.BusRouteVO;
import v4.web.vo.data.GroupVO;
import v4.web.vo.data.MaintenanceVO;

@Api(value="maintenanceApi")
@RestController
public class Maintenance extends BasicWebViewController {
	ObjectMapper mapper = new ObjectMapper();
	
	@Autowired
	TokenService tokenService;
	
	@Autowired
	MaintenanceService maintenanceService;
	
	@Autowired
	ApprovalService approvalService;
	
	@Autowired
	AccountDataService accountDataService;
	
	@Autowired
	MaintenanceDataService maintenanceDataService;
	
	
	@ApiOperation(value = "차량 정비 조회", notes = "차량 정비를 조회한다.")		
	@ApiResponses(value = {
			@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my vehicles"),
			@ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
			@ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
			@ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/maintenance" , method=RequestMethod.GET)
	public ResponseEntity getMaintenance(HttpServletRequest req,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@RequestParam(value="limit" , defaultValue = "10")
			int limit
			,@RequestParam(value="offset" , defaultValue = "0")
			int offset
			,@RequestParam(value="searchType", defaultValue = "")
			String searchType
			,@RequestParam(value="searchText", defaultValue = "")
			String searchText
			,@RequestParam(value="vehicleType", defaultValue = "")
			String vehicleType
			,@RequestParam(value="counting" , defaultValue = "false")
			boolean counting
			) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
			return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
		
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("searchCorpKey", t.getCorpKey());
		param.put("limit", limit);
		param.put("offset", offset);
		param.put("searchType", searchType);
		param.put("searchText", searchText);
		param.put("vehicleType", vehicleType);
		
		String order = req.getParameter("searchOrder");
		param.put("searchOrder", order);
		
		param.put("searchStartDate", req.getParameter("searchStartDate"));
		param.put("searchEndDate", req.getParameter("searchEndDate"));
		
		param.put("maintenanceKey", req.getParameter("maintenanceKey"));
		
		
		List<MaintenanceVO> rtvList = maintenanceDataService.getMaintenanceList(param);
		
		Map<String,Object> rtv = new HashMap<String,Object>();
		rtv.put("result", rtvList);
		
		if(counting){
			int totCnt = maintenanceDataService.getMaintenanceListCnt(param);
			rtv.put("totalCnt", totCnt);
		}
		
		return createResponse(HttpStatus.OK,true,rtv);
	}
	
	
	@ApiOperation(value = "차량 정비 등록", notes = "차량 정비를 등록한다.")		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/maintenance" , method=RequestMethod.POST)
	public ResponseEntity createMaintenance(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@RequestBody(required = true)
			@Valid
			CreateMaintenanceVO createMaintenanceVO
			,BindingResult br
			) throws CreateResponseException, UnauthorizedException, BizException , NotFoundException, UserNotFoundException, VehicleNotFoundException
	{
		
		if(br.hasErrors())
			return createValidErrorResponse(response,new String[]{"result"}, br ,null);
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		createMaintenanceVO.setCorp(t.getCorpKey());
		createMaintenanceVO.setRegAccount(t.getUserKey());
		
		//그룹이 없으면? 탈락이다.
		
		AccountVO accountVO = accountDataService.getAccountFromKey(createMaintenanceVO.getAccountKey(), false);
		
		GroupVO groupVO = accountVO.getGroup();
		if(groupVO == null){
			//일단 에러
			throw new BizException();
		}
		
		//정비를 쓴다는건 결재를 쓰는거다
		//등록시 1차 결재자와 2차 결재자를 가져 와야된다.
		//Map<String,Object> approval = approvalService.getMaintenanceApproval(createMaintenanceVO);
		
		//결재는 한번에 만든다.
		//createMaintenanceVO.setApprovalState("0");
		//createMaintenanceVO.setApprovalStep1Account();
		//createMaintenanceVO.setApprovalStep2Account();
		//createMaintenanceVO.setMaxStep("2");
		
		maintenanceService.createMaintenance(createMaintenanceVO);
		
		return createResponse(HttpStatus.OK);
	}
	
	@ApiOperation(value = "차량 정비 부품 등록", notes = "차량 정비 부품을 등록한다.")		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/maintenanceDetail" , method=RequestMethod.POST)
	public ResponseEntity createMaintenanceDetail(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@RequestBody(required = true)
			@Valid
			CreateMaintenanceDetailVO createMaintenanceDetailVO
			,BindingResult br
			) throws CreateResponseException, UnauthorizedException, BizException , NotFoundException, UserNotFoundException, VehicleNotFoundException
	{
		
		if(br.hasErrors())
			return createValidErrorResponse(response,new String[]{"result"}, br ,null);
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		createMaintenanceDetailVO.setCorp(t.getCorpKey());
		
		maintenanceService.createMaintenanceDetil(createMaintenanceDetailVO);
		
		return createResponse(HttpStatus.OK);
	}
	
}
