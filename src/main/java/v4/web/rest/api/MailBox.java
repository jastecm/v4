package v4.web.rest.api;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import v4.ex.exc.BizException;
import v4.ex.exc.CreateResponseException;
import v4.ex.exc.extents.UnauthorizedException;
import v4.ex.exc.extents.UserNotFoundException;
import v4.ex.exc.extents.VehicleNotFoundException;
import v4.web.controller.BasicWebViewController;
import v4.web.rest.service.AccountService;
import v4.web.rest.service.MailBoxService;
import v4.web.rest.service.TokenService;
import v4.web.vo.TokenVO;
import v4.web.vo.UserVO;


/*
 * @Class name : LoginController
 * @Description : 
 * @Modification Information
 * 
 * @author 
 * @since 
 * @version 
 * @see
 *
 */
@Api(value="mailBoxAPI")
@RestController
public class MailBox extends BasicWebViewController{
		
		ObjectMapper mapper = new ObjectMapper();
	
		@Autowired
		public TokenService tokenService;
		
		@Autowired
		public AccountService accountService;
		
		@Autowired
		public MailBoxService mailBoxService;
		
		@ApiOperation(value = "알림함 리스트", notes = "알림함 리스트" , responseContainer = "List")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of DTC"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/mailBox" , method=RequestMethod.GET)
		public ResponseEntity getMailBoxList(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@RequestParam(value="limit" , required = false , defaultValue = "5")
				@ApiParam(value = "page per count")
				int limit
				,@RequestParam(value="offset" , required = false , defaultValue = "0")
				@ApiParam(value = "offset")
				int offset
				,@RequestParam(value="searchType" , required = false) 
				String searchType
				,@RequestParam(value="searchText" , required = false)
				String searchText
				,@RequestParam(value="counting" , defaultValue = "false")
				boolean counting
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, VehicleNotFoundException 
		{
			
			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("searchAccountKey", t.getUserKey());
			param.put("limit", limit);
			param.put("offset", offset);
			param.put("searchType", searchType); //title,text
			param.put("searchText", searchText);
			
			List<Map<String,Object>> rtvList = mailBoxService.getMailBoxList(param,null);
			Map<String,Object> rtv = new HashMap<String,Object>();
			rtv.put("result", rtvList);
			
			if(counting){
				int totCnt = mailBoxService.getMailBoxListCnt(param,null);
				
				rtv.put("totalCnt", totCnt);
			}
			
			
			return createResponse(HttpStatus.OK,false,rtv);
				
		}
		
		@ApiOperation(value = "알림함 삭제", notes = "알림함 삭제" )		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of DTC"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/mailBox/{mailBoxKey}" , method=RequestMethod.DELETE)
		public ResponseEntity delMailBox(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="mailBoxKey") 
				@ApiParam(value = "mailBoxKey - delimeter ,")
				String mailBoxKey
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, VehicleNotFoundException 
		{
			
			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			String[] mailBoxKeyList = mailBoxKey.split(",");
			for(String boxKey : mailBoxKeyList)
				mailBoxService.deleteMailBox(t.getUserKey(),boxKey);
			
			return createResponse(HttpStatus.OK);
				
		}
		
		@ApiOperation(value = "알림함 삭제", notes = "알림함 삭제" )		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of DTC"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/mailBox" , method=RequestMethod.DELETE)
		public ResponseEntity delMailBox(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, VehicleNotFoundException 
		{
			
			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			mailBoxService.deleteMailBoxAll(t.getUserKey());
			
			return createResponse(HttpStatus.OK);
				
		}
}
