package v4.web.rest.api;

import java.net.HttpURLConnection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javassist.NotFoundException;
import v4.ex.exc.BizException;
import v4.ex.exc.CreateResponseException;
import v4.ex.exc.extents.UnauthorizedException;
import v4.ex.exc.extents.UserNotFoundException;
import v4.ex.exc.extents.VehicleNotFoundException;
import v4.web.controller.BasicWebViewController;
import v4.web.rest.service.ItemService;
import v4.web.rest.service.MailBoxService;
import v4.web.rest.service.TimeLineService;
import v4.web.rest.service.TokenService;
import v4.web.rest.service.data.AccountDataService;
import v4.web.rest.service.data.ItemDataService;
import v4.web.rest.service.data.VehicleDataService;
import v4.web.vo.CreateItemVO;
import v4.web.vo.TokenVO;
import v4.web.vo.UpdateItemVO;
import v4.web.vo.data.ItemVO;
import v4.web.vo.data.PartsVO;

@Api(value="itemApi")
@RestController
public class Item extends BasicWebViewController{
	
	ObjectMapper mapper = new ObjectMapper();
	
	@Autowired
	public TokenService tokenService;
	
	@Autowired
	VehicleDataService vehicleDataService;
	
	@Autowired
	AccountDataService accountDataService;
	
	@Autowired
	ItemDataService itemDataService;
	
	@Autowired
	ItemService itemService;
	
	@Autowired
	TimeLineService timeLineService;
	
	@Autowired
	MailBoxService mailBoxService;
	
	@ApiOperation(value = "차량 소모품 조회", notes = "차량 소모품 현황을 조회한다." , responseContainer = "List")		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my vehicle's Item state"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/vehicle/{vehicleKey}/item" , method=RequestMethod.GET)
	public ResponseEntity getVehicleItemList(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@PathVariable(value="vehicleKey") 
			@ApiParam(value = "target vehicle")
			String vehicleKey
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, VehicleNotFoundException, UserNotFoundException 
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
					
		v4.web.vo.data.VehicleVO vvo = vehicleDataService.getVehicleFromKey(vehicleKey);
		if(vvo == null)
			throw new VehicleNotFoundException();
		
		if(!t.getCorpKey().equals(vvo.getCorp().getCorpKey()))
			return createResponse(HttpStatus.FORBIDDEN);
		
		
		
		List<ItemVO> rtvList = itemDataService.getVehicleItems(vehicleKey,"");
		
		Map<String,Object> rtv = new HashMap<String,Object>();
		rtv.put("result", rtvList);
		
		return createResponse(HttpStatus.OK,true,rtv);
			
	}
	
	@ApiOperation(value = "차량 소모품 조회", notes = "차량 소모품 현황을 조회한다." , response = ItemVO.class)		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns a vehicle's Item state"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/vehicle/{vehicleKey}/item/{itemKey}" , method=RequestMethod.GET)
	public ResponseEntity getVehicleItem(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@PathVariable(value="vehicleKey") 
			@ApiParam(value = "target vehicle")
			String vehicleKey
			,@PathVariable(value="itemKey") 
			@ApiParam(value = "target item")
			String itemKey
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, UserNotFoundException, VehicleNotFoundException
	{
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
					
		v4.web.vo.data.VehicleVO vvo = vehicleDataService.getVehicleFromKey(vehicleKey);
		if(vvo == null)
			throw new VehicleNotFoundException();
		
		if(!t.getCorpKey().equals(vvo.getCorp().getCorpKey()))
			return createResponse(HttpStatus.FORBIDDEN);
		
		
		
		ItemVO rtvList = itemDataService.getVehicleItem(vehicleKey,itemKey,t.getLang());
		
		if(rtvList == null)
			return createResponse(HttpStatus.NOT_FOUND);
		
		Map<String,Object> rtv = new HashMap<String,Object>();
		rtv.put("result", rtvList);
		
		return createResponse(HttpStatus.OK,true,rtv);
	
	}
	
	
	@ApiOperation(value = "소모품 등록", notes = "소모품 정보를 등록한다." , response = ItemVO.class)		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/vehicle/{vehicleKey}/item" , method=RequestMethod.POST)
	public ResponseEntity createItem(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@PathVariable(value="vehicleKey") 
			@ApiParam(value = "target vehicle")
			String vehicleKey
			,@RequestBody(required = true)
			@Valid
			CreateItemVO itemVo
			,BindingResult br
			) throws CreateResponseException, UnauthorizedException, BizException , NotFoundException, UserNotFoundException, VehicleNotFoundException
	{
		
		if(br.hasErrors())
			return createValidErrorResponse(response,new String[]{"result"}, br ,null);
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		v4.web.vo.data.VehicleVO vvo = vehicleDataService.getVehicleFromKey(vehicleKey);
		if(vvo == null)
			throw new VehicleNotFoundException();
		
		if(!t.getCorpKey().equals(vvo.getCorp().getCorpKey()))
			return createResponse(HttpStatus.FORBIDDEN);
		
		
		
		itemVo.setVehicleKey(vehicleKey);
		//insert한걸 다시 셀렉트 해서 넘겨준다.
		itemService.createItem(itemVo);
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.clear();
		param.put("vehicleKey", vehicleKey);
		param.put("eventTime", new Date().getTime());
		param.put("eventValue", itemVo.getPartsKey());
		
		timeLineService.insertTimeLineEvent("item","change", param);

		long mailExpired = 1000*60*60*24*7L;
		String pushAccount = null;
		if(vvo.getCorp().getCorpType().equals("1")) pushAccount = vvo.getDefaultAccountKey();
		else pushAccount=vvo.getManagerKey();
		
		PartsVO pvo = itemDataService.getPart(itemVo.getPartsKey());
		String partsNm = pvo.getPartsNm();
		String pushMsg = vvo.getPlateNum()+" 차량의 소모품 관리항목에 "+partsNm+" 이/가 추가되었습니다.";
		mailBoxService.simgleUserMailBoxInsert(pushAccount, "소모품 추가", pushMsg, mailExpired);
		
		return createResponse(HttpStatus.OK, new String[]{"result"}, itemVo.getItemKey());
			
	}
	
	
	@ApiOperation(value = "소모품 LIST 갱신", notes = "차량 소모품을 갱신한다." )		
	@ApiResponses(value = {
			@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/vehicle/{vehicleKey}/item" , method=RequestMethod.PUT)
	public ResponseEntity updateItem(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@PathVariable(value="vehicleKey") 
			@ApiParam(value = "target vehicle")
			String vehicleKey				
			,@RequestBody(required = true)
			@Valid
			List<UpdateItemVO> itemVo
			,BindingResult br
			) throws CreateResponseException, UnauthorizedException, BizException ,NotFoundException, VehicleNotFoundException, UserNotFoundException
	{
		
		if(br.hasErrors())
			return createValidErrorResponse(response,new String[]{"result"}, br ,null);
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		v4.web.vo.data.VehicleVO vvo = vehicleDataService.getVehicleFromKey(vehicleKey);
		if(vvo == null)
			throw new VehicleNotFoundException();
		
		if(!t.getCorpKey().equals(vvo.getCorp().getCorpKey()))
			return createResponse(HttpStatus.FORBIDDEN);
		
		for(UpdateItemVO vo : itemVo){
			ItemVO item = itemDataService.getVehicleItem(vehicleKey,vo.getItemKey(),t.getLang());
			if(item == null) 
				throw new NotFoundException("can't find item - "+vo.getItemKey());
		}
		
		for(UpdateItemVO vo : itemVo){
			vo.setVehicleKey(vehicleKey);
			itemService.updateItem(vo);
		}
		
		return createResponse(HttpStatus.OK);
	}
	
	
	@ApiOperation(value = "소모품 삭제", notes = "차량 소모품 정보를 삭제한다.")		
	@ApiResponses(value = {
			@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/vehicle/{vehicleKey}/item/{itemKey}" , method=RequestMethod.DELETE)
	public ResponseEntity deleteItem(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@PathVariable(value="vehicleKey") 
			@ApiParam(value = "target vehicle")
			String vehicleKey
			,@PathVariable(value="itemKey") 
			@ApiParam(value = "target item")
			String itemKey				
			) throws CreateResponseException, UnauthorizedException, BizException ,NotFoundException, UserNotFoundException, VehicleNotFoundException
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		v4.web.vo.data.VehicleVO vvo = vehicleDataService.getVehicleFromKey(vehicleKey);
		if(vvo == null)
			throw new VehicleNotFoundException();
		
		if(!t.getCorpKey().equals(vvo.getCorp().getCorpKey()))
			return createResponse(HttpStatus.FORBIDDEN);
		
		ItemVO item = itemDataService.getVehicleItem(vehicleKey,itemKey,t.getLang());
		if(item == null) 
			throw new NotFoundException("can't find item - "+itemKey);
		
		itemService.deleteVehicleItem(vehicleKey,itemKey);
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("vehicleKey", vehicleKey);
		param.put("eventTime", new Date().getTime());
		param.put("eventValue", item.getPartsKey());
		
		timeLineService.insertTimeLineEvent("item","delete", param);
		
		long mailExpired = 1000*60*60*24*7L;
		String pushAccount = null;
		if(vvo.getCorp().getCorpType().equals("1")) pushAccount = vvo.getDefaultAccountKey();
		else pushAccount=vvo.getManagerKey();
		
		PartsVO pvo = itemDataService.getPart(item.getPartsKey());
		String partsNm = pvo.getPartsNm();
		String pushMsg = vvo.getPlateNum()+" 차량의 소모품 관리항목에 "+partsNm+" 이/가 삭제되었습니다.";
		mailBoxService.simgleUserMailBoxInsert(pushAccount, "소모품 삭제", pushMsg, mailExpired);
		
		return createResponse(HttpStatus.OK);
			
	}
	
	@ApiOperation(value = "parts 조회", notes = "차량 소모품 현황을 조회한다." , responseContainer = "List")		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my vehicle's Item state"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/parts" , method=RequestMethod.GET)
	public ResponseEntity getPartsList(HttpServletRequest req,HttpServletResponse res
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestParam(value="partsType" , required = false)
			String partsType
			,@RequestParam(value="searchPartsNm" , required = false)
			String searchPartsNm
			,@RequestParam(value="limit" , defaultValue = "10")
			int limit
			,@RequestParam(value="offset" , defaultValue = "0")
			int offset
			,@RequestParam(value="counting" , defaultValue = "false")
			boolean counting
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, VehicleNotFoundException, UserNotFoundException 
	{
		
					
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("searchPartsType", partsType);
		param.put("searchPartsNm", searchPartsNm);
		param.put("limit", limit);
		param.put("offset", offset);
		
		String order = req.getParameter("searchOrder");
		param.put("searchOrder", order);

		List<PartsVO> rtvList = itemDataService.getPartsList(param,null);
		
		Map<String,Object> rtv = new HashMap<String,Object>();
		rtv.put("result", rtvList);
		
		if(counting){
			int totCnt = itemDataService.getPartsListCnt(param,null);
			
			rtv.put("totalCnt", totCnt);
		}
		
		return createResponse(HttpStatus.OK,true,rtv);
			
	}
	
	
	@ApiOperation(value = "partsNm 조회", notes = "차량 소모품 partsNm 중복제거 리스트." , responseContainer = "List")		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my vehicle's Item state"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/partsDistinct" , method=RequestMethod.GET)
	public ResponseEntity getPartsDistinctList(HttpServletRequest req,HttpServletResponse res
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, VehicleNotFoundException, UserNotFoundException 
	{
		
		Map<String, Object> param = new HashMap<String, Object>();
		
		
		List<PartsVO> rtvList = itemDataService.getPartsDistinctList(param,null);
		
		Map<String,Object> rtv = new HashMap<String,Object>();
		rtv.put("result", rtvList);
		
		
		return createResponse(HttpStatus.OK,true,rtv);
			
	}
}
