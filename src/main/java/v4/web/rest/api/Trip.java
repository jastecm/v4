package v4.web.rest.api;

import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import v4.ex.exc.BizException;
import v4.ex.exc.CreateResponseException;
import v4.ex.exc.extents.UnauthorizedException;
import v4.ex.exc.extents.UserNotFoundException;
import v4.ex.exc.extents.VehicleNotFoundException;
import v4.web.controller.BasicWebViewController;
import v4.web.rest.service.TokenService;
import v4.web.rest.service.TripService;
import v4.web.rest.service.data.TripDataService;
import v4.web.rest.service.data.VehicleDataService;
import v4.web.vo.TokenVO;
import v4.web.vo.UserVO;
import v4.web.vo.data.AllocateVO;
import v4.web.vo.data.TrackInfoVO;
import v4.web.vo.data.TripAnalysisVO;
import v4.web.vo.data.TripVO;
import v4.web.vo.data.VehicleVO;
import v4.web.vo.data.VehicleVO.LatestLocation;


/*
 * @Class name : LoginController
 * @Description : 
 * @Modification 
 * @since 
 * @version 
 * @see
 *
 */
@Api(value="tripApi")
@RestController
public class Trip extends BasicWebViewController{
		
	ObjectMapper mapper = new ObjectMapper();
	
	@Autowired
	TokenService tokenService;
	
	@Autowired
	VehicleDataService vehicleDataService;
	
	@Autowired
	TripDataService tripDataService;
	
	@Autowired
	TripService tripService;
	
	@ApiOperation(value = "차량 주행정보 조회", notes = "차량의 월별 주행 통계를 조회한다." , response = TripAnalysisVO.class)		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of driving info"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	//@RequestMapping(value="/api/{ver}/{resource}/{recourceKey}/summary/{YYYYMM}" , method=RequestMethod.GET)
	//vehicle/vehicleKey
	//account/accountKey
	@RequestMapping(value="/api/{ver}/vehicle/{vehicleKey}/summary/{YYYYMM}" , method=RequestMethod.GET)
	public ResponseEntity getVehicleDrivingList(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@PathVariable(value="YYYYMM")
			@ApiParam(value = "YYYYMM")
			String ym
			,@PathVariable(value="vehicleKey") 
			@ApiParam(value = "target vehicle")
			String vehicleKey
			,@RequestParam(value="scope" , defaultValue = "0") 
			@ApiParam(value = "scope : 1-all , 0-personal")
			String scope
			) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, VehicleNotFoundException 
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
					
		try {
			if(StrUtil.isNullToEmpty(ym)) ym = new SimpleDateFormat("yyyyMM").format(new Date());
			new SimpleDateFormat("yyyyMM").format(new SimpleDateFormat("yyyyMMddHHmmss").parse(ym+"01000000")); 
			
		}catch(Exception e){
			throw new BizException("Invalid ym - " + ym);
		}
		
		v4.web.vo.data.VehicleVO vvo = vehicleDataService.getVehicleFromKey(vehicleKey);
		if(vvo == null)
			throw new VehicleNotFoundException();
		
		if(!t.getCorpKey().equals(vvo.getCorp().getCorpKey()))
			return createResponse(HttpStatus.FORBIDDEN);
		
		
		Map<String,Object> param = new HashMap<String,Object>();
		
		//if(scope.equals("0")) param.put("searchAccountKey",t.getUserKey())
		//else {
			//if(t.auth != master) param.put("searchAccountKey",t.getUserKey())
		//}
		
		param.put("searchVehicleKey", vehicleKey);
		param.put("yyyymm", ym);
		param.put("limit", 5);
		param.put("offset", 0);
		
		TripAnalysisVO rtv = tripDataService.getTripAnalysis(param, null);
		
		Map<String,Object> rtvMap = new HashMap<String,Object>();
		rtvMap.put("result", rtv);
		
		return createResponse(HttpStatus.OK,true,rtvMap);
			
	}
	
	
	
	
	
	@ApiOperation(value = "차량 주행정보 조회", notes = "차량 주행정보를 조회한다." , response = TripVO.class)		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of driving info"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	//vehicle/vehicleKey
	//account/accountKey
	//@RequestMapping(value="/api/{ver}/vehicle/{vehicleKey}/trip/{tripKey}" , method=RequestMethod.GET)
	//->
	@RequestMapping(value="/api/{ver}/trip/{tripKey}" , method=RequestMethod.GET)
	public ResponseEntity getTrip(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@PathVariable(value="tripKey") 
			@ApiParam(value = "tripKey")
			String tripKey
			) throws CreateResponseException, UnauthorizedException, BizException, VehicleNotFoundException, UserNotFoundException 
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
					
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("searchTripKey", tripKey);
		
		TripVO trip = tripDataService.getTripFromKey(tripKey);
		
		if(!t.getCorpKey().equals(trip.getVehicle().getCorp().getCorpKey()))
			return createResponse(HttpStatus.FORBIDDEN);
		
		/*TODO - trip 접근권한체크
		if(!t.getUserKey().equals(trip.driverKey?)) //
			return createResponse(HttpStatus.FORBIDDEN);
		*/
		
		Map<String,Object> rtv = new HashMap<String,Object>();
		rtv.put("result", trip);
		
		return createResponse(HttpStatus.OK,true,rtv);
			
	}
	
	@ApiOperation(value = "차량 주행정보 조회", notes = "차량 주행정보를 조회한다." , response = TripVO.class , responseContainer = "List")		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of driving info"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/trip" , method=RequestMethod.GET)
	public ResponseEntity getTripList(HttpServletRequest req,HttpServletResponse res
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key			
			,@RequestParam(value="limit" , required = false , defaultValue = "5")
			@ApiParam(value = "limit per page")
			int limit
			,@RequestParam(value="offset" , required = false , defaultValue = "0")
			@ApiParam(value = "offset")
			int offset
			,@RequestParam(value="searchType" , required = false)
			String searchType
			,@RequestParam(value="searchText" , required = false)
			String searchText
			,@RequestParam(value="counting" , defaultValue = "false")
			boolean counting
			) throws CreateResponseException, UnauthorizedException, BizException, VehicleNotFoundException, UserNotFoundException 
	{
		
		//인증공통
		TokenVO t = authentication(tokenService,key);
		
		if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
			return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
		//인증공통
				
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("limit", String.valueOf(limit));
		param.put("offset", String.valueOf(offset));
		param.put("searchCorpKey", t.getCorpKey());
		param.put("searchType", searchType);
		param.put("searchText", searchText);
				
		
		String searchVehicleKey = req.getParameter("searchVehicleKey");
		String searchAccountKey = req.getParameter("searchAccountKey");
		String searchStartDate = req.getParameter("searchStartDate");
		String searchEndDate = req.getParameter("searchEndDate");
		String searchDeviceSn = req.getParameter("searchDeviceSn");
		String searchDeviceSeries = req.getParameter("searchDeviceSeries");
		String searchNeedAllocate = req.getParameter("searchNeedAllocate"); //1이면 미배차 
		String searchNeedReport = req.getParameter("searchNeedReport"); //1이면 미운행일지
		/*
			필요한 파라메터
			
			//상세운행기록
			고장여부에 따른 조회  : 정상, 고장
			경고에 따른 조회 : 지역 시간
			
		  
		*/
		
		param.put("searchVehicleKey", searchVehicleKey);
		param.put("searchAccountKey", searchAccountKey);
		param.put("searchStartDate", searchStartDate);
		param.put("searchEndDate", searchEndDate);
		param.put("searchDeviceSn", searchDeviceSn);
		param.put("searchDeviceSeries", searchDeviceSeries);
		param.put("searchNeedAllocate", searchNeedAllocate);
		param.put("searchNeedReport", searchNeedReport);
				
		String order = req.getParameter("searchOrder");
		/*
		여기에 필요한 order 선언
		  
		*/
		
		param.put("searchOrder", order);
		
				
		List<TripVO> trip = tripDataService.getTripList(param,null);
				
		Map<String,Object> rtv = new HashMap<String,Object>();
		rtv.put("result", trip);
				
		if(counting){
			int totCnt = tripDataService.getTripListCnt(param,null);
			/*
			여기에 필요한 파라메터 선언 
			  
			*/
			
			rtv.put("totalCnt", totCnt);
		}
		
		return createResponse(HttpStatus.OK,true,rtv);
			
	}
	
	@ApiOperation(value = "차량 주행정보 조회", notes = "차량 주행정보를 조회한다." , response = TripVO.class , responseContainer = "List")		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of driving info"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	//@RequestMapping(value="/api/{ver}/{resource}/{recourceKey}/trip" , method=RequestMethod.GET)
	//vehicle/vehicleKey
	//account/accountKey
	@RequestMapping(value="/api/{ver}/vehicle/{vehicleKey}/trip" , method=RequestMethod.GET)
	public ResponseEntity getTripList(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@PathVariable(value="vehicleKey") 
			@ApiParam(value = "target vehicle")
			String vehicleKey
			,@RequestParam(name="limit" , defaultValue="5") 
			@ApiParam(value = "limit")
			String limit
			,@RequestParam(name="offset" , defaultValue="0") 
			@ApiParam(value = "offset")
			String offset
			) throws CreateResponseException, UnauthorizedException, BizException, VehicleNotFoundException, UserNotFoundException 
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		v4.web.vo.data.VehicleVO vvo = vehicleDataService.getVehicleFromKey(vehicleKey);
		if(vvo == null)
			throw new VehicleNotFoundException();
		
		if(!t.getCorpKey().equals(vvo.getCorp().getCorpKey()))
			return createResponse(HttpStatus.FORBIDDEN);
		/*
		UserVO user = restAccountService.getAccount(t.getUserKey());
		
		if(user == null) 
			throw new UserNotFoundException();
		
		String lang = user.getLang();
		
		VehicleVO vvo = null;
		if(user.getCorp() == 0)
			vvo = restVehicleService.getVehicle(t.getUserKey(),vehicleKey, "");
		else vvo = restVehicleService.getVehicleInCorp(t.getUserKey(),vehicleKey, false, "");
		
		if(vvo == null)
			throw new VehicleNotFoundException();
		
		user.setRule(vvo.getVehicleMasterRule());
		*/
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("searchVehicleKey", vehicleKey);
		param.put("limit", limit);
		param.put("offset", offset);
		
		List<TripVO> trip = tripDataService.getTripList(param,null);
		
		Map<String,Object> rtv = new HashMap<String,Object>();
		rtv.put("result", trip);
		
		return createResponse(HttpStatus.OK,true,rtv);
			
	}
	
	@ApiOperation(value = "주행목적 변경", notes = "주행 목적 변경(1 업무 , 2 출퇴근 , 3비업무)")		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of driving info"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	//@RequestMapping(value="/api/{ver}/vehicle/{vehicleKey}/trip/{tripKey}/purpose/{purpose}" , method=RequestMethod.PUT)
	//->
	@RequestMapping(value="/api/{ver}/trip/{tripKey}/purpose/{purpose}" , method=RequestMethod.PUT)
	public ResponseEntity purpose(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@PathVariable(value="tripKey") 
			@ApiParam(value = "tripKey")
			String tripKey
			,@PathVariable(value="purpose") 
			@ApiParam(value = "purpose")
			String purpose
			) throws CreateResponseException, UnauthorizedException, BizException, VehicleNotFoundException, UserNotFoundException 
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
					
		/*
		UserVO user = restAccountService.getAccount(t.getUserKey());
		
		if(user == null) 
			throw new UserNotFoundException();
		
		String lang = user.getLang();
		
		VehicleVO vvo = null;
		if(user.getCorp() == 0)
			vvo = restVehicleService.getVehicle(t.getUserKey(),vehicleKey, "");
		else vvo = restVehicleService.getVehicleInCorp(t.getUserKey(),vehicleKey, false, "");
		
		if(vvo == null)
			throw new VehicleNotFoundException();
		
		user.setRule(vvo.getVehicleMasterRule());
		*/
		/*
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("searchTripKey", tripKey);
		
		TripVO trip = tripDataService.getTripFromKey(tripKey);
		*/
		
		
		//TODO - chk auth
		tripService.updatePurpose(tripKey,purpose);
		
		return createResponse(HttpStatus.OK);
			
	}
	
	@ApiOperation(value = "주행목적 변경", notes = "해당날짜의 모든 주행 목적 변경(1 업무 , 2 출퇴근 , 3비업무)" , response = String[].class)		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of driving info"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	//@RequestMapping(value="/api/{ver}/vehicle/{vehicleKey}/trip/{tripKey}/purposeDay/{purpose}" , method=RequestMethod.PUT)
	//->
	@RequestMapping(value="/api/{ver}/trip/{tripKey}/purposeDay/{purpose}" , method=RequestMethod.PUT)
	public ResponseEntity purposeDay(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@PathVariable(value="tripKey") 
			@ApiParam(value = "tripKey")
			String tripKey
			,@PathVariable(value="purpose") 
			@ApiParam(value = "purpose")
			String purpose
			) throws CreateResponseException, UnauthorizedException, BizException, VehicleNotFoundException, UserNotFoundException 
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
					
		/*
		UserVO user = restAccountService.getAccount(t.getUserKey());
		
		if(user == null) 
			throw new UserNotFoundException();
		
		String lang = user.getLang();
		
		VehicleVO vvo = null;
		if(user.getCorp() == 0)
			vvo = restVehicleService.getVehicle(t.getUserKey(),vehicleKey, "");
		else vvo = restVehicleService.getVehicleInCorp(t.getUserKey(),vehicleKey, false, "");
		
		if(vvo == null)
			throw new VehicleNotFoundException();
		
		user.setRule(vvo.getVehicleMasterRule());
		*/
		/*
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("searchTripKey", tripKey);
		
		TripVO trip = tripDataService.getTripFromKey(tripKey);
		*/
		
		tripService.updatePurposeCompareDay(tripKey,purpose);
		
		return createResponse(HttpStatus.OK);
			
	}
	
	@ApiOperation(value = "차량 주행정보 조회", notes = "차량 주행정보를 조회한다." , response = TrackInfoVO.class)		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of driving info"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	//@RequestMapping(value="/api/{ver}/vehicle/{vehicleKey}/trackRecord/{tripKey}" , method=RequestMethod.GET)
	//->
	@RequestMapping(value="/api/{ver}/trackRecord/{tripKey}" , method=RequestMethod.GET)
	public ResponseEntity getTripMap(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@PathVariable(value="vehicleKey") 
			@ApiParam(value = "target vehicle")
			String vehicleKey
			,@PathVariable(value="tripKey") 
			@ApiParam(value = "target tripKey")
			String tripKey
			,@RequestParam(value="gpsAccuracy" , defaultValue = "0")
			int gpsAccuracy
			) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, VehicleNotFoundException 
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		Map<String,Object> param = new HashMap<String,Object>();
		TrackInfoVO rtv = tripDataService.getTrack_trip(tripKey, gpsAccuracy, param, null);
		
		Map<String,Object> rtvMap = new HashMap<String,Object>();
		rtvMap.put("result", rtv);
		
		
		return createResponse(HttpStatus.OK,true,rtvMap);
			
	}
	
	@ApiOperation(value = "차량 주행정보 조회", notes = "차량 주행정보를 조회한다." , response = TrackInfoVO.class)		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of driving info"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	//@RequestMapping(value="/api/{ver}/{resource}/{recourceKey}/trackRecord/date/{YYYYMMDD}" , method=RequestMethod.GET)
	@RequestMapping(value="/api/{ver}/vehicle/{vehicleKey}/trackRecord/date/{YYYYMMDD}" , method=RequestMethod.GET)
	public ResponseEntity getTripMap_date(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@PathVariable(value="vehicleKey") 
			@ApiParam(value = "target vehicle")
			String vehicleKey
			,@PathVariable(value="YYYYMMDD") 
			@ApiParam(value = "target trip date")
			String yyyymmdd
			,@RequestParam(value="gpsAccuracy" , defaultValue = "0")
			int gpsAccuracy
			) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, VehicleNotFoundException 
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		Date d_yyyymmdd = new Date();
		try {
			d_yyyymmdd = new SimpleDateFormat("yyyyMMdd").parse(yyyymmdd); 
		}catch(Exception e){
			return createResponse(HttpStatus.BAD_REQUEST , new String[]{"result"} , "Invalid Date format : yyyymmdd - " + yyyymmdd);
		}
		
		v4.web.vo.data.VehicleVO vvo = vehicleDataService.getVehicleFromKey(vehicleKey);
		if(vvo == null)
			throw new VehicleNotFoundException();
		
		if(!t.getCorpKey().equals(vvo.getCorp().getCorpKey()))
			return createResponse(HttpStatus.FORBIDDEN);
		//TODO - vehicle 접근권한, vehicleLocationInfo 1 chk , trip 접근권한 chk
		/*
		UserVO user = restAccountService.getAccount(t.getUserKey());
		
		if(user == null) 
			throw new UserNotFoundException();
		
		String lang = user.getLang();
		
		VehicleVO vvo = null;
		if(user.getCorp() == 0)
			vvo = restVehicleService.getVehicle(t.getUserKey(),vehicleKey, "");
		else vvo = restVehicleService.getVehicleInCorp(t.getUserKey(),vehicleKey, false, "");
		
		if(vvo == null)
			throw new VehicleNotFoundException();
		
		user.setRule(vvo.getVehicleMasterRule());
		*/
		
		Map<String,Object> param = new HashMap<String,Object>();
		List<TrackInfoVO> rtv = tripDataService.getTrack_date(vehicleKey , null , d_yyyymmdd , gpsAccuracy, param, null);
		
		Map<String,Object> rtvMap = new HashMap<String,Object>();
		rtvMap.put("result", rtv);
		
		
		return createResponse(HttpStatus.OK,true,rtvMap);
			
	}
	
	@ApiOperation(value = "차량 주행정보 조회", notes = "차량 주행정보를 조회한다." , response = TrackInfoVO.class)		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of driving info"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	//@RequestMapping(value="/api/{ver}/{resource}/{recourceKey}/trackRecord/recentTime/{timestamp}" , method=RequestMethod.GET)
	@RequestMapping(value="/api/{ver}/vehicle/{vehicleKey}/trackRecord/recentTime/{timestamp}" , method=RequestMethod.GET)
	public ResponseEntity getTripMap_date(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@PathVariable(value="vehicleKey") 
			@ApiParam(value = "target vehicle")
			String vehicleKey
			,@PathVariable(value="timestamp") 
			@ApiParam(value = "timestamp")
			long timestamp
			,@RequestParam(value="gpsAccuracy" , defaultValue = "0")
			int gpsAccuracy
			) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, VehicleNotFoundException 
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		v4.web.vo.data.VehicleVO vvo = vehicleDataService.getVehicleFromKey(vehicleKey);
		if(vvo == null)
			throw new VehicleNotFoundException();
		
		if(!t.getCorpKey().equals(vvo.getCorp().getCorpKey()))
			return createResponse(HttpStatus.FORBIDDEN);
		//TODO - vehicle 접근권한, vehicleLocationInfo 1 chk , trip 접근권한 chk
		/*
		UserVO user = restAccountService.getAccount(t.getUserKey());
		
		if(user == null) 
			throw new UserNotFoundException();
		
		String lang = user.getLang();
		
		VehicleVO vvo = null;
		if(user.getCorp() == 0)
			vvo = restVehicleService.getVehicle(t.getUserKey(),vehicleKey, "");
		else vvo = restVehicleService.getVehicleInCorp(t.getUserKey(),vehicleKey, false, "");
		
		if(vvo == null)
			throw new VehicleNotFoundException();
		
		user.setRule(vvo.getVehicleMasterRule());
		*/
		Date eDate = new Date();
		Date sDate = new Date(eDate.getTime()-(timestamp));
		
		Map<String,Object> param = new HashMap<String,Object>();
		List<TrackInfoVO> rtv = tripDataService.getTrack_time(vehicleKey , null , sDate , eDate , gpsAccuracy, param, null);
		
		Map<String,Object> rtvMap = new HashMap<String,Object>();
		rtvMap.put("result", rtv);
		
		
		return createResponse(HttpStatus.OK,true,rtvMap);
			
	}
}
