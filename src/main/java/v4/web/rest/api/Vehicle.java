package v4.web.rest.api;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.codehaus.jackson.map.ObjectMapper;
import org.jastecm.lora.msg.LoraMsgConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.common.Globals;
import com.util.UnitConvertUtil;
import org.jastecm.json.JSONException;
import org.jastecm.string.StrUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import v4.ex.exc.BizException;
import v4.ex.exc.CreateResponseException;
import v4.ex.exc.NotFoundException;
import v4.ex.exc.RestConnectException;
import v4.ex.exc.extents.UnauthorizedException;
import v4.ex.exc.extents.UserNotFoundException;
import v4.ex.exc.extents.VehicleNotFoundException;
import v4.web.controller.BasicWebViewController;
import v4.web.rest.service.DeviceService;
import v4.web.rest.service.DownloadService;
import v4.web.rest.service.TokenService;
import v4.web.rest.service.VehicleDBService;
import v4.web.rest.service.VehicleService;
import v4.web.rest.service.data.AccountDataService;
import v4.web.rest.service.data.DeviceDataService;
import v4.web.rest.service.data.VehicleDataService;
import v4.web.vo.CreateBusVO;
import v4.web.vo.CreateEtcVO;
import v4.web.vo.CreateReActiveVehicleVO;
import v4.web.vo.CreateTruckVO;
import v4.web.vo.CreateVehicleVO;
import v4.web.vo.TokenVO;
import v4.web.vo.UpdateVehicleVO;
import v4.web.vo.VehicleVO;
import v4.web.vo.data.AccountVO;
import v4.web.vo.data.DeviceVO;


/*
 * @Class name : LoginController
 * @Description : 
 * @Modification 
 * @since 
 * @version 
 * @see
 *
 */
@Api(value="vehicleApi")
@RestController
public class Vehicle extends BasicWebViewController{
		
		ObjectMapper mapper = new ObjectMapper();
	
		@Autowired
		VehicleDataService vehicleDataService;
		
		@Autowired
		AccountDataService accountDataService;
		
		@Autowired
		DeviceDataService deviceDataService;
		
		@Autowired
		DeviceService deviceService;
		
		@Autowired
		VehicleService vehicleService;
		
		@Autowired
		TokenService tokenService;
		
		@Autowired
		VehicleDBService vehicleDBService;
		
		@Autowired
		DownloadService downloadService;
		
		
		@ApiOperation(value = "차량 리스트 조회", notes = "자신이 접근 가능한 차량 리스트를 조회한다." , response = v4.web.vo.data.VehicleVO.class , responseContainer = "List")		
		@ApiResponses(value = {
				@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my vehicles"),
				@ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
				@ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
				@ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/vehicle" , method=RequestMethod.GET)
		public ResponseEntity getVehicleList(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@RequestParam(value="vehicleType" , required = false)
				String vehicleType
				,@RequestParam(value="limit" , defaultValue = "10")
				int limit
				,@RequestParam(value="offset" , defaultValue = "0")
				int offset
				,@RequestParam(value="searchType" , required = false)
				String searchType
				,@RequestParam(value="searchText" , required = false)
				String searchText
				,@RequestParam(value="counting" , defaultValue = "false")
				boolean counting
				,@RequestParam(value="searchGroupKey", required = false)
				String searchGroupKey
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
		{
			
 			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("searchCorpKey", t.getCorpKey());
			param.put("limit", limit);
			param.put("offset", offset);
			param.put("searchVehicleType", vehicleType);
			param.put("searchType", searchType);
			param.put("searchText", searchText);
			param.put("searchGroupKey", searchGroupKey);
			
			param.put("searchDeviceSeries", req.getParameter("searchDeviceSeries"));
			param.put("searchUseState", req.getParameter("searchUseState"));
			param.put("searchDrivingState", req.getParameter("searchDrivingState"));
			
			param.put("searchStartDate", req.getParameter("searchStartDate"));
			param.put("searchEndDate", req.getParameter("searchEndDate"));
			
			
			String order = req.getParameter("searchOrder");
			param.put("searchOrder", order);
			
			param.put("searchGroupKey", searchGroupKey);
			
			List<v4.web.vo.data.VehicleVO> rtvList = vehicleDataService.getVehicleList(param,null);
			
			Map<String,Object> rtv = new HashMap<String,Object>();
			rtv.put("result", rtvList);
			
			if(counting){
				int totCnt = vehicleDataService.getVehicleListCnt(param,null);
				param.put("searchUseState", "1");
				int usedCnt = vehicleDataService.getVehicleListCnt(param,null);
				param.put("searchUseState", "0");
				int unusedCnt = vehicleDataService.getVehicleListCnt(param,null);
				param.remove("searchUseState");
				
				param.put("searchDrivingState", "driving");
				int drivingCnt = vehicleDataService.getVehicleListCnt(param,null);
				param.put("searchDrivingState", "parking");
				int parkingCnt = vehicleDataService.getVehicleListCnt(param,null);
				param.remove("searchDrivingState");
				
				rtv.put("totalCnt", totCnt);
				rtv.put("usedCnt", usedCnt);
				rtv.put("unusedCnt", unusedCnt);
				rtv.put("drivingCnt", drivingCnt);
				rtv.put("parkingCnt", parkingCnt);
			}
			
			return createResponse(HttpStatus.OK,true,rtv);
			
		}
		
		@ApiOperation(value = "차량 조회", notes = "차량 정보를 조회한다." , response = v4.web.vo.data.VehicleVO.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns a vehicle"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/vehicle/{vehicleKey}" , method=RequestMethod.GET)
		public ResponseEntity getVehicle(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="vehicleKey") 
				@ApiParam(value = "target vehicle")
				String vehicleKey
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, VehicleNotFoundException 
		{
			
			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			v4.web.vo.data.VehicleVO vehicle = vehicleDataService.getVehicleFromKey(vehicleKey);
			
			if(vehicle == null) 
				throw new VehicleNotFoundException();
			else if(!vehicle.getCorp().getCorpKey().equals(t.getCorpKey()))
				return createResponse(HttpStatus.FORBIDDEN , new String[]{"result"} , "can not access resource");
			
			
			
			
			Map<String,Object> rtv = new HashMap<String,Object>();
			rtv.put("result", vehicle);
			
			return createResponse(HttpStatus.OK,true,rtv);			
				
				
		}
		
		@ApiOperation(value = "차량 등록", notes = "차량을 등록한다.(바로등록)" , response = String.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_FORBIDDEN, message = "Forbidden"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/vehicle" , method=RequestMethod.POST)
		public ResponseEntity createVehile(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@RequestBody(required = true)
				@Valid
				CreateVehicleVO vehicleVo
				,BindingResult br
				) throws CreateResponseException, UnauthorizedException, BizException, RestConnectException, NotFoundException 
		{

			if(br.hasErrors())
				return createValidErrorResponse(response,new String[]{"result"}, br ,null);

			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			AccountVO account = accountDataService.getAccountFromKey(t.getUserKey(),true);
			
			if(account == null)
				throw new UserNotFoundException();
			//여기까지 거의공통//
			
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("searchDeviceSn", vehicleVo.getDeviceSn());
			DeviceVO device = deviceDataService.getDevice(param,null);
			String deviceId = device.getDeviceId();
			
			
			if(deviceId == null)
				throw new NotFoundException("can't find device : " + vehicleVo.getDeviceSn());
			
			vehicleVo.setDeviceId(deviceId);
			vehicleVo.setCorpKey(t.getCorpKey());
			
			if(StrUtil.isNullToEmpty(vehicleVo.getDeviceSound())) vehicleVo.setDeviceSound("0");
			
			if(!account.getUnitLength().equals("km")){
				double totDist = UnitConvertUtil.unitToKm(vehicleVo.getTotDistance(), account.getUnitLength());
				vehicleVo.setTotDistance(UnitConvertUtil.kmToM(totDist));
			}else{
				vehicleVo.setTotDistance(UnitConvertUtil.kmToM(vehicleVo.getTotDistance()));
			}
			
			vehicleVo.setRegUser(account.getAccountKey());
			if(account.getCorp().getCorpType().equals("1")){
				vehicleVo.setDefaultAccountKey(account.getAccountKey());
			}
			
			try {
				
				Map<String,Object> rtv = vehicleService.createVehicleTemp(account,vehicleVo);
				
				if((HttpStatus)rtv.get("code") != HttpStatus.CREATED)
					return createResponse((HttpStatus)rtv.get("code"),new String[]{"result"}, (String)rtv.get("msg"));
				
				/*RestDvpMngController./api/{ver}/dvpMng/initialize/{deviceSn} copy*/
				//CreateVehicleVO vo = vehicleService.getTempVehicle(device.getDeviceSn());
				
				String convCode = "";
				int totDistance = 0;
				String deviceSound = "";
				Map<Integer,byte[]> vehicleDb = null;
				Map<Integer,byte[]> firmware = null;
				
				v4.web.vo.DeviceVO dvo = deviceService.getDeviceFromId(deviceId);
				
				if(StrUtil.isNullToEmpty(vehicleVo.getVehicleKey())){
					//new
					convCode = vehicleVo.getConvCode();
					totDistance = (int)(vehicleVo.getTotDistance());
					deviceSound = vehicleVo.getDeviceSound();
					
					dvo.setConvCode(convCode);
					firmware = downloadService.getFirmware(dvo); 
					vehicleDb =downloadService.getVehicleDb(dvo);
				}
				
				/////////////////////////
				
				byte[] dbNm = vehicleDb.get(0);
				String strDbNm = new String(dbNm,"UTF-8");
				byte[] firmNm = firmware.get(0);
				String strFirmNm = new String(firmNm,"UTF-8");
				
				vehicleService.updateLastestDbInfoTemp(device.getDeviceSn(),strDbNm,strFirmNm,convCode,totDistance,deviceSound);
				/*end of copy*/
				
				
				CreateVehicleVO vo = vehicleService.getTempVehicle(vehicleVo.getDeviceSn());
				
				rtv = vehicleService.createVehicle(account,vo);
				if((HttpStatus)rtv.get("code") == HttpStatus.CREATED){
					String vehicleKey = (String) rtv.get("msg");
					if(vehicleKey != null){
						/*
						LoraMsgConnector connector = new LoraMsgConnector(device.getAppEui(), Globals.LORA_KEYS.get(device.getAppEui()));
						Map<String,String> res = connector.loraDeviceChk(deviceId, Globals.LORA_SUBSCRIVE_NM);
						if(res.get(LoraMsgConnector.RESPONSE_CODE).equals("200")){
							System.out.println("already subscribe ON!");
						}else{
							res = connector.loraDeviceSubOn(deviceId, Globals.LORA_SUBSCRIVE_NM, Globals.LORA_SUBSCRIVE);
							if(res.get(LoraMsgConnector.RESPONSE_CODE).equals("201")){
								System.out.println("subscribe ON! Success");
							}
						}
						*/
						//if  kcc = treOn
						/*
						int adminCorp = restDeviceService.getAdminCorpFromSn(vehicleVo.getDeviceSn());
						
						if(adminCorp == 1){ //kcc only
						
							viewcar.pro.vo.VehicleVO vvo = new viewcar.pro.vo.VehicleVO();
							vvo.setVehicleKey(vehicleKey);
							vvo = vehicleService.getVehicle(vvo);
							
							String payload = mapper.writeValueAsString(vvo);
							
							JSONObject obj = new JSONObject(payload);
							Map<String,String> vehicleInfo = restVehicleDbService.getVehicleDbInfo("ko", vo.getConvCode());
							String vehicleInfoPayload = mapper.writeValueAsString(vehicleInfo);
							JSONObject objVehicleDbInfo = new JSONObject(vehicleInfoPayload);
							obj.put("vehicleDbInfo", objVehicleDbInfo);
							
							realayConnectService.sendMsgAsync(t.getUserKey(), key, vehicleVo.getDeviceSn(), obj.toString(), "treOn",Globals.MODE);
						}
						*/
					}
				}
				return createResponse((HttpStatus)rtv.get("code"),new String[]{"result"}, (String)rtv.get("msg"));	
			}catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return createResponse(HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
		}
		
		@ApiOperation(value = "차량 - 단말 매치(사용중지->사용중)", notes = "사용중지중인 차량에 단말을 매치하여 사용중인 상태로 변경한다." , response = String.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_FORBIDDEN, message = "Forbidden"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/vehicle/active" , method=RequestMethod.POST)
		public ResponseEntity createVehile(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@RequestBody(required = true)
				@Valid
				CreateReActiveVehicleVO vehicleVo
				,BindingResult br
				) throws CreateResponseException, UnauthorizedException, BizException, RestConnectException, NotFoundException 
		{

			if(br.hasErrors())
				return createValidErrorResponse(response,new String[]{"result"}, br ,null);

			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			AccountVO account = accountDataService.getAccountFromKey(t.getUserKey(),true);
			
			if(account == null)
				throw new UserNotFoundException();
			//여기까지 거의공통//
			
			
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("searchDeviceSn", vehicleVo.getDeviceSn());
			DeviceVO device = deviceDataService.getDevice(param,null);
			
			if(device == null)
				throw new NotFoundException("can't find device : " + vehicleVo.getDeviceSn());
			
			
			String deviceId = device.getDeviceId();
			
			
			
			
			CreateVehicleVO cvvo = new CreateVehicleVO();
			
			cvvo.setVehicleKey(vehicleVo.getVehicleKey());
			cvvo.setDeviceSn(vehicleVo.getDeviceSn());
			cvvo.setCorpKey(t.getCorpKey());
			cvvo.setDeviceId(deviceId);
			cvvo.setRegUser(account.getAccountKey());
			if(StrUtil.isNullToEmpty(cvvo.getDeviceSound())) cvvo.setDeviceSound("0");
			
			try {
				
				Map<String,Object> rtv = vehicleService.createVehicleTemp(account,cvvo);
				if((HttpStatus)rtv.get("code") != HttpStatus.CREATED){
					return createResponse((HttpStatus)rtv.get("code"),new String[]{"result"}, (String)rtv.get("msg"));	
				}
				/*RestDvpMngController./api/{ver}/dvpMng/initialize/{deviceSn} copy*/
				//CreateVehicleVO vo = vehicleService.getTempVehicle(device.getDeviceSn());
				
				String convCode = "";
				int totDistance = 0;
				String deviceSound = "";
				Map<Integer,byte[]> vehicleDb = null;
				Map<Integer,byte[]> firmware = null;
				
				v4.web.vo.DeviceVO dvo = deviceService.getDeviceFromId(deviceId);
				
				if(!StrUtil.isNullToEmpty(cvvo.getVehicleKey())){
					//re active
					v4.web.vo.data.VehicleVO vvo = vehicleDataService.getVehicleFromKey(cvvo.getVehicleKey());
					
					convCode = vvo.getVehicleModel().getConvCode();
					totDistance = (int)vvo.getTotDistance().doubleValue();
					deviceSound = device.getDownloadDeviceSound();
					
					dvo.setConvCode(convCode);
					
					firmware = downloadService.getFirmware(dvo); 
					vehicleDb =downloadService.getVehicleDb(dvo);
					
				}
				
				/////////////////////////
				
				byte[] dbNm = vehicleDb.get(0);
				String strDbNm = new String(dbNm,"UTF-8");
				byte[] firmNm = firmware.get(0);
				String strFirmNm = new String(firmNm,"UTF-8");
				
				vehicleService.updateLastestDbInfoTemp(device.getDeviceSn(),strDbNm,strFirmNm,convCode,totDistance,deviceSound);
				/*end of copy*/
				
				
				cvvo = vehicleService.getTempVehicle(cvvo.getDeviceSn());
				
				rtv = vehicleService.createVehicle(account,cvvo);
				if((HttpStatus)rtv.get("code") == HttpStatus.CREATED){
					String vehicleKey = (String) rtv.get("msg");
					if(vehicleKey != null){
						/*
						LoraMsgConnector connector = new LoraMsgConnector(device.getAppEui(), Globals.LORA_KEYS.get(device.getAppEui()));
						Map<String,String> res = connector.loraDeviceChk(deviceId, Globals.LORA_SUBSCRIVE_NM);
						if(res.get(LoraMsgConnector.RESPONSE_CODE).equals("200")){
							System.out.println("already subscribe ON!");
						}else{
							res = connector.loraDeviceSubOn(deviceId, Globals.LORA_SUBSCRIVE_NM, Globals.LORA_SUBSCRIVE);
							if(res.get(LoraMsgConnector.RESPONSE_CODE).equals("201")){
								System.out.println("subscribe ON! Success");
							}
						}
						*/
						//if  kcc = treOn
						/*
						int adminCorp = restDeviceService.getAdminCorpFromSn(vehicleVo.getDeviceSn());
						
						if(adminCorp == 1){ //kcc only
						
							viewcar.pro.vo.VehicleVO vvo = new viewcar.pro.vo.VehicleVO();
							vvo.setVehicleKey(vehicleKey);
							vvo = vehicleService.getVehicle(vvo);
							
							String payload = mapper.writeValueAsString(vvo);
							
							JSONObject obj = new JSONObject(payload);
							Map<String,String> vehicleInfo = restVehicleDbService.getVehicleDbInfo("ko", vo.getConvCode());
							String vehicleInfoPayload = mapper.writeValueAsString(vehicleInfo);
							JSONObject objVehicleDbInfo = new JSONObject(vehicleInfoPayload);
							obj.put("vehicleDbInfo", objVehicleDbInfo);
							
							realayConnectService.sendMsgAsync(t.getUserKey(), key, vehicleVo.getDeviceSn(), obj.toString(), "treOn",Globals.MODE);
						}
						*/
					}
				}
				return createResponse((HttpStatus)rtv.get("code"),new String[]{"result"}, (String)rtv.get("msg"));	
			}catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return createResponse(HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
		}
		
		@ApiOperation(value = "차량 등록", notes = "차량을 등록한다.(pcMng 연동)" , response = String.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_FORBIDDEN, message = "Forbidden"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/vehicleTemp" , method=RequestMethod.POST)
		public ResponseEntity createVehileTemp(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@RequestBody(required = true)
				@Valid
				CreateVehicleVO vehicleVo
				,BindingResult br
				) throws CreateResponseException, UnauthorizedException, BizException, RestConnectException, NotFoundException, JSONException 
		{

			if(br.hasErrors())
				return createValidErrorResponse(response,new String[]{"result"}, br ,null);

			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			AccountVO account = accountDataService.getAccountFromKey(t.getUserKey(),true);
			
			if(account == null)
				throw new UserNotFoundException();
			
			
			try {
				
				if(StrUtil.isNullToEmpty(vehicleVo.getDeviceSound())) vehicleVo.setDeviceSound("0");
				if(!account.getUnitLength().equals("km")){
					double totDist = UnitConvertUtil.unitToKm(vehicleVo.getTotDistance(), account.getUnitLength());
					vehicleVo.setTotDistance(UnitConvertUtil.kmToM(totDist));
				}else{
					vehicleVo.setTotDistance(UnitConvertUtil.kmToM(vehicleVo.getTotDistance()));
				}
				vehicleVo.setRegUser(account.getAccountKey());
				
				Map<String,Object> rtv = vehicleService.createVehicleTemp(account,vehicleVo);
				if((HttpStatus)rtv.get("code") == HttpStatus.CREATED){
					String rtvPage = "pcmgrProto://newVehicle/?key="+t.getUserKey();
					return createResponse((HttpStatus)rtv.get("code"),new String[]{"result"}, rtvPage);	
				}else return createResponse((HttpStatus)rtv.get("code"),new String[]{"result"}, (String)rtv.get("msg"));
				
			}catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return createResponse(HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
		}
		
		@ApiOperation(value = "차량 - 단말 매치(사용중지->사용중) - pcMng용", notes = "사용중지중인 차량에 단말을 매치하여 사용중인 상태로 변경한다.  - pcMng를 통해 등록" , response = String.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_FORBIDDEN, message = "Forbidden"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/vehicleTemp/active" , method=RequestMethod.POST)
		public ResponseEntity createVehileTemp(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@RequestBody(required = true)
				@Valid
				CreateReActiveVehicleVO vehicleVo
				,BindingResult br
				) throws CreateResponseException, UnauthorizedException, BizException, RestConnectException, NotFoundException, JSONException 
		{

			if(br.hasErrors())
				return createValidErrorResponse(response,new String[]{"result"}, br ,null);

			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			AccountVO account = accountDataService.getAccountFromKey(t.getUserKey(),true);
			
			if(account == null)
				throw new UserNotFoundException();
			
			
			try {
				
				CreateVehicleVO cvvo = new CreateVehicleVO();
				cvvo.setVehicleKey(vehicleVo.getVehicleKey());
				cvvo.setDeviceSn(vehicleVo.getDeviceSn());
				if(StrUtil.isNullToEmpty(cvvo.getDeviceSound())) cvvo.setDeviceSound("0");
				
				Map<String,Object> rtv = vehicleService.createVehicleTemp(account,cvvo);
				if((HttpStatus)rtv.get("code") == HttpStatus.CREATED){
					String rtvPage = "pcmgrProto://newVehicle/?key="+t.getUserKey();
					return createResponse((HttpStatus)rtv.get("code"),new String[]{"result"}, rtvPage);	
				}else return createResponse((HttpStatus)rtv.get("code"),new String[]{"result"}, (String)rtv.get("msg"));
				
			}catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return createResponse(HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
		}
		
		@ApiOperation(value = "차량 사용 중지", notes = "차량에서 단말을 제거하여 사용중지처리함(승용,버스,트럭 공용)")		
		@ApiResponses(value = {
				@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/vehicle/{vehicleKey}/inactive" , method=RequestMethod.DELETE)
		public ResponseEntity inactiveVehicle(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="vehicleKey") 
				@ApiParam(value = "target vehicleKey arr - delimeter ',' ")
				String vehicleKey
				) throws CreateResponseException, UnauthorizedException, BizException, VehicleNotFoundException, UserNotFoundException 
		{
			
			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			AccountVO account = accountDataService.getAccountFromKey(t.getUserKey(),true);
			
			if(account == null)
				throw new UserNotFoundException();
			
			
			String[] vehicleKeys = vehicleKey.split(",");
			
			for(String vKey : vehicleKeys){
				v4.web.vo.data.VehicleVO vehicle = vehicleDataService.getVehicleFromKey(vKey);
				
				if(vehicle == null) 
					throw new VehicleNotFoundException();
				
				//if(account.관리자권한){
					vehicleService.inactiveVehicle(vehicle.getVehicleKey());
					
					
					try {
						if(null == vehicle.getDevice()) 
							return createResponse(HttpStatus.NOT_FOUND , new String[]{"result"} , "already inactive vehicle");
						
						/*
						String deviceId = vehicle.getDevice().getDeviceId();
						LoraMsgConnector connector = new LoraMsgConnector(vehicle.getDevice().getAppEui(), Globals.LORA_KEYS.get(vehicle.getDevice().getAppEui()));
						Map<String, String> res = connector.loraDeviceChk(deviceId, Globals.LORA_SUBSCRIVE_NM);
						
						if(res.get(LoraMsgConnector.RESPONSE_CODE).equals("404")){
							System.out.println("already subscribe OFF!");
						}else{
							connector.loraDeviceSubOff(deviceId, Globals.LORA_SUBSCRIVE_NM);
							if(res.get(LoraMsgConnector.RESPONSE_CODE).equals("200")){
								System.out.println("subscribe OFF! Success");
							}
						}
						*/
						/* kcc tre Off
						int adminCorp = restDeviceService.getAdminCorpFromSn(vehicle.getDeviceSn());
						if(adminCorp == 1)
						realayConnectService.sendMsgAsync(t.getUserKey(),key, vehicle.getDeviceSn(), new ObjectMapper().writeValueAsString(vehicle),"treOff",Globals.MODE);
						 */
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				
				//}
			}
			
			
			
			return createResponse(HttpStatus.OK);				
				
		}
		
		@ApiOperation(value = "차량 삭제 ", notes = "차량삭제 - force delete")		
		@ApiResponses(value = {
				@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/vehicle/{vehicleKey}" , method=RequestMethod.DELETE)
		public ResponseEntity deleteVehicle(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="vehicleKey") 
				@ApiParam(value = "target vehicleKey arr - delimeter ',' ")
				String vehicleKey
				) throws CreateResponseException, UnauthorizedException, BizException, VehicleNotFoundException, UserNotFoundException 
		{
				
			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			
			String[] vehicleKeys = vehicleKey.split(",");
			
			for(String vKey : vehicleKeys){
				v4.web.vo.data.VehicleVO vehicle = vehicleDataService.getVehicleFromKey(vKey);
				
				if(vehicle == null) 
					throw new VehicleNotFoundException();
				
				//if(account.관리자권한){
					vehicleService.deleteVehicle(vehicle.getVehicleKey());
					/*
					try {
						if(null != vehicle.getDevice()) {
							String deviceId = vehicle.getDevice().getDeviceId();
							LoraMsgConnector connector = new LoraMsgConnector(vehicle.getDevice().getAppEui(), Globals.LORA_KEYS.get(vehicle.getDevice().getAppEui()));
							Map<String, String> res = connector.loraDeviceChk(deviceId, Globals.LORA_SUBSCRIVE_NM);
							
							if(res.get(LoraMsgConnector.RESPONSE_CODE).equals("404")){
								System.out.println("already subscribe OFF!");
							}else{
								connector.loraDeviceSubOff(deviceId, Globals.LORA_SUBSCRIVE_NM);
								if(res.get(LoraMsgConnector.RESPONSE_CODE).equals("200")){
									System.out.println("subscribe OFF! Success");
								}
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					*/
					/* kcc tre Off
					int adminCorp = restDeviceService.getAdminCorpFromSn(vehicle.getDeviceSn());
					if(adminCorp == 1)
						realayConnectService.sendMsgAsync(t.getUserKey(),key, vehicle.getDeviceSn(), new ObjectMapper().writeValueAsString(vehicle),"treOff",Globals.MODE);
					*/
				
				//}else{//자식사용자
					//unlinkVehicle(accountKey,vehicleKey)
				//}
			}
			
			
			
			//return createResponse(HttpStatus.OK,new String[]{"result"}, "OK");			
			return createResponse(HttpStatus.OK);
		}
		
		
		
		
		
		
		
		
		@ApiOperation(value = "단말 업데이트", notes = "해당 단말의 각종 최신버전을 업데이트하기위한 api - pcMng용" , response = String.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_FORBIDDEN, message = "Forbidden"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/vehicleTemp/{deviceSn}/{convCode}/{totDistance}/{deviceSound}" , method=RequestMethod.GET)
		public ResponseEntity createVehileTempGet(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@PathVariable(value="deviceSn") 
				@ApiParam(value = "deviceSn")
				String deviceSn
				,@PathVariable(value="convCode") 
				@ApiParam(value = "convCode")
				String convCode
				,@PathVariable(value="totDistance") 
				@ApiParam(value = "totDistance")
				String totDistance
				,@PathVariable(value="deviceSound") 
				@ApiParam(value = "deviceSound")
				String deviceSound
				,@RequestHeader(value="key"  , required = true)
				@ApiParam(value = "key")
				String key
				) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, JSONException 
		{

			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			
			Map<String,String> chkConvCode = vehicleDBService.getVehicleDbInfo("ko",convCode);
			
			if(chkConvCode == null)
				return createResponse(HttpStatus.NOT_FOUND,new String[]{"result"}, "convCode.notFound");
			
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("searchDeviceSn", deviceSn);
			
			v4.web.vo.data.VehicleVO vehicle = vehicleDataService.getVehicle(param, null);
			
			if(vehicle == null)
				return createResponse(HttpStatus.NOT_FOUND, new String[]{"result"}, "not found device or none active device");
			
			String rtvPage = "pcmgrProto://updateVehicle/?connectionKey="+vehicle.getDevice().getConnectionKey()+"&convCode="+convCode+"&totDistance="+totDistance+"&deviceSound="+deviceSound+"&key="+t.getUserKey();
			
			return createResponse(HttpStatus.CREATED,new String[]{"result","orgVehicle"}, rtvPage,vehicle);
				
		}
		
		
		@ApiOperation(value = "차량 정보 갱신 - TODO", notes = "차량 정보를 갱신한다.(일반정보 - 버스,트럭,작업차 별도 api 생성할것)" , response = VehicleVO.class)		
		@ApiResponses(value = {
				@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/vehicle/{vehicleKey}" , method=RequestMethod.PUT)
		public ResponseEntity updateVehile(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="vehicleKey" )
				@ApiParam(value = "vehicleKey")
				String vehicleKey
				,@RequestBody(required = true)
				@ApiParam(value="value object VehicleVO")
				UpdateVehicleVO vehicleVo
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, VehicleNotFoundException 
		{
			
			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			v4.web.vo.data.VehicleVO vehicle = vehicleDataService.getVehicleFromKey(vehicleKey);
			
			if(vehicle == null)
				throw new VehicleNotFoundException();
			
			//update vehicle info
			//vehicleService.updateVehicle(vehicleVo);
			
			return createResponse(HttpStatus.OK);				
		}
		
		
		
		@ApiOperation(value = "차량 갱신 - TODO", notes = "차량 정보를 갱신한다.(단말과 상관있는 정보)(only accept 'totDistance','deviceSound','connectionKey','convCode')")		
		@ApiResponses(value = {
				@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/vehicle/{vehicleKey}/{field}/{val}" , method=RequestMethod.PUT)
		public ResponseEntity updateVehileField(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="vehicleKey" )
				@ApiParam(value = "vehicleKey")
				String vehicleKey
				,@PathVariable(value="field" )
				@ApiParam(value = "field")
				String field
				,@PathVariable(value="val" )
				@ApiParam(value = "val")
				String val
				
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, VehicleNotFoundException 
		{
			
			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			if(!field.equals("totDistance")&&!field.equals("deviceSound")&&!field.equals("connectionKey")&&!field.equals("convCode")){
				return createResponse(HttpStatus.BAD_REQUEST,new String[]{"result"}, "only accept 'totDistance','deviceSound','connectionKey','convCode'");	
			}
			
			
			
			v4.web.vo.data.VehicleVO vehicle = vehicleDataService.getVehicleFromKey(vehicleKey);
			
			if(vehicle == null)
				throw new VehicleNotFoundException();
			
			
			try {
				if(field.equals("totDistance")){
					val = String.valueOf(Integer.parseInt(val)*1000);
				}
			} catch (NumberFormatException e) {
				return createResponse(HttpStatus.BAD_REQUEST,new String[]{"result"}, "totDistance - NumberFormatException - val : "+val);
			}
			
			//vehicleService.updateVehicleField(vehicleKey,field,val);
			return createResponse(HttpStatus.OK);
		}
		
		
		/*
		@ApiOperation(value = "차량 리스트 조회", notes = "해당월에 자신이 한번이라도 배차했던 차량 리스트를 조회" , response = VehicleVO.class , responseContainer = "List")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my vehicles"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/monthVehicle" , method=RequestMethod.GET)
		public ResponseEntity monthVehicle(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestParam(value="yyyymm" , required = true)
				@ApiParam(value = "yyyymm")
				String yyyymm
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
		{
			
			TokenVO t = authentication(restTokenService,key);
			
			if(t == null) 
				throw new UnauthorizedException();
			
			UserVO user = restAccountService.getAccount(t.getUserKey());
			
			if(user == null) 
				throw new UserNotFoundException();
			
			String lang = user.getLang();
			
			
			List<VehicleVO> rtvList = null;
			if(user.getCorp() == 0)
				createResponse(HttpStatus.FORBIDDEN);
			else rtvList = restVehicleService.getVehicleListInCorpAllocateMonth(t.getUserKey(), yyyymm ,false, lang);
			
			return createResponse(HttpStatus.OK, restTokenService,t,new String[]{"result"}, rtvList);
				
		}
		
		*/
		
		
		/*
		@ApiOperation(value = "차량 위치 갱신전 체크 - TODO", notes = "차량의 현제위치를 변경하기전, 자신이 차량위치를 변경할수 있는지 체크(자신이 해당차량의 마지막 사용자이거나 , 차량이 주행중이 아닐때만 가능)")		
		@ApiResponses(value = {
				@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/vehicelLocationChangeBeCan/{vehicleKey}" , method=RequestMethod.GET)
		public ResponseEntity vehicelLocationChangeBeCan(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="vehicleKey")
				@ApiParam(value = "vehicleKey")
				String vehicleKey
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
		{
			
			TokenVO t = authentication(restTokenService,key);
			
			if(t == null) 
				throw new UnauthorizedException();
			
			UserVO user = restAccountService.getAccount(t.getUserKey());
			
			if(user == null) 
				throw new UserNotFoundException();
			
			String lang = user.getLang();
			
			int chk = restVehicleService.vehicelLocationChangeBeCan(t.getUserKey(),vehicleKey);
			
			if(chk == 0)
				return createResponse(HttpStatus.OK);	
			else{
				String msg = "";
				if(chk == 1) msg = "lastestTrip is not mine";
				if(chk == 2) msg = "now driving";
				return createResponse(HttpStatus.FORBIDDEN, restTokenService,t,new String[]{"result","resultMsg"}, chk , msg);
			}
			
		}
		*/
		
		/*
		@ApiOperation(value = "차량 위치 갱신 - TODO", notes = "차량 위치를 갱신한다.")		
		@ApiResponses(value = {
				@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/vehicelLocation/{vehicleKey}" , method=RequestMethod.PUT)
		public ResponseEntity vehicelLocation(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="vehicleKey")
				@ApiParam(value = "vehicleKey")
				String vehicleKey
				,@RequestParam(value="lat" , required = true)
				@ApiParam(value = "lat")
				double lat
				,@RequestParam(value="lon" , required = true)
				@ApiParam(value = "lon")
				double lon
				,@RequestParam(value="addr" , required = true)
				@ApiParam(value = "addr")
				String addr
				,@RequestParam(value="detail" , required = false)
				@ApiParam(value = "detail")
				String detail
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
		{
			
			TokenVO t = authentication(restTokenService,key);
			
			if(t == null) 
				throw new UnauthorizedException();
			
			UserVO user = restAccountService.getAccount(t.getUserKey());
			
			if(user == null) 
				throw new UserNotFoundException();
			
			String lang = user.getLang();
			
			int chk = restVehicleService.vehicelLocationChangeBeCan(t.getUserKey(),vehicleKey);
			
			if(chk == 0){
				
				VehicleVO vvo = new VehicleVO();
				vvo.setAccountKey(t.getUserKey());
				vvo.setVehicleKey(vehicleKey);
				vvo.setLastestLocation(lon+","+lat);
				vvo.setLastestLocationAddr(addr);
				vvo.setLastestLocationAddrDetail(detail);
				vvo.setLang(lang);
				
				restVehicleService.updateLastestLoc(vvo);
				
				return createResponse(HttpStatus.OK);	
			}else{
				String msg = "";
				if(chk == 1) msg = "lastestTrip is not mine";
				if(chk == 2) msg = "now driving";
				return createResponse(HttpStatus.FORBIDDEN, restTokenService,t,new String[]{"result","resultMsg"}, chk , msg);
			}
			
		}
		*/
		
		@ApiOperation(value = "버스 등록", notes = "버스 등록" , response = String.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_FORBIDDEN, message = "Forbidden"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/bus" , method=RequestMethod.POST)
		public ResponseEntity createBus(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@RequestBody(required = true)
				@Valid
				CreateBusVO vehicleVo
				,BindingResult br
				) throws CreateResponseException, UnauthorizedException, BizException, RestConnectException, NotFoundException 
		{

			if(br.hasErrors())
				return createValidErrorResponse(response,new String[]{"result"}, br ,null);

			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			AccountVO account = accountDataService.getAccountFromKey(t.getUserKey(),true);
			
			if(account == null)
				throw new UserNotFoundException();
			
			//여기까지 거의공통//
			
			DeviceVO device = null;
			if(!StrUtil.isNullToEmpty(vehicleVo.getDeviceSn())){
				device = deviceDataService.getDeviceFromSn(vehicleVo.getDeviceSn());
				if(device == null)
					throw new NotFoundException("can't find device : " + vehicleVo.getDeviceSn());
				else vehicleVo.setDeviceId(device.getDeviceId());
			}
			
			
			
			try {
				
				if(!account.getUnitLength().equals("km")){
					double totDist = UnitConvertUtil.unitToKm(vehicleVo.getTotDistance(), account.getUnitLength());
					vehicleVo.setTotDistance(UnitConvertUtil.kmToM(totDist));
				}else{
					vehicleVo.setTotDistance(UnitConvertUtil.kmToM(vehicleVo.getTotDistance()));
				}
				
				vehicleVo.setRegUser(account.getAccountKey());
				
				Map<String,Object> rtv = vehicleService.createBus(account,vehicleVo);
				
				if((HttpStatus)rtv.get("code") == HttpStatus.CREATED && !StrUtil.isNullToEmpty(vehicleVo.getDeviceSn())){
					
					String deviceId = device.getDeviceId();
					
					String vehicleKey = (String) rtv.get("msg");
					if(vehicleKey != null){
						/*
						LoraMsgConnector connector = new LoraMsgConnector(device.getAppEui(), Globals.LORA_KEYS.get(device.getAppEui()));
						Map<String,String> res = connector.loraDeviceChk(deviceId, Globals.LORA_SUBSCRIVE_NM);
						if(res.get(LoraMsgConnector.RESPONSE_CODE).equals("200")){
							System.out.println("already subscribe ON!");
						}else{
							res = connector.loraDeviceSubOn(deviceId, Globals.LORA_SUBSCRIVE_NM, Globals.LORA_SUBSCRIVE);
							if(res.get(LoraMsgConnector.RESPONSE_CODE).equals("201")){
								System.out.println("subscribe ON! Success");
							}
						}
						*/
					}
				}
				
				return createResponse((HttpStatus)rtv.get("code"),new String[]{"result"}, (String)rtv.get("msg"));	
			}catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return createResponse(HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
		}
		
		@ApiOperation(value = "버스 - 단말 매치", notes = "버스 - 단말 매치" , response = String.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_FORBIDDEN, message = "Forbidden"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/bus/active" , method=RequestMethod.POST)
		public ResponseEntity activeBus(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@RequestBody(required = true)
				@Valid
				CreateReActiveVehicleVO vehicleVo
				,BindingResult br
				) throws CreateResponseException, UnauthorizedException, BizException, RestConnectException, NotFoundException 
		{

			if(br.hasErrors())
				return createValidErrorResponse(response,new String[]{"result"}, br ,null);

			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			AccountVO account = accountDataService.getAccountFromKey(t.getUserKey(),true);
			
			if(account == null)
				throw new UserNotFoundException();
			//여기까지 거의공통//
			
			
			DeviceVO device = deviceDataService.getDeviceFromSn(vehicleVo.getDeviceSn());
			if(device == null)
				throw new NotFoundException("can't find device : " + vehicleVo.getDeviceSn());
			
			String deviceId = device.getDeviceId();
			
			CreateBusVO cvvo = new CreateBusVO();
			
			cvvo.setVehicleKey(vehicleVo.getVehicleKey());
			cvvo.setDeviceSn(vehicleVo.getDeviceSn());
			cvvo.setCorpKey(t.getCorpKey());
			cvvo.setDeviceId(deviceId);
			if(StrUtil.isNullToEmpty(cvvo.getDeviceSound())) cvvo.setDeviceSound("0");
			
			try {
				
				Map<String,Object> rtv = vehicleService.createBus(account,cvvo);
				
				if((HttpStatus)rtv.get("code") == HttpStatus.CREATED){
					String vehicleKey = (String) rtv.get("msg");
					if(vehicleKey != null){
						/*
						LoraMsgConnector connector = new LoraMsgConnector(device.getAppEui(), Globals.LORA_KEYS.get(device.getAppEui()));
						Map<String,String> res = connector.loraDeviceChk(deviceId, Globals.LORA_SUBSCRIVE_NM);
						if(res.get(LoraMsgConnector.RESPONSE_CODE).equals("200")){
							System.out.println("already subscribe ON!");
						}else{
							res = connector.loraDeviceSubOn(deviceId, Globals.LORA_SUBSCRIVE_NM, Globals.LORA_SUBSCRIVE);
							if(res.get(LoraMsgConnector.RESPONSE_CODE).equals("201")){
								System.out.println("subscribe ON! Success");
							}
						}
						*/
					}
				}
				return createResponse((HttpStatus)rtv.get("code"),new String[]{"result"}, (String)rtv.get("msg"));	
			}catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return createResponse(HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
		}
		
		
		@ApiOperation(value = "트럭 등록", notes = "트럭 등록" , response = String.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_FORBIDDEN, message = "Forbidden"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/truck" , method=RequestMethod.POST)
		public ResponseEntity createTruck(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@RequestBody(required = true)
				@Valid
				CreateTruckVO vehicleVo
				,BindingResult br
				) throws CreateResponseException, UnauthorizedException, BizException, RestConnectException, NotFoundException 
		{

			if(br.hasErrors())
				return createValidErrorResponse(response,new String[]{"result"}, br ,null);

			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			AccountVO account = accountDataService.getAccountFromKey(t.getUserKey(),true);
			
			if(account == null)
				throw new UserNotFoundException();
			//여기까지 거의공통//
			
			
			DeviceVO device = null;
			
			if(!StrUtil.isNullToEmpty(vehicleVo.getDeviceSn())){
				device = deviceDataService.getDeviceFromSn(vehicleVo.getDeviceSn());
				if(device == null)
					throw new NotFoundException("can't find device : " + vehicleVo.getDeviceSn());
				else vehicleVo.setDeviceId(device.getDeviceId());
			}
			
			try {
				
				if(!account.getUnitLength().equals("km")){
					double totDist = UnitConvertUtil.unitToKm(vehicleVo.getTotDistance(), account.getUnitLength());
					vehicleVo.setTotDistance(UnitConvertUtil.kmToM(totDist));
				}else{
					vehicleVo.setTotDistance(UnitConvertUtil.kmToM(vehicleVo.getTotDistance()));
				}
				
				vehicleVo.setRegUser(account.getAccountKey());
				
				Map<String,Object> rtv = vehicleService.createTruck(account,vehicleVo);
				
				if((HttpStatus)rtv.get("code") == HttpStatus.CREATED && !StrUtil.isNullToEmpty(vehicleVo.getDeviceSn())){
					
					String deviceId = device.getDeviceId();
					
					String vehicleKey = (String) rtv.get("msg");
					if(vehicleKey != null){
						/*
						LoraMsgConnector connector = new LoraMsgConnector(device.getAppEui(), Globals.LORA_KEYS.get(device.getAppEui()));
						Map<String,String> res = connector.loraDeviceChk(deviceId, Globals.LORA_SUBSCRIVE_NM);
						if(res.get(LoraMsgConnector.RESPONSE_CODE).equals("200")){
							System.out.println("already subscribe ON!");
						}else{
							res = connector.loraDeviceSubOn(deviceId, Globals.LORA_SUBSCRIVE_NM, Globals.LORA_SUBSCRIVE);
							if(res.get(LoraMsgConnector.RESPONSE_CODE).equals("201")){
								System.out.println("subscribe ON! Success");
							}
						}
						*/
					}
				}
				
				return createResponse((HttpStatus)rtv.get("code"),new String[]{"result"}, (String)rtv.get("msg"));	
			}catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return createResponse(HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
		}
		
		@ApiOperation(value = "트럭 - 단말 매치", notes = "트럭 - 단말 매치" , response = String.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_FORBIDDEN, message = "Forbidden"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/truck/active" , method=RequestMethod.POST)
		public ResponseEntity activeTruck(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@RequestBody(required = true)
				@Valid
				CreateReActiveVehicleVO vehicleVo
				,BindingResult br
				) throws CreateResponseException, UnauthorizedException, BizException, RestConnectException, NotFoundException 
		{

			if(br.hasErrors())
				return createValidErrorResponse(response,new String[]{"result"}, br ,null);

			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			AccountVO account = accountDataService.getAccountFromKey(t.getUserKey(),true);
			
			if(account == null)
				throw new UserNotFoundException();
			//여기까지 거의공통//
			
			
			DeviceVO device = deviceDataService.getDeviceFromSn(vehicleVo.getDeviceSn());
			if(device == null)
				throw new NotFoundException("can't find device : " + vehicleVo.getDeviceSn());
			
			String deviceId = device.getDeviceId();
			
			CreateTruckVO cvvo = new CreateTruckVO();
			
			cvvo.setVehicleKey(vehicleVo.getVehicleKey());
			cvvo.setDeviceSn(vehicleVo.getDeviceSn());
			cvvo.setCorpKey(t.getCorpKey());
			cvvo.setDefaultAccountKey(account.getAccountKey());
			cvvo.setDeviceId(deviceId);
			if(StrUtil.isNullToEmpty(cvvo.getDeviceSound())) cvvo.setDeviceSound("0");
			
			try {
				
				Map<String,Object> rtv = vehicleService.createTruck(account,cvvo);
				
				if((HttpStatus)rtv.get("code") == HttpStatus.CREATED){
					String vehicleKey = (String) rtv.get("msg");
					if(vehicleKey != null){
						/*
						LoraMsgConnector connector = new LoraMsgConnector(device.getAppEui(), Globals.LORA_KEYS.get(device.getAppEui()));
						Map<String,String> res = connector.loraDeviceChk(deviceId, Globals.LORA_SUBSCRIVE_NM);
						if(res.get(LoraMsgConnector.RESPONSE_CODE).equals("200")){
							System.out.println("already subscribe ON!");
						}else{
							res = connector.loraDeviceSubOn(deviceId, Globals.LORA_SUBSCRIVE_NM, Globals.LORA_SUBSCRIVE);
							if(res.get(LoraMsgConnector.RESPONSE_CODE).equals("201")){
								System.out.println("subscribe ON! Success");
							}
						}
						*/
					}
				}
				return createResponse((HttpStatus)rtv.get("code"),new String[]{"result"}, (String)rtv.get("msg"));	
			}catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return createResponse(HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
		}
		
		@ApiOperation(value = "작업차 등록", notes = "작업차 등록" , response = String.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_FORBIDDEN, message = "Forbidden"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/etcVehicle" , method=RequestMethod.POST)
		public ResponseEntity createEtc(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@RequestBody(required = true)
				@Valid
				CreateEtcVO vehicleVo
				,BindingResult br
				) throws CreateResponseException, UnauthorizedException, BizException, RestConnectException, NotFoundException 
		{

			if(br.hasErrors())
				return createValidErrorResponse(response,new String[]{"result"}, br ,null);

			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			AccountVO account = accountDataService.getAccountFromKey(t.getUserKey(),true);
			
			if(account == null)
				throw new UserNotFoundException();
			//여기까지 거의공통//
			
			try {
				
				if(!account.getUnitLength().equals("km")){
					double totDist = UnitConvertUtil.unitToKm(vehicleVo.getTotDistance(), account.getUnitLength());
					vehicleVo.setTotDistance(UnitConvertUtil.kmToM(totDist));
				}else{
					vehicleVo.setTotDistance(UnitConvertUtil.kmToM(vehicleVo.getTotDistance()));
				}
				
				Map<String,Object> rtv = vehicleService.createEtc(account,vehicleVo);
				
				return createResponse((HttpStatus)rtv.get("code"),new String[]{"result"}, (String)rtv.get("msg"));	
			}catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return createResponse(HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
		}
		
		@ApiOperation(value = "차량 주행정보 조회", notes = "차량 주행정보를 조회한다." , response = v4.web.vo.data.VehicleLocationVO.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of driving info"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/vehicle/location" , method=RequestMethod.GET)
		public ResponseEntity getLocation(HttpServletRequest req,HttpServletResponse res
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@RequestParam(value="vehicleKeys" , required = false)
				@ApiParam(value = "target vehicles - delimeter ','")
				String vehicleKeys
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, VehicleNotFoundException 
		{
			
			TokenVO t = authentication(tokenService,key);
			
			if(t == null) 
				throw new UnauthorizedException();
			
			Map<String,Object> param = new HashMap<String,Object>();
			
			
			if(StrUtil.isNullToEmpty(vehicleKeys)){
				vehicleKeys = "";
				param.put("vehicleKeys",vehicleKeys);
			}else{
				String[] arrVehicle = vehicleKeys.split(",");
				param.put("vehicleKeys",arrVehicle);
			}
			
			param.put("searchCorpKey",t.getCorpKey());
			
			
			
			
			param.put("searchDrivingState", req.getParameter("searchDrivingState")); //1주행중 ,0 x
			param.put("searchAllocateSate", req.getParameter("searchAllocateSate")); //1배차중,고정차량 ,0 x
			param.put("searchVehicleGroup", req.getParameter("searchVehicleGroup")); //차량그룹
			param.put("searchDriverGroup", req.getParameter("searchDriverGroup")); //driver그룹
			param.put("searchDriverKey", req.getParameter("searchDriverKey")); //driver
			param.put("searchAreaLev1", req.getParameter("searchAreaLev1")); //시/도
			param.put("searchAreaLev2", req.getParameter("searchAreaLev2")); //시군구
			
			List<v4.web.vo.data.VehicleLocationVO> arrVvo = vehicleDataService.getVehicleLocationList(param, null);
			
			Map<String,Object> rtvMap = new HashMap<String,Object>();
			rtvMap.put("result", arrVvo);
					
			return createResponse(HttpStatus.OK,true,rtvMap);
				
		}
}
