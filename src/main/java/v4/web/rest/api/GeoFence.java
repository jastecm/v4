package v4.web.rest.api;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import v4.ex.exc.BizException;
import v4.ex.exc.CreateResponseException;
import v4.ex.exc.extents.UnauthorizedException;
import v4.ex.exc.extents.UserNotFoundException;
import v4.ex.exc.extents.VehicleNotFoundException;
import v4.web.controller.BasicWebViewController;
import v4.web.rest.service.GeoFenceService;
import v4.web.rest.service.TokenService;
import v4.web.rest.service.data.AccountDataService;
import v4.web.rest.service.data.VehicleDataService;
import v4.web.vo.AccountVO;
import v4.web.vo.CreateGeoFenceVO;
import v4.web.vo.GeoFenceVO;
import v4.web.vo.TokenVO;
import v4.web.vo.VehicleVO;


/*
 * @Class name : LoginController
 * @Description : 
 * @Modification Information
 * 
 * @author 
 * @since 
 * @version 
 * @see
 *
 */
@Api(value="geoFenceApi")
@RestController
public class GeoFence extends BasicWebViewController{
		
		private final int max = 5;
		
		ObjectMapper mapper = new ObjectMapper();
	
		@Autowired
		TokenService tokenService;
		
		@Autowired
		VehicleDataService vehicleDataService;
		
		@Autowired
		AccountDataService accountDataService;
		
		@Autowired
		GeoFenceService geoFenceService;
		
		@ApiOperation(value = "geo펜스 등록", notes = "geo펜스 등록(단일 차량)")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of insure info"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/geofence" , method=RequestMethod.POST)
		public ResponseEntity geofenceSave(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@RequestBody(required = true)
				CreateGeoFenceVO vo
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, VehicleNotFoundException 
		{
			
			TokenVO t = authentication(tokenService,key);
			
			if(t == null) 
				throw new UnauthorizedException();
			
			if(vo.getLon() == null || vo.getLon() == 0 || vo.getLat() == null || vo.getLat() == 0)
				return createResponse(HttpStatus.BAD_REQUEST,new String[]{"result"}, "latlon.required");
			
			if(vo.getDistance() == null || vo.getDistance() <= 0)
				return createResponse(HttpStatus.BAD_REQUEST,new String[]{"result","resultMsg"}, "distance.badRequest","distance must be greater than 0");
			
			
			v4.web.vo.data.AccountVO account = accountDataService.getAccountFromKey(t.getUserKey(),true);
			
			vo.setCorpKey(t.getCorpKey());
			
			//
			GeoFenceVO chk = new GeoFenceVO();//geoFenceService.getGeoFenceAllChk(vo);
			if(chk != null)
				return createResponse(HttpStatus.CONFLICT,new String[]{"result"}, "geo.conflict");
			
			int geoFenceMax = max;
			
			geoFenceMax = account.getCorp().getGeoMaxCnt();
			
			if(t.getServiceGrade().equals("3"))
				geoFenceMax = 9999;
				
			//geoFenceService.addGeoFence(vo,geoFenceMax);
			
			return createResponse(HttpStatus.CREATED);
				
		}
		
		@ApiOperation(value = "geo펜스 조회", notes = "geo펜스 조회")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of insure info"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/geofence" , method=RequestMethod.GET)
		public ResponseEntity geofenceList(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				//limit offset searchType searchText counting
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, VehicleNotFoundException 
		{
			
			return createResponse(HttpStatus.OK);
				
		}
		
		@ApiOperation(value = "geo펜스 조회", notes = "geo펜스 조회" , response = GeoFenceVO.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/geofence/{fenceKey}" , method=RequestMethod.GET)
		public ResponseEntity getGeoFenceList(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="fenceKey") 
				@ApiParam(value = "target geoFence")
				String fenceKey 
				) throws CreateResponseException, UnauthorizedException, BizException, VehicleNotFoundException, UserNotFoundException 
		{
			//check fence-corpKey
			return createResponse(HttpStatus.OK);
		}
		
		@ApiOperation(value = "geo펜스 모든 차량추가", notes = "geo펜스 차량추가")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of insure info"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/geofence/{fenceKey}/vehicle" , method=RequestMethod.POST)
		public ResponseEntity geofenceAddVehicleAll(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="vehicleKey") 
				@ApiParam(value = "target vehicle")
				String vehicleKey
				,@PathVariable(value="fenceKey") 
				@ApiParam(value = "fenceKey")
				String fenceKey				
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, VehicleNotFoundException 
		{
			
			//check fence-corpKey
			return createResponse(HttpStatus.CREATED);
		}
		
		@ApiOperation(value = "geo펜스 모든 차량삭제", notes = "geo펜스 차량삭제")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of insure info"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/geofence/{fenceKey}/vehicle" , method=RequestMethod.DELETE)
		public ResponseEntity geofenceDelVehicleAll(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="vehicleKey") 
				@ApiParam(value = "target vehicle")
				String vehicleKey
				,@PathVariable(value="fenceKey") 
				@ApiParam(value = "fenceKey")
				String fenceKey				
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, VehicleNotFoundException 
		{

			//check fence-corpKey
			return createResponse(HttpStatus.CREATED);
				
		}
		
		@ApiOperation(value = "geo펜스 차량추가", notes = "geo펜스 차량추가")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of insure info"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/geofence/{fenceKey}/vehicle/{vehicleKey}" , method=RequestMethod.POST)
		public ResponseEntity geofenceAddVehicle(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="vehicleKey") 
				@ApiParam(value = "target vehicle")
				String vehicleKey
				,@PathVariable(value="fenceKey") 
				@ApiParam(value = "fenceKey")
				String fenceKey				
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, VehicleNotFoundException 
		{
			
			//check fence-corpKey
			//check vehicle-corpKey
			//check vehicle-duplicate
			return createResponse(HttpStatus.CREATED);
				
		}
		
		@ApiOperation(value = "geo펜스 차량삭제", notes = "geo펜스 차량삭제")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of insure info"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/geofence/{fenceKey}/vehicle/{vehicleKey}" , method=RequestMethod.DELETE)
		public ResponseEntity geofenceDelVehicle(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="vehicleKey") 
				@ApiParam(value = "target vehicle")
				String vehicleKey
				,@PathVariable(value="fenceKey") 
				@ApiParam(value = "fenceKey")
				String fenceKey				
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, VehicleNotFoundException 
		{
			
			//check fence-corpKey
			//check vehicle-corpKey
			//check already delete veihcle - 404
			return createResponse(HttpStatus.CREATED);
				
		}
		@ApiOperation(value = "geo펜스 수정", notes = "geo펜스 수정" , response = GeoFenceVO.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of insure info"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/geofence/{fenceKey}" , method=RequestMethod.PUT)
		public ResponseEntity corpGeofenceUpdate(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="fenceKey") 
				@ApiParam(value = "fenceKey")
				String fenceKey
				,@RequestBody(required = true)
				CreateGeoFenceVO vo
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, VehicleNotFoundException 
		{
			//check fence-corpKey
			//if lat,lon - duplicate chk
			//white space -> null chk
			//update 
			return createResponse(HttpStatus.OK);
				
		}
		
		
		@ApiOperation(value = "geo펜스 삭제", notes = "geo펜스 삭제" , response = GeoFenceVO.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of insure info"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/geofence/{fenceKey}" , method=RequestMethod.DELETE)
		public ResponseEntity delFence(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="fenceKey") 
				@ApiParam(value = "fenceKey")
				String fenceKey
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, VehicleNotFoundException 
		{
			//check fence-corpKey
			//check fence-vehicle delete
			return createResponse(HttpStatus.OK);
		}
		
		@ApiOperation(value = "geo펜스Hstr 조회", notes = "geo펜스 조회")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of insure info"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/geofenceHstr/{target}/{targetKey}" , method=RequestMethod.GET)
		public ResponseEntity geofenceHstrList(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="target") 
				@ApiParam(value = "vehicle/account")
				String fenceKey
				,@PathVariable(value="targetKey") 
				@ApiParam(value = "vehicleKey/accountKey")
				String targetKey
				//limit offset searchStartDate searchEndDate counting
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, VehicleNotFoundException 
		{
			
			return createResponse(HttpStatus.OK);
				
		}
}
