package v4.web.rest.api;

import java.net.HttpURLConnection;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.codehaus.jackson.map.ObjectMapper;
import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javassist.NotFoundException;
import v4.ex.exc.BizException;
import v4.ex.exc.CreateResponseException;
import v4.ex.exc.extents.UnauthorizedException;
import v4.ex.exc.extents.UserNotFoundException;
import v4.ex.exc.extents.VehicleNotFoundException;
import v4.web.controller.BasicWebViewController;
import v4.web.rest.service.PenaltyService;
import v4.web.rest.service.TokenService;
import v4.web.rest.service.data.PenaltyDataService;
import v4.web.vo.CreateBusRouteStationVO;
import v4.web.vo.CreatePenaltyVO;
import v4.web.vo.TokenVO;
import v4.web.vo.data.BusRouteVO;
import v4.web.vo.data.PenaltyVO;

@Api(value="penaltyApi")
@RestController
public class Penalty extends BasicWebViewController {
	
	ObjectMapper mapper = new ObjectMapper();
	
	@Autowired
	TokenService tokenService;
	
	@Autowired
	PenaltyService penaltyService;
	
	@Autowired
	PenaltyDataService penaltyDataService;
	
	@ApiOperation(value = "벌점/과태료 조회", notes = "벌점/과태료를 조회한다." , response = v4.web.vo.data.PenaltyVO.class , responseContainer = "List")		
	@ApiResponses(value = {
			@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my vehicles"),
			@ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
			@ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
			@ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/penalty" , method=RequestMethod.GET)
	public ResponseEntity getPenaltyList(HttpServletRequest req,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@RequestParam(value="limit" , defaultValue = "10")
			int limit
			,@RequestParam(value="offset" , defaultValue = "0")
			int offset
			,@RequestParam(value="searchType" , required = false)
			String searchType
			,@RequestParam(value="searchText" , required = false)
			String searchText
			,@RequestParam(value="counting" , defaultValue = "false")
			boolean counting
			) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
			return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
		
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("searchCorpKey", t.getCorpKey());
		param.put("limit", limit);
		param.put("offset", offset);
		param.put("searchType", searchType);
		param.put("searchText", searchText);
		
		String order = req.getParameter("searchOrder");
		param.put("searchOrder", order);
		
		param.put("searchStartDate", req.getParameter("searchStartDate"));
		param.put("searchEndDate", req.getParameter("searchEndDate"));
		
		param.put("penaltyType", req.getParameter("penaltyType"));
		param.put("violationType", req.getParameter("violationType"));
		param.put("paymentType", req.getParameter("paymentType"));
		
		param.put("accountKey", t.getUserKey());
		
		List<PenaltyVO> rtvList = penaltyDataService.getPenaltyList(param);
		
		Map<String,Object> rtv = new HashMap<String,Object>();
		rtv.put("result", rtvList);
		
		if(counting){
			int totCnt = penaltyDataService.getPenaltyListCnt(param);
			int penaltyPoint = penaltyDataService.getPenaltyPointCnt(param);
			int penaltyPrice = penaltyDataService.getPenaltyPriceCnt(param);
			rtv.put("totalCnt", totCnt);
			rtv.put("penaltyPoint", penaltyPoint);
			rtv.put("penaltyPrice", penaltyPrice);
		}
		
		return createResponse(HttpStatus.OK,true,rtv);
	}
	
	@ApiOperation(value = "벌점/과태료 등록", notes = "벌점/과태료를 등록한다.")		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/penalty" , method=RequestMethod.POST)
	public ResponseEntity createPenalty(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@RequestBody(required = true)
			@Valid
			CreatePenaltyVO createPenaltyVO
			,BindingResult br
			) throws CreateResponseException, UnauthorizedException, BizException , NotFoundException, UserNotFoundException, VehicleNotFoundException
	{
		
		if(br.hasErrors())
			return createValidErrorResponse(response,new String[]{"result"}, br ,null);
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		createPenaltyVO.setCorpKey(t.getCorpKey());
		createPenaltyVO.setAccountKey(t.getUserKey());
		
		penaltyService.createPenalty(createPenaltyVO);
		
		return createResponse(HttpStatus.OK);
	}
	
	@ApiOperation(value = "벌점/과태료 삭제", notes = "벌점/과태료 정보를 삭제한다.")		
	@ApiResponses(value = {
			@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/penalty/{penaltyKeys}" , method=RequestMethod.DELETE)
	public ResponseEntity deletePenalty(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@PathVariable(value="penaltyKeys") 
			@ApiParam(value = "target penaltyKey arr - delimeter ',' ")
			String penaltyKeys
			) throws CreateResponseException, UnauthorizedException, BizException ,NotFoundException, UserNotFoundException, VehicleNotFoundException
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("corpKey", t.getCorpKey());
		
		List<String> penaltyKey = Arrays.asList(penaltyKeys.split(","));
		
		for(int i = 0 ; i < penaltyKey.size() ; i++){
			param.put("penaltyKey", penaltyKey.get(i));
			penaltyService.deletePenalty(param);
		}
		
		return createResponse(HttpStatus.OK);
	}
}
