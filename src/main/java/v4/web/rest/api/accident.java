package v4.web.rest.api;

import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.codehaus.jackson.map.ObjectMapper;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.common.Globals;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javassist.NotFoundException;
import v4.ex.exc.BizException;
import v4.ex.exc.CreateResponseException;
import v4.ex.exc.extents.UnauthorizedException;
import v4.ex.exc.extents.UserNotFoundException;
import v4.ex.exc.extents.VehicleNotFoundException;
import v4.mqtt.component.MqttComponent;
import v4.web.controller.BasicWebViewController;
import v4.web.rest.service.AccidentService;
import v4.web.rest.service.TokenService;
import v4.web.rest.service.data.AccidentDataService;
import v4.web.vo.CreateAccidentVO;
import v4.web.vo.CreateBusRouteStationVO;
import v4.web.vo.TokenVO;
import v4.web.vo.UpdateAccidentResultVO;
import v4.web.vo.UpdateBusRouteStationVO;
import v4.web.vo.data.AccidentVO;
import v4.web.vo.data.BusRouteVO;
import v4.web.vo.data.DrivingPurposeVO;

@Api(value="accidentApi")
@RestController
public class accident extends BasicWebViewController {
	
	ObjectMapper mapper = new ObjectMapper();
	
	@Autowired
	TokenService tokenService;
	
	@Autowired
	AccidentService accidentServcie;
	
	@Autowired
	AccidentDataService accidentDataService;
	
	@Autowired
	MqttComponent mqttComponent; 
	
	public void startSubscribe(String topic) throws Exception {
		try {
			mqttComponent.getMattClinet().subscribe(topic, 0);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@ApiOperation(value = "mqtt test", notes = "mqtt test")		
	@ApiResponses(value = {
			@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my vehicles"),
			@ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
			@ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
			@ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/mqtt" , method=RequestMethod.GET)
	public ResponseEntity getAccidentList(HttpServletRequest req,HttpServletResponse response
			) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
	{
		
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		String result = "";
		System.out.println("mqtt status start");
		try {
			if (!mqttComponent.getMattClinet().isConnected()){
				System.out.println("mqtt status disconnect");
				
				try {
					MqttConnectOptions op = new MqttConnectOptions();
					op.setUserName(Globals.MQTT_PROP_USER);
					op.setPassword(Globals.MQTT_PROP_PW.toCharArray());		
					mqttComponent.getMattClinet().connect(op);
					
					if(mqttComponent.getMattClinet().isConnected()){
						System.out.println("----- startSubscribe ----- \n /vonxc/#/rx");
						String topic = "vonxc/+/rx";
						if(!Globals.DEV) startSubscribe(topic);
						topic = "vonxc/+/tx";
						if(!Globals.DEV) startSubscribe(topic);
					}
					
				} catch (MqttException e) {
					e.printStackTrace();
					System.out.println("mqtt reconnection fail");
				}
				
			}
			if(mqttComponent.getMattClinet().isConnected()) result = "1";
			else result = "0";			

			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return createResponse(HttpStatus.OK,new String[]{"result"},result);
		
	}
	
	@ApiOperation(value = "사고 보고서 조회", notes = "사고 보고서를 조회한다." , response = v4.web.vo.data.AccidentVO.class , responseContainer = "List")		
	@ApiResponses(value = {
			@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my vehicles"),
			@ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
			@ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
			@ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/accident" , method=RequestMethod.GET)
	public ResponseEntity getAccidentList(HttpServletRequest req,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@RequestParam(value="limit" , defaultValue = "10")
			int limit
			,@RequestParam(value="offset" , defaultValue = "0")
			int offset
			,@RequestParam(value="searchType" , required = false)
			String searchType
			,@RequestParam(value="searchText" , required = false)
			String searchText
			,@RequestParam(value="counting" , defaultValue = "false")
			boolean counting
			) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
			return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
		
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("searchCorpKey", t.getCorpKey());
		param.put("limit", limit);
		param.put("offset", offset);
		param.put("searchType", searchType);
		param.put("searchText", searchText);
		
		String order = req.getParameter("searchOrder");
		param.put("searchOrder", order);
		
		param.put("searchStartDate", req.getParameter("searchStartDate"));
		param.put("searchEndDate", req.getParameter("searchEndDate"));
		
		param.put("accidentKey", req.getParameter("accidentKey"));
		
		List<AccidentVO> rtvList = accidentDataService.getAccidentList(param);
		
		Map<String,Object> rtv = new HashMap<String,Object>();
		rtv.put("result", rtvList);
		
		if(counting){
			int totCnt = accidentDataService.getAccidentListCnt(param);
			rtv.put("totalCnt", totCnt);
		}
		
		return createResponse(HttpStatus.OK,true,rtv);
	}
	
	@ApiOperation(value = "보험 리스트 조회", notes = "보험 리스트를 조회한다." , response = v4.web.vo.data.DrivingPurposeVO.class , responseContainer = "List")		
	@ApiResponses(value = {
			@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my vehicles"),
			@ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
			@ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
			@ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/insurance" , method=RequestMethod.GET)
	public ResponseEntity getVehicleList(HttpServletRequest req,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
			return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
		
		List<Map<String,Object>> rtvList = accidentServcie.getInsureList();
		
		Map<String,Object> rtv = new HashMap<String,Object>();
		rtv.put("result", rtvList);
		
		
		
		return createResponse(HttpStatus.OK,true,rtv);
		
	}
	
	@ApiOperation(value = "사고 보고서 등록", notes = "사고 보고서를 등록한다.")		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/accident" , method=RequestMethod.POST)
	public ResponseEntity createAccident(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@RequestBody(required = true)
			@Valid
			CreateAccidentVO createAccidentVO
			,BindingResult br
			) throws CreateResponseException, UnauthorizedException, BizException , NotFoundException, UserNotFoundException, VehicleNotFoundException
	{
		
		if(br.hasErrors())
			return createValidErrorResponse(response,new String[]{"result"}, br ,null);
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		createAccidentVO.setCorpKey(t.getCorpKey());
		createAccidentVO.setRegUser(t.getUserKey());
		
		accidentServcie.createAccident(createAccidentVO);
		
		
		return createResponse(HttpStatus.OK);
	}
	
	@ApiOperation(value = "사고보고서 처리결과 입력", notes = "사고보고서 처리 결과를 입력한다.")		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/accidentResult" , method=RequestMethod.PUT)
	public ResponseEntity createAccidentResult(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@RequestBody(required = true)
			@Valid
			UpdateAccidentResultVO updateAccidentResultVO
			,BindingResult br
			) throws CreateResponseException, UnauthorizedException, BizException , NotFoundException, UserNotFoundException, VehicleNotFoundException
	{
		
		if(br.hasErrors())
			return createValidErrorResponse(response,new String[]{"result"}, br ,null);
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null)
			throw new UnauthorizedException();
		
		updateAccidentResultVO.setCorpKey(t.getCorpKey());
		
		accidentServcie.updateAccidentResult(updateAccidentResultVO);
		
		return createResponse(HttpStatus.OK);
	}
}
