package v4.web.rest.api;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.codehaus.jackson.map.ObjectMapper;
import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javassist.NotFoundException;
import v4.ex.exc.BizException;
import v4.ex.exc.CreateResponseException;
import v4.ex.exc.extents.UnauthorizedException;
import v4.ex.exc.extents.UserNotFoundException;
import v4.ex.exc.extents.VehicleNotFoundException;
import v4.web.controller.BasicWebViewController;
import v4.web.rest.service.ExpensesService;
import v4.web.rest.service.TokenService;
import v4.web.rest.service.data.ExpensesDataService;
import v4.web.rest.service.data.VehicleDataService;
import v4.web.vo.CreateExpensesBulkVO;
import v4.web.vo.CreateExpensesVO;
import v4.web.vo.TokenVO;
import v4.web.vo.data.BusRouteVO;
import v4.web.vo.data.ExpensesTotalVO;
import v4.web.vo.data.ExpensesVO;


@Api(value="expensesApi")
@RestController
public class Expenses extends BasicWebViewController {
	
	ObjectMapper mapper = new ObjectMapper();
	
	@Autowired
	TokenService tokenService;
	
	@Autowired
	ExpensesService expensesService;
	
	@Autowired
	ExpensesDataService expensesDataService;
	
	@Autowired
	VehicleDataService vehicleDataService;
	
	@ApiOperation(value = "차량 경비 등록", notes = "차량 경비를 등록한다." , response = CreateExpensesVO.class)		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/expenses" , method=RequestMethod.POST)
	public ResponseEntity createExpenses(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@RequestBody(required = true)
			@Valid
			List<CreateExpensesVO> createExpensesVO
			,BindingResult br
			) throws CreateResponseException, UnauthorizedException, BizException , NotFoundException, UserNotFoundException, VehicleNotFoundException
	{
		
		if(br.hasErrors())
			return createValidErrorResponse(response,new String[]{"result"}, br ,null);
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		for(CreateExpensesVO vo : createExpensesVO){
			vo.setCorp(t.getCorpKey());
			vo.setAccountKey(t.getUserKey());
			expensesService.createExpenses(vo);
		}
		
		return createResponse(HttpStatus.OK);
	}
	
	@ApiOperation(value = "차량 경비 대량 등록", notes = "차량 경비를 대량 등록한다." , response = CreateExpensesBulkVO.class)		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/expensesBulk" , method=RequestMethod.POST)
	public ResponseEntity createExpensesBulk(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@RequestBody(required = true)
			@Valid
			List<CreateExpensesBulkVO> createExpensesVO
			,BindingResult br
			) throws CreateResponseException, UnauthorizedException, BizException , NotFoundException, UserNotFoundException, VehicleNotFoundException
	{
		
		if(br.hasErrors())
			return createValidErrorResponse(response,new String[]{"result"}, br ,null);
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		for(CreateExpensesBulkVO vo : createExpensesVO){
			
			v4.web.vo.data.VehicleVO vehicle = vehicleDataService.getVehicleFromPlateNum(vo.getPlateNum());
			if(vehicle == null) 
				throw new VehicleNotFoundException();
			
			vo.setCorp(t.getCorpKey());
			vo.setAccountKey(t.getUserKey());
			vo.setVehicleKey(vehicle.getVehicleKey());
			expensesService.createExpensesBulk(vo);
		}
		
		return createResponse(HttpStatus.OK);
	}
	
	@ApiOperation(value = "전체 일반차량 경비 조회", notes = "전체 일반차량 경비 조회한다." , response = v4.web.vo.data.ExpensesTotalVO.class, responseContainer = "List")		
	@ApiResponses(value = {
			@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my vehicles"),
			@ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
			@ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
			@ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/expensesTotal/{expensesDate}" , method=RequestMethod.GET)
	public ResponseEntity busRouteStationList(HttpServletRequest req,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@PathVariable(value="expensesDate") 
			@ApiParam(value = "target expensesDate timeStamp")
			String expensesDate
			,@RequestParam(value="searchType" , required = false)
			String searchType
			,@RequestParam(value="searchText" , required = false)
			String searchText
			
			) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
			return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
		
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("corp", t.getCorpKey());
		param.put("expensesDate", expensesDate);
		param.put("searchType", searchType);
		param.put("searchText", searchText);
		
		ExpensesTotalVO rtvList = expensesDataService.getExpensesTotal(param);
		Map<String,Object> rtv = new HashMap<String,Object>();
		rtv.put("result", rtvList);
		
		return createResponse(HttpStatus.OK,true,rtv);
	}
	
	@ApiOperation(value = "전체 경비 조회", notes = "전체 경비 조회한다." , response = v4.web.vo.data.ExpensesVO.class, responseContainer = "List")		
	@ApiResponses(value = {
			@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my vehicles"),
			@ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
			@ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
			@ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/expenses" , method=RequestMethod.GET)
	public ResponseEntity getExpensesList(HttpServletRequest req,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@RequestParam(value="limit" , defaultValue = "10")
			int limit
			,@RequestParam(value="offset" , defaultValue = "0")
			int offset
			,@RequestParam(value="expensesDate" , required = false)
			String expensesDate
			,@RequestParam(value="searchType" , required = false)
			String searchType
			,@RequestParam(value="searchText" , required = false)
			String searchText
			,@RequestParam(value="searchOrder" , required = false)
			String searchOrder
			,@RequestParam(value="counting" , defaultValue = "false")
			boolean counting
			) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
			return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
		
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("corp", t.getCorpKey());
		param.put("expensesDate", expensesDate);
		param.put("searchType", searchType);
		param.put("searchText", searchText);
		param.put("searchOrder", searchOrder);
		
		List<ExpensesVO> rtvList = expensesDataService.getExpensesList(param);
		
		Map<String,Object> rtv = new HashMap<String,Object>();
		rtv.put("result", rtvList);
		
		if(counting){
			int totCnt = expensesDataService.getExpensesListCnt(param);
			rtv.put("totalCnt", totCnt);
		}

		return createResponse(HttpStatus.OK,true,rtv);
	}
	
}
