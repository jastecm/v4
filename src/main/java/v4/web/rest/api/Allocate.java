package v4.web.rest.api;

import java.net.HttpURLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.util.TimeUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javassist.NotFoundException;
import v4.ex.exc.BizException;
import v4.ex.exc.CreateResponseException;
import v4.ex.exc.extents.UnauthorizedException;
import v4.ex.exc.extents.UserNotFoundException;
import v4.ex.exc.extents.VehicleNotFoundException;
import v4.web.controller.BasicWebViewController;
import v4.web.rest.service.AllocateService;
import v4.web.rest.service.TokenService;
import v4.web.rest.service.data.AccountDataService;
import v4.web.rest.service.data.AllocateDataService;
import v4.web.rest.service.data.VehicleDataService;
import v4.web.vo.CreateAllocateVO;
import v4.web.vo.TokenVO;
import v4.web.vo.data.AccountVO;
import v4.web.vo.data.AllocateVO;
import v4.web.vo.data.VehicleVO;

@Api(value="allocateApi")
@RestController
public class Allocate extends BasicWebViewController{
	
	@Autowired
	public TokenService tokenService;
	
	@Autowired
	public AccountDataService accountDataService;
	
	@Autowired
	public VehicleDataService vehicleDataService;
	
	@Autowired
	AllocateService allocateService;
	
	@Autowired
	AllocateDataService allocateDataService;
	/*
	@Autowired
	CommonService commonService;
	
	@Autowired
	CorpService corpService;
	
	@Autowired
	GroupService groupService;
	*/
	
	@ApiOperation(value = "배차정보 조회", notes = "배차정보 리스트를 조회한다"  , response = AllocateVO.class , responseContainer = "List")		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns be can allocate vehicle list"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/allocate" , method=RequestMethod.GET)
	public ResponseEntity getAccountApprovalList(HttpServletRequest req,HttpServletResponse res
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@RequestParam(value="limit" , required = false , defaultValue = "5")
			@ApiParam(value = "limit per page")
			int limit
			,@RequestParam(value="offset" , required = false , defaultValue = "0")
			@ApiParam(value = "offset")
			int offset
			,@RequestParam(value="searchType" , required = false)
			String searchType
			,@RequestParam(value="searchText" , required = false)
			String searchText
			,@RequestParam(value="counting" , defaultValue = "false")
			boolean counting
			,@RequestParam(value="allocateStatus" , defaultValue = "complate") //status는 counting 참조
			String allocateStatus
	) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 		
	{
		
		//인증공통
		TokenVO t = authentication(tokenService,key);
		
		if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
			return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
		//인증공통
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("limit", String.valueOf(limit));
		param.put("offset", String.valueOf(offset));
		param.put("searchCorpKey", t.getCorpKey());
		param.put("searchType", searchType);
		param.put("searchText", searchText);
		param.put("searchAllocateStatus", allocateStatus);
		
		param.put("searchAllocateKey", req.getParameter("searchAllocateKey"));
		
		
		param.put("searchStartDate", req.getParameter("searchStartDate"));
		param.put("searchEndDate", req.getParameter("searchEndDate"));
		param.put("searchFixedStartDate", req.getParameter("searchFixedStartDate"));
		
		
		String order = req.getParameter("searchOrder");
		param.put("searchOrder", order);
		
		List<AllocateVO> rtvList = allocateDataService.getAllocateList(param,null);
		
		Map<String,Object> rtv = new HashMap<String,Object>();
		rtv.put("result", rtvList);
		
		if(counting){
			param.put("searchAllocateStatus", "complate"); //승인 :총 배차 수 x건
			int totCnt = allocateDataService.getAllocateListCnt(param,null);
			param.put("searchAllocateStatus", "waiting"); //승인 : 배차대기
			int waitingCnt = allocateDataService.getAllocateListCnt(param,null);
			param.put("searchAllocateStatus", "processing"); //승인 : 배차중
			int processingCnt = allocateDataService.getAllocateListCnt(param,null);
			param.put("searchAllocateStatus", "end"); //승인 : 배차종료
			int endCnt = allocateDataService.getAllocateListCnt(param,null);
			
			param.put("searchAllocateStatus", "step0"); //상신
			int step0Cnt = allocateDataService.getAllocateListCnt(param,null);
			param.put("searchAllocateStatus", "step1"); //1차합의
			int step1Cnt = allocateDataService.getAllocateListCnt(param,null);			
			param.put("searchAllocateStatus", "reserve"); //반려
			int reserveCnt = allocateDataService.getAllocateListCnt(param,null);
			
			rtv.put("totalCnt", totCnt);
			rtv.put("waitingCnt",waitingCnt);
			rtv.put("processingCnt", processingCnt);
			rtv.put("endCnt", endCnt);
			rtv.put("step0Cnt", step0Cnt);
			rtv.put("step1Cnt", step1Cnt);
			rtv.put("step2Cnt", totCnt); //승인수는 총 배차수와 같음
			rtv.put("reserveCnt", reserveCnt);
		}
		
		return createResponse(HttpStatus.OK,true,rtv);
	}
	 
	
	
	@ApiOperation(value = "배차가능차량 조회", notes = "배차가능차량을 조회한다" , response = VehicleVO.class)		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns be can allocate vehicle list"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/allocate/search/{start}/{end}" , method=RequestMethod.GET)
	public ResponseEntity getAllocateVehicleSearch(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@PathVariable(value="start") 
			@ApiParam(value = "startTime(unixTimeStamp)" , required = true)
			long startDate
			,@PathVariable(value="end") 
			@ApiParam(value = "endTime(unixTimeStamp)" , required = true)
			long endDate
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key

	) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 		
	{
		
		/*******인증공통********/
		TokenVO t = authentication(tokenService,key);
		
		if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
			return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
		/*******인증공통********/
		
		
		//시간체크
		if(startDate >= endDate)
			return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "startDate.can not greater then endDate");
		
		//시간체크
		if(startDate <= new Date().getTime())
			return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "startDate.can not greater then currentTime");
				
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("searchCorpKey", t.getCorpKey());
		param.put("searchUseState", "1");
		param.put("searchAllocateStart", startDate);
		param.put("searchAllocateEnd", endDate);
		
		List<VehicleVO> rtvList = vehicleDataService.getVehicleList(param, null);
		
		
		return createResponse(HttpStatus.OK, new String[]{"result"}, rtvList);
	}
	
	
	@ApiOperation(value = "배차 등록", notes = "배차를 등록한다" , response = CreateAllocateVO.class)		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "Create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_ACCEPTED, message = "Accepted"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/allocate/{vehicleKey}" , method=RequestMethod.POST)
	public ResponseEntity createAllocate(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@PathVariable(value="vehicleKey")
			@ApiParam(value = "vehicleKey")
			String vehicleKey
			,@RequestBody(required = true)
			@Valid
			CreateAllocateVO vo,BindingResult br
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, VehicleNotFoundException, UserNotFoundException
	{
		
		
		if(br.hasErrors())
			return createValidErrorResponse(response,new String[]{"result"}, br ,null);
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
			return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
		
		//시간체크
		if(vo.getStartDate() >= vo.getEndDate())
			return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "startDate.can not greater then endDate");
		
		//시간체크
		if(vo.getStartDate() <= new Date().getTime())
			return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "startDate.can not greater then currentTime");
		
		/* 앱용 - 앱은 배차가있으면 배차를 더 못함
		AllocateVO chker = allocateService.getMyAllocate(t.getUserKey());
		
		if(chker != null)
			throw new BizException("예약중인 배차가 있습니다.");
		*/
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("searchCorpKey", t.getCorpKey());
		param.put("searchUseState", "1");
		param.put("searchAllocateStart", vo.getStartDate());
		param.put("searchAllocateEnd", vo.getEndDate());
		param.put("searchAllocateVehicle", vehicleKey);
		
		List<VehicleVO> rtvList = vehicleDataService.getVehicleList(param, null);
		if(rtvList.size() == 0)
			return createResponse(HttpStatus.CONFLICT, new String[]{"result"}, "vehicleKey.already allocate used vehicle");
		
		//빠른 체크를위해 인증전 체크
		AccountVO user = accountDataService.getAccountFromKey(t.getUserKey(), false);
		
		vo.setVehicleKey(vehicleKey);
		vo.setRegAccountKey(t.getUserKey());		
		vo.setCorpKey(t.getCorpKey());		
		/*****************기업만 배차를 한다************************/
		/*String allocateKey = null;
		allocateKey = allocateService.createAlloccate(vo).getAllocateKey();*/
		
		//배차전에 해당 유저 user가 부서가 있는지 확인한다. 부서가 없으면 결재X
		//해당 부서에 부서장이 있는지 확인한다 없으면 결재X
		/**************************그룹 및 그룹 부서장 체크 로직***************************/
		if(null == user.getGroup()){
			return createResponse(HttpStatus.FORBIDDEN, new String[]{"result"}, "accountKey.can not allocate none group user");
		}
		
		if(null == user.getGroup().getManager())
			return createResponse(HttpStatus.FORBIDDEN, new String[]{"result"}, "accountKey.can not allocate empty group manager");
		/**************************그룹 및 그룹 부서장 체크 로직***************************/
		
		String allocateKey = null;
		
		
		param.clear();
		param.put("searchCorpKey", t.getCorpKey());
		param.put("searchVehicleKey", vehicleKey);
		
		VehicleVO vehicle = vehicleDataService.getVehicle(param, null);
		
		//배차 결재를 사용인 경우
		if(user.getCorp().getApproval().getAllocateApproval().equals("1")){
			//별도 결재가가 있는지 확인한다.
			String approvalStep1 = null;
			String approvalStep2 = null;
			
			if(vehicle.getVehicleManager() == null){
				//별도 결재자가 없다.

				//1차 결재자
				vo.setApprovalState("0");
				//maxStep 2
				vo.setMaxStep("2");
				
				
				
				if(StrUtil.isNullToEmpty(user.getCorp().getApproval().getAllocateApprovalStep1()))
					approvalStep1 = vehicle.getManagerKey();
				else
					approvalStep1 = user.getCorp().getApproval().getAllocateApprovalStep1();					
				//1차 결재자
				
				//2차 결재자가 셋팅되어 있는지 확인
				if(StrUtil.isNullToEmpty(user.getCorp().getApproval().getAllocateApprovalStep2()))
					approvalStep2 = user.getGroup().getManager().getAccountKey();
				else
					approvalStep2 = user.getCorp().getApproval().getAllocateApprovalStep2();
				
				//자동승인 여부 1이면 사용 0이면 미사용
				boolean autoApprovalStep1 = user.getCorp().getApproval().getAutoAllocateApprovalStep1().equals("1");
				boolean autoApprovalStep2 = user.getCorp().getApproval().getAutoAllocateApprovalStep2().equals("1");
						
				if(autoApprovalStep1){
					//1 1과 1 0 만 0 1인경우
					if(autoApprovalStep2){
						//1 1
						vo.setApprovalStep1Date(new Date().getTime());
						vo.setApprovalStep2Date(new Date().getTime());
						vo.setApprovalState("2");
					}else{
						//1 0
						vo.setApprovalStep1Date(new Date().getTime());
						vo.setApprovalStep2Date(null);
						vo.setApprovalState("1");
					}
				}else{
					vo.setApprovalStep1Date(null);
					vo.setApprovalStep2Date(null);
				}
				
			}else{
				vo.setApprovalState("0");
				//별도 결재자가 있다
				//maxStep 1
				vo.setMaxStep("1");
				//vehicle에 있는 vehicleManager
				
				approvalStep1 = vehicle.getManagerKey();
			}
			
			vo.setApprovalStep1Account(approvalStep1);
			vo.setApprovalStep2Account(approvalStep2);
			//1차 결재자와 2차 결재자가 셋팅이 완료
			
			allocateKey = allocateService.createAlloccate(vo).getAllocateKey();
			return createResponse(HttpStatus.CREATED, new String[]{"result"}, allocateKey);
		}else{
			//결재를 사용하지 않는다.
			//바로 배차를 생성한다.
			//allocateKey = allocateService.createAlloccate(vo).getAllocateKey();
			return createResponse(HttpStatus.FORBIDDEN, new String[]{"result"}, "allocate.not used allocate system");
			
			
		}
	}
	
	//상신이 없는 배차 취소 그냥 allocate
	@ApiOperation(value = "배차 취소", notes = "배차를 취소한다.(now < start){delete}" )		
	@ApiResponses(value = {
			@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
			@ApiResponse(code = HttpURLConnection.HTTP_NO_CONTENT, message = "success but no contents"),
	        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/allocate/{allocateKey}" , method=RequestMethod.DELETE)
	public ResponseEntity deleteAllocate(HttpServletRequest request,HttpServletResponse response
		,@PathVariable(value="ver") 
		@ApiParam(value = "version of application")
		String ver				
		,@PathVariable(value="allocateKey") 
		@ApiParam(value = "target allocateKey")
		String allocateKey
		,@RequestHeader(value="key"  , required = false)
		@ApiParam(value = "key")
		String key				
	) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, ParseException, UserNotFoundException 
	{
		
		/*******인증공통********/
		TokenVO t = authentication(tokenService,key);
		
		if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
			return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
		
		//AccountVO user = accountDataService.getAccountFromKey(t.getUserKey(), false);
		/*******인증공통********/
		
		AllocateVO vo = allocateDataService.getAllocateFromKey(allocateKey);
		
		if(vo == null)
			throw new NotFoundException("allocate.not found");
		
		//등록자만 삭제가능
		if(vo.getRegAccount().getAccountKey().equals(t.getUserKey()))
			return createResponse(HttpStatus.FORBIDDEN, new String[]{"result"}, "accountKey.only regAccount can delete");
		
		//반려는 취소못함
		if(vo.getApproval().getState().equals("-1"))
			return createResponse(HttpStatus.FORBIDDEN, new String[]{"result"}, "approvalStep.already reject approval");
		
		//결재 진행중 취소불가
		if(!vo.getApproval().getState().equals("0"))
			return createResponse(HttpStatus.FORBIDDEN, new String[]{"result"}, "approvalStep.approval line already processing");
		
		allocateService.deleteAllocate(allocateKey);
		
		return createResponse(HttpStatus.OK);
			
	}
	/*
	@ApiOperation(value = "배차 반납전 체크", notes = "배차반납전 체크 - 해당배차의 마지막 주행이 차량의 최종 주행일경우 주차위치 지정가능 아니면 주차위치 지정 불가(response가 null이면 불가)" , response = LocationVO.class)		
	@ApiResponses(value = {
			@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
			@ApiResponse(code = HttpURLConnection.HTTP_NO_CONTENT, message = "success but no contents"),
	        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/allocateReleaseChk/{allocateKey}" , method=RequestMethod.GET)
	public ResponseEntity allocateReleaseChk(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@PathVariable(value="allocateKey") 
			@ApiParam(value = "allocateKey")
			String allocateKey
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, ParseException, UserNotFoundException 
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		String lang = account.getLang();
		
		if(account == null)
			throw new UserNotFoundException();
		
		AllocateVO chker = new AllocateVO();
		chker.setAccountKey(t.getUserKey());
		chker.setAllocateKey(allocateKey);
		chker.setLang(lang);
		
		chker = allocateService.getAllocate(chker);
		
		if(chker == null)
			throw new NotFoundException("배차가 없습니다.");
		
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		if(new Date().getTime() < sdf.parse(chker.getStartDate()).getTime())
			throw new BizException("배차가 시작되지 않았습니다.");

		LocationVO rtv = allocateService.allocateReleaseChk(chker);
		
		return createResponse(HttpStatus.OK, new String[]{"result"}, rtv);
			
	}
	
	@ApiOperation(value = "배차 반납", notes = "배차를 종료한다." , response = AllocateVO.class)		
	@ApiResponses(value = {
			@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
			@ApiResponse(code = HttpURLConnection.HTTP_NO_CONTENT, message = "success but no contents"),
	        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/allocateRelease/{allocateKey}" , method=RequestMethod.PUT)
	public ResponseEntity allocateReturn(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@PathVariable(value="allocateKey") 
			@ApiParam(value = "target allocateId")
			String allocateKey
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@RequestBody(required = true)
			@Valid
			LocationVO vo
			,BindingResult br
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, ParseException, UserNotFoundException 
	{
		
		if(br.hasErrors())
			return createValidErrorResponse(response,new String[]{"result"}, br ,null);
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		String lang = account.getLang();
		
		if(account == null)
			throw new UserNotFoundException();
		
		AllocateVO chker = new AllocateVO();
		chker.setAccountKey(t.getUserKey());
		chker.setAllocateKey(allocateKey);
		chker.setLang(lang);
		
		chker = allocateService.getAllocate(chker);
		
		if(chker == null)
			throw new NotFoundException("배차가 없습니다.");
		
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		if(new Date().getTime() < sdf.parse(chker.getStartDate()).getTime())
			throw new BizException("배차가 시작되지 않았습니다.");
		
		chker.setReturnFlag("1");
		
		allocateService.allocateReturn(chker.getAllocateKey(),vo.getLocation(),vo.getLocationAddr(),vo.getLocationAddrDetail());
		
		LocationVO loc = allocateService.allocateReleaseChk(chker);
		if(loc != null){
			String vehicleKey = chker.getVehicleKey();
			VehicleVO vvo = new VehicleVO();
			vvo.setCorpKey(account.getCorpKey());
			vvo.setVehicleKey(vehicleKey);	
			vvo = vehicleService.getVehicleInAllocate(vvo);
			
			vvo.setLastestLocation(vo.getLocation());
			vvo.setLastestLocationAddr(vo.getLocationAddr());
			vvo.setLastestLocationAddrDetail(vo.getLocationAddrDetail());
			vvo.setLang(lang);
			
			vehicleService.updateLastestLoc(vvo);
			
			MqttUtil util = new MqttUtil();
			String[] lonLat = vo.getLocation().split(",");
			
			if(lang.equals("ko")){
				String addr = util.getGPSAdressByGeometryEn(Double.parseDouble(lonLat[1]),Double.parseDouble(lonLat[0]));
				vvo.setLang("en");
				vvo.setLastestLocationAddr(addr);
				vehicleService.updateLastestLoc(vvo);
			}else{
				String addr = util.getGPSAdressByGeometryVerSK(Double.parseDouble(lonLat[1]),Double.parseDouble(lonLat[0]));
				vvo.setLang("ko");
				vvo.setLastestLocationAddr(addr);
				vehicleService.updateLastestLoc(vvo);
			}
		}
		
		return createResponse(HttpStatus.OK);
			
	}
	
	@ApiOperation(value = "배차 연장", notes = "배차를 연장한다.(409 is already used)" , response = AllocateVO.class)		
	@ApiResponses(value = {
			@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
			@ApiResponse(code = HttpURLConnection.HTTP_NO_CONTENT, message = "success but no contents"),
	        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/allocateExtend/{allocateKey}/{extendTime}" , method=RequestMethod.PUT)
	public ResponseEntity allocateExtend(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@PathVariable(value="allocateKey") 
			@ApiParam(value = "target allocateKey")
			String allocateKey
			,@PathVariable(value="extendTime") 
			@ApiParam(value = "extendTime(hour)")
			int extendTime
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, ParseException, UserNotFoundException 
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		String lang = account.getLang();
		
		if(account == null)
			throw new UserNotFoundException();
		
		AllocateVO chker = new AllocateVO();
		chker.setAccountKey(t.getUserKey());
		chker.setAllocateKey(allocateKey);
		chker.setLang(lang);
		
		chker = allocateService.getAllocate(chker);
		
		if(chker == null)
			throw new NotFoundException("배차가 없습니다.");
		
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		long endDate = sdf.parse(chker.getEndDate()).getTime();
		if(new Date().getTime() > endDate)
			throw new BizException("배차가 이미 종료되었습니다.");

		////
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("allocateKey", allocateKey);
		param.put("vehicleKey", chker.getVehicleKey());
		param.put("time", extendTime);
		HashMap<String,String> nextAllocate = allocateService.getNextAllocate(param);
		
		if(nextAllocate == null){
			//바로 업데이트 한다
			allocateService.updateAllocateExtend(param);
		}else{
			//다음배차가 있다 시간 계산을 해야 된다.
			long start = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(nextAllocate.get("startDate")).getTime();
			long end = endDate;
			int hour = (int) (((start - end) / 1000) /3600);
			
			//내가 연장할려는 시간이 연장할수 있는 시간보다 작거나 같으면 가능
			if(extendTime <= hour){
				//업데이트
				allocateService.updateAllocateExtend(param);
			}else{
				return createResponse(HttpStatus.INTERNAL_SERVER_ERROR,new String[]{"result","temp"}, "요청한 연장을 처리할수 없습니다.",nextAllocate);
			}
		}
		
		return createResponse(HttpStatus.OK);
			
	}
	@ApiOperation(value = "배차 결제 승인", notes = "배차 결제를 승인한다" , response = String.class)
	@RequestMapping(value="/api/{ver}/allocate/approvalAccept/{allocateKey}/{step}", method=RequestMethod.PUT)
	public ResponseEntity approvalAccept(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@PathVariable(value="allocateKey") 
			@ApiParam(value = "target allocateKey")
			String allocateKey
			,@PathVariable(value="step") 
			@ApiParam(value = "step")
			String step
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, VehicleNotFoundException 
	{
		
		try {
			TokenVO t = authentication(tokenService,key);
			
			if(t == null) 
				throw new UnauthorizedException();
			
			AccountVO account = accountService.getAccount(t.getUserKey());
			String lang = account.getLang();
			
			if(account == null)
				throw new UserNotFoundException();
			
			
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("allocateKey", allocateKey);
			param.put("corpKey", account.getCorpKey());
			
			AllocateVO vo = allocateService.getAllocateApproval(param);
			
			CorpVO corpVo = corpService.getCorp(account.getCorpKey(), null);
			
			
			//maxstep이 step이랑 같으면 더이상 결재진행을 할수가 없다.
			if(vo.getMaxStep().equals(step)){
				return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "이미 결재가 완료 되었습니다.");
			}
			
			//1차 승인
			if(step.equals("1")){
				//상신인 경우
				if(vo.getApprovalState().equals("0")){
					if(vo.getApprovalStep1Account().equals(account.getAccountKey())){
						//후결인경우
						 * 
						 * 
						if(vo.getAffterAllocate().equals("1")){
							//2차 승인이 필요하다.
							String nextAccount = vo.getApprovalStep2Account();
							String nextStep = "2";
								
							allocateService.updateApprovalState(allocateKey,account.getAccountKey(),step);
							
						}else{
							
							allocateService.updateApprovalState(allocateKey,account.getAccountKey(),step);
							
						}*
						*
						//
						//근데 2차가 자동승인인 경우 자동승인 처리한다.
						allocateService.updateApprovalState(allocateKey,account.getAccountKey(),step);
						if(corpVo.getAutoAllocateApprovalStep2().equals("1")){
							allocateService.updateApprovalState(allocateKey,account.getAccountKey(),step + 1);
						}
						
						
					}else{
						return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "승인권한이 없습니다.");
					}
				}else{
					return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "승인 가능한 단계가 아닙니다.");
				}
			}else if(step.equals("2")){//2차 승인인경우
				if(vo.getApprovalState().equals("1")){
					if(vo.getApprovalStep2Account().equals(account.getAccountKey())){

						//결재완료
						allocateService.updateApprovalState(allocateKey,account.getAccountKey(),step);
						
						//후결인경우
						if(vo.getAffterAllocate().equals("1")) 
							allocateService.updateInstantAllocateApprovalComplate(allocateKey);
						
					}else{
						return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "승인권한이 없습니다.");
					}
				}else{
					return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "승인 가능한 단계가 아닙니다.");
				}
			}else if(step.equals("-1")){//반려
				//상신0 1차 1일때
				if(vo.getApprovalState().equals("0") || vo.getApprovalState().equals("1")){
					//만약에 상신처리인경우
					if(vo.getApprovalState().equals("0")){					

						if(vo.getApprovalStep1Account().equals(account.getAccountKey()) || vo.getApprovalStep2Account().equals(account.getAccountKey()) || vo.getAccountKey().equals(account.getAccountKey())){
							//반려 처리
							allocateService.updateApprovalReject(allocateKey,account.getAccountKey(),step);
							if(vo.getAffterAllocate().equals("1")) {
								allocateService.updateInstantAllocateApprovalReject(allocateKey);
								allocateService.updateApprovalRejectAllocateKeyUpdate(allocateKey);
							}else{
								//배차삭제
								//trip셀렉트 해서 미배차생성 trip에 미배차 키 업데이트
								//allocate
								//driving.xml
								allocateService.returnAllocate(allocateKey);
							}
						}else{
							return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "반려권한이 없습니다.");
						}
						
						
					}else if(vo.getApprovalState().equals("1")){
						
						
						if(vo.getApprovalStep2Account().equals(account.getAccountKey())){
							allocateService.updateApprovalReject(allocateKey,account.getAccountKey(),step);
							if(vo.getAffterAllocate().equals("1")) {
								allocateService.updateInstantAllocateApprovalReject(allocateKey);
								allocateService.updateApprovalRejectAllocateKeyUpdate(allocateKey);
							}
						}else{
							return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "반려권한이 없거나, 이미 승인하였습니다.");
						}
						
						
					}else{
						//dead code
					}
				}else{
					return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "반려 가능한 단계가 아닙니다.");
				}
			}else{
				return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "잘못된 접근입니다.");
			}
				
			Map<String,Object> rtv = new HashMap<String,Object>();
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			rtv.put("date", sdf.format(new Date().getTime()));
			rtv.put("groupNm", account.getGroupNm());
			rtv.put("name", account.getName());
			rtv.put("position", account.getCorpPosition());
			
			return createResponse(HttpStatus.OK, new String[]{"result"}, rtv);
		} catch (Exception e) {
			e.printStackTrace();
			return createResponse(HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		
			
	}
	*/
}
