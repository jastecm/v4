package v4.web.rest.api;

import java.io.File;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.common.CustomCollectionValidator;
import com.common.Globals;
import com.common.MailUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.util.TimeUtil;
import org.jastecm.string.StrUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import v4.ex.exc.BizException;
import v4.ex.exc.CreateResponseException;
import v4.ex.exc.NotFoundException;
import v4.ex.exc.extents.UnauthorizedException;
import v4.ex.exc.extents.UserNotFoundException;
import v4.web.controller.BasicWebViewController;
import v4.web.rest.service.AccountService;
import v4.web.rest.service.CorpService;
import v4.web.rest.service.TokenService;
import v4.web.rest.service.data.AccountDataService;
import v4.web.rest.service.data.CorpDataService;
import v4.web.vo.AccountVO;
import v4.web.vo.CreateCorpVO;
import v4.web.vo.CreateInviteVO;
import v4.web.vo.CreatePartnerVO;
import v4.web.vo.CreatePersonVO;
import v4.web.vo.CreateRentVO;
import v4.web.vo.CreateUserVO;
import v4.web.vo.InviteStep1VO;
import v4.web.vo.LoginVO;
import v4.web.vo.RegistStep1VO;
import v4.web.vo.TokenVO;
import v4.web.vo.data.CorpVO;






/*
 * @Class name : LoginController
 * @Description : 
 * @Modification Information
 * 
 * @author 
 * @since 
 * @version 
 * @see
 *
 */
@Api(value="accountApi")
@RestController
public class Account extends BasicWebViewController{
		
		@Autowired
		public AccountService accountService;
		
		@Autowired
		public AccountDataService accountDataService;

		@Autowired
		public CorpService corpService;
		
		@Autowired
		public CorpDataService corpDataService;
		
		@Autowired
		public TokenService tokenService;
		
		@Autowired
		CustomCollectionValidator customCollectionValidator;
		
		@ApiOperation(value = "아이디 중복 체크", notes = "아이디 중복을 체크 한다." , response = String.class )		
		@ApiResponses(value = {
				@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_CONFLICT, message = "conflict")
		})
		@RequestMapping(value="/api/{ver}/account/conflictChk" , method=RequestMethod.GET)
		public ResponseEntity accountIdDuplicateCheck(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver				
				,@RequestParam(value="accountId" , required = true)
				@ApiParam(value = "accountId")
				String accountId
		)  throws CreateResponseException, UnauthorizedException, NotFoundException, BizException {
			
			
			v4.web.vo.data.AccountVO chkVo = accountDataService.getAccountFromId(accountId);
			
			if(chkVo != null)
				return createResponse(HttpStatus.CONFLICT,new String[]{"result"},"accountId.duplicate");
			
			
			return createResponse(HttpStatus.OK);
		}
		
		@ApiOperation(value = "계정 등록(개인)", notes = "계정 등록(개인)" , response = String.class , produces = "application/json;charset=utf-8")		
		@ApiResponses(value = {
				@ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
		        @ApiResponse(code = HttpURLConnection.HTTP_CONFLICT, message = "Conflict"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/account/personal" , method=RequestMethod.POST)
		public ResponseEntity createUserPersonal(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver				
				,@RequestBody(required = true)
				@Valid
				CreatePersonVO vo
				,BindingResult br
		)  throws CreateResponseException, BizException, JsonProcessingException  {
			
			if(br.hasErrors())
				return createValidErrorResponse(response,new String[]{"result"}, br ,null);
									
			if(accountDataService.getAccountFromId(vo.getAccountId()) != null)
				return createResponse(HttpStatus.CONFLICT, new String[]{"result"}, "accountId.duplicate");
			
			if(corpService.createCorp(vo)){				
				if(accountService.createUser(vo,1,0)){
					return createResponse(HttpStatus.CREATED);
				}else{
					return createResponse(HttpStatus.INTERNAL_SERVER_ERROR, new String[]{"result"}, "create fail");
				}
			}else{
				return createResponse(HttpStatus.INTERNAL_SERVER_ERROR, new String[]{"result"}, "create fail");
			}
			
		}
		
		
		@ApiOperation(value = "계정 등록(법인)", notes = "계정 등록(법인)" , response = String.class , produces = "application/json;charset=utf-8")		
		@ApiResponses(value = {
				@ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
		        @ApiResponse(code = HttpURLConnection.HTTP_CONFLICT, message = "Conflict"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/account/corp" , method=RequestMethod.POST)
		public ResponseEntity createUserCorp(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver				
				,@RequestBody(required = true)
				@Valid
				CreateCorpVO vo
				,BindingResult br
		)  throws CreateResponseException, BizException, JsonProcessingException  {
			
			if(br.hasErrors())
				return createValidErrorResponse(response,new String[]{"result"}, br ,null);
									
			if(accountDataService.getAccountFromId(vo.getAccountId()) != null)
				return createResponse(HttpStatus.CONFLICT, new String[]{"result"}, "accountId.duplicate");
			
			if(corpService.createCorp(vo)){
				if(accountService.createCorpUser(vo,1,0)){
					return createResponse(HttpStatus.CREATED);
				}else{
					return createResponse(HttpStatus.INTERNAL_SERVER_ERROR, new String[]{"result"}, "create fail");
				}
			}else{
				return createResponse(HttpStatus.INTERNAL_SERVER_ERROR, new String[]{"result"}, "create fail");
			}
			
		}
		
		@ApiOperation(value = "계정 등록(렌트)", notes = "계정 등록(렌트)" , response = String.class , produces = "application/json;charset=utf-8")		
		@ApiResponses(value = {
				@ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
		        @ApiResponse(code = HttpURLConnection.HTTP_CONFLICT, message = "Conflict"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/account/rent" , method=RequestMethod.POST)
		public ResponseEntity createUserRent(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver				
				,@RequestBody(required = true)
				@Valid
				CreateRentVO vo
				,BindingResult br
		)  throws CreateResponseException, BizException, JsonProcessingException  {
			
			if(br.hasErrors())
				return createValidErrorResponse(response,new String[]{"result"}, br ,null);
									
			if(accountDataService.getAccountFromId(vo.getAccountId()) != null)
				return createResponse(HttpStatus.CONFLICT, new String[]{"result"}, "accountId.duplicate");
			
			if(corpService.createCorp(vo)){				
				if(accountService.createRentUser(vo,1,0)){
					return createResponse(HttpStatus.CREATED);
				}else{
					return createResponse(HttpStatus.INTERNAL_SERVER_ERROR, new String[]{"result"}, "create fail");
				}
			}else{
				return createResponse(HttpStatus.INTERNAL_SERVER_ERROR, new String[]{"result"}, "create fail");
			}
			
		}
		
		@ApiOperation(value = "계정 등록(파트너)", notes = "계정 등록(파트너)" , response = String.class , produces = "application/json;charset=utf-8")		
		@ApiResponses(value = {
				@ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
		        @ApiResponse(code = HttpURLConnection.HTTP_CONFLICT, message = "Conflict"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/account/partner" , method=RequestMethod.POST)
		public ResponseEntity createUserPartner(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver				
				,@RequestBody(required = true)
				@Valid
				CreatePartnerVO vo
				,BindingResult br
		)  throws CreateResponseException, BizException, JsonProcessingException  {
			
			if(br.hasErrors())
				return createValidErrorResponse(response,new String[]{"result"}, br ,null);
									
			if(accountDataService.getAccountFromId(vo.getAccountId()) != null)
				return createResponse(HttpStatus.CONFLICT, new String[]{"result"}, "accountId.duplicate");
			
			if(corpService.createPartner(vo)){				
				if(accountService.createPartnerUser(vo,1,0)){
					return createResponse(HttpStatus.CREATED);
				}else{
					return createResponse(HttpStatus.INTERNAL_SERVER_ERROR, new String[]{"result"}, "create fail");
				}
			}else{
				return createResponse(HttpStatus.INTERNAL_SERVER_ERROR, new String[]{"result"}, "create fail");
			}
		}
		
		@ApiOperation(value = "초대 회원가입 메일발송", notes = "초대 회원가입 step1 - 인증 email발송" , response = String.class )		
		@ApiResponses(value = {
				@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/invite" , method=RequestMethod.POST)
		public ResponseEntity invite(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver	
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@RequestBody(required = true)
				@Valid
				InviteStep1VO vo
				,BindingResult br
		)  throws CreateResponseException, UnauthorizedException, NotFoundException, BizException {
			
			if(br.hasErrors())
				return createValidErrorResponse(response,new String[]{"result"}, br ,null);
			
			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			v4.web.vo.data.AccountVO account = accountDataService.getAccountFromKey(t.getUserKey(),true);
			
			v4.web.vo.data.AccountVO chkVo = accountDataService.getAccountFromId(vo.getAccountId());
			
			if(chkVo != null)
				return createResponse(HttpStatus.CONFLICT,new String[]{"result"},"accountId.duplicate");
			
			String registToken = jwt.generateAccountInviteToken(vo.getAccountId(), account.getCorp().getCorpKey());
			System.out.println(registToken);

			//email send
			String url = Globals.VDAS_URL_PREFIX+"/index?cmd=/inviteRegist/step1&key="+registToken;
			String mailBody = "<a href='"+url+"'>이메일 인증완료</a>";
			
			//TODO if (Globals.WIN) { 
			if (true) {
				//url = "http://localhost:8080/vdasPro"+"/inviteRegist/step1?key="+registToken;
				mailBody = "<a href='"+url+"'>이메일 인증완료</a>";
				MailUtil mu = new MailUtil();
				String from = "upjjw2682@gmail.com";
				String fromName = "정종웅";
				String[] to = { vo.getAccountId() };
				String[] cc = {};
				String[] bcc = {};
				String subject = "회원가입 인증메일";
				String content = mailBody;
				File[] attachFiles = {};
				try {
					mu.sendSmtpMail(from, fromName, to, cc, bcc, subject,content, attachFiles);
				} catch (Exception e) {
					e.printStackTrace();
					return createResponse(HttpStatus.INTERNAL_SERVER_ERROR,new String[]{"result"},"mailSend.fail");
				}
			} else {
				/*
				MailUtilServer mu = new MailUtilServer();
				String from = Globals.MAIL_SENDER;
				String fromName = "뷰카";
				String[] to = { vo.getAccountId() };
				String[] cc = {};
				String[] bcc = {};
				String subject = "회원가입 인증메일";
				String content = mailBody;
				File[] attachFiles = {};
				try {
					mu.sendSmtpMail(from, fromName, to, cc, bcc, subject,content, attachFiles);
				} catch (Exception e) {
					e.printStackTrace();
					return createResponse(HttpStatus.INTERNAL_SERVER_ERROR,new String[]{"result"},"mailSend.fail");
				}
				*/
			}
			
			return createResponse(HttpStatus.CREATED);
			
				
		}
		
		@ApiOperation(value = "계정 등록(초대가입)", notes = "계정 등록(초대가입)" , response = String.class , produces = "application/json;charset=utf-8")		
		@ApiResponses(value = {
				@ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
		        @ApiResponse(code = HttpURLConnection.HTTP_CONFLICT, message = "Conflict"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/account/invite" , method=RequestMethod.POST)
		public ResponseEntity createUserInvite(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver				
				,@RequestBody(required = true)
				@Valid
				CreateInviteVO vo
				,BindingResult br
		)  throws CreateResponseException, BizException, JsonProcessingException  {
			
			if(br.hasErrors())
				return createValidErrorResponse(response,new String[]{"result"}, br ,null);
									
			if(accountDataService.getAccountFromId(vo.getAccountId()) != null)
				return createResponse(HttpStatus.CONFLICT, new String[]{"result"}, "accountId.duplicate");
			
			CorpVO corp = corpDataService.getCorpFromKey(vo.getCorpKey());
			
			if(corp==null)
				return createResponse(HttpStatus.NOT_FOUND, new String[]{"result"}, "corpKey.notFound");
			
			if(accountService.createUser(vo,2,0)){
				return createResponse(HttpStatus.CREATED);
			}else{
				return createResponse(HttpStatus.INTERNAL_SERVER_ERROR, new String[]{"result"}, "create fail");
			}
			
			
		}
		
		@ApiOperation(value = "계정 등록 (사용자 개별등록)", notes = "계정 등록(사용자 개별등록)" , response = String.class , produces = "application/json;charset=utf-8")		
		@ApiResponses(value = {
				@ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
		        @ApiResponse(code = HttpURLConnection.HTTP_CONFLICT, message = "Conflict"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/account/one" , method=RequestMethod.POST)
		public ResponseEntity createUserOneReg(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@RequestBody(required = true)
				@Valid
				CreateUserVO vo
				,BindingResult br
		)  throws CreateResponseException, BizException, JsonProcessingException, UserNotFoundException  {
			
			if(br.hasErrors())
				return createValidErrorResponse(response,new String[]{"result"}, br ,null);
			
			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			//여기까지 거의공통//
			
			if(accountDataService.getAccountFromId(vo.getAccountId()) != null)
				return createResponse(HttpStatus.CONFLICT, new String[]{"result"}, "accountId.duplicate");
			
			vo.setCorpKey(t.getCorpKey()); 
			
			if(accountService.createUser(vo,2,0)){
				return createResponse(HttpStatus.CREATED);
			}else{
				return createResponse(HttpStatus.INTERNAL_SERVER_ERROR, new String[]{"result"}, "create fail");
			}
			
			
		}
		
		@ApiOperation(value = "계정 등록 (사용자 대량등록)", notes = "계정 등록(사용자 개별등록)" , response = String.class , produces = "application/json;charset=utf-8")		
		@ApiResponses(value = {
				@ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
		        @ApiResponse(code = HttpURLConnection.HTTP_CONFLICT, message = "Conflict"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/account/array" , method=RequestMethod.POST)
		public ResponseEntity createUserManyReg(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver	
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@RequestBody(required = true)
				@Valid
				List<CreateUserVO> vo
				,BindingResult br
		)  throws CreateResponseException, BizException, JsonProcessingException, UserNotFoundException  {
			
			//컬렉션이 경우 사용
			customCollectionValidator.validate(vo, br);

	        if (br.hasErrors()) {
	        	return createValidErrorResponse(response,new String[]{"result"}, br ,null);
	        }
	        
	        TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());

			for(CreateUserVO objVo : vo)
				objVo.setCorpKey(t.getCorpKey());
	        
	        try {
	        	accountService.createUser(vo, 2, 0);
			} catch (Exception e) {
				return createResponse(HttpStatus.CONFLICT, new String[]{"result"}, "accountId.duplicate");
			}
	        
			
			return createResponse(HttpStatus.CREATED);
		}
		
		@ApiOperation(value = "로그인", notes = "이미 발급되어있는 토큰을 가져온다." , response = String.class)		
		@ApiResponses(value = {
				@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "login.notActive"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "login.fail , login.tokenExpired"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/token" , method=RequestMethod.GET)
		public ResponseEntity getToken(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver				
				,@RequestHeader
				@ApiParam(value = "user ID")
				String id
				,@RequestHeader
				@ApiParam(value = "user PW")
				String pw
		)  throws CreateResponseException, UnauthorizedException, BizException {
			
			LoginVO vo = new LoginVO();
			vo.setId(id);
			vo.setPw(pw);
			
			v4.web.vo.data.AccountVO chkVo = accountDataService.getAccountFromIdPw(vo);
			
			if(chkVo == null)
				return createResponse(HttpStatus.NOT_FOUND , new String[]{"result"} , "login.fail");
			
			List<Long> arrExpired = tokenService.getUserToken(chkVo.getAccountKey());
			
			if(arrExpired == null || arrExpired.size() == 0)
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , "token.notActive");
			
			long ex = arrExpired.get(0);
			
			if(ex - TimeUtil.now() < 0)
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , "token.tokenExpired");
			
			
			TokenVO t = new TokenVO(ex,chkVo.getAccountKey(),chkVo.getCorp().getCorpKey(),chkVo.getCorp().getCorpType(),chkVo.getCorp().getServiceGrade());
			
			String token = jwt.generateToken(t);
			
			return createResponse(HttpStatus.OK, new String[]{"result"}, token);
				
		}
		
		@ApiOperation(value = "로그인", notes = "새로운 토큰을 생성한다." , response = String.class)		
		@ApiResponses(value = {
				@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/token" , method=RequestMethod.POST)
		public ResponseEntity createToken(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver				
				,@RequestBody(required = true)
				@Valid
				LoginVO vo
				,BindingResult br
		)  throws CreateResponseException, UnauthorizedException, BizException {
			
			if (br.hasErrors()) {
	        	return createValidErrorResponse(response,new String[]{"result"}, br ,null);
	        }
			
			v4.web.vo.data.AccountVO chkVo = accountDataService.getAccountFromIdPw(vo);
			
			if(chkVo == null)
				return createResponse(HttpStatus.NOT_FOUND , new String[]{"result"} , "login.fail");
			
			
			
			
			TokenVO t = new TokenVO(TimeUtil.now()+vo.getExpire(),chkVo.getAccountKey(),chkVo.getCorp().getCorpKey(),chkVo.getCorp().getCorpType(),chkVo.getCorp().getServiceGrade());
			
			String token = jwt.generateToken(t);
			
			tokenService.clearUserToken(chkVo.getAccountKey());
			tokenService.addUserToken(t);
			
			return createResponse(HttpStatus.CREATED, new String[]{"result"}, token);
				
		}
		@ApiOperation(value = "로그인", notes = "이미 발급되어있는 토큰을 가져온다.(phone login)" , response = String.class)		
		@ApiResponses(value = {
				@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "login.notActive"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "login.fail , login.tokenExpired"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value={"/api/{ver}/token/CP","/api/{ver}/token/MP"} , method=RequestMethod.GET)
		public ResponseEntity getTokenFromPhone(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver				
				,@RequestHeader
				@ApiParam(value = "certification phoneNumber")
				String id
				,@RequestHeader
				@ApiParam(value = "userPw")
				String pw
		)  throws CreateResponseException, UnauthorizedException, BizException {
			
			id.replaceAll("-", "");
			LoginVO vo = new LoginVO();
			vo.setId(id);
			vo.setPw(pw);
			
			v4.web.vo.data.AccountVO chkVo = accountDataService.getAccountFromPhonePw(vo);
			
			if(chkVo == null)
				return createResponse(HttpStatus.NOT_FOUND , new String[]{"result"} , "login.fail");
			
			List<Long> arrExpired = tokenService.getUserToken(chkVo.getAccountKey());
			
			if(arrExpired == null || arrExpired.size() == 0)
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , "token.notActive");
			
			long ex = arrExpired.get(0);
			
			if(ex - TimeUtil.now() < 0)
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , "token.tokenExpired");
			
			TokenVO t = new TokenVO(ex,chkVo.getAccountKey(),chkVo.getCorp().getCorpKey(),chkVo.getCorp().getCorpType(),chkVo.getCorp().getServiceGrade());
			
			String token = jwt.generateToken(t);
			
			return createResponse(HttpStatus.OK, new String[]{"result"}, token);
			
				
		}
		
		@ApiOperation(value = "로그인", notes = "새로운 토큰을 생성한다.(phone login)" , response = String.class)		
		@ApiResponses(value = {
				@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value={"/api/{ver}/token/CP","/api/{ver}/token/MP"} , method=RequestMethod.POST)
		public ResponseEntity createTokenPhone(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver				
				,@RequestBody(required = true)
				@Valid
				LoginVO vo
				,BindingResult br
		)  throws CreateResponseException, UnauthorizedException, BizException {
			
			if (br.hasErrors()) {
	        	return createValidErrorResponse(response,new String[]{"result"}, br ,null);
	        }
			
			vo.setId(vo.getId().replaceAll("-", ""));
			v4.web.vo.data.AccountVO chkVo = accountDataService.getAccountFromPhonePw(vo);
			
			if(chkVo == null)
				return createResponse(HttpStatus.NOT_FOUND , new String[]{"result"} , "login.fail");
			
			
			TokenVO t = new TokenVO(TimeUtil.now()+vo.getExpire(),chkVo.getAccountKey(),chkVo.getCorp().getCorpKey(),chkVo.getCorp().getCorpType(),chkVo.getCorp().getServiceGrade());
			
			String token = jwt.generateToken(t);
			
			tokenService.clearUserToken(chkVo.getAccountKey());
			tokenService.addUserToken(t);
			
			return createResponse(HttpStatus.CREATED, new String[]{"result"}, token);
				
		}
		
		@ApiOperation(value = "회원가입(step1)", notes = "회원가입 step1 - 인증 email발송" , response = String.class )		
		@ApiResponses(value = {
				@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/regist" , method=RequestMethod.POST)
		public ResponseEntity regist(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver				
				,@RequestBody(required = true)
				@Valid
				RegistStep1VO vo
				,BindingResult br
		)  throws CreateResponseException, UnauthorizedException, NotFoundException, BizException {
			
			if(br.hasErrors())
				return createValidErrorResponse(response,new String[]{"result"}, br ,null);
			
			if(accountDataService.getAccountFromId(vo.getAccountId()) != null)
				return createResponse(HttpStatus.CONFLICT, new String[]{"result"}, "accountId.duplicate");
			
			
			String registToken = jwt.generateEmailAuthToken(vo.getAccountId(), vo.getAccountPw());
			System.out.println(registToken);

			//email send
			String url = Globals.VDAS_URL_PREFIX+"/index?cmd=/regist/step1&key="+registToken;
			String mailBody = "<a href='"+url+"'>이메일 인증완료</a>";
			
			//if (Globals.WIN) {
			if (true) {
				mailBody = "<a href='"+url+"'>이메일 인증완료</a>";
				MailUtil mu = new MailUtil();
				String from = "upjjw2682@gmail.com";
				String fromName = "정종웅";
				String[] to = { vo.getAccountId() };
				String[] cc = {};
				String[] bcc = {};
				String subject = "회원가입 인증메일";
				String content = mailBody;
				File[] attachFiles = {};
				try {
					mu.sendSmtpMail(from, fromName, to, cc, bcc, subject,content, attachFiles);
				} catch (Exception e) {
					e.printStackTrace();
					return createResponse(HttpStatus.INTERNAL_SERVER_ERROR,new String[]{"result"},"mailSend.fail");
				}
			} else {
				/*
				MailUtilServer mu = new MailUtilServer();
				String from = Globals.MAIL_SENDER;
				String fromName = "뷰카";
				String[] to = { vo.getAccountId() };
				String[] cc = {};
				String[] bcc = {};
				String subject = "회원가입 인증메일";
				String content = mailBody;
				File[] attachFiles = {};
				try {
					mu.sendSmtpMail(from, fromName, to, cc, bcc, subject,content, attachFiles);
				} catch (Exception e) {
					e.printStackTrace();
					return createResponse(HttpStatus.INTERNAL_SERVER_ERROR,new String[]{"result"},"mailSend.fail");
				}
				*/
			}
			
			return createResponse(HttpStatus.CREATED);
			
				
		}
		
		@ApiOperation(value = "유저 리스트를 가져온다", notes = "" , response = AccountVO.class , responseContainer = "List")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my account"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/account" , method=RequestMethod.GET)
		public ResponseEntity getUserList(HttpServletRequest req,HttpServletResponse res
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@RequestParam(value="limit" , defaultValue = "10")
				int limit
				,@RequestParam(value="offset" , defaultValue = "0")
				int offset
				,@RequestParam(value="searchType" , defaultValue = "")
				String searchType
				,@RequestParam(value="searchText" , defaultValue = "")
				String searchText
				,@RequestParam(value="searchGroupKey" , defaultValue = "")
				String searchGroupKey
				,@RequestParam(value="counting" , defaultValue = "false")
				boolean counting
		) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException {

			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());

			Map<String,Object> param = new HashMap<String,Object>();
			param.put("searchCorpKey", t.getCorpKey());
			param.put("limit", limit);
			param.put("offset", offset);
			param.put("searchType", searchType);
			param.put("searchText", searchText);
			param.put("searchGroupKey", searchGroupKey);
			
			
			param.put("searchDrivingState", req.getParameter("searchDrivingState"));
			param.put("searchStartDate", req.getParameter("searchStartDate"));
			param.put("searchEndDate", req.getParameter("searchEndDate"));
			
			String order = req.getParameter("searchOrder");
			param.put("searchOrder", order);
			
			
			List<v4.web.vo.data.AccountVO> rtvList = accountDataService.getAccountList(param,null);
			
			Map<String,Object> rtv = new HashMap<String,Object>();
			rtv.put("result", rtvList);

			if(counting){
				int totCnt = accountDataService.getAccountListCnt(param,null);
				
				param.put("searchDrivingState", "driving");
				int drivingCnt = accountDataService.getAccountListCnt(param,null);
				
				param.put("searchDrivingState", "parking");
				int parkingCnt = accountDataService.getAccountListCnt(param,null);
				
				param.put("searchDrivingState", "unused");
				int unusedCnt = accountDataService.getAccountListCnt(param,null);
				param.remove("searchDrivingState");
				
				rtv.put("totalCnt", totCnt);
				rtv.put("drivingCnt", drivingCnt);
				rtv.put("parkingCnt", parkingCnt);
				rtv.put("unusedCnt", unusedCnt);
			}
			
			return createResponse(HttpStatus.OK,rtv);
		}
		
		@ApiOperation(value = "계정 정보를 삭제", notes = "현제 계정 정보를 삭제한다.")		
		@ApiResponses(value = {
				@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/account/{accountKey}" , method=RequestMethod.DELETE)
		public ResponseEntity deleteUser(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver								
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="accountKey") 
				@ApiParam(value = "target accountKey arr - delimeter ',' ")
				String accountKey
				) throws CreateResponseException, UnauthorizedException, BizException {
			
			try {
				
				TokenVO t = authentication(tokenService,key);
				
				if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
					return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
				
				accountService.accountUseStop(accountKey);
				
				return createResponse(HttpStatus.OK);
				
			} catch (Exception e) {
				e.printStackTrace();
				return createResponse(HttpStatus.INTERNAL_SERVER_ERROR, new String[]{"result"}, "internal server error");
			}
		}
		
/*
		@ApiOperation(value = "Force 비밀번호 변경", notes = "Force 비밀번호 변경" , response = String.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my account"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/forceChangePassword" , method=RequestMethod.PUT)
		public ResponseEntity forceChangePassword(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = true)
				@ApiParam(value = "key")
				String key
				,@RequestHeader(required = true)
				@ApiParam(value = "pw")
				String pw					
		) throws CreateResponseException, UnauthorizedException, BizException ,DefaultInitCheckException{
	
			TokenVO t = authenticationChangePw(key);
			
			if(t == null) 
				throw new UnauthorizedException();
			
			Pattern p = Pattern.compile("^.{8,20}$");
			
			if(!p.matcher(pw).find()){
				return createResponse(HttpStatus.BAD_REQUEST,new String[]{"result"},"invalid password pattern : ^.{8,20}$");
			}
			
			restAccountService.changePassword(t.getUserKey(),pw);
			
			return createResponse(HttpStatus.NO_CONTENT);
			
		}
		*/
		
		/*
		@ApiOperation(value = "비밀번호 변경", notes = "비밀번호 변경" , response = String.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my account"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/changePassword" , method=RequestMethod.PUT)
		public ResponseEntity changePassword(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = true)
				@ApiParam(value = "key")
				String key
				,@RequestHeader(value="oldPw"  , required = true)
				@ApiParam(value = "oldPw")
				String oldPw
				,@RequestHeader(value="newPw"  , required = true)
				@ApiParam(value = "newPw")
				String newPw
		) throws CreateResponseException, UnauthorizedException, BizException {
	
			TokenVO t = authentication(restTokenService,key);
			
			if(t == null) 
				throw new UnauthorizedException();
			
			int chk = restAccountService.changePassword(t.getUserKey(),oldPw,newPw);
			if(chk == 0)
				return createResponse(HttpStatus.FORBIDDEN);
			
			return createResponse(HttpStatus.OK);
				
		}
		*/
		
		/*
		@ApiOperation(value = "비밀번호 찾기", notes = "비밀번호 찾기" , response = String.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my account"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/findPassword" , method=RequestMethod.GET)
		public ResponseEntity findPassword(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="email"  , required = false)
				@ApiParam(value = "email")
				String email				
				,@RequestParam(value="name"  , required = false)
				@ApiParam(value = "name")
				String name
				,@RequestParam(value="web"  , required = false)
				@ApiParam(value = "web")
				String web
		) throws CreateResponseException, UnauthorizedException, BizException, UnsupportedEncodingException {
	
			String userKey = restAccountService.findAccountOne(name,email);
			
			if(StrUtil.isNullToEmpty(userKey)){
				return createResponse(HttpStatus.NOT_FOUND);
			}else{
				
				UserVO uvo = restAccountService.getAccount(userKey);
				
				long expired = TimeUtil.now()+(180*60);
				TokenVO tvo = new TokenVO(expired,userKey);
				String token = jwt.generateChangePwToken(tvo);
				
				String protocol = "viewcar://app/changePassword?key="+token;
				if(!StrUtil.isNullToEmpty(web)) protocol = "https://vdaspro.viewcar.co.kr/vdasPro/account/changePass.do?key="+token;
				//if(!StrUtil.isNullToEmpty(web)) protocol = "http://localhost:8080/vdasPro/account/findAccountPw3.do?key="+token;
				
				restAccountService.sendChangePasswordMail(uvo.getAuthMail(),protocol);
				
				
				
				return createResponse(HttpStatus.OK,new String[]{"result"},uvo.getAuthMail());
			}
			
			
				
		}
		*/
		
		/*
		@ApiOperation(value = "비밀번호 찾기", notes = "비밀번호 찾기" , response = String.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my account"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/findPasswordToId" , method=RequestMethod.GET)
		public ResponseEntity findPasswordToId(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="email"  , required = false)
				@ApiParam(value = "email")
				String email				
				,@RequestParam(value="name"  , required = false)
				@ApiParam(value = "name")
				String name
				,@RequestParam(value="web"  , required = false)
				@ApiParam(value = "web")
				String web
		) throws CreateResponseException, UnauthorizedException, BizException, UnsupportedEncodingException {
	
			String userKey = restAccountService.findAccountOne(name,email);
			
			if(StrUtil.isNullToEmpty(userKey)){
				return createResponse(HttpStatus.NOT_FOUND);
			}else{
				
				UserVO uvo = restAccountService.getAccount(userKey);
				
				long expired = TimeUtil.now()+(180*60);
				TokenVO tvo = new TokenVO(expired,userKey);
				String token = jwt.generateChangePwToken(tvo);
				
				String protocol = "viewcar://app/changePassword?key="+token;
				if(!StrUtil.isNullToEmpty(web)) protocol = "https://vdaspro.viewcar.co.kr/vdasPro/account/changePass.do?key="+token;
				//if(!StrUtil.isNullToEmpty(web)) protocol = "http://localhost:8080/vdasPro/account/findAccountPw3.do?key="+token;
				
				restAccountService.sendChangePasswordMail(email,protocol);
				
				
				
				return createResponse(HttpStatus.OK,new String[]{"result"},email);
			}
			
			
				
		}
		*/
		
		/*
		@ApiOperation(value = "아이디 찾기", notes = "아이디 찾기" , response = String.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my account"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/findAccount" , method=RequestMethod.GET)
		public ResponseEntity findAccount(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="phone"  , required = false)
				@ApiParam(value = "phone")
				String phone
				,@RequestParam(value="name"  , required = false)
				@ApiParam(value = "name")
				String name
		) throws CreateResponseException, UnauthorizedException, BizException, UnsupportedEncodingException {
	
			
			List<Map<String,String>> rtvList = restAccountService.findAccount(phone,name);
			List<Map<String,String>> rtvListResult = new ArrayList<Map<String,String>>();
			if(rtvList == null || rtvList.isEmpty()){
				return createResponse(HttpStatus.NOT_FOUND);
			}else{
				for(Map<String,String> rtv : rtvList){
					
					String[] mailSplit = rtv.get("userId").split("@");
					String pre = mailSplit[0].substring(0, mailSplit[0].length()-3);
					rtv.put("userId", pre+"***@"+mailSplit[1]);
					
					String suf = mailSplit[1];
					String[] sufSplit = suf.split("\\.");
					String sufPre = sufSplit[0].substring(0, sufSplit[0].length()-3);
					rtv.put("userId", pre+"***@"+sufPre+"***."+sufSplit[1]);
					
					String[] authSplit = rtv.get("authMail").split("@");
					String pre_ = authSplit[0].substring(0, authSplit[0].length()-3);
					rtv.put("authMail", pre+"***@"+authSplit[1]);
					
					String suf_ = authSplit[1];
					String[] sufSplit_ = suf_.split("\\.");
					String sufPre_ = sufSplit_[0].substring(0, sufSplit_[0].length()-3);
					rtv.put("authMail", pre_+"***@"+sufPre_+"***."+sufSplit_[1]);
					
					rtvListResult.add(rtv);
				}
			}
			
			return createResponse(HttpStatus.OK,new String[]{"result"}, rtvListResult);
				
		}
		*/
		
		/*
		@ApiOperation(value = "아이디 찾기 none ***", notes = "아이디 찾기 none ***" , response = String.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my account"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/findAccountVeriComplate" , method=RequestMethod.GET)
		public ResponseEntity findAccountVeriComplate(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="phone"  , required = false)
				@ApiParam(value = "phone")
				String phone
				,@RequestParam(value="name"  , required = false)
				@ApiParam(value = "name")
				String name
		) throws CreateResponseException, UnauthorizedException, BizException, UnsupportedEncodingException {
	
			
			CreateUserVO userVo = new CreateUserVO();
			userVo.setMobilePhone(phone);
			if(restAccountService.smsVeriComplateChk(userVo) != 1)
				return createResponse(HttpStatus.UNAUTHORIZED, new String[]{"result"}, "phone verification fail");
			
			List<Map<String,String>> rtvList = restAccountService.findAccount(phone,name);
			if(rtvList == null || rtvList.isEmpty())
				return createResponse(HttpStatus.NOT_FOUND);
			
			
			return createResponse(HttpStatus.OK,new String[]{"result"}, rtvList);
				
		}
		*/
		
		/*
		@ApiOperation(value = "sms 인증요청", notes = "sms 인증요청" , response = String.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my account"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		
		@RequestMapping(value="/api/{ver}/smsCertReq" , method=RequestMethod.GET)
		public ResponseEntity smsCertReq(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="phone"  , required = false)
				@ApiParam(value = "phone")
				String phone
				,@RequestParam(value="duplicate" , required = false , defaultValue = "0")
				@ApiParam(value = "duplicate")
				String dulpicate
		) throws CreateResponseException, UnauthorizedException, BizException {
	
			if(dulpicate.equals("1")){
				CreateUserVO userVo = new CreateUserVO();
				
				userVo.setCorp("0");
				userVo.setMobilePhone(phone);
				
				if(restAccountService.duplicateChkPhone(userVo) > 0)
					return createResponse(HttpStatus.CONFLICT, new String[]{"result"}, "duplicate error. phone is unique in corperation");
			}
			
			
			String rtvList = restAccountService.smsCertReq(phone);
			
			return createResponse(HttpStatus.OK,new String[]{"testResult"}, rtvList);
				
		}
		
		*/
		
		/*
		@ApiOperation(value = "sms 검증요청", notes = "sms 검증요청" , response = String.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my account"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		
		@RequestMapping(value="/api/{ver}/smsVeriReq" , method=RequestMethod.GET)
		public ResponseEntity smsVeriReq(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="phone"  , required = false)
				@ApiParam(value = "phone")
				String phone
				,@RequestHeader(value="certNum"  , required = false)
				@ApiParam(value = "certNum")
				String certNum
		) throws CreateResponseException, UnauthorizedException, BizException {
	
			boolean rtv = restAccountService.smsVeriReq(phone,certNum);
			
			if(rtv) return createResponse(HttpStatus.NO_CONTENT);
			else return createResponse(HttpStatus.NOT_FOUND);
			
				
		}
		*/
		
		/*
		@ApiOperation(value = "mail 인증요청", notes = "mail 인증요청" , response = String.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my account"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		
		@RequestMapping(value="/api/{ver}/mailCertReq" , method=RequestMethod.GET)
		public ResponseEntity mailCertReq(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="mail"  , required = false)
				@ApiParam(value = "mail")
				String mail
		) throws CreateResponseException, UnauthorizedException, BizException {
			
			CreateUserVO userVo = new CreateUserVO();
			userVo.setUserId(mail);
			if(restAccountService.duplicateChkUserId(userVo) > 0)
				return createResponse(HttpStatus.CONFLICT, new String[]{"result"}, "duplicate error. already registed mail");
			
			
			String rtvList = restAccountService.mailCertReq(mail);
			
			return createResponse(HttpStatus.OK,new String[]{"testResult"}, rtvList);
				
		}
		
		*/
		
		/*
		@ApiOperation(value = "mail 검증요청", notes = "mail 검증요청" , response = String.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my account"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		
		@RequestMapping(value="/api/{ver}/mailVeriReq" , method=RequestMethod.GET)
		public ResponseEntity mailVeriReq(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="mail"  , required = false)
				@ApiParam(value = "mail")
				String mail
				,@RequestHeader(value="certNum"  , required = false)
				@ApiParam(value = "certNum")
				String certNum
		) throws CreateResponseException, UnauthorizedException, BizException {
	
			boolean rtv = restAccountService.mailVeriReq(mail,certNum);
			
			if(rtv) return createResponse(HttpStatus.NO_CONTENT);
			else return createResponse(HttpStatus.NOT_FOUND);
			
				
		}
		*/
		
		/*
		@ApiOperation(value = "내정보 조회", notes = "자신의 정보를 가져온다" , response = UserVO.class)		
		@ApiResponses(value = {
				@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NO_CONTENT, message = "no contents"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/myAccount" , method=RequestMethod.GET)
		public ResponseEntity myAccount(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				) throws CreateResponseException, UnauthorizedException, BizException {
			
				
				TokenVO t = authentication(restTokenService,key);
				
				if(t == null) 
					throw new UnauthorizedException();
					
				UserVO rtvList = restAccountService.getAccount(t.getUserKey());
				
				return createResponse(HttpStatus.OK, restTokenService,t,new String[]{"result"}, rtvList);
				
		}
		*/
		
		
		
		
		
		/*
		@ApiOperation(value = "계정 정보를 갱신", notes = "계정 정보를 갱신한다." , response = UserVO.class)		
		@ApiResponses(value = {
				@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
				@ApiResponse(code = HttpURLConnection.HTTP_NO_CONTENT, message = "success not no contents"),
		        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/account" , method=RequestMethod.PUT)
		public ResponseEntity updateUser(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@RequestBody(required = true)
				@Valid
				CreateUserVO userVo
				,BindingResult br
				) throws CreateResponseException, UnauthorizedException, BizException {
			
			try {
				if(br.hasErrors())
					return createValidErrorResponse(response,new String[]{"result"}, br ,null);
				
				TokenVO t = authentication(restTokenService,key);
				
				if(t == null) 
					throw new UnauthorizedException();
					
				UserVO chkVo = restAccountService.getAccount(t.getUserKey());
				
				if(chkVo != null){
					userVo.setUserKey(t.getUserKey());
					userVo.setMobilePhone(chkVo.getMobilePhone());
					userVo.setAuthMail(chkVo.getUserId());
					restAccountService.updateUser(userVo);
					
					return createResponse(HttpStatus.OK);
					
				}else{
					throw new UnauthorizedException();
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				return createResponse(HttpStatus.INTERNAL_SERVER_ERROR, new String[]{"result"}, "internal server error");
			}
		}
		*/
		
		/*
		@ApiOperation(value = "sms 수신동의", notes = "sms 수신동의" , response = UserVO.class)		
		@ApiResponses(value = {
				@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
				@ApiResponse(code = HttpURLConnection.HTTP_NO_CONTENT, message = "success not no contents"),
		        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/account/smsAccept" , method=RequestMethod.PUT)
		public ResponseEntity smsAccept(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@RequestParam(value="accept"  , required = true)
				@ApiParam(value = "accept value 1,0")
				String accept
				) throws CreateResponseException, UnauthorizedException, BizException {
			
			try {
				
				TokenVO t = authentication(restTokenService,key);
				
				if(t == null) 
					throw new UnauthorizedException();
				
				UserVO userInfo = restAccountService.getAccount(t.getUserKey());
				
				if(userInfo == null)
					throw new UserNotFoundException();
				
				restAccountService.updateSmsAccept(t.getUserKey(),accept);
				
				
				return createResponse(HttpStatus.OK);
				
			} catch (Exception e) {
				e.printStackTrace();
				return createResponse(HttpStatus.INTERNAL_SERVER_ERROR, new String[]{"result"}, "internal server error");
			}
		}
		*/
		
		/*
		@ApiOperation(value = "계정 정보를 삭제", notes = "현제 계정 정보를 삭제한다.")		
		@ApiResponses(value = {
				@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/account" , method=RequestMethod.DELETE)
		public ResponseEntity deleteUser(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver								
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				) throws CreateResponseException, UnauthorizedException, BizException {
			
			try {
				
				TokenVO t = authentication(restTokenService,key);
				
				if(t == null) 
					throw new UnauthorizedException();
				
				UserVO user = restAccountService.getAccount(t.getUserKey());
				
				if(user == null) 
					throw new UserNotFoundException();
				
				restAccountService.inactive(user,key);
				
				return createResponse(HttpStatus.NO_CONTENT, restTokenService,t,new String[]{"result"}, "no contents");
				
			} catch (Exception e) {
				e.printStackTrace();
				return createResponse(HttpStatus.INTERNAL_SERVER_ERROR, new String[]{"result"}, "internal server error");
			}
		}
		*/
		
		
		
}
