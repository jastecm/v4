package v4.web.rest.dao;

import java.util.List;
import java.util.Map;

import v4.web.vo.CreateAccidentVO;
import v4.web.vo.UpdateAccidentResultVO;

public interface AccidentDAO {

	void createAccident(CreateAccidentVO createAccidentVO);

	List<Map<String, Object>> getInsureList();

	void updateAccidentResult(UpdateAccidentResultVO updateAccidentResultVO);


}
