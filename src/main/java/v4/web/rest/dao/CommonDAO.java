package v4.web.rest.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import v4.web.vo.CodeVO;
import v4.web.vo.FileVO;
import v4.web.vo.GroupVO;
import v4.web.vo.UserVO;
import v4.web.vo.VehicleVO;


public interface CommonDAO {
	
	public String getAuthId();
	public int chkAuthId(Map<String,String> param);
	
	//	
	public List<CodeVO> getCodeList(CodeVO vo);
	public List<String> getDeviceSeries(Map<String, String> param);
	public List<Map<String, String>> getDeviceList(Map<String, String> param);
	public List<Map<String, String>> getFreeDeviceList(Map<String, String> param);
	public List<UserVO> getUserList(Map<String, String> param);
	public UserVO getUser(Map<String, String> param);
	public void insertFileUpload(FileVO vo);
	public FileVO getFile(String vo);
	public List<GroupVO> getGroupList(Map<String, String> param);
	public String getDeivceIdFromSn(String deviceSn);
	public int getVehicleCnt(Map<String, String> param);
	public int getUserCnt(Map<String, String> param);
	public List<Map<String, String>> getCityLev1(Map<String, String> param);
	public List<Map<String, String>> getCityLev2(Map<String, String> param);
	public List<Map<String, String>> getGroupCode(Map<String, String> param);
	public List<Map<String, String>> getGroupUserList(Map<String, String> param);
	public List<Map<String, Object>> getCorpList(Map<String, String> param);
	public List<Map<String, String>> getGroupVehicleList(Map<String, String> param);
	public Integer getDrivingCnt(Map<String, String> param);
	public Integer getSearchDrivingCnt(Map<String, Object> param);
	public int sendReport(Map<String, String> param);
	public Integer getReportCnt(Map<String, String> param);
	public List<Map<String,String>> getAllocateList(Map<String, String> param);
	public List<Map<String, String>> getCode(Map<String, String> param);
	public int getDistance(Map<String, String> param);
	public List<Map<String, Object>> getMasterList(Map<String, String> param);
	public Integer getVehicleEtcCnt(Map<String, String> param);
	public List<HashMap<String, Object>> getTree();
	
	
	//새로생성
	public Map<String, Object> getCorp(Map<String, Object> param);
	public String getGroupManagerAccountKey(String groupKey);
}
