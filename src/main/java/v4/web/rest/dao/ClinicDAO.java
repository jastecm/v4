package v4.web.rest.dao;

import java.util.List;
import java.util.Map;

import v4.web.vo.ClinicVO;
import v4.web.vo.CreateClinicVO;
import v4.web.vo.CreateMaintenanceCompanyVO;
import v4.web.vo.CreateVehiclePartsEtcVO;
import v4.web.vo.MaintenanceCompanyVO;
import v4.web.vo.VehiclePartsEtcVO;
import v4.web.vo.VehiclePartsVO;

public interface ClinicDAO {

	void addMaintenance(CreateClinicVO vo);

	void deleteMaintenance(Map<String, Object> param);

	void updateMainteance(CreateClinicVO vo);

	ClinicVO getMaintenance(Map<String, Object> param);

	void updateApprovalState(Map<String, Object> param);

	void addMaintenanceDetail(Map<String, Object> param);

	void deleteMaintenanceDetail(Map<String, Object> param);

	List<MaintenanceCompanyVO> getMaintenanceCompanyList(Map<String, Object> param);

	void deleteMaintenanceCompany(Map<String, Object> param);

	void insertMaintenanceCompany(CreateMaintenanceCompanyVO vo);

	List<VehiclePartsVO> getVehiclePartsList(Map<String, Object> param);

	List<Map<String,Object>> getVehiclePartTypeList();
	
	List<VehiclePartsEtcVO> getVehiclePartsEtcList(Map<String, Object> param);

	void deleteVehiclePartsEtc(Map<String, Object> param);

	void createVehiclePartsEtc(CreateVehiclePartsEtcVO vo);

	void addMaintenanceEtc(CreateClinicVO vo);

	void addMaintenanceEtcDetail(Map<String, Object> param);

	void deleteMaintenanceEtc(Map<String, Object> param);

	void deleteMaintenanceEtcDetail(Map<String, Object> param);

	void updateApprovalStateEtc(Map<String, Object> param);

}
