package v4.web.rest.dao;

import v4.web.vo.AdminCorpVO;

public interface AdminCorpDAO {

	AdminCorpVO getAdminCorpInfo(String adminCorp);

}
