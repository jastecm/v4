package v4.web.rest.dao;

import java.util.List;
import java.util.Map;

import v4.web.vo.CreateItemVO;
import v4.web.vo.ItemVO;
import v4.web.vo.UpdateItemVO;
import v4.web.vo.data.TripVO;

public interface ItemDAO {
	List<ItemVO> getVehicleItems(Map<String, String> param);

	ItemVO getVehicleItem(Map<String, String> param);

	void createVehicleItem(CreateItemVO itemVo);

	void updateVehicleItem(UpdateItemVO itemVo);

	void deleteVehicleItem(Map<String, String> param);

	void updateItemMax(UpdateItemVO itemVo);

	void updateItemByTrip(TripVO vo);
}
