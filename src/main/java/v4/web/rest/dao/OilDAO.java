package v4.web.rest.dao;

import v4.web.vo.OilVO;

public interface OilDAO {

	OilVO getUnitOilPrice(String date);

	void insertOilPrice(OilVO vo);

	int oilPriceChk(OilVO vo);
	
	
	
}
