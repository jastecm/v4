package v4.web.rest.dao;

import java.util.Map;

import v4.web.vo.CreateBusAllocateVO;
import v4.web.vo.CreateBusFavoritesVO;
import v4.web.vo.CreateBusRouteStationVO;
import v4.web.vo.CreateDrivingPurposeVO;
import v4.web.vo.UpdateDrivingPurposeVO;

public interface BusDAO {
	void createDrivingPurpose(CreateDrivingPurposeVO drivingPurposeVo);

	void deleteDrivingPurpose(Map<String, Object> param);

	void deleteBusServiceRoute(Map<String, Object> param);

	void updateDrivingPurpose(UpdateDrivingPurposeVO updateDrivingPurposeVo);

	void createBusRoute(Map<String, Object> param);

	void createBusRouteStation(Map<String, Object> param2);

	void deleteBusRoute(Map<String, Object> param);

	void deleteBusRouteStation(Map<String, Object> param);

	void updateBusRoute(Map<String, Object> param);

	Map<String, Object> getBusArrivalTime(Map<String, Object> param);

	Map<String, Object> busMappingCheck(Map<String, Object> param);

	void createBusAllocate(CreateBusAllocateVO busAllocateVO);

	void deleteBusAllocate(Map<String, Object> param);

	void createBusFavorites(CreateBusFavoritesVO busFavoritesVO);

	void deleteBusFavorites(Map<String, Object> param);


}
