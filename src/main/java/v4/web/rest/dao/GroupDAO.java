package v4.web.rest.dao;

import java.util.List;
import java.util.Map;

import v4.web.vo.CreateGroupVO;
import v4.web.vo.GroupVO;

public interface GroupDAO {
	
	void addGroup(Map<String, Object> param);
	
	int getLev0(Map<String, Object> param);

	String getCorpName(Map<String, Object> param);

	int getLevSeq(Map<String, Object> param);

	Map<String, Object> getParentLevSeq(CreateGroupVO reqVo);

	String getLev0Key(Map<String, Object> param);

	List<GroupVO> getGroupList(Map<String, Object> param);

	List<GroupVO> getGroupListDesending(Map<String, Object> param);

	GroupVO getGroup(Map<String, Object> param);

	void delGroup(GroupVO vo);

	void updateAccountGroupKeyToNull(GroupVO vo);

	void delGroupTemp(Map<String, Object> param);

	void insertGroupTemp(Map<String, Object> param);

	void initGroupTemp(Map<String, Object> param);

	void updateTemp();

	void updateCorpGroup(Map<String, Object> param);

	List<GroupVO> getParentGroupTree(Map<String, Object> param);

	int getGroupListCnt(Map<String, Object> param);

	void groupAddUser(Map<String, Object> param);

	void groupDeleteUser(Map<String, Object> param);

	void groupAddVehicle(Map<String, Object> param);

	void groupDeleteVehicle(Map<String, Object> param);

	void delVehicleGroup(GroupVO vo);


	
}
