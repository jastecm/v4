package v4.web.rest.dao;

import java.util.Map;

import v4.web.vo.DeviceVO;




public interface DeviceDAO {

	DeviceVO getDeivceFromSn(String deviceSn);
	
	DeviceVO getDeivceFromId(String deviceId);

	DeviceVO getActiveDeviceChkFromId(String deviceId);

	DeviceVO getActiveDeviceChkFromSn(String deviceSn);
	
	DeviceVO getHstrDeviceFromId(Map<String, Object> param);

	void createHstrDevice(DeviceVO vo);

	void updateConnectionKey(Map<String, Object> param);

	void updateHstrDeviceAccount(DeviceVO vo);

	void updateDeviceShiftDay(Map<String, Object> param);

	String getDeviceShiftDay(String deviceId);

	void setDeviceHello(Map<String, Object> param);

	void setDeviceProvision(Map<String, Object> param);

	void setDeviceFault(Map<String, Object> param);

	int isDeviceMaster(Map<String, Object> param);

	DeviceVO getDeivceFromKey(String deviceKey);

	void updateDownloadInfo(Map<String, Object> param);

	void setDeviceCorpKey(DeviceVO vo);

	void updateLatestPacket(String deviceId);

	
	
}
