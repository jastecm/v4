package v4.web.rest.dao;

import v4.web.vo.CreateExpensesBulkVO;
import v4.web.vo.CreateExpensesVO;

public interface ExpensesDAO {

	void createExpenses(CreateExpensesVO createExpensesVO);

	void createExpensesBulk(CreateExpensesBulkVO vo);

}
