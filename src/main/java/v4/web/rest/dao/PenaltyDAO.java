package v4.web.rest.dao;

import java.util.Map;

import v4.web.vo.CreatePenaltyVO;

public interface PenaltyDAO {

	void createPenalty(CreatePenaltyVO createPenaltyVO);

	void deletePenalty(Map<String, Object> param);
	
}
