package v4.web.rest.dao;

import java.util.Map;

import v4.web.vo.DeviceVO;
import v4.web.vo.DownloadVO;



public interface DownloadDAO {
	
	String getVehicleDbPathByConvCode(String convCode);
	
	DownloadVO getDeviceSeriesVehicleDb(String deviceSn);
	
	DownloadVO getVerMng(String adminCorp);
	////////
	DownloadVO getVer(DownloadVO vo);
	

	String getVehicleBcmSurport(String convCode);

	

	void updateFirmVersion_S31(String version);

	

	void updateVer(Map<String, String> param);

	String getWebViewVer();

	void setWebViewVer(String ver);
	
}
