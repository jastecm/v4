package v4.web.rest.dao;

import java.util.List;
import java.util.Map;

public interface MailBoxDAO {

	void allUserMailBoxInsert(Map<String, Object> param);

	void simgleUserMailBoxInsert(Map<String, Object> param);

	List<Map<String, Object>> getMailBox(Map<String, Object> param);
	int getMailBoxCnt(Map<String, Object> param);

	void deleteMailBox(Map<String, Object> param);
	
	void deleteMailBoxExpired();

	void deleteMailBoxAll(String userKey);

		
}
