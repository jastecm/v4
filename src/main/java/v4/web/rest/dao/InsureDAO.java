package v4.web.rest.dao;

import java.util.List;
import java.util.Map;

import v4.web.vo.InsureVO;

public interface InsureDAO {

	InsureVO getInsure(Map<String, Object> param);

	void createInsure(InsureVO insureVo);
	void updateInsure(InsureVO insureVo);
	void deleteInsure(String key);

	List<Map<String,String>> getMiligeWaringList(Map<String, Object> param);

	
}
