package v4.web.rest.dao;

import java.util.List;
import java.util.Map;

import v4.web.vo.WorkingTimeAreaVO;

public interface WorkingAreaTimeDAO {
/*
	void addWorkingArea(WorkingTimeAreaVO vo);

	List<WorkingTimeAreaVO> getWorkingArea(Map<String, String> param);

	void deleteWorkingArea(String key);

	int getWorkingAreaTotCnt(String corp);

	void updateWorkingArea(WorkingTimeAreaVO vo);

	void addWorkingTime(WorkingTimeAreaVO vo);

	List<WorkingTimeAreaVO> getWorkingTime(Map<String, String> param);

	int getWorkingTimeTotCnt(String corp);

	void deleteWorkingTime(String key);

	void updateWorkingTime(WorkingTimeAreaVO vo);

	List<WorkingAreaDrivingVO> workingAreaDrivingResult(Map<String, String> param);

	
*/
	void deleteWorkingTime(String key);
	
	List<WorkingTimeAreaVO> getWorkingTime(Map<String, Object> param);
	
	void updateWorkingTime(WorkingTimeAreaVO vo);
	
	void addWorkingTime(WorkingTimeAreaVO vo);
	
	void updateWorkingArea(WorkingTimeAreaVO vo);
	
	void deleteWorkingArea(String key);
	
	void addWorkingArea(WorkingTimeAreaVO vo);
	
	List<WorkingTimeAreaVO> getWorkingArea(Map<String, Object> param);
	
	String getCityByPoint(String point);

	String getAccountWorkingTime(Map<String, Object> param);

	String getAccountWorkingArea(String accountKey);

	int getWorkingTimeCnt(Map<String, Object> param);

	int getWorkingTimeTotalCnt(Map<String, Object> param);

}
