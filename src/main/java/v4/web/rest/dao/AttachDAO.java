package v4.web.rest.dao;

import java.util.Map;

public interface AttachDAO {

	void saveAttachInfo(Map<String,Object> vo);
	
}
