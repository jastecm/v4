package v4.web.rest.dao;

import java.util.List;
import java.util.Map;

import v4.web.vo.PushControllVO;

public interface PushDAO {

	void pushKeyRefresh(Map<String, Object> param);

	void removeDeny(Map<String, Object> param);

	void addDeny(Map<String, Object> param);

	List<String> getDenyList(String userKey);

	List<Map<String, String>> getPushList(Map<String, Object> param);
	
	void delPushList(String pushKey);


	void updatePushControllState(PushControllVO vo);

	List<PushControllVO> getPushControllList(String accountKey);
	
	PushControllVO getPushControll(Map<String,Object> param);

	void updatePushControllLastest(PushControllVO vo);

	void insertPushHstr(Map<String, Object> param);

	void updatePushControllAll(Map<String, Object> param);
}
