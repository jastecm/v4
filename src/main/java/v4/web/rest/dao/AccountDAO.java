package v4.web.rest.dao;

import java.util.List;
import java.util.Map;

import v4.web.vo.AccountVO;
import v4.web.vo.LoginVO;



public interface AccountDAO {
	
	public AccountVO getAccountFromIdPw(LoginVO vo);
	public AccountVO getAccountFromId(String userId);
	public AccountVO getAccountFromKey(String userKey);
	
	public void createUser(AccountVO avo);
	public void updateCorpManager(Map<String, Object> param);
	public AccountVO getAccountFromKeySession(String userKey);
	
	public List<AccountVO> getAccountList(Map<String, Object> param);
	public int getAccountListCnt(Map<String, Object> param);
	public void accountUseStop(String string);
	public int getAllAccountCount(Map<String, Object> param);
	public void updateAccureData(Map<String, Object> param);
	public void updateAccureDataReverse(Map<String, Object> param);
	
/////
	/*
	public int duplicateChkPhone(CreateUserVO vo);
	public int duplicateChkUserId(CreateUserVO vo);
	
	public void createUser(CreateUserVO vo);	
	public List<UserVO> getAccountsFromPhone(String userKey);
	
	public void inactive(String userKey);
	
	public void deleteSmsCert(String phone);
	public void insertSmsCert(Map<String, String> param);
	public int chkSmsCert(Map<String, String> param);
	public void veriChkSmsComplate(Map<String, String> param);
	
	public void deleteMailCert(String mail);
	public void insertMailCert(Map<String, String> param);
	public int chkMailCert(Map<String, String> param);
	public void veriChkMailComplate(Map<String, String> param);
	public int smsVeriComplateChk(CreateUserVO userVo);
	public int mailVeriComplateChk(CreateUserVO userVo);
	
	public void clearSmsCert();
	public void clearSmsVeri();
	public void clearMailCert();
	public void clearMailVeri();
	public List<Map<String,String>> findAccount(Map<String, String> param);
	public String findAccountKey(Map<String, String> param);
	public void changePassword(Map<String, String> param);
	public int changeNewPassword(Map<String, String> param);
	public void updateUser(CreateUserVO userVo);
	public void updateSmsAccept(Map<String, String> param);
	public UserVO getAccountFromId(String decodeId);
	public List<UserVO> getInviteUser(String vehicleKey);
	public List<UserVO> getInvitedUser(String vehicleKey);
	*/
	
}
