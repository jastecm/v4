package v4.web.rest.dao;

import java.util.Map;

import v4.web.vo.AccountVO;
import v4.web.vo.ApprovalConfigVO;
import v4.web.vo.CorpVO;
import v4.web.vo.CreateApprovalConfigVO;
import v4.web.vo.CreateCorpVO;
import v4.web.vo.CreatePartnerVO;
import v4.web.vo.CreatePersonVO;
import v4.web.vo.CreateRentVO;

public interface CorpDAO {

	void createCorp(CreateCorpVO vo);

	void createCorpPerson(CreatePersonVO vo);
	
	void createCorpRent(CreateRentVO vo);
	
	void createPartner(CreatePartnerVO vo);

	CorpVO getCorp(Map<String, Object> param);

	void changeCorpVehicleManager(Map<String, Object> param);

	AccountVO getCorpVehicleManager(Map<String, Object> param);

	void updateApprovalConfig(CreateApprovalConfigVO createApprovalConfigVo);

	ApprovalConfigVO getApprovalConfig(Map<String, Object> param);

	

	
	
	
}
