package v4.web.rest.dao.data;

import java.util.List;
import java.util.Map;

import v4.web.vo.data.ExpensesTotalVO;
import v4.web.vo.data.ExpensesVO;

public interface ExpensesREPO {

	ExpensesTotalVO getExpensesTotal(Map<String, Object> param);

	List<ExpensesVO> getExpensesList(Map<String, Object> param);

	int getExpensesListCnt(Map<String, Object> param);


}
