package v4.web.rest.dao.data;

import java.util.List;
import java.util.Map;

import v4.web.vo.data.MaintenanceVO;

public interface MaintenanceREPO {

	List<MaintenanceVO> getMaintenanceList(Map<String, Object> param);

	int getMaintenanceListCnt(Map<String, Object> param);

}
