package v4.web.rest.dao.data;

import java.util.List;
import java.util.Map;

import v4.web.vo.data.DeviceVO;

public interface DeviceREPO {

	List<DeviceVO> getDeviceList(Map<String,Object> param);
	DeviceVO getDevice(Map<String,Object> param);
	DeviceVO getDeviceFromKey(String key);
	DeviceVO getDeviceFromSn(String key);
	DeviceVO getDeviceFromId(String key);
	int getDeviceListCnt(Map<String,Object> param);
	DeviceVO getDeviceEnd(Map<String,Object> param);

	
}
