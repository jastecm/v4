package v4.web.rest.dao.data;

import java.util.List;
import java.util.Map;

import v4.web.vo.data.PenaltyVO;

public interface PenaltyREPO {

	List<PenaltyVO> getPenaltyList(Map<String, Object> param);

	int getPenaltyListCnt(Map<String, Object> param);

	int getPenaltyPointCnt(Map<String, Object> param);

	int getPenaltyPriceCnt(Map<String, Object> param);

}
