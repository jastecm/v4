package v4.web.rest.dao.data;

import java.util.List;
import java.util.Map;

import v4.web.vo.CreateDrivingPurposeVO;
import v4.web.vo.data.BusRouteVO;
import v4.web.vo.data.DrivingPurposeVO;

public interface BusREPO {

	List<DrivingPurposeVO> getDrivingPurpose(Map<String, Object> param);

	int getDrivingPurposeCnt(Map<String, Object> param);

	List<BusRouteVO> getBusRouteList(Map<String, Object> param);

	int getBusRouteListCnt(Map<String, Object> param);

	List<Map<String, Object>> getBusAllocateList(Map<String, Object> param);

	int getBusAllocateListCnt(Map<String, Object> param);

}
