package v4.web.rest.dao.data;

import java.util.List;
import java.util.Map;

import v4.web.vo.data.VehicleLocationVO;
import v4.web.vo.data.VehicleVO;




public interface VehicleLocationREPO {

	List<VehicleLocationVO> getVehicleLocationList(Map<String, Object> param);

	

	
}
