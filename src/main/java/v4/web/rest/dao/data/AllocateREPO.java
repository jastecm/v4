package v4.web.rest.dao.data;

import java.util.List;
import java.util.Map;

import v4.web.vo.LoginVO;
import v4.web.vo.data.AccountVO;
import v4.web.vo.data.AllocateVO;

public interface AllocateREPO {

	List<AllocateVO> getAllocateList(Map<String, Object> param);

	AllocateVO getAllocate(Map<String, Object> param);

	AllocateVO getAllocateFromKey(String key);

	int getAllocateListCnt(Map<String, Object> param);

	
	
}
