package v4.web.rest.dao.data;

import java.util.List;
import java.util.Map;

import v4.web.vo.data.TripVO;

public interface TripREPO {

	List<TripVO> getTripList(Map<String,Object> param);
	TripVO getTrip(Map<String,Object> param);
	TripVO getTripFromKey(String key);
	TripVO getTripFromMsg(Map<String,Object> param);
	
	int getTripListCnt(Map<String,Object> param);
	TripVO getTripEnd(Map<String,Object> param);
	
	TripVO getTripNoneVehicle(Map<String,Object> param);
	TripVO getTripNoneAccount(Map<String,Object> param);
	
}
