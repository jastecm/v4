package v4.web.rest.dao.data;

import java.util.List;
import java.util.Map;

import v4.web.vo.LoginVO;
import v4.web.vo.data.AccountVO;

public interface AccountREPO {

	List<AccountVO> getAccountList(Map<String,Object> param);
	AccountVO getAccount(Map<String,Object> param);
	AccountVO getAccountFromKey(String key);
	AccountVO getAccountFromKeySimple(String key);
	AccountVO getAccountFromId(String id);
	AccountVO getAccountFromIdPw(LoginVO vo);
	AccountVO getAccountFromPhonePw(LoginVO vo);
	int getAccountListCnt(Map<String,Object> param);
	AccountVO getAccountEnd(Map<String,Object> param);
	
	AccountVO getAccountNoneGroup(Map<String,Object> param);
	AccountVO getAccountNoneAll(Map<String,Object> param);
	AccountVO getAccountGroupOnly(Map<String,Object> param);
	
	
}
