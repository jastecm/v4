package v4.web.rest.dao.data;

import java.util.List;
import java.util.Map;

import v4.web.vo.data.GroupVO;

public interface GroupREPO {

	List<GroupVO> getGroupList(Map<String,Object> param);
	List<GroupVO> getGroupParentList(Map<String,Object> param);
	List<GroupVO> getGroupChildList(Map<String,Object> param);
	GroupVO getGroup(Map<String,Object> param);
	GroupVO getGroupFromKey(String key);
	int getGroupListCnt(Map<String,Object> param);
	GroupVO getGroupEnd(Map<String,Object> param);
	
}
