package v4.web.rest.dao.data;

import java.util.List;
import java.util.Map;

import v4.web.vo.data.TrackInfoVO;

public interface TrackREPO {

	TrackInfoVO getTrackInfo(Map<String, Object> param);
	List<TrackInfoVO> getTrackInfoList(Map<String, Object> param);
	

	
}
