package v4.web.rest.dao.data;

import java.util.List;
import java.util.Map;

import v4.web.vo.data.AccidentVO;

public interface AccidentREPO {

	List<AccidentVO> getAccidentList(Map<String, Object> param);

	int getAccidentListCnt(Map<String, Object> param);

}
