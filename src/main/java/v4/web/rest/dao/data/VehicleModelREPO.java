package v4.web.rest.dao.data;

import java.util.Map;

import v4.web.vo.data.VehicleModelVO;




public interface VehicleModelREPO {

	
	VehicleModelVO getVehicleModel(Map<String,Object> param);

	
}
