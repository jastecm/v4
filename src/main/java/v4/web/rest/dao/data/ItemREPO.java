package v4.web.rest.dao.data;

import java.util.List;
import java.util.Map;

import v4.web.vo.data.ItemVO;
import v4.web.vo.data.PartsVO;


public interface ItemREPO {
	List<ItemVO> getVehicleItems(Map<String, Object> param);

	ItemVO getVehicleItem(Map<String, Object> param);

	List<PartsVO> getPartsList(Map<String, Object> param);

	int getPartsListCnt(Map<String, Object> param);

	PartsVO getPart(Map<String, Object> param);

	List<PartsVO> getPartsDistinctList(Map<String, Object> param);

}
