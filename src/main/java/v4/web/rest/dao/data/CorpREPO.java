package v4.web.rest.dao.data;

import java.util.Map;

import v4.web.vo.data.ApprovalVO;
import v4.web.vo.data.CorpVO;

public interface CorpREPO {

	
	CorpVO getCorp(Map<String,Object> param);

	CorpVO getCorpFromKey(String key);
	
	ApprovalVO getApproval(String key);
	
}
