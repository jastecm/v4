package v4.web.rest.dao.data;

import java.util.List;
import java.util.Map;

import v4.web.vo.data.VehicleVO;




public interface VehicleREPO {

	
	VehicleVO getVehicle(Map<String, Object> param);
	
	VehicleVO getVehicleFromKey(String key);
	
	List<VehicleVO> getVehicleList(Map<String, Object> param);
	
	int getVehicleListCnt(Map<String, Object> param);

	VehicleVO getVehicleFromPlateNum(String plateNum);
	
//	void updateVehicle(Map<String, Object> param);

	
}
