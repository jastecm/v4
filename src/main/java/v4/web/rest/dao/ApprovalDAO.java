package v4.web.rest.dao;

import java.util.Map;

public interface ApprovalDAO {

	Map<String, Object> getMaintenanceApproval(Map<String, Object> param);

}
