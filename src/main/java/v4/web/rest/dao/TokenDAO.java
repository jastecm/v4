package v4.web.rest.dao;

import java.util.List;

import v4.web.vo.TokenVO;




public interface TokenDAO {
	
	public void clearUserToken(String userKey);
	public void semiClearUserToken(String userKey);
	public void addUserToken(TokenVO t);
	public List<Long> getUserToken(String userKey);
}
