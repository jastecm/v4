package v4.web.rest.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import v4.web.vo.AllocateVO;
import v4.web.vo.CreateAllocateVO;
import v4.web.vo.LocationVO;
import v4.web.vo.TripVO;

public interface AllocateDAO {
	
	void createAllocate(CreateAllocateVO vo);
	
	void deleteAllocate(String allocateKey);
	/*
	AllocateVO getMyAllocate(String userKey);
	
	

	void createAlloccateApproval(CreateAllocateVO vo);

	

	void updateAllocateApprovalComplate(Map<String, Object> param);

	AllocateVO getAllocate(AllocateVO chker);

	LocationVO allocateReleaseChk(AllocateVO chker);

	void allocateReturn(Map<String, String> param);

	HashMap<String, String> getNextAllocate(Map<String, Object> param);

	void updateAllocateExtend(Map<String, Object> param);

	AllocateVO getAllocateApproval(Map<String, Object> param);

	void updateApprovalState(Map<String, Object> param);

	void updateInstantAllocateApprovalComplate(Map<String, Object> param);

	void updateInstantAllocateApprovalReject(Map<String, Object> param);

	void updateApprovalRejectAllocateKeyUpdate(Map<String, Object> param);

	List<Map<String,String>> getDrivingList(Map<String, Object> param);

	TripVO getTrip(String string);

	AllocateVO getAllocateMatchedResult(TripVO vo);

	void insertInstantAllocate(AllocateVO avo);

	void updateTripAllocateKey(Map<String, Object> param2);

	List<AllocateVO> getAlocateList(Map<String, Object> param);

	String getVehicleAllocateUser(String vehicleKey);
	*/
}
