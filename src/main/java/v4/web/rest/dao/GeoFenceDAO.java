package v4.web.rest.dao;

import java.util.List;
import java.util.Map;

import v4.web.vo.CreateGeoFenceVO;
import v4.web.vo.GeoFenceVO;



public interface GeoFenceDAO {

	GeoFenceVO getGeoFenceAllChk(CreateGeoFenceVO vo);
	
	int getGeoFenceCnt(CreateGeoFenceVO vo);

	void addGeoFence(CreateGeoFenceVO vo);
	
	List<GeoFenceVO> getGeoFenceList(Map<String,Object> param);
	
	void updateGeoFence(CreateGeoFenceVO vo);
	
	void deleteGeoFence(String fenceKey);
	
	GeoFenceVO getGeoFence(String fenceKey);
	
	GeoFenceVO getGeoFenceChk(Map<String, Object> param);
	
	void updateFenceMonitor(Map<String, Object> param);
	/*
	
	void updateGeoFencePush(GeoFenceVO vo);
	
	
	
	

	
	

	void updateAccountPush(Map<String, Object> param);

	void updateGeofenceOnOff(Map<String, String> param);

	void saveGeofenceHstr(Map<String, Object> param);

	List<Map<String, String>> getGeoFenceUserList(Map<String, String> param);

	List<Map<String, String>> getGeoFenceUserDetail(Map<String, String> param);

	void deleteGeoFenceHstr(String fenceKey);
*/

	

	
	


	
	
}
