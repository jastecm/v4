package v4.web.rest.dao;

import java.util.Map;




public interface TripDAO {

	void updatePurpose(Map<String, Object> param);

	void updatePurposeCompareDay(Map<String, Object> param);

		
}
