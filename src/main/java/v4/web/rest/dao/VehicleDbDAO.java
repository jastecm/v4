package v4.web.rest.dao;

import java.util.List;
import java.util.Map;

import v4.web.vo.VehicleDbVO;





public interface VehicleDbDAO {

	List<String> getManufacture(String lang);

	List<String> getModelMaster(Map<String, String> param);

	List<String> getYear(Map<String, String> param);

	List<String> getModelHeader(Map<String, String> param);

	List<String> getTransmission(Map<String, String> param);

	List<String> getFuelType(Map<String, String> param);

	List<String> getVolume(Map<String, String> param);

	Map<String,String> getConvCode(VehicleDbVO vo);

	Map<String, String> getVehicleDbInfo(Map<String, String> param);
	
}
