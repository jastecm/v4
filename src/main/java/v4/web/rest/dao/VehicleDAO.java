package v4.web.rest.dao;

import java.util.List;
import java.util.Map;

import v4.web.vo.AllocateVO;
import v4.web.vo.CreateBusVO;
import v4.web.vo.CreateEtcVO;
import v4.web.vo.CreateTruckVO;
import v4.web.vo.CreateVehicleVO;
import v4.web.vo.VehicleVO;
import v4.web.vo.data.TripVO;




public interface VehicleDAO {

	void createVehicle(CreateVehicleVO vo);

	void createVehicleTemp(CreateVehicleVO vo);
	
	int duplicatePlateNumChk(String plateNum);

	void deleteVehicleTemp(String deviceSn);

	VehicleVO getVehicle(Map<String, Object> param);

	List<VehicleVO> getVehicleList(Map<String, Object> param);
	int getVehicleListCnt(Map<String, Object> param);
	

	void inactiveVehicle(String vehicleKey);

	void deleteVehicle(String vehicleKey);
	
	List<VehicleVO> getAllocateListBeCan(AllocateVO vo);

	void updateLastestLocation(Map<String, Object> param);

	void updateLastestTrip(Map<String, Object> param);

	void updateLastestTripStart(Map<String, Object> param);

	void updateTotDistance(TripVO vo);

	VehicleVO getVehicleInAllocate(VehicleVO vvo);

	void updateLastestLoc(VehicleVO vvo);

	CreateVehicleVO getTempVehicle(String deviceSn);

	void updateLastestDbInfoTemp(Map<String, Object> param);

	void reactiveVehicle(CreateVehicleVO vo);

	void updateVehicle(Map<String, Object> param);

	void createBus(CreateBusVO vo);

	void reactiveBus(CreateBusVO vo);

	void reactiveTruck(CreateTruckVO vo);

	void createTruck(CreateTruckVO vo);
	
	void createEtc(CreateEtcVO vo);

	
}
