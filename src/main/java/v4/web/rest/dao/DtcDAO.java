package v4.web.rest.dao;

import java.util.List;
import java.util.Map;



public interface DtcDAO {
	/*
	public DTCCodeVO getDtcManufactureDetail(DTCCodeVO vo);
	public DTCCodeVO getDtcGenaralDetail(DTCCodeVO vo);
	public List<Map<String, String>> getDtcHeaderList(Map<String, String> param);
	public void removeAction(String vehicleKey);
	public void removeHstrRecord(String dtcKey);
	public void removeHstr(String dtcKey);
	*/
	public void insertDtcHstr(Map<String, Object> param);
	public int chkTripDtc(Map<String, Object> param);
	public String dtcKeyFromTime(Map<String, Object> param);
	public void appendDtc(Map<String, Object> param);
	public String getTripDtcKey(String tripKey);
	public List<Map<String, String>> getDtcList(Map<String, Object> param);
	public Map<String, String> getDtcDesc(Map<String, String> param);
	
	
}
