package v4.web.rest.dao;

import java.util.List;
import java.util.Map;

import v4.web.vo.EventVO_Attach;
import v4.web.vo.EventVO_Item;
import v4.web.vo.EventVO_Milige;
import v4.web.vo.EventVO_Trip;
import v4.web.vo.EventVO_Trouble;


public interface TimeLineDAO {
	void insertTimeLineEvent(Map<String, Object> vo);

	int getTripListCnt(Map<String, String> param);
	List<EventVO_Trip> getTripList(Map<String, String> param);

	int getItemListCnt(Map<String, String> param);
	List<EventVO_Item> getItemList(Map<String, String> param);


	int getMiligeListCnt(Map<String, String> param);
	List<EventVO_Milige> getMiligeList(Map<String, String> param);

	int getAttachListCnt(Map<String, String> param);
	List<EventVO_Attach> getAttachList(Map<String, String> param);

	int getTroubleListCnt(Map<String, String> param);
	List<EventVO_Trouble> getTroubleList(Map<String, String> param);
}
