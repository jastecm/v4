package v4.web.rest.dao;

import java.util.Map;

import v4.web.vo.TripRecordVO;
import v4.web.vo.TripVO;




public interface DeviceMsgDAO {

	void insertTdr(TripRecordVO vo);

	int chkTdr(TripRecordVO vo);
	
	TripVO getTrip(Map<String, Object> param);

	void updateValidTripState(String tripKey);

	void updateMicroTrip(v4.web.vo.data.TripVO trip);

	void tripInit(TripVO newTripVo);

	void updateTrip(v4.web.vo.data.TripVO tempTrip);



	
}
