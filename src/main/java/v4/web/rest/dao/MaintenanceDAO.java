package v4.web.rest.dao;

import java.util.Map;

import v4.web.vo.CreateMaintenanceVO;

public interface MaintenanceDAO {

	void createMaintenance(CreateMaintenanceVO createMaintenanceVO);

	void updateMaintenance(Map<String, Object> param);

	void insertMaintenanceDetail(Map<String, Object> param);

}
