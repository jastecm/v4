package v4.web.rest.controller;

import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import v4.ex.exc.BizException;
import v4.ex.exc.CreateResponseException;
import v4.ex.exc.DefaultInitCheckException;
import v4.ex.exc.NotFoundException;
import v4.ex.exc.extents.UnauthorizedException;
import v4.ex.exc.extents.VehicleNotFoundException;
import v4.web.controller.BasicWebViewController;
import v4.web.rest.service.DeviceService;
import v4.web.rest.service.DtcService;
import v4.web.rest.service.VehicleDBService;
import v4.web.rest.service.VehicleService;
import v4.web.vo.DeviceVO;
import v4.web.vo.DtcDescriptionVO;
import v4.web.vo.DtcVO;
import v4.web.vo.VehicleDbVO;
import v4.web.vo.VehicleVO;




/*
 * @Class name : LoginController
 * @Description : 
 * @Modification Information
 * 
 * @author 
 * @since 
 * @version 
 * @see
 *
 */
@Api(value="restVehicleDBController")
@RestController
public class RestVehicleDBController extends BasicWebViewController{
		
		ObjectMapper mapper = new ObjectMapper();
	
		@Autowired
		public VehicleDBService vehicleDBService;
		
		@ApiOperation(value = "차량DB - convCode", notes = "차량DB - convCode" , response = String.class , responseContainer = "List")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list manufacture"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/vehicleDb/{lang}/convCode/{convCode}" , method={RequestMethod.GET,RequestMethod.POST})
		public ResponseEntity getVehicleDbConvCode(HttpServletRequest req,HttpServletResponse response
				
				,@PathVariable(value="lang") 
				@ApiParam(value = "decide return language")
				String lang
				,@PathVariable(value="convCode") 
				@ApiParam(value = "convCode")
				String convCode
				) throws CreateResponseException, UnauthorizedException, BizException 
		{
			
			String vlogKey = jwt.generateVlogToken();
			System.out.println(vlogKey);
			
			Map<String,String> rtvList = null;
			if(convCode.equals("000000000000")){
				rtvList = new HashMap<String,String>();
				rtvList.put("DB_VERSION", "1");
				rtvList.put("COMPOUND_FUEL_EFFICIENCY", "0");
				rtvList.put("FUEL_TYPE", "gasoline");
				rtvList.put("VOLUME_I", "2000");
				rtvList.put("CYLINDER", "4");
			}else{
				rtvList = vehicleDBService.getVehicleDbInfo(lang,convCode);
			}
			
			
			return createResponse(HttpStatus.OK, new String[]{"result"}, rtvList);
				
		}
		
		@ApiOperation(value = "차량DB - 제조사", notes = "차량DB - 제조사" , response = String.class , responseContainer = "List")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list manufacture"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/vehicleDb/{lang}/manufacture" , method={RequestMethod.GET,RequestMethod.POST})
		public ResponseEntity getVehicleDbManufacture(HttpServletRequest req,HttpServletResponse response
				
				,@PathVariable(value="lang") 
				@ApiParam(value = "decide return language")
				String lang
				
				) throws CreateResponseException, UnauthorizedException, BizException 
		{
			
			List<String> rtvList = vehicleDBService.getManufacture(lang);
			
			
			return createResponse(HttpStatus.OK, new String[]{"result"}, rtvList);
				
		}
		
		
		@ApiOperation(value = "차량DB - 차종", notes = "차량DB - 차종" , response = String.class , responseContainer = "List")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list modelMaster"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/vehicleDb/{lang}/manufacture/{manufacture}/modelMaster" , method={RequestMethod.GET,RequestMethod.POST})
		public ResponseEntity getVehicleDbModelMaster(HttpServletRequest req,HttpServletResponse response
				
				,@PathVariable(value="lang") 
				@ApiParam(value = "decide return language")
				String lang
				
				,@PathVariable(value="manufacture") 
				@ApiParam(value = "manufacture")
				String manufacture
				
				) throws CreateResponseException, UnauthorizedException, BizException 
		{
			
			List<String> rtvList = vehicleDBService.getModelMaster(lang,manufacture);
			return createResponse(HttpStatus.OK, new String[]{"result"}, rtvList);
			
		}
		
		
		@ApiOperation(value = "차량DB - 년식", notes = "차량DB - 년식" , response = String.class , responseContainer = "List")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns year"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/vehicleDb/{lang}/manufacture/{manufacture}/modelMaster/{modelMaster}/year" , method={RequestMethod.GET,RequestMethod.POST})
		public ResponseEntity getVehicleDbYear(HttpServletRequest req,HttpServletResponse response
				
				,@PathVariable(value="lang") 
				@ApiParam(value = "decide return language")
				String lang
				
				,@PathVariable(value="manufacture") 
				@ApiParam(value = "manufacture")
				String manufacture
		
				,@PathVariable(value="modelMaster") 
				@ApiParam(value = "modelMaster")
				String modelMaster
				) throws CreateResponseException, UnauthorizedException, BizException 
		{
			
			List<String> rtvList = vehicleDBService.getYear(lang,manufacture,modelMaster);
			
			return createResponse(HttpStatus.OK, new String[]{"result"}, rtvList);
				
		}
		
		@ApiOperation(value = "차량DB - 모델", notes = "차량DB - 모델" , response = String.class , responseContainer = "List")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list modelHeader"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/vehicleDb/{lang}/manufacture/{manufacture}/modelMaster/{modelMaster}/year/{year}/modelHeader" , method={RequestMethod.GET,RequestMethod.POST})
		public ResponseEntity getVehicleDbModelHeader(HttpServletRequest req,HttpServletResponse response
				
				,@PathVariable(value="lang") 
				@ApiParam(value = "decide return language")
				String lang
				
				,@PathVariable(value="manufacture") 
				@ApiParam(value = "manufacture")
				String manufacture
		
				,@PathVariable(value="modelMaster") 
				@ApiParam(value = "modelMaster")
				String modelMaster
				
				,@PathVariable(value="year") 
				@ApiParam(value = "year")
				String year
				) throws CreateResponseException, UnauthorizedException, BizException 
		{
			/*
			String ip = req.getHeader("X-FORWARDED-FOR");
	        if (ip == null)
	            ip = req.getRemoteAddr();
	        
	        System.out.println(ip);
			
			List apiIpCounter = apiCount.get(ip);
			if(apiIpCounter == null || apiIpCounter.size() < apiLimit){				
				List<String> rtvList = vehicleDBService.getModelHeader(lang,manufacture,modelMaster,year);
				if(modelMaster.length() > 0){
					if(apiIpCounter == null) {
						apiIpCounter = new ArrayList<Long>(); 
						apiIpCounter.add(new Date().getTime());
						apiCount.put(ip,apiIpCounter);
					}else{
						apiIpCounter.add(new Date().getTime());
					}
				}
				
				return createResponse(HttpStatus.OK, new String[]{"result"}, rtvList);
			}else{
				System.out.println("ip cooltime - " + ip);
				return createResponse(HttpStatus.FORBIDDEN, new String[]{"result"}, apiIpCounter.get(0));
			}
			*/
			List<String> rtvList = vehicleDBService.getModelHeader(lang,manufacture,modelMaster,year);	
			return createResponse(HttpStatus.OK, new String[]{"result"}, rtvList);
		}
		/*
		public static final int apiLimit = 100;
		public static final int apiDurationMin = 3;
		public static final int apiDuration = (1000*60)*apiDurationMin; 
		
		public static Map<String,List<Long>> apiCount = new HashMap<String,List<Long>>();
		 */
		
		@ApiOperation(value = "차량DB - 변속기", notes = "차량DB - 변속기" , response = String.class , responseContainer = "List")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of transmission"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/vehicleDb/{lang}/manufacture/{manufacture}/modelMaster/{modelMaster}/year/{year}/modelHeader/{modelHeader}/transmission" , method={RequestMethod.GET,RequestMethod.POST})
		public ResponseEntity getVehicleDbTransmission(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="lang") 
				@ApiParam(value = "decide return language")
				String lang
				,@PathVariable(value="manufacture") 
				@ApiParam(value = "manufacture")
				String manufacture
				,@PathVariable(value="modelMaster") 
				@ApiParam(value = "modelMaster")
				String modelMaster
				,@PathVariable(value="year") 
				@ApiParam(value = "year")
				String year
				,@PathVariable(value="modelHeader") 
				@ApiParam(value = "modelHeader")
				String modelHeader
				) throws CreateResponseException, UnauthorizedException, BizException 
		{
			
			List<String> rtvList = vehicleDBService.getTransmission(lang,manufacture,modelMaster,year,modelHeader);
			
			
			return createResponse(HttpStatus.OK, new String[]{"result"}, rtvList);
				
		}
		
		@ApiOperation(value = "차량DB - 유종", notes = "차량DB - 유종" , response = String.class , responseContainer = "List")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of fuelType"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/vehicleDb/{lang}/manufacture/{manufacture}/modelMaster/{modelMaster}/year/{year}/modelHeader/{modelHeader}/fuelType" , method={RequestMethod.GET,RequestMethod.POST})
		public ResponseEntity getVehicleDbFuelType(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="lang") 
				@ApiParam(value = "decide return language")
				String lang
				,@PathVariable(value="manufacture") 
				@ApiParam(value = "manufacture")
				String manufacture
				,@PathVariable(value="modelMaster") 
				@ApiParam(value = "modelMaster")
				String modelMaster
				,@PathVariable(value="year") 
				@ApiParam(value = "year")
				String year
				,@PathVariable(value="modelHeader") 
				@ApiParam(value = "modelHeader")
				String modelHeader
				) throws CreateResponseException, UnauthorizedException, BizException 
		{
			
			List<String> rtvList = vehicleDBService.getFuelType(lang,manufacture,modelMaster,year,modelHeader);
			
			
			return createResponse(HttpStatus.OK, new String[]{"result"}, rtvList);
				
		}
		
		@ApiOperation(value = "차량DB - 배기량", notes = "차량DB - 배기량" , response = String.class , responseContainer = "List")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of volume"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/vehicleDb/{lang}/manufacture/{manufacture}/modelMaster/{modelMaster}/year/{year}/modelHeader/{modelHeader}/volume" , method={RequestMethod.GET,RequestMethod.POST})
		public ResponseEntity getVehicleDbVolume(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="lang") 
				@ApiParam(value = "decide return language")
				String lang
				,@PathVariable(value="manufacture") 
				@ApiParam(value = "manufacture")
				String manufacture
				,@PathVariable(value="modelMaster") 
				@ApiParam(value = "modelMaster")
				String modelMaster
				,@PathVariable(value="year") 
				@ApiParam(value = "year")
				String year
				,@PathVariable(value="modelHeader") 
				@ApiParam(value = "modelHeader")
				String modelHeader
				) throws CreateResponseException, UnauthorizedException, BizException 
		{
			
			
			List<String> rtvList = vehicleDBService.getVolume(lang,manufacture,modelMaster,year,modelHeader);
			
			
			return createResponse(HttpStatus.OK, new String[]{"result"}, rtvList);				
		}
		
		@ApiOperation(value = "차량DB - 차량코드", notes = "차량DB - 차량코드" , response = String.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of volume"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/vehicleDbCode/{lang}/{manufacture}/{modelMaster}/{year}/{modelHeader}/{fuelType}/{transmission}/{volume}" , method={RequestMethod.GET,RequestMethod.POST})
		public ResponseEntity getVehicleDbConvCode(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="lang") 
				@ApiParam(value = "decide return language")
				String lang
				,@PathVariable(value="manufacture") 
				@ApiParam(value = "manufacture")
				String manufacture
				,@PathVariable(value="modelMaster") 
				@ApiParam(value = "modelMaster")
				String modelMaster
				,@PathVariable(value="year") 
				@ApiParam(value = "year")
				String year
				,@PathVariable(value="modelHeader") 
				@ApiParam(value = "modelHeader")
				String modelHeader
				,@PathVariable(value="fuelType") 
				@ApiParam(value = "fuelType")
				String fuelType
				,@PathVariable(value="transmission") 
				@ApiParam(value = "transmission")
				String transmission
				,@PathVariable(value="volume") 
				@ApiParam(value = "volume")
				String volume
				) throws CreateResponseException, UnauthorizedException, BizException,DefaultInitCheckException
		{
			
			
			
			VehicleDbVO vo = new VehicleDbVO(lang,manufacture,modelMaster,year,modelHeader,volume,transmission,fuelType);
			Map<String,String> rtvList = vehicleDBService.getConvCode(vo);
			
			
			return createResponse(HttpStatus.OK, new String[]{"result"}, rtvList);				
		}
		
		
}
