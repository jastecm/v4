package v4.web.rest.controller;

import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import v4.ex.exc.BizException;
import v4.ex.exc.CreateResponseException;
import v4.ex.exc.NotFoundException;
import v4.ex.exc.extents.UnauthorizedException;
import v4.ex.exc.extents.VehicleNotFoundException;
import v4.web.controller.BasicWebViewController;
import v4.web.rest.service.DeviceService;
import v4.web.rest.service.DtcService;
import v4.web.rest.service.VehicleDBService;
import v4.web.rest.service.VehicleService;
import v4.web.vo.DeviceVO;
import v4.web.vo.DtcDescriptionVO;
import v4.web.vo.DtcVO;
import v4.web.vo.VehicleVO;


/*
 * @Class name : LoginController
 * @Description : 
 * @Modification Information
 * 
 * @author 
 * @since 
 * @version 
 * @see
 *
 */
@Api(value="restDTCController")
@RestController
public class RestDTCController extends BasicWebViewController{
		
		ObjectMapper mapper = new ObjectMapper();
	/*
		@Autowired
		public RestTokenService restTokenService;
		
		@Autowired
		public RestVehicleService restVehicleService;
		
		@Autowired
		public RestAccountService restAccountService;
		
		@Autowired
		public RestDtcService restDtcService;
		
		@Autowired
		AccountService accountService;
		
		@Autowired
		public RestDeviceService restDeviceService;
		
		@Autowired
		public RestRealayService restRealayService;
		
		@Autowired
		PushService pushService;
		
		@Autowired
		RestTimeLineService restTimeLineService;
		
		@Autowired
		RestAllocateService restAllocateService;
		
		@Autowired
		PayService payService;
*/
		
		@Autowired
		VehicleService vehicleService;
		
		@Autowired
		DeviceService deviceService;
		
		@Autowired
		DtcService dtcService;
		
		@Autowired
		VehicleDBService vehicleDBService;
		
		private static List<String> dtcPushMemory = new ArrayList<String>();
		
/*	
		@ApiOperation(value = "차량 진단 리스트 조회", notes = "차량 진단 리스트를 조회한다" , response = DTCVO.class , responseContainer = "List")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of DTC"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/vehicle/{vehicleKey}/dtc" , method=RequestMethod.GET)
		public ResponseEntity getVehicleDtcList(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="vehicleKey") 
				@ApiParam(value = "target vehicle")
				String vehicleKey
				,@RequestParam(value="limit" , required = false , defaultValue = "5")
				@ApiParam(value = "page per count")
				int limit
				,@RequestParam(value="offset" , required = false , defaultValue = "0")
				@ApiParam(value = "offset")
				int offset
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, VehicleNotFoundException 
		{
			
			TokenVO t = authentication(restTokenService,key);
			
			if(t == null) 
				throw new UnauthorizedException();
			
			UserVO user = restAccountService.getAccount(t.getUserKey());
			
			if(user == null) 
				throw new UserNotFoundException();
			
			String lang = user.getLang();
			
			VehicleVO vvo = null;
			if(user.getCorp() == 0)
				vvo = restVehicleService.getVehicle(t.getUserKey(),vehicleKey, lang);
			else vvo = restVehicleService.getVehicleInCorp(t.getUserKey(),vehicleKey, false, lang);
			
			if(vvo == null)
				throw new VehicleNotFoundException();
			
			List<Map<String,String>> rtvList = restDtcService.getDtcHeaderList(vehicleKey,limit,offset,lang);
			
			
			return createResponse(HttpStatus.OK, restTokenService,t,new String[]{"result"}, rtvList);
				
		}
		
		@ApiOperation(value = "차량 진단 리스트 조회", notes = "차량 진단 리스트를 조회한다" , response = DTCVO.class , responseContainer = "List")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of DTC"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/vehicle/{vehicleKey}/dtcLastest" , method=RequestMethod.GET)
		public ResponseEntity getVehicleDtcLastest(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="vehicleKey") 
				@ApiParam(value = "target vehicle")
				String vehicleKey
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, VehicleNotFoundException 
		{
			
			TokenVO t = authentication(restTokenService,key);
			
			if(t == null) 
				throw new UnauthorizedException();
			
			UserVO user = restAccountService.getAccount(t.getUserKey());
			
			if(user == null) 
				throw new UserNotFoundException();
			
			String lang = user.getLang();
			
			VehicleVO vvo = null;
			if(user.getCorp() == 0)
				vvo = restVehicleService.getVehicle(t.getUserKey(),vehicleKey, lang);
			else vvo = restVehicleService.getVehicleInCorp(t.getUserKey(),vehicleKey, false, lang);
			
			if(vvo == null)
				throw new VehicleNotFoundException();
			
			List<Map<String,String>> rtvList = restDtcService.getDtcHeaderList(vehicleKey,1,0,lang);
			
			Map<String,String> rtv = null;
			if(rtvList.size() > 0 ) rtv = rtvList.get(0);			
				
			
			return createResponse(HttpStatus.OK, restTokenService,t,new String[]{"result"}, rtv);
				
		}
		
		@ApiOperation(value = "차량 진단 상세 조회", notes = "차량 진단 상세 조회한다" , response = DTCVO.class , responseContainer = "List")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of DTC"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/vehicle/{vehicleKey}/dtc/{dtcKey}" , method=RequestMethod.GET)
		public ResponseEntity getVehicleDtcDetail(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="vehicleKey") 
				@ApiParam(value = "target vehicle")
				String vehicleKey
				,@PathVariable(value="dtcKey") 
				@ApiParam(value = "target dtc")
				String dtcKey
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, VehicleNotFoundException 
		{
			
			TokenVO t = authentication(restTokenService,key);
			
			if(t == null) 
				throw new UnauthorizedException();
			
			UserVO user = restAccountService.getAccount(t.getUserKey());
			
			if(user == null) 
				throw new UserNotFoundException();
			
			String lang = user.getLang();
			
			VehicleVO vvo = null;
			if(user.getCorp() == 0)
				vvo = restVehicleService.getVehicle(t.getUserKey(),vehicleKey, lang);
			else vvo = restVehicleService.getVehicleInCorp(t.getUserKey(),vehicleKey, false, lang);
			
			if(vvo == null)
				throw new VehicleNotFoundException();
			
			List<Map<String,String>> rtvList = restDtcService.getDtcList(vehicleKey,dtcKey,lang);
			
			
			return createResponse(HttpStatus.OK, restTokenService,t,new String[]{"result"}, rtvList);
				
		}
		
		@ApiOperation(value = "차량 진단 등록", notes = "차량 진단 정보를 등록한다." )		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/vehicle/{vehicleKey}/dtc" , method=RequestMethod.POST)
		public ResponseEntity createVehileDtc(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="vehicleKey") 
				@ApiParam(value = "target vehicleKey")
				String vehicleKey				
				,@RequestBody(required = true)
				DTCReqVO dtcVO
				) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException 
		{
			
			TokenVO t = authentication(restTokenService,key);
			
			if(t == null) 
				throw new UnauthorizedException();
			
			UserVO user = restAccountService.getAccount(t.getUserKey());
			
			if(user == null) 
				throw new UserNotFoundException();
			
			String lang = user.getLang();
			
			
			VehicleVO vehicle = null;
			if(user.getCorp() == 0)
				vehicle = restVehicleService.getVehicle(t.getUserKey(),vehicleKey, lang);
			else vehicle = restVehicleService.getVehicleInCorp(t.getUserKey(),vehicleKey, false, lang);
			
			if(vehicle == null) 
				throw new VehicleNotFoundException();
			
			
			if(StrUtil.isNullToEmpty(dtcVO.getPacket())){
				restDtcService.removeAction(vehicleKey);
				return createResponse(HttpStatus.CREATED);
			}
			
			RealayDtcVO dtc = restDtcService.parseDtcPacket(dtcVO.getPacket(),vehicle.getConvCode(),vehicle.getDtcManufacture()); 
			
			String dtcKey = restDtcService.saveRealayDtc(vehicle,dtc);
			
			List<Map<String,String>> rtvList = restDtcService.getDtcList(vehicleKey,dtcKey,lang);

			if(rtvList.size() > 0){
				String dtcCode = rtvList.get(0).get("dtcCode");
				if(!StrUtil.isNullToEmpty(dtcCode)){
					if(!dtcPushMemory.contains(dtcKey)){
						dtcPushMemory.add(dtcKey);
						
						String mailBoxAccount = "";
						if(user.getCorp()==0){
							mailBoxAccount = vehicle.getVehicleMaster();
						}else{
							mailBoxAccount = restAllocateService.getVehicleAllocateUser(vehicle.getVehicleKey());
							if(StrUtil.isNullToEmpty(mailBoxAccount)) mailBoxAccount = vehicle.getFixedUser();
							if(StrUtil.isNullToEmpty(mailBoxAccount)) mailBoxAccount = vehicle.getVehicleMaster();
						}
						
						String pushMsg = "%s 차량에서 고장이 발생하였습니다. 차량진단 목록을 확인해주세요.";
						pushMsg = String.format(pushMsg, vehicle.getPlateNum());
						
						//type maintenance
						pushService.pushChkAccept("3", vehicle.getVehicleMaster(), "", "고장 감지", pushMsg, vehicle.getCorp() , vehicle.getCorp() != 0 , mailBoxAccount);
						
						Map<String,String> param = new HashMap<String,String>();
						
						param.put("vehicleKey", vehicle.getVehicleKey());
						param.put("eventTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						param.put("eventValue", dtcKey);
						
						restTimeLineService.insertTimeLineEvent("trouble","", param);
					}
				}
			}
			
			return createResponse(HttpStatus.CREATED, restTokenService,t,new String[]{"result"}, rtvList);
				
		}
		
		@ApiOperation(value = "차량 진단 등록 - realy용", notes = "차량 진단 정보를 등록한다." )		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/device/{deviceId}/realyDtc" , method=RequestMethod.POST)
		public ResponseEntity createRealayDtc(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="deviceId") 
				@ApiParam(value = "target deviceId")
				String deviceId				
				,@RequestBody(required = true)
				RealayDtcVO dtcVO
				) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException 
		{
			
			UserVO corpVo = restRealayService.getApiCorp(key);
			
			int corp = corpVo.getCorp();
			
			String masterAccount = accountService.getMasterUserInCorp(corp);
			
			UserVO user = restAccountService.getAccount(masterAccount);
			
			if(user == null)
				throw new UserNotFoundException();
			
			DeviceVO device = restDeviceService.getActiveDeviceChkFromId(deviceId);
			if(device == null)
				throw new NotFoundException();
			
			VehicleVO vehicle = restVehicleService.getVehicleByActiveDevice(device);
			if(vehicle == null)
				throw new NotFoundException();
			
			if(!payService.chkPayment(device.getDeviceKey())) throw new UnauthorizedException();
			
			String dtcKey = restDtcService.saveRealayDtc(vehicle,dtcVO);
			 
			List<Map<String,String>> rtvList = restDtcService.getDtcList(vehicle.getVehicleKey(),dtcKey,"");
			
			if(rtvList.size() > 0){
				String dtcCode = rtvList.get(0).get("dtcCode");
				if(!StrUtil.isNullToEmpty(dtcCode)){
					if(!dtcPushMemory.contains(dtcKey)){
						
						String mailBoxAccount = "";
						if(user.getCorp()==0){
							mailBoxAccount = vehicle.getVehicleMaster();
						}else{
							mailBoxAccount = restAllocateService.getVehicleAllocateUser(vehicle.getVehicleKey());
							if(StrUtil.isNullToEmpty(mailBoxAccount)) mailBoxAccount = vehicle.getFixedUser();
							if(StrUtil.isNullToEmpty(mailBoxAccount)) mailBoxAccount = vehicle.getVehicleMaster();
						}
						
						dtcPushMemory.add(dtcKey);
						
						String pushMsg = "%s 차량에서 고장이 발생하였습니다. 차량진단 목록을 확인해주세요.";
						pushMsg = String.format(pushMsg, vehicle.getPlateNum());
						
						//type maintenance
						pushService.pushChkAccept("3", vehicle.getVehicleMaster(), "", "고장 감지", pushMsg, vehicle.getCorp() , vehicle.getCorp() != 0 , mailBoxAccount);
						
						Map<String,String> param = new HashMap<String,String>();
						
						param.put("vehicleKey", vehicle.getVehicleKey());
						param.put("eventTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						param.put("eventValue", dtcKey);
						
						restTimeLineService.insertTimeLineEvent("trouble","", param);
						
						
					}else{
						dtcPushMemory.remove(dtcKey);
					}
				}
			}
			
			return createResponse(HttpStatus.CREATED);
				
		}
		
		@ApiOperation(value = "차량 진단 정보를 삭제", notes = "차량 진단 정보를 삭제한다.")		
		@ApiResponses(value = {
				@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "success"),
		        @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "bad request"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/vehicle/{vehicleKey}/dtc/{dtcKey}" , method=RequestMethod.DELETE)
		public ResponseEntity deleteVehicleDtc(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="vehicleKey") 
				@ApiParam(value = "target vehicleKey")
				String vehicleKey
				,@PathVariable(value="dtcKey") 
				@ApiParam(value = "target dtc  arr - delimeter ',' ")
				String dtcKey
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, VehicleNotFoundException 
		{
			
			TokenVO t = authentication(restTokenService,key);
			
			if(t == null) 
				throw new UnauthorizedException();
			
			UserVO user = restAccountService.getAccount(t.getUserKey());
			
			if(user == null) 
				throw new UserNotFoundException();
			
			String lang = user.getLang();
			
			
			VehicleVO vehicle = null;
			if(user.getCorp() == 0)
				vehicle = restVehicleService.getVehicle(t.getUserKey(),vehicleKey, lang);
			else vehicle = restVehicleService.getVehicleInCorp(t.getUserKey(),vehicleKey, false, lang);
			
			if(vehicle == null) 
				throw new VehicleNotFoundException();
			
			for(String dKey : dtcKey.split(","))
				restDtcService.removeHstr(dKey);
			
			return createResponse(HttpStatus.OK);
				
		}
*/		
		
		
		@ApiOperation(value = "DTC packet parser", notes = "DTC packet parser" , response = String.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list manufacture"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/vehicleDb/{lang}/convCode/{convCode}/packet/{packet}" , method={RequestMethod.GET})
		public ResponseEntity getVehicleDbDtcCode(HttpServletRequest req,HttpServletResponse response
				
				,@PathVariable(value="lang") 
				@ApiParam(value = "decide return language")
				String lang
				,@PathVariable(value="convCode") 
				@ApiParam(value = "convCode")
				String convCode
				,@PathVariable(value="packet") 
				@ApiParam(value = "packet")
				String packet
				) throws CreateResponseException, UnauthorizedException, BizException, VehicleNotFoundException 
		{
			
			VehicleVO vehicle = null;
			
			Map<String,String> vehicleInfo = vehicleDBService.getVehicleDbInfo("en",convCode);
			
			if(vehicleInfo == null)
				throw new VehicleNotFoundException();
			
			String dtcManufacture = vehicleInfo.get("MANUFACTURE");
			
			if(!dtcManufacture.equals("HYUNDAI") && dtcManufacture.equals("KIA") && dtcManufacture.equals("SAMSUNG") && dtcManufacture.equals("SSANGYONG") && dtcManufacture.equals("CHEVROLET"))
				dtcManufacture = "GENERAL";
				
			
			DtcVO dtc = dtcService.parseDtcPacket(packet,convCode,dtcManufacture);
			
			if(dtc.getDtcCode().size()>0){
				for(int i = 0 ; i < dtc.getDtcCode().size() ; i++){
					String dtcCode = dtc.getDtcCode().get(i);
					Map<String,String> dtcDesc = dtcService.getDtcDesc(dtcCode,dtcManufacture,lang);
					dtc.getDtcDesc().add(dtcDesc);
				}
			}
			
			return createResponse(HttpStatus.OK, new String[]{"result"}, dtc);
				
		}
		
		@ApiOperation(value = "get dtc code description", notes = "get dtc code description" , response = String.class)				
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list manufacture"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		
		@RequestMapping(value="/api/vehicleDb/{lang}/convCode/{convCode}/dtcCode/{dtcCode}" , method={RequestMethod.GET})
		public ResponseEntity getVehicleDbDtcCode2(HttpServletRequest req,HttpServletResponse response
				
				,@PathVariable(value="lang") 
				@ApiParam(value = "decide return language")
				String lang
				,@PathVariable(value="convCode") 
				@ApiParam(value = "convCode")
				String convCode
				,@PathVariable(value="dtcCode") 
				@ApiParam(value = "dtcCode")
				String dtcCode
				) throws CreateResponseException, UnauthorizedException, BizException, VehicleNotFoundException, IllegalAccessException, InvocationTargetException 
		{
			
			VehicleVO vehicle = null;
			
			Map<String,String> vehicleInfo = vehicleDBService.getVehicleDbInfo("en",convCode);
			
			if(vehicleInfo == null)
				throw new VehicleNotFoundException();
			
			String dtcManufacture = vehicleInfo.get("MANUFACTURE");
			
			if(!dtcManufacture.equals("HYUNDAI") && dtcManufacture.equals("KIA") && dtcManufacture.equals("SAMSUNG") && dtcManufacture.equals("SSANGYONG") && dtcManufacture.equals("CHEVROLET"))
				dtcManufacture = "GENERAL";
			
			DtcDescriptionVO dtc = new DtcDescriptionVO();
			if(!"".equals(dtcCode)){
				Map<String,String> dtcDesc = dtcService.getDtcDesc(dtcCode,dtcManufacture,lang);
				BeanUtils.populate(dtc, dtcDesc);
			}
			
			return createResponse(HttpStatus.OK, new String[]{"result"}, dtc);
				
		}
		
		@ApiOperation(value = "dtc packet parser - from deviceId", notes = "dtc packet parser - from deviceId" , response = String.class)				
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list manufacture"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/vehicleDb/{lang}/deviceId/{deviceId}/packet/{packet}" , method={RequestMethod.GET})
		public ResponseEntity getVehicleDbDtcCodeFromDeviceId(HttpServletRequest req,HttpServletResponse response
				
				,@PathVariable(value="lang") 
				@ApiParam(value = "decide return language")
				String lang
				,@PathVariable(value="deviceId") 
				@ApiParam(value = "deviceId")
				String deviceId
				,@PathVariable(value="packet") 
				@ApiParam(value = "packet")
				String packet
				) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException 
		{

			DeviceVO dvo = deviceService.getActiveDeviceChkFromId(deviceId);
			
			if(dvo == null)
				throw new NotFoundException();
			
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("searchDeviceId", deviceId);
			VehicleVO vehicle = vehicleService.getVehicle(param, null);
			
			if(vehicle == null)
				throw new VehicleNotFoundException();
			
			DtcVO dtc = dtcService.parseDtcPacket(packet,vehicle.getConvCode(),vehicle.getDtcManufacture());
			
			if(dtc.getDtcCode().size()>0){
				for(int i = 0 ; i < dtc.getDtcCode().size() ; i++){
					String dtcCode = dtc.getDtcCode().get(i);
					Map<String,String> dtcDesc = dtcService.getDtcDesc(dtcCode,vehicle.getDtcManufacture(),lang);
					if(!vehicle.getDtcManufacture().equals("GENERAL") && dtcDesc == null) dtcDesc = dtcService.getDtcDesc(dtcCode,"GENERAL",lang);
					if(dtcDesc == null) {
						dtcDesc = new HashMap<String,String>();
						dtcDesc.put("manufacture", vehicle.getDtcManufacture());
						dtcDesc.put("code", dtcCode);
						dtcDesc.put("typeName", "제조사 특수 고장 코드");
						dtcDesc.put("descript", "제조사 특수 고장 코드");
					}
					dtc.getDtcDesc().add(dtcDesc);
				}
			}
			
			return createResponse(HttpStatus.OK, new String[]{"result"}, dtc);
				
		}
		
		
		
		
}
