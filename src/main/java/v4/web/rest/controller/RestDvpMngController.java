package v4.web.rest.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.jastecm.lora.msg.LoraMsgConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.common.Globals;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import org.jastecm.string.StrUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import v4.ex.exc.BizException;
import v4.ex.exc.CreateResponseException;
import v4.ex.exc.NotFoundException;
import v4.ex.exc.RestConnectException;
import v4.ex.exc.extents.UnauthorizedException;
import v4.ex.exc.extents.VehicleNotFoundException;
import v4.web.controller.BasicWebViewController;
import v4.web.rest.service.AdminCorpService;
import v4.web.rest.service.DeviceService;
import v4.web.rest.service.DownloadService;
import v4.web.rest.service.VehicleDBService;
import v4.web.rest.service.VehicleService;
import v4.web.rest.service.data.AccountDataService;
import v4.web.vo.CreateVehicleVO;
import v4.web.vo.DeviceVO;
import v4.web.vo.VehicleVO;
import v4.web.vo.data.AccountVO;


/*
 * @Class name : LoginController
 * @Description : 
 * @Modification Information
 * 
 * @author 
 * @since 
 * @version 
 * @see
 *
 */
@Api(value="restDvpMngController")
@RestController
public class RestDvpMngController extends BasicWebViewController{
		
		ObjectMapper mapper = new ObjectMapper();
		
		@Autowired
		VehicleService vehicleService;
		
		@Autowired
		DeviceService deviceService;

		@Autowired
		AdminCorpService adminCorpService;
		
		@Autowired
		DownloadService downloadService;
		
		@Autowired
		AccountDataService accountDataService;
		
		@Autowired
		VehicleDBService vehicleDbService;

		@ApiOperation(value = "dvpMng newVehicle", notes = "" )		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my vehicles"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/dvpMng/hello" , method=RequestMethod.GET)
		public ResponseEntity hello(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException 
		{
			
		    return new ResponseEntity(null, null, HttpStatus.OK);
			
		}
		
		@ApiOperation(value = "dvpMng newVehicle", notes = "" )		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my vehicles"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/dvpMng/initialize/{deviceSn}" , method=RequestMethod.GET)
		public ResponseEntity vehicleDb(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@PathVariable(value="deviceSn") 
				@ApiParam(value = "deviceSn")
				String deviceSn
				) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, UnsupportedEncodingException 
		{
			
			CreateVehicleVO vo = vehicleService.getTempVehicle(deviceSn);
			
			if(vo == null)
				throw new NotFoundException("can't find temp vehicle");
			
			
			String convCode = "";
			int totDistance = 0;
			String deviceSound = "";
			Map<Integer,byte[]> vehicleDb = null;
			Map<Integer,byte[]> firmware = null;
			
			DeviceVO dvo = deviceService.getDeviceFromSn(deviceSn);
			
			if(StrUtil.isNullToEmpty(vo.getVehicleKey())){
				//new
				
				convCode = vo.getConvCode();
				totDistance = (int)vo.getTotDistance();
				deviceSound = vo.getDeviceSound();
				
				firmware = downloadService.getFirmware(dvo); 
				vehicleDb =downloadService.getVehicleDb(dvo);
				
			}else {
				//re active
				Map<String,Object> param = new HashMap<String,Object>();
				param.put("searchVehicleKey", vo.getVehicleKey());
				param.put("searchCorpKey",dvo.getCorpKey());
				VehicleVO vvo = vehicleService.getVehicle(param, null);
				
				convCode = vvo.getConvCode();
				totDistance = (int)vvo.getTotDistance();
				deviceSound = vvo.getDeviceSound();
				
				dvo.setConvCode(convCode);
				
				firmware = downloadService.getFirmware(dvo); 
				vehicleDb =downloadService.getVehicleDb(dvo);
				
			}
			
			/////////////////////////
			
			byte[] dbNm = vehicleDb.get(0);
			String strDbNm = new String(dbNm,"UTF-8");
			byte[] firmNm = firmware.get(0);
			String strFirmNm = new String(firmNm,"UTF-8");
			
			vehicleService.updateLastestDbInfoTemp(deviceSn,strDbNm,strFirmNm,convCode,totDistance,deviceSound);
			
			MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		    params.set("Content-Type", MediaType.TEXT_PLAIN_VALUE + ";charset=utf-8");
			
		   
		    
		    Map<String,Object> body = new HashMap<String,Object>();
		    body.put("convCode", convCode);
		    body.put("totDistance", totDistance);
		    body.put("deviceSound", deviceSound);
		    body.put("vehicleDb", vehicleDb);
		    body.put("firmware", firmware);
		    
		    if(!StrUtil.isNullToEmpty(dvo.getBeaconMaj()) && !StrUtil.isNullToEmpty(dvo.getBeaconMin()) ){
		    	body.put("ibeaconMajor", dvo.getBeaconMaj());
		    	body.put("ibeaconMinor", dvo.getBeaconMin());
		    }
		    
		    String strBody;
		    try {
				strBody = mapper.writeValueAsString(body);
			} catch (JsonGenerationException e) {
				e.printStackTrace();
				throw new CreateResponseException(e.getMessage());
			} catch (JsonMappingException e) {
				e.printStackTrace();
				throw new CreateResponseException(e.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
				throw new CreateResponseException(e.getMessage());
			}
			
		    return new ResponseEntity(strBody, params, HttpStatus.OK);
			
		}
		
		@ApiOperation(value = "dvpMng updateVehicle", notes = "" )		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my vehicles"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/dvpMng/initialize/{deviceSn}/{convCode}" , method=RequestMethod.GET)
		public ResponseEntity getFirmwareNdb(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@PathVariable(value="deviceSn") 
				@ApiParam(value = "deviceSn")
				String deviceSn
				,@PathVariable(value="convCode") 
				@ApiParam(value = "convCode")
				String convCode
				) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, UnsupportedEncodingException 
		{
			
			DeviceVO dvo = deviceService.getDeviceFromSn(deviceSn);
			
			Map<Integer,byte[]> vehicleDb = null;
			Map<Integer,byte[]> firmware = null;
			
			firmware = downloadService.getFirmware(dvo); 
			vehicleDb =downloadService.getVehicleDb(dvo);
			
			/*
			byte[] dbNm = vehicleDb.get(0);
			String strDbNm = new String(dbNm,"UTF-8");
			byte[] firmNm = firmware.get(0);
			String strFirmNm = new String(firmNm,"UTF-8");
			
			vehicleService.updateLastestDbInfoTemp(deviceSn,strDbNm,strFirmNm,convCode,0,null);
			*/
			
			MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		    params.set("Content-Type", MediaType.TEXT_PLAIN_VALUE + ";charset=utf-8");
			
		    Map<String,Object> body = new HashMap<String,Object>();
		    body.put("vehicleDb", vehicleDb);
		    body.put("firmware", firmware);
		    
		    String strBody;
		    try {
				strBody = mapper.writeValueAsString(body);
			} catch (JsonGenerationException e) {
				e.printStackTrace();
				throw new CreateResponseException(e.getMessage());
			} catch (JsonMappingException e) {
				e.printStackTrace();
				throw new CreateResponseException(e.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
				throw new CreateResponseException(e.getMessage());
			}
			
		    return new ResponseEntity(strBody, params, HttpStatus.OK);
			
		}
		
		@ApiOperation(value = "dvp mng new vehicle confirm", notes = "" )		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my vehicles"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/dvpMng/confirm/{deviceSn}/{connectionKey}" , method=RequestMethod.GET)
		public ResponseEntity confirm(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@PathVariable(value="deviceSn") 
				@ApiParam(value = "deviceSn")
				String deviceSn
				,@PathVariable(value="connectionKey") 
				@ApiParam(value = "connectionKey")
				String connectionKey
				) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, RestConnectException 
		{
		
			/*
			DeviceVO dvo = deviceService.getDeviceFromSn(deviceSn);
			
			CreateVehicleVO vo = vehicleService.getTempVehicle(deviceSn);
			
			if(vo == null)
				throw new NotFoundException("can't find temp vehicle");
			
			vo.setConnectionKey(connectionKey);
			
			AccountVO avo = accountDataService.getAccountFromKey(vo.getRegUser(),true);
			
			if(avo == null)
				throw new NotFoundException("can't find temp vehicle account");
			
			
			Map<String,Object> rtv = vehicleService.createVehicle(avo,vo);
			
			try {
				
				String deviceId = dvo.getDeviceId();
				LoraMsgConnector connector = new LoraMsgConnector(dvo.getAppEui(), Globals.LORA_KEYS.get(dvo.getAppEui()));
				Map<String, String> res = connector.loraDeviceChk(deviceId, Globals.LORA_SUBSCRIVE_NM);
				
				if(res.get(LoraMsgConnector.RESPONSE_CODE).equals("200")){
					System.out.println("already subscribe ON!");
				}else{
					connector.loraDeviceSubOn(deviceId, Globals.LORA_SUBSCRIVE_NM , Globals.LORA_SUBSCRIVE);
					if(res.get(LoraMsgConnector.RESPONSE_CODE).equals("201")){
						System.out.println("subscribe ON! Success");
					}
				}
				
				 kcc tre Off
				if(adminCorp == 1){ //kcc only
					
					viewcar.pro.vo.VehicleVO vvo = new viewcar.pro.vo.VehicleVO();
					vvo.setVehicleKey(vo.getVehicleKey());
					vvo = vehicleService.getVehicle(vvo);
					
					String payload = mapper.writeValueAsString(vvo);
					System.out.println(payload);
					JSONObject obj = new JSONObject(payload);
					Map<String,String> vehicleInfo = restVehicleDbService.getVehicleDbInfo("ko", vvo.getConvCode());
					String vehicleInfoPayload = mapper.writeValueAsString(vehicleInfo);
					System.out.println(vehicleInfoPayload); 
					JSONObject objVehicleDbInfo = new JSONObject(vehicleInfoPayload);
					obj.put("vehicleDbInfo", objVehicleDbInfo);
					
					realayConnectService.sendMsgAsync(vo.getAccountKey(), key, deviceSn, obj.toString(), "treOn",Globals.MODE);
				}
				 
				
			}catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
			//TODO accountService.destroyPcmng(uvo);
			
			MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		    params.set("Content-Type", MediaType.TEXT_PLAIN_VALUE + ";charset=utf-8");
			
		    return new ResponseEntity((String)rtv.get("msg"), params, (HttpStatus)rtv.get("code"));
			*/
			return null;
				
		}		
		
		
		
		@ApiOperation(value = "dvp mng update confirm", notes = "" )		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my vehicles"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/dvpMng/confirm/{deviceSn}/{connectionKey}/{convCode}/{totDistance}/{deviceSound}/{key}" , method=RequestMethod.GET)
		public ResponseEntity confirmUpdate(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@PathVariable(value="deviceSn") 
				@ApiParam(value = "deviceSn")
				String deviceSn
				,@PathVariable(value="connectionKey") 
				@ApiParam(value = "connectionKey")
				String connectionKey
				,@PathVariable(value="convCode") 
				@ApiParam(value = "convCode")
				String convCode
				,@PathVariable(value="totDistance") 
				@ApiParam(value = "totDistance")
				String totDistance
				,@PathVariable(value="deviceSound") 
				@ApiParam(value = "deviceSound")
				String deviceSound
				,@PathVariable(value="key") 
				@ApiParam(value = "key")
				String key
				) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, RestConnectException 
		{
			String accountKey = key;
			DeviceVO device = deviceService.getDeviceFromSn(deviceSn);
				
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("searchDeviceId", device.getDeviceId());
			param.put("searchCorpKey",device.getCorpKey());
			VehicleVO vehicle = vehicleService.getVehicle(param, null);
			
			HttpStatus rtv = HttpStatus.OK;
			
			if(vehicle == null)
				rtv = HttpStatus.NOT_FOUND;
			
			vehicleService.updateVehicle(vehicle.getVehicleKey(),convCode,totDistance,deviceSound);
			deviceService.updateConnectionKey(vehicle.getDeviceId(),connectionKey);
			
			//TODO accountService.destroyPcmng(uvo);
			
			//TODO update device
			//vo.getDownloadConvCode()
			//vo.getDownloadDb() //getVehicleDbVer
			//vo.getDownloadDeviceSound() 
			//vo.getDownloadFirm() //getFirmVer
			//vo.getDownloadTotDistance()
			
			MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		    params.set("Content-Type", MediaType.TEXT_PLAIN_VALUE + ";charset=utf-8");
			
		    return new ResponseEntity(null, params, rtv);
		}
		
		@ApiOperation(value = "dvp mng fire event", notes = "" )		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my vehicles"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/dvpMng/fire/{key}" , method=RequestMethod.GET)
		public ResponseEntity fire(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@PathVariable(value="key") 
				@ApiParam(value = "key")
				String key
				) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, RestConnectException 
		{
			String accountKey = key;
			
			//TODO accountService.destroyPcmng(uvo);
			
			MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		    params.set("Content-Type", MediaType.TEXT_PLAIN_VALUE + ";charset=utf-8");
			
		    return new ResponseEntity(null, params, HttpStatus.OK);
		}
		
		/////////////////////////
		
		@ApiOperation(value = "update app - get vehicleInfo", notes = "update app - get vehicleInfo" , response = DeviceVO.class , responseContainer = "List")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my device"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/dvpMng/device/{deviceSn}/vehicle" , method=RequestMethod.GET)
		public ResponseEntity getDeviceVehicle(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@PathVariable(value="deviceSn") 
				@ApiParam(value = "device S/N")
				String deviceSn
				) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException 
		{
				
			DeviceVO device = deviceService.getDeviceFromSn(deviceSn);
			
			if(device == null)
				throw new NotFoundException("not active or found device");
			
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("searchDeviceId", device.getDeviceId());
			param.put("searchCorpKey",device.getCorpKey());
			VehicleVO vehicle = vehicleService.getVehicle(param, null);
			
			if(vehicle == null)
				throw new VehicleNotFoundException();
			
			return createResponse(HttpStatus.OK, new String[]{"result"}, vehicle);
				
		}
		
		@ApiOperation(value = "update app - get vehicleSpec", notes = "update app - get vehicleSpec" , response = DeviceVO.class , responseContainer = "List")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my device"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/vehicleDb/device/{deviceSn}" , method=RequestMethod.GET)
		public ResponseEntity getDeviceVehicleDb(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@PathVariable(value="deviceSn") 
				@ApiParam(value = "device S/N")
				String deviceSn
				) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException 
		{
				
			
			DeviceVO device = deviceService.getDeviceFromSn(deviceSn);
			
			if(device == null)
				throw new NotFoundException("not active or found device");
			
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("searchDeviceId", device.getDeviceId());
			param.put("searchCorpKey",device.getCorpKey());
			VehicleVO vehicle = vehicleService.getVehicle(param, null);
			
			if(vehicle == null)
				throw new VehicleNotFoundException();
			
			String convCode = vehicle.getConvCode();
			
			Map<String,String> rtvList = vehicleDbService.getVehicleDbInfo("en",convCode);
			
			return createResponse(HttpStatus.OK, new String[]{"result"}, rtvList);
				
		}
		
		
}
