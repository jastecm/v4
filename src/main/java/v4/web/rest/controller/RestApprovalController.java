package v4.web.rest.controller;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import v4.ex.exc.BizException;
import v4.ex.exc.CreateResponseException;
import v4.ex.exc.extents.UnauthorizedException;
import v4.ex.exc.extents.UserNotFoundException;
import v4.web.controller.BasicWebViewController;
import v4.web.rest.service.AccountService;
import v4.web.rest.service.CorpService;
import v4.web.rest.service.TokenService;
import v4.web.vo.AccountVO;
import v4.web.vo.ApprovalConfigVO;
import v4.web.vo.CreateApprovalConfigVO;
import v4.web.vo.GroupVO;
import v4.web.vo.TokenVO;
import v4.web.vo.UpdateGroupVO;


/*@Api(value="restApprovalController")
@RestController
*/
public class RestApprovalController extends BasicWebViewController {
	
	@Autowired
	public TokenService tokenService;
	
	@Autowired
	public AccountService accountService;
	
	@Autowired
	CorpService corpService;
	
	@ApiOperation(value = "결재관리 생성", notes = "결재를 생성 관리 합니다.")		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create complate"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/approvalConfig" , method=RequestMethod.POST)
	public ResponseEntity approvalConfig(HttpServletRequest req,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = true)
			@ApiParam(value = "key")
			String key
			,@RequestBody(required = true)
			@Valid
			CreateApprovalConfigVO createApprovalConfigVo
			,BindingResult br
			) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
	{
		
		if(br.hasErrors())
			return createValidErrorResponse(response,new String[]{"result"}, br ,null);
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
			return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		
		if(account == null)
			throw new UserNotFoundException();
		//여기까지 거의공통//
		
		createApprovalConfigVo.setCorpKey(account.getCorpKey());
		corpService.updateApprovalConfig(createApprovalConfigVo);
					
		
		return createResponse(HttpStatus.OK);
			
	}
	
	
	@ApiOperation(value = "결재관리 정보 조회", notes = "결재관리 정보를 조회 합니다.",response = ApprovalConfigVO.class)		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create complate"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/approvalConfig" , method=RequestMethod.GET)
	public ResponseEntity getApprovalConfig(HttpServletRequest req,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = true)
			@ApiParam(value = "key")
			String key
			) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
			return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		
		if(account == null)
			throw new UserNotFoundException();
		//여기까지 거의공통//
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("corpKey", account.getCorpKey());
		
		ApprovalConfigVO vo = corpService.getApprovalConfig(param);
		
		return createResponse(HttpStatus.OK,new String[]{"result"}, vo);
			
	}

}
