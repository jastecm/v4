package v4.web.rest.controller;

import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.common.Globals;
import org.jastecm.json.JSONException;
import org.jastecm.string.StrUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import v4.ex.exc.BizException;
import v4.ex.exc.CreateResponseException;
import v4.ex.exc.NotFoundException;
import v4.ex.exc.RestConnectException;
import v4.ex.exc.extents.UnauthorizedException;
import v4.ex.exc.extents.UserNotFoundException;
import v4.web.controller.BasicWebViewController;
import v4.web.rest.service.AccountService;
import v4.web.rest.service.CommonService;
import v4.web.rest.service.CorpService;
import v4.web.rest.service.TokenService;
import v4.web.vo.AccountVO;
import v4.web.vo.FileVO;
import v4.web.vo.TokenVO;


@Api(value="restFileController")
@RestController
public class RestFileController extends BasicWebViewController {
	
	@Autowired
	public AccountService accountService;

	@Autowired
	public CorpService corpService;
	
	@Autowired
	public TokenService tokenService;
	
	@Autowired
	public CommonService commonService;
	
	@ApiOperation(value = "파일 업로드", notes = "파일을 업로드 한다." , response = String.class)		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_FORBIDDEN, message = "Forbidden"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/fileUpload" , method=RequestMethod.POST)
	public ResponseEntity createVehileTemp(HttpServletRequest req,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			) throws CreateResponseException, UnauthorizedException, BizException, RestConnectException, NotFoundException, JSONException 
	{

		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
			return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		
		if(account == null)
			throw new UserNotFoundException();
		
		
		try {
			
			HashMap map = new HashMap();
			
			//utf-8면 실질적으로jsp에서는 화면이 깨짐 서블릿에서는 euc-kr 로 생성
			response.setCharacterEncoding("euc-kr");
			boolean file_result;
			File f_midir = new File(Globals.FILE_PATH+System.getProperty("file.separator"));
			file_result =  f_midir.mkdirs();
			
			//----------------------------파일 저장에 필요한 객체 생성 부분------------------------------------
			//디스크 파일 아이템 factory
			DiskFileItemFactory  factory = new DiskFileItemFactory();
			//업로드시 사용할 임시 메모리
			factory.setSizeThreshold(4096);
			//업로드시 사용할 temp path
			factory.setRepository(f_midir);
			ServletFileUpload upload = new ServletFileUpload(factory);
			//사이즈 제한
			upload.setSizeMax(Integer.parseInt(Globals.FILE_SIZE));
			//멀티파트로 넘어온지 여부
			boolean isMultipart = ServletFileUpload.isMultipartContent(req);
			//이 시점에서 temp path에 파일이 업로드 (파일뿐만아니라 필드값들도 일단 파일형태로 저장된다.)
			List items = upload.parseRequest(req);

			String uploadType = "";
			Iterator iter = items.iterator();
			while(iter.hasNext()){
				FileItem item = (FileItem)iter.next();
				if(item.isFormField()){
					String formKey = item.getFieldName();    					
					String value = item.getString("utf-8");
					if(formKey.equals("uploadType")){
						uploadType = value;
						break;
					}
				}
			}
			
			if(StrUtil.isNullToEmpty(uploadType)) throw new Exception("upload type is undefinded");
			
			File rtvDir = new File(f_midir.getPath()+System.getProperty("file.separator")+uploadType);
			rtvDir.mkdirs();
			
			//업로드된 파일 파싱
			iter = items.iterator();
			map = commonService.getFileUpload(iter,rtvDir,t.getUserKey());
			
			
			return createResponse(HttpStatus.CREATED, new String[]{"result"}, map);
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return createResponse(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@ApiOperation(value = "이미지 다운", notes = "이미지 다운를 다운로드한다" , response = File.class)		
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns imgFile"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/{imgUuid}/{key}/imgDown" , method=RequestMethod.GET)
	public ResponseEntity imgDown(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@PathVariable(value="imgUuid") 
			@ApiParam(value = "imgUuid")
			String imgUuid
			,@PathVariable(value="key")
			@ApiParam(value = "key")
			String key			
			) throws Exception 
	{
		
		TokenVO t = authentication(tokenService,key);
		
		if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
			return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		
		if(account == null)
			throw new UserNotFoundException();
		
		FileVO file = commonService.getFile(imgUuid);
		if(file == null){
			return createResponse(HttpStatus.NOT_FOUND);
		}else{
			String imgPath = "";
			imgPath = file.getFilePath()+System.getProperty("file.separator")+file.getSaveFileNm();
			
			try {		 
				File img = new File(imgPath);

				InputStreamResource resource = new InputStreamResource(new FileInputStream(img));

				return ResponseEntity.ok() 
						.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + URLEncoder.encode(file.getOrgFileNm()))
						.contentType(MediaType.parseMediaType("application/octet-stream")).contentLength(img.length())
						.body(resource);

			} catch (Exception e) {
				return createResponse(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
			
	}
}
