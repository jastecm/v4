package v4.web.rest.controller;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javassist.NotFoundException;
import v4.ex.exc.BizException;
import v4.ex.exc.CreateResponseException;
import v4.ex.exc.extents.UnauthorizedException;
import v4.ex.exc.extents.UserNotFoundException;
import v4.ex.exc.extents.VehicleNotFoundException;
import v4.web.controller.BasicWebViewController;
import v4.web.rest.service.AccountService;
import v4.web.rest.service.CorpService;
import v4.web.rest.service.GroupService;
import v4.web.rest.service.TokenService;
import v4.web.rest.service.WorkingAreaTimeService;
import v4.web.vo.AccountVO;
import v4.web.vo.CreateWorkingAreaVO;
import v4.web.vo.CreateWorkingTimeVO;
import v4.web.vo.TokenVO;
import v4.web.vo.UpdateWorkingAreaVO;
import v4.web.vo.UpdateWorkingTimeVO;
import v4.web.vo.WorkingTimeAreaVO;

@Api(value="restWorkingAreaTimeController")
@RestController
public class RestWorkingAreaTimeController extends BasicWebViewController {
	
	@Autowired
	private TokenService tokenService;

	@Autowired
	private AccountService accountService;
	
	@Autowired
	private CorpService corpService;
	
	@Autowired
	private GroupService groupService;
	
	@Autowired
    private WorkingAreaTimeService workingAreaTimeService;
	
	
	@ApiOperation(value = "이수 지역 리스트", notes = "이수 지역 리스트를 조회한다. " , response = WorkingTimeAreaVO.class, responseContainer = "List")
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns be can allocate vehicle list"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/workingArea" , method=RequestMethod.GET)
	public ResponseEntity getWorkingAreaList(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key				
			,@RequestParam(value="limit" , required = false , defaultValue = "5")
			@ApiParam(value = "limit per page")
			int limit
			,@RequestParam(value="offset" , required = false , defaultValue = "0")
			@ApiParam(value = "offset")
			int offset
			,@RequestParam(value="searchName" , required = false)
			@ApiParam(value = "searchName")
			String searchName
			,@RequestParam(value="cityLev1" , required = false)
			@ApiParam(value = "cityLev1")
			String cityLev1
			,@RequestParam(value="cityLev2" , required = false)
			@ApiParam(value = "cityLev2")
			String cityLev2
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, VehicleNotFoundException, UserNotFoundException
	{
		/*******인증공통********/
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		
		if(account == null)
			throw new UserNotFoundException();
		/*******인증공통********/
		
		
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("limit", String.valueOf(limit));
		param.put("offset", String.valueOf(offset));
		param.put("searchName", searchName);
		param.put("cityLev1", cityLev1);
		param.put("cityLev2", cityLev2);
		
		List<WorkingTimeAreaVO> rtvList = workingAreaTimeService.getWorkingArea(account,param);
		
		return createResponse(HttpStatus.OK, new String[]{"result"}, rtvList);
	}
	
	@ApiOperation(value = "이수 지역 등록", notes = "이수 지역 등록한다" , response = WorkingTimeAreaVO.class)
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "Create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_ACCEPTED, message = "Accepted"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/workingArea" , method=RequestMethod.POST)
	public ResponseEntity createWorkingArea(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key				
			,@RequestBody(required = true)
			@Valid
			CreateWorkingAreaVO vo,BindingResult br
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, VehicleNotFoundException, UserNotFoundException
	{
		
		if(br.hasErrors())
			return createValidErrorResponse(response,new String[]{"result"}, br ,null);
		
		
		/*******유저 인증공통(체크로직)********/
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		String lang = account.getLang();
		
		if(account == null)
			throw new UserNotFoundException();
		/*******유저 인증공통(체크로직)********/
		
		
		workingAreaTimeService.addWorkingArea(account.getCorpKey(),vo.getGroupkey(),vo.getAccountKey(),vo.getCityLev1(),vo.getCityLev2());
		
		
		return createResponse(HttpStatus.CREATED, new String[]{"result"}, "");
	}
	
	@ApiOperation(value = "이수 지역 삭제", notes = "이수 지역을 삭제한다")
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "Create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_ACCEPTED, message = "Accepted"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/workingArea/{workAreaKey}" , method=RequestMethod.DELETE)
	public ResponseEntity deleteWorkingArea(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key		
			,@PathVariable(value="workAreaKey") 
			@ApiParam(value = "target workAreaKey")
			String workAreaKey
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, VehicleNotFoundException, UserNotFoundException
	{
		
		/*******유저 인증공통(체크로직)********/
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		String lang = account.getLang();
		
		if(account == null)
			throw new UserNotFoundException();
		/*******유저 인증공통(체크로직)********/
		
		workingAreaTimeService.deleteWorkingArea(account,workAreaKey);
		
		return createResponse(HttpStatus.OK);
	}
	
	@ApiOperation(value = "이수 지역 수정", notes = "이수지역을 수정한다")
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "Create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_ACCEPTED, message = "Accepted"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/workingArea/{workAreaKey}" , method=RequestMethod.PUT)
	public ResponseEntity updateWorkingArea(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@PathVariable(value="workAreaKey") 
			@ApiParam(value = "target workAreaKey")
			String workAreaKey
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@RequestBody(required = true)
			@Valid
			UpdateWorkingAreaVO vo
			,BindingResult br
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, VehicleNotFoundException, UserNotFoundException
	{
		
		if(br.hasErrors())
			return createValidErrorResponse(response,new String[]{"result"}, br ,null);
		
		
		/*******유저 인증공통(체크로직)********/
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		String lang = account.getLang();
		
		if(account == null)
			throw new UserNotFoundException();
		/*******유저 인증공통(체크로직)********/
		
		workingAreaTimeService.updateWorkingArea(account.getCorpKey(),workAreaKey,vo.getGroupKey(),vo.getAccountKey(),vo.getCityLev1(),vo.getCityLev2());
		
		
		return createResponse(HttpStatus.OK);
	}
	
	@ApiOperation(value = "업무시간 등록", notes = "업무시간 등록한다" , response = WorkingTimeAreaVO.class)
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "Create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_ACCEPTED, message = "Accepted"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/workingTime" , method=RequestMethod.POST)
	public ResponseEntity createWorkingTime(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key				
			,@RequestBody(required = true)
			@Valid
			CreateWorkingTimeVO vo,BindingResult br
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, VehicleNotFoundException, UserNotFoundException
	{
		
		if(br.hasErrors())
			return createValidErrorResponse(response,new String[]{"result"}, br ,null);
		
		
		/*******유저 인증공통(체크로직)********/
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		String lang = account.getLang();
		
		if(account == null)
			throw new UserNotFoundException();
		/*******유저 인증공통(체크로직)********/
		
		
		workingAreaTimeService.addWorkingTime(account.getCorpKey(),vo.getGroupKey(),vo.getAccountKey(),vo.getStartH(),vo.getStartM(),vo.getEndH(),vo.getEndM(),vo.getDay());
		
		
		return createResponse(HttpStatus.CREATED, new String[]{"result"}, "");
	}
	
	@ApiOperation(value = "업무 시간 수정", notes = "업무 시간을 수정한다")
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "Create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_ACCEPTED, message = "Accepted"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/workingTime/{workTimeKey}" , method=RequestMethod.PUT)
	public ResponseEntity updateMainteance(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@PathVariable(value="workTimeKey") 
			@ApiParam(value = "target workTimeKey")
			String workTimeKey
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@RequestBody(required = true)
			@Valid
			UpdateWorkingTimeVO vo
			,BindingResult br
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, VehicleNotFoundException, UserNotFoundException
	{
		
		if(br.hasErrors())
			return createValidErrorResponse(response,new String[]{"result"}, br ,null);
		
		
		/*******유저 인증공통(체크로직)********/
		TokenVO t = authentication(tokenService,key);
		
		if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
			return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		String lang = account.getLang();
		
		if(account == null)
			throw new UserNotFoundException();
		/*******유저 인증공통(체크로직)********/
		
		workingAreaTimeService.updateWorkingTime(account.getCorpKey(),
				workTimeKey
				,vo.getGroupKey()
				,vo.getAccountKey()
				,vo.getStartH()
				,vo.getStartM()
				,vo.getEndH()
				,vo.getEndM()
				,vo.getDay());
		
		
		return createResponse(HttpStatus.OK);
	}
	
	@ApiOperation(value = "업무 시간 리스트", notes = "업무 시간 리스트를 조회한다. " , response = WorkingTimeAreaVO.class, responseContainer = "List")
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns be can allocate vehicle list"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/workingTime" , method=RequestMethod.GET)
	public ResponseEntity getWorkingTimeList(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key				
			,@RequestParam(value="limit" , required = false , defaultValue = "10")
			@ApiParam(value = "limit per page")
			int limit
			,@RequestParam(value="offset" , required = false , defaultValue = "0")
			@ApiParam(value = "offset")
			int offset
			,@RequestParam(value="searchType" , required = false, defaultValue = "")
			@ApiParam(value = "searchType")
			String searchType
			,@RequestParam(value="searchText" , required = false, defaultValue = "")
			@ApiParam(value = "searchText")
			String searchText
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, VehicleNotFoundException, UserNotFoundException
	{
		/*******인증공통********/
		TokenVO t = authentication(tokenService,key);
		
		if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
			return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		
		if(account == null)
			throw new UserNotFoundException();
		/*******인증공통********/
		
		
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("corpKey", account.getCorpKey());
		param.put("limit", limit);
		param.put("offset", offset);
		param.put("searchType", searchType);
		param.put("searchText", searchText);
		
		List<WorkingTimeAreaVO> rtvList = workingAreaTimeService.getWorkingTime(param);
		int cnt = 0;
		if(offset == 0){
			cnt = workingAreaTimeService.getWorkingTimeCnt(param,null);
		}
		
		String[] rtvField = offset==0?new String[]{"result","resultCnt"}:new String[]{"result"};
		Object[] rtvData = offset==0?new Object[]{rtvList,cnt}:new Object[]{rtvList};
		
		return createResponse(HttpStatus.OK, rtvField, rtvData);
	}
	
	@ApiOperation(value = "업무 시간 리스트 전체갯수", notes = "업무 시간 리스트 전체갯수를 조회한다. " , response = WorkingTimeAreaVO.class, responseContainer = "List")
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns be can allocate vehicle list"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/workingTimeTotalCnt" , method=RequestMethod.GET)
	public ResponseEntity getWorkingTimeListTotalCnt(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key				
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, VehicleNotFoundException, UserNotFoundException
	{
		/*******인증공통********/
		TokenVO t = authentication(tokenService,key);
		
		if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
			return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		
		if(account == null)
			throw new UserNotFoundException();
		/*******인증공통********/
		
		
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("corpKey", account.getCorpKey());

		int cnt = workingAreaTimeService.getWorkingTimeTotalCnt(param);

		
		return createResponse(HttpStatus.OK, new String[]{"result"}, cnt);
	}
	
	@ApiOperation(value = "업무 시간 삭제", notes = "업무 시간을 삭제한다")
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "Create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_ACCEPTED, message = "Accepted"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/workingTime/{workTimeKey}" , method=RequestMethod.DELETE)
	public ResponseEntity deleteWorkingTime(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key		
			,@PathVariable(value="workTimeKey") 
			@ApiParam(value = "target workTimeKey")
			String workTimeKey
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, VehicleNotFoundException, UserNotFoundException
	{
		
		/*******유저 인증공통(체크로직)********/
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		String lang = account.getLang();
		
		if(account == null)
			throw new UserNotFoundException();
		/*******유저 인증공통(체크로직)********/

		workingAreaTimeService.deleteWorkingTime(account,workTimeKey);
		
		return createResponse(HttpStatus.OK);
	}
}
