package v4.web.rest.controller;

import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.jastecm.string.DateUtil;
import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javassist.NotFoundException;
import v4.ex.exc.BizException;
import v4.ex.exc.CreateResponseException;
import v4.ex.exc.extents.UnauthorizedException;
import v4.ex.exc.extents.UserNotFoundException;
import v4.ex.exc.extents.VehicleNotFoundException;
import v4.web.controller.BasicWebViewController;
import v4.web.rest.service.AccountService;
import v4.web.rest.service.ClinicService;
import v4.web.rest.service.CorpService;
import v4.web.rest.service.GroupService;
import v4.web.rest.service.TokenService;
import v4.web.rest.service.VehicleService;
import v4.web.vo.AccountVO;
import v4.web.vo.ClinicVO;
import v4.web.vo.CorpVO;
import v4.web.vo.CreateClinicVO;
import v4.web.vo.CreateMaintenanceCompanyVO;
import v4.web.vo.CreateVehiclePartsEtcVO;
import v4.web.vo.GroupVO;
import v4.web.vo.MaintenanceCompanyVO;
import v4.web.vo.TokenVO;
import v4.web.vo.UpdateClinicVO;
import v4.web.vo.VehiclePartsEtcVO;
import v4.web.vo.VehiclePartsVO;
import v4.web.vo.VehicleVO;

//@Api(value="restAllocateController")
//@RestController
public class RestClinicController extends BasicWebViewController {
	/*
	@Autowired
	public TokenService tokenService;
	
	@Autowired
	public AccountService accountService;
	
	@Autowired
	public VehicleService vehicleService;
	
	@Autowired
	CorpService corpService;
	
	@Autowired
	GroupService groupService;
	
	@Autowired
	ClinicService clinicService;
	
	@ApiOperation(value = "정비 부품 리스트", notes = "정비 부품 리스트를 조회한다. " , response = VehiclePartsVO.class, responseContainer = "List")
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns be can allocate vehicle list"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/vehicleParts" , method=RequestMethod.GET)
	public ResponseEntity getvehiclePartsList(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key				
			,@RequestParam(value="limit" , required = false , defaultValue = "5")
			@ApiParam(value = "limit per page")
			int limit
			,@RequestParam(value="offset" , required = false , defaultValue = "0")
			@ApiParam(value = "offset")
			int offset
			,@RequestParam(value="partsType" , required = false)
			@ApiParam(value = "partsType")
			String partsType
			,@RequestParam(value="itemFlag" , required = false)
			@ApiParam(value = "itemFlag")
			String itemFlag
			,@RequestParam(value="partsNm" , required = false)
			@ApiParam(value = "partsNm")
			String partsNm
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, VehicleNotFoundException, UserNotFoundException
	{
		
		*//*******인증공통********//*
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		
		if(account == null)
			throw new UserNotFoundException();
		*//*******인증공통********//*
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("limit", limit);
		param.put("offset", offset);
		param.put("partsType", partsType);
		param.put("itemFlag", itemFlag);
		param.put("partsNm", partsNm);
		
		List<VehiclePartsVO> list = clinicService.getVehiclePartsList(param);
		
		return createResponse(HttpStatus.OK, new String[]{"result"}, list);
	}
	
	@ApiOperation(value = "정비 부품 타입 리스트", notes = "정비 부품 타입 리스트를 조회한다. " , responseContainer = "List")
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns be can allocate vehicle list"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/vehiclePartType" , method=RequestMethod.GET)
	public ResponseEntity getvehiclePartTypeList(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key							
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, VehicleNotFoundException, UserNotFoundException
	{
		
		*//*******인증공통********//*
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		
		if(account == null)
			throw new UserNotFoundException();
		*//*******인증공통********//*
		
		
		List<Map<String,Object>> list = clinicService.getVehiclePartTypeList();
		
		return createResponse(HttpStatus.OK, new String[]{"result"}, list);
	}
	
	
	@ApiOperation(value = "일반 정비 부품 리스트", notes = "일반 정비 부품 리스트를 조회한다. " , response = VehiclePartsEtcVO.class, responseContainer = "List")
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns be can allocate vehicle list"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/vehiclePartsEtc" , method=RequestMethod.GET)
	public ResponseEntity getvehiclePartsEtcList(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key				
			,@RequestParam(value="limit" , required = false , defaultValue = "5")
			@ApiParam(value = "limit per page")
			int limit
			,@RequestParam(value="offset" , required = false , defaultValue = "0")
			@ApiParam(value = "offset")
			int offset
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, VehicleNotFoundException, UserNotFoundException
	{
		
		*//*******인증공통********//*
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		
		if(account == null)
			throw new UserNotFoundException();
		*//*******인증공통********//*
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("limit", String.valueOf(limit));
		param.put("offset", String.valueOf(offset));
		
		List<VehiclePartsEtcVO> list = clinicService.getVehiclePartsEtcList(param);
		
		return createResponse(HttpStatus.OK, new String[]{"result"}, list);
	}
	
	@ApiOperation(value = "일반 정비 부품 등록", notes = "일반 정비 부품을 등록한다" , response = CreateVehiclePartsEtcVO.class)
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "Create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_ACCEPTED, message = "Accepted"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/vehiclePartsEtc" , method=RequestMethod.POST)
	public ResponseEntity createVehiclePartsEtc(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key				
			,@RequestBody(required = true)
			@Valid
			CreateVehiclePartsEtcVO vo,BindingResult br
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, VehicleNotFoundException, UserNotFoundException
	{
		
		if(br.hasErrors())
			return createValidErrorResponse(response,new String[]{"result"}, br ,null);
		
		
		*//*******유저 인증공통(체크로직)********//*
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		String lang = account.getLang();
		
		if(account == null)
			throw new UserNotFoundException();
		*//*******유저 인증공통(체크로직)********//*
		
		vo.setCorpKey(account.getCorpKey());
		
		String partsKey = clinicService.createVehiclePartsEtc(vo);
		
		return createResponse(HttpStatus.CREATED, new String[]{"result"}, partsKey);
	}
	
	@ApiOperation(value = "일반 정비 부품 삭제", notes = "일반 정비 부품을 삭제한다")
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "Create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_ACCEPTED, message = "Accepted"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/vehiclePartsEtc/{partsKey}" , method=RequestMethod.DELETE)
	public ResponseEntity deleteVehiclePartsEtc(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key		
			,@PathVariable(value="partsKey") 
			@ApiParam(value = "target partsKey")
			String partsKey
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, VehicleNotFoundException, UserNotFoundException
	{
		
		*//*******유저 인증공통(체크로직)********//*
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		String lang = account.getLang();
		
		if(account == null)
			throw new UserNotFoundException();
		*//*******유저 인증공통(체크로직)********//*
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("corpKey", account.getCorpKey());
		param.put("partsKey", partsKey);
		
		clinicService.deleteVehiclePartsEtc(param);
		
		return createResponse(HttpStatus.OK);
	}
	
	@ApiOperation(value = "정비 업체 리스트", notes = "정비 업체 리스트를 조회한다. " , response = MaintenanceCompanyVO.class, responseContainer = "List")
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns be can allocate vehicle list"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/maintenanceCompany" , method=RequestMethod.GET)
	public ResponseEntity getMaintenanceCompanyList(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key				
			,@RequestParam(value="limit" , required = false , defaultValue = "5")
			@ApiParam(value = "limit per page")
			int limit
			,@RequestParam(value="offset" , required = false , defaultValue = "0")
			@ApiParam(value = "offset")
			int offset
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, VehicleNotFoundException, UserNotFoundException
	{
		
		*//*******인증공통********//*
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		
		if(account == null)
			throw new UserNotFoundException();
		*//*******인증공통********//*
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("corpKey", account.getCorpKey());
		param.put("limit", String.valueOf(limit));
		param.put("offset", String.valueOf(offset));
		
		List<MaintenanceCompanyVO> list = clinicService.getMaintenanceCompanyList(param);
		
		return createResponse(HttpStatus.OK, new String[]{"result"}, list);
	}
	
	@ApiOperation(value = "정비 업체 삭제", notes = "정비 업체를 삭제한다")
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "Create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_ACCEPTED, message = "Accepted"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/maintenanceCompany/{companyKey}" , method=RequestMethod.DELETE)
	public ResponseEntity deleteMaintenanceCompany(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key		
			,@PathVariable(value="companyKey") 
			@ApiParam(value = "target companyKey")
			String companyKey
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, VehicleNotFoundException, UserNotFoundException
	{
		
		*//*******유저 인증공통(체크로직)********//*
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		String lang = account.getLang();
		
		if(account == null)
			throw new UserNotFoundException();
		*//*******유저 인증공통(체크로직)********//*
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("corpKey", account.getCorpKey());
		param.put("companyKey", companyKey);
		
		clinicService.deleteMaintenanceCompany(param);
		
		
		return createResponse(HttpStatus.OK);
	}
	
	@ApiOperation(value = "정비 업체 등록", notes = "정비 업체를 등록한다" , response = CreateMaintenanceCompanyVO.class)
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "Create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_ACCEPTED, message = "Accepted"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/maintenanceCompany" , method=RequestMethod.POST)
	public ResponseEntity createMaintenanceCompany(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key				
			,@RequestBody(required = true)
			@Valid
			CreateMaintenanceCompanyVO vo,BindingResult br
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, VehicleNotFoundException, UserNotFoundException
	{
		
		if(br.hasErrors())
			return createValidErrorResponse(response,new String[]{"result"}, br ,null);
		
		
		*//*******유저 인증공통(체크로직)********//*
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		String lang = account.getLang();
		
		if(account == null)
			throw new UserNotFoundException();
		*//*******유저 인증공통(체크로직)********//*
		
		vo.setCorpKey(account.getCorpKey());
		
		String companyKey = clinicService.insertMaintenanceCompany(vo);
		
		return createResponse(HttpStatus.CREATED, new String[]{"result"}, companyKey);
	}
	
	//일반작업차파츠 리스트
	
	@ApiOperation(value = "정비 결재 리스트", notes = "정비 결재 리스트를 조회한다. " , response = ClinicVO.class, responseContainer = "List")
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns be can allocate vehicle list"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/maintenance" , method=RequestMethod.GET)
	public ResponseEntity getMainteanceList(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key				
			,@RequestParam(value="limit" , required = false , defaultValue = "5")
			@ApiParam(value = "limit per page")
			int limit
			,@RequestParam(value="offset" , required = false , defaultValue = "0")
			@ApiParam(value = "offset")
			int offset
			,@RequestParam(value="maintenanceKey" , required = false)
			@ApiParam(value = "maintenanceKey")
			String maintenanceKey
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, VehicleNotFoundException, UserNotFoundException
	{
		
		*//*******인증공통********//*
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		
		if(account == null)
			throw new UserNotFoundException();
		*//*******인증공통********//*
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("maintenanceKey", maintenanceKey);
		
		
		List<ClinicVO> list = clinicService.getMaintenanceApprovalList(param);
		
		
		return createResponse(HttpStatus.OK, new String[]{"result"}, list);
	}
	
	
	@ApiOperation(value = "정비 등록", notes = "정비를 등록한다" , response = String.class)
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "Create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_ACCEPTED, message = "Accepted"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/maintenance" , method=RequestMethod.POST)
	public ResponseEntity createMainteance(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key				
			,@RequestBody(required = true)
			@Valid
			CreateClinicVO vo,BindingResult br
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, VehicleNotFoundException, UserNotFoundException
	{
		
		*//*******유저 인증공통(체크로직)********//*
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		String lang = account.getLang();
		
		if(account == null)
			throw new UserNotFoundException();
		*//*******유저 인증공통(체크로직)********//*
		
		*//*******차량 공통(체크로직)********//*
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("searchVehicleKey", vo.getVehicleKey());
		param.put("searchCorpKey",account.getCorpKey());
		VehicleVO vehicle = vehicleService.getVehicle(param,null);
		
		if(vehicle == null) 
			throw new VehicleNotFoundException();
		*//*******차량 공통(체크로직)********//*
		
		*//**************************그룹 및 그룹 부서장 체크 로직***************************//*	
		AccountVO accountVo = accountService.getAccount(t.getUserKey());
		if(StrUtil.isNullToEmpty(accountVo.getGroupKey())){
			//null이면
			throw new BizException("부서가 없는 사용자는 배차를 하실수 없습니다.");
		}
		
		Map<String,Object> groupParam = new HashMap<String,Object>();
		groupParam.put("searchGroupKey", accountVo.getGroupKey());
		groupParam.put("corpKey", accountVo.getCorpKey());
		
		GroupVO groupVo = groupService.getGroup(groupParam);
		
		if(StrUtil.isNullToEmpty(groupVo.getManagerAccountKey())){
			throw new BizException("부서장이 없는 사용자는 배차를 하실수 없습니다.");
		}
		*//**************************그룹 및 그룹 부서장 체크 로직***************************//*
		
		CorpVO corpVo = corpService.getCorp(account.getCorpKey(), null);
		
		vo.setAccountKey(accountVo.getAccountKey());
		vo.setCorpKey(accountVo.getCorpKey());
		
		//정비 결재를 사용한다.
		if("1".equals(corpVo.getMaintenanceApproval())){
			vo.setApprovalState("0");
			
			if(StrUtil.isNullToEmpty(vehicle.getVehicleManager())){
				//별도결재가 없음	
				vo.setMaxStep("2"); //2번 결재를 타야한다.
				
				if(StrUtil.isNullToEmpty(corpVo.getMaintenanceApprovalStep1())){
					vo.setApprovalStep1Account(corpVo.getVehicleManager());
				}else{
					vo.setApprovalStep1Account(corpVo.getMaintenanceApprovalStep1());
				}
				
				if(StrUtil.isNullToEmpty(corpVo.getMaintenanceApprovalStep2())){
					vo.setApprovalStep2Account(groupVo.getManagerAccountKey());
				}else{
					vo.setApprovalStep2Account(corpVo.getMaintenanceApprovalStep2());
				}
				
				
				if(corpVo.getAutoMaintenanceApprovalStep1().equals("1")){
					//1 1과 1 0 만 0 1인경우
					if(corpVo.getAutoMaintenanceApprovalStep2().equals("1")){
						//1 1
						vo.setApprovalStep1Date(DateUtil.getNow());
						vo.setApprovalStep2Date(DateUtil.getNow());
						vo.setApprovalState("2");
					}else{
						//1 0
						vo.setApprovalStep1Date(DateUtil.getNow());
						vo.setApprovalStep2Date(null);
						vo.setApprovalState("1");
					}
				}else{
					vo.setApprovalStep1Date(null);
					vo.setApprovalStep2Date(null);
				}
				
				
				
			}else{
				//별도결재자 있음
				vo.setMaxStep("1");
				vo.setApprovalStep1Account(vehicle.getVehicleManager());
				vo.setApprovalStep2Account(null);
			}
			
		}
		
		String maintenanceKey = clinicService.addMaintenance(accountVo,vo);
		
		System.out.println(maintenanceKey);
		
		//결재를 사용하지 않으면 상세도 같이 입력이 될수 있다?
		if(!"1".equals(corpVo.getMaintenanceApproval())){
			clinicService.addMaintenanceDetail(maintenanceKey,vo);
		}
		
				
		return createResponse(HttpStatus.CREATED, new String[]{"result"}, maintenanceKey);
	}
	
	
	@ApiOperation(value = "정비 삭제", notes = "정비를 삭제한다")
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "Create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_ACCEPTED, message = "Accepted"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/maintenance/{maintenanceKey}" , method=RequestMethod.DELETE)
	public ResponseEntity deleteMainteance(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@PathVariable(value="maintenanceKey") 
			@ApiParam(value = "target maintenanceKey")
			String maintenanceKey
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key		
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, VehicleNotFoundException, UserNotFoundException
	{
		
		*//*******유저 인증공통(체크로직)********//*
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		String lang = account.getLang();
		
		if(account == null)
			throw new UserNotFoundException();
		*//*******유저 인증공통(체크로직)********//*
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("maintenanceKey", maintenanceKey);
		
		clinicService.deleteMaintenance(param);
		
		
		return createResponse(HttpStatus.OK);
	}
	
	@ApiOperation(value = "정비 수정", notes = "정비를 수정한다")
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "Create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_ACCEPTED, message = "Accepted"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/maintenance/{maintenanceKey}" , method=RequestMethod.PUT)
	public ResponseEntity updateMainteance(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@PathVariable(value="maintenanceKey") 
			@ApiParam(value = "target maintenanceKey")
			String maintenanceKey
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			,@RequestBody(required = true)
			@Valid
			UpdateClinicVO vo
			,BindingResult br
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, VehicleNotFoundException, UserNotFoundException
	{
		
		if(br.hasErrors())
			return createValidErrorResponse(response,new String[]{"result"}, br ,null);
		
		
		*//*******유저 인증공통(체크로직)********//*
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		String lang = account.getLang();
		
		if(account == null)
			throw new UserNotFoundException();
		*//*******유저 인증공통(체크로직)********//*
		
		
		vo.setMaintenanceKey(maintenanceKey);
		
		clinicService.updateMainteance(vo);
		
		
		
		return createResponse(HttpStatus.OK);
	}
	
	
	@ApiOperation(value = "정비 결제 승인", notes = "정비 결제를 승인한다" , response = String.class)
	@RequestMapping(value="/api/{ver}/maintenance/approvalAccept/{maintenanceKey}/{step}", method=RequestMethod.PUT)
	public ResponseEntity approvalAccept(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@PathVariable(value="maintenanceKey") 
			@ApiParam(value = "target maintenanceKey")
			String maintenanceKey
			,@PathVariable(value="step") 
			@ApiParam(value = "step")
			String step
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, VehicleNotFoundException 
	{
		
		*//*******인증공통********//*
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		String lang = account.getLang();
		
		if(account == null)
			throw new UserNotFoundException();
		*//*******인증공통********//*
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("maintenanceKey", maintenanceKey);
		param.put("corpKey", account.getCorpKey());
		
		ClinicVO vo = clinicService.getMaintenance(param);
		CorpVO corpVo = corpService.getCorp(account.getCorpKey(), null);
		
		
		if(step.equals("1")){
			if(vo.getApprovalState().equals("0")){
				if(vo.getApprovalStep1Account().equals(account.getAccountKey())){
					clinicService.updateApprovalState(maintenanceKey,account.getAccountKey(),step);
					if(corpVo.getAutoMaintenanceApprovalStep2().equals("1")){
						clinicService.updateApprovalState(maintenanceKey,account.getAccountKey(),step + 1);
					}
				}else{
					return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "승인권한이 없습니다.");
				}
			}else{
				return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "승인 가능한 단계가 아닙니다.");
			}
		}else if(step.equals("2")){
			if(vo.getApprovalState().equals("1")){
				if(vo.getApprovalStep2Account().equals(account.getAccountKey())){

					//결재완료
					clinicService.updateApprovalState(maintenanceKey,account.getAccountKey(),step);
					
					
				}else{
					return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "승인권한이 없습니다.");
				}
			}else{
				return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "승인 가능한 단계가 아닙니다.");
			}
		}else if(step.equals("-1")){//반려
			//상신0 1차 1일때
			if(vo.getApprovalState().equals("0") || vo.getApprovalState().equals("1")){
				//만약에 상신처리인경우
				if(vo.getApprovalState().equals("0")){					

					if(vo.getApprovalStep1Account().equals(account.getAccountKey()) || vo.getApprovalStep2Account().equals(account.getAccountKey()) || vo.getAccountKey().equals(account.getAccountKey())){
						//반려 처리
						clinicService.updateApprovalState(maintenanceKey,account.getAccountKey(),step);

					}else{
						return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "반려권한이 없습니다.");
					}
					
					
				}else if(vo.getApprovalState().equals("1")){
					
					
					if(vo.getApprovalStep2Account().equals(account.getAccountKey())){
						clinicService.updateApprovalState(maintenanceKey,account.getAccountKey(),step);
					}else{
						return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "반려권한이 없거나, 이미 승인하였습니다.");
					}
					
					
				}else{
					//dead code
				}
			}else{
				return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "반려 가능한 단계가 아닙니다.");
			}
		}else{
			return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "잘못된 접근입니다.");
		}
		
		Map<String,Object> rtv = new HashMap<String,Object>();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		rtv.put("date", sdf.format(new Date().getTime()));
		rtv.put("groupNm", account.getGroupNm());
		rtv.put("name", account.getName());
		rtv.put("position", account.getCorpPosition());
		
		return createResponse(HttpStatus.OK, new String[]{"result"}, rtv);
	
	}
	
	@ApiOperation(value = "일반 정비 등록", notes = "일반 정비를 등록한다" , response = String.class)
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "Create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_ACCEPTED, message = "Accepted"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/maintenanceEtc" , method=RequestMethod.POST)
	public ResponseEntity createMainteanceEtc(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key				
			,@RequestBody(required = true)
			@Valid
			CreateClinicVO vo,BindingResult br
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, VehicleNotFoundException, UserNotFoundException
	{
		
		*//*******유저 인증공통(체크로직)********//*
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		String lang = account.getLang();
		
		if(account == null)
			throw new UserNotFoundException();
		*//*******유저 인증공통(체크로직)********//*
		
		*//*******차량 공통(체크로직)********//*
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("searchVehicleKey", vo.getVehicleKey());
		
		VehicleVO vehicle = vehicleService.getVehicle(param,null);
		
		if(vehicle == null) 
			throw new VehicleNotFoundException();
		*//*******차량 공통(체크로직)********//*
		
		*//**************************그룹 및 그룹 부서장 체크 로직***************************//*	
		AccountVO accountVo = accountService.getAccount(t.getUserKey());
		if(StrUtil.isNullToEmpty(accountVo.getGroupKey())){
			//null이면
			throw new BizException("부서가 없는 사용자는 배차를 하실수 없습니다.");
		}
		
		Map<String,Object> groupParam = new HashMap<String,Object>();
		groupParam.put("searchGroupKey", accountVo.getGroupKey());
		groupParam.put("corpKey", accountVo.getCorpKey());
		
		GroupVO groupVo = groupService.getGroup(groupParam);
		
		if(StrUtil.isNullToEmpty(groupVo.getManagerAccountKey())){
			throw new BizException("부서장이 없는 사용자는 배차를 하실수 없습니다.");
		}
		*//**************************그룹 및 그룹 부서장 체크 로직***************************//*
		
		CorpVO corpVo = corpService.getCorp(account.getCorpKey(), null);
		
		vo.setAccountKey(accountVo.getAccountKey());
		vo.setCorpKey(accountVo.getCorpKey());
		
		//정비 결재를 사용한다.
		if("1".equals(corpVo.getMaintenanceApproval())){
			vo.setApprovalState("0");
			
			if(StrUtil.isNullToEmpty(vehicle.getVehicleManager())){
				//별도결재가 없음	
				vo.setMaxStep("2"); //2번 결재를 타야한다.
				
				if(StrUtil.isNullToEmpty(corpVo.getMaintenanceApprovalStep1())){
					vo.setApprovalStep1Account(corpVo.getVehicleManager());
				}else{
					vo.setApprovalStep1Account(corpVo.getMaintenanceApprovalStep1());
				}
				
				if(StrUtil.isNullToEmpty(corpVo.getMaintenanceApprovalStep2())){
					vo.setApprovalStep2Account(groupVo.getManagerAccountKey());
				}else{
					vo.setApprovalStep2Account(corpVo.getMaintenanceApprovalStep2());
				}
				
				
				if(corpVo.getAutoMaintenanceApprovalStep1().equals("1")){
					//1 1과 1 0 만 0 1인경우
					if(corpVo.getAutoMaintenanceApprovalStep2().equals("1")){
						//1 1
						vo.setApprovalStep1Date(DateUtil.getNow());
						vo.setApprovalStep2Date(DateUtil.getNow());
						vo.setApprovalState("2");
					}else{
						//1 0
						vo.setApprovalStep1Date(DateUtil.getNow());
						vo.setApprovalStep2Date(null);
						vo.setApprovalState("1");
					}
				}else{
					vo.setApprovalStep1Date(null);
					vo.setApprovalStep2Date(null);
				}
				
				
				
			}else{
				//별도결재자 있음
				vo.setMaxStep("1");
				vo.setApprovalStep1Account(vehicle.getVehicleManager());
				vo.setApprovalStep2Account(null);
			}
			
		}
		
		String maintenanceKey = clinicService.addMaintenanceEtc(accountVo,vo);
		
		System.out.println(maintenanceKey);
		
		//결재를 사용하지 않으면 상세도 같이 입력이 될수 있다?
		if(!"1".equals(corpVo.getMaintenanceApproval())){
			clinicService.addMaintenanceEtcDetail(maintenanceKey,vo);
		}
		
				
		return createResponse(HttpStatus.CREATED, new String[]{"result"}, maintenanceKey);
	}
	
	@ApiOperation(value = "일반 정비 삭제", notes = "일반 정비를 삭제한다")
	@ApiResponses(value = {
	        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "Create success"),
	        @ApiResponse(code = HttpURLConnection.HTTP_ACCEPTED, message = "Accepted"),
	        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
	        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
	        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
	})
	@RequestMapping(value="/api/{ver}/maintenanceEtc/{maintenanceKey}" , method=RequestMethod.DELETE)
	public ResponseEntity deleteMainteanceEtc(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@PathVariable(value="maintenanceKey") 
			@ApiParam(value = "target maintenanceKey")
			String maintenanceKey
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key		
			) throws CreateResponseException, UnauthorizedException, BizException, NotFoundException, VehicleNotFoundException, UserNotFoundException
	{
		
		*//*******유저 인증공통(체크로직)********//*
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		String lang = account.getLang();
		
		if(account == null)
			throw new UserNotFoundException();
		*//*******유저 인증공통(체크로직)********//*
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("corpKey", account.getCorpKey());
		param.put("maintenanceKey", maintenanceKey);
		
		clinicService.deleteMaintenanceEtc(param);
		
		
		return createResponse(HttpStatus.OK);
	}
	
	
	@ApiOperation(value = "일반 정비 결제 승인", notes = "일반 정비 결제를 승인한다" , response = String.class)
	@RequestMapping(value="/api/{ver}/maintenanceEtc/approvalAccept/{maintenanceKey}/{step}", method=RequestMethod.PUT)
	public ResponseEntity approvalAcceptEtc(HttpServletRequest request,HttpServletResponse response
			,@PathVariable(value="ver") 
			@ApiParam(value = "version of application")
			String ver
			,@PathVariable(value="maintenanceKey") 
			@ApiParam(value = "target maintenanceKey")
			String maintenanceKey
			,@PathVariable(value="step") 
			@ApiParam(value = "step")
			String step
			,@RequestHeader(value="key"  , required = false)
			@ApiParam(value = "key")
			String key
			) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, VehicleNotFoundException 
	{
		
		*//*******인증공통********//*
		TokenVO t = authentication(tokenService,key);
		
		if(t == null) 
			throw new UnauthorizedException();
		
		AccountVO account = accountService.getAccount(t.getUserKey());
		String lang = account.getLang();
		
		if(account == null)
			throw new UserNotFoundException();
		*//*******인증공통********//*
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("maintenanceKey", maintenanceKey);
		param.put("corpKey", account.getCorpKey());
		
		ClinicVO vo = clinicService.getMaintenance(param);
		CorpVO corpVo = corpService.getCorp(account.getCorpKey(), null);
		
		
		if(step.equals("1")){
			if(vo.getApprovalState().equals("0")){
				if(vo.getApprovalStep1Account().equals(account.getAccountKey())){
					clinicService.updateApprovalStateEtc(maintenanceKey,account.getAccountKey(),step);
					if(corpVo.getAutoMaintenanceApprovalStep2().equals("1")){
						clinicService.updateApprovalStateEtc(maintenanceKey,account.getAccountKey(),step + 1);
					}
				}else{
					return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "승인권한이 없습니다.");
				}
			}else{
				return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "승인 가능한 단계가 아닙니다.");
			}
		}else if(step.equals("2")){
			if(vo.getApprovalState().equals("1")){
				if(vo.getApprovalStep2Account().equals(account.getAccountKey())){

					//결재완료
					clinicService.updateApprovalStateEtc(maintenanceKey,account.getAccountKey(),step);
					
					
				}else{
					return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "승인권한이 없습니다.");
				}
			}else{
				return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "승인 가능한 단계가 아닙니다.");
			}
		}else if(step.equals("-1")){//반려
			//상신0 1차 1일때
			if(vo.getApprovalState().equals("0") || vo.getApprovalState().equals("1")){
				//만약에 상신처리인경우
				if(vo.getApprovalState().equals("0")){					

					if(vo.getApprovalStep1Account().equals(account.getAccountKey()) || vo.getApprovalStep2Account().equals(account.getAccountKey()) || vo.getAccountKey().equals(account.getAccountKey())){
						//반려 처리
						clinicService.updateApprovalStateEtc(maintenanceKey,account.getAccountKey(),step);

					}else{
						return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "반려권한이 없습니다.");
					}
					
					
				}else if(vo.getApprovalState().equals("1")){
					
					
					if(vo.getApprovalStep2Account().equals(account.getAccountKey())){
						clinicService.updateApprovalStateEtc(maintenanceKey,account.getAccountKey(),step);
					}else{
						return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "반려권한이 없거나, 이미 승인하였습니다.");
					}
					
					
				}else{
					//dead code
				}
			}else{
				return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "반려 가능한 단계가 아닙니다.");
			}
		}else{
			return createResponse(HttpStatus.BAD_REQUEST, new String[]{"result"}, "잘못된 접근입니다.");
		}
		
		Map<String,Object> rtv = new HashMap<String,Object>();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		rtv.put("date", sdf.format(new Date().getTime()));
		rtv.put("groupNm", account.getGroupNm());
		rtv.put("name", account.getName());
		rtv.put("position", account.getCorpPosition());
		
		return createResponse(HttpStatus.OK, new String[]{"result"}, rtv);
	
	}*/
}
