package v4.web.rest.controller;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.util.string.StrUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import v4.ex.exc.BizException;
import v4.ex.exc.CreateResponseException;
import v4.ex.exc.extents.UnauthorizedException;
import v4.ex.exc.extents.UserNotFoundException;
import v4.ex.exc.extents.VehicleNotFoundException;
import v4.web.controller.BasicWebViewController;
import v4.web.rest.service.AccountService;
import v4.web.rest.service.CorpService;
import v4.web.rest.service.GroupService;
import v4.web.rest.service.TokenService;
import v4.web.rest.service.VehicleService;
import v4.web.vo.AccountVO;
import v4.web.vo.CreateGroupVO;
import v4.web.vo.GroupVO;
import v4.web.vo.TokenVO;
import v4.web.vo.UpdateGroupVO;
import v4.web.vo.VehicleVO;


/*
 * @Class name : LoginController
 * @Description : 
 * @Modification 
 * @since 
 * @version 
 * @see
 *
 */
/*@Api(value="restGroupController")
@RestController
*/
public class RestGroupController extends BasicWebViewController{
		
		ObjectMapper mapper = new ObjectMapper();
	
		@Autowired
		public AccountService accountService;
		
		@Autowired
		public TokenService tokenService;
		
		@Autowired
		public GroupService groupService;
		
		@Autowired
		public CorpService corpService;
		
		@Autowired
		public VehicleService vehicleService;
		
		@ApiOperation(value = "그룹생성", notes = "그룹을 생성한다.")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create complate"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/group" , method=RequestMethod.POST)
		public ResponseEntity createGroup(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = true)
				@ApiParam(value = "key")
				String key
				,@RequestBody(required = true)
				@Valid
				CreateGroupVO groupVo
				,BindingResult br
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
		{
			
			if(br.hasErrors())
				return createValidErrorResponse(response,new String[]{"result"}, br ,null);
			
			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			AccountVO account = accountService.getAccount(t.getUserKey());
			
			if(account == null)
				throw new UserNotFoundException();
			//여기까지 거의공통//
			
			//String groupKey = groupService.addGroup(groupVo,account);
			
			//groupService.sortGroup(account);

			//return createResponse(HttpStatus.OK,new String[]{"result"}, groupKey);
			return createResponse(HttpStatus.OK,new String[]{"result"}, "");
				
		}
		
		@ApiOperation(value = "그룹조회", notes = "전체 그룹을 가져 온다.",response = GroupVO.class, responseContainer="List")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create complate"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/groupTree" , method=RequestMethod.GET)
		public ResponseEntity getGroupTree(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = true)
				@ApiParam(value = "key")
				String key
				,@RequestParam(value="limit" , defaultValue = "10")
				int limit
				,@RequestParam(value="offset" , defaultValue = "0")
				int offset
				,@RequestParam(value="searchType" , defaultValue = "")
				String searchType
				,@RequestParam(value="searchText" , defaultValue = "")
				String searchText
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
		{
			
			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			AccountVO account = accountService.getAccount(t.getUserKey());
			
			if(account == null)
				throw new UserNotFoundException();
			//여기까지 거의공통//
			
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("corpKey", account.getCorpKey());
			param.put("limit", limit);
			param.put("offset", offset);
			param.put("searchType", searchType);
			param.put("searchText", searchText);
			
			List<GroupVO> rtvList = groupService.getGroupList(param);
			int cnt = 0;
			if(offset == 0){
				cnt = groupService.getGroupListCnt(param,null);
			}
			
			String[] rtvField = offset==0?new String[]{"result","resultCnt"}:new String[]{"result"};
			Object[] rtvData = offset==0?new Object[]{rtvList,cnt}:new Object[]{rtvList};
			
			return createResponse(HttpStatus.OK,rtvField, rtvData);
				
		}
		
		@ApiOperation(value = "상위그룹조회", notes = "전체 상위 그룹을 가져 온다.",response = GroupVO.class, responseContainer="List")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create complate"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/parentGroupTree" , method=RequestMethod.GET)
		public ResponseEntity getParentGroupTree(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = true)
				@ApiParam(value = "key")
				String key
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
		{
			
			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			AccountVO account = accountService.getAccount(t.getUserKey());
			
			if(account == null)
				throw new UserNotFoundException();
			//여기까지 거의공통//
			
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("corpKey", account.getCorpKey());
			
			List<GroupVO> rtvList = groupService.getParentGroupTree(param);
			
			return createResponse(HttpStatus.OK,new String[]{"result"}, rtvList);
				
		}
		
		@ApiOperation(value = "그룹조회", notes = "해당키 하위 모든 그룹을 가져 온다.",response = GroupVO.class, responseContainer="List")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create complate"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/groupTree/{parentGroupKey}" , method=RequestMethod.GET)
		public ResponseEntity getGroupTreePoint(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = true)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="parentGroupKey") 
				@ApiParam(value = "parentGroupKey")
				String parentGroupKey
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
		{
			
			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			AccountVO account = accountService.getAccount(t.getUserKey());
			
			if(account == null)
				throw new UserNotFoundException();
			//여기까지 거의공통//
			
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("searchGroupKey", parentGroupKey);
			List<GroupVO> rtvList = groupService.getGroupListDesending(param);
			
			return createResponse(HttpStatus.OK,new String[]{"result"}, rtvList);
				
		}
		
		@ApiOperation(value = "그룹조회", notes = "한개의 그룹 정보만 가져온다",response = GroupVO.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create complate"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/group/{groupKey}" , method=RequestMethod.GET)
		public ResponseEntity getGroup(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = true)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="groupKey") 
				@ApiParam(value = "groupKey")
				String groupKey
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
		{
			
			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			AccountVO account = accountService.getAccount(t.getUserKey());
			
			if(account == null)
				throw new UserNotFoundException();
			//여기까지 거의공통//
			
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("searchGroupKey", groupKey);
			param.put("corpKey", account.getCorpKey());
			GroupVO rtv = groupService.getGroup(param);
			
			return createResponse(HttpStatus.OK,new String[]{"result"}, rtv);
				
		}
		
		@ApiOperation(value = "그룹수정", notes = "그룹명을 변경한다.")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create complate"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/group/{groupKey}" , method=RequestMethod.PUT)
		public ResponseEntity updateGroup(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = true)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="groupKey") 
				@ApiParam(value = "groupKey")
				String groupKey
				,@RequestBody(required = true)
				@Valid
				UpdateGroupVO groupVo // ->updateGroupVO - groupNm , parentGroupKey
				,BindingResult br
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
		{
			
			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			AccountVO account = accountService.getAccount(t.getUserKey());
			
			if(account == null)
				throw new UserNotFoundException();
			//여기까지 거의공통//
			
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("searchGroupKey", groupKey);
			
			groupService.updateGroup(groupVo,param);			
			
			return createResponse(HttpStatus.OK);
				
		}
		
		@ApiOperation(value = "그룹 삭제", notes = "해당 그룹을 삭제한다. (하위 그룹도 같이 삭제 된다)")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create complate"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/group/{groupKey}" , method=RequestMethod.DELETE)
		public ResponseEntity deleteGroup(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = true)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="groupKey") 
				@ApiParam(value = "groupKey")
				String groupKey				
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
		{
			
			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			AccountVO account = accountService.getAccount(t.getUserKey());
			
			if(account == null)
				throw new UserNotFoundException();
			//여기까지 거의공통//
			
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("searchGroupKey", groupKey);
			
			groupService.deleteGroup(param); // *하부조직들 모두 삭제해야함*,*삭제된조직의 사람들 모두 무소속으로 업데이트*
			
			return createResponse(HttpStatus.OK);
				
		}
		
		
		@ApiOperation(value = "차량관리자 정보 조회", notes = "차량관리자 정보 조회를 가져온다",response = AccountVO.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create complate"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/corp/manager" , method=RequestMethod.GET)
		public ResponseEntity getCorpVehicleManager(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = true)
				@ApiParam(value = "key")
				String key
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
		{
			
			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			AccountVO account = accountService.getAccount(t.getUserKey());
			
			if(account == null)
				throw new UserNotFoundException();
			//여기까지 거의공통//
			
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("corpKey", account.getCorpKey());
			
			AccountVO rtv = corpService.getCorpVehicleManager(param);
			
			return createResponse(HttpStatus.OK,new String[]{"result"}, rtv);
				
		}
		
		
		@ApiOperation(value = "차량관리자 수정", notes = "차량관리자를 변경한다.")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create complate"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/corp/manager/{accountKey}" , method=RequestMethod.PUT)
		public ResponseEntity updateVehicleManager(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = true)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="accountKey") 
				@ApiParam(value = "accountKey")
				String accountKey
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
		{
			
			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			AccountVO account = accountService.getAccount(t.getUserKey());
			
			if(account == null)
				throw new UserNotFoundException();
			//여기까지 거의공통//
			
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("accountKey", accountKey);
			param.put("corpKey", account.getCorpKey());
			
			corpService.changeCorpVehicleManager(param);
			
			return createResponse(HttpStatus.OK);
				
		}
		
		@ApiOperation(value = "그룹 사용자 추가", notes = "그룹에 사용자를 추가한다.")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create complate"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/groupKey/{groupKey}/accountKey/{accountKey}" , method=RequestMethod.POST)
		public ResponseEntity groupAddUser(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = true)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="groupKey" ) 
				@ApiParam(value = "groupKey", required = true)
				String groupKey
				,@PathVariable(value="accountKey") 
				@ApiParam(value = "accountKey" , required = true)
				String accountKey
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
		{
			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			AccountVO account = accountService.getAccount(t.getUserKey());
			
			if(account == null)
				throw new UserNotFoundException();
			//여기까지 거의공통//
			
			AccountVO reqAccount = accountService.getAccount(accountKey);
			if(reqAccount == null)
				return createResponse(HttpStatus.NOT_FOUND, new String[]{"result"}, "user not found");
			//groupKey와 accountKey을 검증해야 한다
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("searchGroupKey", groupKey);
			param.put("corpKey", account.getCorpKey());
			param.put("accountKey", accountKey);
			
			GroupVO groupVo = groupService.getGroup(param);
			if(groupVo == null)
				return createResponse(HttpStatus.NOT_FOUND, new String[]{"result"}, "group not found");
			
			//여기다가 해당 유저의 groupKey을 업데이트 시켜 줘야 한다.
			
			groupService.groupAddUser(param);

			return createResponse(HttpStatus.OK,new String[]{"result"}, groupKey);
				
		}
		
		@ApiOperation(value = "그룹 사용자 삭제", notes = "그룹에 사용자를 삭제한다.")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create complate"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/groupKey/{groupKey}/accountKey/{accountKey}" , method=RequestMethod.DELETE)
		public ResponseEntity groupDeleteUser(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = true)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="groupKey" ) 
				@ApiParam(value = "groupKey", required = true)
				String groupKey
				,@PathVariable(value="accountKey") 
				@ApiParam(value = "accountKey" , required = true)
				String accountKey
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
		{
			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			AccountVO account = accountService.getAccount(t.getUserKey());
			
			if(account == null)
				throw new UserNotFoundException();
			//여기까지 거의공통//
			
			AccountVO reqAccount = accountService.getAccount(accountKey);
			if(reqAccount == null)
				return createResponse(HttpStatus.NOT_FOUND, new String[]{"result"}, "user not found");
			//groupKey와 accountKey을 검증해야 한다
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("searchGroupKey", groupKey);
			param.put("corpKey", account.getCorpKey());
			param.put("accountKey", accountKey);
			
			GroupVO groupVo = groupService.getGroup(param);
			if(groupVo == null)
				return createResponse(HttpStatus.NOT_FOUND, new String[]{"result"}, "group not found");
			
			//여기다가 해당 유저의 groupKey을 업데이트 시켜 줘야 한다.
			
			groupService.groupDeleteUser(param);

			return createResponse(HttpStatus.OK,new String[]{"result"}, groupKey);
				
		}
		
		
		@ApiOperation(value = "그룹 차량 추가", notes = "그룹에 차량을 추가한다.")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create complate"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/groupKey/{groupKey}/vehicleKey/{vehicleKey}" , method=RequestMethod.POST)
		public ResponseEntity groupAddVehicle(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = true)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="groupKey" ) 
				@ApiParam(value = "groupKey", required = true)
				String groupKey
				,@PathVariable(value="vehicleKey") 
				@ApiParam(value = "vehicleKey" , required = true)
				String vehicleKey
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
		{
			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			AccountVO account = accountService.getAccount(t.getUserKey());
			
			if(account == null)
				throw new UserNotFoundException();
			//여기까지 거의공통//
			
			//차량검증
			Map<String, Object> param2 = new HashMap<String, Object>();
			param2.put("searchVehicleKey", vehicleKey);
			param2.put("searchCorpKey",account.getCorpKey());
			VehicleVO vehicle = vehicleService.getVehicle(param2,null);
			
			if(vehicle == null) 
				return createResponse(HttpStatus.NOT_FOUND, new String[]{"result"}, "vehicle not found");
			
			
			
			//groupKey와 accountKey을 검증해야 한다
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("searchGroupKey", groupKey);
			param.put("corpKey", account.getCorpKey());
			param.put("vehicleKey", vehicleKey);
			
			//그룹 검증
			GroupVO groupVo = groupService.getGroup(param);
			if(groupVo == null)
				return createResponse(HttpStatus.NOT_FOUND, new String[]{"result"}, "group not found");
			
			groupService.groupAddVehicle(param);
			
			//여기서 insert을 시켜 줘야 하는데
			

			return createResponse(HttpStatus.OK,new String[]{"result"}, groupKey);
				
		}
		
		@ApiOperation(value = "그룹 차량 삭제", notes = "그룹에 차량을 삭제한다.")		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "create complate"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/groupKey/{groupKey}/vehicleKey/{vehicleKey}" , method=RequestMethod.DELETE)
		public ResponseEntity groupDeleteVehicle(HttpServletRequest req,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = true)
				@ApiParam(value = "key")
				String key
				,@PathVariable(value="groupKey" ) 
				@ApiParam(value = "groupKey", required = true)
				String groupKey
				,@PathVariable(value="vehicleKey") 
				@ApiParam(value = "vehicleKey" , required = true)
				String vehicleKey
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
		{
			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			AccountVO account = accountService.getAccount(t.getUserKey());
			
			if(account == null)
				throw new UserNotFoundException();
			//여기까지 거의공통//
			
			//차량검증
			Map<String, Object> param2 = new HashMap<String, Object>();
			param2.put("searchVehicleKey", vehicleKey);
			param2.put("searchCorpKey",account.getCorpKey());
			VehicleVO vehicle = vehicleService.getVehicle(param2,null);
			
			if(vehicle == null) 
				return createResponse(HttpStatus.NOT_FOUND, new String[]{"result"}, "vehicle not found");
			
			
			
			//groupKey와 accountKey을 검증해야 한다
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("searchGroupKey", groupKey);
			param.put("corpKey", account.getCorpKey());
			param.put("vehicleKey", vehicleKey);
			
			//그룹 검증
			GroupVO groupVo = groupService.getGroup(param);
			if(groupVo == null)
				return createResponse(HttpStatus.NOT_FOUND, new String[]{"result"}, "group not found");
			
			groupService.groupDeleteVehicle(param);
			
			//여기서 insert을 시켜 줘야 하는데
			

			return createResponse(HttpStatus.OK,new String[]{"result"}, groupKey);
				
		}
}
