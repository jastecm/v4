package v4.web.rest.controller;

import java.net.HttpURLConnection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import v4.ex.exc.BizException;
import v4.ex.exc.CreateResponseException;
import v4.ex.exc.DefaultInitCheckException;
import v4.ex.exc.extents.UnauthorizedException;
import v4.web.controller.BasicWebViewController;
import v4.web.rest.service.AccountService;
import v4.web.rest.service.PushService;
import v4.web.rest.service.TokenService;
import v4.web.vo.TokenVO;




/*
 * @Class name : LoginController
 * @Description : 
 * @Modification Information
 * 
 * @author 
 * @since 
 * @version 
 * @see
 *
 */
@Api(value="restPushController")
@RestController
public class RestPushController extends BasicWebViewController{
		
		@Autowired
		public AccountService accountService;
		
		@Autowired
		public TokenService tokenService;
		
		@Autowired
		public PushService pushService;
		
		@ApiOperation(value = "fcmKey refresh", notes = "fcmKey refresh(create포함)" , response = String.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my account"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/push/refresh" , method=RequestMethod.PUT)
		public ResponseEntity pushKeyRefresh(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = true)
				@ApiParam(value = "key")
				String key
				,@RequestHeader(value="fcmKey"  , required = true)
				@ApiParam(value = "fcmKey")
				String fcmKey
		) throws CreateResponseException, UnauthorizedException, BizException ,DefaultInitCheckException{
	
			TokenVO t = authentication(tokenService,key);
			
			if(t == null) 
				throw new UnauthorizedException();
			
			pushService.pushKeyRefresh(t.getUserKey(),fcmKey);
			
			return createResponse(HttpStatus.OK);
				
		}
		
		/*
		@ApiOperation(value = "push 거부리스트", notes = "거절 가능타입 - itemWarning\nmileageWarning\nmileageOver\nbattertWarning\naccidentWarning\n"
				+"dtcWarning\ndrivingStart\ndrivingEnd\nreportMailing\ninsuranceWarning\nappUpdate\nfirmwareUpdate" , response = String.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my account"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/push/denyList" , method=RequestMethod.GET)
		public ResponseEntity pushDenyList(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = true)
				@ApiParam(value = "key")
				String key
		) throws CreateResponseException, UnauthorizedException, BizException ,DefaultInitCheckException{
	
			TokenVO t = authenticationChangePw(key);
			
			if(t == null) 
				throw new UnauthorizedException();
			
			Map<String, Object> rtv = pushService.getDenyList(t.getUserKey());
			
			return createResponse(HttpStatus.OK,new String[]{"result"}, rtv);
				
		}
		
		@ApiOperation(value = "push 거절,승인", notes = "거절 가능타입 - itemWarning\nmileageWarning\nmileageOver\nbattertWarning\naccidentWarning\n"
				+"dtcWarning\ndrivingStart\ndrivingEnd\nreportMailing\ninsuranceWarning\nappUpdate\nfirmwareUpdate" , response = String.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my account"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/push/denyList/{denyType}" , method=RequestMethod.PUT)
		public ResponseEntity pushDeny(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = true)
				@ApiParam(value = "key")
				String key
				,@RequestParam(value="denyType"  , required = true)
				@ApiParam(value = "denyType")
				String denyType
				,@RequestParam(value="accept"  , required = true)
				@ApiParam(value = "push accept 1:승인 0:거부")
				String accept
		) throws CreateResponseException, UnauthorizedException, BizException ,DefaultInitCheckException{
	
			TokenVO t = authenticationChangePw(key);
			
			if(t == null) 
				throw new UnauthorizedException();
			
			pushService.pushDenyUpdate(t.getUserKey(),denyType,accept);
			
			return createResponse(HttpStatus.OK);
				
		}
		
		@ApiOperation(value = "subscribe topic", notes = "토픽구독(noti : 공지,event : 이벤트알림,test : 테스트용)" , response = String.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my account"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/push/subscribe/{topic}" , method=RequestMethod.PUT)
		public ResponseEntity pushSubscribe(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = true)
				@ApiParam(value = "key")
				String key
				,@RequestHeader(value="topic"  , required = true)
				@ApiParam(value = "topic")
				String topic
				,@RequestParam(value="accept"  , required = true)
				@ApiParam(value = "accept")
				String accept
		) throws CreateResponseException, UnauthorizedException, BizException ,DefaultInitCheckException{
	
			TokenVO t = authenticationChangePw(key);
			
			if(t == null) 
				throw new UnauthorizedException();
			
			pushService.pushSubscribe(t.getUserKey(),topic,accept);
			
			return createResponse(HttpStatus.OK);
				
		}
		
		@ApiOperation(value = "push", notes = "body = {title:'title' , body:'msg'} " , response = String.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns list of my account"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/push/msg" , method=RequestMethod.POST)
		public ResponseEntity pushMsg(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@RequestHeader(value="key"  , required = true)
				@ApiParam(value = "key")
				String key
				,@RequestParam(value="topic"  , required = false)
				@ApiParam(value = "topic(empty - personal , topic - noti,event,test group")
				String topic
				,@RequestBody(required = true)
				String msg
		) throws CreateResponseException, UnauthorizedException, BizException ,DefaultInitCheckException{
	
			TokenVO t = authenticationChangePw(key);
			
			if(t == null) 
				throw new UnauthorizedException();
			
			JSONObject jObj;
			try {
				jObj = new JSONObject(msg);
				String title = (String)jObj.get("title");
				String body = (String)jObj.get("body");
				
				pushService.push(t.getUserKey(),topic,title,body);
			} catch (JSONException e) {
				e.printStackTrace();
				throw new DefaultInitCheckException(e.getMessage());
			}
			
			return createResponse(HttpStatus.OK);
				
		}
		*/
}
