package v4.web.rest.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import v4.ex.exc.BizException;
import v4.ex.exc.CreateResponseException;
import v4.ex.exc.RestConnectException;
import v4.ex.exc.extents.UnauthorizedException;
import v4.ex.exc.extents.UserNotFoundException;
import v4.web.controller.BasicWebViewController;
import v4.web.rest.service.AccountService;
import v4.web.rest.service.DeviceService;
import v4.web.rest.service.DownloadService;
import v4.web.rest.service.TokenService;
import v4.web.rest.service.VehicleService;
import v4.web.vo.AccountVO;
import v4.web.vo.DeviceVO;
import v4.web.vo.DownloadVO;
import v4.web.vo.TokenVO;
import v4.web.vo.VehicleVO;


/*
 * @Class name : LoginController
 * @Description : 
 * @Modification Information
 * 
 * @author 
 * @since 
 * @version 
 * @see
 *
 */
@Api(value="restDownloadController")
@RestController
public class RestDownloadController extends BasicWebViewController{
		
		ObjectMapper mapper = new ObjectMapper();
	
		@Autowired
		TokenService tokenService;
		
		@Autowired
		AccountService accountService;
		
		@Autowired
		DeviceService deviceService;
		
		@Autowired
		DownloadService downloadService;
		
		@Autowired
		VehicleService vehicleService;
		
		@ApiOperation(value = "각종버전 조회", notes = "각종최신 버전을 조회한다." , response = DownloadVO.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns ver"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/download/ver/{osType}/{deviceKey}" , method=RequestMethod.GET)
		public ResponseEntity getDownloadVer(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@PathVariable(value="osType") 
				@ApiParam(value = "osType")
				String osType
				,@PathVariable(value="deviceKey") 
				@ApiParam(value = "deviceKey")
				String deviceKey
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key	
		)  throws CreateResponseException, UnauthorizedException, BizException, RestConnectException, UserNotFoundException  {
	
			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			AccountVO account = accountService.getAccount(t.getUserKey());
			
			if(account == null)
				throw new UserNotFoundException();
			
			DeviceVO device = deviceService.getDeviceFromKey(deviceKey);
			
			if(device == null)
				return createResponse(HttpStatus.NOT_FOUND, new String[]{"result"}, "not found device or none active device");
			
			DownloadVO rtvList = downloadService.getVer(device,osType);
			
			return createResponse(HttpStatus.OK,new String[]{"result"}, rtvList);
				
		}
		
		
		@ApiOperation(value = "차량DB 다운", notes = "차량DB를 다운로드한다" , response = DownloadVO.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns vehicleDB"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/download/vehicle/{deviceKey}" , method=RequestMethod.GET)
		public ResponseEntity getDownloadVehicle(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@PathVariable(value="deviceKey") 
				@ApiParam(value = "deviceKey")
				String deviceKey
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key			
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException 
		{
			
			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			AccountVO account = accountService.getAccount(t.getUserKey());
			
			if(account == null)
				throw new UserNotFoundException();
			
			DeviceVO device = deviceService.getDeviceFromKey(deviceKey);
			
			if(device == null)
				return createResponse(HttpStatus.NOT_FOUND, new String[]{"result"}, "not found device or none active device");
			
			if(StrUtil.isNullToEmpty(device.getConvCode()))
				return createResponse(HttpStatus.NOT_FOUND, new String[]{"result"}, "none active device");
			
			Map<Integer,byte[]> rtvList = downloadService.getVehicleDb(device); 
			
			try {
				
				byte[] nm = rtvList.get(0);
				String strNm = new String(nm,"UTF-8");
				Map<String,Object> param = new HashMap<String,Object>();
				param.put("deviceSn", device.getDeviceSn());
				param.put("downloadProperty", "downloadDb");
				param.put("downloadPropertyValue", strNm);
				deviceService.updateDownloadInfo(param);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			
			return createResponse(HttpStatus.OK, new String[]{"result"}, rtvList);
				
		}
		
		@ApiOperation(value = "Firmware 다운", notes = "Firmware를 다운로드한다" , response = DownloadVO.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns firmware"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/download/firmware/{deviceKey}" , method=RequestMethod.GET)
		public ResponseEntity getDownloadFirmware(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@PathVariable(value="deviceKey") 
				@ApiParam(value = "deviceKey")
				String deviceKey
				,@RequestHeader(value="key"  , required = false)
				@ApiParam(value = "key")
				String key			
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, RestConnectException 
		{
			
			TokenVO t = authentication(tokenService,key);
			
			if(t == null || StrUtil.isNullToEmpty(t.getUserKey()))
				return createResponse(HttpStatus.UNAUTHORIZED , new String[]{"result"} , t==null?"Unauthorized":t.getMsg());
			
			AccountVO account = accountService.getAccount(t.getUserKey());
			
			if(account == null)
				throw new UserNotFoundException();
			
			DeviceVO device = deviceService.getDeviceFromKey(deviceKey);
			
			if(device == null)
				return createResponse(HttpStatus.NOT_FOUND, new String[]{"result"}, "not found device or none active device");
			
			if(StrUtil.isNullToEmpty(device.getConvCode()))
				return createResponse(HttpStatus.NOT_FOUND, new String[]{"result"}, "none active device");
			
			Map<Integer,byte[]> rtvList = downloadService.getFirmware(device); 
			
			try {
				
				byte[] nm = rtvList.get(0);
				String strNm = new String(nm,"UTF-8");
				Map<String,Object> param = new HashMap<String,Object>();
				param.put("deviceSn", device.getDeviceSn());
				param.put("downloadProperty", "downloadFirm");
				param.put("downloadPropertyValue", strNm);
				deviceService.updateDownloadInfo(param);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return createResponse(HttpStatus.OK,new String[]{"result"}, rtvList);
				
		}
		
		///////////
		
		@ApiOperation(value = "각종버전 조회", notes = "각종최신 버전을 조회한다." , response = DownloadVO.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns ver"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/download/ver/{deviceSn}" , method=RequestMethod.GET)
		public ResponseEntity getDownloadVer2(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@PathVariable(value="deviceSn") 
				@ApiParam(value = "deviceSn")
				String deviceSn
		)  throws CreateResponseException, UnauthorizedException, BizException, RestConnectException  {
	
			
			DeviceVO device = deviceService.getDeviceFromSn(deviceSn);
			
			if(device == null)
				return createResponse(HttpStatus.NOT_FOUND, new String[]{"result"}, "not found device or none active device");
			
			String vehicleKey = device.getVehicleKey();
			
			if(StrUtil.isNullToEmpty(vehicleKey))
				return createResponse(HttpStatus.NOT_FOUND, new String[]{"result"}, "none active device");
			
			DownloadVO verVo = downloadService.getVer(device,"ios");
			
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("searchDeviceSn", deviceSn);
			param.put("searchCorpKey",device.getCorpKey());
			VehicleVO vehicle = vehicleService.getVehicle(param, null);
			
			if(vehicle == null)
				return createResponse(HttpStatus.NOT_FOUND, new String[]{"result"}, "not found vehicle or none active vehicle");
			
			String dbType = verVo.getDbType();
			String firmware = verVo.getFirmware();
			String vehicleVer = verVo.getVehicleVer();
			String plateNum = vehicle.getPlateNum();
			String convCode = vehicle.getConvCode();
			String modelMaster = vehicle.getModelMaster();
			String cKey = device.getConnectionKey();
			
			Map<String,String> rtv = new HashMap<String,String>();
			
			rtv.put("dbType", dbType);
			rtv.put("firmware", firmware);
			rtv.put("vehicleVer", vehicleVer);
			rtv.put("plateNum", plateNum);
			rtv.put("convCode", convCode);
			rtv.put("modelMaster", modelMaster);
			rtv.put("connectionKey", cKey);
			
			return createResponse(HttpStatus.OK, new String[]{"result"}, rtv);
			
				
		}
		
		@ApiOperation(value = "차량DB 다운", notes = "차량DB를 다운로드한다" , response = DownloadVO.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns vehicleDB"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/download/{deviceSn}" , method=RequestMethod.GET)
		public ResponseEntity getDownloadVehicleDb(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@PathVariable(value="deviceSn") 
				@ApiParam(value = "deviceSn")
				String deviceSn
				) throws CreateResponseException, UnauthorizedException, BizException 
		{
			
			DeviceVO device = deviceService.getDeviceFromSn(deviceSn);
			
			if(device == null)
				return createResponse(HttpStatus.NOT_FOUND, new String[]{"result"}, "not found device or none active device");
			
			String vehicleKey = device.getVehicleKey();
			
			if(StrUtil.isNullToEmpty(vehicleKey))
				return createResponse(HttpStatus.NOT_FOUND, new String[]{"result"}, "none active device");
			
			DownloadVO verVo = downloadService.getVer(device,"ios");
			
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("searchDeviceSn", deviceSn);
			param.put("searchCorpKey",device.getCorpKey());
			VehicleVO vehicle = vehicleService.getVehicle(param, null);
			
			if(vehicle == null)
				return createResponse(HttpStatus.NOT_FOUND, new String[]{"result"}, "not found vehicle or none active vehicle");
			
			File rtv = downloadService.getVehicleDbFile(device);
			
			try {
				String fileNm = rtv.getName();
				String nm = fileNm.split("_")[1].split("\\.")[0];
				param.clear();
				param.put("deviceSn", device.getDeviceSn());
				param.put("downloadProperty", "downloadDb");
				param.put("downloadPropertyValue", nm);
				deviceService.updateDownloadInfo(param);
			} catch (BizException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {		 

				InputStreamResource resource = new InputStreamResource(new FileInputStream(rtv));
				
				//createResponse는 나중에 만들자.
				return ResponseEntity.ok()
						.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + rtv.getName())
						.contentType(MediaType.parseMediaType("application/octet-stream")).contentLength(rtv.length())
						.body(resource);

			} catch (Exception e) {
				return createResponse(HttpStatus.INTERNAL_SERVER_ERROR,new String[]{"result"}, "");
			}
			
		}
		
		@ApiOperation(value = "차량DB 다운", notes = "차량DB를 다운로드한다" , response = DownloadVO.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns vehicleDB"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/download/vehicleDb/{deviceSn}" , method=RequestMethod.GET)
		public ResponseEntity getDownloadVehicleDb2(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@PathVariable(value="deviceSn") 
				@ApiParam(value = "deviceSn")
				String deviceSn
				) throws CreateResponseException, UnauthorizedException, BizException 
		{
			
			
			DeviceVO device = deviceService.getDeviceFromSn(deviceSn);
			
			if(device == null)
				return createResponse(HttpStatus.NOT_FOUND, new String[]{"result"}, "not found device or none active device");
			
			String vehicleKey = device.getVehicleKey();
			
			if(StrUtil.isNullToEmpty(vehicleKey))
				return createResponse(HttpStatus.NOT_FOUND, new String[]{"result"}, "none active device");
			
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("searchDeviceSn", deviceSn);
			param.put("searchCorpKey",device.getCorpKey());
			VehicleVO vehicle = vehicleService.getVehicle(param, null);
			
			if(vehicle == null)
				return createResponse(HttpStatus.NOT_FOUND, new String[]{"result"}, "not found vehicle or none active vehicle");
			
			Map<Integer,byte[]> rtvList = downloadService.getVehicleDb(device);
			
			try {
				
				byte[] nm = rtvList.get(0);
				String strNm = new String(nm,"UTF-8");
				param.clear();
				param.put("deviceSn", device.getDeviceSn());
				param.put("downloadProperty", "downloadDb");
				param.put("downloadPropertyValue", strNm);
				deviceService.updateDownloadInfo(param);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return createResponse(HttpStatus.OK,new String[]{"result"}, rtvList);
		}
		
		@ApiOperation(value = "Firmware 다운", notes = "Firmware를 다운로드한다" , response = DownloadVO.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns firmware"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/download/f/w/{deviceSn}" , method=RequestMethod.GET)
		public ResponseEntity getDownloadFirmware2(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@PathVariable(value="deviceSn") 
				@ApiParam(value = "deviceSn")
				String deviceSn
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, RestConnectException 
		{
			
			
			DeviceVO device = deviceService.getDeviceFromSn(deviceSn);
			
			if(device == null)
				return createResponse(HttpStatus.NOT_FOUND, new String[]{"result"}, "not found device or none active device");
			
			Map<Integer,byte[]> rtv = downloadService.getFirmware(device);
			
			try {
				
				byte[] nm = rtv.get(0);
				String strNm = new String(nm,"UTF-8");
				Map<String,Object> param = new HashMap<String,Object>();
				param.put("deviceSn", device.getDeviceSn());
				param.put("downloadProperty", "downloadFirm");
				param.put("downloadPropertyValue", strNm);
				deviceService.updateDownloadInfo(param);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return createResponse(HttpStatus.OK,new String[]{"result"}, rtv);
				
		}
		
		@ApiOperation(value = "udpateApp confirm", notes = "udpateApp confirm" , response = DownloadVO.class)		
		@ApiResponses(value = {
		        @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Returns firmware"),
		        @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "Unauthorized"),
		        @ApiResponse(code = HttpURLConnection.HTTP_NOT_FOUND, message = "Not found"),
		        @ApiResponse(code = HttpURLConnection.HTTP_INTERNAL_ERROR, message = "Internal server problems")
		})
		@RequestMapping(value="/api/{ver}/download/confirm/{deviceSn}/{connectionKey}" , method=RequestMethod.PUT)
		public ResponseEntity getDownloadConfirm(HttpServletRequest request,HttpServletResponse response
				,@PathVariable(value="ver") 
				@ApiParam(value = "version of application")
				String ver
				,@PathVariable(value="deviceSn") 
				@ApiParam(value = "deviceSn")
				String deviceSn
				,@PathVariable(value="connectionKey") 
				@ApiParam(value = "connectionKey")
				String connectionKey
				) throws CreateResponseException, UnauthorizedException, BizException, UserNotFoundException, RestConnectException 
		{
			
			
			DeviceVO device = deviceService.getDeviceFromSn(deviceSn);
			
			if(device == null)
				return createResponse(HttpStatus.NOT_FOUND, new String[]{"result"}, "not found device or none active device");
			
			deviceService.updateConnectionKey(device.getDeviceId(),connectionKey);
			
			return createResponse(HttpStatus.OK);
				
		}
}
