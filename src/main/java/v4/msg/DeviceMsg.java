package v4.msg;

import v4.ex.exc.BizException;

public interface DeviceMsg {

	String tdr(Object tdrVo) throws BizException;
	String trip(Object tripVo) throws BizException;
	String provision(Object provisionVo) throws BizException;
	String locationReport(Object locationReportVo) throws BizException;
	String hello(Object helloVo) throws BizException;
	String fault(Object fualtVo) throws BizException;
	String dtc(Object dtcVo) throws BizException;
	String attach(Object attchVo) throws BizException;
	
}
