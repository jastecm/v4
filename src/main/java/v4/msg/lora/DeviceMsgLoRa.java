package v4.msg.lora;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jastecm.influx.com.common.Globals;
import org.jastecm.lora.msg.LoraMsgConnector;
import org.jastecm.lora.vo.extention.LoraMsg_ATTACH;
import org.jastecm.lora.vo.extention.LoraMsg_CONFIRM_PROVISION;
import org.jastecm.lora.vo.extention.LoraMsg_CONFIRM_TRIP;
import org.jastecm.lora.vo.extention.LoraMsg_DTC;
import org.jastecm.lora.vo.extention.LoraMsg_FAULT;
import org.jastecm.lora.vo.extention.LoraMsg_HELLO;
import org.jastecm.lora.vo.extention.LoraMsg_PROVISION;
import org.jastecm.lora.vo.extention.LoraMsg_TDR;
import org.jastecm.lora.vo.extention.LoraMsg_TRIP0;
import org.jastecm.lora.vo.extention.LoraMsg_TRIP1;
import org.jastecm.lora.vo.extention.LoraMsg_TRIP2;
import org.jastecm.string.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;

import com.common.AddrApiUtil;
import com.util.TimeUtil;

import v4.ex.exc.BizException;
import v4.msg.DeviceMsg;
import v4.socket.EchoHandler;
import v4.web.rest.dao.DeviceMsgDAO;
import v4.web.rest.service.AccountService;
import v4.web.rest.service.AttachService;
import v4.web.rest.service.DeviceService;
import v4.web.rest.service.DtcService;
import v4.web.rest.service.GeoFenceService;
import v4.web.rest.service.ItemService;
import v4.web.rest.service.MailBoxService;
import v4.web.rest.service.MiligeService;
import v4.web.rest.service.OilService;
import v4.web.rest.service.PushService;
import v4.web.rest.service.PushType;
import v4.web.rest.service.ScoringService;
import v4.web.rest.service.TimeLineService;
import v4.web.rest.service.VehicleService;
import v4.web.rest.service.WorkingAreaTimeService;
import v4.web.rest.service.data.AccountDataService;
import v4.web.rest.service.data.AllocateDataService;
import v4.web.rest.service.data.DeviceDataService;
import v4.web.rest.service.data.TripDataService;
import v4.web.rest.service.data.VehicleDataService;
import v4.web.vo.AttachVO;
import v4.web.vo.DtcVO;
import v4.web.vo.OilVO;
import v4.web.vo.TripRecordVO;
import v4.web.vo.data.AccountVO;
import v4.web.vo.data.AllocateVO;
import v4.web.vo.data.DeviceVO;
import v4.web.vo.data.TripVO;
import v4.web.vo.data.VehicleVO;

@Service("deviceMsgLora")
public class DeviceMsgLoRa implements DeviceMsg{

	@Autowired
	DeviceDataService deviceDataService;
	
	@Autowired
	DeviceService deviceService;
	
	@Autowired
	VehicleDataService vehicleDataService;
	
	@Autowired
	VehicleService vehicleService;
	
	@Autowired
	TripDataService tripDataService;
	
	@Autowired
	OilService oilService;
	
	@Autowired
	GeoFenceService geoFenceService;
	
	@Autowired
	PushService pushService;
	
	@Autowired
	MailBoxService mailBoxService;
	
	@Autowired
	WorkingAreaTimeService workingAreaTimeService;
	
	@Autowired
	DtcService dtcService;
	
	@Autowired
	ItemService itemService;
	
	@Autowired
	MiligeService miligeService;
	
	
	@Autowired
	ScoringService scoringService;
	
	@Autowired
	TimeLineService timeLineService;
	
	@Autowired
	AttachService attachService;
	
	@Autowired
	AllocateDataService allocateDataService;
	
	@Autowired
	AccountDataService accountDataService;		
	
	@Autowired
	AccountService accountService;
	
	@Autowired
	DeviceMsgDAO deviceMsgDao;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
	
	@Override
	public String tdr(Object tdrVo) throws BizException {
		if(tdrVo instanceof LoraMsg_TDR){
			LoraMsg_TDR tdr = (LoraMsg_TDR) tdrVo;
			
			String deviceId = tdr.getDevice();
			
			DeviceVO device = deviceDataService.getDeviceFromId(deviceId);
			
			if(null == device.getVehicle()) {
				System.out.println("ignore tdr - device("+deviceId+") is not active");
				return null;
			}
			
			deviceService.updateLatestPacket(device.getDeviceId());
			
			VehicleVO vehicle = device.getVehicle();
			
			if(vehicle == null) {
				System.out.println("ignore tdr - vehicle("+deviceId+") is null");
				return null;
			}
			
			
			if(new Date().getTime() > device.getExpireDate().getTime() ) {
				System.out.println("ignore tdr - device("+deviceId+") payment expired");
				return null;
			}
			
			
			String curTime = sdf.format(new Date(tdr.getCurTime())); 
			System.out.println(curTime);
			
			/*
			//shift issue
			long gpsTime =tdr.getCurTime(); 
			long now = TimeUtil.now()*1000;
			long shiftTime = gpsTime - now;
			long shiftDay = 0;			
			long hour = 60*60*1000;
			if ( shiftTime > 23*hour ) shiftDay = (shiftTime + hour) / (24*hour);
				
			if(shiftDay > 0) gpsTime = gpsTime - ((24*hour)*shiftDay);
			
			String shiftCurTime = sdf.format(new Date(gpsTime));
			System.out.println(shiftCurTime);
			*/
			/*
			TripRecordVO vo = new TripRecordVO(device.getDeviceKey(),vehicle.getVehicleKey(),tdr.getTripId(),tdr.getRpm(),tdr.getVss()
					,tdr.getLon(),tdr.getLat(),tdr.getCurTime(),tdr.getSuddenMarker(),tdr.getDistance(),tdr.getFco(),tdr.getRapidStart()
					,tdr.getRapidStop(),tdr.getRapidAccel(),tdr.getRapidDeaccel(),tdr.getRapidTurn(),tdr.getRapidUtern(),tdr.getOverSpeed(),tdr.getOverSpeedLong()
					,tdr.getEncrypt(),tdr.getvMaj()+"."+tdr.getvMin(),shiftDay);
			*/
			TripRecordVO vo = new TripRecordVO(device.getDeviceKey(),vehicle.getVehicleKey(),tdr.getTripId(),tdr.getRpm(),tdr.getVss()
					,tdr.getLon(),tdr.getLat(),tdr.getCurTime(),tdr.getSuddenMarker(),tdr.getDistance(),tdr.getFco(),tdr.getRapidStart()
					,tdr.getRapidStop(),tdr.getRapidAccel(),tdr.getRapidDeaccel(),tdr.getRapidTurn(),tdr.getRapidUtern(),tdr.getOverSpeed(),tdr.getOverSpeedLong()
					,tdr.getEncrypt(),tdr.getvMaj()+"."+tdr.getvMin(),0);
			
			if(!vehicle.getLocationSetting().equals("1")){
				vo.setLat(0d);
				vo.setLon(0d);
			}
			
			//deviceService.updateDeviceShiftDay(deviceId,shiftDay);
			
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("searchDeviceKey", device.getDeviceKey());
			param.put("searchTripId", vo.getTripId());
			
			TripVO trip = tripDataService.getTripFromMsg(param);
			boolean firstTrip = false;
			
			Map<String,String> autoDriverMatcher = new HashMap<String,String>();
			 
			if(trip == null){
				
				autoDriverMatcher = autoAllocateReportMatch(tdr.getCurTime(),vehicle);
				
				String fwVer = device.getHelloFwVer();
				v4.web.vo.TripVO newTripVo = new v4.web.vo.TripVO(device.getDeviceKey(), vo.getTripId(), autoDriverMatcher.get("driver"), vehicle.getVehicleKey() , vo.getGpsTime(), 10, "0", 0 , fwVer 
						,autoDriverMatcher.get("needAllocate") 
						,autoDriverMatcher.get("allocateKey")
						,autoDriverMatcher.get("needReport")
						,autoDriverMatcher.get("reportPurpose")
						,autoDriverMatcher.get("reportContents"));
				deviceMsgDao.tripInit(newTripVo);
				trip = tripDataService.getTripFromMsg(param);
				firstTrip = true;
			}
			
			String driver = trip.getDriverKey();
			
			deviceMsgDao.insertTdr(vo);
			
			//200이하 트립이면 유효트립으로 전환
			if(trip.getTripState() == 0 && vo.getDistance() > 200)
				deviceMsgDao.updateValidTripState(trip.getTripKey());
			
			//유효트립이고 트립이 오기 전이면
			if(trip.getTripState() < 2){
				
				String addr = vehicle.getLocationSetting().equals("1")?"주행중입니다.":"GPS정보가 수집되지 않았습니다.";
				String addrEn = vehicle.getLocationSetting().equals("1")?"NOW DRIVING":"NOT COLLECT GPS DATA";
				
				if(vo.getLat() != 0 && vo.getLon() != 0 && firstTrip){
					addr = AddrApiUtil.getGPSAdressByGeometryVerSK(vo.getLat(), vo.getLon());
					addrEn = AddrApiUtil.getGPSAdressByGeometryEn(vo.getLat(), vo.getLon());
				}
				
				//위치갱신
				vehicleService.vehicleLastestInfoUpdate(vehicle.getVehicleKey(),0,vo.getLon()+","+vo.getLat(),addr,addrEn,vo.getGpsTime(),null,driver);
				
				//micro trip update - distance,fco,avgFco,price,endDate
				trip.setDistance(vo.getDistance());
				trip.setFco(vo.getFco());
				
				if(trip.getFco() == 0) trip.setAvgFco(0d);
				else trip.setAvgFco((double)trip.getDistance()/(double)trip.getFco());

				trip.setEndDate(new Date(tdr.getCurTime()));
				
				if(trip.getStartLat() == 0 && trip.getStartLon() == 0){
					trip.setStartLat(vo.getLat());
					trip.setStartLon(vo.getLon());
												
					if(!StrUtil.isNullToEmpty(addr)) trip.setStartAddr(addr);
					if(!StrUtil.isNullToEmpty(addrEn)) trip.setStartAddrEn(addrEn);
				}else{
					trip.setEndLat(vo.getLat());
					trip.setEndLon(vo.getLon());
					
					if(!StrUtil.isNullToEmpty(addr)) trip.setEndAddr(addr);
					if(!StrUtil.isNullToEmpty(addrEn)) trip.setEndAddrEn(addrEn);
				}
				
				
				String fuel = vehicle.getVehicleModel().getFuel();
				if(fuel.equals("DIESEL") || fuel.equals("GASOLINE") || fuel.equals("LPI")){
					String sDate = new SimpleDateFormat("yyyy-MM-dd").format(vo.getGpsTime());
					OilVO ovo = oilService.getUnitOilPrice(sDate);
					double oilUnitPrice = 0; 
					if(fuel.equals("DIESEL")) oilUnitPrice = Double.parseDouble(ovo.getDiesel()); 
					else if(fuel.equals("GASOLINE")) oilUnitPrice = Double.parseDouble(ovo.getGasoline()); 
					else if(fuel.equals("LPI")) oilUnitPrice = Double.parseDouble(ovo.getLpg()); 
					
					trip.setOilUnitPrice((int)oilUnitPrice);
					trip.setOilPrice((int)(oilUnitPrice * (vo.getFco()/1000d)));
				}else{
					//HYBRID,ELECTRIC
					trip.setOilUnitPrice(0);
					trip.setOilPrice(0);
				}
					
				if((vo.getLat() != 0 && vo.getLon() != 0))
					geoFenceService.geoFence(vehicle,driver,trip.getTripKey(),vo.getLat(),vo.getLon());
				}
				
				deviceMsgDao.updateMicroTrip(trip);
				
				if(firstTrip){
					
					vehicleService.updateLastTripStart(vo.getVehicleKey(),trip.getTripKey());
					
					String pushMsg = "%s 차량의 주행이 시작되었습니다.";
					String title = "주행 시작";
					pushMsg = String.format(pushMsg, vehicle.getPlateNum());
					System.out.println(pushMsg);
					
					pushService.pushChkAccept(PushType.PUSHTYPE_TRIP,driver, "", title, pushMsg);
					
					pushService.insertPushHstr(vehicle.getCorp().getCorpKey(),"2",title,pushMsg);
					
					long mailExpired = 1000*60*60*24*7L;						
					mailBoxService.simgleUserMailBoxInsert(driver, title, pushMsg, mailExpired);
					
				}
				
			}else return null;
			
			return "";
		
	}

	@Override
	public String trip(Object tripVo) throws BizException {
		if(tripVo instanceof LoraMsg_TRIP0){
			
			LoraMsg_TRIP0 trip = (LoraMsg_TRIP0) tripVo;
			
			String deviceId = trip.getDevice();
			
			DeviceVO device = deviceDataService.getDeviceFromId(deviceId);
			
			if(null == device.getVehicle()) {
				System.out.println("ignore tdr - device("+deviceId+") is not active");
				return null;
			}
			
			deviceService.updateLatestPacket(device.getDeviceId());
			
			VehicleVO vehicle = device.getVehicle();
			
			if(vehicle == null) {
				System.out.println("ignore tdr - vehicle("+deviceId+") is null");
				return null;
			}
			
			
			if(new Date().getTime() > device.getExpireDate().getTime() ) {
				System.out.println("ignore tdr - device("+deviceId+") payment expired");
				return null;
			}
			
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("searchDeviceKey", device.getDeviceKey());
			param.put("searchTripId", trip.getTripId());
			TripVO tempTrip = tripDataService.getTripFromMsg(param);
			
			Map<String,String> autoDriverMatcher = new HashMap<String,String>();
			boolean initTripPhase = true;
			if(tempTrip == null){
				
				autoDriverMatcher = autoAllocateReportMatch(trip.getStartTime(),vehicle);
				
				String fwVer = device.getHelloFwVer();
				v4.web.vo.TripVO newTripVo = new v4.web.vo.TripVO(device.getDeviceKey(), trip.getTripId(), autoDriverMatcher.get("driver"), vehicle.getVehicleKey() , trip.getStartTime(), 10, "0", 0 , fwVer 
						,autoDriverMatcher.get("needAllocate") 
						,autoDriverMatcher.get("allocateKey")
						,autoDriverMatcher.get("needReport")
						,autoDriverMatcher.get("reportPurpose")
						,autoDriverMatcher.get("reportContents"));
				deviceMsgDao.tripInit(newTripVo);
				tempTrip = tripDataService.getTripFromMsg(param);
				initTripPhase = false;
			}
			
			if(tempTrip.getTripState()==3) {
				sendConfirm(trip.getDevice(),device.getAppEui(),trip.getTripSeq(),trip.getMaxPart());
				return null;
			}
			
			if(tempTrip.getPart().indexOf(String.valueOf(trip.getCurrentPart())) == -1){
				if(initTripPhase){
					autoDriverMatcher = autoAllocateReportMatch(trip.getStartTime(),vehicle);
					String driver = autoDriverMatcher.get("driver");
					
					if(null != driver && !driver.equals(tempTrip.getDriverKey())){
						AccountVO avo = accountDataService.getAccountFromKey(driver, true);
						tempTrip.setDriver(avo);
					}
					tempTrip.setNeedAllocate(autoDriverMatcher.get("needAllocate"));
					tempTrip.setAllocateKey(autoDriverMatcher.get("allocateKey"));
					tempTrip.setNeedReport(autoDriverMatcher.get("needReport"));
					tempTrip.setPurpose(autoDriverMatcher.get("reportPurpose"));
					tempTrip.setContents(autoDriverMatcher.get("reportContents"));
					if(tempTrip.getAccureFlag().equals("0") && tempTrip.getNeedReport().equals("0")){
						tempTrip.setAccureFlag("1");
						accountService.updateAccureData(driver,tempTrip.getDistance(),tempTrip.getStartDate(),tempTrip.getEndDate());
					}
				}
				
				
				tempTrip.setFco(trip.getFco());
				tempTrip.setDistance(trip.getDist());
				tempTrip.setTotDistance(trip.getTotDist());
				tempTrip.setPartTotal(trip.getMaxPart());
				tempTrip.setTripState(2);
				tempTrip.setVersion(trip.getvMaj()+"."+trip.getvMin());
				if(trip.getFco() == 0) tempTrip.setAvgFco(0d);
				else tempTrip.setAvgFco((double)trip.getDist()/(double)trip.getFco());
				
				
				if(!vehicle.getLocationSetting().equals("1")){
					tempTrip.setStartLat(0d);
					tempTrip.setStartLon(0d);
					tempTrip.setEndLat(0d);
					tempTrip.setEndLon(0d);
				}else{
					tempTrip.setStartLat(trip.getStartLat());
					tempTrip.setStartLon(trip.getStartLon());
					tempTrip.setEndLat(trip.getEndLat());
					tempTrip.setEndLon(trip.getEndLon());
				}
				/*
				long startTime = trip.getStartTime();
				long endTime = trip.getEndTime();
				long hour = 60*60*1000;
				long interval = endTime-startTime;
				long shiftDay = interval / (24*hour);
				if(shiftDay > 0) startTime += shiftDay*(24*hour);
								
				String deviceShiftDay = deviceService.getDeviceShiftDay(deviceId);
				if(StrUtil.isNullToEmpty(deviceShiftDay)) deviceShiftDay = "0";
				
				startTime -= Long.parseLong(deviceShiftDay)* (24*hour);
				endTime -= Long.parseLong(deviceShiftDay)* (24*hour);
				
				tempTrip.setShiftDay(String.valueOf(deviceShiftDay));
				 */
				tempTrip.setStartDate(new Date(trip.getStartTime()));
				tempTrip.setEndDate(new Date(trip.getEndTime()));
				
				String fuel = vehicle.getVehicleModel().getFuel();
				
				if(fuel.equals("DIESEL") || fuel.equals("GASOLINE") || fuel.equals("LPI")){
					String sDate = new SimpleDateFormat("yyyy-MM-dd").format(trip.getStartTime());
					OilVO ovo = oilService.getUnitOilPrice(sDate);
					
					double oilUnitPrice = 0; 
					if(fuel.equals("DIESEL")) oilUnitPrice = Double.parseDouble(ovo.getDiesel()); 
					else if(fuel.equals("GASOLINE")) oilUnitPrice = Double.parseDouble(ovo.getGasoline()); 
					else if(fuel.equals("LPI")) oilUnitPrice = Double.parseDouble(ovo.getLpg()); 
					
					tempTrip.setOilUnitPrice((int)oilUnitPrice);
					tempTrip.setOilPrice((int)(oilUnitPrice * (trip.getFco()/1000d)));
				}else{
					tempTrip.setOilUnitPrice(0);
					tempTrip.setOilPrice(0);
				}
				
				String startAddr = "GPS정보가 수집되지 않았습니다.";
				String startAddrEn = "NOT COLLECT GPS DATA";
				String endAddr = "GPS정보가 수집되지 않았습니다.";
				String endAddrEn = "NOT COLLECT GPS DATA";
				if(tempTrip.getEndLat() != 0 && tempTrip.getEndLon() != 0){
					startAddr = AddrApiUtil.getGPSAdressByGeometryVerSK(tempTrip.getStartLat(), tempTrip.getStartLon());
					startAddrEn = AddrApiUtil.getGPSAdressByGeometryEn(tempTrip.getStartLat(), tempTrip.getStartLon());
				}
				tempTrip.setStartAddr(startAddr);
				tempTrip.setStartAddrEn(startAddrEn);
				if(tempTrip.getEndLat() != 0 && tempTrip.getEndLon() != 0){
					endAddr = AddrApiUtil.getGPSAdressByGeometryVerSK(tempTrip.getEndLat(), tempTrip.getEndLon());
					endAddrEn = AddrApiUtil.getGPSAdressByGeometryEn(tempTrip.getEndLat(), tempTrip.getEndLon());
				}
				tempTrip.setEndAddr(endAddr);
				tempTrip.setEndAddrEn(endAddrEn);
				
				String pushMsg = "%s 차량의 주행이 종료되었습니다.";
				pushMsg = String.format(pushMsg, vehicle.getPlateNum());
				String title = "주행 종료";
				System.out.println(pushMsg);
				
				
				if(vehicle.getCorp().getCorpType().equals("1")){
					
				}else{
					String workingArea = workingAreaTimeService.chkWorkingArea(tempTrip.getDriverKey(),tempTrip.getStartLon(),tempTrip.getStartLat(),tempTrip.getEndLon(),tempTrip.getEndLat());
					tempTrip.setWorkingAreaWarning(workingArea);
					String workingTime = workingAreaTimeService.chkWorkingTime(tempTrip.getDriverKey(),tempTrip.getStartDate().getTime(),tempTrip.getEndDate().getTime());
					tempTrip.setWorkingTimeWarning(workingTime);
				}
				
				vehicleService.vehicleLastestInfoUpdate(vehicle.getVehicleKey(),trip.getDist(),tempTrip.getEndLon()+","+tempTrip.getEndLat(),tempTrip.getEndAddr(),tempTrip.getEndAddrEn(),tempTrip.getEndDate().getTime(),tempTrip.getTripKey(),tempTrip.getDriverKey());
				
				long endTime2 = trip.getEndTime();
				long now = TimeUtil.now()*1000;
				long interval2 = 1000*60*10; //10분
				if(now - endTime2 < interval2){
					pushService.pushChkAccept(PushType.PUSHTYPE_TRIP,tempTrip.getDriverKey(), "", title, pushMsg);
					pushService.insertPushHstr(vehicle.getCorp().getCorpKey(),"2",title,pushMsg);
					
					long mailExpired = 1000*60*60*24*7L;						
					mailBoxService.simgleUserMailBoxInsert(tempTrip.getDriverKey(), title, pushMsg, mailExpired);
				}
				
				
				String startAreaPoint = trip.getStartLon()+" "+trip.getStartLat();
				String startAreaCode = workingAreaTimeService.getCityByPoint(startAreaPoint);
				String endAreaPoint = trip.getEndLon()+" "+trip.getEndLat();
				String endAreaCode = workingAreaTimeService.getCityByPoint(endAreaPoint);
				
				
				tempTrip.setStartGisCode(startAreaCode);
				tempTrip.setEndGisCode(endAreaCode);
				
				dtcService.dtcTripChk(vehicle.getVehicleKey(),trip.getStartTime(),trip.getEndTime());
				
				/*
				try {
					if(mapService.mapMatchedResult(vo.getTripId(),String.valueOf(vvo.getCorp()))) vo.setTripMatched("1");
				} catch (Exception e) {
					vo.setTripMatched("0");
				}
				*/
				
				if(trip.getFco() == 0) tempTrip.setAvgFco(0d);
				else tempTrip.setAvgFco((double)trip.getDist()/(double)trip.getFco());
				
				int emptyItem = itemService.updateItemByTrip(tempTrip);
				
				if(emptyItem > 0) tempTrip.setItemEmpty("1");
				else tempTrip.setItemEmpty("0");
				
				miligeService.updateTotDistance(tempTrip);
				
				String rePart = tempTrip.getPart().equals("0")?String.valueOf(trip.getCurrentPart()):tempTrip.getPart()+","+trip.getCurrentPart();
				tempTrip.setPart(rePart);

				if(rePart.split(",").length == trip.getMaxPart()){
				
					tempTrip.setTripState(3);
					
					scoringService.calcScoring(tempTrip);
					
				}
				
				//tempTrip.setPART1 property
				deviceMsgDao.updateTrip(tempTrip);
				
				if(tempTrip.getTripState() == 3)
					sendConfirm(trip.getDevice(),device.getAppEui(),trip.getTripSeq(),trip.getMaxPart());
				
			}
			
			
			
			
			
		}else if(tripVo instanceof LoraMsg_TRIP1){
			LoraMsg_TRIP1 trip = (LoraMsg_TRIP1) tripVo;
			
			String deviceId = trip.getDevice();
			
			DeviceVO device = deviceDataService.getDeviceFromId(deviceId);
			
			if(null == device.getVehicle()) {
				System.out.println("ignore tdr - device("+deviceId+") is not active");
				return null;
			}
			
			deviceService.updateLatestPacket(device.getDeviceId());
			
			VehicleVO vehicle = device.getVehicle();
			
			if(vehicle == null) {
				System.out.println("ignore tdr - vehicle("+deviceId+") is null");
				return null;
			}
			
			
			if(new Date().getTime() > device.getExpireDate().getTime() ) {
				System.out.println("ignore tdr - device("+deviceId+") payment expired");
				return null;
			}
			
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("searchDeviceKey", device.getDeviceKey());
			param.put("searchTripId", trip.getTripId());
			TripVO tempTrip = tripDataService.getTripFromMsg(param);
			
			Map<String,String> autoDriverMatcher = new HashMap<String,String>();
			boolean initTripPhase = true;
			if(tempTrip == null){
				
				autoDriverMatcher = autoAllocateReportMatch(trip.getStartTime(),vehicle);
				
				String fwVer = device.getHelloFwVer();
				v4.web.vo.TripVO newTripVo = new v4.web.vo.TripVO(device.getDeviceKey(), trip.getTripId(), autoDriverMatcher.get("driver"), vehicle.getVehicleKey() , trip.getStartTime(), 10, "0", 0 , fwVer 
						,autoDriverMatcher.get("needAllocate") 
						,autoDriverMatcher.get("allocateKey")
						,autoDriverMatcher.get("needReport")
						,autoDriverMatcher.get("reportPurpose")
						,autoDriverMatcher.get("reportContents"));
				deviceMsgDao.tripInit(newTripVo);
				tempTrip = tripDataService.getTripFromMsg(param);
				initTripPhase = false;
			}
			
			if(tempTrip.getTripState()==3) {
				sendConfirm(trip.getDevice(),device.getAppEui(),trip.getTripSeq(),trip.getMaxPart());
				return null;
			}
			
			if(tempTrip.getPart().indexOf(String.valueOf(trip.getCurrentPart())) == -1){
				if(initTripPhase){
					autoDriverMatcher = autoAllocateReportMatch(trip.getStartTime(),vehicle);
					String driver = autoDriverMatcher.get("driver");
					
					if(null != driver && !driver.equals(tempTrip.getDriverKey())){
						AccountVO avo = accountDataService.getAccountFromKey(driver, true);
						tempTrip.setDriver(avo);
					}
					tempTrip.setNeedAllocate(autoDriverMatcher.get("needAllocate"));
					tempTrip.setAllocateKey(autoDriverMatcher.get("allocateKey"));
					tempTrip.setNeedReport(autoDriverMatcher.get("needReport"));
					tempTrip.setPurpose(autoDriverMatcher.get("reportPurpose"));
					tempTrip.setContents(autoDriverMatcher.get("reportContents"));
					if(tempTrip.getAccureFlag().equals("0") && tempTrip.getNeedReport().equals("0")){
						tempTrip.setAccureFlag("1");
						accountService.updateAccureData(driver,tempTrip.getDistance(),tempTrip.getStartDate(),tempTrip.getEndDate());
					}
				}
				tempTrip.setPhone(trip.getCp());
				tempTrip.setCoolantTemp(trip.getCoolantTemp());
				tempTrip.setCo2Emission(trip.getCo2Emission());
				tempTrip.setFuelCutTime(trip.getFuelCutTime());
				tempTrip.setHighSpeed(trip.getHighSpeed());
				tempTrip.setMeanSpeed(trip.getMeanSpeed());
				tempTrip.setIdleTime(trip.getIdleTime());
				tempTrip.setEcuVolt(trip.getEcuVolt());
				tempTrip.setDeviceVolt(trip.getDeviceVolt());
				tempTrip.setEcoTime(trip.getEcoTime());
				tempTrip.setEcoSpeedTime(trip.getEcoSpeedTime());
				tempTrip.setPartTotal(trip.getMaxPart());
				tempTrip.setTripState(2);
				
				String rePart = tempTrip.getPart().equals("0")?String.valueOf(trip.getCurrentPart()):tempTrip.getPart()+","+trip.getCurrentPart();
				tempTrip.setPart(rePart);
				if(rePart.split(",").length == trip.getMaxPart()){
					tempTrip.setTripState(3);
					
					scoringService.calcScoring(tempTrip);
					
				}
				
				//tempTrip.setPART2 property
				deviceMsgDao.updateTrip(tempTrip);
				
				if(tempTrip.getTripState() == 3)
					sendConfirm(trip.getDevice(),device.getAppEui(),trip.getTripSeq(),trip.getMaxPart());
				
			}
			
			
			
			
			
		}else if(tripVo instanceof LoraMsg_TRIP2){
			LoraMsg_TRIP2 trip = (LoraMsg_TRIP2) tripVo;
			
			String deviceId = trip.getDevice();
			
			DeviceVO device = deviceDataService.getDeviceFromId(deviceId);
			
			if(null == device.getVehicle()) {
				System.out.println("ignore tdr - device("+deviceId+") is not active");
				return null;
			}
			
			deviceService.updateLatestPacket(device.getDeviceId());
			
			VehicleVO vehicle = device.getVehicle();
			
			if(vehicle == null) {
				System.out.println("ignore tdr - vehicle("+deviceId+") is null");
				return null;
			}
			
			
			if(new Date().getTime() > device.getExpireDate().getTime() ) {
				System.out.println("ignore tdr - device("+deviceId+") payment expired");
				return null;
			}
			
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("searchDeviceKey", device.getDeviceKey());
			param.put("searchTripId", trip.getTripId());
			TripVO tempTrip = tripDataService.getTripFromMsg(param);
			
			Map<String,String> autoDriverMatcher = new HashMap<String,String>();
			boolean initTripPhase = true;
			if(tempTrip == null){
				
				autoDriverMatcher = autoAllocateReportMatch(trip.getStartTime(),vehicle);
				
				String fwVer = device.getHelloFwVer();
				v4.web.vo.TripVO newTripVo = new v4.web.vo.TripVO(device.getDeviceKey(), trip.getTripId(), autoDriverMatcher.get("driver"), vehicle.getVehicleKey() , trip.getStartTime(), 10, "0", 0 , fwVer 
						,autoDriverMatcher.get("needAllocate") 
						,autoDriverMatcher.get("allocateKey")
						,autoDriverMatcher.get("needReport")
						,autoDriverMatcher.get("reportPurpose")
						,autoDriverMatcher.get("reportContents"));
				deviceMsgDao.tripInit(newTripVo);
				tempTrip = tripDataService.getTripFromMsg(param);
				initTripPhase = false;
			}
			
			if(tempTrip.getTripState()==3) {
				sendConfirm(trip.getDevice(),device.getAppEui(),trip.getTripSeq(),trip.getMaxPart());
				return null;
			}
			
			if(tempTrip.getPart().indexOf(String.valueOf(trip.getCurrentPart())) == -1){
				if(initTripPhase){
					autoDriverMatcher = autoAllocateReportMatch(trip.getStartTime(),vehicle);
					String driver = autoDriverMatcher.get("driver");
					
					if(null != driver && !driver.equals(tempTrip.getDriverKey())){
						AccountVO avo = accountDataService.getAccountFromKey(driver, true);
						tempTrip.setDriver(avo);
					}
					tempTrip.setNeedAllocate(autoDriverMatcher.get("needAllocate"));
					tempTrip.setAllocateKey(autoDriverMatcher.get("allocateKey"));
					tempTrip.setNeedReport(autoDriverMatcher.get("needReport"));
					tempTrip.setPurpose(autoDriverMatcher.get("reportPurpose"));
					tempTrip.setContents(autoDriverMatcher.get("reportContents"));
					if(tempTrip.getAccureFlag().equals("0") && tempTrip.getNeedReport().equals("0")){
						tempTrip.setAccureFlag("1");
						accountService.updateAccureData(driver,tempTrip.getDistance(),tempTrip.getStartDate(),tempTrip.getEndDate());
					}
				}
				
				
				tempTrip.setUnder20(trip.getUnder20());
				tempTrip.setUnder40(trip.getUnder40());
				tempTrip.setUnder60(trip.getUnder60());
				tempTrip.setUnder80(trip.getUnder80());
				tempTrip.setUnder100(trip.getUnder100());
				tempTrip.setUnder120(trip.getUnder120());
				tempTrip.setUnder140(trip.getUnder140());
				tempTrip.setOver140(trip.getOver140());
				tempTrip.setPartTotal(trip.getMaxPart());
				tempTrip.setTripState(2);
				
				String rePart = tempTrip.getPart().equals("0")?String.valueOf(trip.getCurrentPart()):tempTrip.getPart()+","+trip.getCurrentPart();
				tempTrip.setPart(rePart);
				if(rePart.split(",").length == trip.getMaxPart()){
					tempTrip.setTripState(3);
					
					scoringService.calcScoring(tempTrip);
					
				}
				
				deviceMsgDao.updateTrip(tempTrip);
				
				if(tempTrip.getTripState() == 3)
					sendConfirm(trip.getDevice(),device.getAppEui(),trip.getTripSeq(),trip.getMaxPart());
				
			}
			
		}
		
		return null;
	}

	private void sendConfirm(String device,String appEui, int tripSeq, int totPart) {
		/*
		try {
			
			LoraMsg_CONFIRM_TRIP confirm = new LoraMsg_CONFIRM_TRIP(tripSeq,totPart);
			List<String> confirmMsg = confirm.getPackets();
			LoraMsgConnector connector = new LoraMsgConnector(appEui, Globals.LORA_KEYS.get(appEui));
			
			
			for(String m : confirmMsg){
				echo(device,"TODO","TODO",m,confirm.toString());
				Map<String,String> res = connector.loraDeviceMsgSend(device, m);
				System.out.println(res);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		*/
	}

	@Override
	public String provision(Object provisionVo) {
		/*
		LoraMsg_PROVISION vo = (LoraMsg_PROVISION)provisionVo;
		
		try {
			
			String deviceId = vo.getDevice();
			
			DeviceVO device = deviceDataService.getDeviceFromId(deviceId);
			
			if(device == null) return null;
			deviceService.updateLatestPacket(device.getDeviceId());
			deviceService.setDeviceProvision(vo);
			
			
			
			LoraMsg_CONFIRM_PROVISION confirm = new LoraMsg_CONFIRM_PROVISION(vo.getPacket());
			List<String> confirmMsg = confirm.getPackets();
			LoraMsgConnector connector = new LoraMsgConnector(device.getAppEui(), Globals.LORA_KEYS.get(device.getAppEui()));
			
			try {
				for(String m : confirmMsg){
					echo(deviceId,"TODO","TODO",m,confirm.toString());
					Map<String,String> res = connector.loraDeviceMsgSend(deviceId, m);
					System.out.println(res);
				} 
			}catch (Exception e) {
				e.printStackTrace();
			}
				
			
		} catch (BizException e) {
			e.printStackTrace();
		}
		*/
		return null;
	}

	@Override
	public String locationReport(Object locationReportVo) {
		// NOT USED
		return null;
	}

	@Override
	public String hello(Object helloVo) {
		LoraMsg_HELLO vo = (LoraMsg_HELLO)helloVo;
		
		String deviceId = vo.getDevice();
		
		try {
			DeviceVO device = deviceDataService.getDeviceFromId(deviceId);
			
			if(device == null) return null;
			deviceService.updateLatestPacket(device.getDeviceId());
			deviceService.setDeviceHello(vo);
			
		} catch (BizException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public String fault(Object fualtVo) {
		LoraMsg_FAULT vo = (LoraMsg_FAULT)fualtVo;
		
		try {
			
			String deviceId = vo.getDevice();
			
			DeviceVO device = deviceDataService.getDeviceFromId(deviceId);
			
			if(device == null) return null;
			deviceService.updateLatestPacket(device.getDeviceId());
			
			deviceService.setDeviceFault(vo);
			
		} catch (BizException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	private static List<String> dtcPushMemory = new ArrayList<String>();
	
	@Override
	public String dtc(Object dtcVo) {
		
		LoraMsg_DTC vo = (LoraMsg_DTC)dtcVo;
		
		String deviceId = vo.getDevice();
		
		try {
			DeviceVO device = deviceDataService.getDeviceFromId(deviceId);
			deviceService.updateLatestPacket(device.getDeviceId());
			
			VehicleVO vehicle = device.getVehicle();
			if(null == vehicle) {
				System.out.println("ignore dtc - device("+deviceId+") is not active");
				return null;
			}
			
				
			if(new Date().getTime() > device.getExpireDate().getTime() ) {
				System.out.println("ignore tdr - device("+deviceId+") payment expired");
				return null;
			}
			
			DtcVO dtc = new DtcVO();
			dtc.setCurTime(vo.getCurTime());
			dtc.setDtcCode(vo.getDtcCode());
			dtc.setDtcKind(vo.getDtcKind());
			dtc.setDtcType(vo.getDtcType());
			
			String dtcKey = dtcService.saveDtc(vehicle,dtc);
			
			List<Map<String,String>> rtvList = dtcService.getDtcList(vehicle.getVehicleKey(),dtcKey,"");
			
			if(rtvList.size() > 0){
				String dtcCode = rtvList.get(0).get("dtcCode");
				if(!StrUtil.isNullToEmpty(dtcCode)){
					if(!dtcPushMemory.contains(dtcKey)){
						
						
						String pushAccount = "";
						
						if(vehicle.getCorp().getCorpType().equals("1")){
							pushAccount = vehicle.getDefaultAccountKey();
						}else{
							pushAccount = vehicle.getManagerKey();
						}
						
						dtcPushMemory.add(dtcKey);
						
						String pushMsg = "%s 차량에서 고장이 발생하였습니다. 차량진단 목록을 확인해주세요.";
						pushMsg = String.format(pushMsg, vehicle.getPlateNum());
						//type maintenance = 3
						String pushType = PushType.PUSHTYPE_DTC;
						String title = "고장 감지";
						pushService.pushChkAccept(pushType, pushAccount, "", title, pushMsg);
						pushService.insertPushHstr(vehicle.getCorp().getCorpKey(), pushType, title, pushMsg);
						
						Map<String,Object> param = new HashMap<String,Object>();						
						param.put("vehicleKey", vehicle.getVehicleKey());
						param.put("eventTime", new Date().getTime());
						param.put("eventValue", dtcKey);
						
						timeLineService.insertTimeLineEvent("trouble","", param);
						long mailExpired = 1000*60*60*24*7L;						
						mailBoxService.simgleUserMailBoxInsert(pushAccount, title, pushMsg, mailExpired);
						
						
					}else{
						dtcPushMemory.remove(dtcKey);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public String attach(Object attachVo) {
		LoraMsg_ATTACH vo = (LoraMsg_ATTACH)attachVo;
		
		String deviceId = vo.getDevice();
		
		try {
			DeviceVO device = deviceDataService.getDeviceFromId(deviceId);
			deviceService.updateLatestPacket(device.getDeviceId());
			VehicleVO vehicle = device.getVehicle();
			if(null == vehicle) {
				System.out.println("ignore dtc - device("+deviceId+") is not active");
				return null;
			}
			
				
			if(new Date().getTime() > device.getExpireDate().getTime() ) {
				System.out.println("ignore tdr - device("+deviceId+") payment expired");
				return null;
			}
			
			AttachVO avo = new AttachVO();
			avo.setAttachTime(vo.getAttachTime());
			avo.setAttachType(vo.getAttachType());
			avo.setDettachTime(vo.getDettachTime());
			avo.setDettachType(vo.getDettachType());
			
			attachService.saveAttachInfo(avo, vehicle, device);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return null;
	}
	
	@Autowired
	protected ApplicationContext act;
	
	public Object getBean(Class<?> c, Object... args) {
		return (Object)act.getBean(c,args);
	}

	public Object getBean(String c, Object... args) {
		return (Object)act.getBean(c,args);
	}
	
	private void echo(String fr,String ct, String ri, String payload, String msg) throws Exception{
		EchoHandler echo = (EchoHandler)getBean("echoHandler");
		echo.handleTextMessage(null
				, new TextMessage(("from[ThingPulg] - "+fr+" - tpTime : "+ct+" , tpRqi : "+ri
				+"\n payload : "+payload
				+"\n parse : "+msg)),fr);
	}

	private Map<String,String> autoAllocateReportMatch(long allocateTime , VehicleVO vehicle) throws BizException{
		String driver = null;
		String needAllocate = "0";
		String needReport = "1";
		String allocateKey = null;
		String reportPurpose = "1";
		String reportContents = "";
		
		Map<String,String> rtv = new HashMap<String,String>();
		
		if(vehicle.getCorp().getCorpType().equals("1")){
			driver = vehicle.getDefaultAccountKey();
			needReport = "0";
		}else{
			//autoReport 체크
			needReport = vehicle.getAutoReport().equals("1")?"0":"1";
			reportPurpose = vehicle.getAutoReportPurpose();
			reportContents = vehicle.getAutoReportContents();
			driver = vehicle.getDefaultAccountKey();
			
			//배차 필요시스템 체크
			needAllocate = vehicle.getCorp().getApproval().getAllocateApproval();
			
			//배차 필요시스템이라도 지정차량이라면 배차필요없음
			if(needAllocate.equals("1") && vehicle.getFixed().equals("1")){ 
				needAllocate = "0";
				driver = vehicle.getFixedAccountKey();
			}
			
			if(needAllocate.equals("1")){
				//배차 필요면 배차시스템에서 현시간으로 배차사용자 추정
				Map<String,Object> param = new HashMap<String,Object>();
				param.put("searchVehicleKey", vehicle.getVehicleKey());
				param.put("searchAllocateDate", allocateTime);
				param.put("searchAllocateStatus", "complate");
				param.put("limit", 1);
				param.put("offset", 0);
				param.put("searchOrder", "startDate");
				AllocateVO avo = allocateDataService.getAllocate(param,null);
				
				//배차시스템인데 배차가 되어있으면 운행일지 자동작성 갱신
				if(avo != null){
					allocateKey = avo.getAllocateKey();
					driver = avo.getAccount().getAccountKey();
					needReport = "0";
					reportPurpose = avo.getPurpose();
					reportContents = avo.getContents();						
				}
				
			}
			
			rtv.put("driver",driver);
			rtv.put("needAllocate",needAllocate);
			rtv.put("needReport",needReport);
			rtv.put("allocateKey",allocateKey);
			rtv.put("reportPurpose",reportPurpose);
			rtv.put("reportContents",reportContents);
		}
		
		
		
		
		
		return rtv;
		
	}
}
