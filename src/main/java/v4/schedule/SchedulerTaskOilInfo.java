package v4.schedule;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

@Component
public class SchedulerTaskOilInfo {
	
	private Log logger = LogFactory.getLog(getClass());
	/*
	@Autowired
	private OilService oilService;
	
	@Scheduled(cron="0 0 * * * *") 
	public void getOilPriceByAPI(){
		if(Globals.MODE.equals("2") || Globals.MODE.equals("1")){
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			
			logger.debug("getOilPriceByAPI start : " + df.format(calendar.getTime()));
			
			try {
				String opinetUrl = "http://www.opinet.co.kr/api/avgAllPrice.do?out=json&code=";
				String apiKey = "F108161223";
				
				String rtvStr = getJSONData(opinetUrl+apiKey);
				
				System.out.println(rtvStr);
				
				JSONObject jObj = new JSONObject(rtvStr);
				JSONArray jArry = jObj.getJSONObject("RESULT").getJSONArray("OIL");
				
				OilVO vo = new OilVO();
				
				for(int i = 0 ; i < jArry.length() ; i ++){
					JSONObject data = jArry.getJSONObject(i);
					String tradeDt = data.getString("TRADE_DT");
					String prodCd = data.getString("PRODCD"); 
					String price =  data.getString("PRICE");
					
					vo.setTradeDt(tradeDt);
					
					if(prodCd.equals("B034")) vo.setGasolineHigh(price);
					else if(prodCd.equals("B027")) vo.setGasoline(price);
					else if(prodCd.equals("D047")) vo.setDiesel(price);
					else if(prodCd.equals("K015")) vo.setLpg(price);
					
				}
		    	
				oilService.insertOilPrice(vo);
				
			} catch (Exception e) {
				logger.error(e.getMessage());
				e.printStackTrace();
			}
			
			logger.debug("getOilPriceByAPI end"); 
		}
			
	}
	
	public String getJSONData(String apiURL) throws Exception {
		String jsonString = new String();
		String buf;
		URL url = new URL(apiURL);
		URLConnection conn = url.openConnection();
		BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
		while ((buf = br.readLine()) != null) {
			jsonString += buf;
		}
		return jsonString;
	}
	*/
}

