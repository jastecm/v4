package v4.schedule;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jastecm.string.StrUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.common.Globals;
import com.util.SMS;

@Controller
public class SchedulerRestTask {
	
	private Log logger = LogFactory.getLog(getClass());
	
	public String path = "/api/schedulerRestTask";
	public String rtvPage ="/common/emptyPage";
	
	@RequestMapping(value = "/api/hello")
	public String hello(HttpServletRequest request,HttpServletResponse response,
			ModelMap model) throws Exception {
		
		model.addAttribute("result", "1");
		return rtvPage;
		
	}
	
	@RequestMapping(value = "/api/serverEmergencySms")
	public String serverEmergencySms(HttpServletRequest request,HttpServletResponse response,
			ModelMap model) throws Exception {
		
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		logger.debug("serverEmergencySms start  " + df.format(c.getTime()));
		
		String result = "";
		String msg = "";
		try {
			try {
				msg = (String)request.getParameter("msg");
				logger.debug("serverEmergencySms - before converting msg : " + msg);
				//msg = URLDecoder.decode(msg, "UTF-8");
				logger.debug("serverEmergencySms - converted msg : " + msg);
			} catch (Exception e) {
				logger.debug("serverEmergencySms - msg convert fail");
				result = "0";
				return rtvPage;
			}
			
			
			try {
				SMS sms = new SMS();
				
				String phone = Globals.VDAS_MANAGER1+";"+Globals.VDAS_MANAGER4;//+";"+Globals.VDAS_MANAGER5;
				if(!StrUtil.isNullToEmpty(msg)) sms.sms_send("1", phone, msg, "0", "0");
				result = "1";
			} catch (Exception e) {
				e.printStackTrace();
				result = "0";
				return rtvPage;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			result = "0";
		}
		
		if(result.equals("1")) logger.debug("server emergency sms send result - complate");
		else  logger.debug("server emergency sms send result - fail");
		
		model.addAttribute("result", result);
		
		return rtvPage;
		
	}
}

