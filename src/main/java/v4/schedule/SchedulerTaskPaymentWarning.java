package v4.schedule;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

//@Component
public class SchedulerTaskPaymentWarning {
	
	private Log logger = LogFactory.getLog(getClass());
	/*
	@Autowired
	private RestDeviceService restDeviceService;
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private CommonService commonService;
	
	@Autowired
	private PushService pushService;
	
	@Scheduled(cron="0 0 14 * * *") 
	//@Scheduled(cron="0 * * * * *")
	public void getPaymenyWarning(){
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
		
		logger.debug("paymenyWarning start : " + df.format(calendar.getTime()));
		
		try {
			String pushMsg = "%s/%s 차량의 이용권이 %d일 남았습니다. 만료 후에는 운행 데이터가 기록되지 않으니 이용권을 꼭 구매해주세요.";
			String msgBoxMsg = "%s/%s 차량의 이용권이 %d일 남았습니다. %s 23:59 이후 운행 데이터는 기록되지 않으니 이용권을 꼭 구매해주세요.";
			String smsMsg = "[뷰카프로 기간만료알림]\n %s 차량의 이용권이 %s일 남았습니다.";
			String pushMsgStop = "%s/%s 차량의 이용권 결제가 만료되어 %s 23:59 이후 운행 데이터는 기록되지 않으니 이용권을 꼭 구매해주세요.";
			String msgBoxMsgStop = "%s/%s 차량의 이용권 결제가 만료되어 %s 23:59 이후 운행 데이터는 기록되지 않으니 이용권을 꼭 구매해주세요.";
			String smsMsgStop = "[뷰카프로]\n %s 차량의 이용권이 만료되었습니다.";
			String mailTitle = "[뷰카프로] 서비스 기간만료 알림";
			String corpSmsMsg = "[뷰카프로] %s의 서비스 이용권이 만료 예정인 차량이 있습니다. 웹에서 확인해주세요.";
			
			
			List<Map<String,Object>> rtvList = restDeviceService.getDeviceExpireWarningList(0);
			
			for(Map<String,Object> vo : rtvList){
				String accountKey = (String)vo.get("accountKey");
				String plateNum = (String)vo.get("plateNum");
				String modelMaster = (String)vo.get("modelMaster");
				Date expireDate = (Date)vo.get("expireDate");
				String expireDateStr = df2.format(new Date(expireDate.getTime()));
				String name = (String)vo.get("name");
				String phone = (String)vo.get("mobilePhone");
				String corpNm = (String)vo.get("corpName");
				String authMail = (String)vo.get("authMail");
				int corp = (Integer)vo.get("corp"); 
				long remainDay = (Long)vo.get("remainDay");
				String lastestPaymentType = (String)vo.get("lastestPaymentType");
				
				if(lastestPaymentType == null || lastestPaymentType.equals("loramonthlypayment")) continue;
				
				String rtvPushMsg = "";
				String rtvSmsMsg = "";
					
				rtvPushMsg = String.format(pushMsg, plateNum,modelMaster,remainDay);
				rtvSmsMsg = String.format(smsMsg,plateNum,remainDay);
				
				pushService.push(accountKey, "", "이용권 만료", rtvPushMsg);
				
				SMS sms = new SMS();
				String smsResult = sms.sms_send("", phone,rtvSmsMsg, "0", "0");
				System.out.println(smsResult);
				
			}
			
			rtvList = restDeviceService.getDeviceExpireList(0);
			
			for(Map<String,Object> vo : rtvList){
				String accountKey = (String)vo.get("accountKey");
				String plateNum = (String)vo.get("plateNum");
				String modelMaster = (String)vo.get("modelMaster");
				Date expireDate = (Date)vo.get("expireDate");
				String expireDateStr = df2.format(new Date(expireDate.getTime()));
				String name = (String)vo.get("name");
				String phone = (String)vo.get("mobilePhone");
				String corpNm = (String)vo.get("corpName");
				String authMail = (String)vo.get("authMail");
				int corp = (Integer)vo.get("corp"); 
				long remainDay = (Long)vo.get("remainDay");
				String lastestPaymentType = (String)vo.get("lastestPaymentType");
				
				String rtvPushMsg = "";
				String rtvSmsMsg = "";
				
				rtvPushMsg = String.format(pushMsgStop, plateNum,modelMaster,expireDateStr);
				rtvSmsMsg = String.format(smsMsgStop,plateNum);
				
				pushService.push(accountKey, "", "이용권 만료", rtvPushMsg);
				
				SMS sms = new SMS();
				String smsResult = sms.sms_send("", phone,rtvSmsMsg, "0", "0");
				System.out.println(smsResult);
				
			}
			
			List<Map<String, Object>> corpList = commonService.getCorpList(new HashMap<String,String>());
			
			for(Map<String,Object> corp : corpList){
				int corpId = (Integer)corp.get("seq"); 
				if(corpId != 0){
					rtvList = restDeviceService.getDeviceExpireWarningList(corpId);
					if(rtvList.size() > 0){
						
						String authMail = accountService.getMasterMailInCorp(corpId);
						String authSms = accountService.getMasterSmsInCorp(corpId);
						
						
						String corpNm = (String)corp.get("corpName");
						
						String rtvCorpSmsMsg = String.format(corpSmsMsg, corpNm);
						SMS sms = new SMS();
						String smsResult = sms.sms_send("", authSms,rtvCorpSmsMsg, "0", "0");
						System.out.println(smsResult);
						
						
						String mailMsg = createMailMsg(corpNm,rtvList);
						if (Globals.WIN) {
							
							MailUtil mu = new MailUtil();
							String from = "upjjw2682@gmail.com";
							String fromName = "정종웅";
							String[] to = { authMail };
							String[] cc = {};
							String[] bcc = {};
							String subject = mailTitle;
							String content = mailMsg;
							File[] attachFiles = {};
							mu.sendSmtpMail(from, fromName, to, cc, bcc, subject,
									content, attachFiles);
									
						} else {
							MailUtilServer mu = new MailUtilServer();
							String from = Globals.MAIL_SENDER;
							String fromName = "뷰카";
							String[] to = { authMail };
							String[] cc = {};
							String[] bcc = {};
							String subject = mailTitle;
							String content = mailMsg;
							File[] attachFiles = {};
							mu.sendSmtpMail(from, fromName, to, cc, bcc, subject,
									content, attachFiles);
						}
						
					}
				}
			}
			
			for(Map<String,Object> corp : corpList){
				int corpId = (Integer)corp.get("seq"); 
				if(corpId != 0){
					rtvList = restDeviceService.getDeviceExpireList(corpId);
					if(rtvList.size() > 0){
						
						String authMail = accountService.getMasterMailInCorp(corpId);
						String authSms = accountService.getMasterSmsInCorp(corpId);
						
						
						String corpNm = (String)corp.get("corpName");
						
						String rtvCorpSmsMsg = String.format(corpSmsMsg, corpNm);
						SMS sms = new SMS();
						String smsResult = sms.sms_send("", authSms,rtvCorpSmsMsg, "0", "0");
						System.out.println(smsResult);
						
						
						String mailMsg = createMailStopMsg(corpNm,rtvList);
						if (Globals.WIN) {
							MailUtil mu = new MailUtil();
							String from = "upjjw2682@gmail.com";
							String fromName = "정종웅";
							String[] to = { authMail };
							String[] cc = {};
							String[] bcc = {};
							String subject = mailTitle;
							String content = mailMsg;
							File[] attachFiles = {};
							mu.sendSmtpMail(from, fromName, to, cc, bcc, subject,
									content, attachFiles);
						} else {
							MailUtilServer mu = new MailUtilServer();
							String from = Globals.MAIL_SENDER;
							String fromName = "뷰카";
							String[] to = { authMail };
							String[] cc = {};
							String[] bcc = {};
							String subject = mailTitle;
							String content = mailMsg;
							File[] attachFiles = {};
							mu.sendSmtpMail(from, fromName, to, cc, bcc, subject,
									content, attachFiles);
						}
						
					}
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		
		logger.debug("getMiligeWaringList end"); 
	}
			
	public String createMailMsg(String corpNm , List<Map<String,Object>> rtvList){
		StringBuffer sb = new StringBuffer();
		
		sb.append("<html>");
		sb.append("	<head>");
		sb.append("		<title>confirm1</title>");
		sb.append("		<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");
		sb.append("		<style type='text/css'>");
		sb.append("		p {color:#ed1c24;}");
		sb.append("		</style>");
		sb.append("	</head>");
		sb.append("	<body bgcolor='#FFFFFF' leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'>");
		sb.append("		<table id='Table_01' width='740' height='642' border='0' cellpadding='0' cellspacing='0'>");
		sb.append("			<tr>");
		sb.append("				<td>");
		sb.append("					<img src='"+Globals.VDAS_URL_PREFIX+"/common/images/confirm1_01.gif' width='40' height='68' alt=''></td>");
		sb.append("				<td>");
		sb.append("					<img src='"+Globals.VDAS_URL_PREFIX+"/common/images/confirm1_02.gif' width='660' height='68' alt=''></td>");
		sb.append("				<td>");
		sb.append("					<img src='"+Globals.VDAS_URL_PREFIX+"/common/images/confirm1_03.gif' width='40' height='68' alt=''></td>");
		sb.append("			</tr>");
		sb.append("			<tr>");
		sb.append("				<td>");
		sb.append("					<img src='"+Globals.VDAS_URL_PREFIX+"/common/images/confirm1_04.gif' width='40' height='40' alt=''></td>");
		sb.append("				<td>");
		sb.append(					"안녕하세요. "+corpNm+" 담당자님, 뷰카입니다.");
		sb.append("				</td>");
		sb.append("				<td>");
		sb.append("					<img src='"+Globals.VDAS_URL_PREFIX+"/common/images/confirm1_06.gif' width='40' height='40' alt=''>");
		sb.append("				</td>");
		sb.append("			</tr>");
		sb.append("			<tr>");
		sb.append("				<td>");
		sb.append("					<img src='"+Globals.VDAS_URL_PREFIX+"/common/images/confirm1_07.gif' width='40' height='82' alt=''></td>");
		sb.append("				<td>");
		
		sb.append("뷰카 서비스 이용권이 만료 예정인 차량이 있습니다.<br>");
		sb.append("이용권 만료 후에는 운행 데이터는 기록되지 않으니 이용권을 꼭 구매해주세요.<br><br>");
		
		sb.append("- 이용권 만료 예정 차량<br>");
		SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
		
		for(Map<String,Object> vo : rtvList){
			String accountKey = (String)vo.get("accountKey");
			String plateNum = (String)vo.get("plateNum");
			String modelMaster = (String)vo.get("modelMaster");
			Date expireDate = (Date)vo.get("expireDate");
			String expireDateStr = df2.format(new Date(expireDate.getTime()));
			String name = (String)vo.get("name");
			String phone = (String)vo.get("mobilePhone");
			long remainDay = (Long)vo.get("remainDay");
			
			String msg = "<br>%s/%s/만료일 : %s 23:59";
			
			sb.append(String.format(msg,plateNum,modelMaster,expireDateStr));
		}
		
		sb.append("				</td>");
		sb.append("				<td>");
		sb.append("					<img src='"+Globals.VDAS_URL_PREFIX+"/common/images/confirm1_09.gif' width='40' height='82' alt=''></td>");
		sb.append("			</tr>");
		sb.append("			<tr>");
		sb.append("				<td colspan='3'>");
		sb.append("					<img src='"+Globals.VDAS_URL_PREFIX+"/common/images/confirm1_10.gif' width='740' height='97' alt=''></td>");
		sb.append("			</tr>");
		sb.append("			<tr>");
		sb.append("				<td>");
		sb.append("					<img src='"+Globals.VDAS_URL_PREFIX+"/common/images/confirm1_11.gif' width='40' height='142' alt=''></td>");
		sb.append("				<td>");
		sb.append("					<img src='"+Globals.VDAS_URL_PREFIX+"/common/images/confirm1_12.gif' width='660' height='142' alt=''></td>");
		sb.append("				<td>");
		sb.append("					<img src='"+Globals.VDAS_URL_PREFIX+"/common/images/confirm1_13.png' width='40' height='142' alt=''></td>");
		sb.append("			</tr>");
		sb.append("		</table>");
		sb.append("	</body>");
		sb.append("</html>");
		
		return sb.toString();
	}
	
	public String createMailStopMsg(String corpNm , List<Map<String,Object>> rtvList){
		StringBuffer sb = new StringBuffer();
		
		sb.append("<html>");
		sb.append("	<head>");
		sb.append("		<title>confirm1</title>");
		sb.append("		<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");
		sb.append("		<style type='text/css'>");
		sb.append("		p {color:#ed1c24;}");
		sb.append("		</style>");
		sb.append("	</head>");
		sb.append("	<body bgcolor='#FFFFFF' leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'>");
		sb.append("		<table id='Table_01' width='740' height='642' border='0' cellpadding='0' cellspacing='0'>");
		sb.append("			<tr>");
		sb.append("				<td>");
		sb.append("					<img src='"+Globals.VDAS_URL_PREFIX+"/common/images/confirm1_01.gif' width='40' height='68' alt=''></td>");
		sb.append("				<td>");
		sb.append("					<img src='"+Globals.VDAS_URL_PREFIX+"/common/images/confirm1_02.gif' width='660' height='68' alt=''></td>");
		sb.append("				<td>");
		sb.append("					<img src='"+Globals.VDAS_URL_PREFIX+"/common/images/confirm1_03.gif' width='40' height='68' alt=''></td>");
		sb.append("			</tr>");
		sb.append("			<tr>");
		sb.append("				<td>");
		sb.append("					<img src='"+Globals.VDAS_URL_PREFIX+"/common/images/confirm1_04.gif' width='40' height='40' alt=''></td>");
		sb.append("				<td>");
		sb.append(					"안녕하세요. "+corpNm+" 담당자님, 뷰카입니다.");
		sb.append("				</td>");
		sb.append("				<td>");
		sb.append("					<img src='"+Globals.VDAS_URL_PREFIX+"/common/images/confirm1_06.gif' width='40' height='40' alt=''>");
		sb.append("				</td>");
		sb.append("			</tr>");
		sb.append("			<tr>");
		sb.append("				<td>");
		sb.append("					<img src='"+Globals.VDAS_URL_PREFIX+"/common/images/confirm1_07.gif' width='40' height='82' alt=''></td>");
		sb.append("				<td>");
		
		sb.append("뷰카 서비스 이용권이 만료된 차량이 있습니다.<br>");
		sb.append("이용권 만료 후에는 운행 데이터는 기록되지 않으니 이용권을 꼭 구매해주세요.<br><br>");
		
		sb.append("- 이용권 만료 차량<br>");
		SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
		
		for(Map<String,Object> vo : rtvList){
			String accountKey = (String)vo.get("accountKey");
			String plateNum = (String)vo.get("plateNum");
			String modelMaster = (String)vo.get("modelMaster");
			Date expireDate = (Date)vo.get("expireDate");
			String expireDateStr = df2.format(new Date(expireDate.getTime()));
			String name = (String)vo.get("name");
			String phone = (String)vo.get("mobilePhone");
			long remainDay = (Long)vo.get("remainDay");
			
			String msg = "<br>%s/%s/만료일 : %s 23:59";
			
			sb.append(String.format(msg,plateNum,modelMaster,expireDateStr));
		}
		
		sb.append("				</td>");
		sb.append("				<td>");
		sb.append("					<img src='"+Globals.VDAS_URL_PREFIX+"/common/images/confirm1_09.gif' width='40' height='82' alt=''></td>");
		sb.append("			</tr>");
		sb.append("			<tr>");
		sb.append("				<td colspan='3'>");
		sb.append("					<img src='"+Globals.VDAS_URL_PREFIX+"/common/images/confirm1_10.gif' width='740' height='97' alt=''></td>");
		sb.append("			</tr>");
		sb.append("			<tr>");
		sb.append("				<td>");
		sb.append("					<img src='"+Globals.VDAS_URL_PREFIX+"/common/images/confirm1_11.gif' width='40' height='142' alt=''></td>");
		sb.append("				<td>");
		sb.append("					<img src='"+Globals.VDAS_URL_PREFIX+"/common/images/confirm1_12.gif' width='660' height='142' alt=''></td>");
		sb.append("				<td>");
		sb.append("					<img src='"+Globals.VDAS_URL_PREFIX+"/common/images/confirm1_13.png' width='40' height='142' alt=''></td>");
		sb.append("			</tr>");
		sb.append("		</table>");
		sb.append("	</body>");
		sb.append("</html>");
		
		return sb.toString();
	}
	*/
}

