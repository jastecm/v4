package v4.schedule;

import org.springframework.stereotype.Component;

@Component
public class SchedulerIOSReceiptPulling {
	/*
	private Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	private RestPaymentService restPaymentService;
	private static String iosPw = "a6648e2f5dab4ab8bd7399ecc4e8ffef";
	
	//@Scheduled(cron="0 * * * * *") 
	@Scheduled(cron="0 0 4 * * *")
	public void SchedulerIOSReceiptPullingProcess(){
		if(Globals.MODE.equals("2")){
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			
			logger.debug("SchedulerIOSReceiptPulling start : " + df.format(calendar.getTime()));
			
			try {
				
				List<Map<String,String>> arr = restPaymentService.getRenewalPaymentIOSList();
				
				for(Map<String,String> latest : arr){
					String accountKey = latest.get("accountKey");
					String payload = latest.get("payload");
					String deviceSn = latest.get("renewalTarget");
					
					Map<String,String> rtv = call(payload, this.iosPw);
					
					String res = rtv.get("resMsg");
					try {
						
						JSONObject resObj = new JSONObject(res);
						
						System.out.println(payload);
						System.out.println(resObj.toString(4));
						
						int status = resObj.getInt("status");
						String environment = resObj.getString("environment");
						
						if(status == 0 && (environment.equals("Sandbox") || environment.toLowerCase().equals("sandbox")) ){
						//if(status == 0 && false ){
							
						}else{
							if(status == 0){
								
								JSONObject receipt = resObj.getJSONObject("receipt");
								boolean subOn = false;
								
								if(resObj.has("latest_receipt_info")){
									//subscription receipt
									JSONArray latestReceipt = resObj.getJSONArray("latest_receipt_info");
									JSONArray pending_renewal_info = resObj.getJSONArray("pending_renewal_info");
									JSONObject pending_renewal_info_obj = pending_renewal_info.getJSONObject(0);
									                    		   
									for(int i = 0 ; i < latestReceipt.length() ; i++){
										JSONObject jObj = latestReceipt.getJSONObject(i);
										
										boolean subOnChk = restPaymentService.paymentIOS(jObj,accountKey,deviceSn,pending_renewal_info_obj);
										
										if(!subOn && subOnChk) subOn = true;
									}
									
									if(subOn) restPaymentService.paymentIOSRenewalbleUpdate(accountKey,"1");
									else restPaymentService.paymentIOSRenewalbleUpdate(accountKey,"0");
									
								}else{
									//none subscription receipt
									JSONArray receiptArr = receipt.getJSONArray("in_app");
									for(int i = 0 ; i < receiptArr.length() ; i++){
										JSONObject jObj = receiptArr.getJSONObject(i);
										restPaymentService.paymentIOS(jObj,accountKey,deviceSn,null);
									}
								}
								
							}else{
								logger.error("invalid receipt - " + status);
							}
						}
						
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			logger.debug("SchedulerIOSReceiptPulling end - " + df.format(calendar.getTime())); 
		}
			
	}
	
	
	
	public static Map<String,String> call(String receiptData , String p) throws IOException{
		//https://sandbox.itunes.apple.com/verifyReceipt
		//https://buy.itunes.apple.com/verifyReceipt
		String url = "https://sandbox.itunes.apple.com/verifyReceipt";
		String method = "POST";
		
		ObjectMapper mapper = new ObjectMapper();
		
		Map<String,String> rtvObj = new HashMap<String,String>();
		rtvObj.put("resCode",null);
		rtvObj.put("resMsg",null);
		
		Map<String, String> query = new HashMap<String, String>();
		query.put("receipt-data", receiptData);
		query.put("password", p);
		
		
		URL obj = new URL(url);
		
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setDoInput(true);
		con.setDoOutput(true);
		
		// optional default is GET
		con.setRequestMethod(method);
		
		
		//add request header			
		con.setRequestProperty("content-type", "application/json;charset=UTF-8");  
		
		if(query != null){
			String bodyStr = "";
			bodyStr = mapper.writeValueAsString(query);
			
			byte[] outputInBytes = bodyStr.getBytes("UTF-8");
			OutputStream os = con.getOutputStream();
			os.write( outputInBytes );    
			os.close();
		}
		
		System.out.println("Sending \""+method+"\" request to URL : " + url);
		
		int responseCode = con.getResponseCode();
		
		System.out.println("Response Code : " + responseCode);
		rtvObj.put("resCode",String.valueOf(responseCode));
		
		if(responseCode < 300){
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream(),"UTF-8"));
			String inputLine;
			StringBuffer response = new StringBuffer();
	
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			System.out.println(response.toString());
			rtvObj.put("resMsg",response.toString());
		}else{
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getErrorStream(),"UTF-8"));
			String inputLine;
			StringBuffer response = new StringBuffer();
	
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			System.out.println(response.toString());
			rtvObj.put("resMsg",response.toString());
		}
			
		
		return rtvObj;
	}
	*/
}

