package v4.schedule;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import v4.web.rest.service.MailBoxService;

@Component
public class SchedulerTaskMailBoxExpired {
	
	private Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	private MailBoxService mailBoxService;
	
	@Scheduled(cron="0 0 4 * * *") 
	//@Scheduled(cron="0 * * * * *")
	public void mailBoxExpiredDel(){
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		logger.debug("mailBoxExpiredDel start : " + df.format(calendar.getTime()));
		
		try {
			
			mailBoxService.deleteMailBoxExpired();
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		
		logger.debug("mailBoxExpiredDel end"); 
	}
}

