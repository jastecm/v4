package v4.socket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

public class EchoHandler extends TextWebSocketHandler {

	
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		System.out.println(session.getId()+"접속");
		connectedUser.add(session);
	}
	
	@Override
	public void handleBinaryMessage(WebSocketSession session, BinaryMessage message) {
		super.handleBinaryMessage(session, message);
	}
	
	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		System.out.println(session.getId()+"종료");
		connectedUser.remove(session);
		subTarget.remove(session.getId());
	}
	
	private static List<WebSocketSession> connectedUser;
	private static Map<String,String> subTarget;
	
	public EchoHandler(){
		connectedUser = new ArrayList<WebSocketSession>();
		subTarget = new HashMap<String,String>();
	}
	
	@Override
	public void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		
		
		
		if(session != null){
			String sub = message.getPayload();
			
			for(WebSocketSession webSession : connectedUser){
				if(webSession.getId().equals(session.getId())){
					subTarget.put(session.getId(), sub);
					webSession.sendMessage(new TextMessage((sub+" subscribe start")));
				}
			}
			
		}
		
		
	}
	
	public void handleTextMessage(WebSocketSession session, TextMessage message,String fr) throws Exception {
		
		
		
		if(session == null){
			for(WebSocketSession webSession : connectedUser){
				
				String target = subTarget.get(webSession.getId());
				if(target == null){
					webSession.sendMessage(new TextMessage(message.getPayload()));
				}
				else if(target != null && target.equals(fr)){
					webSession.sendMessage(new TextMessage(message.getPayload()));
				}
				
				
			}
			
		}
		
		
	}
	
	
}




