package v4.mqtt.component;

import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;

/*
 * @Class name : CommonController.java
 * @Description : 
 * @Modification Information
 * 
 * @author 
 * @since 
 * @version 
 * @see
 *
 */
public interface MqttComponent {

	public MqttClient getMattClinet();
	MqttCallback setTopicCallback();
}