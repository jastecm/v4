package v4.mqtt.component;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.stereotype.Component;

import com.common.Globals;

import v4.mqtt.delivery.MqttMessageDeliveryService;

/*
 * @Class name : CommonController.java
 * @Description : 
 * @Modification Information
 * 
 * @author 
 * @since 
 * @version 
 * @see
 *
 */
@Component("mqttComponent")
public class MqttComponentImpl implements MqttComponent{
	private Log logger = LogFactory.getLog(getClass());

	private MqttClient mqttClinet;
	
	public MqttClient getMattClinet() {
		return mqttClinet;
	}

	private MqttCallback mqttCallBack;
	
	@Resource(name = "mqttMessageDeliveryService")
	private MqttMessageDeliveryService mqttMessageDeliveryService;
	
	public MqttComponentImpl() {
		try {
			
			this.mqttClinet = new MqttClient(Globals.MQTT_PROP_URL,MqttClient.generateClientId(),new MemoryPersistence());
			this.mqttCallBack = setTopicCallback();
			this.mqttClinet.setCallback(this.mqttCallBack);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}
	
	public MqttCallback setTopicCallback() {

		return new MqttCallback() {

			@Override
			public void connectionLost(Throwable cause) {
				// TODO Auto-generated method stub
			}

			@Override
			public void messageArrived(String topic, MqttMessage message)
					throws Exception {
				// TODO Auto-generated method stub
				mqttMessageDeliveryService.onReceived(topic, message.getPayload());
			}

			@Override
			public void deliveryComplete(IMqttDeliveryToken token) {
				// TODO Auto-generated method stub

			}
		};

	}
}