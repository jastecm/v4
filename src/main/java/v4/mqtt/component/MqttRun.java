package v4.mqtt.component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.common.Globals;

/*
 * @Class name : CommonController.java
 * @Description : 
 * @Modification Information
 * 
 * @author 
 * @since 
 * @version 
 * @see
 *
 */
@Component
public class MqttRun {
	private Log logger = LogFactory.getLog(getClass());

	@Resource(name = "mqttComponent")
	MqttComponent mqttComponent;
	
	//@Resource(name = "accountService")
	//AccountService accountService;
	
	//public static List<String> subList = new ArrayList<String>();
	
	@PostConstruct
	public void postCon(){
		try {
			if (!mqttComponent.getMattClinet().isConnected()){
				//MqttConnectOptions op = new MqttConnectOptions();
				//op.setUserName(Globals.MQTT_PROP_URL);
				//op.setPassword(Globals.MQTT_PROP_PW.toCharArray());				
				mqttComponent.getMattClinet().connect();
			}
			
			
			String topic = "+/+/tx";
			if(!Globals.DEV) startSubscribe(topic);
			topic = "+/+/tx/+";
			if(!Globals.DEV) startSubscribe(topic);
		}catch (Exception e) {
			e.printStackTrace();
		}
			
	}
	
	
	public void startSubscribe(String topic) throws Exception {
		try {
			System.out.println("----- startSubscribe -----");
			System.out.println(topic);
			mqttComponent.getMattClinet().subscribe(topic, 0);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}