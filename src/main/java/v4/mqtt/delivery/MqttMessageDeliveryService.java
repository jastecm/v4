package v4.mqtt.delivery;


/*
 * @Class name : CommonController.java
 * @Description : 
 * @Modification Information
 * 
 * @author 
 * @since 
 * @version 
 * @see
 *
 */
public interface MqttMessageDeliveryService {
	
	public void onReceived(String topic,byte[] msg)throws Exception;


}