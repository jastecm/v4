package com.common;

import java.util.HashMap;
import java.util.Map;

/**
 *  Class Name : Globals.java
 *  Description : 시스템 구동 시 프로퍼티를 통해 사용될 전역변수를 정의한다.
 *  Modification Information
 *
 *     수정일         수정자                   수정내용
 *   -------    --------    ---------------------------
 *
 *  @author 
 *  @since 
 *  @version 1.0
 *  @see
 *
 */

public class Globals {

	public static Map<String,String> LORA_KEYS;
	static {
		LORA_KEYS = new HashMap<String,String>();
		LORA_KEYS.put("0010081000000260", "RDZVSno0Slk0NkxjbUJHU0VyV29nNkI4WGhRVzgwYXZORUFDbXZNdkc4eWhFTDZnVG5pR3lNZWpQaW4xOGVCWQ==");
		LORA_KEYS.put("9999991000000032", "RDZVSno0Slk0NkxjbUJHU0VyV29nNkI4WGhRVzgwYXZORUFDbXZNdkc4eWhFTDZnVG5pR3lNZWpQaW4xOGVCWQ==");
		LORA_KEYS.put("0010071000000169", "WnVpaWk3Y2tQNzBHMitPa3NmRkpDOE0wdnVUcW82eFhQRVlJUzZaaHl5azdmNVFnbktkRVpmYWZ2TUVLeGd3Sw==");
		LORA_KEYS.put("0010071000000371", "RDZVSno0Slk0NkxjbUJHU0VyV29nNkI4WGhRVzgwYXZORUFDbXZNdkc4eWhFTDZnVG5pR3lNZWpQaW4xOGVCWQ==");//한화사용(일부 자스텍꺼있음)
		               
	}
	
	public static final String OS = System.getProperty("os.name");
	public static final boolean WIN = OS.toLowerCase().indexOf("windows")>-1;
	public static final boolean DEV = OS.toLowerCase().indexOf("windows")>-1;
	public static final String MODE = PropertiesLoader.getGlobalsProperty("Globals.mode");
	
	public static final String FILE_SIZE = PropertiesLoader.getGlobalsProperty("Globals.maxFileSize");
	public static final String FILE_PATH = WIN?PropertiesLoader.getGlobalsProperty("Globals.defaultFilePath"):PropertiesLoader.getGlobalsProperty("Globals.defaultFilePathServer");
	
	public static final String MAIL_SENDER = PropertiesLoader.getGlobalsProperty("Globals.mailSender");
	
	public static final String FIRMWARE_LOCATION = WIN?PropertiesLoader.getGlobalsProperty("Globals.localFirmwareLocation"):PropertiesLoader.getGlobalsProperty("Globals.serverFirmwareLocation");
	
	public static final String VEHICLE_DB_FILE_LOCATION = WIN?PropertiesLoader.getGlobalsProperty("Globals.localDbFileLocation"):PropertiesLoader.getGlobalsProperty("Globals.serverDbFileLocation");

	public static final String EASYPAY_CERT_PATH = WIN?PropertiesLoader.getGlobalsProperty("Globals.localEasypayCertPath"):PropertiesLoader.getGlobalsProperty("Globals.easypayCertPath");
	public static final String EASYPAY_LOG_PATH = WIN?PropertiesLoader.getGlobalsProperty("Globals.localEasypayLogPath"):PropertiesLoader.getGlobalsProperty("Globals.easypayLogPath");

	public static final String VDAS_MANAGER1 = PropertiesLoader.getGlobalsProperty("Globals.vdasManager1");
	public static final String VDAS_MANAGER2 = PropertiesLoader.getGlobalsProperty("Globals.vdasManager2");
	public static final String VDAS_MANAGER3 = PropertiesLoader.getGlobalsProperty("Globals.vdasManager3");
	public static final String VDAS_MANAGER4 = PropertiesLoader.getGlobalsProperty("Globals.vdasManager4");
	public static final String VDAS_MANAGER5 = PropertiesLoader.getGlobalsProperty("Globals.vdasManager5");
	public static final String VDAS_MANAGER6 = PropertiesLoader.getGlobalsProperty("Globals.vdasManager6");
	
	public static final String FCM_SERVER_KEY = PropertiesLoader.getGlobalsProperty("fcm.serverKey");
	
	public static final String LORA_SUBSCRIVE_MODE0 = PropertiesLoader.getGlobalsProperty("mode0.lora.subscribe.urlPrefix");
	public static final String LORA_SUBSCRIVE_MODE1 = PropertiesLoader.getGlobalsProperty("mode1.lora.subscribe.urlPrefix");
	public static final String LORA_SUBSCRIVE_MODE2 = PropertiesLoader.getGlobalsProperty("mode2.lora.subscribe.urlPrefix");
	public static final String LORA_SUBSCRIVE_NM_MODE0 = PropertiesLoader.getGlobalsProperty("mode0.lora.subscribe.name");
	public static final String LORA_SUBSCRIVE_NM_MODE1 = PropertiesLoader.getGlobalsProperty("mode1.lora.subscribe.name");
	public static final String LORA_SUBSCRIVE_NM_MODE2 = PropertiesLoader.getGlobalsProperty("mode2.lora.subscribe.name");
	public static final String LORA_SUBSCRIVE = PropertiesLoader.getGlobalsProperty("mode"+MODE+".lora.subscribe.urlPrefix");
	public static final String LORA_SUBSCRIVE_NM = PropertiesLoader.getGlobalsProperty("mode"+MODE+".lora.subscribe.name");
	
	public static final String VDAS_URL_PREFIX = PropertiesLoader.getGlobalsProperty("mode"+MODE+".vdas.urlPrefix");
	public static final String VLOG_URL_PREFIX = PropertiesLoader.getGlobalsProperty("mode"+MODE+".vlog.urlPrefix");
	public static final String REALAY_URL_PREFIX = PropertiesLoader.getGlobalsProperty("mode"+MODE+".relay.urlPrefix");
	
	/*db Property*/
	public static final String DBCP_DRIVER_CLASS_NAME = PropertiesLoader.getDbProperty("Globals.DriverClassName");
	public static final String DBCP_URL = PropertiesLoader.getDbProperty("Globals.Url");
	public static final String DBCP_USERNAME = PropertiesLoader.getDbProperty("Globals.UserName");
	public static final String DBCP_PASSWORD = PropertiesLoader.getDbProperty("Globals.Password");
	
	/*mqtt Property*/
	public static final String MQTT_PROP_URL = PropertiesLoader.getMqttProperty("MQTT.mqttUrl");
	public static final String MQTT_PROP_USER = PropertiesLoader.getMqttProperty("MQTT.mqttUser");
	public static final String MQTT_PROP_PW = PropertiesLoader.getMqttProperty("MQTT.mqttPw");
	
	
	
}
