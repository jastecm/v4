package com.common;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jastecm.string.StrUtil;

import v4.web.vo.data.AccountVO;



public class AdminSecurityFilter implements Filter {
	
	private Log logger = LogFactory.getLog(getClass());
	
	private List<String> nonSecureUrlList;
	private List<String> nonSecureIndexList;
	
	public List<String> getNonSecureUrlList() {
		return nonSecureUrlList;
	}

	public void setNonSecureUrlList(List<String> nonSecureUrlList) {
		this.nonSecureUrlList = nonSecureUrlList;
	}
	
	public void setNonSecureIndexList(List<String> nonSecureIndexList) {
		this.nonSecureIndexList = nonSecureIndexList;
	}

	public void destroy() {
		// TODO Auto-generated method stub
	}
	
	
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		
		String key = (String)httpRequest.getSession().getAttribute("_KEY");
		AccountVO user = (AccountVO)httpRequest.getSession().getAttribute("V4");
		
		String servletPath = httpRequest.getServletPath();
		String reqURI = httpRequest.getRequestURI();
		
		
		if(servletPath.matches(".+\\..+$")){
			//System.out.println(".xx pass");
			chain.doFilter(httpRequest, response);
		}else{
			String lang = request.getLocale().getLanguage().toLowerCase();
			try {
				if(((String)httpRequest.getSession(true).getAttribute("_LANG"))==null){
					if(user == null){
						if(lang.equals("kr") || lang.equals("ko_kr") || lang.equals("ko")) lang = "ko";
						else lang = "en";
						httpRequest.getSession(true).setAttribute("_LANG",lang);
					}else{
						httpRequest.getSession(true).setAttribute("_LANG",StrUtil.isNullToEmpty(user.getLang(),"ko"));
					}
				}
				
				
				if(!StrUtil.isNullToEmpty((String)httpRequest.getParameter("changeLang"))){
					httpRequest.getSession(true).setAttribute("_LANG",(String)httpRequest.getParameter("changeLang"));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			String[] arrPath = servletPath.split("/");
			System.out.println("서블릿패스"+ servletPath);
			System.out.println("reqURI"+ reqURI);
			System.out.println(Arrays.toString(arrPath));
			
			
			if(arrPath[1].equals("api")) {
				System.out.println("case api");
				chain.doFilter(httpRequest, response);
			}
			else if(nonSecureUrlList.contains(httpRequest.getServletPath()) || arrPath[1].equals("lora")) {
				if(nonSecureUrlList.contains(httpRequest.getServletPath())) System.out.println("case none secure url");
				if(arrPath[1].equals("lora")) System.out.println("case lora msg url");
				chain.doFilter(httpRequest, response);
			}
			else if(arrPath[1].equals("index") && !StrUtil.isNullToEmpty(key)){
				System.out.println("case secure success");
				chain.doFilter(httpRequest, response);
				//request.getRequestDispatcher("/main").forward(request, response);
			}else if(arrPath[1].equals("index") && StrUtil.isNullToEmpty(key)){
				if(StrUtil.isNullToEmpty(request.getParameter("cmd")))
					chain.doFilter(new FilteredRequest(httpRequest,"/main"), response);
				else{
					String cmd = request.getParameter("cmd");
					if(!nonSecureIndexList.contains(cmd)){
						System.out.println("case secure fail - cmd : " + cmd);
						chain.doFilter(new FilteredRequest(httpRequest,"/login"), response);
					}else{
						System.out.println("case none secure index cmd");
						chain.doFilter(httpRequest, response);
					}
				}
			}
			else{
				System.out.println("case unKnown url");
				//개발용
				if(!StrUtil.isNullToEmpty(key)) 
					chain.doFilter(httpRequest, response);
				else
					request.getRequestDispatcher("/main").forward(request, response);
				//개발용 end
				
				//운영용			
				//request.getRequestDispatcher("/404").forward(request, response);
				//운영용 end
				
				//request.getRequestDispatcher("/main").forward(request, response);
				//chain.doFilter(httpRequest, response);
			}
				
		}
		
			
			
	}
	
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
	}
	
	static class FilteredRequest extends HttpServletRequestWrapper {

		private String path;
		
        public FilteredRequest(ServletRequest request,String forwardPath) {
            super((HttpServletRequest)request);
            this.path = forwardPath;
        }

        public String getParameter(String paramName) {
            String value = super.getParameter(paramName);
            if ("cmd".equals(paramName)) {
                value = path;
            }
            return value;
        }

        public String[] getParameterValues(String paramName) {
            String values[] = super.getParameterValues(paramName);
            if ("cmd".equals(paramName)) {
                for (int index = 0; index < values.length; index++) {
                    values[index] = path;
                }
            }
            return values;
        }
        
    }
}
