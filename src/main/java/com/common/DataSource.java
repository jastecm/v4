package com.common;

public class DataSource extends org.apache.commons.dbcp.BasicDataSource {

	public DataSource() {
		
		setDriverClassName(Globals.DBCP_DRIVER_CLASS_NAME);
		setUrl(Globals.DBCP_URL);
		setUsername(Globals.DBCP_USERNAME);
		setPassword(Globals.DBCP_PASSWORD);
		setInitialSize(10);
		setMaxActive(100);
		setMaxIdle(30);
		setMinIdle(10);
		setRemoveAbandoned(true);
		setRemoveAbandonedTimeout(30);
		setValidationQuery("SELECT 1");
		
	}
	
}
