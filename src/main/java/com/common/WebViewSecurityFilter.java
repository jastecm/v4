package com.common;

import java.io.IOException;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;


public class WebViewSecurityFilter implements Filter {

	private List<String> nonSecureUrlList;

	public void destroy() {
		// TODO Auto-generated method stub
	}
		
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		
		String servletPath = httpRequest.getServletPath();
		String reqURI = httpRequest.getRequestURI();
		
		//UserVO user = (UserVO)httpRequest.getSession().getAttribute("VIEWCAR");
		
		String sessionId = httpRequest.getSession().getId();
		
		int viewChk = servletPath.indexOf("/web/");
		int apiChk = servletPath.indexOf("/api/");
		
		if(viewChk == 0){
			//System.out.println("webView");
			/*
			if(user == null){
				request.getRequestDispatcher("/web/?page=login").forward(request, response);
			}else {
				chain.doFilter(httpRequest, response);
			}
			*/
		}else if(apiChk == 0){
			System.out.println("api");
			chain.doFilter(httpRequest, response);
		}else{
			System.out.println("webPage");
			chain.doFilter(httpRequest, response);
		}
		
				
		
		
	}
	
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
	}
	
}
