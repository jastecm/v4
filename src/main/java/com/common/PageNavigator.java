package com.common;
 
public class PageNavigator
{
  int currentPage;
  int rowsPerPage;
  int pagePerGroup;
  int startRow;
  int endRow;
  int totalSize;
  int pageTotal = 0;
  int pageGroupStart;
  int pageGroupEnd;

  public PageNavigator(int i, int j)
  {
    setCurrentPage(i);
    setRowsPerPage(j);
    setPagePerGroup(10); }

  public PageNavigator(int i, int j, int k) {
    setCurrentPage(i);
    setRowsPerPage(j);
    setPagePerGroup(k); }

  public int getCurrentPage() {
    if ((this.pageTotal > 0) && (this.currentPage > this.pageTotal)) this.currentPage = this.pageTotal;
    return this.currentPage; }

  public void setCurrentPage(int currentPage) {
    if (currentPage < 1) currentPage = 1;
    this.currentPage = currentPage;
    calcStartEndRow(currentPage); }

  public int getRowsPerPage() {
    return this.rowsPerPage; }

  public void setRowsPerPage(int rowsPerPage) {
    this.rowsPerPage = rowsPerPage;
    calcStartEndRow(this.currentPage);
  }

  public int getPagePerGroup()
  {
    return this.pagePerGroup;
  }

  public void setPagePerGroup(int pagePerGroup)
  {
    this.pagePerGroup = pagePerGroup;
    calcStartEndRow(this.currentPage); }

  public int getStartRow() {
    return this.startRow; }

  public int getEndRow() {
    return this.endRow;
  }

  private void calcStartEndRow(int currentPage) {
    //this.startRow = ((currentPage - 1) * this.rowsPerPage + 1);
    this.startRow = ((currentPage - 1) * this.rowsPerPage + 1) -1;
    this.endRow = (this.startRow + this.rowsPerPage - 1); }

  public int getTotalSize() {
    return this.totalSize; }

  public void setTotalSize(int totalSize) {
    this.totalSize = totalSize;
    setPageTotal();
    setPageGroupStart();
    setPageGroupEnd();
  }

  public int getPageTotal() {
    return this.pageTotal; }

  public int getPageGroupStart() {
    return this.pageGroupStart; }

  public int getPageGroupEnd() {
    return this.pageGroupEnd; }

  public void setPageTotal() {
    int tempPageTotal = this.totalSize / this.rowsPerPage;
    if (this.totalSize > tempPageTotal * this.rowsPerPage)
      ++tempPageTotal;
    this.pageTotal = tempPageTotal; }

  public void setPageGroupStart() {
    this.pageGroupStart = ((this.currentPage - 1) / this.pagePerGroup * this.pagePerGroup + 1); }

  public void setPageGroupEnd() {
    int tempPageGroupEnd = getPageGroupStart() + this.pagePerGroup;
    if (tempPageGroupEnd > getPageTotal())
      tempPageGroupEnd = getPageTotal() + 1;
    if (tempPageGroupEnd > 1) --tempPageGroupEnd;
    this.pageGroupEnd = tempPageGroupEnd; }

  public void setPageGroupEnd(int totalPage) {
    this.pageGroupEnd = totalPage;
  }
}