package com.common;
 
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
 
public class HttpToHttpsFilter implements Filter {
 
	public void doFilter(ServletRequest request, ServletResponse response,FilterChain chain) throws java.io.IOException, ServletException {
 
		/*
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
 
		String uri = req.getRequestURI();
		String getProtocol = req.getScheme();
		String getDomain = req.getServerName();
		String getPort = Integer.toString(req.getServerPort());
		
		if (!getDomain.equals("localhost") && req.getScheme().equals("http") ) {
		//if (req.getRemoteUser() != null && req.getScheme().equals("http") ) {
            String url = "https://" + req.getServerName() + req.getContextPath() + req.getServletPath();
            if (req.getPathInfo() != null) {
                url += req.getPathInfo();
            }
            System.out.println(url);
            res.sendRedirect(url);
        } else {
            chain.doFilter(request, response);
        }
 */
		chain.doFilter(request, response);
	}
 
	@Override
	public void init(FilterConfig arg0) throws ServletException {
	 
	}
	 
	@Override
	public void destroy() {
	 
	}
 
}