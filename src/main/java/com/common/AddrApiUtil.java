package com.common;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jastecm.json.JSONArray;
import org.jastecm.json.JSONObject;
import org.jastecm.string.StrUtil;




/*
 * @Class name : CommonController.java
 * @Description : 
 * @Modification Information
 * 
 * @author 
 * @since 
 * @version 
 * @see
 *
 */
public class AddrApiUtil{
	
	private static final String SKT_AUTHKEY = "27f53640-e198-4550-a075-70f96a5fc34f"; //skt api project - v3 TODO move v4 project
	private static final String GOOGLE_AUTHKEY = "27f53640-e198-4550-a075-70f96a5fc34f"; //google api project - v3 TODO move v4 project
	
	private Log logger = LogFactory.getLog(getClass());
	public static String getGPSAdressByGeometryVerSK(double latitude, double longitude) {
		try {
			String url = "https://api2.sktelecom.com/tmap/geo/reversegeocoding?lon="+longitude+"&callback=&coordType=WGS84GEO&addressType=A04&lat="+latitude+"&version=1";

			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// optional default is GET
			con.setRequestMethod("GET");

			//add request header
			con.setRequestProperty("Accept", "application/json;charset=utf-8");
			con.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
			con.setRequestProperty("appKey", SKT_AUTHKEY);

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);
			System.out.println(con.getContentEncoding());
			
			System.out.println(con.getContentType());
			
			
			if(responseCode == 200){
				BufferedReader in = new BufferedReader(
				        new InputStreamReader(con.getInputStream(),"UTF-8"));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				

		    	String getGPSAdreesJSONData =  response.toString();
		    	System.out.println(getGPSAdreesJSONData);
		    	JSONObject jObj = new JSONObject(getGPSAdreesJSONData);
		    	JSONObject address = (JSONObject) jObj.get("addressInfo");
		    	
		    	
		    	if(address != null) {
		    		StringBuffer sb = new StringBuffer();
		    		sb.append((String)address.get("city_do")); 
		    		
		    		sb.append(" "); 
		    		sb.append((String)address.get("gu_gun"));
		    		
		    		if(!StrUtil.isNullToEmpty((String)address.get("eup_myun"))){
		    			sb.append(" "); 
			    		sb.append((String)address.get("eup_myun"));
		    		}
		    		
		    		if(!StrUtil.isNullToEmpty((String)address.get("roadName"))){
		    			sb.append(" "); 
			    		sb.append((String)address.get("roadName"));
			    		
			    		if(!StrUtil.isNullToEmpty((String)address.get("buildingIndex"))){
			    			sb.append(" "); 
				    		sb.append((String)address.get("buildingIndex"));
			    		}
		    		}else if(!StrUtil.isNullToEmpty((String)address.get("adminDong"))){
		    			sb.append(" "); 
			    		sb.append((String)address.get("adminDong"));
			    		if(!StrUtil.isNullToEmpty((String)address.get("bunji"))){
			    			sb.append(" "); 
				    		sb.append((String)address.get("bunji"));
			    		}
		    		}else if(!StrUtil.isNullToEmpty((String)address.get("legalDong"))){
		    			sb.append(" "); 
			    		sb.append((String)address.get("legalDong"));
			    		if(!StrUtil.isNullToEmpty((String)address.get("bunji"))){
			    			sb.append(" "); 
				    		sb.append((String)address.get("bunji"));
			    		}
		    		}
		    		
		    		return sb.toString();
		    		
		    	}
			}else{
				if(responseCode == 204) 
					return "알수없는 주소";
				else return null;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			return "알수없는 주소";
		}
		
		return null;
		
	}
	
	
	public static String getGPSAdressByAddressVerSK(String addr) {
		try {
			String enAddr = URLEncoder.encode(addr, "UTF-8"); 

			String url = "https://api2.sktelecom.com/tmap/geo/fullAddrGeo?addressFlag=F02&coordType=WGS84GEO&version=1&format=json&fullAddr="+enAddr;
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// optional default is GET
			con.setRequestMethod("GET");

			//add request header
			con.setRequestProperty("Accept", "application/json;charset=utf-8");
			con.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
			con.setRequestProperty("appKey", SKT_AUTHKEY);

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);
			System.out.println(con.getContentEncoding());
			
			System.out.println(con.getContentType());
			
			
			if(responseCode == 200){
				BufferedReader in = new BufferedReader(
				        new InputStreamReader(con.getInputStream(),"UTF-8"));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				

		    	String getGPSAdreesJSONData =  response.toString();
		    	return getGPSAdreesJSONData;
			}else{
				BufferedReader in = new BufferedReader(
				        new InputStreamReader(con.getErrorStream(),"UTF-8"));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				
					System.out.println(response.toString());
		    	String getGPSAdreesJSONData =  response.toString();
		    	return getGPSAdreesJSONData;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	public static String getApiAddress(double latitude, double longitude) {
		String apiURL = "http://maps.googleapis.com/maps/api/geocode/json?latlng="
				+ latitude + "," + longitude + "&language=ko&key="+GOOGLE_AUTHKEY;
		return apiURL;
	}
	
	public static String getApiAddressEn(double latitude, double longitude) {
		String apiURL = "http://maps.googleapis.com/maps/api/geocode/json?latlng="
				+ latitude + "," + longitude + "&language=en&key="+GOOGLE_AUTHKEY;
		
		return apiURL;
	}

	/*****************************************************/
	public static String getOSMApiAddress(double latitude, double longitude) {
		String apiURL = "http://nominatim.openstreetmap.org/reverse?format=json&zoom=18&addressdetails=1";
		apiURL += "&lat="+latitude+"&lon="+longitude;
			
		return apiURL;
		/*
		String apiURL = "http://maps.googleapis.com/maps/api/geocode/json?latlng="
				+ latitude + "," + longitude + "&language=ko";
		return apiURL;
		*/
	}

	public static String getJSONData(String apiURL) throws Exception {
		String jsonString = new String();
		String buf;
		URL url = new URL(apiURL);
		URLConnection conn = url.openConnection();
		BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
		while ((buf = br.readLine()) != null) {
			jsonString += buf;
		}
		return jsonString;
	}
	/*
	 * osm용
	private String getGPSAdressByGeometry(double latitude, double longitude){		
		
		try {
			String getGPSAdreesApiUrl = getOSMApiAddress(latitude,longitude);
	    	String getGPSAdreesJSONData =  getJSONData(getGPSAdreesApiUrl);
	    	
	    	JSONObject jObj = new JSONObject(getGPSAdreesJSONData);
	    	JSONObject address = (JSONObject) jObj.get("address");
	    	String city = "";
	    	if(address.has("city")) city = (String)address.get("city");
	    	String town = "";
	    	if(address.has("town")) town = (String)address.get("town");
	    	String village = "";
	    	if(address.has("village")) village = (String)address.get("village");
	    	String road = "";
	    	if(address.has("road")) road = (String)address.get("road");
	    	
	    	city = remainOnlyKoreanAndBlank(city);
	    	town = remainOnlyKoreanAndBlank(town);
	    	village = remainOnlyKoreanAndBlank(village);
	    	road = remainOnlyKoreanAndBlank(road);
	    	
	    	String rtv = "";
	    	if(city.replaceAll(" ", "").length() != 0) rtv += city; 
	    	if(town.replaceAll(" ", "").length() != 0) rtv += " "+town; 
	    	if(village.replaceAll(" ", "").length() != 0) rtv += " "+village; 
	    	if(road.replaceAll(" ", "").length() != 0) rtv += " "+road; 
	    	
	    	return rtv.trim();
	    	
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
    	
	}
	 */
	/*
	public static void main(String[] args){
		MqttUtil m = new MqttUtil();
		m.getGPSAdressByGeometry(36.8802964, 127.07558549);
	}
	*/
	public static  String getGPSAdressByGeometry(double latitude, double longitude){		
		String rtv = "";
		
		try {
			String getGPSAdreesApiUrl = getApiAddress(latitude,longitude);
	    	String getGPSAdreesJSONData =  getJSONData(getGPSAdreesApiUrl);
	    	JSONObject jObj = new JSONObject(getGPSAdreesJSONData);
	    	JSONArray jArray = (JSONArray) jObj.get("results");
	    	
	    	if(!jArray.isNull(0)){
	    		jObj = (JSONObject) jArray.get(0);
	    		rtv = (String) jObj.get("formatted_address");
	    		rtv = rtv.replace("대한민국 ", "");
	    	}
	    	/* 도로명우선
	    	if(jArray.length() > 1 && !jArray.isNull(1)){
	    		jObj = (JSONObject) jArray.get(1);
	    		rtv = (String) jObj.get("formatted_address");
	    		rtv = rtv.replace("대한민국 ", "");
	    	}else if(!jArray.isNull(0)){
	    		jObj = (JSONObject) jArray.get(0);
	    		rtv = (String) jObj.get("formatted_address");
	    		rtv = rtv.replace("대한민국 ", "");
	    	}
	    	*/
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	return rtv;
	}
	
	public static String getGPSAdressByGeometryEn(double latitude, double longitude){		
		String rtv = "";
		
		try {
			String getGPSAdreesApiUrl = getApiAddressEn(latitude,longitude);
	    	String getGPSAdreesJSONData =  getJSONData(getGPSAdreesApiUrl);
	    	JSONObject jObj = new JSONObject(getGPSAdreesJSONData);
	    	JSONArray jArray = (JSONArray) jObj.get("results");
	    	
	    	if(!jArray.isNull(0)){
	    		jObj = (JSONObject) jArray.get(0);
	    		rtv = (String) jObj.get("formatted_address");
	    		rtv = rtv.replace(", South Korea", "").replace("South Korea", "");
	    	}
	    	/* 도로명우선
	    	if(jArray.length() > 1 && !jArray.isNull(1)){
	    		jObj = (JSONObject) jArray.get(1);
	    		rtv = (String) jObj.get("formatted_address");
	    		rtv = rtv.replace(", South Korea", "").replace("South Korea", "");
	    	}else if(!jArray.isNull(0)){
	    		jObj = (JSONObject) jArray.get(0);
	    		rtv = (String) jObj.get("formatted_address");
	    		rtv = rtv.replace(", South Korea", "").replace("South Korea", "");
	    	}*/
		} catch (Exception e) {
			e.printStackTrace();
			return "알수없는 주소";
		}
    	
    	return rtv;
	}
	
	public static String remainOnlyKoreanAndBlank(String str){
		return str.replaceAll("[^\uAC00-\uD7AF\u1100-\u11FF\u3130-\u318F]+", "");
	}
	
	
	
}