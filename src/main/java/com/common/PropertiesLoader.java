package com.common;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.jboss.logging.Logger;

/**
 *  Class Name : PropertiesLoader.java
 *  Description : 시스템 구동 시 프로퍼티를 통해 사용될 전역변수를 정의한다.
 *  Modification Information
 *
 *     수정일         수정자                   수정내용
 *   -------    --------    ---------------------------
 *
 *  @author 
 *  @since 
 *  @version 1.0
 *  @see
 *
 */

public class PropertiesLoader {
	
	static Logger logger = Logger.getLogger(PropertiesLoader.class);
	
	static final String OS = System.getProperty("os.name");
	static final boolean DEV = OS.toLowerCase().indexOf("windows")>-1;
	static final char FILE_SEPARATOR     = File.separatorChar;
	static final String BASE_PATH = System.getProperty("user.dir")+FILE_SEPARATOR;
	
	static final String path = (DEV)?PropertiesLoader.class.getResource("").getPath().substring(0, PropertiesLoader.class.getResource("").getPath().lastIndexOf("classes"))
    		+ "classes" + FILE_SEPARATOR + "properties"+FILE_SEPARATOR
    		:BASE_PATH+"v4"+FILE_SEPARATOR;
	
	static File gloabalsProperties = new File(path+"globals.properties");
	static File dbProperties = new File(path+"db.properties");
	static File mqttProperties = new File(path+"mqtt.properties");
	
	
	
	private static String getProperty(File property,String key){
		try {
    		
    		Properties properties = new Properties();
    		if(!property.exists()){
    			logger.error("can't found properties - " + property.getAbsolutePath());
    			return null;
    		}
    			
    		String propertyPath = property.getAbsolutePath();
    		
    		FileInputStream fis = new FileInputStream(propertyPath);
    		
    		properties.load(new java.io.BufferedInputStream(fis));
             
            String val = properties.getProperty(key);
             
            return val; 


        } catch (Exception e) {
        	
        	return null;
        }
	}
	
	public static String getGlobalsProperty(String key){
		try {
    		
			return getProperty(gloabalsProperties, key);

        } catch (Exception e) {
        	return null;
        }
	}
	
	public static String getMqttProperty(String key){
		try {
    		
			return getProperty(mqttProperties, key);

        } catch (Exception e) {
        	return null;
        }
	}
	
	public static String getDbProperty(String key){
		try {
    		
			return getProperty(dbProperties, key);

        } catch (Exception e) {
        	return null;
        }
	}
}
