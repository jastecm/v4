package com.common;

import java.io.File;

import javax.annotation.PostConstruct;

import org.jboss.logging.Logger;
import org.springframework.stereotype.Component;

/**
 *  Class Name : PropertiesLoader.java
 *  Description : 시스템 구동 시 프로퍼티를 통해 사용될 전역변수를 정의한다.
 *  Modification Information
 *
 *     수정일         수정자                   수정내용
 *   -------    --------    ---------------------------
 *
 *  @author 
 *  @since 
 *  @version 1.0
 *  @see
 *
 */
@Component
public class PropertiesLoader_Influx {
	
	static Logger logger = Logger.getLogger(PropertiesLoader_Influx.class);
	
	static final String OS = System.getProperty("os.name");
	static final boolean DEV = OS.toLowerCase().indexOf("windows")>-1;
	static final char FILE_SEPARATOR     = File.separatorChar;
	static final String BASE_PATH = System.getProperty("user.dir")+FILE_SEPARATOR;
	
	static final String path = (DEV)?PropertiesLoader_Influx.class.getResource("").getPath().substring(0, PropertiesLoader_Influx.class.getResource("").getPath().lastIndexOf("classes"))
    		+ "classes" + FILE_SEPARATOR + "influxProperties"+FILE_SEPARATOR
    		:BASE_PATH+"v4"+FILE_SEPARATOR+"influxProperties"+FILE_SEPARATOR;
	
	static File influxProperties = new File(path+"influx.properties");
	static File gloabalsProperties = new File(path+"globals.properties");
	static File mqttProperties = new File(path+"mqtt.properties");
	
	@PostConstruct
	public void setInfluxProperties(){
		System.out.println("set influx client properties setting");
		org.jastecm.influx.com.common.PropertiesLoader.setInfluxProperties(influxProperties);
		org.jastecm.influx.com.common.PropertiesLoader.setGloabalsProperties(gloabalsProperties);
		org.jastecm.influx.com.common.PropertiesLoader.setMqttProperties(mqttProperties);
	}
}
