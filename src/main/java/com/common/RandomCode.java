package com.common;

import java.util.Random;


public class RandomCode {

	final String AB = "123456789";
	Random rnd = new Random();

	public String randomString( int len ) 
	{
	   StringBuilder sb = new StringBuilder( len );
	   for( int i = 0; i < len; i++ ) 
	      sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
	   return sb.toString();
	}
	
	
	
}
