package com.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ValidateUtil {
	
	public static String PATTERN_NUM = "^[0-9]*$";
	public static String PATTERN_ENG = "^[a-zA-Z]*$";
	public static String PATTERN_KOR = "^[ㄱ-ㅎ가-힣]*$";
	public static String PATTERN_ENUM = "^[a-zA-Z0-9]*$";
	public static String PATTERN_KNUM = "^[ㄱ-ㅎ가-힣0-9]*$";
	public static String PATTERN_EMAIL = "^[_0-9a-zA-Z-]+@[0-9a-zA-Z]+(.[_0-9a-zA-Z-]+)*$";
	public static String PATTERN_HP = "^01(?:0|1|[6-9])-(?:\\d{3}|\\d{4})-\\d{4}$";
	public static String PATTERN_TEL = "^\\d{2,3}-\\d{3,4}-\\d{4}$";
	public static String PATTERN_SSN = "^\\d{7}-[1-4]\\d{6}";
	public static String PATTERN_IP4 = "([0-9]{1,3})\\.([0-9]{1,3})\\.([0-9]{1,3})\\.([0-9]{1,3})";
	
	public static String PATTERN_PW_8_16 = "^[a-zA-Z0-9!@#$%^&*+=-]{8,16}$";
	public static String PATTERN_ENUM_5_12 = "^[a-zA-Z0-9]{5,12}$";
	public static String PATTERN_ENUM_8_12 = "^[a-zA-Z0-9]{8,12}$";
	public static String PATTERN_CORPNUM = "^\\d{3}-\\d{2}-\\d{5}$";
	
	public static boolean validChk(String pattern ,boolean nullEmptyReturnValue, String str) {
		if(str == null || str.trim().equals("")) return nullEmptyReturnValue;
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(str);
		return m.matches();
	}
}
