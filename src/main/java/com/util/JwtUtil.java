package com.util;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import v4.web.vo.InviteStep1VO;
import v4.web.vo.RegistStep1VO;
import v4.web.vo.TokenVO;

@Component
public class JwtUtil {

    @Value("${jwt.secret}")
    private String secret;
    
    
    //이하 삭제예정
    @Value("${jwt.secretPw}")
    private String secretPw;
    
    @Value("${jwt.secretVlogKey}")
    private String secretVlogKey;
    
    @Value("${jwt.secretVlogPw}")
    private String secretVlogPw;
    
    @Value("${jwt.secretInvite}")
    private String secretInvite;
    
    @Value("${jwt.deviceSecret}")
    private String deviceSecret;
    

    /**
     * Tries to parse specified String as a JWT token. If successful, returns User object with username, id and role prefilled (extracted from token).
     * If unsuccessful (token is invalid or not containing all required user properties), simply returns null.
     * 
     * @param token the JWT token to parse
     * @return the User object extracted from specified token or null if a token is invalid.
     */
    public TokenVO parseToken(String token) {
    	
        try {
        	
        	Claims body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
                        
            
        	return new TokenVO(new Integer((Integer) body.get("expired")).longValue(),(String)body.get("userKey"),(String)body.get("corpKey"),(String)body.get("corpType"),(String)body.get("serviceGrade"));
            

        } catch (SignatureException e2){
        	return null;
        } catch (Exception e) {
            return null;
        }
    	
    }
    
    public RegistStep1VO parseEmailAuthToken(String token) {
    	
        try {
        	
        	Claims body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
                        
            return new RegistStep1VO((String)body.get("accountId"), (String)body.get("accountPw"));

        } catch (SignatureException e2){
        	return null;
        } catch (Exception e) {
            return null;
        }
    	
    }
    public InviteStep1VO parseAccountInviteToken(String token) {
    	
        try {
        	
        	Claims body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
                        
            return new InviteStep1VO((String)body.get("accountId"), (String)body.get("corpKey"));

        } catch (SignatureException e2){
        	return null;
        } catch (Exception e) {
            return null;
        }
    	
    }

    public TokenVO parseChangePwToken(String token) {
    	
        try {
        	
        	Claims body = Jwts.parser()
                    .setSigningKey(secretPw)
                    .parseClaimsJws(token)
                    .getBody();
                        
            
            return new TokenVO(new Integer((Integer) body.get("expired")).longValue(),(String)body.get("userKey"),(String)body.get("corpKey"),(String)body.get("corpType"),(String)body.get("serviceGrade"));
            
            
            

        } catch (SignatureException e2){
        	return null;
        } catch (Exception e) {
            return null;
        }
    	
    }
    
    public boolean parseVlogAuthToken(String token) {
    	
        try {
        	
        	Claims body = Jwts.parser()
                    .setSigningKey(secretVlogPw)
                    .parseClaimsJws(token)
                    .getBody();
                        
        	return ((String)body.get("userKey")).equals(secretVlogKey);
            

        } catch (SignatureException e2){
        	return false;
        } catch (Exception e) {
            return false;
        }
    	
    }
    
    public boolean parseRealayAuthToken(String token) {
    	
        try {
        	
        	Claims body = Jwts.parser()
                    .setSigningKey(secretVlogPw)
                    .parseClaimsJws(token)
                    .getBody();
                        
        	return ((String)body.get("userKey")).equals(secretVlogKey);
            

        } catch (SignatureException e2){
        	return false;
        } catch (Exception e) {
            return false;
        }
    	
    }
    
    public Map<String,String> parseInviteToken(String token){
    	Claims body = Jwts.parser()
                .setSigningKey(secretInvite)
                .parseClaimsJws(token)
                .getBody();
        
    	Map<String, String> param = new HashMap<String, String>();
    	param.put("accountKey", (String)body.get("accountKey"));
    	param.put("vehicleKey", (String)body.get("vehicleKey"));
    	return param;
    }
    
    public Map parseTokenGetDeviceAuth(String token) {
    	
        try {
        	
        	Claims body = Jwts.parser()
                    .setSigningKey(deviceSecret)
                    .parseClaimsJws(token)
                    .getBody();
                        
        	long expired = (long)body.get("expired");
        	String accountKey = (String)body.get("accountKey");
        	String deviceSn = (String)body.get("deviceSn");

        	if(expired > new Date().getTime()){
        		Map<String,String> rtv = new HashMap<String,String>();
                rtv.put("accountKey",accountKey);
                rtv.put("deviceSn",deviceSn);
                
                return rtv;
        	}else{
        		return null;
        	}
        		
        } catch (SignatureException e2){
        	return null;
        } catch (Exception e) {
            return null;
        }
    	
    }

    public String parseTokenGetDeviceId(String token) {
    	
        try {
        	
        	Claims body = Jwts.parser()
                    .setSigningKey(deviceSecret)
                    .parseClaimsJws(token)
                    .getBody();
                        
        	long expired = (long)body.get("expired");
        	String deviceId = (String)body.get("deviceId");

        	if(expired > new Date().getTime()){
        		return deviceId;
        	}else{
        		return null;
        	}
        		
        } catch (SignatureException e2){
        	return null;
        } catch (Exception e) {
            return null;
        }
    	
    }

    public String parseTokenGetDeviceSn(String token) {
    	
        try {
        	
        	Claims body = Jwts.parser()
                    .setSigningKey(deviceSecret)
                    .parseClaimsJws(token)
                    .getBody();
                        
        	long expired = (long)body.get("expired");
        	String deviceSn = (String)body.get("deviceSn");

        	if(expired > new Date().getTime()){
        		return deviceSn;
        	}else{
        		return null;
        	}
        		
        } catch (SignatureException e2){
        	return null;
        } catch (Exception e) {
            return null;
        }
    	
    }
    /**
     * Generates a JWT token containing username as subject, and userId and role as additional claims. These properties are taken from the specified
     * User object. Tokens validity is infinite.
     * 
     * @param u the user for which the token will be generated
     * @return the JWT token
     */
    public String generateToken(TokenVO u) {
        Claims claims = Jwts.claims();
        claims.put("userKey", u.getUserKey());
        claims.put("expired", u.getExpired());
        claims.put("corpKey", u.getCorpKey());
        claims.put("corpType", u.getCorpType());
        claims.put("serviceGrade", u.getServiceGrade());

        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();
    }
    
    public String generateEmailAuthToken(String userId , String userPw) {
        Claims claims = Jwts.claims();
        claims.put("accountId", userId);
        claims.put("accountPw", userPw);

        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();
    }
    
    /**
     * 회원 초대용 토큰키
     * @param userId
     * @param corpKey
     * @return
     */
    public String generateAccountInviteToken(String userId , String corpKey) {
        Claims claims = Jwts.claims();
        claims.put("accountId", userId);
        claims.put("corpKey", corpKey);

        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();
    }
    
    public String generateChangePwToken(TokenVO u) {
        Claims claims = Jwts.claims();
        claims.put("userKey", u.getUserKey());
        claims.put("expired", u.getExpired());

        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS256, secretPw)
                .compact();
    }
    
    public String generateVlogToken() {
        Claims claims = Jwts.claims();
        claims.put("userKey", secretVlogKey);

        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS256, secretVlogPw)
                .compact();
    }
    
    public String generateInviteToken(String accountKey , String vehicleKey){
    	Claims claims = Jwts.claims();
        claims.put("accountKey", accountKey);
        claims.put("vehicleKey", vehicleKey);

        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS256, secretInvite)
                .compact();
    }

    
    
}