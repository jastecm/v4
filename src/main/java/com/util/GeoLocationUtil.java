package com.util;

import java.io.File;
import java.io.IOException;

import com.maxmind.geoip.Location;
import com.maxmind.geoip.LookupService;

public class GeoLocationUtil {
	
	private static File geoFile;
	private static LookupService lookup;
	
	static {
		try {
			geoFile = new File("C:\\location\\GeoLiteCity.dat");
			lookup = new LookupService(geoFile, LookupService.GEOIP_MEMORY_CACHE);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static String getLocation(String ipAddress){
		Location locationServices = lookup.getLocation(ipAddress);
		
		//들어있는 값들
		/*System.out.println(locationServices.region);
		System.out.println(locationServices.city);
		System.out.println(locationServices.postalCode);
		System.out.println(locationServices.latitude);
		System.out.println(locationServices.longitude);*/
		
		if(locationServices == null){
			return "KR";
		}
		
		return locationServices.countryCode;
	}
	
	public static void main(String[] args) {
		//호출
		System.out.println(GeoLocationUtil.getLocation("110.87.34.68"));
		System.out.println(GeoLocationUtil.getLocation("62.80.182.42"));
		System.out.println(GeoLocationUtil.getLocation("101.236.21.181"));
		System.out.println(GeoLocationUtil.getLocation("167.99.68.155"));
		System.out.println(GeoLocationUtil.getLocation("5.160.97.3"));
		System.out.println(GeoLocationUtil.getLocation("52.207.236.228"));
		System.out.println(GeoLocationUtil.getLocation("138.118.86.236"));
		System.out.println(GeoLocationUtil.getLocation("173.249.48.138"));
	}
	
	
}
