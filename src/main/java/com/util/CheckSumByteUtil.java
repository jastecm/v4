package com.util;

/*
 * @Class name : CommonController.java
 * @Description : 
 * @Modification Information
 * 
 * @author 
 * @since 
 * @version 
 * @see
 *
 */
public class CheckSumByteUtil{
	public static int makeCheckSumFor4byte(byte[] data) {
	      int checksum = 0;

	      for (int i = 0; i < data.length; i++) {
	         checksum += (0x000000FF) & data[i];
	      }

	      return checksum;
	   }
	
	
}