package com.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


public class TimeUtil {

	
    private static DateFormat dfm = new SimpleDateFormat("yyyyMMddHHmm");  

    public static long timeConversion(String time){
        
    	dfm.setTimeZone(TimeZone.getTimeZone("GMT+9:00"));//Specify your timezone 
    	
    	try{
    		return dfm.parse(time).getTime()/1000L;  
    	}catch (ParseException e){
    		e.printStackTrace();
    		return 0;
    	}
    
    }

    public static long now(){
        return new Date().getTime()/1000L;  
    }

    public static String vlogTimeConverter(String strTime){
    	try {
    		if(strTime == null) return null;
    		
    		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        	DateFormat dfm2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			return dfm2.format(dfm.parse(strTime));
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	
    	return null;
    }
    
    public static long kstToServerTime(long time){
		Calendar c = Calendar.getInstance();
		c.setTime(new Date((time*1000L)-c.getTimeZone().getRawOffset()));
		return c.getTimeInMillis();
    }
    
    public static long appToServerTime(long time){
		Calendar c = Calendar.getInstance();
		c.setTime(new Date((time*1000L)));
		return c.getTimeInMillis();
    }
}