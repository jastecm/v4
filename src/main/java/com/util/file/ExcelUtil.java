package com.util.file;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.util.ClassUtil;

public final class ExcelUtil {	
	
	/**
	 * 엑셀다운로드
	 * String headerName 
	 * String columnName 
	 * @param headerName
	 * @param columnName
	 * @param dataList
	 * @return 
	 */
	public static File setExcelDownLoad(String headerName, String columnName, List<?> dataList, String filePath, String fileNm){
		String newFileNm = filePath + System.getProperty("file.separator") + fileNm;
		new File(filePath).mkdirs();
		try {
			String[] headerNames = headerName.split("\\|");
			String[] columnNames = columnName.split("\\|");
			
			XSSFWorkbook wb = new XSSFWorkbook();
			Sheet sheet = wb.createSheet("sheet1");
			/** 해더 생성 */
			sheet = setHeaderRow(sheet, headerNames);			
	
			for (int i = 0; i < dataList.size(); i++) {
				Object vo = dataList.get(i);
				Map<String, Object> dataMap = ClassUtil.toMap(vo);
				sheet = setCellRow(sheet, (i+1), columnNames, dataMap);
			}
			// 해당 워크시트를 저장함.
			FileOutputStream stream = null;
		
			stream = new FileOutputStream(newFileNm);
			wb.write(stream);
			stream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return new File(newFileNm);
	}	
	
	public static File setExcelDownLoad(String headerName1, String headerName2, String columnName, List<?> dataList, String filePath, String fileNm){
		String newFileNm = filePath + System.getProperty("file.separator") + fileNm;
		new File(filePath).mkdirs();
		try {
			String[] headerNames1 = headerName1.split("\\|");
			String[] headerNames2 = headerName2.split("\\|");
			String[] columnNames = columnName.split("\\|");
			
			XSSFWorkbook wb = new XSSFWorkbook();
			Sheet sheet = wb.createSheet("sheet1");
			/** 해더 생성 */
			sheet = setHeaderRow(sheet, headerNames1, 0);			
			
			//sheet.addMergedRegion(CellRangeAddress.valueOf("A1:A2"));
			
			sheet.addMergedRegion(new CellRangeAddress(0,1,0,0));
			sheet.addMergedRegion(new CellRangeAddress(0,1,1,1));
			sheet.addMergedRegion(new CellRangeAddress(0,1,2,2));
			sheet.addMergedRegion(new CellRangeAddress(0,0,3,5));
			sheet.addMergedRegion(new CellRangeAddress(0,0,6,7));
			sheet.addMergedRegion(new CellRangeAddress(0,1,8,8));
			sheet.addMergedRegion(new CellRangeAddress(0,1,9,9));
			sheet.addMergedRegion(new CellRangeAddress(0,1,10,10));
			sheet.addMergedRegion(new CellRangeAddress(0,1,11,11));
			sheet.addMergedRegion(new CellRangeAddress(0,1,12,12));
			
			sheet = setHeaderRow(sheet, headerNames2, 1, 3);
			
			for (int i = 0; i < dataList.size(); i++) {
				Object vo = dataList.get(i);
				Map<String, Object> dataMap = ClassUtil.toMap(vo);
				sheet = setCellRow(sheet, (i+1), columnNames, dataMap);
			}
			// 해당 워크시트를 저장함.
			FileOutputStream stream = null;
		
			stream = new FileOutputStream(newFileNm);
			wb.write(stream);
			stream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return new File(newFileNm);
	}

	/**
	 * 엑셀 해더 부분 설정
	 * @param sheet
	 * @param headerNames
	 */
	private static Sheet setHeaderRow(Sheet sheet, String[] headerNames) {
		Row headerRow = sheet.createRow(0);
		for (int i = 0; i < headerNames.length; i++) {
			headerRow.createCell(i).setCellValue(headerNames[i]);			
		}		
		return sheet;
	}
	
	private static Sheet setHeaderRow(Sheet sheet, String[] headerNames, int rowNum) {
		Row headerRow = sheet.createRow(rowNum);
		for (int i = 0; i < headerNames.length; i++) {
			headerRow.createCell(i).setCellValue(headerNames[i]);			
		}		
		return sheet;
	}
	
	private static Sheet setHeaderRow(Sheet sheet, String[] headerNames, int rowNum, int cellNum) {
		Row headerRow = sheet.createRow(rowNum);
		for (int i = 0; i < headerNames.length; i++) {
			headerRow.createCell(i).setCellValue(headerNames[i]);			
		}		
		return sheet;
	}
	
	/**
	 * 엑셀 DATA부분 설정
	 * @param sheet
	 * @param rowNum
	 * @param columnNames
	 * @param dataMap
	 * @return
	 */
	private static Sheet setCellRow(Sheet sheet, int rowNum, String[] columnNames, Map<String, Object> dataMap) {
		Row headerRow = sheet.createRow(rowNum);
		for (int i = 0; i < columnNames.length; i++) {
			String data = null;
			if(columnNames[i].equals("rowNum")){
				data = Integer.toString(rowNum);
			}else{
				data = dataMap.get(columnNames[i]) == null? "" : dataMap.get(columnNames[i]).toString();				
			}
			headerRow.createCell(i).setCellValue(data);			
		}
		return sheet;
	}
	
	/**
	 * 엑셀upload
	 * String columnName
	 * @param headerName
	 * @param columnName
	 * @param dataList
	 * @return 
	 */
	public static List<Object> setExcelUpLoad(HttpServletRequest request, String columnName, String filePath, Class<?> clazz){
		/** 받아온 파일을 서버에 저장 */
		File destination = createFile(request,filePath);
		String[] columnNames = columnName.split("\\|");
		List<Object> classList = new ArrayList<Object>();
		
//		String regExpNm = "[^가-힣a-zA-Z]";
		String regExpNum = "[^0-9]"; 
//		String regExpNumSign = "[^-0-9]";
		String value = "";
		
		try {
			XSSFWorkbook workBook = new XSSFWorkbook(new FileInputStream(destination));
			XSSFSheet sheet = null;
			XSSFRow row = null;
			XSSFCell cell = null;
			/** 첫번째 시트*/
			sheet = workBook.getSheetAt(0);
			/** 시트의 ROW 갯수*/
			int rows = sheet.getPhysicalNumberOfRows();
			/** RWO 갯수 만큼 loop */
			for (int r = 1; r < rows; r++) {
				row = sheet.getRow(r);
				if(row == null) continue;
				Map<String, Object> map = new HashMap<String, Object>();
				for (int i = 0; i < columnNames.length; i++) {
					cell = row.getCell(i);
					if (cell == null) {
						continue;
					}
					switch(cell.getCellType()){				       
						case 0: 			// XSSFCell.CELL_TYPE_NUMERIC:
							if(HSSFDateUtil.isCellDateFormatted(cell)){
								Date date = cell.getDateCellValue();
								SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
								value = format.format(date).toString();
							}
							else
								value = ""+ (long)Math.round(cell.getNumericCellValue());						     		
				     		break;
				     	case 1:   //XSSFCell.CELL_TYPE_STRING:
				     		value = cell.getRichStringCellValue().getString();
				     		break;				       
				     	case 2:    //XSSFCell.CELL_TYPE_FORMULA :
				     		value = cell.getCellFormula();
				     		break;				       
				     	case 3:     //XSSFCell.CELL_TYPE_BLANK:
				     		value = "";
				     		break;				       
				     	case 4:     //XSSFCell.CELL_TYPE_BOOLEAN:
				     		value = "";
				     		break;
				     	case 5:     //XSSFCell.CELL_TYPE_ERROR:
				     		value = ""; //cell.getErrorCellString();				       
				     	default:		
				     		value = "";
				    	    System.out.println("");				       
					}
					
					map.put(columnNames[i], value);
				}
				Object instance = clazz.newInstance();
				BeanUtils.populate(instance, map);
				classList.add(instance);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} finally {
			destination.delete();
		}
		return classList;
	}
	
	/**
	 * HttpServletRequest 넘어온 파일을 서버에 저장
	 * @param request
	 * @return
	 */
	private static File createFile(HttpServletRequest request, String filePath) {
		MultipartRequest FileNames = (MultipartRequest)request;
		Iterator<String> iterator = FileNames.getFileNames();
		File destination = null;
		new File(filePath).mkdirs();
		while (iterator.hasNext()) {
			String element = iterator.next();
			List<MultipartFile> multiparts = FileNames.getFiles(element);
			for (int it = 0; multiparts.size() > it; it++) {
				CommonsMultipartFile file = (CommonsMultipartFile)multiparts.get(it);
				try {
					FileItem fileItem = file.getFileItem();
					// 파일명이 null 아니면 파일 업로드 실행
					if(!fileItem.getName().equals("")){
						// 새로운 파일 생성
						filePath = filePath + "/" + file.getOriginalFilename();
						destination = new File(filePath);
						fileItem.write(destination);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return destination;
	}
}
