package com.util;


import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;


public final class ClassUtil {
	
	public static Object bindObject(HttpServletRequest request, Class<?> clazz) {
		Map<String, ? extends Object> map = parameterToMap(request);
		Object instance = null;
		try {
			instance = clazz.newInstance();
			BeanUtils.populate(instance, map);
		} catch (InstantiationException e) {
			throw new RuntimeException(e.getMessage());
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e.getMessage());
		} catch (InvocationTargetException e) {
			throw new RuntimeException(e.getMessage());
		}
		return instance;
	}

	public static Object bindObject(HttpServletRequest request, Class<?> clazz, int n) {
		Map<String, ? extends Object> map = parameterToMap(request, n);
		Object instance = null;
		try {
			instance = clazz.newInstance();
			BeanUtils.populate(instance, map);		
		} catch (InstantiationException e) {
			throw new RuntimeException(e.getMessage());
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e.getMessage());
		} catch (InvocationTargetException e) {
			throw new RuntimeException(e.getMessage());
		}
		return instance;
	}
	
	public static Object getParameter(HttpServletRequest request, String key) {
		Map<?, ?> map = parameterToMap(request);
		return map.get(key);
	}
	
	public static Map<String, Object> parameterToMap(HttpServletRequest request, int n) {
		Map<String, Object> parameterMap = new HashMap<String, Object>();
		
		Enumeration<?> e = request.getParameterNames();
		String name = null, valu = "";
		while (e.hasMoreElements()) {
			name = e.nextElement().toString();
    		
    		if(request.getParameterValues(name).length > 1) {
    			valu = request.getParameterValues(name)[n] ;
    		} else {
    			valu = request.getParameter(name);
    		}
    		if("".equals(valu)){
				valu = null;
			}
    		parameterMap.put(name, valu);
		}
		return parameterMap;
	}
	
	private static Map<String, Object> parameterToMap(HttpServletRequest request) {
		Map<String, Object> parameterMap = new HashMap<String, Object>();

		Enumeration<?> e = request.getParameterNames();
		String parameterName = null;
		String[] parameterValues = null;

		while (e.hasMoreElements()) {
			parameterName = (String) e.nextElement();
			parameterValues = request.getParameterValues(parameterName);
			try {
				if (parameterValues.length > 1) {
					for (int i = 0; i < parameterValues.length; i++) {
						parameterValues[i] = URLDecoder.decode(
								parameterValues[i], "UTF-8");
						if ("".equals(parameterValues[i])) {
							parameterValues[i] = null;
						}
					}
					parameterMap.put(parameterName, parameterValues);
				} else {
					if ("".equals(parameterValues[0])) {
						parameterMap.put(parameterName, null);
					} else {
//						parameterMap.put(parameterName, URLDecoder.decode(parameterValues[0], "UTF-8"));
						parameterMap.put(parameterName, parameterValues[0]);
					}

				}
			} catch (UnsupportedEncodingException e1) {
				e1.printStackTrace();
			}
		}
		return parameterMap;
	}	
	
	
	/**
	 * VO 클래스를 Map으로 변환
	 * @param vo
	 * @return
	 */
	public static Map<String, Object> toMap(Object vo) {
		Map<String, Object> map = new HashMap<String, Object>();

		for (Field field : vo.getClass().getDeclaredFields()) {
			try {
				field.setAccessible(true);
				map.put(field.getName(), field.get(vo));
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return map;
	}
	
	/**
	 * 다중 체크박스 값을 String 으로 변환 
	 * @param request
	 * @param key
	 * @return
	 */
	public static String multiCheckToString(HttpServletRequest request, String key, String delimiter){
		Object obj = getParameter(request, key);
		String value = "";
		if(obj != null && obj.getClass().isArray()){
			String[] values = (String[])obj;
			for (int i = 0; i < values.length; i++) {
				value += values[i] + delimiter;
			}
			value = value.substring(0, value.length()-1);
		}else if(obj != null){
			value = (String)obj;
		}
		return value;
	}
	
}
