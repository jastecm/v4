package com.util;

import java.util.Map;

import v4.web.vo.AccountVO;

public final class Util {
	
	public static void setCurrentInfoToMap(AccountVO vo, Map<String,? super String> param){
		param.put("corpKey", vo.getCorpKey());
		param.put("lang", vo.getLang());
	}
}
