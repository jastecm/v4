package com.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.jboss.logging.Logger;

//import com.infinityplus.viewcarclinic.rest.MobileService;

public class SMS {
	
	Logger logger = Logger.getLogger(SMS.class);
	
	//SMS 솔루션 시작
	public String sg_http(String gourl) {
	 
		String StrTemp="";
		long startTime = System.currentTimeMillis();		//long SG_TIMER = System.currentTimeMillis();

		SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
		Date currentTime = new Date ( );
		String today = mSimpleDateFormat.format ( currentTime );
		
		try {

			URL url = new URL(gourl);
			URLConnection con = url.openConnection();
			con.setDoOutput(true);
			
			BufferedReader  br = new BufferedReader( new InputStreamReader( con.getInputStream() ));

			String line;
			while( (line = br.readLine()) != null )
				StrTemp = StrTemp + line;
			
			long endTime = System.currentTimeMillis();			
			logger.info("/sendSMS : ## 시작시간 : " +  today + " " + formatTime(startTime) + " ## 종료시간 : " + formatTime(endTime) + " ## 소요시간(초.0f) : " + ( endTime - startTime )/1000.0f +"초");
			
			br.close();
		    
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("/sendSMS : ## 전송실패 ");
		} finally {
			System.out.println("/sendSMS : ## SMS 결과코드 : " + StrTemp + " ## SMS 수신코드 설명 : " + smsReceiveCodeExplain(StrTemp) );
			System.out.println("/sendSMS : ## 시작시간 : " +  today + " " + formatTime(startTime) );
		}

		return StrTemp;
	}

	public String sg_url_encode(String strName) throws Exception {
		String ret_val ="";
		if (!"".equals(strName)) {
			ret_val = URLEncoder.encode(strName,"euc-kr");
		}
		return ret_val;
	}
	
	public String sms_send(String v3, String v4, String v5, String v6, String v7) throws Exception {
		String v1 = "dartz"; // 아이디
		String v2 = "dartz1234";// 비밀번호
		// 보낸사람 v3 = "16881635"
		// 받는사람 v4 = "01094685964"
		//01062640797 
		// 보낼내용 v5 = "~!@#$%^&*()_+`1234567890-=="
		// 발송일자 v6
		// 발송시각 v7
		
		String[] toNumber = v4.split(";");
		
		String strurl = "";
		String sResult = "";
		
		logger.info("/mobile/sendSMS  toNumber Length " +  toNumber.length);
		v3 = "15998439";
		
		for(int i=0; i<toNumber.length;i++) {
			//toNumber[i] = toNumber[i].replaceAll("-", "");
			logger.info("/mobile/sendSMS : ##보낸사람 : " + v3 + " ## 받는사람 : " + toNumber[i] );
			strurl="http://sms.annexlab.com/smssendv10.asp?v1="+v1+"&v2="+v2+"&v3="+v3+"&v4="+toNumber[i]+"&v5="+sg_url_encode(v5)+"&v6="+v6+"&v7="+v7;
			sResult += sg_http(strurl)+",";
		}
		
		return sResult;
	}
	
	public String clinicSmsSend(String v3, String v4, String v5, String v6, String v7) throws Exception {
		String v1 = "dartz"; // 아이디
		String v2 = "dartz1234";// 비밀번호
		// 보낸사람 v3 = "16881635"
		// 받는사람 v4 = "01094685964"
		// 보낼내용 v5 = "~!@#$%^&*()_+`1234567890-=="
		// 발송일자 v6
		// 발송시각 v7
		
		String strurl = "";
		String sResult = "";
		
		logger.info("/clinic/sendSMS : ## 전송내역 : 보낸사람 : " + v3 + " ## 받는사람 : " + v4 );
		strurl="http://sms.annexlab.com/smssendv10.asp?v1="+v1+"&v2="+v2+"&v3="+v3+"&v4="+v4+"&v5="+sg_url_encode(v5)+"&v6="+v6+"&v7="+v7;
		sResult = sg_http(strurl);
		
		return sResult;
	}

	public String sms_cancel(String v1) {
		String v2 = "dartz"; // 아이디
		String v3 = "dartz1234"; // 비밀번호
	
		String strurl = "http://sms.annexlab.com/smscancelv10.asp?v1="+v1+"+v2="+v2+"+v3="+v3;
	
		return sg_http(strurl);
	}
	

	public String formatTime(long lTime) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(lTime);
		return (c.get(Calendar.HOUR_OF_DAY) + "시 " + c.get(Calendar.MINUTE) + "분 " + c.get(Calendar.SECOND) + "." + c.get(Calendar.MILLISECOND) + "초");
	}
	
	//WEB 발송 오류 결과 코드
	public String smsReceiveCodeExplain(String errCode) {
		
		int i = Integer.parseInt(errCode);
		String retEx = "";
		
		if(i > 0) {
			
			// 발송 SMS 고유번호 리턴-해당 고유번호는 SMS예약취소시 사용
			retEx = "정상";
			
		} else {
			
			if(i <= -90000) {
				
				retEx = "발송량 제한";
				
			} else {
				
				switch (i) {
					case -2	:	retEx = "사용자 아이디/비밀번호 오류";				break;
					case -3	:	retEx = "IP 사용 거부";								break;
					case -4	:	retEx = "일반 발송 파라미터 값 오류(SQL에 삽입될수 없는 값)";		break;
					case -5	:	retEx = "예약 발송 파라미터 값 오류(SQL에 삽입될수 없는 값)";		break;
					case -6	:	retEx = "발송 과금 처리 오류시 (해당코드 발생시 관리자 문의)";		break;
					case -7	:	retEx = "발송 과금 처리 오류시 (해당코드 발생시 관리자 문의)";		break;
					case -8	:	retEx = "예약일자 오류일 경우";						break;
					case -9	:	retEx = "수신전화번호 오류 or 불법스팸일 경우";		break;
					case -11 :	retEx = "입력값 오류";								break;
					case -12 :	retEx = "국제 SMS 일 경우";							break;
					case -14 :	retEx = "동일발신번호로 동일 수신번호에게 하루 200개 이상 발송 불가";	break;
					case -20 :	retEx = "동일번호, 동일내용은 하루 20건이상 발송할 경우 차단";			break;
					case -21 :	retEx = "동일번호에 1분동안 10건 이상 발송할 경우 차단";				break;
					case -99 :	retEx = "수신번호 or 사용자아이디 or 사용자비밀번호 가 없을 경우";		break;
					case -101 :	retEx = "스팸 메시지 차단";												break;
					default	:	retEx = "해당 숫자가 없습니다";			      		break;
			    }
			
			}
		}
		
		return retEx;
	}

}


/*
WEB 발송 오류 결과 코드 설명 ( 발송코드 : 0)

오류코드	오류설명
0 이상일경우	정상 [발송 SMS 고유번호 리턴-해당 고유번호는 SMS예약취소시 사용합니다]
-2	사용자 아이디/비밀번호 오류
-3	IP 사용 거부
-4	일반 발송 파라미터 값 오류(SQL에 삽입될수 없는 값)
-5	예약 발송 파라미터 값 오류(SQL에 삽입될수 없는 값)
-6	발송 과금 처리 오류시 (해당코드 발생시 관리자 문의)
-7	발송 과금 처리 오류시 (해당코드 발생시 관리자 문의)
-8	예약일자 오류일 경우
-9	수신전화번호 오류 or 불법스팸일 경우
-11	입력값 오류
-12	국제 SMS 일 경우
-14	동일발신번호로 동일 수신번호에게 하루 200개 이상 발송 불가
-20	동일번호, 동일내용은 하루 20건이상 발송될수 없습니다.
-21	동일번호에 1분동안 10건 이상 발송할 경우 차단.
-99	수신번호 or 사용자아이디 or 사용자비밀번호 가 없을 경우
-101	스팸 메시지 차단
-90000대	발송량 제한
*/