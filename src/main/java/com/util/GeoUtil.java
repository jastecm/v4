package com.util;

import static java.lang.Math.PI;
import static java.lang.Math.abs;
import static java.lang.Math.asin;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static java.lang.Math.toRadians;

public final class GeoUtil {
	
	/**
     * mean radius of the earth
     */
    public final static double R = 6371000; // m
    /**
     * Radius of the earth at equator
     */
    public final static double R_EQ = 6378137; // m
    /**
     * Circumference of the earth
     */
    public final static double C = 2 * PI * R;
    public final static double KM_MILE = 1.609344;

    /**
     * Calculates distance of (from, to) in meter.
     * <p>
     * http://en.wikipedia.org/wiki/Haversine_formula a = sin짼(�lat/2) +
     * cos(lat1).cos(lat2).sin짼(�long/2) c = 2.atan2(�쉆, ��1�뭓)) d = R.c
     */
    public static double calcDist( double fromLat, double fromLon, double toLat, double toLon )
    {
        double normedDist = calcNormalizedDist(fromLat, fromLon, toLat, toLon);
        return R * 2 * asin(sqrt(normedDist));
    }

    public static double calcDenormalizedDist( double normedDist )
    {
        return R * 2 * asin(sqrt(normedDist));
    }

    /**
     * Returns the specified length in normalized meter.
     */
    public static double calcNormalizedDist( double dist )
    {
        double tmp = sin(dist / 2 / R);
        return tmp * tmp;
    }

    public static double calcNormalizedDist( double fromLat, double fromLon, double toLat, double toLon )
    {
        double sinDeltaLat = sin(toRadians(toLat - fromLat) / 2);
        double sinDeltaLon = sin(toRadians(toLon - fromLon) / 2);
        return sinDeltaLat * sinDeltaLat
                + sinDeltaLon * sinDeltaLon * cos(toRadians(fromLat)) * cos(toRadians(toLat));
    }

    /**
     * Circumference of the earth at different latitudes (breitengrad)
     */
    public static double calcCircumference( double lat )
    {
        return 2 * PI * R * cos(toRadians(lat));
    }

    public static boolean isDateLineCrossOver( double lon1, double lon2 )
    {
        return abs(lon1 - lon2) > 180.0;
    }

    public static double calcNormalizedEdgeDistance( double r_lat_deg, double r_lon_deg,
                                              double a_lat_deg, double a_lon_deg,
                                              double b_lat_deg, double b_lon_deg )
    {
        return calcNormalizedEdgeDistanceNew(r_lat_deg, r_lon_deg, a_lat_deg, a_lon_deg, b_lat_deg, b_lon_deg, false);
    }

    /**
     * New edge distance calculation where no validEdgeDistance check would be necessary
     * <p>
     * @return the normalized distance of the query point "r" to the project point "c" onto the line
     * segment a-b
     */
    public static double calcNormalizedEdgeDistanceNew( double r_lat_deg, double r_lon_deg,
                                                 double a_lat_deg, double a_lon_deg,
                                                 double b_lat_deg, double b_lon_deg, boolean reduceToSegment )
    {
        double shrinkFactor = calcShrinkFactor(a_lat_deg, b_lat_deg);

        double a_lat = a_lat_deg;
        double a_lon = a_lon_deg * shrinkFactor;

        double b_lat = b_lat_deg;
        double b_lon = b_lon_deg * shrinkFactor;

        double r_lat = r_lat_deg;
        double r_lon = r_lon_deg * shrinkFactor;

        double delta_lon = b_lon - a_lon;
        double delta_lat = b_lat - a_lat;

        if (delta_lat == 0)
            // special case: horizontal edge
            return calcNormalizedDist(a_lat_deg, r_lon_deg, r_lat_deg, r_lon_deg);

        if (delta_lon == 0)
            // special case: vertical edge        
            return calcNormalizedDist(r_lat_deg, a_lon_deg, r_lat_deg, r_lon_deg);

        double norm = delta_lon * delta_lon + delta_lat * delta_lat;
        double factor = ((r_lon - a_lon) * delta_lon + (r_lat - a_lat) * delta_lat) / norm;

        // make new calculation compatible to old
        if (reduceToSegment)
        {
            if (factor > 1)
                factor = 1;
            else if (factor < 0)
                factor = 0;
        }
        // x,y is projection of r onto segment a-b
        double c_lon = a_lon + factor * delta_lon;
        double c_lat = a_lat + factor * delta_lat;
        return calcNormalizedDist(c_lat, c_lon / shrinkFactor, r_lat_deg, r_lon_deg);
    }

    private static double calcShrinkFactor( double a_lat_deg, double b_lat_deg )
    {
        return cos(toRadians((a_lat_deg + b_lat_deg) / 2));
    }




    public boolean isCrossBoundary( double lon1, double lon2 )
    {
        return abs(lon1 - lon2) > 300;
    }

}
