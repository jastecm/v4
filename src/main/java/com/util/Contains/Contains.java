package com.util.Contains;

import java.util.HashMap;
import java.util.Map;

public final class Contains {
	
	
	public static String OLD_AUTH_KEY = "oldAuth";
	public static String NEW_AUTH_KEY = "newAuth";
	
	public static final String jastecmVonxcOrgDB = "jastecmVonxcLoraDB";
	
	public static final String jastecmVonxcPocDB = "jastecmVonxcPocDB";
	public static final String jastecmVonxcTestDB = "jastecmVonxcTestDB";
	public static final String jastecmVonxcSprintDB = "jastecmVonxcSprintDB";
	public static final String jastecmVonxcDB = "jastecmVonxcDB";
	public static final String jastecmLoraDB = "jastecmLoraDB";
	
	public static Map<String,String> deviceTarget= null;
	static{
		deviceTarget = new HashMap<String,String>();
		
		deviceTarget.put("VONXC00001", jastecmVonxcSprintDB);
		deviceTarget.put("VONXC00002", jastecmVonxcSprintDB);
		deviceTarget.put("VONXC00003", jastecmVonxcSprintDB);
		deviceTarget.put("VONXC00004", jastecmVonxcSprintDB);
		deviceTarget.put("VONXC00005", jastecmVonxcSprintDB);
		deviceTarget.put("VONXC00006", jastecmVonxcSprintDB);
		deviceTarget.put("VONXC00007", jastecmVonxcSprintDB);
		deviceTarget.put("VONXC00008", jastecmVonxcSprintDB);
		deviceTarget.put("VONXC00009", jastecmVonxcSprintDB);
		deviceTarget.put("VONXC00010", jastecmVonxcSprintDB);
		deviceTarget.put("VONXC00011", jastecmVonxcSprintDB);
		deviceTarget.put("VONXC00012", jastecmVonxcSprintDB);
		deviceTarget.put("VONXC00013", jastecmVonxcSprintDB);
		deviceTarget.put("VONXC00014", jastecmVonxcSprintDB);
		deviceTarget.put("VONXC00015", jastecmVonxcSprintDB);
		deviceTarget.put("VONXC00016", jastecmVonxcSprintDB);
		deviceTarget.put("VONXC00017", jastecmVonxcSprintDB);
		deviceTarget.put("VONXC00018", jastecmVonxcSprintDB);
		deviceTarget.put("VONXC00019", jastecmVonxcSprintDB);
		deviceTarget.put("VONXC00020", jastecmVonxcSprintDB);
		deviceTarget.put("VONXC00021", jastecmVonxcPocDB);
		deviceTarget.put("VONXC00022", jastecmVonxcPocDB);
		deviceTarget.put("VONXC00023", jastecmVonxcPocDB);
		deviceTarget.put("VONXC00024", jastecmVonxcPocDB);
		
		deviceTarget.put("VONXC00025", jastecmVonxcTestDB);
		
		deviceTarget.put("VONXC00026", jastecmVonxcPocDB);
		deviceTarget.put("VONXC00027", jastecmVonxcPocDB);
		deviceTarget.put("VONXC00028", jastecmVonxcPocDB);
		deviceTarget.put("VONXC00029", jastecmVonxcPocDB);
		deviceTarget.put("VONXC00030", jastecmVonxcPocDB);
		deviceTarget.put("VONXC00031", jastecmVonxcPocDB);
		deviceTarget.put("VONXC00032", jastecmVonxcPocDB);
		deviceTarget.put("VONXC00033", jastecmVonxcPocDB);
		deviceTarget.put("VONXC00034", jastecmVonxcPocDB);
		deviceTarget.put("VONXC00035", jastecmVonxcPocDB);
		
		deviceTarget.put("VONXC00036", jastecmVonxcTestDB);
		
		deviceTarget.put("VONXC99999", jastecmVonxcTestDB);
		deviceTarget.put("VONXC99998", jastecmVonxcTestDB);
		deviceTarget.put("VONXC99997", jastecmVonxcTestDB);
		deviceTarget.put("VONXC99996", jastecmVonxcTestDB);
		deviceTarget.put("VONXC99995", jastecmVonxcTestDB);
		deviceTarget.put("VONXC99994", jastecmVonxcTestDB);
		deviceTarget.put("VONXC99993", jastecmVonxcTestDB);
		deviceTarget.put("VONXC99992", jastecmVonxcTestDB);
		deviceTarget.put("VONXC99991", jastecmVonxcTestDB);
		deviceTarget.put("VONXC99990", jastecmVonxcTestDB);
	}
}
