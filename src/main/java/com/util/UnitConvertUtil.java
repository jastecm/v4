package com.util;



public class UnitConvertUtil {
	
	//mi -> km : 1.60934
	//km -> mi : 0.621371
	
	public static double unitToKm(double v , String unit){
		
		if(unit.equals("mi")){
			return Math.round(v*1.60934*100)/100d;
		}
		
		return v;
	}
	
	public static double unitToKm(int v , String unit){
		
		if(unit.equals("mi")){
			return Math.round(v*1.60934*100)/100d;
		}
		
		return v;
	}
	
	public static double kmToM(double v){
		return Math.round(v*1000);
	}
}